﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BBLDatafeed.aspx.cs" Inherits="tikAeroB2C.BBLDatafeed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    </form>
    <%
        if (Request.HttpMethod.ToUpper() == "GET")
        {
            %>
                <div id="dvLoad" style="text-align:center; padding-top:25px;" >
                    <img src="App_Themes/Default/Images/loading.gif" alt="" />
                </div>
    
                <div id="PaymentResult" style="font-weight:bold; font-size:medium; width:100%; text-align:center; padding-top:35px;"></div>
    
                 <div id="dvLoadBar" style="text-align:center; padding-top:25px; display: none;">
                    <div style="width:470px; height:15px; background-color:#F2E0CF;">
                    <div style="font-family: Trebuchet MS; font-size:14px; font-weight:bold; color:#043168; text-align:center; padding-top:10px;">
                        Processing</div>
                    <div style="text-align:center; padding:20px 0px 20px 0px;"><img src="App_Themes/Default/Images/progressbar.gif" width="278" height="21" alt="" /></div>
                    <div style="font-family: Trebuchet MS; font-size:12px; font-weight:bold; color:#043168; text-align:center;">
                        Please wait while we process your booking or payment process.<br />
                        This will take approximately 1 minute and
                        <br />
                        during this time you should not use any of the buttons on your browser.
                        <br />
                        <br />
                    </div>
                    </div>
                </div>   
            <%
            if (Request.QueryString["resultBBL"] != "")
            {

                Response.Write("<form name='payFormCcard' method='post' action='default.aspx'>");
                Response.Write("\r\n");
                Response.Write("<input type='hidden' name='resultBBL' value='" + Request.QueryString["resultBBL"] + "'>");
                Response.Write("\r\n");
                Response.Write("<input type='hidden' name='Ref' value='" + Request.QueryString["Ref"] + "'>");
                Response.Write("\r\n");
                Response.Write("</form>");
                Response.Write("\r\n");
                Response.Write("<script type='text/javascript'>");
                Response.Write("\r\n");
                
                Response.Write("var objPaymentResult = document.getElementById('PaymentResult');");
                Response.Write("\r\n");
                 
    
                if (Request.QueryString["resultBBL"] == "Success")
                {
                    Response.Write("PaymentResult.innerHTML =''; ");
                    Response.Write("\r\n");
                    Response.Write("var objLoader = document.getElementById('dvLoadBar');");
                    Response.Write("\r\n");
                    Response.Write("objLoader.style.display = 'block';");
                    Response.Write("\r\n");
                }
                else
                    Response.Write("PaymentResult.innerHTML ='!!! Tansaction was " + Request.QueryString["resultBBL"] + " !!!'; ");
                
                Response.Write("document.payFormCcard.submit(); ");
                Response.Write("</script>");
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }
    %>
</body>
</html>
