<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CMSLogin.aspx.vb" Inherits="TikAero.Web.CMS.CMSLogin" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

Protected Sub btnSubmit_Click1(ByVal sender As Object, ByVal e As System.EventArgs)

End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Login Page</title>
	<link href="HTML/styleSheets/cms_stylesheet.css" rel="stylesheet" type="text/css" />
	<script language="javascript">
			function validAllElm(){
				var e = '';
				var bool = true;
				if (Form1.ctrClientBase_ctrLogonBase_tboClientNumber.value == ""){
						e += "<li>Please enter User Id.</li>" ;
						Form1.ctrClientBase_ctrLogonBase_tboClientNumber.className = 'error'
				}
				if (Form1.ctrClientBase_ctrLogonBase_tboClientPassword.value == ""){
						e += "<li>Please enter Password.</li>" ;
						Form1.ctrClientBase_ctrLogonBase_tboClientPassword.className = 'error'
				}
				if(e.length > 0){
					 var Container = document.getElementById('Label1');	
					 Container.innerHTML = 	'<ul>' + e + '</ul>';
					 bool = false;
				}
				return 	bool;	 
			}
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Wrapper">
             <div class="ImgHeader"></div>
			 <div class="clearboth"></div>
				<div class="WrapperBody">
				<div class="TBLCMSlogin">
				<div class="CMSloginHeader">Please Enter Your Details</div>
				<TABLE class="FormCustomerLogin"cellSpacing="0" cellPadding="2" border="0" width="400px">
										<TR>
											<TD colSpan="4" height="10"></TD>
										</TR>
										<TR>
											<TD rowSpan="2"><B>User Login</B></TD>
											<TD>User Id:</TD>
											<TD><input type="text" name="ctrClientBase:ctrLogonBase:tboClientNumber" id="ctrClientBase_ctrLogonBase_tboClientNumber" runat="server"></TD>
											<TD>&nbsp;</TD>
										</TR>
										<TR>
											<TD>Password:</TD>
											<TD><input type="text" name="ctrClientBase:ctrLogonBase:tboClientPassword" id="ctrClientBase_ctrLogonBase_tboClientPassword" runat="server"></TD>
										    <TD></TD>
										</TR>									
                    <tr>
                        <td rowspan="1">
                        </td>
                        <td>
                            Langauge</td>
                        <td>
                            <asp:DropDownList ID="ddlLang" runat="server">
                                <asp:ListItem Selected="True" Value="en-US">English</asp:ListItem>
                                <asp:ListItem Value="ja-JP">Japan</asp:ListItem>
                                <asp:ListItem Value="fr-FR">France</asp:ListItem>
                            </asp:DropDownList></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="1">
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td rowspan="1">
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Log In" Width="74px" OnClick="btnSubmit_Click1" /></td>
                        <td>&nbsp;</td>
                    </tr>
				</TABLE>
				</div>				
				</div>
				<font color="red"><asp:Label id="Label1" runat="server"></asp:Label></font> 
				<div class="clearboth"></div>
				<div class="Footer"></div>
			 </div>
    </form>
</body>
</html>