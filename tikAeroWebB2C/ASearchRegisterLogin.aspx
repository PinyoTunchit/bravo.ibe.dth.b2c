<%@ Page Language="C#" AutoEventWireup="true" Theme="Default" CodeBehind="ASearchRegisterLogin.aspx.cs" Inherits="tikAeroB2C.ASearchRegisterLogin" Async="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>aurigny.com - channel islands</title>
   
    <script type="text/javascript" src="Scripts/Main.js"></script>
    <script type="text/javascript" src="Scripts/uxCalendar.js"></script>
    <script type="text/javascript" src="Scripts/NewsRegisterDetail.js"></script>    
    <!--Mark Edit-->    
        <script language="javascript" src="Scripts/TextBox/Global.js" type="text/javascript"></script>
        <script language="javascript" src="Scripts/TextBox/Textbox.Restriction.js" type="text/javascript"></script>
        <script language="javascript" src="Scripts/TextBox/Textbox.MaskEdit.js" type="text/javascript"></script>
        <script language="javascript" src="Scripts/TextBox/Textbox.Trim.js" type="text/javascript"></script>
        <script language="javascript" src="Scripts/TextBox/Textbox.Tip.js" type="text/javascript"></script>
   <!--Mark Edit-->
</head>
    <body>
        <form name="frmMain" id="frmMain" runat="server">
                <!--Fading-->
                <input type="hidden" id="hdLang" name="hdLang"/>
                <div id="dvProgressBar" class="DisableWindow" style="display:none;"></div>
                <!--Start Error Box-->
		        <div id="dvMessageBox" class="ErrorBox" style="display:none;">
			        <div class="ErrorBoxHeader"></div>
			        <div id="dvMessageIcon" class="ErrorBoxMid"></div>
		            <div id="dvErrorMessage" class="ErrorBoxBot">
		              Error Error Error Text Text Text Text Text Text Error Error Error Text Text Text Text Text		            </div>
	                <div class="clearboth"></div>
	                <div class="ButtonError" onclick="ShowMessageBox('', 0,'');">
                        <div class="buttonCornerLeft"></div>
                        <div class="buttonContent">CLOSE</div>
                        <div class="buttonCornerRight"></div>
                    </div>
					<div class="clearboth"></div>
					<div class="ErrorBoxFoot"></div>
                </div>
		        <!--End Error Box-->
                <!--progress bar-->
                <div id="dvFormHolder" class="IndicatorPax" style="display:none;"></div>
                <div id="dvLoad" class="Indicator" style="display:none;">
                    <img src="App_Themes/Default/Images/loading.gif" alt=""/>                </div>
                <!--Loading Bar-->
                <div id="dvLoadBar" class="ProgressBarBox" style="display:none;">
                <div class="ProgressBarHeaderTxt">Processing your changes</div>
                    <div class="ProgressBarAnimate">
                      <img src="App_Themes/Default/Images/progressbar.gif" width="227" height="23" alt=""/>                    </div>
                    <div class="ProgressBarBodyTxt">
                      Please wait while we process your booking or payment process.<br />
                      This will take approximately 1 minute  and <br />
                      during this time you should not use any of the buttons on your browser.                    </div>
                </div>
                
                <asp:ScriptManager ID="smService" runat="server">
                    <Services>
                        <asp:ServiceReference Path="WebService/B2cService.asmx" />
                         <asp:ServiceReference Path="WebService/WebUIRender.asmx" />                
                    </Services>
                </asp:ScriptManager>
                
                <div class="Wrapper">
				<div class="WrapperLeftSection">
                    <div class="ImgHeader">
					
					<div class="clearboth"></div>
                        <div class="HeaderMenu">
                            <!--Menu Section-->
                            <div id="dvMenu" class="menu" runat="server">                            </div>
				        </div>
						</div>
                        <div class="clearboth"></div>
                        <!--Start Main Contend-->
                        <div class="WrapperBody">
						<div class="WrapperPanelLeft" id="dvLeftPanel">				            </div>
				            <div class="WrapperPanelMid">
				                <div id="dvContainer" runat="server" >
				                    <div id="dvLogin">    
    <table border="0" cellspacing="10" cellpadding="0" align="center" bgcolor="#ffffff"  class="RegisterB2C">
	    <tr>
			<td class="groupForm" style="width: 618px; height: 35px;" colspan="2">
				<font size="2"><b>Please enter your details</b></font>
			</td>
		</tr>
		<tr>
		    <td style="width: 100px"></td>
			<td style="width: 518px" align="center">
				<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#dddddd">
				    <tr>
		                <td colspan="2" style="height: 24px"></td>
		            </tr>
					<tr>
		                <td class="groupForm" style="width: 88px; height: 28px; text-align:left;">User Id:</td>
		                <td class="groupForm" style="height: 28px; width: 490px; text-align:left;">
		                <input id="txtUserId" runat="server" style="width: 155px">
		                <span id="spErrUserId" class="RequestStar">*</span></td>
		            </tr>
		            <tr>
		                <td class="groupForm" style="height: 28px; text-align:left;">Password:</td>
		                <td class="groupForm" style="height: 28px; text-align:left;">
		                <input id="txtPassword" onkeypress="return SubmitNewsAdminLogin(this,event)" runat="server" type="password" style="width: 155px">
		                <span id="spErrPassword" class="RequestStar">*</span></td>
		            </tr>
		            <tr>
		                <td colspan="2" style="height: 24px">&nbsp;</td>
		            </tr>
		            <tr>
		                <td></td>
		                <td class="groupFormClear">
		                  <div class="RegisterSubmitButton" onclick="NewsAdminLogin();">
                            <div class="buttonCornerLeft"></div>
                            <div class="buttonContent">Logon</div>
                            <div class="buttonCornerRight"></div>
                          </div> 
		                </td>
		            </tr>
		            <tr>
		                <td colspan="2" style="height: 24px">&nbsp;</td>
		            </tr>
				</table>																			
			</td>
		</tr>
	</table>	
                                    </div>
                                <div id="dvPaxError" class="NBStep4"></div>
                                </div>
				                    <div id="dvSubContainer" style="display:none" runat="server"></div>
				                </div>
                            <!--End Avai Section-->
				        </div>
				        
				       <!--End Main Contend-->
				        <div class="clearboth"></div>
                </div>
				
				</div>
				<div class="clearboth"></div>
	<div id="dvInputMail" style="display:none">
    <table cellSpacing="2" width ="830" border="0" class="RegisterB2C">
        <tr><td colspan="3" class="headerWithSub">Send Email</td></tr>
		<TR>
			<TD class="groupForm" style="width: 100px; height: 30px">From :</TD>
			<TD class="groupForm">&nbsp;</TD>
			<TD class="groupForm" style="width: 714px;">
			<input id="txtMailFrom" readonly runat="server" style="width: 201px">
			<span id="spErrMailFrom" class="RequestStar">*</span></td>
		</TR>
		<tr>
			<td class="groupForm" style="width: 100px; height: 30px">To :</td>
			<td class="groupForm">&nbsp;</td>
			<td class="groupForm" style="width: 714px;">
			<input id="txtMailTo" runat="server" style="width: 600px">
			<span id="spErrMailTo" class="RequestStar">*</span></td>
		</tr>
		<TR>
			<TD class="groupForm" style="width: 100px; height: 30px">Bcc :</TD>
			<TD class="groupForm" style="height: 30px"></TD>
			<TD class="groupForm" style="height: 30px; width: 714px;">
			<input type="hidden" id="txtMailBCC" runat="server" style="width: 100px">
			<span id="lblBcc" runat="server"></span></TD>
		</TR>
		<TR>
			<TD class="groupForm" style="width: 100px">Additional Bcc :</TD>
			<TD class="groupForm"></TD>
			<TD class="groupForm" style="width: 714px">
            <textarea id="txtMailAddBcc" runat="server" style="width: 600px; height: 47px"></textarea>
            <span id="spErrMailBCC" class="RequestStar"></span></TD>
		</TR>
		<TR>
			<TD class="groupForm" style="width: 100px; height: 32px">Subject :</TD>
			<TD class="groupForm" style="height: 32px"></TD>
			<TD class="groupForm" style="height: 32px; width: 714px;">
			<input id="txtMailSubject" runat="server" MaxLength="60" style="width: 600px">
			<span id="spErrMailSubject" class="RequestStar">*</span></TD>
		</TR>
		<TR>
			<TD class="groupForm" style="width: 100px; height: 32px">Attachments :</TD>
			<TD class="groupForm" style="height: 32px"></TD>
			<TD class="groupForm" style="height: 32px; width: 714px;">
			<INPUT id="txtMailAttachFile" style="WIDTH: 400px; height:21px;" type="file" runat="server"></TD>
		</TR>
		<TR>
			<TD class="groupForm" style="width: 100px; ">Message :</TD>
			<TD class="groupForm" ></TD>
			<TD class="groupForm" style="width: 714px;">
			<textarea id="txtMailMessage" runat="server" style="width: 600px; height: 111px"></textarea>
			<span id="spErrMailMessage" class="RequestStar"></span></TD>
		</TR>
		<tr>
			<td align="center" colSpan="3">
			    <div class="RegisterCancelButton" onclick="CloseMailForm()">
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent">Back</div>
					<div class="buttonCornerRight"></div>
			    </div>
				<div class="RegisterSubmitButton" onclick="SendRegisterMail()">
                    <div class="buttonCornerLeft"></div>
                    <div class="buttonContent">Send</div>
                    <div class="buttonCornerRight"></div>
                </div>                 
			</td>
		</tr>
	</table>  
	</div>
	
	<div id="dvTemp" style="display:none"></div>
	 
    <div id="dvResultMailComplete" style="display:none" runat="server">
	  <table width ="830" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="RegisterB2C">
	        <tr><td style="height: 39px">&nbsp;</td></tr>
			<tr>
			<td class="groupForm" style="height: 23px" align="center">
			<div style="FONT-SIZE:20px;COLOR:#000066">
			<STRONG>Send Mail Complete</STRONG></div>
			</td>
			</tr>
			<tr>
				<td  class="groupForm" style="height: 33px">&nbsp;</td>
			</tr>
			<TR>
			    <td align="center">
				<div class="RegisterCancelButton" onclick="GetNewsAdmin();">
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent">Back</div>
					<div class="buttonCornerRight"></div>
			    </div> 
			    </td>
			</TR>
	  </table>
	</div>
	
	<div id="dvResultMailError" style="display:none" runat="server">
	  <table width ="830" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="RegisterB2C">
	        <tr><td style="height: 39px">&nbsp;</td></tr>
			<tr>
			<td class="groupForm" style="height: 23px" align="center">
			<div style="FONT-SIZE:20px;COLOR:#000066">
			<STRONG>Error send mail. Please check and send again.</STRONG></div>
			</td>
			</tr>
			<tr>
				<td  class="groupForm" style="height: 33px">&nbsp;</td>
			</tr>
			<TR>
			    <td align="center">
				<div class="RegisterCancelButton" onclick="GetNewsAdmin();">
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent">Back</div>
					<div class="buttonCornerRight"></div>
			    </div> 
			    </td>
			</TR>
	  </table>
	</div>	
	
				<div class="Footer">
						<div class="FooterBox">
						<ul>
						<li class="left">
						 &copy;  2007-2010 Aurigny Air Services. All rights reserved.  				 </li>
						 <li class="right"><a href="http://www.tiksystems.com" target="_blank"><img src="App_Themes/Default/Images/tiklogo.gif" border="0" /></a> </li>
						 </ul>
						 </div>
						</div>
        </form>
    </body>
</html>