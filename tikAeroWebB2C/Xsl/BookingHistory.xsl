<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">
	<xsl:template name="paging" match="object/paging">
		<xsl:variable name="page">
			<xsl:value-of select="/object/paging/pageindex"/>
		</xsl:variable>
		<table border="0" cellspacing="0" cellpadding="0" class="TBLLiveBooking">
			<xsl:if test="string-length($page)>0">
				<tr>
					<td class="Paging">
						<div align="right">
							<xsl:for-each select="/object/paging/pageindex">
								<xsl:if test="/object/paging/selected!=position()">
									<a>
										<xsl:attribute name="href">
											javascript:GetHistoryBooking('<xsl:value-of select="position()"/>');
										</xsl:attribute>
										<xsl:value-of select="position()"/>
									</a>&#160;
								</xsl:if>
								<xsl:if test="/object/paging/selected=position()">
									<xsl:value-of select="position()"/>&#160;
								</xsl:if>
							</xsl:for-each>
						</div>
					</td>
				</tr>
			</xsl:if>
		</table>
	</xsl:template>
	
	<xsl:template  match="ClientBookings">
		<table border="0" cellspacing="0" cellpadding="0" class="TBLLiveBooking">
			<tr>
				<tr>
					<td class="HeadCOL1">&#160;</td>
					<td class="HeadCOL2">
            <xsl:value-of select="tikLanguage:get('FFP_My_Booking_9','Bookings')" />
          </td>
					<td class="HeadCOL3">
            <xsl:value-of select="tikLanguage:get('FFP_My_Booking_10','Passenger Name')" />
          </td>
					<td class="HeadCOL4">
            <xsl:value-of select="tikLanguage:get('FFP_My_Booking_11','First Segment')" />
          </td>
					<td class="HeadCOL5">&#160;</td>
				</tr>
			</tr>
			<xsl:if test="count(ClientBooking)!=0">
				<xsl:for-each select="ClientBooking">
					<tr>
						<td class="BodyCOL1">
							<xsl:value-of select="id"/>
						</td>
						<td class="BodyCOL2">
							<xsl:value-of select="record_locator"/>
						</td>
						<td class="BodyCOL3">
							<xsl:value-of select="passenger_name"/>
						</td>
						<td class="BodyCOL4">
							<xsl:value-of select="flight_segment"/>
						</td>
						<td class="BodyCOL5">
							<div class="ArrowGray">
								<xsl:attribute name="onclick">
									LoadBookingDetail('<xsl:value-of select="booking_id"/>','<xsl:value-of select="tikLanguage:get('FFP_My_Booking_12','No booking found. This transaction might be added manually. Please contact airline for more detail.')" />')
								</xsl:attribute>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(//ClientBooking)=0">
				<tr>
					<td colspan="5" class="BodyCOL5" style="text-align:center;color:red;">
            <xsl:value-of select="tikLanguage:get('FFP_My_Booking_13','Booking not found')" />
					</td>
				</tr>
			</xsl:if>
		</table>
	</xsl:template>
</xsl:stylesheet>