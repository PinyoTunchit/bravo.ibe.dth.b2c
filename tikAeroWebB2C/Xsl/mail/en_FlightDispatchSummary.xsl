
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="PageTitle">tikAERO FlightDispatch</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/PRI</xsl:variable>
	<xsl:variable name="ColorSpan">Red</xsl:variable>
	<xsl:key name="boarding_class_rcd_group" match="//Passengers/Passenger" use="boarding_class_rcd"/>

	<xsl:variable name="free_seating_flag" select="FlightDispatch/Passengers/Passenger/free_seating_flag"/>
	<xsl:variable name="seat_number" select="FlightDispatch/Passengers/Passenger/seat_number"/>

	<xsl:variable name="MaleWeight">87.5</xsl:variable>
	<xsl:variable name="FemaleWeight">71.6</xsl:variable>
	<xsl:variable name="ChildWeight">34.0</xsl:variable>
	<xsl:variable name="InfantWeight">13.6</xsl:variable>


	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($day, '/', $month, '/', $year)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:value-of select="concat($day, $month, $year)"/>
	</xsl:template>

	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$PageTitle"/>
				</TITLE>
				<STYLE>.PanelFlightDispatch .BookingNumber{
					color: #C4102F;
				}
				
				.PanelFlightDispatch.PanelTopHeader
				{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 24px;
					font-weight: bold;
					color: #00529d;
					padding: 5 0 3 5;
					margin: 0 0 5 0;
					width: 100%;
					cursor: hand;
				}

				
				.PanelItinerary.PanelHeader
				{
					font: bold 11pt Tahoma, Arial, sans-serif, verdana;
					color: #C4102F;
					/*background: url(../Images/0/H/hbh.jpg);*/
					margin: 5px 0px 5px 5px;
					width: 100%;
				}
				.PanelFlightDispatch.PanelHeader IMG{
					vertical-align: middle;
				}

				.PanelFlightDispatch.PanelHeader SPAN{
					padding-left: 4;
				}

				.PanelFlightDispatch.PanelContainer{
					padding: 10 0 10 20;
					width: 100%;
					display: block;
				}
				.PanelFlightDispatch .DetailHeader{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					color: #00529d;
					height: 20px
				}
				.PanelFlightDispatch .Details{
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					color: #666666;
					height: 20px;
					
				}
				.PanelFlightDispatch .GridHeader{
						background: #fdb813;
						color: #031668;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						padding: 2px 2px 2px 5px;
				}
				.PanelFlightDispatch TR.GridItems
					{
						border-bottom: 1px solid #fdb813;
						color: #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 9px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
						 
					}

				.PanelFlightDispatch .GridItems TD
				{
						border-bottom: 1px solid #fdb813;
						color: #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 9px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}

				.PanelFlightDispatch .GridFooter TD SPAN
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					height: 18px;
					padding: 4 5 0 4;
				}

				.PanelFlightDispatch .FooterTotal
				{
					border-bottom: solid 1px #dddddd;
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 9px;
					color: #00529d;
				}

				.PanelFlightDispatch SPAN.FooterTotalLabel
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-weight:bold;
					font-size: 9px;
					color: #00529d;
				}

				.PanelFlightDispatch SPAN.FooterTotalValue
				{
					
				}

				.CommentText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 9px;
					padding: 1 10 1 5;
				}
				.PanelFlightDispatch.PanelContainer{
					padding: 0 0 10 20;
					width: 100%;
					display: block;
				}
				TR.GridItems TD.ItemNumeric{
			 		cursor: pointer;
					font-weight: bold;
					padding: 1px 20px 1px 2px;
					text-align: right;
				}</STYLE>
			</HEAD>

			<BODY leftmargin="10px;" topmargin="5" rightmargin="5" bottommargin="5" marginwidth="5" marginheight="5">
				<DIV style="width:100%;">

					<DIV class="PanelFlightDispatch">

						<DIV class="PanelContainer">
							<DIV class="PanelTopHeader">
								<SPAN>Flight Dispatch Summary</SPAN>
							</DIV>
							<table class="Table" style="border-width:0px;width:650px;border-collapse:collapse;font-size: 10pt;">
								<tr>
									<td>

										<table class="Table" style="border-width:0px;width:650px;font-family: verdana, arial, helvetica, sans-serif;">
											<tr>
												<td style="color: #00529d;font-size: 15px;font-weight: bold;" bgcolor="#FFFFFF" width="15%">Date</td>
												<td style="color: #00529d;font-size: 15px;font-weight: bold;" bgcolor="#FFFFFF" width="15%">Route</td>
												<td style="color: #00529d;font-size: 15px;font-weight: bold;" bgcolor="#FFFFFF" width="15%">Flight</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table class="Table" cellspacing="0px" bgcolor="#fdb813" style="border-width:0px;width:650px;font-family: verdana, arial, helvetica, sans-serif;">
											<tr>
												<td width="15%" style="color: blue;font-size: 24px;font-weight: bold;BORDER-RIGHT: 1px solid #fdb813;BORDER-LEFT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;border-top:1px solid #fdb813;"
												    bgcolor="#FFFFFF">
													<xsl:value-of select="substring(//utc_departure_date_time,7,2)"/>/<xsl:value-of select="substring(//utc_departure_date_time,5,2)"/>/<xsl:value-of select="substring(//utc_departure_date_time,1,4)"/></td>
												<td width="15%" style="color: blue;font-size: 24px;font-weight: bold;BORDER-RIGHT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;border-top:1px solid #fdb813;" bgcolor="#FFFFFF">

													<xsl:value-of select="FlightDispatch/Flight/Flight/origin_rcd"/>&#xA0; -&#xA0;
													<xsl:value-of select="FlightDispatch/Flight/Flight/destination_rcd"/>
												</td>
												<td width="15%" style="color: red;font-size: 24px;font-weight: bold;BORDER-RIGHT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;border-top:1px solid #fdb813;" bgcolor="#FFFFFF">
													<xsl:value-of select="FlightDispatch/Flight/Flight/airline_rcd"/>&#xA0;&#xA0;
													<xsl:value-of select="FlightDispatch/Flight/Flight/flight_number"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</DIV>

						<DIV class="PanelContainer">
							<TABLE class="Table" width="100%" cellspacing="0" border="0">
								<TR align="left" class="GridItems">
									<TD width="15%">STD</TD>
									<TD class="ItemNumeric" width="10%">
										<xsl:choose>
											<xsl:when test="string(//planned_departure_time) &gt; '0'">
												<xsl:value-of select="substring(format-number(number(//planned_departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(//planned_departure_time), '0000'),3,4)"/></xsl:when>
											<xsl:otherwise>
												<span>&#xA0;</span>
											</xsl:otherwise>
										</xsl:choose>
									</TD>
									<TD width="18%">Aircraft Type</TD>
									<TD class="ItemNumeric" width="16%">
										<xsl:value-of select="FlightDispatch/Flight/Flight/aircraft_type_rcd"/>
									</TD>
									<TD width="18%">Registration</TD>
									<TD width="16%" class="ItemNumeric">
										<xsl:value-of select="FlightDispatch/Flight/Flight/matriculation_rcd"/>
									</TD>
								</TR>

								<TR class="GridItems">
									<TD width="15%">MTOW</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:choose>
											<xsl:when test="(//maximum_take_off_weight) &gt; '0'">
												<xsl:value-of select="format-number(FlightDispatch/Flight/Flight/maximum_take_off_weight,'#,##0.0')"/>
											</xsl:when>
											<xsl:otherwise>
												<span>&#xA0;</span>
											</xsl:otherwise>
										</xsl:choose>
									</TD>
									<TD width="18%">Passenger Count</TD>
									<xsl:choose>
										<xsl:when test="count(//Passenger[boarding_class_rcd=boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd = 'INF']) &gt; 0 ">

											<TD width="16%" class="ItemNumeric">
												<xsl:value-of select="count(FlightDispatch/Passengers/Passenger[passenger_type_rcd != 'INF'][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'])"/>+<xsl:value-of select="count(//Passenger[boarding_class_rcd=boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd = 'INF'])"/> INF</TD>
										</xsl:when>
										<xsl:otherwise>
											<TD width="7%" class="ItemNumeric">
												<xsl:value-of select="count(FlightDispatch/Passengers/Passenger[passenger_type_rcd != 'INF'][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'])"/>
											</TD>
										</xsl:otherwise>
									</xsl:choose>
									<TD width="24%">Passenger Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="pax_weight" select="sum(//Passenger[passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN']/pax_weight)"/>
										<xsl:if test="$pax_weight &gt; '0'">
											<xsl:value-of select="format-number($pax_weight,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$pax_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
								</TR>
								<TR class="GridItems">
									<TD width="15%">Empty Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="dry_operating_weight" select="sum(FlightDispatch/Flight/Flight/dry_operating_weight)"/>
										<xsl:if test="$dry_operating_weight &gt; '0'">
											<xsl:value-of select="format-number($dry_operating_weight,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$dry_operating_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
									<TD width="18%">Checked Baggage Count</TD>
									<TD width="16%" class="ItemNumeric">
										<xsl:variable name="number_of_pieces" select="sum(FlightDispatch/Passengers/Passenger/number_of_pieces)"/>
										<xsl:if test="$number_of_pieces &gt; '0'">
											<xsl:value-of select=" $number_of_pieces "/>
										</xsl:if>
										<xsl:if test="$number_of_pieces = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>

									<TD width="24%">Checked Baggage Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="check_in_baggage_weight" select="sum(FlightDispatch/Passengers/Passenger/check_in_baggage_weight)"/>
										<xsl:if test="$check_in_baggage_weight &gt; '0'">
											<xsl:value-of select="format-number($check_in_baggage_weight,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$check_in_baggage_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
								</TR>
								<TR class="GridItems">
									<TD width="15%">Fuel Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="fuel_load_weight" select="sum(FlightDispatch/Flight/Flight/fuel_load_weight)"/>
										<xsl:if test="$fuel_load_weight &gt; '0'">
											<xsl:value-of select="format-number($fuel_load_weight,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$fuel_load_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
									<TD width="18%">Hand Baggage Count</TD>
									<TD width="16%" class="ItemNumeric">

										<xsl:variable name="hand_number_of_pieces" select="sum(FlightDispatch/Passengers/Passenger/hand_number_of_pieces)"/>
										<xsl:if test="$hand_number_of_pieces &gt; '0'">
											<xsl:value-of select=" $hand_number_of_pieces"/>
										</xsl:if>
										<xsl:if test="$hand_number_of_pieces = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
									<TD width="24%">Hand Baggage Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="hand_baggage_weight" select="sum(FlightDispatch/Passengers/Passenger/hand_baggage_weight)"/>
										<xsl:if test="$hand_baggage_weight &gt; '0'">
											<xsl:value-of select=" format-number($hand_baggage_weight,'#,##0.0') "/>
										</xsl:if>
										<xsl:if test="$hand_baggage_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
								</TR>
								<TR class="GridItems">
									<TD width="15%">Crew Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="crew_weight" select="sum(FlightDispatch/Flight/Flight/crew_weight)"/>
										<xsl:if test="$crew_weight &gt; '0'">
											<xsl:value-of select="format-number($crew_weight,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$crew_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>

									<TD width="18%">&#xA0;</TD>
									<TD width="16%">&#xA0;</TD>
									<TD width="24%">Total Hold</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="total_hold" select="sum(FlightDispatch/Passengers/Passenger/check_in_baggage_weight) + sum(FlightDispatch/Flight/Flight/cargo_weight)+ sum(FlightDispatch/Flight/Flight/mail_weight)"/>
										<xsl:if test="$total_hold &gt; '0'">
											<xsl:value-of select="format-number($total_hold,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$total_hold = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
								</TR>

								<TR class="GridItems">
									<TD width="15%">Mail Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="mail_weight" select="sum(FlightDispatch/Flight/Flight/mail_weight)"/>
										<xsl:if test="$mail_weight &gt; '0'">
											<xsl:value-of select="format-number($mail_weight,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$mail_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
									<TD width="18%">Available Pay Load</TD>
									<TD width="16%" class="ItemNumeric">
										<xsl:value-of select="format-number((FlightDispatch/Flight/Flight/maximum_take_off_weight) - (sum(FlightDispatch/Flight/Flight/dry_operating_weight)  +sum(FlightDispatch/Flight/Flight/fuel_load_weight)+ sum(FlightDispatch/Flight/Flight/crew_weight) + sum(FlightDispatch/Flight/Flight/mail_weight) + sum(FlightDispatch/Flight/Flight/cargo_weight)  + sum(//Passenger[passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN']/pax_weight)+ sum(FlightDispatch/Passengers/Passenger/check_in_baggage_weight)  + sum(FlightDispatch/Passengers/Passenger/hand_baggage_weight)),'#,##0.0')"/>
									</TD>
									<TD width="24%">Total Pay Load</TD>
									<TD width="10%" class="ItemNumeric">

										<xsl:variable name="total_payload"
										              select="sum(FlightDispatch/Passengers/Passenger/pax_weight) + sum(FlightDispatch/Passengers/Passenger/check_in_baggage_weight) + sum(FlightDispatch/Passengers/Passenger/hand_baggage_weight) + sum(FlightDispatch/Flight/Flight/cargo_weight)+ sum(FlightDispatch/Flight/Flight/mail_weight)"/>
										<xsl:if test="$total_payload &gt; '0'">
											<xsl:value-of select="format-number($total_payload,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$total_payload = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
								</TR>
								<TR class="GridItems">
									<TD width="15%">Cargo Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="cargo_weight" select="sum(FlightDispatch/Flight/Flight/cargo_weight)"/>
										<xsl:if test="$cargo_weight &gt; '0'">
											<xsl:value-of select="format-number($cargo_weight,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$cargo_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
									<TD width="15%">&#xA0;</TD>
									<TD width="10%" class="ItemNumeric">&#xA0;</TD>
									<TD width="24%">Total Weight</TD>
									<TD width="10%" class="ItemNumeric">
										<xsl:variable name="total_weight"
										              select="sum(FlightDispatch/Flight/Flight/crew_weight) + sum(FlightDispatch/Flight/Flight/fuel_load_weight) + sum(FlightDispatch/Flight/Flight/dry_operating_weight)+sum(FlightDispatch/Passengers/Passenger/pax_weight) + sum(FlightDispatch/Passengers/Passenger/check_in_baggage_weight) + sum(FlightDispatch/Passengers/Passenger/hand_baggage_weight) + sum(FlightDispatch/Flight/Flight/cargo_weight)+ sum(FlightDispatch/Flight/Flight/mail_weight)"/>
										<xsl:if test="$total_weight &gt; '0'">
											<xsl:value-of select="format-number($total_weight,'#,##0.0')"/>
										</xsl:if>
										<xsl:if test="$total_weight = '0'">
											<span>&#xA0;</span>
										</xsl:if>
									</TD>
								</TR>
							</TABLE>
						</DIV>


						<BR></BR>
						<DIV class="PanelContainer">

							<TABLE class="Table" cellspacing="0" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
								<TR align="left" class="GridHeader">
									<TD width="34">Class</TD>
									<TD width="80">Passenger</TD>
									<TD width="37" align="right">Count</TD>
									<TD width="120" align="right">Total Weight</TD>
									<td align="left">&#xA0;</td>
									<td align="left">&#xA0;</td>
								</TR>
								<xsl:for-each select="FlightDispatch/Passengers/Passenger[count(. | key('boarding_class_rcd_group', boarding_class_rcd)[1]) = 1]">
									<xsl:variable name="boarding_class_rcd" select="boarding_class_rcd"/>
									<TR align="right" class="GridItems">
										<TD align="left">&#xA0;<xsl:value-of select="boarding_class_rcd"/></TD>
										<TD width="80" align="left">Adult Male</TD>
										<TD align="right" width="37">
											<xsl:variable name="countM_passenger_type_rcd"
											              select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][gender_type_rcd = 'M' or gender_type_rcd = ''][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR'])"/>
											<xsl:if test="$countM_passenger_type_rcd &gt;'0' ">
												<xsl:value-of select="$countM_passenger_type_rcd"/>&#xA0;&#xA0;</xsl:if>
										</TD>
										<TD width="120" align="right">
											<xsl:choose>
												<xsl:when test="sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][gender_type_rcd = 'M' or gender_type_rcd = ''][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR']/pax_weight) = '0'">
													<span>&#xA0;</span>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="format-number(sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][gender_type_rcd = 'M' or gender_type_rcd = ''][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN' ][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR']/pax_weight),'#,##0.0')"/>&#xA0;&#xA0;</xsl:otherwise>
											</xsl:choose>
										</TD>
										<td align="left">&#xA0;</td>
										<td align="left">&#xA0;</td>
									</TR>
									<TR align="right" class="GridItems">
										<TD align="left">&#xA0;<xsl:value-of select="boarding_class_rcd"/></TD>
										<TD width="80" align="left">Adult Female</TD>
										<TD align="right" width="37">
											<xsl:variable name="countF_passenger_type_rcd"
											              select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][gender_type_rcd = 'F'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR'])"/>
											<xsl:if test="$countF_passenger_type_rcd &gt;'0' ">
												<xsl:value-of select="$countF_passenger_type_rcd"/>&#xA0;&#xA0;</xsl:if>
										</TD>
										<TD width="120">
											<xsl:choose>
												<xsl:when test="sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][gender_type_rcd = 'F'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR']/pax_weight) = '0'">
													<span>&#xA0;</span>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="format-number(sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][gender_type_rcd = 'F'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR']/pax_weight),'#,##0.0')"/>&#xA0;&#xA0;</xsl:otherwise>
											</xsl:choose>
										</TD>
										<td align="left">&#xA0;</td>
										<td align="left">&#xA0;</td>
									</TR>
									<TR align="right" class="GridItems">
										<TD align="left">&#xA0;<xsl:value-of select="boarding_class_rcd"/></TD>
										<TD width="80" align="left">Children</TD>
										<TD align="right" width="37">
											<xsl:variable name="countC_passenger_type_rcd"
											              select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd ='CHD' or passenger_type_rcd = 'UMNR'])"/>
											<xsl:if test="$countC_passenger_type_rcd &gt;'0' ">
												<xsl:value-of select="$countC_passenger_type_rcd"/>&#xA0;&#xA0;</xsl:if>
										</TD>
										<TD align="right" width="120">
											<xsl:choose>
												<xsl:when test="sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd = 'CHD']/pax_weight) = '0'">
													<span>&#xA0;</span>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="format-number(sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd = 'CHD']/pax_weight),'#,##0.0')"/>&#xA0;&#xA0;</xsl:otherwise>
											</xsl:choose>
										</TD>
										<td align="left">&#xA0;</td>
										<td align="left">&#xA0;</td>
									</TR>
									<TR align="right" class="GridItems">
										<xsl:variable name="count_passenger_type_rcd"
										              select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd = 'INF'])"/>
										<TD align="left">&#xA0;<xsl:value-of select="boarding_class_rcd"/></TD>
										<TD width="80" align="left">Infants</TD>

										<TD width="37">
											<xsl:variable name="countU_passenger_type_rcd"
											              select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd = 'INF'])"/>

											<xsl:if test="$countU_passenger_type_rcd &gt; '0' ">
												<xsl:value-of select="$countU_passenger_type_rcd"/>&#xA0;&#xA0;</xsl:if>
										</TD>
										<TD width="120">
											<xsl:choose>
												<xsl:when test="sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd = 'INF']/pax_weight) = '0'">
													<span>&#xA0;</span>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="format-number(sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED' or passenger_check_in_status_rcd = 'FLOWN'][passenger_type_rcd = 'INF']/pax_weight),'#,##0.0')"/>&#xA0;&#xA0;</xsl:otherwise>
											</xsl:choose>
										</TD>
										<td align="left">&#xA0;</td>
										<td align="left">&#xA0;</td>
									</TR>
								</xsl:for-each>
							</TABLE>
						</DIV>
						<p/>
						<p/>
						<DIV class="PanelContainer">
							<table class="Table" cellspacing="0px" bgcolor="#fdb813" style="border-width:0px;width:100%;">
								<tr class="GridHeader">
									<td align="left" width="50%" style="BORDER-RIGHT: 1px solid #fdb813;BORDER-LEFT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;border-top:1px solid #fdb813;">Fwd Hold</td>
									<td align="left" width="50%" style="BORDER-RIGHT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;border-top:1px solid #fdb813;">Rear Hold</td>
								</tr>
								<tr style="height:50px;">
									<td width="50%" align="left" bgcolor="#FFFFFF" style="BORDER-RIGHT: 1px solid #fdb813;BORDER-LEFT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;">&#xA0;</td>
									<td width="50%" align="left" bgcolor="#FFFFFF" style="BORDER-RIGHT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;">&#xA0;</td>
								</tr>
							</table>
						</DIV>
						<DIV class="PanelContainer">
							<table class="Table" cellspacing="0px" bgcolor="#fdb813" style="border-width:0px;width:100%;">

								<tr class="GridHeader">
									<td align="center" width="100%" style="BORDER-RIGHT: 1px solid #fdb813;BORDER-LEFT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;border-top:1px solid #fdb813;">Freight Distribution</td>
								</tr>
								<tr style="height:50px;">
									<td width="100%" align="left" bgcolor="#FFFFFF" style="BORDER-RIGHT: 1px solid #fdb813;BORDER-LEFT: 1px solid #fdb813;border-bottom: 1px solid #fdb813;">&#xA0;</td>
								</tr>
							</table>
						</DIV>
					</DIV>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylesheet edited using Stylus Studio - (c) 2004-2006. Progress Software Corporation. All rights reserved. --><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="no" externalpreview="no" url="file:///c:/Development/XSL/_CASE/a/FlightDispatch__May25_0951.XML" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0"
		          profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no"
		          validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->