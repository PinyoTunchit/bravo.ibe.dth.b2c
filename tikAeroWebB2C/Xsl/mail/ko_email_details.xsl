<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://xsltsl.org/xsl/documentation/1.0" xmlns:dt="http://xsltsl.org/date-time" xmlns:str="http://xsltsl.org/string" xmlns:CharCode="http://www.tikaero.com/Printing">
	<xsl:key name="flight_id_group" match="//Booking/Tickets/Ticket" use="flight_id"/>
	<xsl:key name="booking_segment_id_group" match="//Booking/TicketQuotes/Flight" use="booking_segment_id"/>
	<xsl:key name="flight_group" match="/Booking/Itinerary/FlightSegment" use="concat(od_origin_rcd, '|', od_destination_rcd)"/>
	<xsl:key name="booking_id_group" match="//Booking/Itinerary/FlightSegment" use="booking_id"/>
	<xsl:key name="flight_number_group" match="//Booking/Itinerary/FlightSegment" use="flight_number"/>
	<xsl:key name="flight_connection_id_group" match="//Booking/Itinerary/FlightSegment" use="flight_connection_id"/>
	<xsl:variable name="booking_source_rcd" select="//Booking/Header/BookingHeader/booking_source_rcd"/>
	<xsl:variable name="client_profile_id" select="//Booking/Header/BookingHeader/client_profile_id"/>
	<xsl:variable name="language_rcd" select="//Booking/Header/BookingHeader/language_rcd"/>
	<xsl:variable name="od_destination_rcd" select="//Booking/Itinerary/FlightSegment/od_destination_rcd"/>
	<xsl:variable name="booking_source">
		<xsl:choose>
			<xsl:when test="$booking_source_rcd = 'INT' ">INT</xsl:when>
			<xsl:when test="$booking_source_rcd = 'B2C' ">B2C</xsl:when>
			<xsl:when test="$booking_source_rcd = 'B2B' ">B2B</xsl:when>
			<xsl:when test="$booking_source_rcd = 'B2B' and  $client_profile_id != '' ">B2E</xsl:when>
			<xsl:when test="$booking_source_rcd = 'int'">INT</xsl:when>
			<xsl:when test="$booking_source_rcd = 'b2c'">B2C</xsl:when>
			<xsl:when test="$booking_source_rcd = 'b2b' ">B2B</xsl:when>
			<xsl:when test="$booking_source_rcd = 'b2b' and  $client_profile_id != '' ">B2E</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="language_check">
		<xsl:choose>
			<xsl:when test="$language_rcd = 'EN' ">ENUS</xsl:when>
			<xsl:when test="$language_rcd = 'JA' ">JAJP</xsl:when>
			<xsl:when test="$language_rcd = 'KO' ">KOKR</xsl:when>
			<xsl:when test="$language_rcd = 'TW'  ">ZHTW</xsl:when>
			<xsl:when test="$language_rcd = 'HK'  ">ZHHK</xsl:when>
			<xsl:when test="$language_rcd = 'CN' ">ZHCN</xsl:when>
			<xsl:when test="$language_rcd = 'en' ">ENUS</xsl:when>
			<xsl:when test="$language_rcd = 'ja' ">JAJP</xsl:when>
			<xsl:when test="$language_rcd ='ko' ">KOKR</xsl:when>
			<xsl:when test="$language_rcd = 'tw' ">ZHTW</xsl:when>
			<xsl:when test="$language_rcd = 'hk' ">ZHHK</xsl:when>
			<xsl:when test="$language_rcd = 'cn' ">ZHCN</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="RowsPerPage">32</xsl:variable>




	<xsl:template name="styleColor">
		<!--Do not  use "Import" or "Include" for StyleSheet (CSS) because it error on EXE, but Web work fine. -->
		<STYLE TYPE="text/css">/*DO NOT DELETE COMMENT BELOW*/
	  /*Modify Date: 18MAR2011*/
	  /**********************************************/
	  body 
	  {
	  color: 
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					8.5pt;
	  }
	  
      table
      {
      border-spacing: 		0px;
      empty-cells: 				show;
      margin: 						0px;
      padding: 					0px;
      }
      
      td								
      {
      vertical-align: 			top;
      }

      th								
      {
      border: 						.09mm;
      border-left: 				0px;
      border-right: 				0px;
      color: 						#e5e5e5;
      text-align: 					center;
      }

      .blueline					
      {
      border: 						.09mm;
      border-bottom: 			solid #e5e5e5 1px;
      }

      .documentheader	
      { 
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					8.5pt;
	  font-weight: 				bold;	
	  height: 						6mm;
	  vertical-align: 			middle;
	  color: 						#e5e5e5; 
      }

      .documenttotal		
      {
      border-bottom: 			solid #e5e5e5 1px;
      color: 						#000000;
      font-weight: 				bold;
      }

      .imgfooter 				
      {
      border-left-color: 		#FFFFFF;
      border-left-style: 		solid;
      border-top-style: 		solid;
      border-width: 			0 0 0 15px;
      left: 							0px;
      }

      .imglogo					
      {
      border-color: 				White;
      border-style: 				none;
      vertical-align: 			top;
      }

      .pagebreak 
      { 
      page-break-after: 		always; 
      }

      .tabledetails				
      {
      border-left: 				solid #ffffff 0px;
      border-right: 				solid #ffffff 0px;
      color: 						#000000;
      width: 						759px;
	  height: 						6mm;
      }

      .tablereportfooter		
      {
      border-bottom: 			solid #e5e5e5 0px;
      border-left: 				solid #e5e5e5 0px;
      border-right: 				solid #e5e5e5 0px;
	  margin: 						0px;
      }

      .tablereportheader	
      {
      border-left: 				solid #ffffff 0px;
      border-right: 				solid #ffffff 0px;
      border-top: 				solid #ffffff 0px;
      width: 						759px;
      }

      .tdheader					
      {
      border-left: 				0px;
      border-right: 				0px;
      border-style: 				none;
      color: 						#031668;
      font-weight: 				bold;
      text-align: 					left;
      }

      .tdmargin					
      { 
      width: 						20px; 
      }

      .tdorderheader			
      {
      border: 						solid 1px #e5e5e5;
      border-left: 				0px;
      border-right: 				0px;
      }

      .tdtotalmargin			
      { width: 						450px; 
      }

      .grid-main 
      {
	  
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					9pt;
	  height: 						4.25mm;
	  color: 						#000000;
	 
     
	 
   
      }

      .grid-top
      {
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					8.5pt;
 	  color: 						#000000;
	  vertical-align: 			middle;
	   border-right: 				.0pt solid;
       background-color: 	white;
	 
     
      }

      .grid-even
      {
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					8.5pt;
	  color: 						#000000;
	  height: 						6mm;
	  vertical-align: 			middle;
      border-left-style: 		none;
      border-right: 				.0pt solid;
      border-top: 				.0pt solid;
      border-bottom-style: none;
      background-color: 	white;
        
      }

	 p.msonormal	
	 { 
	 color: 							#000000; 
	 font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	 font-size: 					9pt; 
	 margin-bottom: 			.0001pt; 
	 margin-left: 				0in; 
	 margin-right: 				0in; 
	 margin-top: 				0in; 
	 mso-style-parent: 		""; 
	 text-align: 					justify; 
	 }
	  p.msonormal1	
	 { 
	 color: 							#000000; 
	 font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	 font-size: 					4pt; 
	 margin-bottom: 			.0001pt; 
	 margin-left: 				0in; 
	 margin-right: 				0in; 
	 margin-top: 				0in; 
	 mso-style-parent: 		""; 
	 text-align: 					justify; 
	 } 
	 h1 
	 {
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					14pt; 
	 color: 						#ffffff; 
	 margin: 						0px; 
	 line-height:					6mm;
	  width: 						673px; 
	  margin: 						0px 0px 0px 36px;
	 background-color: 			#e5e5e5;

	 }
	 
	 h2 
	 {
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					8.5pt; 
	 line-height:					5mm;
	 }
	 
	 h3 
	 {
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					8.5pt;
	 }
	 
	 h4 
	 {
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					8.5pt;
	 }

	h1+p 
	{
  	margin-top: 					0;
	height: 							6mm;
	}</STYLE>
	</xsl:template>


	<!--HeaderSection-->
	<xsl:template match="Booking/Header">
		<!-- variable Header-->
		<xsl:variable name="BarURL">http://www.avantikjp.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
		<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>

		<br/>
		<table border="0" style=" width:673px; margin: 0px 0px 0px 36px;">
			<tr>
				<td>
					<table border="0" style=" width:300px;">
						<tr>
							<td class="documentheader" style="color: #000000; width: 300px;font-size:8pt;">
								<img alt="PEACH LOGO" src="http://www.avantikjp.com/XSLImages/jad/JAD_Logo.jpg" width="205"/>
								<br/>
								<xsl:choose>
									<xsl:when test="/Booking/Header/BookingHeader[own_agency_flag =1]">flypeach.com 을 이용해 주셔서 감사 드립니다.
								예약하신 내용과 규정을 확인하여 주십시오</xsl:when>
									<xsl:otherwise>이용 해 주셔서 고맙습니다.
										<br/>예약하신 내용과 규정을 확인하여 주십시오</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table border="0" style="width:365px;color: #000000;border-right:solid #5f497a 2px;border-left:solid #5f497a 2px;border-top:solid #5f497a 2px;border-bottom:solid #5f497a 2px;">
						<tr>
							<td align="left" class="documentheader" style="color: #000000;font-size:11pt;font-weight: bold;">탑승수속용  <p/>바코드</td>
							<td>
								<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="150"/>
							</td>
						</tr>
						<tr>
							<td>&#xA0;</td>
							<td class="documentheader" style="color: #000000;font-size:8pt;">바코드를 리더기에 읽혀주십시오.</td>
						</tr>
						<tr>
							<td class="documentheader" style="color: #000000;font-size:11pt;font-weight: bold;">예약번호</td>
							<td class="documentheader" align="left" style="color: #b634bb;font-size:26pt;">
								<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br/>


		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:665px;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;">
			<xsl:choose>
				<xsl:when test="/Booking/Header/BookingHeader[own_agency_flag =1]">

					<tr>
						<td>
							<table cellspacing="0" style="color:#000000;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;color: white; background-color: #b634bb;font-size:10pt;width: 300px;">

								<tr style="color:#ffffff;">
									<td>예약 상세 내용</td>
								</tr>
							</table>
							<table cellspacing="0" style=" border-style: none;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; table-layout:fixed;width: 300px;">
								<tr>
									<td>
										<table cellspacing="0" style="width: 300px;">
											<tr>
												<td>
													<table cellspacing="0" style="width: 300px;">
														<tr>
															<td class="documentheader" style="color: #000000;width: 100px;font-size:10pt;">신청자: &#xA0;</td>
															<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">
																<xsl:value-of select="/Booking/Header/BookingHeader/contact_name"/>
															</td>
														</tr>
														<tr>
															<tr>
																<td class="documentheader" style="color: #000000;width: 100px;font-size:10pt;">전화번호:</td>
																<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">
																	<xsl:value-of select="/Booking/Header/BookingHeader/phone_home"/>
																</td>
															</tr>
															<tr>
																<td class="documentheader" style="color: #000000;width: 100px;font-size:10pt;">휴대폰번호:</td>
																<td class="documentheader" style="color: #000000;width:200px;font-size:10pt;">
																	<xsl:value-of select="/Booking/Header/BookingHeader/phone_mobile"/>
																</td>
															</tr>
															<tr>
																<td class="documentheader" style="color: #000000;width: 100px;font-size:10pt;">E-mail:</td>
																<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">
																	<xsl:value-of select="/Booking/Header/BookingHeader/contact_email"/>
																</td>
															</tr>
															<td class="documentheader" style="color: #000000;width:100px;font-size:10pt;">예약일:</td>
															<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">
																<xsl:call-template name="dt:formatdate">
																	<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
																</xsl:call-template>&#xA0;</td>
														</tr>
														<tr>
															<td class="documentheader" style="color: #000000;width:100px;font-size:10pt;">최종변경일：</td>
															<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">
																<xsl:call-template name="dt:formatdate">
																	<xsl:with-param name="date" select="/Booking/Header/BookingHeader/update_date_time"/>
																</xsl:call-template>&#xA0;</td>
														</tr>
													</table>
												</td>
												<!--<td align="right" width="300px">
										</td>-->
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table cellspacing="0" style="color:#000000;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;color: white; background-color: #b634bb;font-size:10pt;width: 350px;">

								<tr style="color:#ffffff;">
									<td>Peach 최상의</td>
								</tr>
							</table>


							<xsl:choose>
								<xsl:when test="/Booking/Itinerary/FlightSegment[domestic_flag = 1]">
									<table border="0" cellspacing="0" style=" border-style: none; table-layout:fixed;width: 350px;">
										<tr>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/domestic_hotel/redirect.html">
													<img alt="호텔" src="http://www.flypeach.com/itinerary/pc/icon/01.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href=" http://www.flypeach.com/jp/ja-jp/rentcar.aspx?pc_Itinerary">
													<img alt="렌터카" src="http://www.flypeach.com/itinerary/pc/icon/02.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/insulance/redirect.html">
													<img alt="보험" src="http://www.flypeach.com/itinerary/pc/icon/03.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">호텔</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">렌터카</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">보험</td>
										</tr>
										<tr>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/baggage/redirect.html?kr">
													<img alt="추가 수하물" src="http://www.flypeach.com/itinerary/pc/icon/04.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/seat/redirect.html?kr">
													<img alt="좌석 지정" src="http://www.flypeach.com/itinerary/pc/icon/05.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/change/redirect.html?kr">
													<img alt="예약 변경" src="http://www.flypeach.com/itinerary/pc/icon/06.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">추가 수하물</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">좌석 지정</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">예약 변경</td>
										</tr>
									</table>
								</xsl:when>
								<xsl:otherwise>
									<table border="0" cellspacing="0" style=" border-style: none; table-layout:fixed;">
										<tr>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/international_hotel/redirect.html">
													<img alt="호텔" src="http://www.flypeach.com/itinerary/pc/icon/01.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.rentalcars.com/affxml/Home.do?affiliateCode=peach&amp;adplat=itinerary&amp;adcamp=itinerary">
													<img alt="렌터카" src="http://www.flypeach.com/itinerary/pc/icon/02.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/fuchsia/redirect.html">
													<img alt="면세점" src="http://www.flypeach.com/itinerary/pc/icon/03.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">호텔</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">렌터카</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">면세점</td>
										</tr>
										<tr>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/baggage/redirect.html?kr">
													<img alt="추가 수하물" src="http://www.flypeach.com/itinerary/pc/icon/04.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/seat/redirect.html?kr">
													<img alt="좌석 지정" src="http://www.flypeach.com/itinerary/pc/icon/05.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/change/redirect.html?kr">
													<img alt="예약 변경" src="http://www.flypeach.com/itinerary/pc/icon/06.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">추가 수하물</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">좌석 지정</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">예약 변경</td>
										</tr>
									</table>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<td>
							<table border="0" cellspacing="0"
							       style="table-layout:fixed; color:#000000;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;color: white; background-color: #b634bb;font-size:10pt;width: 665px;">
								<tr>
									<td style="color:#ffffff;">예약 상세 내용</td>
								</tr>
							</table>
							<table border="0" cellspacing="0" style="table-layout:fixed; font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;font-size:10pt;width: 665px;">
								<tr>
									<td style="width: 335px;">여행대리점 ： &#xA0;<xsl:value-of select="/Booking/Header/BookingHeader/agency_name"/></td>

									<td style="width: 338px;">예약일 :  &#xA0;<xsl:call-template name="dt:formatdate">
											<xsl:with-param name="date" select="/Booking/Header/BookingHeader/update_date_time"/></xsl:call-template></td>
								</tr>
							</table>
						</td>
					</tr>
				</xsl:otherwise>
			</xsl:choose>
		</table>
		<br/>
		<br/>
		<!--</div>-->
	</xsl:template>
	<!--End HeaderSection-->

	<xsl:variable name="FlightRowsHeader">
		<table cellspacing="0" class="grid-main" style="width: 665px;">
			<tr>

				<td style="color: #000000; background-color: #de81d3; width: 65px" align="left">편명</td>
				<td style="color: #000000; background-color: #de81d3; width: 200px" align="left">출발</td>
				<td style="color: #000000; background-color: #de81d3; width: 200px" align="left">도착</td>
				<td style="color: #000000; background-color: #de81d3; width: 100px" align="left">운임 종류</td>
				<td style="color: #000000; background-color: #de81d3; width: 100px" align="left">상태</td>

				<!--<th>Class</th>-->
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Itinerary">

		<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table cellspacing="0" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">

						<tr>
							<td>여정 상세 내용</td>
						</tr>
					</table>
					<!--<xsl:copy-of select="$FlightRowsHeader"/>-->
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:for-each select="/Booking/Itinerary/FlightSegment[count(. | key('flight_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">

						<xsl:variable name="od_origin_rcd" select="od_origin_rcd"/>
						<xsl:variable name="od_destination_rcd" select="od_destination_rcd"/>
						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="color: #000000; background-color: #ffffff;margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;font-size:10pt;font-weight:bold;">
							<tr>
								<td>

									<xsl:choose>
										<xsl:when test="position() = 1">

											<span>가는 편</span>
											<xsl:copy-of select="$FlightRowsHeader"/>
										</xsl:when>

										<xsl:otherwise>

											<span>오는 편</span>
											<xsl:copy-of select="$FlightRowsHeader"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
						</table>
						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;font-size:10pt;">

							<xsl:for-each select="/Booking/Itinerary/FlightSegment[od_origin_rcd = $od_origin_rcd][od_destination_rcd =$od_destination_rcd]">
								<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
								<xsl:variable name="passenger_id" select="passenger_id"/>

								<xsl:value-of select="CharCode:increment()"/>

								<xsl:if test="segment_status_rcd !='US'">
									<tr>

										<td bgcolor="#FFFFFF" style="width:65px;">
											<SPAN>

												<xsl:value-of select="airline_rcd"/>
												<xsl:value-of select="flight_number"/>
												<xsl:if test="string-length(departure_date) = '0'">
													<xsl:text>OPEN</xsl:text>
												</xsl:if>
											</SPAN>
										</td>
										<td bgcolor="#FFFFFF" style="width:200px;">

											<xsl:value-of select="origin_name"/>&#xA0;(<xsl:value-of select="origin_rcd"/>)

											<p class="msonormal"/>
											<xsl:if test="string-length(departure_date) &gt; '0'">
												<xsl:call-template name="dt:formatdate">
													<xsl:with-param name="date" select="departure_date"/>
												</xsl:call-template>&#xA0;
												<xsl:call-template name="get_day_name">
													<xsl:with-param name="utc" select="departure_date"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="string-length(departure_date) = '0'">
												<xsl:text>OPEN</xsl:text>
											</xsl:if>&#xA0;
											<xsl:if test="string(departure_time) != '0'">
												<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
											<p class="msonormal"/>  터미널: &#xA0;&#xA0;<xsl:value-of select="origin_terminal"/><p/></td>

										<td bgcolor="#FFFFFF" style="width:200px;">
											<xsl:value-of select="destination_name"/>&#xA0;(<xsl:value-of select="destination_rcd"/>)
											<p class="msonormal"/>
											<xsl:if test="string-length(arrival_date) &gt; '0'">
												<xsl:call-template name="dt:formatdate">
													<xsl:with-param name="date" select="arrival_date"/>
												</xsl:call-template>&#xA0;
												<xsl:call-template name="get_day_name">
													<xsl:with-param name="utc" select="arrival_date"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="string-length(arrival_date) = '0'">
												<xsl:text>OPEN</xsl:text>
											</xsl:if>&#xA0;<xsl:if test="string(arrival_time) != '0'">
												<xsl:value-of select="substring(format-number(number(arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(arrival_time), '0000'),3,4)"/></xsl:if>
											<p class="msonormal"/>  터미널: &#xA0;&#xA0;<xsl:value-of select="destination_terminal"/><p/></td>

										<td bgcolor="#FFFFFF" align="left" style="width:100px;">
											<xsl:for-each select="//Tickets/Ticket[booking_segment_id=$booking_segment_id]">
												<xsl:if test="position()=last()">
													<xsl:value-of select="fare_code"/>
												</xsl:if>
											</xsl:for-each>
										</td>
										<td bgcolor="#FFFFFF" style="width:100px;">

											<xsl:value-of select="status_name"/>
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>

							<!--<xsl:for-each select="/Booking/Itinerary/FlightSegment">-->
						</table>
					</xsl:for-each>
				</td>
			</tr>
		</table>
		<br/>
	</xsl:template>
	<!--End FlightsSection-->

	<!--TicketSection-->
	<xsl:variable name="TicketRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
			<tr bgcolor="#de81d3">

				<td align="left" style="color: #000000;width: 50px;">편명</td>
				<td align="left" style="color: #000000;width: 180px">이름</td>
				<td align="left" style="color: #000000;width: 50px;">성별</td>
				<td align="left" style="color: #000000;width: 120px">항공권 번호</td>
				<td align="left" style="color: #000000;width: 50px">구분</td>
				<td align="left" style="color: #000000;width: 50px;">좌석</td>
				<td align="left" style="color: #000000;width: 80px;">수하물</td>
				<td align="left" style="color: #000000;width: 85px;">상태</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Tickets">
		<!--Ticket-->
		<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table cellspacing="0" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


						<tr>
							<td>탑승자 정보</td>
						</tr>
					</table>
					<!--<h1>TICKETS</h1>-->
					<xsl:copy-of select="$TicketRowsHeader"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:value-of select="CharCode:increment()"/>

					<xsl:for-each select="/Booking/Tickets/Ticket">
						<xsl:sort select="departure_date" order="ascending"/>


						<xsl:variable name="booking_id" select="booking_id"/>
						<xsl:variable name="booking_segment_id_tax" select="booking_segment_id"/>

						<xsl:variable name="booking_segment_id" select="//Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
						<xsl:variable name="passenger_id" select="passenger_id"/>


						<!-- <xsl:variable name="booking_segment_id" select="booking_segment_id"/> -->

						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
							<xsl:value-of select="CharCode:increment()"/>
							<tr bgcolor="#FFFFFF">
								<td align="left" style="width: 50px;">



									<xsl:value-of select="airline_rcd"/>

									<xsl:value-of select="flight_number"/>
									<xsl:if test="string-length(departure_date) = '0'">
										<xsl:text>OPEN</xsl:text>
									</xsl:if>
								</td>
								<td align="left" style="width: 180px;">
									<xsl:choose>
										<xsl:when test="not(string-length(lastname) &gt; '0')">
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="lastname"/>
											<xsl:text> / </xsl:text>
											<xsl:value-of select="firstname"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
								<td align="left" style="width: 50px;">
									<xsl:value-of select="title"/>
								</td>

								<td align="left" style="width: 120px;">
									<xsl:value-of select="ticket_number"/>
								</td>
								<td style="width:50px;" align="left" valign="top">
									<xsl:value-of select="passenger_type_rcd"/>
								</td>
								<td align="left" style="width: 50px;">
									<xsl:value-of select="seat_number"/>
								</td>

								<td align="left" style="width: 80px;">

									<xsl:value-of select="(sum(//Fees/Fee[passenger_id=$passenger_id][booking_segment_id=$booking_segment_id_tax][void_date_time=''][fee_category_rcd = 'BAGSALES']/number_of_units)+ sum(//Tickets/Ticket[passenger_id=$passenger_id][booking_segment_id=$booking_segment_id_tax][void_date_time='']/piece_allowance))"/>
								</td>
								<td align="left" style="width: 85px;">

									<xsl:for-each select="//Itinerary/FlightSegment[booking_segment_id=$booking_segment_id_tax]">

										<xsl:value-of select="status_name"/>
									</xsl:for-each>
								</td>
							</tr>
						</table>
					</xsl:for-each>
				</td>
			</tr>
		</table>
		<br/>
	</xsl:template>
	<!--End TicketSection-->

	<!--Auxiliaries-->
	<xsl:variable name="RemarkRowsHeader">
		<table cellspacing="0" class="grid-main" style="width: 665px;">
			<tr>
				<td style="color: #000000; background-color: #de81d3;width: 265px;" align="left">Code</td>
				<td style="color: #000000; background-color: #de81d3;width: 400px;" align="left">Description</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Remarks">

		<xsl:if test="count(/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX'])&gt;0">
			<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
				<tr>
					<td>
						<table style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


							<tr>
								<td>Auxiliaries</td>
							</tr>
						</table>
						<!--<h1>AUXILIARIES</h1>-->
						<xsl:copy-of select="$RemarkRowsHeader"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:for-each select="/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
							<xsl:value-of select="CharCode:increment()"/>
							<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">
								<tr bgcolor="#FFFFFF">
									<td align="left" style="width: 265px;">
										<xsl:value-of select="display_name"/>
									</td>
									<td align="left" style="width: 400px;">
										<xsl:value-of select="remark_text"/>
									</td>
								</tr>
							</table>
						</xsl:for-each>
					</td>
				</tr>
			</table>
			<br/>
		</xsl:if>
	</xsl:template>
	<!--End Auxiliaries-->

	<!--Special Services-->
	<xsl:variable name="ServicesRowsHeader">
		<table cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;table-layout:fixed;  width: 665px;">
			<tr>
				<td style="color: #000000; background-color: #de81d3;width: 65px" align="left">편명</td>
				<td style="color: #000000; background-color: #de81d3;width: 300px" align="left">이름</td>
				<td style="color: #000000; background-color: #de81d3;width: 300px" align="left">서비스 내용</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/SpecialServices">
		<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


						<tr>
							<td>도움이 필요한 서비스</td>
						</tr>
					</table>
					<!--<h1>SPECIAL SERVICES</h1>-->
					<xsl:copy-of select="$ServicesRowsHeader"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')]">
						<xsl:sort select="departure_date" order="ascending"/>
						<!--<xsl:if test="position()=1">-->
						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">
							<xsl:value-of select="CharCode:increment()"/>

							<tr bgcolor="#FFFFFF">
								<td style="width: 65px; border-right: .0pt;">
									<xsl:value-of select="airline_rcd"/>&#xA0;<xsl:value-of select="flight_number"/></td>
								<td style=" border-right: .0pt;width: 300px">
									<xsl:value-of select="lastname"/>/&#xA0;<xsl:value-of select="firstname"/></td>

								<td style=" border-right: .0pt;width: 300px">
									<xsl:value-of select="special_service_rcd"/>
									<xsl:text>&#xA0;</xsl:text>
									<xsl:value-of select="display_name"/>
								</td>
							</tr>
						</table>
						<!--<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
							<xsl:copy-of select="$ReportFooter"/>
							<div style="page-break-after:always">
								<br/>
							</div>
							<xsl:copy-of select="$ReportHeader"/>
							<xsl:copy-of select="$ServicesRowsHeader"/>
						</xsl:if>-->
						<!--</xsl:if>-->
					</xsl:for-each>
					<!--Filler -->
					<xsl:choose>
						<!-- case of only one page-->
						<xsl:when test="CharCode:getCounter() &lt;= $RowsPerPage">
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
		</table>
		<br/>
		<!--End Filler -->
	</xsl:template>
	<!--End Special Services-->

	<!--Passport Information-->

	<xsl:variable name="PassengersRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
			<tr>
				<td align="left" style="width:165px; color: #000000; background-color: #de81d3;">영문 이름</td>
				<td align="left" style="color: #000000; background-color: #de81d3;width: 150px;">국적</td>
				<td align="left" style="width: 100px; color: #000000; background-color: #de81d3;">여권번호</td>
				<td align="left" style="width: 150px;color: #000000; background-color: #de81d3;">발행국</td>
				<td align="left" style="width: 100px; color: #000000; background-color: #de81d3;">유효기간</td>
			</tr>
		</table>
	</xsl:variable>

	<xsl:template match="Booking/Passengers">


		<xsl:if test="count(/Booking/Itinerary/FlightSegment[domestic_flag != 1])">

			<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
				<tr>
					<td>
						<table style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


							<tr>
								<td>여권정보</td>
							</tr>
						</table>
						<!--<h1>SPECIAL SERVICES</h1>-->
						<xsl:copy-of select="$PassengersRowsHeader"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:value-of select="CharCode:increment()"/>
						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">
							<xsl:for-each select="//Passengers/Passenger[passport_number !='']">
								<xsl:variable name="passenger_type" select="passenger_type_rcd"/>

								<tr bgcolor="#FFFFFF">
									<td align="left" style="width:165px;">
										<xsl:value-of select="lastname"/>&#xA0;<xsl:value-of select="firstname"/></td>
									<td align="left" style="width: 150px;">
										<xsl:value-of select="nationality_display_name"/>
									</td>
									<td align="left" style="width: 100px;">
										<xsl:if test="document_type_rcd = 'P'">
											<xsl:value-of select="passport_number"/>
										</xsl:if>
									</td>

									<td align="left" style="width: 150px;">
										<xsl:value-of select="passport_issue_country_display_name"/>
									</td>

									<td align="left" style="width: 100px;">
										<xsl:call-template name="dt:formatdate">
											<xsl:with-param name="date" select="passport_expiry_date"/>
										</xsl:call-template>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</td>
				</tr>
			</table>
		</xsl:if>
		<br/>
	</xsl:template>

	<!--End Passport Information-->
	<!--QuoteSection-->
	<xsl:variable name="QuoteRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
			<tr>
				<td align="left" style="width: 465px; color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">항목</td>
				<td align="right" style="width: 200px;color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">합계</td>
			</tr>
		</table>
	</xsl:variable>

	<xsl:key name="passenger_type_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<xsl:key name="charge_type_group" match="//TicketQuotes/Total" use="charge_type"/>
	<xsl:template match="Booking/TicketQuotes">

		<xsl:if test="position()=1">
			<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
				<tr>
					<td>
						<table cellspacing="1px" bgcolor="#e5e5e5" style="font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


							<tr>
								<td>지불 내역</td>
							</tr>
						</table>
						<!--<h1>QUOTES</h1>-->
						<xsl:copy-of select="$QuoteRowsHeader"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:value-of select="CharCode:increment()"/>

						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">

							<xsl:value-of select="CharCode:increment()"/>
							<xsl:for-each select="/Booking/TicketQuotes/Flight[count(. | key('booking_segment_id_group',booking_segment_id)[1]) = 1]">

								<xsl:variable name="booking_segment_id" select="booking_segment_id"/>

								<tr bgcolor="#FFFFFF">

									<td align="left" style="width: 465px;">
										<!--<xsl:value-of select="fare_type_rcd"/>&#xA0;&#xA0;-->
										<xsl:text>Flight&#xA0;-&#xA0;Fare&#xA0;</xsl:text>
										<xsl:value-of select="airline_rcd"/>&#xA0;
										<xsl:value-of select="flight_number"/>
									</td>

									<td style="width: 200px;" align="right">
										<xsl:value-of select="currency_rcd"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="format-number(sum(//TicketQuotes/Flight[booking_segment_id=$booking_segment_id]/charge_amount),'#,###')"/>
									</td>
								</tr>
							</xsl:for-each>


							<xsl:for-each select="//Fees/Fee">

								<xsl:sort select="display_name" order="ascending"/>

								<xsl:if test="position()=1">
									<xsl:value-of select="CharCode:increment()"/>
									<tr bgcolor="#FFFFFF">

										<td align="left" style="width: 465px;">
											<xsl:value-of select="display_name"/>
										</td>

										<td align="right" style="width: 200px;">
											<xsl:value-of select="currency_rcd"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="format-number((fee_amount_incl),'#,##0')"/>
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="position()!=1">
									<xsl:value-of select="CharCode:increment()"/>
									<tr bgcolor="#FFFFFF">
										<td style="width: 465px; border-right: .0pt;">
											<xsl:value-of select="display_name"/>
										</td>

										<td style="width: 200px;" align="right" bgcolor="#FFFFFF">
											<xsl:value-of select="currency_rcd"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="format-number((fee_amount_incl),'#,##0')"/>
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>

							<xsl:value-of select="CharCode:increment()"/>
							<tr bgcolor="#FFFFFF">

								<th colspan="1" style="color:#000000;width: 465px;text-align: left;font-size:10pt;font-weight : bold;">
									<span>합계</span>
								</th>
								<th colspan="1" style="text-align: right;color:#000000;width: 200px;">
									<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
									<xsl:text>&#xA0;</xsl:text>
									<xsl:value-of select="format-number((sum(//TicketQuotes/Flight/charge_amount)) + sum(//Fees/Fee/fee_amount_incl),'#,##0')"/>
								</th>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br/>
		</xsl:if>
	</xsl:template>
	<!--End QuoteSection-->





	<!--Payment Section-->
	<xsl:variable name="PaymentRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="width: 665px;">
			<tr>
				<td align="left" style="color: #000000; background-color: #de81d3;width: 200px;font-size:10pt;font-weight : bold;">항목</td>
				<td align="left" style="color: #000000;background-color: #de81d3; width: 200px;font-size:10pt;font-weight : bold;">상세</td>
				<td align="center" style="color: #000000; background-color: #de81d3;width: 65px;font-size:10pt;font-weight : bold;">날짜</td>
				<td align="right" style="color: #000000; background-color: #de81d3;width: 100px;font-size:10pt;font-weight : bold;">구입금액</td>
				<td align="right" style="color: #000000; background-color: #de81d3;width: 100px;font-size:10pt;font-weight : bold;">지불금액</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Payments">
		<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table cellspacing="1px" bgcolor="#e5e5e5" style="font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


						<tr>
							<td>지불 수단 내역</td>
						</tr>
					</table>
					<!--<h1>PAYMENTS</h1>-->
					<xsl:copy-of select="$PaymentRowsHeader"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:value-of select="CharCode:increment()"/>
					<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px; width: 665px;">
						<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
						<xsl:variable name="Ticket_total"
						              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee/fee_amount_incl)"/>


						<xsl:choose>
							<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">
								<!--<table cellspacing="0" class="grid-main" style="border-bottom: .0pt;border-top: none;border-left: none; border-right: none; table-layout:fixed; width: 673px; margin: 0px 0px 0px 36px;">-->
								<xsl:value-of select="CharCode:increment()"/>
								<tr bgcolor="#FFFFFF">
									<td align="left" style="width:200px;;">항공권 요금과 수수료</td>
									<td align="left" style="width:200px;">&#xA0;</td>
									<td align="center" style="width:65px;">&#xA0;</td>
									<td align="right" style="width:100px;">
										<xsl:if test="starts-with(string($Ticket_total), '-')">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0')"/></xsl:if>
										<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
											<xsl:value-of select="format-number($Ticket_total,'#,##0')"/>
										</xsl:if>
									</td>
									<td align="right" style="width:100px;">&#xA0;</td>
								</tr>


								<xsl:for-each select="/Booking/Payments/Payment[void_date_time=''][form_of_payment_subtype_rcd !='REFUND']">
									<xsl:if test="form_of_payment !=''">
										<xsl:value-of select="CharCode:increment()"/>
										<tr bgcolor="#FFFFFF">
											<td align="left" style="width: 200px; ">
												<xsl:choose>
													<xsl:when test="form_of_payment_subtype !=''">
														<xsl:value-of select="form_of_payment_subtype"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="form_of_payment"/>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="left" style="width: 200px;">
												<xsl:value-of select="document_number"/>
											</td>
											<td align="left" style="width: 65px;">
												<xsl:choose>
													<xsl:when test="void_date_time != ''">
														<xsl:text>XX</xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:if test="payment_date_time != ''">
															<xsl:call-template name="dt:formatdate">
																<xsl:with-param name="date" select="payment_date_time"/>
															</xsl:call-template>
														</xsl:if>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" style="width: 100px;">
												<xsl:choose>
													<xsl:when test="not(payment_amount &gt;= 0)">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
														<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0')"/>
													</xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" style="width: 100px;">
												<xsl:choose>
													<xsl:when test="payment_amount &gt; 0">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
														<xsl:value-of select="format-number(payment_amount,'#,##0')"/>
													</xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</xsl:if>
								</xsl:for-each>
								<xsl:value-of select="CharCode:increment()"/>
								<tr bgcolor="#FFFFFF">

									<xsl:variable name="SubTotal" select="$Ticket_total - $Payment_total"/>
									<xsl:choose>
										<xsl:when test="number($SubTotal) &lt; 0">

											<th colspan="3" style="text-align: left;color:#000000;width: 465px;;font-size:10pt;font-weight : bold;">미결제 잔액</th>
											<xsl:if test="number($SubTotal) != 0">
												<xsl:if test="number($SubTotal) &gt;= 0">

													<th colspan="1" style="text-align: right;color:#000000;width: 100px;">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
														<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0')"/>
													</th>
													<th style="color:#000000;width: 100px;">&#xA0;</th>
												</xsl:if>
												<xsl:if test="number($SubTotal) &lt; 0">
													<th colspan="1" style="text-align: right;color:#000000;width: 100px;">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
														<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0')"/>
													</th>
													<th style="color:#000000;width: 100px;">&#xA0;</th>
												</xsl:if>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>

											<th colspan="4" style="text-align: left;color:#000000;border-top:none; border-right-style: none; width: 565px;;font-size:10pt;font-weight : bold;">미결제 잔액</th>
											<th colspan="1" style="text-align: right;color:#000000;border-top:none; border-right-style: none;width: 100px;">
												<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;0&#xA0;</th>
										</xsl:otherwise>
									</xsl:choose>
								</tr>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="CharCode:increment()"/>
								<tr bgcolor="#FFFFFF">
									<td align="left" style="border-top:none; border-right-style: none; width: 200px;font-size:10pt;font-weight : bold;">항공권 요금과 수수료</td>
									<td align="left" style="border-top:none; border-right-style: none;width: 200px;">&#xA0;</td>
									<td align="left" style="border-top:none; border-right-style: none; width: 65px;">&#xA0;</td>
									<xsl:if test="starts-with(string($Ticket_total), '-')">
										<td align="left" style="border-top:none; border-right-style: none;width: 100px;">&#xA0;</td>
										<td align="right" style="border-top:none; border-right-style: none;width: 100px;">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
											<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0')"/>
										</td>
									</xsl:if>
									<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
										<td align="right" style="border-top:none; border-right-style: none;width: 100px;">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number($Ticket_total,'#,##0')"/></td>
										<td align="left" style="border-top:none; border-right-style: none;width: 100px;">&#xA0;</td>
									</xsl:if>
								</tr>


								<xsl:for-each select="/Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)][form_of_payment_subtype_rcd !='REFUND'][(payment_amount &gt;= 0)]">
									<xsl:if test="form_of_payment_subtype !=''">
										<xsl:value-of select="CharCode:increment()"/>
										<tr bgcolor="#FFFFFF">
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 200px;">
												<xsl:choose>
													<xsl:when test="form_of_payment_subtype !=''">
														<xsl:value-of select="form_of_payment_subtype"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="form_of_payment"/>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none;width: 200px;">
												<xsl:value-of select="document_number"/>
											</td>
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 65px;">
												<xsl:choose>
													<xsl:when test="void_date_time != ''">
														<xsl:text>XX</xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:if test="payment_date_time != ''">
															<xsl:call-template name="dt:formatdate">
																<xsl:with-param name="date" select="payment_date_time"/>
															</xsl:call-template>
														</xsl:if>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" class="grid-even" style="border-top:none; border-right-style: none;width: 100px;">
												<xsl:choose>
													<xsl:when test="not(payment_amount &gt;= 0)">
														<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" class="grid-even" style="border-top:none; border-right-style: none;text-align: right;width: 100px;">
												<xsl:choose>
													<xsl:when test="payment_amount &gt; 0">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number(payment_amount,'#,##0')"/></xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</xsl:if>

									<xsl:if test="form_of_payment_subtype =''">
										<xsl:value-of select="CharCode:increment()"/>
										<tr bgcolor="#FFFFFF">
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 200px">
												<xsl:value-of select="form_of_payment"/>
											</td>
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none;">
												<xsl:value-of select="document_number"/>
											</td>
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 90px">
												<xsl:if test="payment_date_time != ''">
													<xsl:call-template name="dt:formatdate">
														<xsl:with-param name="date" select="payment_date_time"/>
													</xsl:call-template>
												</xsl:if>
											</td>
											<td align="right" class="grid-even" style="border-top:none; border-right-style: none;">
												<xsl:choose>
													<xsl:when test="not(payment_amount &gt;= 0)">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0')"/></xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" class="grid-even" style="border-top:none; border-right-style: none;">
												<xsl:choose>
													<xsl:when test="payment_amount &gt; 0">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number(payment_amount,'#,##0')"/></xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</xsl:if>
								</xsl:for-each>
								<xsl:value-of select="CharCode:increment()"/>
								<tr bgcolor="#FFFFFF">
									<td align="left" style="border-top:none; border-right-style: none; width: 200px">미결제 잔액</td>
									<td align="left" style="border-top:none; border-right-style: none;">&#xA0;</td>
									<td align="left" style="border-top:none; border-right-style: none; width: 90px">&#xA0;</td>

									<xsl:variable name="SubTotal" select="number($Ticket_total) - number(sum(//Booking/Payments/Payment[string-length(void_by)!=38][form_of_payment_subtype_rcd !='REFUND'][not(payment_amount &lt; 0)] /payment_amount))"/>

									<xsl:if test="number($SubTotal) = 0">
										<td align="left" style="border-top:none; border-right-style: none;">&#xA0;</td>
										<td align="right" style="border-top:none; border-right-style: none;">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;0&#xA0;</td>
									</xsl:if>
									<xsl:if test="number($SubTotal) != 0">
										<xsl:if test="number($SubTotal) &gt;= 0">
											<td align="left" style="border-top:none; border-right-style: none;">&#xA0;</td>
											<td align="right" style="border-top:none; border-right-style: none;">
												<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number($SubTotal,'#,##0')"/></td>
										</xsl:if>
										<xsl:if test="number($SubTotal) &lt; 0">
											<td align="right" style="border-top:none; border-right-style: none;">
												<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
												<xsl:value-of select="format-number($SubTotal,'#,##0')"/>
											</td>
											<td align="left" style="border-top:none; border-right-style: none;">&#xA0;</td>
										</xsl:if>
									</xsl:if>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</table>
				</td>
			</tr>
		</table>

		<br/>
	</xsl:template>
	<!--End Payments-->

	<!--Peach Point-->

	<xsl:variable name="PeachRowsHeader">

		<table border="0" cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
			<tr>
				<td align="left" style="width:215px; color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">항목</td>
				<td align="left" style="width:350px; color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">상세</td>
				<td align="left" style="width: 100px; color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">적립 포인트</td>
			</tr>
		</table>
	</xsl:variable>

	<xsl:template match="/Booking">

		<xsl:if test="/Booking/Tickets/Ticket[passenger_status_rcd='XX'][e_ticket_status ='R']">
			<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
				<tr>
					<td>
						<table style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


							<tr>
								<td>피치포인트 적립 예정(변경시 아래의 항목에 관계없이 피치 포인트는 부여되지 않습니다.)</td>
							</tr>
						</table>
						<xsl:copy-of select="$PeachRowsHeader"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:value-of select="CharCode:increment()"/>



						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">
							<xsl:value-of select="CharCode:increment()"/>
							<xsl:for-each select="/Booking/Tickets/Ticket[passenger_status_rcd ='XX'][e_ticket_status ='R']">
								<tr bgcolor="#FFFFFF">

									<td style="width:215px; border-right: .0pt;">
										<xsl:text>피치포인트（적립）</xsl:text>
										<!--<xsl:value-of select="e_ticket_status"/><xsl:value-of select="passenger_status_rcd"/>-->
									</td>
									<td style=" border-right: .0pt;width: 350px">
										<xsl:text>예약번호 : </xsl:text>
										<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text>편명 : </xsl:text>
										<xsl:value-of select="airline_rcd"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="flight_number"/>
									</td>
									<td align="right" style=" border-right: .0pt;width: 100px">

										<xsl:value-of select="currency_rcd"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="format-number(sum(net_total),'#,##0')"/>
									</td>
								</tr>
								<tr bgcolor="#FFFFFF">
									<td style="width:215px; border-right: .0pt;">
										<xsl:text>취소 수수료</xsl:text>
									</td>
									<td style=" border-right: .0pt;width: 350px">
										<xsl:text>예약번호 : </xsl:text>
										<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text>편명 : </xsl:text>
										<xsl:value-of select="airline_rcd"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="flight_number"/>
									</td>

									<td align="right" style="border-right: .0pt;width: 100px">
										<xsl:if test="refund_charge != ''">
											<xsl:text> - </xsl:text>
											<xsl:value-of select="currency_rcd"/>
											<xsl:text> </xsl:text>
											<xsl:value-of select="format-number(sum(refund_charge),'#,##0')"/>&#x20;&#x20;
										</xsl:if>
										<xsl:if test="refund_charge = ''">
											<xsl:value-of select="currency_rcd"/>
											<xsl:text> </xsl:text>
											<xsl:text> 0</xsl:text>
										</xsl:if>
									</td>
								</tr>
							</xsl:for-each>
							<tr bgcolor="#FFFFFF">
								<td style="width:215px; border-right: .0pt;">
									<xsl:text>&#xA0;</xsl:text>
								</td>
								<td style=" border-right: .0pt;width: 350px">
									<xsl:text>&#xA0;</xsl:text>
								</td>

								<td align="right" style=" border-right: .0pt;width: 100px">
									<xsl:text>&#xA0;</xsl:text>
								</td>
							</tr>

							<tr bgcolor="#FFFFFF">
								<td style="width:215px; border-right: .0pt;font-size:10pt;font-weight : bold; ">
									<xsl:text>누적 포인트 합계</xsl:text>
								</td>
								<td style=" border-right: .0pt;width: 350px">
									<xsl:text> </xsl:text>
								</td>
								<td align="right" style=" font: bold; border-right: .0pt;width: 100px">
									<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="format-number(sum(/Booking/Tickets/Ticket[passenger_status_rcd ='XX'][e_ticket_status ='R']/refundable_amount),'#,##0')"/>&#x20;&#x20;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>


	<!--End Peach Point-->

	<!--FARE RULES-->
	<xsl:template match="//BookingHeader">
		<xsl:copy-of select="$Change_Fiight"/>
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table border="0" cellspacing="0" style="font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #b634bb;font-size:10pt;">


						<tr>
							<td>주의 사항</td>
						</tr>
					</table>
					<table class="grid-main" style="border-top: none;border-left: none; table-layout:fixed; width: 665px;margin: 0px 0px 0px 0px;">
						<xsl:choose>
							<xsl:when test="/Booking/Header/BookingHeader[own_agency_flag =1]">

								<tr bgcolor="#FFFFFF">
									<td>
										<p class="msonormal" style="font-size:10pt;">일본내 편의점 및 그 외 지불방법을 선택할 시, 입금기한까지 결재되지 않을 경우 위의 예약은 자동으로 취소되며, 이 여정표는 무효가 됩니다. </p>
										<p class="msonormal" style="font-size:10pt;">예약내용의 변경에는 운임에 따른 수수료가 발생할 수 있습니다. 자세한 내용은 www.flypeach.com 에서 확인해주세요.</p>

										<br/>
										<br/>
										<p class="msonormal" style="font-size:10pt;">저희 Peach 는 고객 여러분을 기내에서 뵙게 되기를 기쁜 마음으로 기다리고 있습니다.</p>
										<br/>
										<br/>
										<p class="msonormal" style="font-size:10pt;">Peach Aviation Limited</p>
										<p class="msonormal" style="font-size:10pt;">
											<a target="_blank" href="http://www.flypeach.com">www.flypeach.com</a>
										</p>
										<p class="msonormal">
										</p>
									</td>
								</tr>
							</xsl:when>
							<xsl:otherwise>
								<tr bgcolor="#FFFFFF">
									<td>
										<p class="msonormal" style="font-size:10pt;">자세한 내용은 구입하신 여행 대리점으로 문의 해 주십시오. </p>
										<p class="msonormal" style="font-size:9pt;">Peach의 운송 약관에 대해서는 www.flypeach.com 에서 확인 해 주십시오. </p>
										<br/>
										<br/>
										<p class="msonormal" style="font-size:10pt;">저희 Peach 는 고객 여러분을 기내에서 뵙게 되기를 기쁜 마음으로 기다리고 있습니다.</p>
										<br/>
										<br/>
										<p class="msonormal" style="font-size:10pt;">Peach Aviation Limited</p>
										<p class="msonormal" style="font-size:10pt;">
											<a target="_blank" href="http://www.flypeach.com">www.flypeach.com</a>
										</p>
										<p class="msonormal">
										</p>
									</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</table>
					<br/>
					<table cellspacing="0" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #b634bb;font-size:8pt;">


						<tr>
							<td align="left">
								<a target="_blank" href="http://www.flypeach.com/kr/corporate/termsandconditions.aspx?&amp;utm_source=KOKR&amp;utm_medium=footer_Terms_of_Conditions&amp;utm_campaign=itinerary">
									<font style="color: #ffffff;">이용 약관</font>
								</a>| <a target="_blank" href="https://www.flypeach.com/kr/corporate/privacypolicy.aspx&amp;utm_source=KOKR&amp;utm_medium=footer_privacypolicy&amp;utm_campaign=itinerary"><font style="color: #ffffff;">개인 정보 보호 정책</font></a></td>
							<td align="right">Copyright© Peach Aviation Limited</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
		<table cellspacing="0" style="margin: 0px 0px 0px 0px;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff ;font-size:8pt;">

			<tr align="center">
				<td>

					<a target="_blank" href="http://www.flypeach.com/ibe/{$booking_source}/redirect_ad1/{$language_check}/{$od_destination_rcd}.aspx">

						<img border="0" src="http://www.flypeach.com/Portals/0/Images/ibe/{$booking_source}/itinerary/ad1/{$language_check}/{$od_destination_rcd}.jpg"/>
					</a>
				</td>
			</tr>
		</table>
		</td>
		</tr>
		</table>
		<br/>
		<br/>



		<div style="page-break-after:always"></div>


		 
		<xsl:if test="/Booking/Header/BookingHeader[own_agency_flag =1]">

			<xsl:copy-of select="$R_OrderRecipient"/>
		</xsl:if>

		<!--<xsl:copy-of select="$R_OrderRowsHeader"/> -->
	</xsl:template>

	<xsl:variable name="R_OrderRecipient">
<xsl:if test="count(/Booking/Fees/Fee[fee_rcd = 'INSHK'])= 0 ">
			<xsl:if test="/Booking/Payments/Payment !=''">
		<table border="0" class="grid-main" style="width:673px; margin: 0px 0px 0px 36px;">
			<tr>
				<td align="right">RECEIPT No.</td>
				<td align="right">
					<xsl:value-of select="/Booking/Payments/Payment/payment_number"/>
				</td>
			</tr>
			<tr>
				<td align="right">표시일</td>
				<td align="right" style=";font-size:11pt;">년   월　일</td>
			</tr>
			<tr>
				<td align="right">DATE OF DISPLAY</td>
				<td align="right">
					<xsl:call-template name="dt:formatdate">
						<xsl:with-param name="date" select="/Booking/Payments/Payment/payment_date_time"/>
					</xsl:call-template>
				</td>
			</tr>
			<tr>
				<td align="center" style="font-size:14pt;color: #000000;">영수증 <p/>RECEIPT</td>
			</tr>
			<tr>
				<td align="left" style="font-size:8pt;color: #000000;">아래의 금액을 수령 하였음을 확인합니다.<p/>This is to certify that Peach Aviation has received the following.</td>
			</tr>
		</table>
		<br/>
		<table border="0" cellspacing="0" style="color: #000000;width:673px; margin: 0px 0px 0px 36px;">
			<tr>

				<td style="width:100px;font-size:9pt;color: #000000;">수신 <p/>RECEIVED FROM</td>
				<td align="right" style="font-size:9pt;color: #000000;border-bottom:1px solid #002256;">様</td>
			</tr>
			<tr>

				<td style="width:100px;color: #000000;border-bottom:1px solid #e5e5e5;">&#xA0;</td>
				<td style="font-size:9pt;color: #000000;border-bottom:1px solid #e5e5e5;">&#xA0;</td>
			</tr>
		</table>
		<br/>
		<table class="tableReportHeader" cellspacing="0" style="color: #000000;width:673px; margin: 0px 0px 0px 36px;">

			<tr>
				<td align="left" style="font-size:10pt;color: #000000;">금액 <p class="msonormal"/>THE SUM OF</td>
				<td/>
				<td>

					<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>

					<xsl:variable name="Ticket_total"
					              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = ''][fee_rcd !='INSU']/fee_amount_incl)"/>

					<xsl:value-of select="/Booking/TicketQuotes/Total/currency_rcd"/>&#xA0;

					<xsl:choose>
						<xsl:when test="//Fees/Fee/fee_rcd ='INSU' ">
							<xsl:value-of select="format-number($Ticket_total,'#,##0')"/>
						</xsl:when>

						<xsl:otherwise>
							<!--<xsl:value-of select="format-number(sum(//Payments/Payment/payment_amount),'#,##0')"/>-->
							<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee/fee_amount_incl),'#,##0')"/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
				<td align="right" style="font-size:10pt;color: #000000;">(세금포함/inc)</td>
				<td/>
			</tr>
		</table>
		<br/>

		<table border="0" cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style=" margin: 0px 0px 0px 150px; table-layout:fixed; width: 400px;">


			<tr align="left" bgcolor="#FFFFFF" style="border-top:none; border-right-style: none; width: 200px;">
				<td>명세 / FORM OF PAYMENT</td>
				<!--<td>
                        <xsl:call-template name="dt:formatdate">
                        <xsl:with-param name="date" select="/Booking/Payments/Payment/payment_date_time"/>
                    </xsl:call-template>
                        </td>-->
			</tr>
		</table>
		<xsl:for-each select="/Booking/Payments/Payment[void_date_time='']">
			<xsl:variable name="booking_id" select="booking_id"/>

			<xsl:variable name="booking_payment_id" select="booking_payment_id"/>
			<xsl:variable name="payment_visa" select="/Booking/Payments/Payment[form_of_payment_rcd='CC']/payment_amount"/>


			<xsl:variable name="fee_amount_incl" select="//Fees/Fee[booking_id=$booking_id][fee_rcd ='INSU']/fee_amount_incl"/>


			<xsl:variable name="total_fee_amount_incl" select="(sum($payment_visa))- $fee_amount_incl"/>
			<!--<xsl:variable name="Ticket_total"
        select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = ''][fee_rcd !='INSU']/fee_amount_incl)"/>-->

			<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style=" margin: 0px 0px 0px 150px; table-layout:fixed; width: 400px;">

				<tr bgcolor="#ffffff">
					<td align="left" style="border-top:none; border-right-style: none; width: 200px; ">
						<xsl:choose>
							<xsl:when test="form_of_payment_subtype !=''">
								<xsl:value-of select="form_of_payment_subtype"/>
							</xsl:when>

							<xsl:otherwise>
								<xsl:value-of select="form_of_payment"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<!--<xsl:call-template name="dt:formatdate">
                        <xsl:with-param name="date" select="/Booking/Payments/Payment/payment_date_time"/>
                    </xsl:call-template> -->

						<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
						<xsl:text> </xsl:text>
						<xsl:choose>
							<xsl:when test="form_of_payment_rcd='CC' and $fee_amount_incl !='' and position()=1">

								<xsl:value-of select="format-number((payment_amount)- $fee_amount_incl,'#,##0')"/>
							</xsl:when>
							<xsl:when test="form_of_payment_rcd='CC' and $fee_amount_incl =''">
								<xsl:value-of select="format-number($payment_visa,'#,##0')"/>
							</xsl:when>

							<xsl:otherwise>
								<xsl:value-of select="format-number(payment_amount,'#,##0')"/>
							</xsl:otherwise>
						</xsl:choose>

						<!-- <xsl:value-of select="format-number($fee_amount_incl,'#,##0')"/> -->
					</td>
				</tr>
			</table>
		</xsl:for-each>
		<br/>

		<table border="0" class="tableReportHeader" cellspacing="0" style=" margin: 0px 0px 0px 36px;table-layout:fixed;width: 673px;">
			<tr class="grid-main">
				<td valign="middle" style="text-align: left;height:60px;color:#000000;background-color: white; border-bottom:1px solid #e5e5e5;border-top: solid 1px #e5e5e5;width: 200px;">단</td>
				<td valign="middle" style="text-align: left;height:60px;color:#000000;background-color: white; border-bottom:1px solid #e5e5e5;border-top: solid 1px #e5e5e5;">운임 및 세금 / 요금 등
					<!--<p class="msonormal1"/>（단, 상기금액에는 여행보험료는 포함되어 있지 않습니다. 여행보험료의 영수증이 필요하신 분은 에이스보험 변경 접수센터로 연락하시기 바랍니다.）
					<p class="msonormal1"/>에이스보험 변경 접수 센터 TEL：03-5740-0741 월～금（토일/공휴일 휴무）9:00－17:00--></td>
			</tr>
			<tr class="grid-main">
				<td valign="middle" style="text-align: left;height:60px;color:#000000;background-color: white; border-bottom:1px solid #e5e5e5;border-top: solid 1px #e5e5e5;width: 200px;">IN PAYMENT OF</td>
				<td valign="middle" style="text-align: left;height:60px;color:#000000;background-color: white; border-bottom:1px solid #e5e5e5;border-top: solid 1px #e5e5e5;">AIR FARE and TAX/FEE/CHARGE FOR THE FOLLOWING.
					<!--<p class="msonormal1"/>(However, the amount above does not include travel insurance fee. For the receipt of travel insurance, please contact to ACE Travel Insurance Call Center.)
					<p class="msonormal1"/>ACE Travel Insurance Call Center (Phone: 03-5740-0741 9:00-17:00 on weekdays)--></td>
			</tr>
			<tr valign="middle" class="grid-main">
				<th style="text-align: left;color:#000000;height:40px;background-color: white; border-bottom:1px solid #e5e5e5;">예약 번호<p class="msonormal"/>BOOKING NUMBER</th>
				<th align="left" style="text-align: left;color:#000000;height:40px;background-color: white; border-bottom:1px solid #e5e5e5;">
					<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
				</th>
			</tr>
			<tr valign="middle" class="grid-main">
				<th style="text-align: left;color:#000000;height:40px;background-color: white; border-bottom:1px solid #e5e5e5;">예약일<p class="msonormal"/>BOOKING DATE</th>
				<th style="text-align: left;color:#000000;height:40px;background-color: white; border-bottom:1px solid #e5e5e5;">
					<xsl:call-template name="dt:formatdate">
						<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
					</xsl:call-template>
				</th>
			</tr>
		</table>
		<br/>
		<table border="0" class="tableReportHeader" cellspacing="0" style=" margin: 0px 0px 0px 36px;table-layout:fixed;width: 673px;">

			<tr>
				<td style="height:90px;width: 350px;">
					<img alt="Smile" src="http://www.avantikjp.com/XSLImages/jad/JAD_Logo.jpg" width="150"/>
				</td>
				<td style="height:90px;">
					<td class="grid-main" style="text-align: left;color:#000000; background-color: white;">Peach Aviation  주식회사
						<p class="msonormal"/>Peach Aviation Limited</td>
				</td>
			</tr>
			<tr>
				<td class="grid-main" style="height:90px;text-align: left;color:#000000 ;background-color: white;">상기 내역은 영수 금액에 대한 전자 발행서입니다. <p class="msonormal"/>
				This is an electronic display of receipt data.</td>
			</tr>
		</table>
		</xsl:if>
		</xsl:if>
		<br/>
	</xsl:variable>
	<!--End FARE RULES-->
	<xsl:variable name="Important">
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:665px;border-right:solid #000000 1px;border-left:solid #000000 1px;border-top:solid #000000 1px;border-bottom:solid #000000 1px;">
			<tr>
				<td>
					<p style="width:660px;color: #ffffff; background-color: #a8b400;font-size:10pt;font-weight : bold; font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;">
						<strong>탑승 전 확인 사항</strong>
					</p>

					<p class="msonormal">
					<strong>오사카(간사이) 출발 모든 승객은 승객 본인이 직접 셀프 체크인 기계를 이용해 탑승 수속을 하시게 됩니다.</strong>
					</p>
					<p class="msonormal">탑승 수속 시 본 확인서 상단의 바코드가 필요하므로 반드시 인쇄된 종이나, 휴대전화에 여정표 화면을 소지하셔야 합니다. 인쇄하신 항공권의 바코드 부분이 접히지 않도록 해 주십시오. 또한 비나 물에 젖으면 바코드가 읽히지 않을 수 있으므로 주의해 주시기 바랍니다.</p>
					<br/>
					<p class="msonormal">Peach의 체크인 카운터 혹은 탑승구는 기존의 항공회사들과는 다른 경우가 있습니다. </p>
				 	<p class="msonormal">반드시 탑승 전 내용을 확인 해 주시고, 시간적 여유를 가지고 공항에 와 주시기 바랍니다.  </p>
					<br/>
					<p class="msonormal">정시 출발을 위해 지정된 시간 내에 탑승 수속을 완료 해 주시기 바랍니다.</p>
					<p class="msonormal">
						<strong>일본 국내선의 경우는 출발시각</strong><font style="color:red"><b><u>30분 전</u></b></font><strong>까지 탑승수속을 완료해 주십시오. (간사이 국제공항과 나하공항은 25분 전)</strong>
					</p>
					<p class="msonormal">
						<strong>국제선의 경우는 출발시각</strong><font style="color:red"><b><u>50분 전</u></b></font><b>까지 탑승수속을 완료해 주십시오.</b>
					</p>
					<p class="msonormal">이후에는 탑승 수속이 불가하며, 다른 편으로의 변경 및 환불도 일체 불가능합니다.</p>
					<br/>
					<p class="msonormal">탑승하실 때 좌석 번호를 확인 해 주십시오. 12, 13열은 비상구 좌석으로 이용에 제한이 있습니다.</p>
					<p class="msonormal">또한, 좌석 번호 <u>11,12 열은</u>등받이가 고정되어 있으므로 양해 바랍니다.</p>
				</td>
			</tr>
		</table>
		<br/>



		<!--<xsl:for-each select="/Booking/Itinerary/FlightSegment[flight_connection_id !=''][count(. | key('flight_connection_id_group',flight_connection_id)[1]) = 1]">

			<xsl:variable name="flight_connection_id" select="flight_connection_id"/>

			<xsl:choose>
				<xsl:when test="position() != 0 ">
					<xsl:if test="position()=last()">-->
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:665px;border-right:solid #000000 1px;border-left:solid #000000 1px;border-top:solid #000000 1px;border-bottom:solid #000000 1px;">
			<tr>
				<td>
					<p style="width:660px;color: #ffffff; background-color: #a8b400;font-size:10pt;font-weight : bold; font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;">
						<strong>환승편에 대하여:</strong>
					</p>
					<p class="msonormal">Peach에서는 자동 수하물 환승(Baggage Thru Check-In) 및 체크인을 지원하지 않습니다. 환승 시, 환승 공항 도착 후 다음 편 이용을 위해서는 반드시 <b><font style="color:red">승객 본인이 다시 탑승수속을 진행 해 주셔야 합니다. </font></b>또한 위탁하신 수하물이 있으실 경우  환승 공항에 도착 후 일단 수하물을 찾으신 후, 환승을 위한 탑승 수속 시
						<b><font style="color:red">다시 짐을 맡기셔야 합니다.</font></b>국제선 환승편을 이용하시는 탑승객께서는 환승 공항에서 출입국 및 통관 관련 절차를 받으셔야 합니다. 여행 관련 서류(여권, 비자 등)의 준비 및 소지에 관한 책임은 탑승객 본인에게 있으므로, 반드시 사전에 해당 서류를 확인해주시기 바립니다.  </p>
				</td>
			</tr>
		</table>
		<br/>
		<!--	</xsl:if>
				</xsl:when>
				<xsl:otherwise>&#xA0;</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>-->
	</xsl:variable>
	<xsl:variable name="Change_Fiight">
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:665px;border-right:solid #000000 1px;border-left:solid #000000 1px;border-top:solid #000000 1px;border-bottom:solid #000000 1px;">
			<tr>
				<td>
					<p style="width:660px;color: #ffffff; background-color: #a8b400;font-size:10pt;font-weight : bold; font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;">
						<strong>변경 및 취소에 대해</strong>
					</p>
					<p class="msonormal">예약 내용의 변경에 따라 운임에 따른 수수료가 부과되는 경우가 있습니다.</p>
					<p class="msonormal">예약 변경은 예약한 항공편의 출발시각 1시간 전까지 인터넷 및 콜센터, 공항카운터에서 접수 가능합니다.</p>
					<p class="msonormal">다만 해피피치를 변경할 경우, 새롭게 변경되는 해당 운항편의 출발일 1일전 오전 0:00까지 예약이 가능하면, 당일 및 다음날 항공편으로의 변경은 해피피치 플러스로 변경이 됩니다. </p>
					<p class="msonormal">
						<font style="color:red"><b>1)항공편을 변경하시면 기존의 좌석지정, 위탁수하물 내용도 모두 취소되오니 양해 바랍니다.</b></font>
					</p>
					<p class="msonormal">변경 후 다시 좌석 지정을 하시거나 위탁수하물이 있으실 경우 별도 요금이 추가됩니다.</p>
					<p class="msonormal">
						<font style="color:red"><b>2)고객 사정에 의해 일정을 취소할 경우 환불이 불가합니다.</b></font>
					</p>
					<p class="msonormal">해피피치 플러스의 경우는 피치 포인트로 전환됩니다. (취소 수수료를 차감한 금액에 한해)</p>

					<p class="msonormal">포인트 확인까지는 일주일 정도의 기간이 소요되므로 양해 바랍니다.</p>
				</td>
			</tr>
		</table>
		<br/>
	</xsl:variable>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\1.xml" htmlbaseurl="" outputurl="" processortype="msxmldotnet" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->