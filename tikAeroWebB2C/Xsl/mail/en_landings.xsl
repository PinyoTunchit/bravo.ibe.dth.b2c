<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="PageTitle">tikAERO Landing and Takeoffs</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable>
	<xsl:variable name="ColorSpan">Red</xsl:variable>

	<xsl:key name="airport_rcd_group" match="//Landings/Details" use="airport_rcd"/>
	<xsl:key name="landing_flag_group" match="//Landings/Details" use="landing_flag"/>



	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($day, '/', $month, '/', $year)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:value-of select="concat($day, $month, $year)"/>
	</xsl:template>

<xsl:template name="jadate">
		<xsl:param name="date"/>
<script type="text/javascript">function checkTime(i)
{
if (i &lt; 10)
  
  i="0" + i;
  
return i;
}	
		
{var a_p = "";
	 var d = new Date();
	 var month=new Array(12);
month[0]="January";
month[1]="February";
month[2]="March";
month[3]="April";
month[4]="May";
month[5]="June";
month[6]="July";
month[7]="August";
month[8]="September";
month[9]="October";
month[10]="November";
month[11]="December";

										 
														var curr_hour = d.getHours();
														var curr_min = d.getMinutes();
														curr_min =checkTime(curr_min);
														var curr_Date = d.getDate();
														var curr_Month = month[d.getMonth()];
														var curr_Year = d.getYear();
														 
												         

														document.write(curr_Date + " " + curr_Month + " " +curr_Year + " " + curr_hour + ":"+ curr_min)
													}</script>
	</xsl:template>
	<xsl:template name="minutesdate">
		<xsl:param name="date"/>
		<script type="text/javascript">{var a_p = "";
	 var d = new Date();
	 
 
														var curr_hour = d.getHours();
														var curr_min = d.getMinutes();
														 
														document.write( curr_hour + ":"+ curr_min  )
														}</script>
	</xsl:template>
	<xsl:template name="makeTime">
		<xsl:param name="data"/>
		<xsl:variable name="hours" select="(floor($data div 60))"/>
		<xsl:variable name="minutes" select="(floor($data) mod 60)"/>
		<xsl:value-of select="concat(format-number($hours,'#0'),'.',format-number($minutes,'00'))"/>
	</xsl:template>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$PageTitle"/>
				</TITLE>
				 <STYLE>.HeaderBackground { background: url(http://www.tikaero.com/xslimages/AAS/Images/0/H/hhe.gif) fixed no-repeat left center; }
					.base { color: ; font-family: 12pt Tahoma, Arial, sans-serif, verdana; font-size: 9pt; width: 100%; height:100%}
					.base .content{ color: ; float: left;  font-size: 9pt; }
					.dummy{ }
					.base .content .contentHeader{ margin: 5px 0px 0px 0px; padding: 0px 0px 0px 5px; }
					.base .quickSearch { float: left; width: 170px; }					
					.base .quickSearch .quickSearchHeader{ background: url(http://www.tikaero.com/xslimages/AAS/Images/0/H/hqs.gif) fixed no-repeat left center; padding: 10px 0px 5px 0px; }
					.base .quickSearch DIV.input { font-size: 8pt; float: left; padding: 2px 4px 2px 5px; text-align: left; }
					.base .quickSearch DIV.label{ float: left; padding: 2px 4px 2px 5px; text-align: right; vertical-align: middle; }
					.BarcodeHeader	{ color: ; font-size: 7pt; font-style: normal; font-weight: bold; padding: 0px 0px 0px 12px; text-align: left; width: 100%; }
					.PanelItinerary .Barcode	{font: bold 15pt Tahoma, Arial, sans-serif, verdana;padding: 1 5 1 5; }
					.footer	{ background: url(http://www.tikaero.com/xslimages/AAS/Images/0/I/bgb.gif); color: #201384; font-size: 8pt; padding: 15px 5px 0px 5px; width: 100%; }
					.footer A	{ height: 20px; text-align: right; width: 50%; }
					.footer	SPAN	{ height: 20px; padding-top: 3px; width: 50%; }
					.grid TD.gridItems0Button	{ padding: 1px 5px 1px 1px; text-align: right; }
					.grid TR.GridFooter TD	{ background-color: transparent; border-bottom: 1px solid  #fdb813; color: #666666; font: bold 8pt ; border-top: 1px solid #fdb813; height: 22px;  }
					.grid TR.GridFooter TD.Summary	{ background-color: transparent; }
					 
					.grid TR.gridHeader	{border-right:1px solid #fdb813;border-left:1px solid #fdb813;background:#fdb813;color: #003964;font-size:11px;font-weight:bold;padding: 4px 2px 4px 5px;font-family: Tahoma, Arial, sans-serif, verdana;}
					.grid TR.GridPrice	{ border-right:1px solid #fdb813;border-left:1px solid #fdb813;background-color: tranparent; color: ; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridHeader TD	{border-right:1px solid #fdb813;border-left:1px solid #fdb813; padding: 4px 2px 4px 5px; border-bottom: 1px solid  #F7C07D; }
					.grid TR.gridHeaderFight { {border-right:1px solid #fdb813;border-left:1px solid #fdb813;background-color: #fdb813; color: ; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridItems0 TD { border-bottom: 1px solid #F7C07D;color: color:#0D54B1  ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems1 TD { border-top: 1px solid  #fdb813; color: #F7C07D ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid tr.griditems2 td{color:;cursor:pointer;font-size:11px;font-weight:normal;height:15px;padding:0px 1px 0px 5px;}
					.grid TR.gridItems02 TD { color:  ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems0 TD { color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; }
					.grid TR.gridItems01 TD {font-family: Tahoma, Arial, sans-serif, verdana;color: ; cursor: pointer;  font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0o TD{ background-color: #FFBD3B; border-bottom: 1px solid  #DAE1E6; color: ; cursor: pointer;  font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01 TD	{font-family: Tahoma, Arial, sans-serif, verdana;border-right:1px solid #F7C07D;border-left:1px solid #F7C07D;border-bottom: 1px solid #F7C07D; color: #003964; ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01o TD	{ background-color: #CEE6F7; color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0Input	{{border-right:1px solid #fdb813;border-left:1px solid #fdb813; color: ;font-size: 11px; font-weight: normal; height: 22px; padding: 5px 1px 1px 5px; }
					.header	{ width: 100%; }
					.header .subHeader	{ background-color: ; }
					/* End Forms \*/
					/* Headers \*/
					.headers	{color:#C4102F; font: bold 12pt /*background: url(../Images/0/H/hbh.jpg);*/ margin: 5px 0px 5px 5px; width: 100%; }
					.headers .ItineraryGrid	{ background: url(http://www.tikaero.com/xslimages/Aurigny/Images/0/I/iin.gif) fixed no-repeat left center; }
					.headers .SearchResultGrid{ background: url(http://www.tikaero.com/xslimages/AAS/Images/0/I/isr.gif) fixed no-repeat left center; }
					.headers DIV	{ font: bold 8pt  margin: 3px 0px 3px 0px; padding: 5px 0px 5px 17px; }
					.headers DIV SPAN.S0{ color: #FF6600; }
					.headers SPAN.Button { width: 49%; }
					.Itinerary	{ color: ; font-size: 8pt; font-style: normal; padding: 10px 12px 0px 12px; text-align: left; width: 100%; }
					.Itinerary Span.O	{ color: #FF6600; font-weight: bold; }
					.Itinerary Span.1	{ color: #FF6600; font-weight: bold; text-decoration: underline; }
					.Itinerary Span.2 { color: ; font-weight: bold; }
					.welcometext,.
					.commenttext{font-family: Tahoma, Arial, sans-serif, verdana;font-size:12px;padding:1 10 1 5;}
	 				table.MsoNormalTable{mso-style-parent:"";	font-size:10.0pt;	font-family:"helvetica, Tahoma, Arial, sans-serif, verdana";	}
 					p.MsoNormal{mso-style-parent:"";	margin-bottom:.0001pt;	font-size:8.0pt;	font-family: Tahoma, Arial, sans-serif, verdanal;	color:navy;	font-weight:bold;	margin-left:0in; margin-right:0in; margin-top:0in}</STYLE>
				 
				<!--End Style Sheet-->
			</HEAD>
			<BODY>
				   <DIV class="PanelFlightDispatch">
					<img src="{$BaseURL}ISK_logo.jpg"/>
	 				</DIV>
					<p/>
						 
					
				 <table class="Grid" rules="all" cellpadding="3" border="0" style="width:100%;font: bold 12pt Tahoma, Arial, sans-serif, verdana; color:#C4102F;font-size: 12px; ">
					<tr>
						<td align="left" width="30%">
							<xsl:text>Landing and Takeoffs</xsl:text>
						</td>
						<td align="left" width="20%">&#xA0;</td>
						<td align="right" width="50%">Print date : &#xA0;&#xA0;


							<xsl:call-template name="jadate">
								<xsl:with-param name="date"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
				  
 <DIV class="PanelItinerary">
								<table class="Grid"  rules="all"   cellspacing="1" cellpadding="0" border="0"  style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12pt;">
		 
			<tr class="gridHeader" valign="middle">
									<TD width="20%" align="center">Flight</TD>
									<TD width="20%" align="center">Landings</TD>
<!--									<TD width="20%" align="center">occurences</TD>-->
									<TD width="20%" align="center">Takeoffs</TD>
<!--									<TD width="20%" align="center">occurences</TD>-->
								</tr>
								<xsl:for-each select="//Landings/Details[count(. | key('airport_rcd_group', airport_rcd)[1]) = 1]">

									<xsl:variable name="airport_rcd" select="airport_rcd"/>

									<TR class="GridItems01">

										<TD width="20%" align="center">
											<xsl:value-of select="airport_rcd"/>
										</TD>

										<xsl:for-each select="//Landings/Details[ landing_flag = 1][airport_rcd = $airport_rcd]">
											<xsl:variable name="landing_flag_1" select="landing_flag"/>


											<xsl:if test="$landing_flag_1 = 1">
												<TD width="20%" align="center">

													<xsl:value-of select="occurences"/>
												</TD>


<!--												<TD width="20%" align="center">

													<xsl:value-of select="occurences"/>
												</TD>-->
											</xsl:if>
										</xsl:for-each>

										<xsl:for-each select="//Landings/Details[ landing_flag = 0][airport_rcd = $airport_rcd]">
											<xsl:variable name="landing_flag_2" select="landing_flag"/>
											<xsl:if test="$landing_flag_2 = 0">
												<TD width="20%" align="center">

													<xsl:value-of select="occurences"/>
												</TD>
<!--												<TD width="20%" align="center">

													<xsl:value-of select="occurences"/>
												</TD>-->
											</xsl:if>
										</xsl:for-each>
									</TR>
								</xsl:for-each>
								<tr class="GridItems01">
									<td width="20%" align="center"> <b>Total</b>
									</td>
<!--									<td width="20%" align="center">&#x20;
									</td>-->
									<td width="20%" align="center">
										<b><xsl:value-of select="sum(//Details[landing_flag=1]/occurences)"/></b>
									</td>
<!--									<td width="20%" align="center">&#x20;
									</td>-->
									<td width="20%" align="center">
										<b><xsl:value-of select="sum(//Details[landing_flag=0]/occurences)"/></b>
									</td>
								</tr>
							</table>
					</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="no" externalpreview="no" url="file:///c:/Development/XSL/_CASE/landing.xml" htmlbaseurl="" outputurl="" processortype="msxml4" useresolver="no" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->