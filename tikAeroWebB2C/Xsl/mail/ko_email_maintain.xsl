<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				                      xmlns:tikLanguage="tik:Language">
	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; UTF-8" />
				<title>Peach | Peach</title>
				<style>
					html, body {	height:auto;margin: 0;	padding: 0;	background-color: #ffffff;	font: 11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial;height:100%;}
					*html html, body {	margin: 0;	padding: 0;	background-color:#ffffff;	height: 100%;	FONT:11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial;height:100%;}
					IMG {BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; BORDER-BOTTOM: 0px}

					.ImgHeader{ background-color:#72c367; width:860px; height:120px;}
					
					.WrapperBody{ padding:20px 20px 50px 20px; width:720px; min-height:180px; _height:620px; background-color:#FFFFFF;}

					/*Footer*/
					/*.Footer{ background-image: url(../Images/ImgFooter.jpg); width:860px; height:24px; font:11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial; }*/
					.Footer li{ padding-top:13px; list-style:none; color:#FFFFFF; float:left;}
					.EmailBodyText{height:22px; color:#174a7c; padding-bottom:10px;}
					.EmailUser { height:22px; color:#174a7c;}
					.EmailBody{height:22px;color:#174a7c;}
					
					/*********************************/
					.Wrapper { WIDTH:760px; TEXT-ALIGN: left; margin: 0 auto; }
					.mainlogo {margin-bottom: 20px; margin-left: 15px;}
					.emailregistercontent {margin-left: 15px;}
					.emailregisterfooter {background: #b634bb; float: left; width: 100%; color: #FFF;}
					.emailregisterfooter td, .emailregisterfooter td a {color: #FFF; text-decoration: none;}
					.emailregisterfooter .COL1 {width: 50%; margin-left: 15px; float: left; text-decoration: none;}
					.emailregisterfooter .COL2 {float: right; margin-right: 15px; text-align: right;}
					.emailregisterfooter .copyright {float: right; margin-right: 15px;}
					.clear-all {clear: both;}
				</style>
			</head>
			<body>
				<div class="Wrapper">
					<div class="mainlogo">
						<img>
							<xsl:attribute name="src">
								<xsl:value-of select="MailDetail/HeaderURL" />/App_Themes/Default/Images/mainlogo.png
							</xsl:attribute>
						</img>
					</div>
					
					<xsl:if test="MailDetail/Action = 'create'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />&#160;님
							</div>
					
							<p>
								피치항공(<a href="http://www.flypeach.com">www.flypeach.com</a>) 회원으로 가입해주신 고객님께 진심으로감사드립니다!  

							</p>
					
							<p>
								가입하신 아이디와 비밀번호로 예약 확인 및 변경*1, 회원정보 수정 등의 메뉴를 간단히 이용하실 수 있어, 보다 편리하게 인터넷 예약을 하실 수 있습니다. 
							</p>
					
							<p>
								또한 피치항공의 메일링 서비스에 등록하시면, PEACH의 최신 정보는 물론, 각종 이벤트 소식을 빠르고 편리하게 메일로 받아보실 수 있습니다.  
							</p>
					
							<p>
								회원님의 가입정보는 다음과 같습니다. 
							</p>
					
							<p>
								<div>아이디： <span><xsl:value-of select="MailDetail/UserName" /></span></div>
								<div>패스워드： 
									<span>
										<xsl:if test="MailDetail/Password = ''">
											Password
										</xsl:if>
										<xsl:if test="MailDetail/Password != ''">
											<xsl:value-of select="MailDetail/Password" />
										</xsl:if>
									</span>
								</div>
							</p>
					
							<p>
								회원 가입 정보를 수정하시려면, 다음 페이지를 참고해주세요.  
								<br/>
                <a href="http://book.flypeach.com/default.aspx?cl=l&#38;langculture=ko-kr">http://book.flypeach.com/default.aspx?cl=l&#38;langculture=ko-kr</a>
							</p>
					
							<p>
								저희 Peach는 고객님께 안전하고 편리한 서비스를 제공해드리기 위해 언제나 최선의 노력을 다하고 있습니다.  
							</p>
							
							<p>
								*1 운임 종류에 따라 예약 변경이 불가능한 경우도 있습니다. 자세한 내용은 <a href="http://www.flypeach.com">www.flypeach.com</a> 에서 확인해주세요. 

							</p>
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<xsl:if test="MailDetail/Action = 'reset'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="tikLanguage:get('Email_13','Dear')" />&#160; <xsl:value-of select="MailDetail/Title" />&#160;
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />,
							</div>
					
							<p>
								Your password has been successfully reset.
							</p>
					
							<p>
								<div>Username: <span><xsl:value-of select="MailDetail/UserName" /></span></div>
								<div>New Password: 
									<span>
										<xsl:value-of select="MailDetail/Password" />
									</span>
								</div>
							</p>
					
							<p>
								If you'd like to amend your profile details, please visit 
								<br/>
								<a href="http://www.flypeach.com/profile">http://www.flypeach.com/profile</a>
							</p>
					
							<p>
								Sincerely yours,
							</p>
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<xsl:if test="MailDetail/Action = 'change'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="tikLanguage:get('Email_13','Dear')" />&#160; <xsl:value-of select="MailDetail/Title" />&#160;
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />,
							</div>
					
							<p>
								Your password has been successfully changed.
							</p>
					

							<p>
								If you'd like to amend your profile details, please visit 
								<br/>
								<a href="http://www.flypeach.com/profile">http://www.flypeach.com/profile</a>
							</p>
					
							<p>
								Sincerely yours,
							</p>
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<div class="clear-all"></div>
					
					<table class="emailregisterfooter">
						<tr>
							<td class="COL1"><a href="www.flypeach.com/termsandconditions.aspx">이용규약</a> | <a href="www.flypeach.com/privacypolicy.aspx">개인정보 취급 안내</a></td>
							<td class="COL2">Copyright © Peach Aviation Limited</td>
						</tr>
						
					</table>
				</div>

			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>