<?xml version="1.0" encoding="utf-8"?>
<!-- 
Project: InterSky
File name: en_email_details.xsl
-->
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:doc="http://xsltsl.org/xsl/documentation/1.0" 
                xmlns:dt="http://xsltsl.org/date-time" 
                xmlns:str="http://xsltsl.org/string" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
                xmlns:ms="urn:schemas-microsoft-com:xslt" 
                xmlns:CharCode="http://www.tiksystems.com/Printing">
	<!--	<xsl:import href="string.xsl"/>
	<xsl:import href="date-time.xsl"/>
	<xsl:import href="functions.xsl"/>-->
	<xsl:output method="html" indent="yes"/>
	<!--MAIN-->
	<xsl:template match="Booking">
		<xsl:variable name="passenger_id"/>
		<xsl:variable name="booking_segment_id"/>
		<STYLE TYPE="text/css">
			table 						{ border-spacing: 0px; empty-cells: show; margin: 0px; padding: 0px; }
			td								{ font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 8pt; vertical-align: top; }
			th								{ color: #002256; font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 8pt; text-align: center; border-left: 0px; border-right: 0px; border: .09mm solid #002256   }
			.blueline					{ border-bottom: solid #002256 1px; border: .09mm solid #002256 }
			.documentheader	{ color: #002256; font-size: 9pt; font-weight: bold; }
			.documenttotal		{ border-bottom: solid #002256 1px; color: #002256; font-size: 9pt; font-weight: bold; }
			.imglogo					{ border-color: White; border-style: none; vertical-align: top; }
			.pagebreak 				{ page-break-after: always; }
			.tabledetails				{ border-left: solid #002256 0px; border-right: solid #002256 0px; width: 759px;color: #002256; }
			.pagebreak 		 		{ page-break-after: always; }
			.tablereportheader	{ border-left: solid #002256 0px; border-right: solid #002256 0px; border-top: solid #002256 0px; width: 759px; }
			.tdmargin					{ width: 20px;}
			.tdorderheader			{ border: solid 1px #002256;  border-right: 0px;  border-left: 0px;   }
			.tdtotalmargin			{ width: 450px; }
			.tablereportfooter		{ border-bottom: solid #002256 0px; border-left: solid #002256 0px; border-right: solid #002256 0px; bottom: 2px; height: 175px; width: 759px; }
			.imgfooter 				{ left: 10px; border-width: 0 0 0 15px; border-top-style: solid; border-left-style: solid; border-left-color: #FFFFFF;}
			.tdheader					{color: #031668; font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size:13pt; text-align: left; font-weight: bold;  border-left: 0px; border-right: 0px; border-style: none}
			.auto-style2 			{ border: 0px solid #002256 ; color: #002256 ; }
			.auto-style3 			{ border: 1px solid #002256 ; color: #002256 ; font-size: 9pt; }
			.grid-even 				{ background-color: white; border-bottom-style: none; border-left-style: none; border-right: .75pt solid black; border-right-color: #002256; border-top: .75pt solid black; border-top-color: #002256; color: black; font-weight: normal; }
			.grid-main 				{ background-color: white; border-bottom: 0pt solid black; border-bottom-color: white; border-left: .0pt solid black; border-left-color: white; border-right-style: none; border-top: .0pt solid black; border-top-color: white; }
			.grid-top 					{ background-color: white; border-bottom-style: none; border-left-style: none; border-right: .75pt solid black; border-right-color: #002256; border-top-style: none; color: black; font-weight: normal; }
		</STYLE>
		<!--<xsl:call-template name="style"></xsl:call-template>-->
		<xsl:for-each select="/Booking/Passengers/Passenger">
			<!--User MSXML for Processor-->
			<!--xsl:   Cannot find a matching 0-argument function named {urn:schemas-sqlxml-org:vbs}increase()-->
			<!--				Set:<xsl:value-of select="CharCode:increment()"/><br/>
								Return:<xsl:value-of select ="CharCode:getCounter()"/>-->
			<xsl:variable name="p_passenger_id" select="passenger_id"/>
			<xsl:variable name="PositionPassenger" select="position()"/>
			<xsl:variable name="LastPassenger" select="last()"/>
			<xsl:for-each select="/Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
				<xsl:variable name="f_booking_segment_id" select="booking_segment_id"/>
				<xsl:choose>
					<xsl:when test="count(/Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']) =1">
							<xsl:copy-of select="$ReportHeader"/>
							<!--<xsl:call-template name="PageNumber"></xsl:call-template>-->
							<xsl:call-template name="OrderRecipient">
								<xsl:with-param name="PassengerID" select="$p_passenger_id"/>
							</xsl:call-template>
							<xsl:call-template name="Filler">
								<xsl:with-param name="fillercount" select="1"/>
							</xsl:call-template>
							<xsl:call-template name="Segment">
								<xsl:with-param name="PassengerID" select="$p_passenger_id "/>
								<xsl:with-param name="BookingSegmentID" select="$f_booking_segment_id"/>
							</xsl:call-template>
							<xsl:call-template name="Filler">
								<xsl:with-param name="fillercount" select="1"/>
							</xsl:call-template>
							<xsl:call-template name="TicketsDetail">
								<xsl:with-param name="BookingSegmentID" select="$f_booking_segment_id"/>
								<xsl:with-param name="PassengerID" select="$p_passenger_id "/>
							</xsl:call-template>
							<xsl:call-template name="Filler">
								<xsl:with-param name="fillercount" select="25"/>
							</xsl:call-template>
							<xsl:copy-of select="$ReportFooterIntinerary"/>
							<div style="page-break-after:always"></div>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="(position() mod 2) = 0 ">
							<xsl:call-template name="Segment">
								<xsl:with-param name="PassengerID" select="$p_passenger_id"/>
								<xsl:with-param name="BookingSegmentID" select="$f_booking_segment_id"/>
							</xsl:call-template>
							<xsl:call-template name="Filler">
								<xsl:with-param name="fillercount" select="1"/>
							</xsl:call-template>
							<xsl:call-template name="TicketsDetail">
								<xsl:with-param name="BookingSegmentID" select="$f_booking_segment_id"/>
								<xsl:with-param name="PassengerID" select="$p_passenger_id "/>
							</xsl:call-template>
							<xsl:call-template name="Filler">
								<xsl:with-param name="fillercount" select="14"/>
							</xsl:call-template>
							<xsl:copy-of select="$ReportFooterIntinerary"/>
							<xsl:choose>
								<xsl:when test="$LastPassenger != $PositionPassenger">
									<div style="page-break-after:always"></div>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="position() != last()">
											<div style="page-break-after:always"></div>
										</xsl:when>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
						<xsl:if test="(position() mod 2) != 0 ">
							<xsl:copy-of select="$ReportHeader"/>
							<!--<xsl:call-template name="PageNumber"></xsl:call-template>-->
							<xsl:call-template name="OrderRecipient">
								<xsl:with-param name="PassengerID" select="$p_passenger_id"/>
							</xsl:call-template>
							<xsl:call-template name="Filler">
								<xsl:with-param name="fillercount" select="1"/>
							</xsl:call-template>
							<xsl:call-template name="Segment">
								<xsl:with-param name="PassengerID" select="$p_passenger_id "/>
								<xsl:with-param name="BookingSegmentID" select="$f_booking_segment_id"/>
							</xsl:call-template>
							<xsl:call-template name="Filler">
								<xsl:with-param name="fillercount" select="1"/>
							</xsl:call-template>
							<xsl:call-template name="TicketsDetail">
								<xsl:with-param name="BookingSegmentID" select="$f_booking_segment_id"/>
								<xsl:with-param name="PassengerID" select="$p_passenger_id "/>
							</xsl:call-template>
							<xsl:call-template name="Filler">
								<xsl:with-param name="fillercount" select="1"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:for-each>
		<div style="page-break-after:always"></div>
		<xsl:call-template name="ReceiptDetail">
		</xsl:call-template>
	</xsl:template>
	<!--MAIN END-->
	<!--ALL TEMPLATE-->
	<xsl:template name="PageNumber">
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:variable name="v_PageNumber" select="CharCode:getCounter_t()"/>
		<table class="tableReportHeader" cellspacing="0">
			<tr>
				<td class="tdmargin"/>
				<td style="font-size: 8pt;color:#002256 ; font-family: Arial;">
					Page <xsl:value-of select="$v_PageNumber"/>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="PageNumberInvoice">
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:variable name="v_PageNumber" select="CharCode:getCounter_t()"/>
		<table class="tableReportHeader" cellspacing="0">
			<tr>
				<td class="tdmargin"/>
				<td style="font-size: 8pt;color:#002256 ; font-family: Arial;">
					Page <xsl:value-of select="$v_PageNumber"/>

				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="PageNumberInvoice2">
		<xsl:variable name="v_PageNumber" select="CharCode:getPage()"/>
		<table class="tableReportHeader" cellspacing="0">
			<tr>
				<td class="tdmargin"/>
				<td style="font-size: 8pt;color:#002256 ; font-family: Arial;">
					Page <xsl:value-of select="$v_PageNumber"/>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="style">
		<!--<link type="text/css" rel="stylesheet" href="D:\Development\xsl_html_pagebreak\StyleSheet.css"/>-->
		<!--Do not  use "Import" or "Include" for StyleSheet (CSS) because it error on EXE, but Web work fine. -->
		<STYLE TYPE="text/css">
			table 						{ border-spacing: 0px; empty-cells: show; margin: 0px; padding: 0px; }
			td								{ font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 8pt; vertical-align: top; }
			th								{ color: #002256; font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 8pt; text-align: center; border-left: 0px; border-right: 0px; border: .09mm solid #002256   }
			.blueline					{ border-bottom: solid #002256 1px; border: .09mm solid #002256 }
			.documentheader	{ color: #002256; font-size: 9pt; font-weight: bold; }
			.documenttotal		{ border-bottom: solid #002256 1px; color: #002256; font-size: 9pt; font-weight: bold; }
			.imglogo					{ border-color: White; border-style: none; vertical-align: top; }
			.pagebreak 				{ page-break-after: always; }
			.tabledetails				{ border-left: solid #002256 0px; border-right: solid #002256 0px; width: 759px;color: #002256; }
			.pagebreak 		 		{ page-break-after: always; }
			.tablereportheader	{ border-left: solid #002256 0px; border-right: solid #002256 0px; border-top: solid #002256 0px; width: 759px; }
			.tdmargin					{ width: 20px;}
			.tdorderheader			{ border: solid 1px #002256;  border-right: 0px;  border-left: 0px;   }
			.tdtotalmargin			{ width: 450px; }
			.tablereportfooter		{ border-bottom: solid #002256 0px; border-left: solid #002256 0px; border-right: solid #002256 0px; bottom: 2px; height: 175px; width: 759px; }
			.imgfooter 				{ left: 10px; border-width: 0 0 0 15px; border-top-style: solid; border-left-style: solid; border-left-color: #FFFFFF;}
			.tdheader					{color: #031668; font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size:13pt; text-align: left; font-weight: bold;  border-left: 0px; border-right: 0px; border-style: none}
			.auto-style2 			{ border: 0px solid #002256 ; color: #002256 ; }
			.auto-style3 			{ border: 1px solid #002256 ; color: #002256 ; font-size: 9pt; }
			.grid-even 				{ background-color: white; border-bottom-style: none; border-left-style: none; border-right: .75pt solid black; border-right-color: #002256; border-top: .75pt solid black; border-top-color: #002256; color: black; font-weight: normal; }
			.grid-main 				{ background-color: white; border-bottom: 0pt solid black; border-bottom-color: #FF0000; border-left: .0pt solid black; border-left-color: #FF0000; border-right-style: none; border-top: .0pt solid black; border-top-color: #FF0000; }
			.grid-top 					{ background-color: white; border-bottom-style: none; border-left-style: none; border-right: .75pt solid black; border-right-color: #002256; border-top-style: none; color: black; font-weight: normal; }
		</STYLE>
	</xsl:template>
	<xsl:template name="Filler">
		<xsl:param name="fillercount" select="1"/>
		<xsl:if test="$fillercount &gt; 0">
			<!--<table class="tabledetails">-->
			<table class="tabledetails" cellspacing="0" style="table-layout:fixed;">
				<tr>
					<!--<td class="blueline">&#xA0;</td>-->
					<td style=" background-color: white;">&#xA0;</td>
				</tr>
			</table>
			<xsl:call-template name="Filler">
				<xsl:with-param name="fillercount" select="$fillercount - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="FillerPay">
		<xsl:param name="fillercountPay" select="1"/>
		<xsl:param name="Firstfillercount" select="0"/>
		<xsl:if test="$fillercountPay &gt; 0">
			<xsl:if test="($Firstfillercount = 0) and ($fillercountPay &gt; 1)">
				<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
					<TD class="grid-even" style="align=left;background-color: white; border-bottom:1px solid #002256; ">
						&#xA0;
					</TD>
					<TD class="grid-even">
						&#xA0;
					</TD>
					<TD class="grid-even" style="background-color: white; border-bottom:1px solid #002256;border-right: 1pt solid #002256; ">
						&#xA0;
					</TD>
				</TR>
			</xsl:if>
			<xsl:if test="($Firstfillercount  &gt; 0) and ($fillercountPay &gt; 1)">
				<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
					<TD class="grid-even" style="align=left;background-color: white; border-bottom:1px solid #002256; ">
						&#xA0;
					</TD>
					<TD class="grid-even">
						&#xA0;
					</TD>
					<TD class="grid-even" style="background-color: white; border-bottom:1px solid #002256;border-right: 1pt solid #002256; ">
						&#xA0;
					</TD>
				</TR>
			</xsl:if>
			<xsl:if test="$fillercountPay = 1 ">
				<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
					<TD class="grid-even" style="align=left;background-color: white; border-bottom: 0px solid #002256;">
						&#xA0;
					</TD>
					<TD class="grid-even" style="background-color: white; border-bottom: 0px solid #002256;">
						&#xA0;
					</TD>
					<TD class="grid-even" style="background-color: white; border-bottom: 0px solid #002256;border-right: 1pt solid #002256; ">
						&#xA0;
					</TD>
				</TR>
			</xsl:if>
			<xsl:call-template name="FillerPay">
				<xsl:with-param name="fillercountPay" select="$fillercountPay - 1"/>
				<xsl:with-param name="Firstfillercount" select="$Firstfillercount + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="OrderRecipient">
		<xsl:param name="PassengerID"/>
		<table class="tabledetails">
			<tr>
				<td>
					<table class="tabledetails" cellspacing="0" style="width: 580px;">
						<tr>
							<td style="width: 15px"/>
							<td class="documentheader" style="width: 110px">Passenger Name:&#xA0;</td>
							<td class="documentheader" style="width: 390px">
								<strong>
									<xsl:value-of select="/Booking/Passengers/Passenger[passenger_id = $PassengerID]/lastname"/>&#xA0;/&#xA0;<xsl:value-of select="/Booking/Passengers/Passenger[passenger_id = $PassengerID]/firstname"/>&#xA0;<xsl:value-of select="/Booking/Passengers/Passenger[passenger_id = $PassengerID]/title"/>
								</strong>
							</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 12px"/>
							<td class="documentheader">Reservation-Nbr:</td>
							<td class="documentheader">
								<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>&#xA0;</td>
							<td/>
						</tr>
					</table>
					<table class="tabledetails" cellspacing="0" style="width: 580px;color: #002256 ;">
						<tr>
							<td style="width: 15px"/>
							<td>&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>
								<span style="font-size: 8pt;">All times noted are local times.  Please keep this receipt with you throughout your journey.&#xA0;</span>
							</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>
								<span style="font-size: 8pt;">Your  InterSky ticket is stored  in our reservation system. Find fare rules at the bottom of this page&#xA0;</span>
							</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>
								<span style="font-size: 8pt;">and payment conditions overleaf on page 2.&#xA0;</span>
							</td>
							<td class="tdmargin"/>
						</tr>
					</table>
				</td>
				<td style="font-size: 9pt;color: #002256 ;" align="right" width="164">InterSky Call-Center <br/>Intl: +43 5574 48800 46 <br/>DE: +49 7541 286 96 84 <br/>AT: +43 5574 48800 46 <br/>CH: +41 31 819 72 22 <br/>
					reservation@intersky.biz
					<br/>Mon - Fri: 08:00 - 18:00</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="Segment">
		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>
		<xsl:variable name="BarURL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
		<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>
		<!--SegmentHeader-->
		<table class="tableReportHeader" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<td class="tdheader" style="font-size: 13pt;font-weight: bold;width: 567px;">
					<xsl:value-of select="position()"/>
					<xsl:text>) From</xsl:text>&#xA0;
					<xsl:value-of select="origin_name"/>&#xA0;
										(<xsl:value-of select="origin_rcd"/>) 	to
					<xsl:value-of select="destination_name"/>&#xA0;
										(<xsl:value-of select="destination_rcd"/>)</td>
				<td class="tdheader" style="text-align: right;font-size: 13pt;font-weight: bold;">Flight&#xA0;<xsl:value-of select="airline_rcd"/>
					<xsl:value-of select="flight_number"/>
				</td>
				<td class="tdmargin"/>
			</tr>
		</table>
		<!--SegmentDetail-->
		<xsl:variable name="SegmentDetail" select="8"/>
		<table class="tableReportHeader" cellspacing="0" style="table-layout:fixed;">
			<tr>
				<td class="tdmargin" style="height: 22px;"/>
				<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 2px; width: 106px">Departure Date</td>
				<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1px; width: 106px">Latest Check-In</td>
				<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1px; width: 106px">Departure</td>
				<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1px; width: 106px">Arrival</td>
				<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1px; width: 103px">Class of Travel</td>
				<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1px; width: 62px">Status</td>
				<td class="auto-style3" style="font-weight: bold;border-width: 2px 2px 1px 1px; width: 130px">Baggage Allowance</td>
				<!--<td class="tdmargin"/>-->
			</tr>
			<tr>
				<td class="tdmargin" style="height: 22px;"/>
				<td class="auto-style3" style="border-width: 1px 1px 1px 2px">
					<xsl:if test="string-length(departure_date) &gt; 0">
						<xsl:call-template name="dt:ddmmmyyyy-en">
							<xsl:with-param name="date" select="departure_date"/>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="string-length(departure_date) = 0">
						<xsl:text>OPEN</xsl:text>
					</xsl:if>
				</td>
				<td class="auto-style3">
					<xsl:if test="string(check_in_date_time) != '0'">
						<xsl:value-of select="substring(check_in_date_time, 10,5)"/>
					</xsl:if>&#xA0;
				</td>
				<td class="auto-style3">
					<xsl:if test="string(departure_time) != '0'">
						<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/>&#xA0;Terminal:&#xA0;<xsl:value-of select="origin_terminal"/>
					</xsl:if>
				</td>
				<td class="auto-style3">
					<xsl:if test="string(planned_arrival_time) != '0'">
						<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/>&#xA0;Terminal:&#xA0;<xsl:value-of select="destination_terminal"/>
					</xsl:if>
				</td>
				<td class="auto-style3">
					<xsl:call-template name="BoardingClass">
						<xsl:with-param name="PassengerID" select="$PassengerID"/>
						<xsl:with-param name="BookingSegmentID" select="$BookingSegmentID"/>
					</xsl:call-template>
				</td>
				<td class="auto-style3" style="width: 98px">
					<xsl:value-of select="status_name"/>
				</td>
				<td class="auto-style3" style="border-width: 1px 2px 1px 1px">
					<xsl:value-of select="/Booking/Tickets/Ticket[position()&lt;=1]/baggage_weight "/>&#xA0;Kgs.</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin" style="height: 20px;"/>
				<td class="auto-style3" style="font-weight: bold; border-width: 1px 1px 1px 2px" colspan="2">Operated by</td>
				<td class="auto-style3" style="font-weight: bold;">On behalf of</td>
				<td class="auto-style3" style="font-weight: bold;">Not valid before</td>
				<td class="auto-style3" style="font-weight: bold;">Not valid after</td>
				<td class="auto-style3" style="font-weight: bold; border-width: 1px 2px 2px 1px;" colspan="2" rowspan="2">
					<div class="Barcode" style="height: 19px">
						<table width="100%" border="0">
							<tbody>
								<tr>
									<td align="center" width="100%">
										<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="164" height="35"/>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin" style="height: 22px;"/>
				<td class="auto-style3" style="border-width: 1px 1px 2px 2px;" colspan="2">
					<span style="font-size: 8pt;">
						<xsl:value-of select="airline_name"/>&#xA0;</span>
				</td>
				<td class="auto-style3" style="border-width: 1px 1px 2px 1px;">&#xA0;</td>
				<td class="auto-style3" style="border-width: 1px 1px 2px 1px;">
					<xsl:if test="string-length(/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')] [(booking_segment_id = $BookingSegmentID)] [(position() &lt;= 1)]/not_valid_before_date) &gt; 0">
						<xsl:call-template name="dt:ddmmmyyyy-en">
							<xsl:with-param name="date" select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')] [(booking_segment_id = $BookingSegmentID)] [(position() &lt;= 1)]/not_valid_before_date"/>
						</xsl:call-template>
					</xsl:if>&#xA0;</td>
				<td class="auto-style3" style="border-width: 1px 1px 2px 1px;">
					<xsl:if test="string-length(/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')] [(booking_segment_id = $BookingSegmentID)] [(position() &lt;= 1)]/not_valid_after_date) &gt; 0">
						<xsl:call-template name="dt:ddmmmyyyy-en">
							<xsl:with-param name="date" select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')] [(booking_segment_id = $BookingSegmentID)] [(position() &lt;= 1)]/not_valid_after_date"/>
						</xsl:call-template>
					</xsl:if>&#xA0;</td>
				<td class="tdmargin"/>
			</tr>
		</table>
		<xsl:copy-of select="$Information"/>
	</xsl:template>
	<xsl:template name="PassengerDetail">
		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>
		<xsl:param name="PositionID"/>
		<xsl:param name="TicketNumber"/>
		<xsl:param name="SeatNumber"/>
		<TR>
			<td class="tdmargin"/>
			<TD class="blankline">
				<xsl:value-of select="$PositionID"/>. Ticket No:<xsl:value-of select="$TicketNumber"/>&#xA0;/&#xA0;Seat No:<xsl:value-of select="$SeatNumber"/>
			</TD>
			<TD class="blankline">
				<xsl:call-template name="SpecialServices">
					<xsl:with-param name="PassengerID" select="$PassengerID"/>
					<xsl:with-param name="BookingSegmentID" select="$BookingSegmentID"/>
				</xsl:call-template>
			</TD>
		</TR>
	</xsl:template>
	<xsl:template name="SpecialServices">
		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>
		<table cellpadding="0" cellspacing="0" style="width: 100%;color: #002256 ;">
			<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')][(passenger_id = $PassengerID)][(booking_segment_id = $BookingSegmentID)]">
				<TR>
					<td>
						<xsl:value-of select="display_name"/>
					</td>
				</TR>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="TicketsDetail">
		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>
		<table class="tabledetails" cellspacing="1" style="table-layout:fixed;color: #002256 ;">
			<tr>
				<td class="tdmargin">&#xA0;</td>
				<td class="blankline" style="width: 106px">Seat:&#xA0;</td>
				<td class="blankline" style="width: 270px">
					<xsl:value-of select="/Booking/Tickets/Ticket[(passenger_id = $PassengerID)][(booking_segment_id = $BookingSegmentID)]/seat_number"/>&#xA0;</td>
				<td class="blankline" style="width: 86px">Fare basis:</td>
				<td class="blankline" style="width: 242px">
					<xsl:value-of select="/Booking/Tickets/Ticket[(passenger_id = $PassengerID)][(booking_segment_id = $BookingSegmentID)]/fare_code"/>&#xA0;</td>
				<td class="tdmargin">&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin">&#xA0;</td>
				<td class="blankline" style="width: 106px">Ticket:&#xA0;</td>
				<td class="blankline">
					<xsl:value-of select="/Booking/Tickets/Ticket[(passenger_id = $PassengerID)][(booking_segment_id = $BookingSegmentID)]/ticket_number"/>&#xA0;</td>
				<td class="blankline">Special Service:</td>
				<td class="blankline" rowspan="3">
					<xsl:call-template name="SpecialServices">
						<xsl:with-param name="PassengerID" select="$PassengerID"/>
						<xsl:with-param name="BookingSegmentID" select="$BookingSegmentID"/>
					</xsl:call-template>
				</td>
				<td class="tdmargin">&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin">&#xA0;</td>
				<td class="blankline">Endorsement:&#xA0;</td>
				<td class="blankline">
					<xsl:value-of select="/Booking/Tickets/Ticket[(passenger_id = $PassengerID)][(booking_segment_id = $BookingSegmentID)]/endorsement_text"/>&#xA0;</td>
				<td class="blankline">&#xA0;</td>
				<td class="tdmargin">&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin">&#xA0;</td>
				<td class="blankline">Restrictions:&#xA0;</td>
				<td class="blankline">
					<xsl:value-of select="/Booking/Tickets/Ticket[(passenger_id = $PassengerID)][(booking_segment_id = $BookingSegmentID)]/restriction_text"/>&#xA0;</td>
				<td class="blankline">&#xA0;</td>
				<td class="tdmargin">&#xA0;</td>
			</tr>
		</table>
	</xsl:template>
	<!--ReceiptDetail-->
	<xsl:template name="ReceiptDetail">
		<xsl:copy-of select="$R_ReportHeader"/>
		<!--<xsl:call-template name="PageNumberInvoice"></xsl:call-template>-->
		<xsl:value-of select="CharCode:SetPage()"/>
		<xsl:copy-of select="$R_ReportHeaderInvoice"/>
		<xsl:copy-of select="$R_OrderRecipient"/>
		<xsl:copy-of select="$R_OrderRowsHeader"/>
		<xsl:for-each select="/Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
			<xsl:sort select="departure_date" order="ascending"/>
			<xsl:variable name="r_booking_segment_id" select="booking_segment_id"/>
			<xsl:variable name="Description" select="concat(origin_rcd,' - ', destination_rcd)"/>
			<xsl:variable name="FlightNo" select="concat(airline_rcd,'', flight_number)"/>
			<xsl:variable name="FlightDate" select="departure_date"/>
			<xsl:for-each select="/Booking/Tickets/Ticket[(booking_segment_id) = ($r_booking_segment_id)]">
				<xsl:variable name="r_passenger_id" select="passenger_id"/>
				<xsl:variable name="r_firstname" select="/Booking/Passengers/Passenger[passenger_id = $r_passenger_id]/firstname"/>
				<xsl:variable name="r_lastname" select="/Booking/Passengers/Passenger[passenger_id = $r_passenger_id]/lastname"/>
				<xsl:variable name="r_title" select="/Booking/Passengers/Passenger[passenger_id = $r_passenger_id]/title"/>
				<xsl:variable name="PassengerName" select="concat(($r_lastname),' / ',($r_firstname), ' ',($r_title))"/>
				<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:call-template name="R_TicketDetail">
						<xsl:with-param name="PassengerID" select="$r_passenger_id"/>
						<xsl:with-param name="BookingSegmentID" select="$r_booking_segment_id"/>
						<xsl:with-param name="Description" select="$Description"/>
						<xsl:with-param name="FlightNo" select="$FlightNo"/>
						<xsl:with-param name="FlightDate" select="$FlightDate"/>
						<xsl:with-param name="PassengerName" select="$PassengerName"/>
					</xsl:call-template>
					<xsl:call-template name="CheckPageBreak">
						<xsl:with-param name="RowCount" select="CharCode:getCounter()"/>
					</xsl:call-template>
					<xsl:for-each select="/Booking/TicketTaxes/Tax[(booking_segment_id) = ($r_booking_segment_id)][(passenger_id = $r_passenger_id)]">
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:call-template name="R_TaxDetail">
							<xsl:with-param name="PassengerID" select="$r_passenger_id"/>
							<xsl:with-param name="BookingSegmentID" select="$r_booking_segment_id"/>
							<xsl:with-param name="Description" select="$Description"/>
							<xsl:with-param name="FlightNo" select="$FlightNo"/>
							<xsl:with-param name="FlightDate" select="$FlightDate"/>
							<xsl:with-param name="PassengerName" select="$PassengerName"/>
						</xsl:call-template>
						<xsl:call-template name="CheckPageBreak">
							<xsl:with-param name="RowCount" select="CharCode:getCounter()"/>
						</xsl:call-template>
					</xsl:for-each>
					<xsl:for-each select="/Booking/Fees/Fee[void_date_time =''][(booking_segment_id) = ($r_booking_segment_id)][(passenger_id = $r_passenger_id)]">
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:call-template name="R_FeeDetail">
							<xsl:with-param name="PassengerID" select="$r_passenger_id"/>
							<xsl:with-param name="BookingSegmentID" select="$r_booking_segment_id"/>
							<xsl:with-param name="Description" select="$Description"/>
							<xsl:with-param name="FlightNo" select="$FlightNo"/>
							<xsl:with-param name="FlightDate" select="$FlightDate"/>
							<xsl:with-param name="PassengerName" select="$PassengerName"/>
						</xsl:call-template>
						<xsl:call-template name="CheckPageBreak">
							<xsl:with-param name="RowCount" select="CharCode:getCounter()"/>
						</xsl:call-template>
					</xsl:for-each>
				</table>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:for-each select="/Booking/Itinerary/FlightSegment[segment_status_rcd = 'XX']">
			<xsl:sort select="departure_date" order="ascending"/>
			<xsl:variable name="r_booking_segment_id" select="booking_segment_id"/>
			<xsl:variable name="Description" select="concat(origin_rcd,' - ', destination_rcd)"/>
			<xsl:variable name="FlightNo" select="concat(airline_rcd,'', flight_number)"/>
			<xsl:variable name="FlightDate" select="departure_date"/>
			<xsl:for-each select="/Booking/Tickets/Ticket[(booking_segment_id) = ($r_booking_segment_id)]">
				<xsl:variable name="r_passenger_id" select="passenger_id"/>
				<xsl:variable name="r_firstname" select="/Booking/Passengers/Passenger[passenger_id = $r_passenger_id]/firstname"/>
				<xsl:variable name="r_lastname" select="/Booking/Passengers/Passenger[passenger_id = $r_passenger_id]/lastname"/>
				<xsl:variable name="r_title" select="/Booking/Passengers/Passenger[passenger_id = $r_passenger_id]/title"/>
				<xsl:variable name="PassengerName" select="concat(($r_lastname),' / ',($r_firstname), ' ',($r_title))"/>
				<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
					<xsl:for-each select="/Booking/Fees/Fee[void_date_time =''][(booking_segment_id) = ($r_booking_segment_id)][(passenger_id = $r_passenger_id)]">
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:call-template name="R_FeeDetail">
							<xsl:with-param name="PassengerID" select="$r_passenger_id"/>
							<xsl:with-param name="BookingSegmentID" select="$r_booking_segment_id"/>
							<xsl:with-param name="Description" select="$Description"/>
							<xsl:with-param name="FlightNo" select="$FlightNo"/>
							<xsl:with-param name="FlightDate" select="$FlightDate"/>
							<xsl:with-param name="PassengerName" select="$PassengerName"/>
						</xsl:call-template>
						<xsl:call-template name="CheckPageBreak">
							<xsl:with-param name="RowCount" select="CharCode:getCounter()"/>
						</xsl:call-template>
					</xsl:for-each>
				</table>
			</xsl:for-each>
		</xsl:for-each>

		<xsl:if test="(
			(/Booking/Fees/Fee[void_date_time =''][passenger_id=''][booking_segment_id='']) or 
			(/Booking/Fees/Fee[void_date_time =''][passenger_id='']) or 
			(/Booking/Fees/Fee[void_date_time =''][booking_segment_id=''])) = 'TRUE'">
		
			<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
				<xsl:for-each select="(/Booking/Fees/Fee[void_date_time =''][passenger_id=''][booking_segment_id=''])">
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:call-template name="R_FeeDetail_All">						
					</xsl:call-template>
					<xsl:call-template name="CheckPageBreak">
						<xsl:with-param name="RowCount" select="CharCode:getCounter()"/>
					</xsl:call-template>
				</xsl:for-each>
				<xsl:for-each select="(/Booking/Fees/Fee[void_date_time =''][passenger_id=''][booking_segment_id !=''])">
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:call-template name="R_FeeDetail_All">				
					</xsl:call-template>
					<xsl:call-template name="CheckPageBreak">
						<xsl:with-param name="RowCount" select="CharCode:getCounter()"/>
					</xsl:call-template>
				</xsl:for-each>
				<xsl:for-each select="(/Booking/Fees/Fee[void_date_time =''][booking_segment_id=''][passenger_id !=''])">
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:call-template name="R_FeeDetail_All">						
					</xsl:call-template>
					<xsl:call-template name="CheckPageBreak">
						<xsl:with-param name="RowCount" select="CharCode:getCounter()"/>
					</xsl:call-template>
				</xsl:for-each>
			</table>
		</xsl:if>

		<xsl:choose>
			<!-- case of only one page-->
			<xsl:when test="CharCode:getCounter() &lt; 36">
				<xsl:choose>
					<xsl:when test="CharCode:getCounter()+18 &gt; 40">
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="40 - (CharCode:getCounter()+4)"/>
						</xsl:call-template>
						<xsl:copy-of select="$R_ReportFooter"/>
						<div style="page-break-after:always"></div>
						<xsl:copy-of select="$R_ReportHeader"/>
						<xsl:value-of select="CharCode:increment_p()"/>
						<!--<xsl:call-template name="PageNumberInvoice2"></xsl:call-template>-->
						<xsl:copy-of select="$R_ReportHeaderInvoice"/>
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="0"/>
						</xsl:call-template>
						<xsl:copy-of select="$R_OrderRowsHeader"/>
						<xsl:copy-of select="$R_GrandTotal"/>
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="24"/>
						</xsl:call-template>
						<xsl:copy-of select="$R_TermsOfPayment"/>
						<xsl:copy-of select="$R_ReportFooter"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="$R_GrandTotal"/>
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="36 - (CharCode:getCounter()+18)"/> 
						</xsl:call-template>
						<xsl:copy-of select="$R_TermsOfPayment"/>
						<xsl:copy-of select="$R_ReportFooter"/>					
					</xsl:otherwise>
				</xsl:choose>				
			</xsl:when>
			<!-- case of more than one page-->
			<xsl:otherwise>
				<xsl:copy-of select="$R_GrandTotal"/>
				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="1"/>
				</xsl:call-template>
				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="36 - (8+(CharCode:getCounter()+4) mod 36)"/>
				</xsl:call-template>
				<xsl:copy-of select="$R_TermsOfPayment"/>
				<xsl:copy-of select="$R_ReportFooter"/>
			</xsl:otherwise>
		</xsl:choose>
		<!--End Filler -->
		<!--		</xsl:for-each>-->
	</xsl:template>
	<xsl:template name="R_TicketDetail">
		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>
		<xsl:param name="Description"/>
		<xsl:param name="FlightNo"/>
		<xsl:param name="FlightDate"/>
		<xsl:param name="PassengerName"/>
		<table class="grid-main" cellspacing="0" style="table-layout:fixed; border-collapse: collapse;" >
			<tr>
				<td class="tdmargin"  style="background-color: white; border-right:1px solid #002256;" />
				<td style="width:81px" align="left" class="grid-even">
					<xsl:value-of select="$Description"/>&#xA0;
					</td>
				<td class="grid-even" style="width:60px">
					<xsl:value-of select="$FlightNo"/>&#xA0;
					</td>
				<td style="width:76px" align="left" class="grid-even">
					<xsl:call-template name="dt:ddmmmyyyy-en">
						<xsl:with-param name="date" select="$FlightDate"/>
					</xsl:call-template>&#xA0;
					</td>
				<td style="width:200px" align="left" class="grid-even">
					<xsl:value-of select="$PassengerName"/>&#xA0;
					</td>
				<td style="width:146px" align="left" class="grid-even">
					<xsl:if test="(fare_type_rcd = 'FARE')">
						<xsl:text>Ticket</xsl:text>&#xA0;
							(<xsl:value-of select="booking_class_rcd"/>)
						</xsl:if>
					<xsl:if test="not(fare_type_rcd = 'FARE')">
						<!--<xsl:value-of select="fee_rcd"/>&#xA0;-->
					</xsl:if>
						&#xA0;
					</td>
				<td style="width:78px" align="right" class="grid-even">
					<xsl:if test="number(fare_amount_incl - fare_amount) != 0">
						<xsl:if test="number(fare_amount_incl - fare_amount) &gt;= 0">
							<xsl:value-of select="format-number(fare_amount_incl - fare_amount,'###,##0.00')"/>
						</xsl:if>
					</xsl:if>&#xA0;
					</td>
				<td style="width:78px" align="right" class="grid-even">
					<xsl:value-of select="concat('  ', format-number(fare_amount,'#,##0.00'))"/>&#xA0;
					</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="R_TaxDetail">
		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>
		<xsl:param name="Description"/>
		<xsl:param name="FlightNo"/>
		<xsl:param name="FlightDate"/>
		<xsl:param name="PassengerName"/>
		<table class="grid-main" cellspacing="0" style="table-layout:fixed; border-collapse: collapse;" >
			<tr>
				<td class="tdmargin"  style="background-color: white; border-right:1px solid #002256;" />
				<td style="width:81px" align="left" class="grid-even">
					<xsl:value-of select="$Description"/>&#xA0;
					</td>
				<td class="grid-even" style="width:60px">
					<xsl:value-of select="$FlightNo"/>&#xA0;
					</td>
				<td style="width:76px" align="left" class="grid-even">
					<xsl:call-template name="dt:ddmmmyyyy-en">
						<xsl:with-param name="date" select="$FlightDate"/>
					</xsl:call-template>&#xA0;
					</td>
				<td style="width:200px" align="left" class="grid-even">
					<xsl:value-of select="$PassengerName"/>&#xA0;
					</td>
				<td style="width:146px" align="left" class="grid-even">
					<xsl:value-of select="tax_rcd"/>&#xA0;
					</td>
				<td style="width:78px" align="right" class="grid-even">
					<xsl:if test="number(tax_amount_incl - tax_amount) != 0">
						<xsl:if test="number(tax_amount_incl - tax_amount) &gt;= 0">
							<xsl:value-of select="format-number(tax_amount_incl - tax_amount,'###,##0.00')"/>
						</xsl:if>
					</xsl:if>&#xA0;
					</td>
				<td style="width:78px" align="right" class="grid-even">
					<xsl:value-of select="concat('  ', format-number(tax_amount,'#,##0.00'))"/>&#xA0;
					</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="R_FeeDetail">
		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>
		<xsl:param name="Description"/>
		<xsl:param name="FlightNo"/>
		<xsl:param name="FlightDate"/>
		<xsl:param name="PassengerName"/>
		<table class="grid-main" cellspacing="0" style="table-layout:fixed; border-collapse: collapse;" >
			<tr>
				<td class="tdmargin"  style="background-color: white; border-right:1px solid #002256;" />
				<td style="width:81px" align="left" class="grid-even">
					<xsl:value-of select="$Description"/>&#xA0;
					</td>
				<td class="grid-even" style="width:60px">
					<xsl:value-of select="$FlightNo"/>&#xA0;
					</td>
				<td style="width:76px" align="left" class="grid-even">
					<xsl:call-template name="dt:ddmmmyyyy-en">
						<xsl:with-param name="date" select="$FlightDate"/>
					</xsl:call-template>&#xA0;
					</td>
				<td style="width:200px" align="left" class="grid-even">
					<xsl:value-of select="$PassengerName"/>&#xA0;
					</td>
				<td style="width:146px" align="left" class="grid-even">
					<xsl:value-of select="fee_rcd"/>&#xA0;
					</td>
				<td style="width:78px" align="right" class="grid-even">
					<xsl:if test="number(fee_amount_incl - fee_amount) != 0">
						<xsl:if test="number(fee_amount_incl - fee_amount) &gt;= 0">
							<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'###,##0.00')"/>
						</xsl:if>
					</xsl:if>&#xA0;
					</td>
				<td style="width:78px" align="right" class="grid-even">
					<xsl:value-of select="concat('  ', format-number(fee_amount,'#,##0.00'))"/>&#xA0;
					</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="R_FeeDetail_All">
<!--		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>-->
		<xsl:param name="Description"/>
		<table class="grid-main" cellspacing="0" style="table-layout:fixed; border-collapse: collapse;" >
			<tr>
				<td class="tdmargin"  style="background-color: white; border-right:0px solid #002256;" />
				<td style="width:81px;background-color: white; border-right:0px solid #002256; " align="left" class="grid-even" >
					&#xA0;
					</td>
				<td  style="width:60px;background-color: white; border-right:0px solid #002256; " class="grid-even">
					&#xA0;
					</td>
				<td style="width:76px; background-color: white; border-right:0px solid #002256; " align="left" class="grid-even" >
					&#xA0;
					</td>
				<td style="width:200px; " align="left"  class="grid-even">
					&#xA0;
					</td>
				<td style="width:146px; background-color: white; border-bottom:1px solid #002256;" align="left" class="grid-even">
					<xsl:value-of select="fee_rcd"/>&#xA0;(<xsl:value-of select="display_name"/>)
					</td>
				<td style="width:78px; background-color: white; border-bottom:1px solid #002256;" align="right" class="grid-even">
					<xsl:if test="number(fee_amount_incl - fee_amount) != 0">
						<xsl:if test="number(fee_amount_incl - fee_amount) &gt;= 0">
							<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'###,##0.00')"/>
						</xsl:if>
					</xsl:if>&#xA0;
					</td>
				<td style="width:78px; background-color: white; border-bottom:1px solid #002256;" align="right" class="grid-even">
					<xsl:value-of select="concat('  ', format-number(fee_amount,'#,##0.00'))"/>&#xA0;
					</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="CheckPageBreak">
		<xsl:param name="RowCount"/>
		<xsl:if test="($RowCount mod 36) = 0 ">
			<table class="tabledetails" cellspacing="0" style="table-layout:fixed;">
				<tr>
					<td class="tdmargin"/>
					<td style=" background-color: white;border-top:1px solid #002256;">&#xA0;</td>
					<td class="tdmargin"/>
				</tr>
			</table>
			<xsl:copy-of select="$R_ReportFooter"/>
			<div style="page-break-after:always"></div>
			<xsl:copy-of select="$R_ReportHeader"/>
			<!--<xsl:call-template name="PageNumberInvoice2"></xsl:call-template>-->
			<xsl:copy-of select="$R_ReportHeaderInvoice"/>
			<xsl:call-template name="Filler">
				<xsl:with-param name="fillercount" select="0"/>
			</xsl:call-template>
			<xsl:copy-of select="$R_OrderRowsHeader"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="ShowTotalPages">
		<xsl:param name="CurPosition"/>
		<xsl:param name="TotalRowsCount"/>
		<table class="tableReportHeader" cellspacing="0">
			<tr>
				<td class="tdmargin"/>
				<td style="font-size: 8pt;color:#002256; font-family: Arial;">
					<xsl:choose>
						<xsl:when test='($CurPosition mod $TotalRowsCount) != "0"'>
							<xsl:choose>
								<xsl:when test="floor($TotalRowsCount  div 40) &gt; 1">
									<xsl:text>Page 1 of</xsl:text>&#xA0;<xsl:value-of select="round ($TotalRowsCount div 40) +1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Page</xsl:text>&#xA0;<xsl:value-of select="floor($CurPosition div 40) +1"/>&#xA0;of&#xA0;<xsl:value-of select="floor ($TotalRowsCount div 40) +1"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>Page</xsl:text>&#xA0;<xsl:value-of select="floor($CurPosition div 40) +1"/>&#xA0;of&#xA0;<xsl:value-of select="floor ($TotalRowsCount div 40) +1"/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="BoardingClass">
		<xsl:param name="PassengerID"/>
		<xsl:param name="BookingSegmentID"/>
		<xsl:variable name="boarding_rcd" select="//Tickets/Ticket[passenger_id=$PassengerID][booking_segment_id=$BookingSegmentID]/boarding_class_rcd"/>
		<xsl:if test="$boarding_rcd = 'F'">
			First&#xA0;(<xsl:value-of select="booking_class_rcd"/>)&#xA0;
		</xsl:if>
		<xsl:if test="$boarding_rcd = 'C'">
			Business&#xA0;(<xsl:value-of select="booking_class_rcd"/>)&#xA0;
		</xsl:if>
		<xsl:if test="$boarding_rcd = 'Y'">
			Economy&#xA0;(<xsl:value-of select="booking_class_rcd"/>)&#xA0;
		</xsl:if>
	</xsl:template>
	<!--ALL TEMPLATE END-->
	<!-- ALL VARIABLE -->
	<xsl:variable name="ReportHeader">
		<table class="tableReportHeader" cellspacing="0" style="table-layout:fixed; border-collapse: collapse;" >
			<tr>
				<td class="tdmargin"/>
				<td style="font-size: 16pt;color:#002256 ; font-family: Arial;height: 36px; background-color: white; vertical-align: bottom">
					<strong>InterSky Travel Itinerary</strong>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="Information">
		<table class="tabledetails">
			<tr>
				<td class="tdmargin" style="width:13px"/>
				<td class="auto-style2" colspan="4">*Information can be obtained locally</td>
				<td class="auto-style2" colspan="2" align="right">Barcode for check-in</td>
				<td class="tdmargin" style="width:55px"/>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="TermsOfPayment">
		<!--<xsl:for-each select="LineItems/Item">-->
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<td style="width:384px" align="left" class="tdheader" colspan="2">Issuance Details</td>
				<td style="width:10px"/>
				<td style="width:322px" class="tdheader"/>
				<td class="tdmargin"/>
			</tr>
		</table>
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<td class="blueline" style="width:98px" align="left">Issuing Agency:</td>
				<td class="blueline" style="width:241px" align="left">Accounting / 3L</td>
				<td style="width:59px"/>
				<td style="width:328px"/>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td class="blueline">Issuing Place:</td>
				<td class="blueline">InterSky - Bregenz / Austria</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td class="blueline">Remarks:</td>
				<td class="blueline">Testbooking</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="ReportFooterIntinerary">
		<table class="tableReportFooter">
			<tr>
				<td class="imgfooter">
					<table>
						<tr>
							<td width="759" height="174" align="left" valign="top" style="background-repeat:no-repeat;font-size: 7pt; text-align: justify;border-top: solid #B9CAEB 0px;color: #002256;">
									<div style="width: 441px; height: 61px; z-index: 1; position: absolute;">
									Thank you for choosing InterSky. <br/>
									InterSky Condition of Carriage:<a target="_blank" href="http://www.intersky.biz/bedingungen"><span style="color: #002256;text-decoration: none;">&#xA0;www.intersky.biz/bedingungen</span></a><br/>
									InterSky Contact:<a target="_blank" href="http://www.intersky.biz/kontakt"><span style="color: #002256;text-decoration: none;">&#xA0;www.intersky.biz/kontakt</span></a><br/>
									</div>
									<img hspace="0" src="http://www.tikaero.com/xslimages/isk/ISK_footer.gif"  width="716" height="161"/>  
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="R_ReportHeader">
		<table class="tableReportHeader" cellspacing="0" style="table-layout:fixed; border-collapse: collapse;" >
			<tr>
				<td class="tdmargin"/>
				<td style="font-size: 16pt;color:#002256 ; font-family: Arial;height: 36px; background-color: white; vertical-align: bottom">
					<strong>
						InterSky Payment Receipt
					</strong>
				</td>
			</tr>
		</table>
	</xsl:variable >
	<xsl:variable name="R_ReportHeaderInvoice">	
		<table class="tabledetails" cellspacing="0" style="width: 580px;">
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>Bregenz (Austria)&#xA0;<span class="tableheader" style="padding-left: 0px;">
						<xsl:call-template name="dt:ddmmmmyyyy-en">
							<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
						</xsl:call-template>
					</span>
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>
					<strong>Invoice-Nbr:&#xA0;								
					<span class="tableheader" style="padding-left: 0px;">
							<xsl:value-of select="/Booking/Payments/Payment[position() = last()]/payment_number"/>&#xA0;
					</span>
					</strong>
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>
					<strong>Reservation-Nbr:&#xA0;								
					<span class="tableheader" style="padding-left: 0px;">
							<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>&#xA0;
					</span>
					</strong>
				</td>
				<td class="tdmargin"/>
			</tr>
		</table>
		<br/>
	</xsl:variable>
	<xsl:variable name="R_OrderRecipient">
		<table class="tabledetails">
			<tr>
				<td>
					<table class="tabledetails" cellspacing="0" style="width: 580px; height: 85px;">
						<tr>
							<td style="width: 15px"/>
							<td class="documentheader" style="width: 227px">Recipient&#xA0;</td>
							<td class="documentheader" style="width: 172px">
								<xsl:if test="number(/Booking/Header/BookingHeader/own_agency_flag) !=1"> 
									<xsl:choose>
										<xsl:when  test="string-length(/Booking/Header/BookingHeader/legal_name)  &gt; 0"> 
												<xsl:text>Agent/Broker</xsl:text>
										</xsl:when>
										<xsl:otherwise>
												<xsl:text>Agent/Broker</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>&#xA0;
				  			</td>
							<td class="documentheader" style="width: 120px">Sender/Airline&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td style="width: 227px" rowspan="5">
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/title_rcd"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/firstname"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/lastname"/>
									</xsl:call-template>
									<br/>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/address_line1"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/address_line2"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/po_box"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/street"/>
									</xsl:call-template>
									<br/>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/country_rcd"/>
									</xsl:call-template>&#xA0;-&#xA0;
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/zip_code"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/city"/>
									</xsl:call-template>
									<xsl:if test="string-length(/Booking/Header/BookingHeader/booking_tax_id)  &gt; 0"> 
										<br/>VAT-Nbr.:&#xA0;
										<xsl:call-template name="AddrFormat">
											<xsl:with-param name="value" select="/Booking/Header/BookingHeader/booking_tax_id"/>
										</xsl:call-template>
									</xsl:if>
							</td>	             
							<td  rowspan="5">
								<xsl:if  test="number(/Booking/Header/BookingHeader/own_agency_flag) !=1"> 
									<xsl:choose>
										<xsl:when  test="string-length(/Booking/Header/BookingHeader/legal_name)  &gt; 0"> 
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="/Booking/Header/BookingHeader/legal_name"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
												<xsl:call-template name="AddrFormat">
													<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_name"/>
												</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
									<br/>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_address_line1"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_address_line2"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_po_box"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_street"/>
									</xsl:call-template>
									<br/>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_country_rcd"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_zip_code"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_city"/>
									</xsl:call-template>
									<xsl:if test="string-length(/Booking/Header/BookingHeader/agency_tax_id)  &gt; 0"> 
										<br/>VAT-Nbr.:&#xA0;
										<xsl:call-template name="AddrFormat">
											<xsl:with-param name="value" select="/Booking/Header/BookingHeader/agency_tax_id"/>
										</xsl:call-template>
									</xsl:if>
								</xsl:if>
							 </td>
							<td>InterSky Luftfahrt GmbH</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>Bahnhofstrasse 10</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>6900 Bregenz</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>Austria</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>VAT Nbr.:&#xA0;ATU 53122909</td>
							<td class="tdmargin"/>
						</tr>
					</table>
				</td>
				<td style="font-size: 9pt;" align="right" width="164">
					InterSky Call-Center <br/>Intl: +43 5574 48800 46 <br/>DE: +49 7541 286 96 84 <br/>AT: +43 5574 48800 46 <br/>CH: +41 31 819 72 22 <br/>
					reservation@intersky.biz
					<br/>Mo - Fr: 08:00 - 18:00 Uhr</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="R_GrandTotal">
		<!--<xsl:for-each select="LineItems/Item">-->
		<table class="grid-main" cellspacing="0" style="table-layout:fixed; border-collapse: collapse;" >
			<tr>
				<td class="tdmargin" />
				<td style="width:81px; border-top:1px solid #002256;"/>
				<td style="width:60px; border-top:1px solid #002256;"/>
				<td style="width:76px; border-top:1px solid #002256;"/>
				<td style="width:200px; border-top:1px solid #002256;background-color: white; border-right:1px solid #002256;" />
				<td style="width:146px" align="left" class="grid-even">Net Total</td>
				<td style="width:78px" align="right" class="grid-even">&#xA0;</td>
				<td style="width:78px" align="right" class="grid-even">
					<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
					<xsl:variable name="Ticket_total" select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) 
																						- sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) 
																						+ sum(//Fees/Fee[void_date_time = '']/fee_amount)"/>
					<xsl:value-of select="concat('  ', format-number($Ticket_total,'#,##0.00'))"/>&#xA0;
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td style="width:81px"/>
				<td style="width:60px" align="left"/>
				<td style="width:76px" align="left" />
				<td style="width:200px;background-color: white; border-right:1px solid #002256;" />
				<td style="width:146px" align="left" class="grid-even">VAT19%</td>
				<td style="width:78px" align="right" class="grid-even">&#xA0;</td>
				<td style="width:78px" align="right" class="grid-even">
					<xsl:variable name="sum_tax_amount" select="sum(/Booking/TicketQuotes/Total/tax_amount)"/>
					<xsl:variable name="sum_fee_amount" select="(sum(/Booking/Fees/Fee/fee_amount_incl) - sum(/Booking/Fees/Fee/fee_amount))"/>
					<xsl:value-of select="concat('  ', format-number($sum_tax_amount + $sum_fee_amount,'#,##0.00'))"/>&#xA0;
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td style="width:81px"/>
				<td style="width:60px" align="left" />
				<td style="width:76px" align="left" />
				<td style="width:200px;background-color: white; border-right:1px solid #002256;" />
				<td style="width:146px;background-color: white; border-bottom:1px solid #002256;" align="left" class="grid-even">Grand Total</td>
				<td style="width:78px;background-color: white; border-bottom:1px solid #002256;" align="right" class="grid-even">&#xA0;</td>
				<td style="width:78px;background-color: white; border-bottom:1px solid #002256;" align="right" class="grid-even">
					<xsl:variable name="Ticket_total_incl" select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>
					<xsl:value-of select="concat('  ', format-number($Ticket_total_incl,'#,##0.00'))"/>&#xA0;
				</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="R_TermsOfPayment">
		<!--<xsl:for-each select="LineItems/Item">-->
		<table class="grid-main" cellspacing="0" style="table-layout:fixed; border-collapse: collapse;" >
			<tr>
				<td class="tdmargin"  />
				<td style="width:384px" align="left" class="tdheader" colspan="2">Issuance Details</td>
				<td style="width:10px"/>
				<td style="width:322px" class="tdheader">&#xA0;&#xA0;&#xA0;Payment Details</td>
				<td class="tdmargin"/>
			</tr>
		</table>
		<table class="grid-main" style="table-layout:fixed;" cellspacing="0" >
			<tr>
				<td class="tdmargin"  style="background-color: white; border-right:1px solid #002256;" />
				<td class="grid-even" style="width:98px;background-color: white; border-left:1px solid #002256;" align="left">Issuing Agency:</td>
				<td class="grid-even" style="width:241px" align="left">Accounting / 3L</td>
				<td style="width:70px;background-color: white; border-right:1px solid #002256;"/>
				<td rowspan="10"  style="width: 312px; border: 1px solid #002256; background-color: white;" valign="top" class="grid-even" >
					<table class="grid-main"  style="table-layout:fixed; border-collapse: collapse;" cellspacing="0" >
						<tr>
							<td class="grid-even" style="width:100px; border-top; 0px" align="left">Form of Payment</td>
							<td class="grid-even" style="width:138px; border-top: 0px" align="center">Number</td>
							<td class="grid-even" style="width:76px; border-top: 0px;" align="right">EUR&#xA0;</td>
						</tr>
						<xsl:for-each select="Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)]">
							<xsl:if test="form_of_payment_subtype !=''">
								<TR>
									<TD class="grid-even" style="align=left;background-color: white; border-bottom:1px solid #002256;">
										<xsl:value-of select="form_of_payment_subtype"/>&#xA0;
									</TD>
									<TD class="grid-even" style="align=left; background-color: white; border-bottom:1px solid #002256;">
										<xsl:if test="form_of_payment_rcd ='INV'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='BANK'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='CHEQUE'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='MANUAL'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='VOUCHER'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='CC'">
											<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
										</xsl:if>&#xA0;
									</TD>
									<TD class="grid-even" style="text-align: right; background-color: white; border-bottom:1px solid #002256; ">
										<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;
									</TD>
								</TR>
							</xsl:if>
							<xsl:if test="form_of_payment_subtype =''">
								<TR>
									<TD class="grid-even" style="align=left; background-color: white; border-bottom:1px solid #002256;">
										<xsl:value-of select="form_of_payment_rcd"/>&#xA0;
									</TD>
									<TD class="grid-even" style="align=left; background-color: white; border-bottom:1px solid #002256;">
										<xsl:if test="form_of_payment_rcd ='INV'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='BANK'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='CHEQUE'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='MANUAL'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='VOUCHER'">
											<xsl:value-of select="document_number"/>
										</xsl:if>
										<xsl:if test="form_of_payment_rcd ='CC'">
											<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
										</xsl:if>&#xA0;
									</TD>
									<TD class="grid-even" style="text-align: right;background-color: white; border-bottom:1px solid #002256; border-right: 1px solid #002256;">
										<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;
									</TD>
								</TR>
							</xsl:if>
							<xsl:if test="position() = last()">
								<xsl:call-template name="FillerPay">
									<xsl:with-param name="fillercountPay" select="10 - position()"/>
								</xsl:call-template>
							</xsl:if>
						</xsl:for-each>
					</table>
				</td>
			</tr>
			<tr>
				<td class="tdmargin"  style="background-color: white; border-right:1px solid #002256;" />
				<td class="grid-even" style="background-color: white; border-left:1px solid #002256;" align="left">Issuing Place:
								</td>
				<td class="grid-even"  align="left">
								InterSky - Bregenz / Austria </td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"  style="background-color: white; border-right:1px solid #002256;" />
				<td class="grid-even" style="background-color: white; border-bottom:1px solid #002256; border-left:1px solid #002256;" align="left">Remarks:</td>
				<td class="grid-even" style="background-color: white; border-bottom:1px solid #002256;" align="left">&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="R_ReportFooter">
		<table class="tableReportFooter">
			<tr>
				<td class="imgfooter">
					<table>
						<tr>
							<td width="759" height="174" align="left" valign="top" style="background-repeat:no-repeat;font-size: 7pt; text-align: justify;border-top: solid #B9CAEB 0px;color: #002256;">
									<div style="width: 441px; height: 61px; z-index: 1; position: absolute;">
									International flights are taxfree according to § 9 ABS. 2 Z1 USTG 1999,  fares <br/>
									for domestic flights within Germany are subject to a VAT of 19%.<br/>
										<br/>InterSky Luftfahrt GmbH, Bahnhofstr. 10, A-6900 Bregenz   (AT)<br/>
									Firmenbuch-Nbr. FN 215648 f  | UID Nbr. ATU 53122909<br/>
									Hypo Vorarlberg, Bregenz (Austria) | Kto: 103 716 860 16 | BLZ: 58 000<br/>
									Baden-Württembergische Bank,  Konstanz | Kto: 142 64 25 | BLZ:  600 501 01<br/>
									Gerichtsstand, Bregenz (AT)<br/>
									</div>
									<img hspace="0" src="http://www.tikaero.com/xslimages/isk/ISK_footer.gif" width="716" height="161"/> 
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="R_OrderRowsHeader">
		<table class="tableReportHeader" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td style="width: 15px"/>
				<td style="width:500px;" class="tdheader">Fare Details</td>
				<td class="tdmargin"/>
			</tr>
		</table>
		<table class="grid-main" cellspacing="0" style="table-layout:fixed;border-collapse: collapse;" >
			<tr>
				<td class="tdmargin"  style="background-color: white; border-right:1px solid #002256;" />
				<th class="grid-even" style="background-color: white; border-bottom:1px solid #002256;width:81px; text-align: left;">Description</th>
				<th class="grid-even" style="background-color: white; border-bottom:1px solid #002256; width:60px; text-align: left;">Flight</th>
				<th class="grid-even" style="background-color: white; border-bottom:1px solid #002256;width:76px; text-align: left;">Date</th>
				<th class="grid-even" style="background-color: white; border-bottom:1px solid #002256;width:200px; text-align: left;">Passenger Name</th>
				<th class="grid-even" style="background-color: white; border-bottom:1px solid #002256;width:146px; text-align: left;">Type</th>
				<th class="grid-even" style="background-color: white; border-bottom:1px solid #002256;width:78px">VAT</th>
				<th class="grid-even" style="background-color: white; border-bottom:1px solid #002256;width:78px; text-align: right;">EUR&#xA0;&#xA0;</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>

	<!--ALL VARIABLE END-->
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="no" externalpreview="no" url="file:///c:/EXEs/XML/Itien_Dec27_194752.XML" htmlbaseurl="" outputurl="" processortype="msxmldotnet" useresolver="no" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->