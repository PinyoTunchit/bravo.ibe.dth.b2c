<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="BaseURL">http://www.tikaero.com/XSLImages/PRI</xsl:variable>
	<xsl:variable name="PageTitle">tikAERO BoardingPass</xsl:variable>
	<xsl:variable name="passenger_id" select="Booking/Tickets/Ticket/passenger_id"/>
	<xsl:template name="format-date">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<xsl:value-of select="concat($day, $month, $year)" />
	</xsl:template>
	<xsl:template  name="BarcodeInterleaved2of5">
		<xsl:param name="ticket_number"/>
		<xsl:variable name="IfValid">
			<xsl:call-template name="Interleaved2ofValid">
				<xsl:with-param name="ticket_number" select="ticket_number"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="string-length($IfValid) = 0">
			<xsl:variable name="Number">
				<xsl:call-template name="CheckNumber">
					<xsl:with-param name="ticket_number" select="ticket_number"/>
				</xsl:call-template>
			</xsl:variable>
			<SPAN class="Barcode">
				<img class="11" src="about:blank" width="1" height="30"/>
				<img class="11" src="about:blank" width="1" height="30"/>
				<xsl:call-template name="Interleaved2of5Render">
					<xsl:with-param name="Char1" select="substring($Number,1,1)"/>
					<xsl:with-param name="Char2" select="substring($Number,2,1)"/>
				</xsl:call-template>
				<xsl:call-template name="Interleaved2of5Render">
					<xsl:with-param name="Char1" select="substring($Number,3,1)"/>
					<xsl:with-param name="Char2" select="substring($Number,4,1)"/>
				</xsl:call-template>
				<xsl:call-template name="Interleaved2of5Render">
					<xsl:with-param name="Char1" select="substring($Number,5,1)"/>
					<xsl:with-param name="Char2" select="substring($Number,6,1)"/>
				</xsl:call-template>
				<xsl:call-template name="Interleaved2of5Render">
					<xsl:with-param name="Char1" select="substring($Number,7,1)"/>
					<xsl:with-param name="Char2" select="substring($Number,8,1)"/>
				</xsl:call-template>
				<xsl:call-template name="Interleaved2of5Render">
					<xsl:with-param name="Char1" select="substring($Number,9,1)"/>
					<xsl:with-param name="Char2" select="substring($Number,10,1)"/>
				</xsl:call-template>
				<xsl:call-template name="Interleaved2of5Render">
					<xsl:with-param name="Char1" select="substring($Number,11,1)"/>
					<xsl:with-param name="Char2" select="substring($Number,12,1)"/>
				</xsl:call-template>
				<xsl:call-template name="Interleaved2of5Render">
					<xsl:with-param name="Char1" select="substring($Number,13,1)"/>
					<xsl:with-param name="Char2" select="substring($Number,14,1)"/>
				</xsl:call-template>
				<img class="21" src="about:blank" width="1" height="30"/>
				<img class="1" src="about:blank" width="1" height="30"/>
				<SPAN class="TicketNumber"><xsl:value-of select="ticket_number"/></SPAN>
			</SPAN>
		</xsl:if>
		<xsl:if test="string-length($IfValid) != 0">
			<xsl:variable name="Number">
				<xsl:call-template name="CheckNumber">
					<xsl:with-param name="ticket_number" select="ticket_number"/>
				</xsl:call-template>
			</xsl:variable>
			<SPAN class="Barcode">
				<SPAN style="color: red;" class="TicketNumber">Invalid ticket number</SPAN><br/>
				<SPAN class="TicketNumber"><xsl:value-of select="ticket_number"/></SPAN>
			</SPAN>
		</xsl:if>
	</xsl:template>
	<xsl:template  name="Interleaved2ofValid">
		<xsl:param name="ticket_number"/>
		<xsl:if test="string-length($ticket_number) = 0">
			X
		</xsl:if>
	</xsl:template>
	<xsl:template  name="CheckNumber">
		<xsl:param name="ticket_number"/>
		<xsl:variable name="i" select="round(string-length($ticket_number) div 2)"/>
		<xsl:if test="string-length($ticket_number) div 2 != $i">
		<xsl:value-of select="'0'"/>
		</xsl:if>
		<xsl:value-of select="$ticket_number"/>
	</xsl:template>
	<xsl:template  name="Interleaved2of5Render">
		<xsl:param name="Char1"/>
		<xsl:param name="Char2"/>
		<xsl:variable name="Bar1">
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char1"/>
				<xsl:with-param name="Pos" select="'1'"/>
			</xsl:call-template>
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char2"/>
				<xsl:with-param name="Pos" select="'1'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="Bar2">
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char1"/>
				<xsl:with-param name="Pos" select="'2'"/>
			</xsl:call-template>
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char2"/>
				<xsl:with-param name="Pos" select="'2'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="Bar3">
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char1"/>
				<xsl:with-param name="Pos" select="'3'"/>
			</xsl:call-template>
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char2"/>
				<xsl:with-param name="Pos" select="'3'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="Bar4">
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char1"/>
				<xsl:with-param name="Pos" select="'4'"/>
			</xsl:call-template>
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char2"/>
				<xsl:with-param name="Pos" select="'4'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="Bar5">
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char1"/>
				<xsl:with-param name="Pos" select="'5'"/>
			</xsl:call-template>
			<xsl:call-template name="Interleaved2of5Encoding">
				<xsl:with-param name="Character" select="$Char2"/>
				<xsl:with-param name="Pos" select="'5'"/>
			</xsl:call-template>
		</xsl:variable>
		<img class="B{$Bar1}" src="about:blank" width="1" height="30"/>
		<img class="B{$Bar2}" src="about:blank" width="1" height="30"/>
		<img class="B{$Bar3}" src="about:blank" width="1" height="30"/>
		<img class="B{$Bar4}" src="about:blank" width="1" height="30"/>
		<img class="B{$Bar5}" src="about:blank" width="1" height="30"/>
	</xsl:template>
		<xsl:template  name="Interleaved2of5Encoding">
		<xsl:param name="Character"/>
		<xsl:param name="Pos"/>
		<xsl:choose>
			<xsl:when test="$Character = '0'"><xsl:value-of select="substring('11221',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '1'"><xsl:value-of select="substring('21112',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '2'"><xsl:value-of select="substring('12112',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '3'"><xsl:value-of select="substring('22111',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '4'"><xsl:value-of select="substring('11212',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '5'"><xsl:value-of select="substring('21211',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '6'"><xsl:value-of select="substring('12211',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '7'"><xsl:value-of select="substring('11122',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '8'"><xsl:value-of select="substring('21121',number($Pos),1)"/></xsl:when>
			<xsl:when test="$Character = '9'"><xsl:value-of select="substring('12121',number($Pos),1)"/></xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="/">
		<HTML>
		<HEAD>
			<TITLE><xsl:value-of select="$PageTitle"/></TITLE>
			<STYLE>
				div.PrintHeader
{
	background: url(../images/head_ticket2.jpg) no-repeat top right;
}

a.btmClose{
	background: url(../images/button/b_close_s.gif);
	width: 50px;
	height: 20px;
	vertical-align:middle;
}

a:hover
{
	background-position: 0px 20px
}

a.btmNext
{
	background: url(../images/button/b_next_s.gif);
	width: 50px;
	height: 19px;
	vertical-align:middle;
}
.btmNext
{
	width: 50px;
	height: 19px;
	vertical-align:middle;
}
a.btmPrint
{
	background: url(../images/button/b_print_s.gif);
	width: 50px;
	height: 19px;
	vertical-align:middle;
}

div {
	font-family: Tahoma;
	font-size: 7pt;
}

DIV.TicketPanel
{
	width:211mm;
}
.Action
{
	cursor: hand;	
}
.VoidTicket
{
	font: Tahoma;
	font-size: 30pt;
	font-weight: bolder;
	position: Relative;
	left: 100mm;
	top: 30mm;
}

.Ticket1_Body
{
	clear: left;
	font: Tahoma;
 	position: Relative;
	left: 0mm;
 	width: 210mm;
	height: 80mm;
	z-index:1;
}

.Ticket2_Body{
 	position: Relative;

	left: 0mm;
 	width: 210mm;
	height: 80mm;
	z-index:1;
}

.Audit_Body{
 	position: Relative;

	left: 0mm;
 	width: 210mm;
	height: 80mm;
	z-index:1;

}

.Info{
	position: absolute;
	top: 33mm;
	left: 3mm;
 	width: 47mm;
	height: 35mm;
	z-index:1;

}
.Receipt1_FightInfo{
	position: absolute;
	top: 0mm;
	left: 55mm;
 	width: 75mm;
	height: 62mm;
	z-index:1;

}

.Ticket_Panel1{
	position: absolute;
	top: 1.5mm;
	left: 0mm;
 	width: 75mm;
	height: 14mm;
	z-index:1;

}

.Ticket_Panel2{
	position: absolute;
	top: 14.5mm;
	left: 0mm;
 	width: 32mm;
	height: 20mm;
	z-index:1;

}

.Ticket_Panel3{
	position: absolute;
	top: 14.5mm;
	left: 43mm;
 	width: 32mm;
	height: 20mm;
	z-index:1;

}

.FareInfo{
	position: absolute;
	top: 40mm;
	left: 0mm;
 	width: 35mm;
	height: 20mm;
	z-index:1;
}

.TaxID_Label{
 	font-size: 5pt;
	position: absolute;
	top: 53mm;
	left: 0mm;
 	width: 35mm;
	z-index:1;
}

.TaxID{
	font-size: 5pt;
	position: absolute;
	top: 53mm;
	left: 7mm;
 	width: 35mm;
	z-index:1;
}

.Address{
	font-size: 5pt;
	position: absolute;
	top: 55mm;
	left: 0mm;
 	width: 80mm;
	z-index:1;
}

.Ticket_FightInfo{
	position: absolute;
	top: 0mm;
	left:133mm;
 	width: 75mm;
	height: 62mm;
	z-index:1;

}
.BaggageAllowance_Label{
	position: absolute;
	top: 37.5mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;	
}
.BaggageAllowance{
	position: absolute;
	top: 37.5mm;
	left:27mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
	text-align: right;		
}

.TicketComment{
	font-size: 5.5pt;
	position: absolute;
	top: 49mm;
	left:0mm;
 	width: 75mm;
	height: 62mm;
	z-index:1;
	text-align: left;
}
.Ticket_Audit1{
	position: absolute;
	top: 0mm;
	left: 52mm;
 	width: 75mm;
	height: 62mm;
	z-index:1;

}

.Ticket_Audit2{
	position: absolute;
	top: 0mm;
	left:130mm;
 	width: 75mm;
	height: 62mm;
	z-index:1;

}

.PassengerName_Label{
	position: absolute;
	top: 0mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
	text-align: left;
}
.PassengerName{
	font-size: 7pt;
	font-weight: bold;
	position: absolute;
	top: 0mm;
	left:10mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;
	text-align: left;
}

.From_Label{
	position: absolute;
	top: 4mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.From{
	position: absolute;
	top: 4mm;
	left:10mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;
	text-align: left;
}

.To_Label{
	position: absolute;
	top: 8mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.To{
	position: absolute;
	top: 8mm;
	left:10mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;
	text-align: left;
}
.Seat_Label{
	position: absolute;
	top: 0mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.Seat{
	font-size: 9pt;
	font-weight: bold;
	position: absolute;
	top: -.5mm;
	left:17mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}
.Gate_Label{
	position: absolute;
	top: 4mm;
	left:0mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}
.Gate{
	font-size: 9pt;
	font-weight: bold;
	position: absolute;
	top: 3.5mm;
	left:17mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.BoardingTime_Label{
	position: absolute;
	top: 8mm;
	left:0mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}
.BoardingTime{
	font-size: 9pt;
	font-weight: bold;
	position: absolute;
	top: 7.5mm;
	left:17mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.Baggage_Label{
	position: absolute;
	top: 12mm;
	left:0mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}
.Baggage{
	position: absolute;
	top: 12mm;
	left:17mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.Ticket_Label{
	position: absolute;
	top: 18mm;
	left:0mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}
.Ticket{
	position: absolute;
	top: 18mm;
	left:17mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.Agent_Label{
	position: absolute;
	top: 12mm;
	left:0mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}
.Agent{
	position: absolute;
	top: 12mm;
	left:17mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.PNR_Label{
	position: absolute;
	top: 16mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.PNR{
	position: absolute;
	top: 16mm;
	left:17mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}

.Flight_Label{
	position: absolute;
	top: 0mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.Flight{
	font-size: 9pt;
	font-weight: bold;
	position: absolute;
	top: -0.5mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.Date_Label{
	position: absolute;
	top: 4mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.Date{
	font-size: 9pt;
	font-weight: bold;
	position: absolute;
	top: 3.5mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.Time_Label{
	position: absolute;
	top: 8mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.Time{
	font-size: 9pt;
	font-weight: bold;
	position: absolute;
	top: 7.5mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.Class_Label{
	position: absolute;
	top: 12mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;	
}
.Class{
	position: absolute;
	top: 12mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}

.Issue_Label{
	position: absolute;
	top: 16mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;	
}
.Issue{
	position: absolute;
	top: 16mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.Type_Label{
	position: absolute;
	top: 20mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;	
}
.Type{
	position: absolute;
	top: 20mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.Status_Label{
	position: absolute;
	top: 24mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;	
}
.Status{
	position: absolute;
	top: 24mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}

.Fare_Label{
	font-size: 5.5pt;
	position: absolute;
	top: 0mm;
	left:0mm;
 	width: 10mm;
	height: 2mm;
	z-index:1;
}
.Fare{
	font-size: 5.5pt;
	text-align: right;
	position: absolute;
	top: 0mm;
	left:20mm;
 	width: 18mm;
	height: 2mm;
	z-index:1;
}

.Tax_Label{
	font-size: 5.5pt;
	position: absolute;
	top: 2.5mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.Tax{
	font-size: 5.5pt;
	text-align: right;
	position: absolute;
	top: 2.5mm;
	left:20mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}

.YQ_Label{
	font-size: 5.5pt;
	position: absolute;
	top: 5mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.YQ{
	font-size: 5.5pt;
	text-align: right;
	position: absolute;
	top: 5mm;
	left:20mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}

.TicketingFee_Label{
	font-size: 5.5pt;
	position: absolute;
	top: 7.5mm;
	left:0mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;	
}
.TicketingFee{
	font-size: 5.5pt;
	text-align: right;
	position: absolute;
	top: 7.5mm;
	left:20mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}

.ReservationFee_Label{
	font-size: 5.5pt;
	position: absolute;
	top: 10mm;
	left:0mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;	
}
.ReservationFee{
	font-size: 5.5pt;
	text-align: right;
	position: absolute;
	top: 10mm;
	left:20mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}

.TotalFare_Label{
	font-size: 5.5pt;
	position: absolute;
	top: 12.5mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;	
}
.TotalFare{
	font-size: 5.5pt;
	text-align: right;
	position: absolute;
	top: 12.5mm;
	left:20mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}

.FareInfoAudit{
	position: absolute;
	top: 17mm;
	left: 0mm;
 	width: 32mm;
	height: 20mm;
	z-index:1;
}
.Fare_LabelAudit{
	position: absolute;
	top: 0mm;
	left:0mm;
 	width: 20mm;
	height: 2mm;
	z-index:1;
}
.FareAudit{
	text-align: right;
	position: absolute;
	top: 0mm;
	left:20mm;
 	width: 20mm;
	height: 2mm;
	z-index:1;
}

.Tax_LabelAudit{
	position: absolute;
	top: 3.5mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.TaxAudit{
	text-align: right;
	position: absolute;
	top: 3.5mm;
	left:20mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}

.YQ_LabelAudit{
	position: absolute;
	top: 7mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.YQAudit{
	text-align: right;
	position: absolute;
	top: 7mm;
	left:20mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}

.TicketingFee_LabelAudit{
	position: absolute;
	top: 10.5mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.TicketingFeeAudit{
	text-align: right;
	position: absolute;
	top: 10.5mm;
	left:20mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}

.ReservationFee_LabelAudit{
	position: absolute;
	top: 14mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.ReservationFeeAudit{
	text-align: right;
	position: absolute;
	top: 14mm;
	left:20mm;
 	width: 20mm;
	height: 5mm;
	
	z-index:1;
}

.TotalFare_LabelAudit{
	position: absolute;
	top: 17.5mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.TotalFareAudit{
	text-align: right;
	position: absolute;
	top: 17.5mm;
	left:20mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.Audit_Panel2{
	position: absolute;
	top: 43mm;
	left: 0mm;
 	width: 15mm;
	height: 5mm;
	z-index:1;
}
.Seat_LabelAudit{
	position: absolute;
	top: 0mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.SeatAudit{
	position: absolute;
	top: 0mm;
	left:12mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}
.Agent_LabelAudit{
	position: absolute;
	top: 3mm;
	left:0mm;
 	width: 18mm;
	height: 5mm;
	z-index:1;
}
.AgentAudit{
	position: absolute;
	top: 3mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.PNR_LabelAudit{
	position: absolute;
	top: 6mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;
}
.PNRAudit{
	position: absolute;
	top: 6mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;
}
.Audit_Panel3{
	position: absolute;
	top: 43mm;
	left: 45mm;
 	width: 15mm;
	height: 5mm;
	z-index:1;
}
.Issue_LabelAudit{
	position: absolute;
	top: 3mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;	
}
.IssueAudit{
	position: absolute;
	top: 3mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.Type_LabelAudit{
	position: absolute;
	top: 6mm;
	left:0mm;
 	width: 10mm;
	height: 5mm;
	z-index:1;	
}
.TypeAudit{
	position: absolute;
	top: 6mm;
	left:12mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.InfoHeadline{
	font-weight: bold;
	position: absolute;
	top: 0mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.CityFrom{
	position: absolute;
	top: 4mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.CityFromPhone{
	position: absolute;
	top: 4mm;
	left:20mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.CityTo{
	position: absolute;
	top: 8mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.CityToPhone{
	position: absolute;
	top: 8mm;
	left:20mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.Email_Label{
	position: absolute;
	top: 12mm;
	left:0mm;
 	width: 20mm;
	height: 5mm;
	z-index:1;	
}
.Email{
	position: absolute;
	top: 12mm;
	left:20mm;
 	width: 50mm;
	height: 5mm;
	z-index:1;	
}
.WebSite{
	position: absolute;
	top: 16mm;
	left:0mm;
 	width: 50mm;
	height: 5mm;
	z-index:1;	
}
.Barcode{
	position: absolute;
	top: 19mm;
	left:0mm;
	z-index:1;	
}

SPAN.Barcode IMG{
background : Black;
height:30px;
}
SPAN.Barcode IMG.B11{
width: 1px;
margin: 0px 1px 0px 0px;
}
SPAN.Barcode IMG.B12{
width: 1px;
margin: 0px 2px 0px 0px;
}
SPAN.Barcode IMG.B21{
width: 2px;
margin: 0px 1px 0px 0px;
}
SPAN.Barcode IMG.B22{
width: 2px;
margin: 0px 1px 0px 0px;
}
SPAN.Barcode IMG.B1{
width: 1px;
}
SPAN.Barcode SPAN.TicketNumber{
background: White;
font-family: tahoma, verdana, arial, helvetica, sans-serif;
font-size: 8pt;
width: 100%;
padding-left: 10px;
}
.Description{
	font-size: 9pt;
	font-weight: bold;
	position: absolute;
	top: 43mm;
	left:37mm;
 	width: 50mm;
	height: 5mm;
	z-index:1;	
}
.Text_Label{
	position: absolute;
	top: 30mm;
	left:0mm;
 	width: 38mm;
	height: 5mm;
	z-index:1;
}
.Text_font1{
	font-size: 5.5pt;
	position: absolute;
	top: 30.5mm;
	left:0mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;	
}
.Text_font2{
	font-size: 5.5pt;
	position: absolute;
	top: 32.5mm;
	left:0mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;	
}
.Text_font3{
	font-size: 5.5pt;
	position: absolute;
	top: 34.5mm;
	left:0mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;	
	
}		
.Text_Labe2{
	position: absolute;
	top: 14mm;
	left:0mm;
 	width: 38mm;
	height: 5mm;
	z-index:1;
}	
.Text_font4{
	font-size: 5.5pt;
	position: absolute;
	top: 16.5mm;
	left:0mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;	
}
.Text_font5{
	font-size: 5.5pt;
	position: absolute;
	top: 18.5mm;
	left:0mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;	
}
.Text_font6{
	font-size: 5.5pt;
	position: absolute;
	top: 20.5mm;
	left:0mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;	
	
}	
.Text_font7{
	font-size: 5.5pt;
	position: absolute;
	top: 22.5mm;
	left:0mm;
 	width: 100mm;
	height: 5mm;
	z-index:1;	
	
}								
</STYLE>
</HEAD>
<BODY>
<DIV>

 
<DIV  Class="Info">
		<SPAN Class="InfoHeadline">Reservation</SPAN>
	<xsl:for-each select="Booking/Itinerary/FlightSegment">
		<SPAN Class="CityFrom"><xsl:value-of select="origin_name"/></SPAN>
		<SPAN Class="CityFromPhone"><xsl:value-of select="origin_phone"/></SPAN>
		<SPAN Class="CityTo"><xsl:value-of select="destination_name"/></SPAN>
		<SPAN Class="CityToPhone"><xsl:value-of select="destination_phone"/></SPAN>
	</xsl:for-each>
	<xsl:for-each select="Booking/Header/BookingHeader">
		<SPAN Class="Email_Label">e-mail</SPAN>
		<SPAN Class="Email"><a><xsl:attribute name="href">mailto:<xsl:value-of select="contact_email"/> 
		</xsl:attribute>
		<xsl:value-of select="contact_email"/></a>
		</SPAN>
		<SPAN Class="WebSite">
			<a><xsl:attribute name="href">www.orient-thai.com
		</xsl:attribute>
		www.orient-thai.com</a>
		</SPAN>
	</xsl:for-each>
</DIV>
 
	<DIV Class="Receipt1_FightInfo">
		<DIV Class="Ticket_Panel1">
		<xsl:for-each select="Booking/Tickets/Ticket">
			<SPAN Class="PassengerName_Label">Name</SPAN>
			<SPAN Class="PassengerName">
			<xsl:value-of select="title_rcd"/>&#32;
			<xsl:value-of select="firstname"/>&#32;
			<xsl:value-of select="lastname"/>
			</SPAN>
		</xsl:for-each>
		<xsl:for-each select="Booking/Itinerary/FlightSegment">
			<SPAN Class="From_Label">From</SPAN>
			<SPAN Class="From"><xsl:value-of select="origin_name"/></SPAN>
			<SPAN Class="To_Label">To</SPAN>
			<SPAN Class="To"><xsl:value-of select="destination_name"/></SPAN>
		</xsl:for-each>
		</DIV>
		<DIV Class="Ticket_Panel2">
		<xsl:for-each select="Booking/Tickets/Ticket">
			<SPAN Class="Seat_Label">Seat</SPAN>
			<SPAN Class="Seat"><xsl:value-of select="seat_number"/></SPAN>
		</xsl:for-each>
		<xsl:for-each select="Booking/Itinerary/FlightSegment">
			<SPAN Class="Gate_Label">Gate</SPAN>
			<SPAN Class="Gate"><xsl:value-of select="origin_terminal"/></SPAN>
			<SPAN Class="BoardingTime_Label">Boarding Time</SPAN>
			<xsl:choose>
				<xsl:when test="departure_time!=''">
					<SPAN Class="BoardingTime">
			<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/>
			</SPAN>
				</xsl:when>
				<xsl:otherwise>
					&#32;
			</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<xsl:for-each select="Booking/Tickets/Ticket">
			<SPAN Class="Baggage_Label">Baggage</SPAN>
			<SPAN Class="Baggage"><xsl:value-of select="baggage_tax"/></SPAN>
		</xsl:for-each>
		</DIV>
		<xsl:for-each select="Booking/Tickets/Ticket">
		<DIV Class="Ticket_Panel3">
			<SPAN Class="Flight_Label">Fight</SPAN>
			<SPAN Class="Flight"><xsl:value-of select="airline_rcd"/></SPAN>
			<SPAN Class="Date_Label">Date</SPAN>
			<xsl:choose>
				<xsl:when test="departure_date!=''">
					<SPAN Class="Date">
							<xsl:value-of select="substring(departure_date,7,2)"/>.<xsl:value-of select="substring(departure_date,5,2)"/>.<xsl:value-of select="substring(departure_date,1,4)"/>
					</SPAN>
				</xsl:when>
				<xsl:otherwise>
					&#32;
			</xsl:otherwise>
			</xsl:choose>
			
			<SPAN Class="Time_Label">Time</SPAN>
			<xsl:choose>
				<xsl:when test="departure_time!=''">
					 <SPAN Class="Time">
				  <xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/>
				</SPAN>
				</xsl:when>
				<xsl:otherwise>
					&#32;
			</xsl:otherwise>
			</xsl:choose>
		</DIV>
		
		<DIV Class="FareInfo">
			<SPAN Class="Fare_Label">Fare:</SPAN>
			<xsl:if test="fare_amount_incl!=''">
			<SPAN Class="Fare">
			<xsl:value-of select="format-number(fare_amount_incl,'###,###.00')"/>&#32;
			<xsl:value-of select="currency_rcd"/>
			</SPAN>
			</xsl:if>
			
			<SPAN Class="Tax_Label">Tax</SPAN>
			<xsl:if test="tax_amount_incl!=''">
			<SPAN Class="Tax">
			<xsl:value-of select="format-number(tax_amount_incl,'###,###.00')"/>&#32;
			<xsl:value-of select="currency_rcd"/>
			</SPAN>
			</xsl:if>
			
			<SPAN Class="YQ_Label">YQ</SPAN>
			<xsl:if test="yq_amount_incl!=''">
			<SPAN Class="YQ"> 
			<xsl:value-of select="format-number(yq_amount_incl,'###,###.00')"/>&#32;
			<xsl:value-of select="currency_rcd"/>
			</SPAN>
			</xsl:if>
			
			<SPAN Class="TicketingFee_Label">Ticketing Fee</SPAN>
			<xsl:if test="ticketing_fee_amount_incl!=''">
			<SPAN Class="TicketingFee">
			<xsl:value-of select="format-number(ticketing_fee_amount_incl,'###,###.00')"/>&#32;
			<xsl:value-of select="currency_rcd"/>
			</SPAN>
			</xsl:if>
			
			<SPAN Class="ReservationFee_Label">Reservation Fee</SPAN>
			<xsl:if test="reservation_fee_amount_incl!=''">
			<SPAN Class="ReservationFee">
			<xsl:value-of select="format-number(reservation_fee_amount_incl,'###,###.00')"/>&#32;
			<xsl:value-of select="currency_rcd"/>
			</SPAN>
			</xsl:if>
			
			<SPAN Class="TotalFare_Label">Total Fare</SPAN>
			<xsl:if test="acc_net_total!=''">
			<SPAN Class="TotalFare">
			<xsl:value-of select="format-number(acc_net_total,'###,###.00')"/>&#32;
			<xsl:value-of select="currency_rcd"/>
			</SPAN>
			</xsl:if>
		</DIV>
		<DIV Class="Text_Label">
			<SPAN Class="Text_font1"><P>Tax ID 1232333</P></SPAN>
			<SPAN Class="Text_font2"><P>Issued by Orient Thai Airline Co.,Ltd. 138/70 17F Jewellery Center,Nares Rd.,</P></SPAN>
			<SPAN Class="Text_font3"><P>Bangrak, Bangkok 10700</P></SPAN>
			<xsl:if test="string(passenger_id) = string($passenger_id)">
				<DIV class="Barcode">
					<xsl:call-template name="BarcodeInterleaved2of5">
						<xsl:with-param name="ticket_number"/>
					</xsl:call-template>
				</DIV>
			</xsl:if>
			<SPAN Class="Description"><P>Boarding Pass</P></SPAN>
	</DIV>
		</xsl:for-each>
		
	</DIV> 
	<DIV Class="Ticket_FightInfo">
		<DIV Class="Ticket_Panel1">
		<xsl:for-each select="Booking/Tickets/Ticket">
			<SPAN Class="PassengerName_Label">Name</SPAN>
			<SPAN Class="PassengerName">
			<xsl:value-of select="title_rcd"/>&#32;
			<xsl:value-of select="firstname"/>&#32;
			<xsl:value-of select="lastname"/>
			</SPAN>
			</xsl:for-each>
			<xsl:for-each select="Booking/Itinerary/FlightSegment">
			<SPAN Class="From_Label">From</SPAN>
			<SPAN Class="From"><xsl:value-of select="origin_name"/></SPAN>
			<SPAN Class="To_Label">To</SPAN>
			<SPAN Class="To"><xsl:value-of select="destination_name"/></SPAN>
			</xsl:for-each>
		</DIV>
		<DIV Class="Ticket_Panel2">
		<xsl:for-each select="Booking/Tickets/Ticket">
			<SPAN Class="Seat_Label">Seat</SPAN>
			<SPAN Class="Seat"><xsl:value-of select="seat_number"/></SPAN>
		</xsl:for-each>
		<xsl:for-each select="Booking/Itinerary/FlightSegment">
			<SPAN Class="Gate_Label">Gate</SPAN>
			<SPAN Class="Gate"><xsl:value-of select="origin_terminal"/></SPAN>
			<SPAN Class="BoardingTime_Label">Boarding Time</SPAN>
			<xsl:choose>
				<xsl:when test="departure_time!=''">
					<SPAN Class="BoardingTime">
			<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/>
			</SPAN>
				</xsl:when>
				<xsl:otherwise>
					&#32;
			</xsl:otherwise>
			</xsl:choose>
			
			</xsl:for-each>
			<xsl:for-each select="Booking/Tickets/Ticket">
			<SPAN Class="Agent_Label">Agent</SPAN>
			<SPAN Class="Agent"><xsl:value-of select="agency_code"/></SPAN>
			<SPAN Class="PNR_Label">PNR</SPAN>
			<SPAN Class="PNR"><xsl:value-of select="record_locator"/></SPAN>
			</xsl:for-each>
			<DIV Class="Text_Labe2">
				<SPAN Class="Text_font4"><P>* Check-In closed at 18:40</P></SPAN>
				<SPAN Class="Text_font5"><P>* Ticket valid 90 days from date of issue</P></SPAN>
				<SPAN Class="Text_font6"><P>* Non endorsable,non reroutable</P></SPAN>
				<SPAN Class="Text_font7"><P>* Valid on OX only</P></SPAN>
			<xsl:for-each select="Booking/Tickets/Ticket">
				
				<xsl:if test="string(passenger_id) = string($passenger_id)">
					<DIV class="Barcode">
						<xsl:call-template name="BarcodeInterleaved2of5">
							<xsl:with-param name="ticket_number"/>
						</xsl:call-template>
						
					</DIV>
				</xsl:if>
				</xsl:for-each>
				<SPAN Class="Description"><P>Boarding Pass</P></SPAN>
			</DIV>
		  
		</DIV>
		<DIV Class="Ticket_Panel3">
		<xsl:for-each select="Booking/Tickets/Ticket">
			<SPAN Class="Flight_Label">Fight</SPAN>
			<SPAN Class="Flight"><xsl:value-of select="airline_rcd"/></SPAN>
			<SPAN Class="Date_Label">Date</SPAN>
			<xsl:choose>
				<xsl:when test="departure_date!=''">
					<SPAN Class="Date">
							<xsl:value-of select="substring(departure_date,7,2)"/>.<xsl:value-of select="substring(departure_date,5,2)"/>.<xsl:value-of select="substring(departure_date,1,4)"/>
					</SPAN>
				</xsl:when>
				<xsl:otherwise>
					&#32;
			</xsl:otherwise>
			</xsl:choose>
			
			<SPAN Class="Time_Label">Time</SPAN>
			<xsl:choose>
				<xsl:when test="departure_time!=''">
					 <SPAN Class="Time">
				  <xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/>
				</SPAN>
				</xsl:when>
				<xsl:otherwise>
					&#32;
			</xsl:otherwise>
			</xsl:choose>
			<SPAN Class="Class_Label">Class</SPAN>
			<SPAN Class="Class"><xsl:value-of select="boarding_class_rcd"/></SPAN>
			<SPAN Class="Issue_Label">Issue</SPAN>
			<SPAN Class="Issue"> 
			  <Date dt:dt="datetime"></Date>


					<xsl:value-of select="substring(datetime,7,2)"/>.<xsl:value-of select="substring(issue_date,5,2)"/>.<xsl:value-of select="substring(issue_date,1,4)"/>
			</SPAN>
			</xsl:for-each>
			<xsl:for-each select="Booking/Tickets/Ticket">
			
			<SPAN Class="Type_Label">Type</SPAN>
			<SPAN Class="Type"><xsl:value-of select="passenger_type"/></SPAN>
			<SPAN Class="Status_Label">Status</SPAN>
			<SPAN Class="Status"><xsl:value-of select="passenger_status_rcd"/></SPAN>
			</xsl:for-each>
			
		</DIV>
		
		<SPAN Class="BaggageAllowance_Label">BaggageAllowance</SPAN>
		<SPAN Class="BaggageAllowance"><xsl:value-of select="baggage_tag"/></SPAN>
	</DIV>
    	
 
</DIV>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\TPC\Untitled3.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->