<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				                      xmlns:tikLanguage="tik:Language">
	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; UTF-8" />
				<title>Peach | Peach</title>
				<style>
					html, body {	height:auto;margin: 0;	padding: 0;	background-color: #ffffff;	font: 11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial;height:100%;}
					*html html, body {	margin: 0;	padding: 0;	background-color:#ffffff;	height: 100%;	FONT:11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial;height:100%;}
					IMG {BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; BORDER-BOTTOM: 0px}

					.ImgHeader{ background-color:#72c367; width:860px; height:120px;}
					
					.WrapperBody{ padding:20px 20px 50px 20px; width:720px; min-height:180px; _height:620px; background-color:#FFFFFF;}

					/*Footer*/
					/*.Footer{ background-image: url(../Images/ImgFooter.jpg); width:860px; height:24px; font:11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial; }*/
					.Footer li{ padding-top:13px; list-style:none; color:#FFFFFF; float:left;}
					.EmailBodyText{height:22px; color:#174a7c; padding-bottom:10px;}
					.EmailUser { height:22px; color:#174a7c;}
					.EmailBody{height:22px;color:#174a7c;}
					
					/*********************************/
					.Wrapper { WIDTH:760px; TEXT-ALIGN: left; margin: 0 auto; }
					.mainlogo {margin-bottom: 20px; margin-left: 15px;}
					.emailregistercontent {margin-left: 15px;}
					.emailregisterfooter {background: #b634bb; float: left; width: 100%; color: #FFF;}
					.emailregisterfooter td, .emailregisterfooter td a {color: #FFF; text-decoration: none;}
					.emailregisterfooter .COL1 {width: 50%; margin-left: 15px; float: left; text-decoration: none;}
					.emailregisterfooter .COL2 {float: right; margin-right: 15px; text-align: right;}
					.emailregisterfooter .copyright {float: right; margin-right: 15px;}
					.clear-all {clear: both;}
				</style>
			</head>
			<body>
				<div class="Wrapper">
					<div class="mainlogo">
						<img>
							<xsl:attribute name="src">
								<xsl:value-of select="MailDetail/HeaderURL" />/App_Themes/Default/Images/mainlogo.png
							</xsl:attribute>
						</img>
					</div>
					
					<xsl:if test="MailDetail/Action = 'create'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="MailDetail/Title" />&#160;
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />&#160;様
							</div>
					
							<p>
								この度は、本サイト(www.flypeach.com)にアカウントを作成していただき、 ありがとうございます。 
							</p>
					
							<p>
								アカウントの作成により、予約の確認・変更<sup>*1</sup>及びお客様情報を更新することが簡単にできますので、FlyPeach.com でのフライト予約が一層便利になります。
							</p>
					
							<p>
								また、FlyPeach.comのメルマガに登録して頂いた方には、Peachの最新情報をはじめ、特典やキャンペーン情報をお届けします。
							</p>
					
							<p>
								下記はアカウントを作成した際に設定したユーザー名及びパスワードになりますので、大切に保管してください。
							</p>
					
							<p>
								<div>ユーザー名： <span><xsl:value-of select="MailDetail/UserName" /></span></div>
								<div>パスワード： 
									<span>
										<xsl:if test="MailDetail/Password = ''">
											Password
										</xsl:if>
										<xsl:if test="MailDetail/Password != ''">
											<xsl:value-of select="MailDetail/Password" />
										</xsl:if>
									</span>
								</div>
							</p>
					
							<p>
								アカウントの情報を修正したい場合は、下記ページよりお願い致します。
								<br/>
								<a href="http://book.flypeach.com/default.aspx?cl=l&#38;langculture=ja-jp">http://book.flypeach.com/default.aspx?cl=l&#38;langculture=ja-jp</a>
							</p>
					
							<p>
								Peach一同、ご利用をお待ちしております。
							</p>
							
							<p>
								*1 運賃タイプによって予約変更ができない場合もございます。 詳細は <a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a> にてご確認ください。

							</p>
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<xsl:if test="MailDetail/Action = 'reset'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="MailDetail/Title" />&#160;
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />&#160;<xsl:value-of select="tikLanguage:get('Email_13','Dear')" />
							</div>
					
							<p>
								Your password has been successfully reset.
							</p>
					
							<p>
								<div>Username: <span><xsl:value-of select="MailDetail/UserName" /></span></div>
								<div>New Password: 
									<span>
										<xsl:value-of select="MailDetail/Password" />
									</span>
								</div>
							</p>
					
							<p>
								If you'd like to amend your profile details, please visit 
								<br/>
								<a href="http://www.flypeach.com/profile">http://www.flypeach.com/profile</a>
							</p>
					
							<p>
								Sincerely yours,
							</p>
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<xsl:if test="MailDetail/Action = 'change'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="MailDetail/Title" />&#160;
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />&#160;<xsl:value-of select="tikLanguage:get('Email_13','Dear')" />
							</div>
					
							<p>
								Your password has been successfully changed.
							</p>
					

							<p>
								If you'd like to amend your profile details, please visit 
								<br/>
								<a href="http://www.flypeach.com/profile">http://www.flypeach.com/profile</a>
							</p>
					
							<p>
								Sincerely yours,
							</p>
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<div class="clear-all"></div>
					
					<table class="emailregisterfooter">
						<tr>
							<td class="COL1"><a href="www.flypeach.com/termsandconditions.aspx">利用規約</a> | <a href="www.flypeach.com/privacypolicy.aspx">プライパシーポリシー</a></td>
							<td class="COL2">Copyright © Peach Aviation Limited</td>
						</tr>
						
					</table>
				</div>

			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>