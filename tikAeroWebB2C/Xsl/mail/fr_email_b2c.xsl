<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Project: MNA
File name: en_email_b2b.xsl
-No Quote
-No Payment 
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:key name="passenger_type_rcd_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<!--Variable-->
	<xsl:variable name="BarPDF417URL">http://www.tikaeroid.com/TikAeroBarcode/TikAeroBarcodePDF417.ashx?</xsl:variable>
	<xsl:variable name="BarURL">http://www.tikaeroid.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
	<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>
	<xsl:variable name="BaseURL">http://www.tiksystems.com/tikaero/XSLImages/</xsl:variable>
	<xsl:variable name="Titel">TikAero Itinerary</xsl:variable>
	<xsl:variable name="passenger_id" select="Booking/Tickets/Ticket/passenger_id"/>
	<xsl:variable name="booking_segment_id_tax" select="Booking/Tickets/Ticket[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
	<xsl:variable name="booking_segment_id" select="Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
	<xsl:variable name="sum_net_total" select="sum(Booking/Tickets/Ticket/net_total)"/>
	<xsl:variable name="sum_payment_amount" select="sum(Booking/Payments/Payment/payment_amount)"/>
	<!--Template-->

	<!-- Format String -->
	<xsl:template name="CDate2Julian">
		<xsl:param name="date"/>
		<xsl:param name="format" select="000"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="format-number(substring($date,3,2),'00')"/>
		<xsl:variable name="month" select="substring(substring-after('01/00002/03103/05904/09005/12006/15107/18108/21209/24310/27311/30412/334', concat($month1,'/')), 1, 3)"/>
		<xsl:value-of select="$day + $month"/>
	</xsl:template>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($day, '/', $month, '/', $year)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:value-of select="concat($day, '.', $month, '.', $year)"/>
	</xsl:template>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$Titel"/>
				</TITLE>

				<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
				<!--Style Sheet-->
				<STYLE>.HeaderBackground { background: url(http://www.tikaeroid.com/xslimages/AAS/Images/0/H/hhe.gif) fixed no-repeat left center; }
					.base { color: ; font-family: 12pt helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 9pt; width: 100%; height:100%}
					.base .content{ color: ; float: left;  font-size: 9pt; }
					.dummy{ }
					.base .content .contentHeader{ margin: 5px 0px 0px 0px; padding: 0px 0px 0px 5px; }
					.base .quickSearch { float: left; width: 170px; }					
					.base .quickSearch DIV.input { font-size: 8pt; float: left; padding: 2px 4px 2px 5px; text-align: left; }
					.base .quickSearch DIV.label{ float: left; padding: 2px 4px 2px 5px; text-align: right; vertical-align: middle; }
					.BarcodeHeader	{ color: ; font-size: 7pt; font-style: normal; font-weight: bold; padding: 0px 0px 0px 12px; text-align: left; width: 100%; }
					.PanelItinerary .Barcode	{ padding: 1 5 1 5; }
					.footer A	{ height: 20px; text-align: right; width: 50%; }
					.footer	SPAN	{ height: 20px; padding-top: 3px; width: 50%; }
					.grid TD.gridItems0Button	{ padding: 1px 5px 1px 1px; text-align: right; }
					.grid TR.GridFooter TD	{ background-color: transparent; border-bottom: 1px solid  #B9CAEB; color: #666666; font: bold 8pt ; border-top: 1px solid #B9CAEB; height: 22px;  }
					.grid TR.GridFooter TD.Summary	{ background-color: transparent; }
					.grid TR.gridHeader	{background:#e78800 ;color:#ffffff;;font-size:11px;font-weight:bold;padding:4px 2px 4px 5px;}
					.grid TR.GridPrice	{ background-color: tranparent; color: ; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridHeader TD	{ padding: 7px 2px 1px 5px; border-bottom: 1px solid #1278CA; }
					.grid TR.gridHeaderFight { background-color: #FED684; color: ; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridItems0 TD { border-bottom: 1px solid #B9CAEB; color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems1 TD { border-top: 1px solid  #B9CAEB; color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid tr.griditems2 td{color:;cursor:pointer;font-size:11px;font-weight:normal;height:15px;padding:0px 1px 0px 5px;}
					.grid TR.gridItems02 TD { color:  ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems0 TD { color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; }
					.grid TR.gridItems01 TD { color: ; cursor: pointer;  font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0o TD{ background-color: #FFBD3B; border-bottom: 1px solid #FFCC00; color: ; cursor: pointer;  font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01 TD	{ color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01o TD	{ background-color: #CEE6F7; color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0Input	{ color: ;font-size: 11px; font-weight: normal; height: 22px; padding: 5px 1px 1px 5px; }
					.header	{ width: 100%; }
					.header .subHeader	{ background-color: ; }
					/* End Forms \*/
					/* Headers \*/
					.headers	{color:#C4102F; font: bold 12pt /*background: url(../Images/0/H/hbh.jpg);*/ margin: 5px 0px 5px 5px; width: 100%; }
					.headers DIV	{ font: bold 8pt  margin: 3px 0px 3px 0px; padding: 5px 0px 5px 17px; }
					.headers DIV SPAN.S0{ color: #FF6600; }
					.headers SPAN.Button { width: 49%; }
					.Itinerary	{ color: ; font-size: 8pt; font-style: normal; padding: 10px 12px 0px 12px; text-align: left; width: 100%; }
					.Itinerary Span.O	{ color: #FF6600; font-weight: bold; }
					.Itinerary Span.1	{ color: #FF6600; font-weight: bold; text-decoration: underline; }
					.Itinerary Span.2 { color: ; font-weight: bold; }
					.welcometext,.
					.commenttext{font-family:verdana,arial,helvetica,sans-serif;font-size:12px;padding:1 10 1 5;}
					p.msonormal	{ color: #1E336C; font-family: "Verdana"; font-size: 8.5pt; margin-bottom: .0001pt; margin-left: 0in; margin-right: 0in; margin-top: 0in; mso-style-parent: ""; text-align: justify; }
					<!--P {page-break-after: left}-->
					</STYLE>
				<!--End Style Sheet-->
			</HEAD>
			<BODY leftmargin="10px;" topmargin="5" rightmargin="5" bottommargin="5" marginwidth="5" marginheight="5">
				<DIV style="width:100%;height:100%">
					<DIV class="header" align="left" style="width:100%;">
						<BR/>
						<img src="{$BaseURL}TPC/TPClogo.gif"/>
						<BR/>
						<BR/>
						<table border="0" id="table1" width="100%" height="99" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top" width="300">
									<DIV class="headers" style="width: 250px; height: 89px;font-family: helvetica, Tahoma, Arial, sans-serif, verdana;">
										<table style="width: 100%" class="headers">
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 12pt;font-weight: 400">
														<font color="#000000">予約:</font>
													</SPAN>
												</td>
												<td style="width: 183px">
													<xsl:value-of select="Booking/Header/BookingHeader/record_locator"/>
												</td>
											</tr>
											<tr>
												<td style="width: 183px"></td>
												<td style="width: 183px"></td>
											</tr>
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;font-weight: 400">
														<font color="#000000">エージェントコード:</font>&#x20;
													</SPAN>
												</td>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;">
														<xsl:value-of select="Booking/Header/BookingHeader/agency_code"/>
													</SPAN>
												</td>
											</tr>
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;font-weight: 400">
														<font color="#000000">予約の日付:</font>&#x20;
													</SPAN>
												</td>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;">
														<xsl:call-template name="formatdate">
															<xsl:with-param name="date" select="Booking/Header/BookingHeader/create_date_time"/>
														</xsl:call-template>
													</SPAN>
												</td>
											</tr>
										</table>
										<BR/>
									</DIV>
								</td>
								<td>
								</td>
								<td align="center" valign="top" width="14%">
									<xsl:for-each select="//Booking/Itinerary/FlightSegment[not(segment_status_rcd='XX')]">
										<xsl:if test="position()=1">
											<div class="Barcode" style="height: 19px">
												<table width="100%" border="0">
													<tbody>
														<tr>
															<td align="center" width="100%">
																<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="164" height="57"/>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</xsl:if>
									</xsl:for-each>
								</td>
							</tr>
						</table>
					</DIV>
					<DIV class="base">
						<!--Passengers-->
						<DIV class="headers">
							<SPAN style="font-size: 10pt;">搭乗者</SPAN>
							<TABLE class="Grid" cellspacing="0"  border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
								<TR class="gridHeader">
									<TD>&#32;</TD>
									<TD>姓</TD>
									<TD>名</TD>
									<TD>タイトル</TD>
									<TD>種類</TD>
								</TR>
								<xsl:for-each select="Booking/Passengers/Passenger">
									<xsl:variable name="date_of_birth" select="date_of_birth"/>
									<TR class="gridItems0">
										<TD>
											<xsl:choose>
												<xsl:when test="position()&gt;9">
													<xsl:value-of select="concat('0','',string(position()))"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="concat('00','',string(position()))"/>
												</xsl:otherwise>
											</xsl:choose>
										</TD>
										<TD>
											<xsl:value-of select="lastname"/>
										</TD>
										<TD>
											<xsl:value-of select="firstname"/>
										</TD>
										<TD>
											<xsl:value-of select="title_rcd"/>
										</TD>
										<TD>
											<xsl:value-of select="passenger_type_rcd"/>
										</TD>
									</TR>
								</xsl:for-each>
							</TABLE>
						</DIV>
						<br/>
						<!--End Passengers-->
						<!--Flights-->
						<DIV class="headers">
							<SPAN style="font-size: 10pt;">フライト</SPAN>
							<TABLE class="Grid" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
								<TR class="gridHeader">
									<TD/>
									<TD>フライト</TD>
									<TD>出発地</TD>
									<TD>目的地</TD>
									<TD>日付</TD>
									<TD>出発</TD>
									<TD>到着</TD>
									<TD>クラス</TD>
									<TD>状態</TD>
								</TR>
								<xsl:for-each select="Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
									<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
									<TR class="gridItems0">
										<TD>
											<xsl:value-of select="concat('00','',string(position()))"/>
										</TD>
										<TD>
											<SPAN>
												<xsl:value-of select="airline_rcd"/>&#32;<xsl:value-of select="flight_number"/></SPAN>
										</TD>
										<TD>
											<xsl:value-of select="origin_name"/>&#32;(<xsl:value-of select="origin_rcd"/>)</TD>
										<TD>
											<xsl:value-of select="destination_name"/>&#32;(<xsl:value-of select="destination_rcd"/>)</TD>
										<TD>
											<xsl:if test="string-length(departure_date) &gt; '0'">
												<xsl:call-template name="formatdate">
													<xsl:with-param name="date" select="departure_date"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="string-length(departure_date) = '0'">
												<xsl:text>OPEN</xsl:text>
											</xsl:if>
										</TD>
										<TD>
											<xsl:if test="string(departure_time) != '0'">
												<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
										</TD>
										<TD>
											<xsl:if test="string(planned_arrival_time) != '0'">
												<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/></xsl:if>
										</TD>
										<TD>
											<xsl:value-of select="class_name"/>
										</TD>
										<td>
											<xsl:choose>
												<xsl:when test="flight_id != ''">
													<xsl:value-of select="status_name"/>
													<!--<xsl:value-of select="segment_status_rcd"/>-->
												</xsl:when>
												<xsl:when test="not(string-length(departure_date) &gt; '0')">
													<xsl:text>&#32;</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text>Info</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</TR>
								</xsl:for-each>
							</TABLE>
						</DIV>
						<br/>
						<!--End Flights-->
						<!--Auxiliaries-->
						<xsl:for-each select="Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
							<xsl:if test="position()=1">
								<div class="headers">
									<span style="font-size: 10pt">候ふ</span>
									<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0"  border="0"
									       style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
										<tbody>
											<tr class="gridHeader">
												<td align="left">コード</td>
												<td align="left">詳細</td>
											</tr>
											<xsl:for-each select="/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
												<tr class="gridItems0">
													<td align="left" valign="top">
														<xsl:value-of select="display_name"/>
													</td>
													<td align="left" valign="top">
														<xsl:value-of select="remark_text"/>
													</td>
												</tr>
											</xsl:for-each>
										</tbody>
									</table>
								</div>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<!--End Auxiliaries-->
						<!--Seat number-->
						<xsl:for-each select="Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
							<xsl:sort select="seat_number" order="descending"/>
							<xsl:sort select="ticket_number" order="descending"/>
							<xsl:sort select="restriction_text" order="descending"/>
							<xsl:sort select="endorsement_text" order="descending"/>
							<xsl:if test="(string-length(seat_number) &gt;0) or (string-length(ticket_number) &gt;0) or (string-length(restriction_text) &gt;0) or (string-length(endorsement_text) &gt;0)">
								<xsl:if test="position()=1">
									<div class="headers">
										<span style="font-size: 10pt">航空券と座席</span>
										<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0"  border="0"
										       style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
											<tbody>
												<tr class="gridHeader">
													<td align="left">フライト</td>
													<td align="left">搭乗者名</td>
													<td align="left">航空券</td>
													<td align="left">座席</td>
													<td align="left">賛同 &amp; 解ける </td>
													<!--<td align="right" width="5%">&#160;</td>-->
												</tr>
												<xsl:for-each select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
													<xsl:if test="(string-length(seat_number) &gt;0) or (string-length(ticket_number) &gt;0) or (string-length(restriction_text) &gt;0) or (string-length(endorsement_text) &gt;0)">
														<tr class="gridItems0">
															<td align="left" valign="top">
																<xsl:value-of select="airline_rcd"/>&#32;<xsl:value-of select="flight_number"/></td>
															<td align="left" valign="top">
																<xsl:value-of select="lastname"/>/&#32;<xsl:value-of select="firstname"/></td>
															<td align="left" valign="top">
																<xsl:value-of select="ticket_number"/>
															</td>
															<td align="left" valign="top">
																<xsl:value-of select="seat_number"/>
															</td>
															<td align="left" valign="top">
																<xsl:choose>
																	<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) &gt;0)">
																		<xsl:value-of select="endorsement_text"/>
																		<br/>
																		<xsl:value-of select="restriction_text"/>
																	</xsl:when>
																	<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) =0)">
																		<xsl:value-of select="restriction_text"/>
																	</xsl:when>
																	<xsl:when test="(string-length(restriction_text) =0) and (string-length(endorsement_text) &gt;0)">
																		<xsl:value-of select="endorsement_text"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="restriction_text"/>
																	</xsl:otherwise>
																</xsl:choose>
															</td>
														</tr>
													</xsl:if>
												</xsl:for-each>
											</tbody>
										</table>
									</div>
									<br/>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
						<!--End Seat number-->
						<!--Special Services-->
						<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')]">
							<xsl:sort select="departure_date" order="descending"/>
							<xsl:if test="position()=1">
								<div class="headers">
									<SPAN style="font-size: 10pt">料金</SPAN>
									<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0"  border="0"
									       style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
										<tbody>
											<tr class="gridHeader">
													<td>&#32;</td>
												<td>フライト</td>
												<td>搭乗者名</td>
												<td>状態</td>
												<td>特別な要望</td>
												<td>文章</td>
											</tr>
											<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd = 'XX')]">
												<tr class="gridItems0">
													<td>
														<xsl:value-of select="concat('00','',string(position()))"/>
													</td>
													<td>
														<xsl:value-of select="airline_rcd"/>&#32;<xsl:value-of select="flight_number"/></td>
													<td>
														<xsl:value-of select="lastname"/>/&#32;<xsl:value-of select="firstname"/></td>
													<td>
														<xsl:value-of select="special_service_status_rcd"/>
													</td>
													<td>
														<xsl:value-of select="special_service_rcd"/><xsl:text>&#32;</xsl:text><xsl:value-of select="display_name"/></td>
													<td>
														<xsl:value-of select="service_text"/>
													</td>
												</tr>
											</xsl:for-each>
										</tbody>
									</table>
								</div>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<!--End Special Services-->

						<!--Specifying page breaks for printing using CSS-->
						<DIV style="page-break-after:always"><br/></DIV>
						<!--End Specifying page breaks for printing using CSS-->


						<br/>
						<br/>
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal">
<!--									<p class="MsoNormal" align="center" style="text-align:center">
										<b>
											<font size="3">Conditions of Carriage for Passengers and Baggage </font>
										</b>
									</p>
									<br/>-->
									<p class="MsoNormal">
										Your airlines ticket is electronically stored in our system and is subject to conditions of contract.
									</p>
									<br/>
									<p class="MsoNormal">
										PLEASE BRING THIS RECEIPT AND YOUR IDENTITY CARD ON YOUR TRAVEL IN CASE REQUIRED BY AIRPORT/CHECK-IN COUNTER/CUSTOMS AND IMMIGRATION OFFICIALS AS PROOF OF PURCHASE.
									</p>
									<br/>
									<p class="MsoNormal">
										CHECK-IN COUNTERS WILL BE CLOSED 45 MINUTES PRIOR TO DEPARTURE. YOU HAVE TO BE AT THE BOARDING GATE AT LEAST 30 MINUTES BEFORE FLIGHT DEPARTS OR WE WILL LEAVE WITHOUT YOU TO AVOID UNNECESSARY DELAYS.
									</p>
									<br/>
									<p class="MsoNormal">
										<b>Notice:</b>
									</p>
									<p class="MsoNormal">
										CARRIAGE AND OTHER SERVICES PROVIDED BY THE CARRIER ARE SUBJECT TO THE CONDITIONS OF CARRIAGE WHICH ARE HEREBY INCORPORATED BY REFRENCE. THESE CONDITIONS MAY BE OBTAINED FROM THE ISSUING CARRIER.
									</p>
									<br/>
									<p class="MsoNormal">
										<b>DANGEROUS GOODS:</b>
									</p>
									<p class="MsoNormal">
										FOR  SAFETY REASON DANGEROUS ARTICLES SUCH AS COMPRESSED GASES / FLAMMABLE / NON FLAMMABLE / POISONOUS / CORROSIVES / ACIDS / ALKALIS / AND WET CELL BATTERIES / ETILOGIC AGENTS / BACTERIA / VIRUSES ETC/ EXPLOSIVES MUNITIONS/ FIREWORKS/ FLARES/RADIO ACTIVE/OXIDIZING MATERIALS OR OTHER DANGEROUS GOODS ARTICLES MUST NOT BE CARRIED IN PASSENGERS BAGGAGE.
									</p>
								</td>
							</tr>
						</table>
					</DIV>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>