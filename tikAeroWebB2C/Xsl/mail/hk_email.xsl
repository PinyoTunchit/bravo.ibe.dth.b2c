<?xml version="1.0" encoding="utf-8"?>
<!-- 
Project: Peach 
File name: en_email.xsl
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:CharCode="http://www.tikaero.com/Printing">

	<xsl:import href="string.xsl"/>
	<xsl:import href="date-time.xsl"/>
	<xsl:import href="functions.xsl"/>

	<xsl:include href="hk_email_details.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:variable name="Debug">0</xsl:variable>

	<xsl:variable name="OTable">&lt;table style="width: 673px; margin: 0px 0px 0px 36px; height: 779px;"&gt;</xsl:variable>
	<xsl:variable name="CTable">&lt;/table&gt;</xsl:variable>

	<xsl:template match="/">
		<html>
			<!-- Header with StyleSheet-->
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<meta content="en-us" http-equiv="Content-Language"/>
				<meta http-equiv="Pragma" content="no-cache"/>
				<meta http-equiv="Expires" content="-1"/>
				<title>Peach Itinerary</title>
				<xsl:call-template name="styleColor"></xsl:call-template>
				<style type="text/css">
					<!--/* Remove margins from the 'html' and 'body' tags, and ensure the page takes up full screen height */-->html, body 
	{
	height: 100%;
	margin: 0;
	padding: 0;
	}
	#page-background 
	{
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	}
	#content
	{
	position: relative;
	z-index: 1;
	padding: 10px;
	}</style>
			</head>
			<!--Body-->
			<body>
				<table>
					<xsl:if test="$Debug = 1">(Debug)</xsl:if>
					<xsl:if test="$Debug = 1">

						<span style="font-family:trebuchet ms; font-size:120%">Debug Version</span>
						<hr/>
					</xsl:if>
				</table>
				<!--<table cellspacing="1"  style="width: 759px;background-color: yellow; ">
	<tr>
		<td>******************************************************************
		FOR CHECK PAGE WIDTH
		        ******************************************************************
		</td>
	</tr>
</table>-->
				<!--<p>Main Form</p>-->
				<!--<xsl:copy-of select="$ReportHeader"/>-->

				<!--Header-->
				<xsl:apply-templates select="Booking/Header"></xsl:apply-templates>
				<!--End Header-->

				<!--Passengers 
				<xsl:apply-templates select="Booking/Passengers"></xsl:apply-templates>
				 End Passengers-->
				<!--FlightSegment-->
				<xsl:apply-templates select="Booking/Itinerary"></xsl:apply-templates>
				<!--End FlightSegment-->

				<!--Tickets-->
				<xsl:apply-templates select="Booking/Tickets"></xsl:apply-templates>
				<!--End Tickets-->


				<!--Passport Information-->
				<xsl:apply-templates select="Booking/Passengers"></xsl:apply-templates>
				<!--End Passport Information-->


				<!--Auxiliaries-->
				<xsl:apply-templates select="Booking/Remarks"></xsl:apply-templates>
				<!--End Auxiliaries-->



				<!--Special Services-->
				<xsl:apply-templates select="Booking/SpecialServices"></xsl:apply-templates>
				<!--End Special Services-->
				<xsl:copy-of select="$Important"/>
				 
				<!--Quotes-->
				<xsl:apply-templates select="Booking/TicketQuotes"></xsl:apply-templates>
				<!--End Quotes-->
 				
				<!--Payments-->
				<xsl:apply-templates select="Booking/Payments"></xsl:apply-templates>
				<!--End Payments-->

				<!--Tickets-->
				<xsl:apply-templates select="/Booking"></xsl:apply-templates>
				<!--End Tickets-->

				<!--Payments-->
				<xsl:apply-templates select="//BookingHeader"></xsl:apply-templates>
				<!--<xsl:copy-of select="$R_OrderRecipient"/>-->
				<!--End Payments-->

				<!--Filler -->
				<xsl:choose>
					<!-- case of only one page-->
					<xsl:when test="CharCode:getCounter() &lt;= $RowsPerPage">
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="$RowsPerPage - (CharCode:getCounter())"/>
						</xsl:call-template>
						<xsl:copy-of select="$ReportFooter"/>
					</xsl:when>
					<!-- case of more than one page-->
					<xsl:otherwise>
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="$RowsPerPage - (CharCode:getCounter() mod $RowsPerPage )"/>
						</xsl:call-template>
						<xsl:copy-of select="$ReportFooter"/>
					</xsl:otherwise>
				</xsl:choose>
				<!--End Filler -->
				<!--
				<xsl:apply-templates select="Booking/Header/BookingHeader">
				</xsl:apply-templates>
				<xsl:copy-of select="$ReportFooter"/>-->
				<!--</div>-->
			</body>
			<!--End Body-->
		</html>
	</xsl:template>
	<!--End Template match-->
	<!-- variable ReportHeader-->
	<!--<xsl:variable name="ReportHeader">
		<table cellspacing="0" style="width: 673px; margin: 0px 0px 0px 36px;">
			 
			<tr>
				<td>
					 <img alt="Smile" src="http://www.tikaero.com/XSLImages/jad/JAD_Logo.jpg" width="205"/>

				</td>
			</tr>
		</table><br/>
	</xsl:variable>-->
	<!-- End variable ReportHeader-->
	<!-- variable ReportFooter-->
	<xsl:variable name="ReportFooter">
	</xsl:variable>
	<!-- End variable ReportFooter-->
	<!-- Template Filler  (Insert blank line)-->
	<xsl:template name="Filler">
	</xsl:template>
	<!-- End Template Filler-->
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\XML\LVMP22_Itinerary.xml" htmlbaseurl="" outputurl="" processortype="msxmldotnet" useresolver="no" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->