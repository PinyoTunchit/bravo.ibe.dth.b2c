<?xml version="1.0" encoding="utf-8"?>
<!-- 
Project: TCP
File name: en_email_light_b2c.xsl
-VAT
-Same b2b,b2c,b2e
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:key name="passenger_type_rcd_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<!--Variable-->
	<xsl:variable name="BarURL">http://www.tikaeroid.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
	<xsl:variable name="BarPDF417URL">http://www.tikaeroid.com/TikAeroBarcode/TikAeroBarcodePDF417.ashx?</xsl:variable>
	<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>
	<xsl:variable name="BaseURL">http://www.tiksystems.com/tikaero/XSLImages/</xsl:variable>
	<xsl:variable name="Titel">TikAero Itinerary</xsl:variable>
	<xsl:variable name="passenger_id" select="Booking/Tickets/Ticket/passenger_id"/>
	<xsl:variable name="booking_segment_id_tax" select="Booking/Tickets/Ticket[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
	<xsl:variable name="booking_segment_id" select="Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
	<xsl:variable name="sum_net_total" select="sum(Booking/Tickets/Ticket/net_total)"/>
	<xsl:variable name="sum_payment_amount" select="sum(Booking/Payments/Payment/payment_amount)"/>

	<xsl:template name="CDate2Julian">
		<xsl:param name="date"/>
		<xsl:param name="format" select="000"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="format-number(substring($date,3,2),'00')"/>
		<xsl:variable name="month" select="substring(substring-after('01/00002/03103/05904/09005/12006/15107/18108/21209/24310/27311/30412/334', concat($month1,'/')), 1, 3)"/>
		<xsl:value-of select="$day + $month"/>
	</xsl:template>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($day, '/', $month, '/', $year)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:value-of select="concat($day, '.', $month, '.', $year)"/>
	</xsl:template>

	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$Titel"/>
				</TITLE>
				<!--Style Sheet-->
				<STYLE>.HeaderBackground { background: url(http://www.tiksystems.com/tikaero/Images/0/H/hhe.gif) fixed no-repeat left center; }
					.base { color: ; font-family: 12pt Tahoma, Arial, sans-serif, verdana; font-size: 9pt; width: 100%; }
					.base .content{ color: ; float: left; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 9pt; }
					.dummy{ }
					.base .content .contentHeader{ margin: 5px 0px 0px 0px; padding: 0px 0px 0px 5px; }
					.base .quickSearch { float: left; width: 170px; }					
					.base .quickSearch .quickSearchHeader{ background: url(http://www.tiksystems.com/tikaero/Images/0/H/hqs.gif) fixed no-repeat left center; padding: 10px 0px 5px 0px; }
					.base .quickSearch DIV.input { font-family: Tahoma, Arial, sans-serif, verdana; font-size: 8pt; float: left; padding: 2px 4px 2px 5px; text-align: left; }
					.base .quickSearch DIV.label{ float: left; padding: 2px 4px 2px 5px; text-align: right; vertical-align: middle; }
					.BarcodeHeader	{ color: ; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 7pt; font-style: normal; font-weight: bold; padding: 0px 0px 0px 12px; text-align: left; width: 100%; }
					.PanelItinerary .Barcode	{ padding: 1 5 1 5; }
					.footer	{ background: url(http://www.tiksystems.com/tikaero/Images/0/I/bgb.gif); color: #201384; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 8pt; padding: 15px 5px 0px 5px; width: 100%; }
					.footer A	{ height: 20px; text-align: right; width: 50%; }
					.footer	SPAN	{ height: 20px; padding-top: 3px; width: 50%; }
					.grid TD.gridItems0Button	{ padding: 1px 5px 1px 1px; text-align: right; }
					.grid TR.GridFooter TD	{ background-color: transparent; border-bottom: 1px solid #666666; color: #666666; font: bold 8pt Tahoma, Arial, sans-serif, verdana;   border-top: 1px solid #666666; height: 22px;  }
					.grid TR.GridFooter TD.Summary	{ background-color: transparent; color: ; }
					.grid TR.gridHeader	{ background-color: transparent; color: #000000; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.GridPrice	{ background-color: tranparent; color: ; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridHeader TD	{ padding: 7px 2px 1px 5px; border-bottom: 1px solid #666666; }
					.grid TR.gridHeaderFight { background-color: #FED684; color: ; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridItems0 TD { border-bottom: 1px solid #666666; color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems1 TD { border-top: 1px solid #666666; color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid tr.griditems2 td{color:;cursor:pointer;font-size:11px;font-weight:normal;height:15px;padding:0px 1px 0px 5px;}
					.grid TR.gridItems02 TD { color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems0 TD { color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; }
					.grid TR.gridItems01 TD { color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0o TD{ background-color: #FFBD3B; border-bottom: 1px solid #FFCC00; color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01 TD	{ color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01o TD	{ background-color: #CEE6F7; color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0Input	{ color: ; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 5px 1px 1px 5px; }
					.header	{ width: 100%; }
					.header .subHeader	{ background-color: ; }
					/* End Forms \*/
					/* Headers \*/
					.headers	{ font: bold 12pt Tahoma, Arial, sans-serif, verdana; /*background: url(../Images/0/H/hbh.jpg);*/ margin: 5px 0px 5px 5px; width: 100%; }
					.headers .ItineraryGrid	{ background: url(http://www.tiksystems.com/tikaero/Images/0/I/iin.gif) fixed no-repeat left center; }
					.headers .SearchResultGrid{ background: url(http://www.tiksystems.com/tikaero/Images/0/I/isr.gif) fixed no-repeat left center; }
					.headers DIV	{ font: bold 8pt Tahoma, Arial, sans-serif, verdana; margin: 3px 0px 3px 0px; padding: 5px 0px 5px 17px; }
					.headers DIV SPAN.S0{ color: #FF6600; }
					.headers SPAN.Button { width: 49%; }
					.Itinerary	{ color: ; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 8pt; font-style: normal; padding: 10px 12px 0px 12px; text-align: left; width: 100%; }
					.Itinerary Span.O	{ color: #FF6600; font-weight: bold; }
					.Itinerary Span.1	{ color: #FF6600; font-weight: bold; text-decoration: underline; }
					.Itinerary Span.2 { color: ; font-weight: bold; }</STYLE>
				<!--End Style Sheet-->
			</HEAD>
			<BODY leftmargin="10px;" topmargin="10" rightmargin="10" bottommargin="10" marginwidth="10" marginheight="10">
				<DIV style="width:100%;">
					<DIV class="header" align="Center" style="width:100%;">
						<BR/>
						<BR/>
						<BR/>
						<table border="0" id="table1" width="100%" height="99" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top" width="300">
									<DIV class="headers" style="width: 250px; height: 89px;font-family: helvetica, Tahoma, Arial, sans-serif, verdana;">
										<table style="width: 100%" class="headers">
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 12pt;font-weight: 400">
														<font color="#000000">Your Booking:</font>
													</SPAN>
												</td>
												<td style="width: 183px">
													<xsl:value-of select="Booking/Header/BookingHeader/record_locator"/>
												</td>
											</tr>
											<tr>
												<td style="width: 183px"></td>
												<td style="width: 183px"></td>
											</tr>
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;font-weight: 400">
														<font color="#000000">Agency Code:</font>&#x20;
													</SPAN>
												</td>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;">
														<xsl:value-of select="Booking/Header/BookingHeader/agency_code"/>
													</SPAN>
												</td>
											</tr>
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;font-weight: 400">
														<font color="#000000">Booking Date:</font>&#x20;
													</SPAN>
												</td>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;">
														<xsl:call-template name="formatdate">
															<xsl:with-param name="date" select="Booking/Header/BookingHeader/create_date_time"/>
														</xsl:call-template>
													</SPAN>
												</td>
											</tr>
										</table>
										<BR/>
									</DIV>
								</td>
								<td>
								</td>
								<td align="center" valign="top" width="14%">
									<xsl:for-each select="//Booking/Itinerary/FlightSegment[not(segment_status_rcd='XX')]">
										<xsl:if test="position()=1">
											<div class="Barcode" style="height: 19px">
												<table width="100%" border="0">
													<tbody>
														<tr>
															<td align="center" width="100%">
																<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="164" height="57"/>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</xsl:if>
									</xsl:for-each>
								</td>
							</tr>
						</table>
					</DIV>
					<DIV class="base">
						<!--Passengers-->
						<DIV class="headers">
							<SPAN style="font-size: 10pt;">Passengers&#32;</SPAN>
							<TABLE class="Grid" cellspacing="0"  border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
								<TR class="gridHeader">
									<TD>&#32;</TD>
									<TD>Lastname</TD>
									<TD>Firstname</TD>
									<TD>Title</TD>
									<TD>Type</TD>
								</TR>
								<xsl:for-each select="Booking/Passengers/Passenger">
									<xsl:variable name="date_of_birth" select="date_of_birth"/>
									<TR class="gridItems0">
										<TD>
											<xsl:choose>
												<xsl:when test="position()&gt;9">
													<xsl:value-of select="concat('0','',string(position()))"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="concat('00','',string(position()))"/>
												</xsl:otherwise>
											</xsl:choose>
										</TD>
										<TD>
											<xsl:value-of select="lastname"/>
										</TD>
										<TD>
											<xsl:value-of select="firstname"/>
										</TD>
										<TD>
											<xsl:value-of select="title_rcd"/>
										</TD>
										<TD>
											<xsl:value-of select="passenger_type_rcd"/>
										</TD>
									</TR>
								</xsl:for-each>
							</TABLE>
						</DIV>
						<br/>
						<!--End Passengers-->
						<!--Flights-->
						<DIV class="headers">
							<SPAN style="font-size: 10pt;">Flights&#32;</SPAN>
							<TABLE class="Grid" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
								<TR class="gridHeader">
									<TD/>
									<TD>Flight</TD>
									<TD>From</TD>
									<TD>To</TD>
									<TD>Date</TD>
									<TD>Dep</TD>
									<TD>Arr</TD>
									<TD>Class</TD>
									<TD>Status</TD>
								</TR>
								<xsl:for-each select="Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
									<TR class="gridItems0">
										<TD>
											<xsl:value-of select="concat('00','',string(position()))"/>
										</TD>
										<TD>
											<SPAN>
												<xsl:value-of select="airline_rcd"/>&#32;<xsl:value-of select="flight_number"/></SPAN>
										</TD>
										<TD>
											<xsl:value-of select="origin_name"/>&#32;(<xsl:value-of select="origin_rcd"/>)</TD>
										<TD>
											<xsl:value-of select="destination_name"/>&#32;(<xsl:value-of select="destination_rcd"/>)</TD>
										<TD>
											<xsl:if test="string-length(departure_date) &gt; '0'">
												<xsl:call-template name="formatdate">
													<xsl:with-param name="date" select="departure_date"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="string-length(departure_date) = '0'">
												<xsl:text>OPEN</xsl:text>
											</xsl:if>
										</TD>
										<TD>
											<xsl:if test="string(departure_time) != '0'">
												<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
										</TD>
										<TD>
											<xsl:if test="string(planned_arrival_time) != '0'">
												<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/></xsl:if>
										</TD>
										<TD>
											<xsl:value-of select="class_name"/>
										</TD>
										<td>
											<xsl:choose>
												<xsl:when test="flight_id != ''">
													<xsl:value-of select="status_name"/>
													<!--<xsl:value-of select="segment_status_rcd"/>-->
												</xsl:when>
												<xsl:when test="not(string-length(departure_date) &gt; '0')">
													<xsl:text>&#32;</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text>Info</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</TR>
								</xsl:for-each>
							</TABLE>
						</DIV>
						<br/>
						<!--End Flights-->
						<!--Auxiliaries-->
						<xsl:for-each select="Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
							<xsl:if test="position()=1">
								<div class="headers">
									<span style="font-size: 10pt">Auxiliaries</span>
									<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0"  border="0"
									       style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
										<tbody>
											<tr class="gridHeader">
												<td align="left">Code</td>
												<td align="left">Description</td>
											</tr>
											<xsl:for-each select="/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">

												<tr class="gridItems0">
													<td align="left" valign="top">
														<xsl:value-of select="display_name"/>
													</td>
													<td align="left" valign="top">
														<xsl:value-of select="remark_text"/>
													</td>
												</tr>
											</xsl:for-each>
										</tbody>
									</table>
								</div>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<!--End Auxiliaries-->
						<!--Seat number-->
						<xsl:for-each select="Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
							<xsl:sort select="seat_number" order="descending"/>
							<xsl:sort select="ticket_number" order="descending"/>
							<xsl:sort select="restriction_text" order="descending"/>
							<xsl:sort select="endorsement_text" order="descending"/>
							<xsl:if test="(string-length(seat_number) &gt;0) or (string-length(ticket_number) &gt;0) or (string-length(restriction_text) &gt;0) or (string-length(endorsement_text) &gt;0)">
								<xsl:if test="position()=1">
									<div class="headers">
										<span style="font-size: 10pt">Tickets and Seats</span>
										<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0"  border="0"
										       style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
											<tbody>
												<tr class="gridHeader">
													<td align="left">Flight</td>
													<td align="left">Passenger Name</td>
													<td align="left">Ticket</td>
													<td align="left">Seat</td>
													<td align="left">Endorsement &amp; Restrictions</td>
													<!--<td align="right" width="5%">&#160;</td>-->
												</tr>
												<xsl:for-each select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
													<xsl:if test="(string-length(seat_number) &gt;0) or (string-length(ticket_number) &gt;0) or (string-length(restriction_text) &gt;0) or (string-length(endorsement_text) &gt;0)">
														<tr class="gridItems0">
															<td align="left" valign="top">
																<xsl:value-of select="airline_rcd"/>&#32;<xsl:value-of select="flight_number"/></td>
															<td align="left" valign="top">
																<xsl:value-of select="lastname"/>/&#32;<xsl:value-of select="firstname"/></td>
															<td align="left" valign="top">
																<xsl:value-of select="ticket_number"/>
															</td>
															<td align="left" valign="top">
																<xsl:value-of select="seat_number"/>
															</td>
															<td align="left" valign="top">
																<xsl:choose>
																	<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) &gt;0)">
																		<xsl:value-of select="endorsement_text"/>
																		<br/>
																		<xsl:value-of select="restriction_text"/>
																	</xsl:when>
																	<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) =0)">
																		<xsl:value-of select="restriction_text"/>
																	</xsl:when>
																	<xsl:when test="(string-length(restriction_text) =0) and (string-length(endorsement_text) &gt;0)">
																		<xsl:value-of select="endorsement_text"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="restriction_text"/>
																	</xsl:otherwise>
																</xsl:choose>
															</td>
														</tr>
													</xsl:if>
												</xsl:for-each>
											</tbody>
										</table>
									</div>
									<br/>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
						<!--End Seat number-->
						<!--Special Services-->
						<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')]">
							<xsl:sort select="departure_date" order="descending"/>
							<xsl:if test="position()=1">
								<div class="headers">
									<span style="font-size: 10pt">Special Services&#32;</span>
									<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0"  border="0"
									       style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
										<tbody>
											<tr class="gridHeader">
												<td>&#32;</td>
												<td>Flight</td>
												<td>Passenger Name</td>
												<td>Status</td>
												<td>Special Service</td>
												<td>Text</td>
											</tr>
											<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd = 'XX')]">
												<tr class="gridItems0">
													<td>
														<xsl:value-of select="concat('00','',string(position()))"/>
													</td>
													<td>
														<xsl:value-of select="airline_rcd"/>&#32;<xsl:value-of select="flight_number"/></td>
													<td>
														<xsl:value-of select="lastname"/>/&#32;<xsl:value-of select="firstname"/></td>
													<td>
														<xsl:value-of select="special_service_status_rcd"/>
													</td>
													<td>
														<xsl:value-of select="special_service_rcd"/><xsl:text>&#32;</xsl:text><xsl:value-of select="display_name"/></td>
													<td>
														<xsl:value-of select="service_text"/>
													</td>
												</tr>
											</xsl:for-each>
										</tbody>
									</table>
								</div>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<!--End Special Services-->
						<!--Specifying page breaks for printing using CSS-->
						<!--<DIV style="page-break-after:always"><br/></DIV>-->
						<!--End Specifying page breaks for printing using CSS-->
						<!--Quote-->
						<!--AAS, ABA, AEK, GMG, PMT, BEA, IRS, CTS -->
						<xsl:if test="//TicketQuotes/Total = true()">
							<DIV class="headers">
								<SPAN style="font-size: 10pt">Quotes</SPAN>
								<TABLE cellspacing="0"  border="0" class="Grid" style="border-width:0px;width:100%;border-collapse:collapse;">
									<TR class="GridHeader">
										<TD>Passenger</TD>
										<td align="center">Units</td>
										<TD>Charge</TD>
										<td align="right">Amount</td>
										<td align="right">PPN</td>
										<td align="right">Total</td>
									</TR>
									<xsl:for-each select="//Passengers/Passenger[count(. | key('passenger_type_rcd_group', passenger_type_rcd)[1]) = 1]">
										<xsl:variable name="passenger_type_rcd" select="passenger_type_rcd"/>
										<xsl:for-each select="//TicketQuotes/Total[passenger_type_rcd=$passenger_type_rcd]">
											<xsl:variable name="TotalCharge">
												<xsl:if test="charge_type != 'REFUND'">
													<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
												</xsl:if>
												<xsl:if test="charge_type = 'REFUND'">
													<xsl:value-of select="sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
												</xsl:if>
											</xsl:variable>
											<xsl:if test="position()=1">
												<xsl:if test="position()!=last()">
													<tr class="gridItems1">
														<td>
															<xsl:value-of select="passenger_type_rcd"/>
														</td>
														<td align="center">
															<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)"/>
														</td>
														<td>
															<xsl:value-of select="charge_name"/>
														</td>
														<td align="right">
															<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
														</td>
														<td align="right">
															<xsl:if test="number(tax_amount) != 0">
																<xsl:if test="number(tax_amount) &gt;= 0">
																	<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
																</xsl:if>
															</xsl:if>
														</td>
														<td align="right">
															<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/></td>
													</tr>
												</xsl:if>
												<xsl:if test="position()=last()">
													<tr class="gridItems1">
														<td>
															<xsl:value-of select="passenger_type_rcd"/>
														</td>
														<td align="center">
															<xsl:value-of select="passenger_count"/>
														</td>
														<td>
															<xsl:value-of select="charge_name"/>
														</td>
														<td align="right">
															<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
														</td>
														<td align="right">
															<xsl:if test="number(tax_amount) != 0">
																<xsl:if test="number(tax_amount) &gt;= 0">
																	<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
																</xsl:if>
															</xsl:if>
														</td>
														<td align="right">
															<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/></td>
													</tr>
												</xsl:if>
											</xsl:if>
											<xsl:if test="position()!=1">
												<xsl:if test="position()=last()">
													<xsl:if test="charge_type != 'REFUND'">
														<tr class="gridItems02">
															<td>&#32;</td>
															<td>&#32;</td>
															<td>
																<xsl:value-of select="charge_name"/>
															</td>
															<td align="right">
																<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
															</td>
															<td align="right">
																<xsl:if test="number(tax_amount) != 0">
																	<xsl:if test="number(tax_amount) &gt;= 0">
																		<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
																	</xsl:if>
																</xsl:if>
															</td>
															<td>&#32;</td>
														</tr>
													</xsl:if>
													<xsl:if test="charge_type = 'REFUND'">
														<tr class="gridItems02">
															<td>&#32;</td>
															<td align="center">&#32;</td>
															<td>
																<xsl:value-of select="charge_name"/>
															</td>
															<td align="right">&#32;</td>
															<td align="right">&#32;</td>
															<td align="right">-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/></td>
														</tr>
													</xsl:if>
												</xsl:if>
												<xsl:if test="position()!=last()">
													<xsl:if test="charge_type != 'REFUND'">
														<tr class="gridItems02">
															<td>&#32;</td>
															<td>&#32;</td>
															<td>
																<xsl:value-of select="charge_name"/>
															</td>
															<td align="right">
																<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
															</td>
															<td align="right">
																<xsl:if test="number(tax_amount) != 0">
																	<xsl:if test="number(tax_amount) &gt;= 0">
																		<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
																	</xsl:if>
																</xsl:if>
															</td>
															<td>&#32;</td>
														</tr>
													</xsl:if>
													<xsl:if test="charge_type = 'REFUND'">
														<tr class="gridItems02">
															<td>&#32;</td>
															<td align="center">&#32;</td>
															<td>
																<xsl:value-of select="charge_name"/>
															</td>
															<td align="right">&#32;</td>
															<td align="right">&#32;</td>
															<td align="right">-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/></td>
														</tr>
													</xsl:if>
												</xsl:if>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
									<xsl:for-each select="//Fees/Fee[void_date_time = '']">
										<xsl:if test="position()=1">
											<tr class="gridItems1">
												<td>&#32;</td>
												<td>&#32;</td>
												<td>
													<xsl:value-of select="display_name"/>
												</td>
												<td align="right">
													<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
												</td>
												<td align="right">
													<xsl:choose>
														<xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">&#32;</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>
														</xsl:otherwise>
													</xsl:choose>
												</td>
												<td align="right">
													<xsl:value-of select="format-number(sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/></td>
											</tr>
										</xsl:if>
										<xsl:if test="position()!=1">
											<tr class="gridItems02">
												<td>&#32;</td>
												<td>&#32;</td>
												<td>
													<xsl:value-of select="display_name"/>
												</td>
												<td align="right">
													<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
												</td>
												<td align="right">
													<xsl:choose>
														<xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">&#32;</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>
														</xsl:otherwise>
													</xsl:choose>
												</td>
												<td>&#32;</td>
											</tr>
										</xsl:if>
									</xsl:for-each>
									<tr class="GridFooter">
										<td>&#32;</td>
										<td>&#32;</td>
										<td>&#32;</td>
										<td>&#32;</td>
										<td class="Summary" align="right">
											<span>Total</span>
										</td>
										<td align="right">
											<SPAN class="FooterTotalLabel">
												<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
										</td>
									</tr>
								</TABLE>
							</DIV>
						</xsl:if>
						<!--End Quote-->
						<!--Payments-->
						<xsl:if test="//Payments/Payment = true()">
							<br/>
							<div class="headers">
								<span style="font-size: 10pt">Payments</span>
								<table class="Grid" width="100%" border="0" ellspacing="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; border-collapse: collapse; border-right-width: 0px">
									<tbody>
										<tr class="gridHeader">
											<td align="left" width="30%">
												<span class="gridHeader">Description</span>
											</td>
											<td align="left" width="30%">
												<span class="gridHeader">&#32;</span>
											</td>
											<td align="left" width="12%">
												<span class="GridHeader">Status, Date</span>
											</td>
											<td align="right" width="15%">
												<span class="gridHeader">Credit</span>
											</td>
											<td align="right" width="15%">
												<span class="gridHeader">Debit</span>
											</td>
										</tr>
										<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
										<xsl:variable name="Ticket_total"
										              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>
										<xsl:choose>
											<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">
												<tr class="gridItems0" vAlign="middle" align="left">
													<TD align="left">Ticket Cost &amp; Fee</TD>
													<TD align="left">&#32;</TD>
													<TD align="left">&#32;</TD>
													<xsl:if test="starts-with(string($Ticket_total), '-')">
														<TD align="left">&#32;</TD>
														<TD align="right">
															<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></TD>
													</xsl:if>
													<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
														<TD align="right">
															<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></TD>
														<TD align="left">&#32;</TD>
													</xsl:if>
												</tr>
												<xsl:for-each select="Booking/Payments/Payment[void_date_time='']">
													<xsl:if test="form_of_payment_rcd !=''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='CASH'">
																	<xsl:value-of select="form_of_payment_rcd"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='TKT'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CRAGT'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>															
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:choose>
																	<xsl:when test="void_date_time != ''">
																		<xsl:text>XX</xsl:text>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:if test="payment_date_time != ''">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</xsl:if>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
												</xsl:for-each>
												<tr class="gridItems0" vAlign="middle" align="left">
													<xsl:variable name="SubTotal" select="$Ticket_total - $Payment_total"/>
													<TD align="left">
														<xsl:choose>
															<xsl:when test="number($SubTotal) &lt; 0">
																<SPAN class="Summary">
																	<xsl:text>OVERPAID BALANCE</xsl:text>
																</SPAN>
															</xsl:when>
															<xsl:otherwise>
																<SPAN class="Summary">
																	<xsl:text>OUTSTANDING BALANCE</xsl:text>
																</SPAN>
															</xsl:otherwise>
														</xsl:choose>
													</TD>
													<TD align="left">&#32;</TD>
													<TD align="left">&#32;</TD>

													<xsl:if test="number($SubTotal) = 0">
														<TD align="left">&#32;</TD>
														<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
															<SPAN class="Summary">0.00&#32;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
														</TD>
													</xsl:if>
													<xsl:if test="number($SubTotal) != 0">
														<xsl:if test="number($SubTotal) &gt;= 0">
															<TD align="left">&#32;</TD>
															<TD align="right">
																<SPAN class="Summary">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
															</TD>
														</xsl:if>
														<xsl:if test="number($SubTotal) &lt; 0">
															<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
																<SPAN class="Summary">
																	<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
															</TD>
															<TD align="left">&#32;</TD>
														</xsl:if>
													</xsl:if>
												</tr>
											</xsl:when>
											<xsl:otherwise>
												<TR valign="middle" align="left" class="gridItems0">
													<TD align="left">Ticket Cost &amp; Fee</TD>
													<TD align="left">&#32;</TD>
													<TD align="left">&#32;</TD>
													<xsl:if test="starts-with(string($Ticket_total), '-')">
														<TD align="left">&#32;</TD>
														<TD align="right">
															<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></TD>
													</xsl:if>
													<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
														<TD align="right">
															<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></TD>
														<TD align="left">&#32;</TD>
													</xsl:if>
												</TR>
												<xsl:for-each select="Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)]">
													<xsl:if test="form_of_payment_subtype !=''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
															<TD align="left">
																<xsl:value-of select="form_of_payment_subtype"/>
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:choose>
																	<xsl:when test="void_date_time != ''">
																		<xsl:text>XX</xsl:text>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:if test="payment_date_time != ''">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</xsl:if>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
													<xsl:if test="form_of_payment_subtype =''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
															<TD align="left">
																<xsl:value-of select="form_of_payment_rcd"/>
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:if test="payment_date_time != ''">
																	<xsl:call-template name="formatdate">
																		<xsl:with-param name="date" select="payment_date_time"/>
																	</xsl:call-template>
																</xsl:if>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
												</xsl:for-each>
												<TR valign="middle" align="left" class="gridItems0">
													<TD align="left">
														<SPAN class="Summary">OUTSTANDING BALANCE</SPAN>
													</TD>
													<TD align="left">&#32;</TD>
													<TD align="left">&#32;</TD>
													<xsl:variable name="SubTotal" select="number($Ticket_total) - number($Payment_total)"/>
													<xsl:if test="number($SubTotal) = 0">
														<TD align="left">&#32;</TD>\
														<TD align="right">
															<SPAN class="Summary">0.00&#32;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
														</TD>
													</xsl:if>
													<xsl:if test="number($SubTotal) != 0">
														<xsl:if test="number($SubTotal) &gt;= 0">
															<TD align="left">&#32;</TD>
															<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
																<SPAN class="Summary">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
															</TD>
														</xsl:if>
														<xsl:if test="number($SubTotal) &lt; 0">
															<TD align="right">
																<SPAN class="Summary">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/><xsl:text>&#32;</xsl:text><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
															</TD>
															<TD align="left">&#32;</TD>
														</xsl:if>
													</xsl:if>
												</TR>
											</xsl:otherwise>
										</xsl:choose>
									</tbody>
								</table>
							</div>
						</xsl:if>
						<!--End Payments-->
						<br/>
					</DIV>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2008. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="no" externalpreview="no" url="file:///c:/EXEs/XML/LVB5GG.XML" htmlbaseurl="" outputurl="" processortype="internal" useresolver="yes" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator=""/>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->