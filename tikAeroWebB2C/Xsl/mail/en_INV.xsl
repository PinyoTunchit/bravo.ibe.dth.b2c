<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
												 xmlns:doc="http://xsltsl.org/xsl/documentation/1.0" 
												 xmlns:dt="http://xsltsl.org/date-time" 
												 xmlns:str="http://xsltsl.org/string" 
												 extension-element-prefixes="doc str">
	<xsl:import href="string.xsl"/>
	<xsl:import href="date-time.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:variable name="SumRowsCount" select="count(//LineItems/Item)"/>
	<!--MAIN-->
	<xsl:template match="Document">
		<html>
			<head>
				<meta content="en-us" http-equiv="Content-Language"/>
				<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
				<title>Invoice</title>
				<!--<link type="text/css" rel="stylesheet" href="D:\Development\xsl_html_pagebreak\StyleSheet.css"/>-->
				<style type="text/css">
						table { border-spacing: 0px; empty-cells: show; margin: 0px; padding: 0px; }
						td{ font-family: " helvetica", Tahoma, Arial, sans-serif, verdana; font-size: 8pt; vertical-align: top; }
						th{ color: #002256; font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 8pt; text-align: center; border-left: 0px; border-right: 0px; border: .09mm solid #002256   }
						.blueline{ border-bottom: solid #002256 1px; border: .09mm solid #002256 }
						.documentheader{ color: #002256; font-size: 9pt; font-weight: bold; }
						.documenttotal{ border-bottom: solid #002256 1px; color: #002256; font-size: 9pt; font-weight: bold; }
						.imglogo{ border-color: White; border-style: none; vertical-align: top; }
						.pagebreak { page-break-after: always; }
						.tabledetails{ border-left: solid #002256 0px; border-right: solid #002256 0px; width: 759px;color: #002256; }
						.pagebreak 		 	{ page-break-after: always; }
						.tablereportheader{ border-left: solid #002256 0px; border-right: solid #002256 0px; border-top: solid #002256 0px; width: 759px; }
						.tdmargin{ width: 20px; }
						.tdorderheader{ border: solid 1px #002256;  border-right: 0px;  border-left: 0px;   }
						.tdtotalmargin{ width: 450px; }
						.tablereportfooter	{ border-bottom: solid #002256 0px; border-left: solid #002256 0px; border-right: solid #002256 0px; bottom: 2px; height: 175px; width: 759px; }
						.imgfooter { background-image: url('http://www.tikaero.com/xslimages/isk/ISK_footer.gif'); left: 10px; border-width: 0 0 0 15px; border-top-style: solid; border-left-style: solid; border-left-color: #FFFFFF}
						.tdheader{color: #031668; font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size:13pt; text-align: left; font-weight: bold;  border-left: 0px; border-right: 0px; border-style: none}
				</style>
			</head>
			<body>
				<xsl:copy-of select="$ReportHeader"/>
				<xsl:call-template name="ShowTotalPages">
					<xsl:with-param name="CurPosition" select="1"/>
					<xsl:with-param name="TotalRowsCount" select="$SumRowsCount"/>
				</xsl:call-template>
				<xsl:copy-of select="$ReportNumber"/>
				<xsl:copy-of select="$OrderRecipient"/>
				<xsl:copy-of select="$OrderRowsHeader"/>
				<xsl:for-each select="LineItems/Item">
					<xsl:sort select="firstname" order="ascending"/><xsl:sort select="lastname" order="ascending"/>
					<xsl:sort select="departure_date" order="ascending"/>
					<xsl:sort select="e_ticket_flag" order="descending"/>

					<xsl:variable name="CurPo" select="position()"/>
					<xsl:variable name="FlightID" select="flight_id"/>
					<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
					<xsl:variable name="passenger_id" select="passenger_id"/>
					<xsl:variable name="strOri" select="origin_rcd"/>
					<xsl:variable name="strDesi" select="destination_rcd"/>
					<xsl:variable name="VAT"><xsl:call-template name="FloatFormat"><xsl:with-param name="Input" select="number(payment_amount - payment_amount_excl)"/></xsl:call-template></xsl:variable>

					<!--<xsl:variable name="payment_amount_excl" select="net_total div (1+ vat_percentage div 100)"/>-->
					<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
						<tr>
							<td class="tdmargin"/>
							<td class="blueline" style="width:60px">
								<xsl:value-of select="record_locator"/>
								&#xA0;</td>
							<td style="width:81px" align="left" class="blueline">
								<xsl:if test="string-length($strOri) &gt; 0">
									<xsl:value-of select="$strOri"/>&#xA0;-&#xA0;<xsl:value-of select="$strDesi"/>
								</xsl:if>
								&#xA0;</td>
							<td class="blueline" style="width:60px">
								<xsl:value-of select="airline_rcd"/>
								<xsl:value-of select="flight_number"/>&#xA0;</td>
							<td style="width:76px" align="left" class="blueline">
								<xsl:if test="string-length(departure_date) &gt; 0">
									<xsl:call-template name="dt:ddmmmyyyy-en">
										<xsl:with-param name="date" select="departure_date"/>
									</xsl:call-template>
								</xsl:if>&#xA0;</td>
							<td style="width:200px" align="left" class="blueline">
								<xsl:if test="string-length(lastname) &gt; 0">
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="lastname"/>
									</xsl:call-template>/&#xA0;<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="firstname"/>
									</xsl:call-template>
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="title_rcd"/>
									</xsl:call-template>
								</xsl:if>&#xA0;</td>
							<td style="width:86px" align="left" class="blueline">
								<xsl:if test="(fare_type_rcd = 'FARE')">
									<xsl:text>Ticket</xsl:text>&#xA0;
									(<xsl:value-of select="booking_class_rcd"/>)
								</xsl:if>
								<xsl:if test="not(fare_type_rcd = 'FARE')">
									<xsl:value-of select="fee_rcd"/>&#xA0;
								</xsl:if>
								&#xA0;
							</td>
							<td style="width:78px" align="right" class="blueline">
								<xsl:if test="number($VAT) != 0">
									<xsl:value-of select="concat('  ', format-number($VAT,'#,##0.00'))"/>
								</xsl:if>&#xA0;
							</td>
							<td style="width:78px" align="right" class="blueline">
								<xsl:if test="number(payment_amount_excl) != 0">
									<xsl:variable name="payment_amount_excl"><xsl:call-template name="FloatFormat"><xsl:with-param name="Input" select="number(payment_amount_excl)"/></xsl:call-template></xsl:variable>
									<xsl:value-of select="concat('  ', format-number($payment_amount_excl ,'#,##0.00'))"/>	
								</xsl:if>&#xA0;
							</td>
							<td class="tdmargin"/>
						</tr>
					</table>
					<xsl:if test="(position() mod 38) = 0 ">
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="1"/>
						</xsl:call-template>
						<xsl:copy-of select="$ReportFooter"/>
						<DIV style="page-break-after:always"><br/></DIV>
						<xsl:copy-of select="$ReportHeader"/>
						<xsl:call-template name="ShowTotalPages">
							<xsl:with-param name="CurPosition" select="$CurPo"/>
							<xsl:with-param name="TotalRowsCount" select="$SumRowsCount"/>
						</xsl:call-template>
						<xsl:copy-of select="$ReportNumber"/>
						<xsl:copy-of select="$OrderRecipientBlank"/>
						<xsl:copy-of select="$OrderRowsHeader"/>
					</xsl:if>
				</xsl:for-each>
				<!--Filler -->
				<xsl:choose>
					<!-- case of only one page-->  
					<!--Need to update line count on "ShowTotalPages" too-->
					<xsl:when test="count(LineItems/Item) &lt; 38">
						<xsl:copy-of select="$GrandTotal"/>
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="38 - (count(LineItems/Item)+10)"/>
						</xsl:call-template>
					</xsl:when>
					<!-- case of more than one page-->
					<xsl:otherwise>
						<xsl:copy-of select="$GrandTotal"/>
						<xsl:call-template name="Filler">
							<!--(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)-->
							<!--GrandTotal 4 row + Term 4 rows-->
							<xsl:with-param name="fillercount" select="38 - ( ( count(LineItems/Item)-38 ) mod 38 ) - 11"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
				<!--End Filler -->
				<xsl:copy-of select="$TermsOfPayment"/>
				<xsl:copy-of select="$ReportFooter"/>
			</body>
		</html>
	</xsl:template>
	<!--MAIN END-->

	<!--ALL TEMPLATE-->
	<xsl:template name="Filler">
		<xsl:param name="fillercount" select="1"/>
		<xsl:if test="$fillercount &gt; 0">
			<table class="tabledetails">
				<tr>
					<td>
						&#xA0;
					</td>
				</tr>
			</table>
			<xsl:call-template name="Filler">
				<xsl:with-param name="fillercount" select="$fillercount - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="ShowTotalPages">
		<xsl:param name="CurPosition"/>
		<xsl:param name="TotalRowsCount"/>
		<table class="tableReportHeader" cellspacing="0">
			<tr>
				<td class="tdmargin"/>
				<td style="font-size: 8pt;color:#002256; font-family: Arial;" >
					<xsl:choose>  
						<xsl:when test='($CurPosition mod $TotalRowsCount) != "0"'>
							<xsl:choose>
								<xsl:when test='$CurPosition = 1'>
									<xsl:text>Page 1 of</xsl:text>&#xA0;<xsl:value-of select="floor($TotalRowsCount div 38) +1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Page</xsl:text>&#xA0;<xsl:value-of select="round($CurPosition div 38) + 1"/>&#xA0;of&#xA0;<xsl:value-of select="floor ($TotalRowsCount div 38) +1"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>Page</xsl:text>&#xA0;<xsl:value-of select="floor(round($CurPosition) div 38) +1"/>&#xA0;of&#xA0;<xsl:value-of select="floor ($TotalRowsCount div 38) +1"/>
						</xsl:otherwise>
					</xsl:choose>				
				</td>
			</tr>
		</table>
	</xsl:template>
	
	<!--ALL TEMPLATE END-->

	<!-- ALL VARIABLE -->
	<xsl:variable name="ReportHeader">
		<table class="tableReportHeader" cellspacing="0">
			<tr>
				<td class="tdmargin"/>
				<td style="font-size: 16pt;color:#002256; font-family: Arial;" ><strong>
					InterSky Summary Invoice</strong>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="ReportNumber">
		<table class="tabledetails" cellspacing="0" style="width: 580px;">
			<tr>
				<td class="tdmargin"/>
				<td>&#xA0;</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td>Bregenz (Austria)&#xA0;<span class="tableheader" style="padding-left: 0px;">
					<xsl:if test="string-length(/Document/Header/DocumentHeader/create_date_time) &gt; 0">
						<xsl:call-template name="dt:ddmmmmyyyy-en">
							<xsl:with-param name="date" select="/Document/Header/DocumentHeader/create_date_time"/>
						</xsl:call-template>
					</xsl:if></span>
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td><strong>Invoice-Nbr:&#xA0;
				<xsl:if test="string-length(/Document/Header/DocumentHeader/document_number) &gt; 0">								
					<span class="tableheader" style="padding-left: 0px;">
						<xsl:value-of select="/Document/Header/DocumentHeader/document_number"/>&#xA0;
					</span>
				</xsl:if></strong>
				</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>

	<xsl:variable name="OrderRecipient">
		<table class="tabledetails">
			<tr>
				<td>

					<table class="tabledetails" cellspacing="0" style="width: 580px; height: 85px;">
						<tr>
							<td style="width: 15px"/>
							<td class="documentheader" style="width: 120px">Recipient&#xA0;</td>
							<!--							<td class="documentheader" style="width: 120px">&#xA0;</td>-->
							<td class="documentheader" style="width: 120px">Sender/Airline&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td style="width: 317px" rowspan="5">
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="//Document/Header/DocumentHeader/legal_name"/>
								</xsl:call-template>
								<br/>
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="//Document/Header/DocumentHeader/address_line1"/>
								</xsl:call-template>
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="//Document/Header/DocumentHeader/address_line2"/>
								</xsl:call-template>
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="//Document/Header/DocumentHeader/po_box"/>
								</xsl:call-template>
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="//Document/Header/DocumentHeader/street"/>
								</xsl:call-template>
								<br/>
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="//Document/Header/DocumentHeader/country_rcd"/>
								</xsl:call-template>&#xA0;-&#xA0;
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="//Document/Header/DocumentHeader/zip_code"/>
								</xsl:call-template>
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="//Document/Header/DocumentHeader/city"/>
								</xsl:call-template>
								<xsl:if test="string-length(/Booking/Header/BookingHeader/booking_tax_id)  &gt; 0"> 
									<br/>VAT-Nbr.:&#xA0;
									<xsl:call-template name="AddrFormat">
										<xsl:with-param name="value" select="//Document/Header/DocumentHeader/tax_id"/>
									</xsl:call-template>
								</xsl:if>								
							</td>
							<td>InterSky Luftfahrt GmbH</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>Bahnhofstrasse 10</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>6900 Bregenz</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>Austria</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td style="width: 15px"/>
							<td>VAT Nbr.: ATU 53122909</td>
							<td class="tdmargin"/>
						</tr>
					</table>
				</td>
				<td style="font-size: 9pt;" align="right" width="164">
					<!--<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="164" height="57"/>-->InterSky Call-Center <br/>Intl: +43 5574 48800 46 <br/>DE: +49 7541 286 96 84 <br/>AT: +43 5574 48800 46 <br/>CH: +41 31 819 72 22 <br/>
					reservation@intersky.biz
					<br/>Mo - Fr: 08:00 - 18:00 Uhr</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="OrderRecipientBlank">
		<table class="tabledetails">
			<tr>
				<td>
					<table class="tabledetails" cellspacing="0" style="width: 580px; height: 102px;">
						<tr>
							<td style="width: 15px"/>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="GrandTotal">
		<!--<xsl:for-each select="LineItems/Item">-->
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<td style="width:60px" align="left" class="blueline"/>
				<td class="blueline" style="width:81px"/>
				<td style="width:60px" align="left" class="blueline"/>
				<td style="width:76px" align="left" class="blueline"/>
				<td style="width:200px" align="left" class="blueline"/>
				<td style="width:86px" align="left" class="blueline">Net Total</td>
				<td style="width:78px" align="right" class="blueline">&#xA0;</td>
				<td style="width:78px" align="right" class="blueline">
					<xsl:variable name="sum_payment_amount_excl"><xsl:call-template name="FloatFormat"><xsl:with-param name="Input" select="sum(//LineItems/Item/payment_amount_excl)"/></xsl:call-template></xsl:variable>
					<xsl:value-of select="concat('  ', format-number($sum_payment_amount_excl,'#,##0.00'))"/>&#xA0;
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td style="width:60px" align="left" class="blueline"/>
				<td class="blueline" style="width:81px"/>
				<td style="width:60px" align="left" class="blueline"/>
				<td style="width:76px" align="left" class="blueline"/>
				<td style="width:200px" align="left" class="blueline"/>
				<td style="width:86px" align="left" class="blueline">VAT19%</td>
				<td style="width:78px" align="right" class="blueline">&#xA0;</td>
				<td style="width:78px" align="right" class="blueline">
					<xsl:variable name="sum_payment_amount_incl"><xsl:call-template name="FloatFormat"><xsl:with-param name="Input" select="sum(//LineItems/Item/payment_amount)"/></xsl:call-template></xsl:variable>
					<xsl:variable name="sum_payment_amount_excl1"><xsl:call-template name="FloatFormat"><xsl:with-param name="Input" select="sum(//LineItems/Item/payment_amount_excl)"/></xsl:call-template></xsl:variable>
					<xsl:value-of select="concat('  ', format-number($sum_payment_amount_incl - $sum_payment_amount_excl1,'#,##0.00'))"/>&#xA0;
					</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td style="width:60px" align="left" class="blueline"/>
				<td class="blueline" style="width:81px"/>
				<td style="width:60px" align="left" class="blueline"/>
				<td style="width:76px" align="left" class="blueline"/>
				<td style="width:200px" align="left" class="blueline"/>
				<td style="width:86px" align="left" class="blueline">Grand Total</td>
				<td style="width:78px" align="right" class="blueline">&#xA0;</td>
				<td style="width:78px" align="right" class="blueline">
					<xsl:variable name="sum_payment_amount_incl1"><xsl:call-template name="FloatFormat"><xsl:with-param name="Input" select="sum(//LineItems/Item/payment_amount)"/></xsl:call-template></xsl:variable>
					<xsl:value-of select="concat('  ', format-number($sum_payment_amount_incl1,'#,##0.00'))"/>&#xA0;
				</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="TermsOfPayment">
		<!--<xsl:for-each select="LineItems/Item">-->
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<td style="width:384px" align="left" class="tdheader" colspan="2">Issuance Details</td>
				<td style="width:10px"></td>
				<td style="width:322px" class="tdheader">Terms of Payment</td>
				<td class="tdmargin"/>
			</tr>
		</table>
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<td class="blueline" style="width:98px" align="left">Issuing Agency:</td>
				<td class="blueline" style="width:241px" align="left">Accounting / 3L</td>
				<td style="width:59px"></td>
				<td style="width:328px">To be paid immediately upon receipt of this invoice. No deductions.</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td class="blueline" >Issuing Place:</td>
				<td class="blueline" >InterSky - Bregenz / Austria</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
			<tr>
				<td class="tdmargin"/>
				<td class="blueline" >Remarks:</td>
				<td class="blueline" >&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
				<td>&#xA0;</td>
			</tr>
		</table>

	</xsl:variable>
	<xsl:variable name="ReportFooter">
		<table class="tableReportFooter">
			<tr>
				<td class="imgfooter">
					<table>
						<tr>
							<td style="font-size: 7pt; text-align: justify;border-top: solid #002256 0px;color: #002256;">
							International flights are taxfree according to § 9 ABS. 2 Z1 USTG 1999,  fares <br/>
							for domestic flights within Germany are subject to a VAT of 19%.<br/>
								<br/>InterSky Luftfahrt GmbH, Bahnhofstr. 10, A-6900 Bregenz   (AT)<br/>
							Firmenbuch-Nbr. FN 215648 f  | UID Nbr. ATU 53122909<br/>
							Hypo Vorarlberg, Bregenz (Austria) | Kto: 103 716 860 16 | BLZ: 58 000<br/>
							Baden-Württembergische Bank,  Konstanz | Kto: 142 64 25 | BLZ:  600 501 01<br/>
							Gerichtsstand, Bregenz (AT)<br/></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="OrderRowsHeader">
		<table class="tableReportHeader" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td style="width: 15px"/>
				<td style="width:500px;" class="tdheader">Booking Summary</td>
				<td class="tdmargin"/>
			</tr>
		</table>
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th style="width:60px; text-align: left;">PNR</th>
				<th style="width:81px; text-align: left;">Description</th>
				<th style="width:60px; text-align: left;">Flight</th>
				<th style="width:76px; text-align: left;">Date</th>
				<th style="width:200px; text-align: left;">Passenger Name</th>
				<th style="width:86px; text-align: left;">Type</th>
				<th style="width:78px">VAT</th>
				<th style="width:78px; text-align: right;">EUR&#xA0;&#xA0;</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<xsl:variable name="OrderTotals">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdtotalmargin"/>
				<td class="documenttotal" align="right">Subtotal:</td>
				<td class="blueline" align="right">
					<xsl:value-of select="/Data/Order/SubTotal"/>
					&#xA0;
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdtotalmargin"/>
				<td class="documenttotal" align="right">Freight:</td>
				<td class="blueline" align="right">
					<xsl:value-of select="/Data/Order/Freight"/>
					&#xA0;
				</td>
				<td class="tdmargin"/>
			</tr>
			<tr>
				<td class="tdtotalmargin"/>
				<td class="documenttotal" align="right">Total:</td>
				<td class="blueline" align="right">
					<xsl:value-of select="/Data/Order/Total"/>
					&#xA0;
				</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<!--ALL VARIABLE END-->	
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="no" externalpreview="no" url="file:///d:/EXEs/XML/EN_Invoice_20101222_054727_Browse.XML" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->