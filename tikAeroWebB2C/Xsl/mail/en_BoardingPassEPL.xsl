<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                          xmlns:msxsl="urn:schemas-microsoft-com:xslt"
												  xmlns:ms="urn:schemas-microsoft-com:xslt" 
                          xmlns:CharCode="http://www.tiksystems.com/Printing" 
												  xmlns:dt="urn:schemas-microsoft-com:datatypes">
            
  <xsl:key name="passenger_type_rcd_group" match="//Passengers/Passenger" use="passenger_type_rcd" />
  <xsl:template match="/">
    <xsl:value-of select="'Q0,0'"/>
    <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
    <xsl:value-of select="'rN'"/>
    <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
    <xsl:value-of select="'S6'"/>
    <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
    <xsl:value-of select="'D5'"/>
    <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
    <xsl:value-of select="'ZT'"/>
    <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
    <xsl:value-of select="'JB'"/>
    <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
    <xsl:value-of select="'OD'"/>
    <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
    <xsl:value-of select="'R120,0'"/>
    <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>

    <xsl:for-each select="Bagage/Tickets/Ticket">
      <xsl:value-of select="'N'"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      
      <xsl:variable name="passenger_name" select="normalize-space(concat(lastname,'/',firstname,' ',title_rcd))" />
      <xsl:variable name="departure_date" select="concat(substring(departure_date,7,2),CharCode:getMonthString(substring(departure_date,5,2)),substring(departure_date,3,2))" />
      <xsl:variable name="plan_departure_time" select="concat(substring(plan_departure_time,1,2),substring(plan_departure_time,4,2))" />
      <xsl:variable name="PDF_passenger_name" select="substring(concat($passenger_name,CharCode:GetCharacterCode(32,20 - string-length($passenger_name),'')),1,20)" />
      <xsl:variable name="PDF_PNR_code" select="concat(record_locator_display,CharCode:GetCharacterCode(32,7 - string-length(record_locator_display),''))" />
      <xsl:variable name="PDF_from_airport" select="concat(origin_rcd,CharCode:GetCharacterCode(32,3 - string-length(origin_rcd),''))" />
      <xsl:variable name="PDF_to_airport" select="concat(destination_rcd,CharCode:GetCharacterCode(32,3 - string-length(destination_rcd),''))" />
      <xsl:variable name="PDF_desinator" select="concat(airline_rcd,CharCode:GetCharacterCode(32,3 - string-length(airline_rcd),''))" />
      <xsl:variable name="PDF_flight_number" select="concat(flight_number,CharCode:GetCharacterCode(32,5 - string-length(flight_number),''))" />
      <xsl:variable name="PDF_date_of_flight" select="format-number(concat(day_of_year,CharCode:GetCharacterCode(32,3 - string-length(day_of_year),'')),'000')" />
      <xsl:variable name="PDF_compartment" select="concat(boarding_class_rcd,CharCode:GetCharacterCode(32,1 - string-length(boarding_class_rcd),''))" />
      <xsl:variable name="PDF_seat_no" select="concat(seat_number,CharCode:GetCharacterCode(32,4 - string-length(seat_number),''))" />
      <xsl:variable name="PDF_checkin_sequence" select="concat(check_in_sequence,CharCode:GetCharacterCode(32,5 - string-length(check_in_sequence),''))" />
      
      <!--FIX DATA-->
      <xsl:value-of select="concat('GG0080,0900,','&quot;','0','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('GG0084,0019,','&quot;','0','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A15,1047,0,1,2,2,N,','&quot;','Passenger copy','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A52,130,0,1,2,2,N,','&quot;','Boarding Pass','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A50,1012,0,1,2,2,N,','&quot;','Boarding Pass','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A24,164,0,1,2,2,N,','&quot;','Airline Copy','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A376,1325,0,2,1,1,N,','&quot;','Gate','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A197,1326,0,2,1,1,N,','&quot;','Seat','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A375,511,0,2,1,1,N,','&quot;','Gate','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A18,1430,0,2,1,1,N,','&quot;','PNR:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A19,1521,0,2,1,1,N,','&quot;','Note:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A18,1472,0,2,1,1,N,','&quot;','Date:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A17,1282,0,2,1,1,N,','&quot;','Boarding Time:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A196,512,0,2,1,1,N,','&quot;','Seat','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A17,658,0,2,1,1,N,','&quot;','Date:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A16,1220,0,2,1,1,N,','&quot;','Flight:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A16,615,0,2,1,1,N,','&quot;','PNR:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A55,1326,0,2,1,1,N,','&quot;','Seq','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A54,512,0,2,1,1,N,','&quot;','Seq','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A16,467,0,2,1,1,N,','&quot;','Boarding Time:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A15,405,0,2,1,1,N,','&quot;','Flight:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A16,1179,0,2,1,1,N,','&quot;','To:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A15,364,0,2,1,1,N,','&quot;','To:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A16,1101,0,2,1,1,N,','&quot;','Name:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A16,1139,0,2,1,1,N,','&quot;','From:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A14,325,0,2,1,1,N,','&quot;','From:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A14,286,0,2,1,1,N,','&quot;','Name:','&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <!--FIX DATA-->
      
      <!--FLIGHT INFORMATION-->
      <xsl:value-of select="concat('A205,607,0,2,2,2,N,','&quot;',record_locator_display,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A200,1421,0,2,2,2,N,','&quot;',record_locator_display,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      
      <xsl:value-of select="concat('A198,1175,0,3,1,1,N,','&quot;',destination_airport,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A203,363,0,3,1,1,N,','&quot;',destination_airport,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>

      <xsl:if test="string-length(boarding_gate) != 0">
        <xsl:value-of select="concat('A421,543,0,2,2,2,N,','&quot;',boarding_gate,'&quot;')"/>
        <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
        <xsl:value-of select="concat('A422,1357,0,2,2,2,N,','&quot;',boarding_gate,'&quot;')"/>
        <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      </xsl:if>
      
      <xsl:if test="string-length(boarding_time) = 4">
        <xsl:variable name="boarding_time" select="concat(substring(boarding_time,1,2),':',substring(boarding_time,3,2))" />
        <xsl:value-of select="concat('A422,1274,0,2,2,2,N,','&quot;',$boarding_time,'&quot;')"/>
        <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
        <xsl:value-of select="concat('A420,459,0,2,2,2,N,','&quot;',$boarding_time,'&quot;')"/>
        <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      </xsl:if>
      
      <xsl:if test="string-length(boarding_time) = 3">
        <xsl:variable name="boarding_time" select="concat(substring(boarding_time,1,1),':',substring(boarding_time,2,2))" />
        <xsl:value-of select="concat('A422,1274,0,2,2,2,N,','&quot;',0,$boarding_time,'&quot;')"/>
        <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
        <xsl:value-of select="concat('A420,459,0,2,2,2,N,','&quot;',0,$boarding_time,'&quot;')"/>
        <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      </xsl:if>
      
      <xsl:value-of select="concat('A198,1137,0,3,1,1,N,','&quot;',origin_airport,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A203,323,0,3,1,1,N,','&quot;',origin_airport,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      
      
      <xsl:value-of select="concat('A198,1217,0,3,1,1,N,','&quot;',airline_rcd,' ',flight_number,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A203,404,0,3,1,1,N,','&quot;',airline_rcd,' ',flight_number,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      
      <xsl:value-of select="concat('A85,544,0,2,2,2,N,','&quot;',check_in_sequence,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A86,1357,0,2,2,2,N,','&quot;',check_in_sequence,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      
      <xsl:value-of select="concat('A198,1099,0,3,1,1,N,','&quot;',$passenger_name,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A203,283,0,3,1,1,N,','&quot;',$passenger_name,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A249,1356,0,2,2,2,N,','&quot;',seat_number,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A247,541,0,2,2,2,N,','&quot;',seat_number,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A204,648,0,2,2,2,N,','&quot;',$departure_date,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <xsl:value-of select="concat('A201,1468,0,2,2,2,N,','&quot;',$departure_date,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>

      <!--FLIGHT INFORMATION-->
      
      <!--BAR CODE-->
      
      <!--SMALL BAR CODE-->
      <xsl:value-of select="concat('b133,199,P,607,1678,x4,y13,s0,o0,r90,l1,f0,','&quot;',check_in_sequence,'&quot;')"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <!--SMALL BAR CODE-->
      
      <!--BIG BAR CODE-->
      <xsl:if test="passenger_status_rcd = 'OK'">
        <xsl:if test="e_ticket_flag = 1">
          <xsl:value-of select="concat('b127,699,P,607,1678,x4,y4,s0,o0,r90,l1,f0,','&quot;','M','1',$PDF_passenger_name,'Y',$PDF_PNR_code,$PDF_from_airport,$PDF_to_airport,$PDF_desinator,$PDF_flight_number,$PDF_date_of_flight,$PDF_compartment,$PDF_seat_no,$PDF_checkin_sequence,'0100','&quot;')"/>
        </xsl:if>
        <xsl:if test="e_ticket_flag != 1">
          <xsl:value-of select="concat('b127,699,P,607,1678,x4,y4,s0,o0,r90,l1,f0,','&quot;','M','1',$PDF_passenger_name,'N',$PDF_PNR_code,$PDF_from_airport,$PDF_to_airport,$PDF_desinator,$PDF_flight_number,$PDF_date_of_flight,$PDF_compartment,$PDF_seat_no,$PDF_checkin_sequence,'0100','&quot;')"/>
        </xsl:if>
      </xsl:if>
      <xsl:if test="passenger_status_rcd != 'OK'">
        <xsl:if test="e_ticket_flag = 1">
          <xsl:value-of select="concat('b127,699,P,607,1678,x4,y4,s0,o0,r90,l1,f0,','&quot;','M','1',$PDF_passenger_name,'Y',$PDF_PNR_code,$PDF_from_airport,$PDF_to_airport,$PDF_desinator,$PDF_flight_number,$PDF_date_of_flight,$PDF_compartment,$PDF_seat_no,$PDF_checkin_sequence,'0000','&quot;')"/>
        </xsl:if>
        <xsl:if test="e_ticket_flag != 1">
          <xsl:value-of select="concat('b127,699,P,607,1678,x4,y4,s0,o0,r90,l1,f0,','&quot;','M','1',$PDF_passenger_name,'N',$PDF_PNR_code,$PDF_from_airport,$PDF_to_airport,$PDF_desinator,$PDF_flight_number,$PDF_date_of_flight,$PDF_compartment,$PDF_seat_no,$PDF_checkin_sequence,'0000','&quot;')"/>
        </xsl:if>
      </xsl:if>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
      <!--BIG BAR CODE-->

      <!--BAR CODE-->
      <xsl:value-of select="'P'"/>
      <xsl:value-of select="CharCode:GetCharacterCode(10,1,'')"/>
    </xsl:for-each>
    
  </xsl:template>
  <msxsl:script implements-prefix="CharCode" language="JScript">
    <![CDATA[
      var count = 0;
      var strCharacter="";
      var lastPosition;
      var tempDest = "";
      
      
      function GetCharacterCode(Code, Amount, Character)
      {
        var i;
        var CharVal = "";
        if (Character == null)
        {
          Character = "";
        }
        for (i = 0; i < Amount; i++) 
        {
          CharVal = CharVal + (String.fromCharCode(Code) + Character);
        }
        return CharVal;
      }
      
      function getDateString() 
      {
        var mydate=new Date();
        var year=mydate.getYear();
        if (year < 1000)
        year+=1900;
        var day=mydate.getDay();
        var month=mydate.getMonth()+1;
        if (month<10)
        month="0"+month;
        var daym=mydate.getDate();
        if (daym<10)
        daym="0"+daym;
        
        return daym+getMonthString(month)+year.toString().substring(2,4);
      }
      function getMonthString(value)
      {
        var month
        if (value=="01")
          month="JAN";
        else if (value=="02")
          month="FEB";
        else if (value=="03")
          month="MAR";
        else if (value=="04")
          month="APR";
        else if (value=="05")
          month="MAY";
        else if (value=="06")
          month="JUN";
        else if (value=="07")
          month="JUL";
        else if (value=="08")
          month="AUG";
        else if (value=="09")
          month="SEP";
        else if (value=="10")
          month="OCT";
        else if (value=="11")
          month="NOV";
        else
          month="DEC";
          
        return month;
      }
      
		]]>
  </msxsl:script>
  <xsl:template match="@ElapsedTime">
    <xsl:value-of select="CharCode:GetCharacterCode(32,1,'')" />
  </xsl:template>
</xsl:stylesheet>




