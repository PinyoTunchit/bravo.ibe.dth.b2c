<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

	<xsl:variable name="PageTitle">tikAERO Cashbook report</xsl:variable>
	<xsl:variable name="BarPDF417URL">http://www.tikaeroid.com/TikAeroBarcode/TikAeroBarcodePDF417.ashx?</xsl:variable>
	<xsl:variable name="BarURL">http://www.tikaeroid.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>

	<xsl:variable name="BaseURL">http://www.aircal.nc/XSLImages/</xsl:variable>
	<xsl:variable name="Title">tikAERO Summary</xsl:variable>
	<xsl:variable name="ColorSpan">Red</xsl:variable>

	<xsl:variable name="form_of_payment_rcd" select="//CashbookSummary/Summary/form_of_payment_rcd"/>
	<xsl:variable name="receive_currency_rcd" select="//CashbookSummary/Summary/receive_currency_rcd"/>
	<xsl:key name="Summary_payment_rcd_group" match="//CashbookSummary/Summary" use="form_of_payment_rcd"/>
	<xsl:key name="Summary_currency_rcd_group" match="//CashbookSummary/Summary" use="receive_currency_rcd"/>

	<xsl:key name="form_of_payment_rcd_group" match="//CashbookPayments/Payment" use="form_of_payment_rcd"/>
	<xsl:key name="form_of_payment_subtype_rcd_group" match="//CashbookPayments/Payment" use="form_of_payment_subtype_rcd"/>
	<xsl:key name="payment_currency_rcd_group" match="//CashbookPayments/Payment" use="receive_currency_rcd"/>


	<xsl:key name="TKT_reference_group" match="//CashbookCharges/Charge" use="reference"/>

	<xsl:key name="Charge_payment_rcd_group" match="//CashbookCharges/Charge" use="form_of_payment_rcd"/>
	<xsl:key name="Charge_payment_subtype_rcd_group" match="//CashbookCharges/Charge" use="form_of_payment_subtype_rcd"/>
	<xsl:key name="Charge_currency_rcd_group" match="//CashbookCharges/Charge" use="currency_rcd"/>
	<xsl:key name="fee_currency_rcd_group" match="//CashbookCharges/Charge" use="currency_rcd"/>

	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date,7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('     01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1),1, 3)"/>
		<xsl:variable name="hours" select="substring($date, 10,2)"/>
		<xsl:variable name="minutes" select="substring($date,13,2)"/>

		<xsl:value-of select="$day"/>
		<xsl:value-of select="' '"/>
		<xsl:choose>
			<xsl:when test="$month = 'JAN'">January</xsl:when>
			<xsl:when test="$month = 'FEB'">February</xsl:when>
			<xsl:when test="$month = 'MAR'">March</xsl:when>
			<xsl:when test="$month = 'APR'">April</xsl:when>
			<xsl:when test="$month = 'MAY'">May</xsl:when>
			<xsl:when test="$month = 'JUN'">June</xsl:when>
			<xsl:when test="$month = 'JUL'">July</xsl:when>
			<xsl:when test="$month = 'AUG'">August</xsl:when>
			<xsl:when test="$month = 'SEP'">September</xsl:when>
			<xsl:when test="$month = 'OCT'">October</xsl:when>
			<xsl:when test="$month = 'NOV'">November</xsl:when>
			<xsl:when test="$month = 'DEC'">December</xsl:when>
		</xsl:choose>
		<xsl:value-of select="' '"/>
		<xsl:value-of select="$year"/>
		<xsl:value-of select="' '"/>
		<xsl:value-of select="$hours"/>
		<xsl:value-of select="':'"/>
		<xsl:value-of select="$minutes"/>
	</xsl:template>

	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date,7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('     01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1),1, 3)"/>
		<xsl:variable name="hours" select="substring($date, 10,2)"/>
		<xsl:variable name="minutes" select="substring($date,13,2)"/>
		<xsl:value-of select="concat($day,'-', $month,'-' ,$year)"/>
	</xsl:template>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$PageTitle"/>
				</TITLE>
				<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
				<STYLE>.CashbookSummary .BookingNumber{
						color: #C4102F;
					}
					 
					.dummy{}
					
					.CashbookSummary.PanelHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #C4102F; 
						/*background: url(../Images/0/H/hbh.jpg);*/
 						margin: 5px 0px 0px 0px; 
						width: 100%;
					}
					.CashbookSummary .PanelHeader IMG{
						vertical-align: middle;

					}
					.CashbookSummary .PanelContainer{
					padding: 4px 2px 4px 5px;
					width: 100%;
					display: block;
				}
				
					.GridHeader TD
					{
						background: #fdb813;
						color:  #003964;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						border-bottom: 2px solid #fdb813;
						
					}
					.CashbookSummary TR.GridItems
					{
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						padding: 4px 2px 4px 5px;  
					}
					.GridItems TD
					{
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
					    padding: 4px 2px 4px 5px;
					}

					.CashbookSummary TR.GridRemark
				{
					font-family: Tahoma, Arial, sans-serif, verdana;
					font-size: 11px;
					color:  #003964;
 					height: 22px;
					font-weight: normal;
					padding: 4px 2px 4px 5px;
				}

				.CashbookSummary TR.GridTR
				{
					font-family: Tahoma, Arial, sans-serif, verdana;
					font-size: 11px;
					color:  #003964;
 					height: 20px;
				}
					.PanelItinerary .GridFooter TD SPAN
					{
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						height: 15px;
						padding: 10 0 10 20;
					}
					.PanelItinerary .FooterTotal
					{
						border-bottom: solid 1px #fdb813;
					}
					.PanelItinerary SPAN.FooterTotalLabel
					{
							background-color: transparent;
					 		color:  #003964;
							font: bold 11pt Tahoma, Arial, sans-serif, verdana;
							padding: 4px 2px 4px 5px;
					}
					.PanelItinerary SPAN.FooterTotalLabelRed
					{
							background-color: transparent;
					 		color: #FF6633;
							font: bold 11pt Tahoma, Arial, sans-serif, verdana;
							padding: 4px 2px 4px 5px;
					}
					.PanelItinerary SPAN.FooterTotalValue
					{
						color: #000066;
						font-size: 11px;
					} 					
					
					.WelcomeText { 
						font-family: verdana, arial, helvetica, sans-serif; 
						font-size: 12px; 
						padding: 1 10 1 5; 
					} .
					
					.CommentText { 
					font-family: verdana, arial, helvetica, sans-serif; 
						font-size: 12px; 
						padding: 1 10 1 5; 
					}
								.CashbookSummary TR.GridFooter 
				{
					font-family: Tahoma, Arial, sans-serif, verdana;
					font-size: 11px;
					color:  #003964;
 					height: 22px;
					font-weight: normal;
					padding: 4px 2px 4px 5px;
				}
				 
				}

						.TD.FooterTotalLabel
					{
							background-color: transparent;
					 		color:  #003964;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							height: 10px;
							border-bottom: solid 1px #fdb813;
					}

				TD.FooterTotalValue
					{
						background-color: transparent;
					 	color:  #003964;
						font: bold 8pt Tahoma, Arial, sans-serif, verdana;
						height: 10px;
						border-bottom: solid 1px #fdb813;
					}
			 
				.CashbookSummary .BarcodeHeader
				{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 10px;
					font-weight: bold;
					color: #00529d;
					padding: 5 0 3 5;
					margin: 0 0 0 0;
					width:  100%;
					cursor: hand;
				}

				.CashbookSummary .Barcode
				{
					padding: 1 5 1 5;
				}

				.WelcomeText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				.CommentText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				SPAN.Barcode {
				}
				SPAN.Barcode SPAN{
					background: Black;
					height:30px;
				}
				SPAN.Barcode IMG{
					background: Black;
					height:30px;
				}
				SPAN.Barcode IMG.B11{
					width: 1px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B12{
					width: 1px;
					margin: 0px 2px 0px 0px;
				}
				SPAN.Barcode IMG.B21{
					width: 2px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B22{
					width: 2px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B1{
					width: 1px;
				}
				SPAN.Barcode SPAN.TicketNumber{
					background: White;
					font-family: tahoma, verdana, arial, helvetica, sans-serif;
					font-size: 8pt;
					width:  100%;
					padding-left: 10px;
				}
				.locale {
				width: 500;
				border: 1px;
				border-style: solid;
				margin: 10px;
				}
				
				.CashbookSummary TR.GridType
				{
					font-family: Tahoma, Arial, sans-serif, verdana;
					font-size: 11px;
					color:  #003964;
 					height: 15px;
				}
				.CashbookSummary .GridBooking
				{
					font-family: Tahoma, Arial, sans-serif, verdana;
					font-size: 11px;
					color: #031668;
					height: 15px
				}</STYLE>
			</HEAD>
			<BODY>
				<DIV style="width:100%;">
					<DIV class="CashbookSummary">
						<table class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
							<tr>
								<td>
									<!--Summary-->

									<xsl:if test="//CashbookHeader = true()">
										<DIV class="PanelContainer">
											<TABLE class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
												<tr class="GridType">
													<td align="left" width="10%">
														<SPAN class="GridBooking">From:</SPAN>
													</td>
													<td align="left" width="40%">
														<xsl:call-template name="formatdate">
															<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_from"/>
														</xsl:call-template>
													</td>

													<td align="left" width="10%">
														<SPAN class="GridBooking">Closed at:</SPAN>
													</td>
													<td align="left" width="40%">

														<xsl:call-template name="formatdate">
															<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/process_date_time"/>
														</xsl:call-template>
													</td>
												</tr>
												<tr class="GridType">
													<td align="left" width="10%">
														<SPAN class="GridBooking">To:</SPAN>
													</td>
													<td align="left" width="40%">

														<xsl:call-template name="formatdate">
															<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_to"/>
														</xsl:call-template>
													</td>

													<td align="left" width="10%">
														<SPAN class="GridBooking">Closed by:</SPAN>
													</td>
													<td align="left" width="40%">
														<xsl:value-of select="Cashbook/CashbookHeader/Header/process_name"/>
													</td>
												</tr>
												<tr class="GridType">
													<td align="left" width="10%">
														<SPAN class="GridBooking">Cashbook:</SPAN>
													</td>
													<td align="left" width="40%">
														<xsl:value-of select="format-number(Cashbook/CashbookHeader/Header/cashbook_number,'00000000')"/>
													</td>
													<td align="left" width="10%">
														<SPAN class="GridBooking"></SPAN>
													</td>
													<td align="left" width="40%">
													</td>
												</tr>
												<tr class="GridType">
													<td align="left" width="10%">
														<SPAN class="GridBooking">Group:</SPAN>
													</td>
													<td align="left" width="40%">
														<xsl:value-of select="Cashbook/CashbookHeader/Header/agency_group_rcd"/>
													</td>
													<td align="left" width="10%">
														<SPAN class="GridBooking"></SPAN>
													</td>
													<td align="left" width="40%">
													</td>
												</tr>
												<tr class="GridType">
													<td align="left" width="10%">
														<SPAN class="GridBooking">Agency:</SPAN>
													</td>
													<td align="left" width="40%">
														<xsl:value-of select="Cashbook/CashbookHeader/Header/agency_code"/>
													</td>
													<td align="left" width="10%">
														<SPAN class="GridBooking"></SPAN>
													</td>
													<td align="left" width="40%">
													</td>
												</tr>
												<tr class="GridType">
													<td align="left" width="10%">
														<SPAN class="GridBooking">User:</SPAN>
													</td>
													<td align="left" width="40%">
														<xsl:value-of select="Cashbook/CashbookHeader/Header/cashbook_name"/>
													</td>
													<td align="left" width="10%">
														<SPAN class="GridBooking"></SPAN>
													</td>
													<td align="left" width="40%">
													</td>
												</tr>
												<tr class="GridType">
													<td align="left" width="10%">
														<SPAN class="GridBooking">Comment:</SPAN>
													</td>
													<td align="left" width="40%">
														<xsl:value-of select="Cashbook/CashbookHeader/Header/comment"/>
													</td>
													<td align="left" width="10%">
														<SPAN class="GridBooking"></SPAN>
													</td>
													<td align="left" width="40%">
													</td>
												</tr>
											</TABLE>
										</DIV>
									</xsl:if>
									<br></br>
									<DIV class="PanelContainer">
										<DIV class="PanelHeader" style="font-size: 15px;">
											<SPAN>SUMMARY</SPAN>
											<br></br>
											<br></br>
										</DIV>

										<TABLE class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
											<TR class="GridHeader" valign="top">
												<td align="left" width="10%">Type</td>
												<td align="left" width="20%">Name</td>
												<td align="left" width="20%">Sub</td>
												<td align="left" width="20%">Name</td>
												<td align="right" width="10%">Count</td>
												<td align="right" width="5%">&#xA0;</td>
												<td align="left" width="5%">Currency</td>
												<td align="right" width="10%">Amount</td>
											</TR>


											<xsl:for-each select="//CashbookSummary/Summary[count(. | key('Summary_payment_rcd_group', form_of_payment_rcd)[1])= 1]">

												<xsl:sort select="form_of_payment_rcd"/>
												<xsl:sort select="receive_currency_rcd"/>
												<xsl:variable name="form_of_payment_rcd" select="form_of_payment_rcd"/>
												<xsl:variable name="currency_rcd" select="currency_rcd"/>

												<xsl:for-each select="//CashbookSummary/Summary[count(. | key('Summary_currency_rcd_group', receive_currency_rcd)[1])= 1]">

													<xsl:sort select="form_of_payment_rcd"/>
													<xsl:sort select="receive_currency_rcd"/>
													<xsl:variable name="form_of_payment_rcd_2" select="form_of_payment_rcd"/>
													<xsl:variable name="currency_rcd_2" select="receive_currency_rcd"/>

													<xsl:for-each select="//CashbookSummary/Summary[$form_of_payment_rcd = form_of_payment_rcd][receive_currency_rcd = $currency_rcd_2]">


														<tr class="GridRemark" valign="middle">
															<td align="left" width="10%">
																<xsl:value-of select="form_of_payment_rcd"/>
															</td>
															<td align="left" width="20%">
																<xsl:value-of select="form_of_payment"/>
															</td>
															<td align="left" width="20%">
																<xsl:value-of select="form_of_payment_subtype_rcd"/>
															</td>
															<td align="left" width="20%">
																<xsl:value-of select="form_of_payment_subtype"/>
															</td>
															<td align="right" width="10%">
																<xsl:value-of select="format-number(payment_count,'##,###')"/>
															</td>
															<td align="right" width="5%">&#xA0;</td>
															<td align="left" width="5%">
																<xsl:value-of select="receive_currency_rcd"/>
															</td>
															<td align="right" width="10%">
																<xsl:value-of select="format-number(payment_total,'#,##0.00')"/>
															</td>
														</tr>

														<xsl:if test="position()=last()">

															<tr class="GridFooter">

																<td align="left" class="FooterTotalLabel">
																	<SPAN>

																		<xsl:value-of select="form_of_payment_rcd"/>
																	</SPAN>
																</td>
																<td align="left" class="FooterTotalLabel">
																	<SPAN>Total</SPAN>
																</td>
																<td align="right" class="FooterTotalLabel">&#xA0;</td>
																<td align="right" class="FooterTotalLabel">&#xA0;</td>
																<td align="right" class="FooterTotalLabel">
																	<SPAN>
																		<xsl:value-of select="format-number(sum(//CashbookSummary/Summary[form_of_payment_rcd = $form_of_payment_rcd][receive_currency_rcd = $currency_rcd_2]/payment_count),'##,###')"/>
																	</SPAN>
																</td>
																<td align="left" class="FooterTotalLabel">
																	<SPAN>

																		<xsl:value-of select="//CashbookSummary/Summary[form_of_payment_rcd = $form_of_payment_rcd][receive_currency_rcd = $currency_rcd_2]/currency_rcd"/>
																	</SPAN>
																</td>
																<td align="right" class="FooterTotalLabel">&#xA0;</td>
																<td align="right" class="FooterTotalLabel">
																	<SPAN>
																		<xsl:value-of select="format-number(sum(//CashbookSummary/Summary[form_of_payment_rcd = $form_of_payment_rcd][receive_currency_rcd = $currency_rcd_2]/payment_total),'#,##0.00')"/>
																	</SPAN>
																</td>
															</tr>
														</xsl:if>
													</xsl:for-each>
												</xsl:for-each>
											</xsl:for-each>

											<tr align="left" class="GridType">
												<td width="10%">&#xA0;</td>
												<td width="20%">&#xA0;</td>
												<td width="20%">&#xA0;</td>
												<td width="20%">&#xA0;</td>
												<td width="15%">&#xA0;</td>
												<td width="5%">&#xA0;</td>
												<td width="10%">&#xA0;</td>
											</tr>
										</TABLE>
									</DIV>
								</td>
							</tr>
						</table>
					</DIV>
				</DIV>
				<p/>
				<p/>
				<!--End Summary-->
				<!-- Payments-->
				<DIV style="page-break-after:always">
					<br/>
				</DIV>
				<DIV style="width:100%;">

					<DIV class="CashbookSummary">
						<table class="Table" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
							<tr>
								<td>
									<xsl:if test="//CashbookHeader = true()">

										<DIV class="PanelContainer">
											<TABLE class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
												<tr class="GridType">
													<td align="left" width="5%">
														<SPAN class="GridBooking">From:</SPAN>
													</td>
													<td align="left" width="20%">
														<SPAN>
															<xsl:call-template name="formatdate">
																<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_from"/>
															</xsl:call-template>
														</SPAN>
													</td>

													<td align="left" width="5%">
														<SPAN class="GridBooking">To:</SPAN>
													</td>

													<td align="left" width="20%">
														<SPAN>
															<xsl:call-template name="formatdate">
																<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_to"/>
															</xsl:call-template>
														</SPAN>
													</td>
													<td align="left" width="30%"></td>

													<td align="left" width="10%">
														<SPAN class="GridBooking">Cashbook</SPAN>
													</td>
													<td align="right" width="10%">
														<SPAN>
															<xsl:value-of select="format-number(Cashbook/CashbookHeader/Header/cashbook_number,'00000000')"/>&#xA0;&#xA0;</SPAN>
													</td>
												</tr>
											</TABLE>
										</DIV>
									</xsl:if>
									<br></br>
									<DIV class="PanelContainer">
										<DIV class="PanelHeader" style="font-size: 15px;">
											<SPAN>PAYMENTS</SPAN>
											<br></br>
											<br></br>
										</DIV>



										<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_rcd_group', form_of_payment_rcd)[1]) = 1]">
											<xsl:sort select="form_of_payment_rcd"/>
											<xsl:sort select="form_of_payment_subtype_rcd"/>
											<xsl:sort select="currency_rcd"/>
											<xsl:variable name="form_of_payment_rcd_1" select="form_of_payment_rcd"/>
											<!--	<xsl:value-of select="$form_of_payment_rcd_1"/>-->

											<xsl:choose>
												<xsl:when test="$form_of_payment_rcd_1 = 'CASH'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td width="20%">&#xA0;</td>
															<td width="20%">&#xA0;</td>
															<td width="10%">&#xA0;</td>
															<td align="left" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>

														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'CASH'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>

																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="right" width="20%">&#xA0;</td>
																		<td align="left" width="20%">&#xA0;</td>
																		<td align="left" width="10%">&#xA0;</td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CASH'][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CASH'][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CASH'][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd ='CASH'][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
													<br></br>
												</xsl:when>

												<!---End CASH-->
												<!--- CC-->
												<xsl:when test="$form_of_payment_rcd_1 = 'CC'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td align="left" width="20%">Name</td>
															<td align="left" width="20%">Card Number</td>
															<td width="10%"></td>
															<td align="left" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>
														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'CC'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>


																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="left" width="20%">
																			<xsl:value-of select="name_on_card"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																		</td>
																		<td width="10%"></td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CC'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CC'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CC'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd ='CC'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
													<br></br>
												</xsl:when>
												<!---End CC-->
												<!--- VOUCHER-->
												<xsl:when test="$form_of_payment_rcd_1 = 'VOUCHER'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td align="left" width="20%">Name</td>
															<td align="left" width="20%">Voucher Number</td>
															<td width="10%">&#xA0;</td>
															<td align="right" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>
														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'VOUCHER'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>
																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="left" width="20%">
																			<xsl:value-of select="name_on_card"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:value-of select="document_number"/>
																		</td>
																		<td width="10%"></td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'VOUCHER'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'VOUCHER'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'VOUCHER'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd = 'VOUCHER'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
													<br></br>
												</xsl:when>
												<!---End VOUCHER-->
												<!---TKT-->
												<xsl:when test="$form_of_payment_rcd_1 = 'TKT'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td align="left" width="20%">Name</td>
															<td align="right" width="20%">Ticket Number</td>
															<td width="10%"></td>
															<td align="right" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>
														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'TKT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>



																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="left" width="20%">
																			<xsl:value-of select="name_on_card"/>
																		</td>
																		<td align="right" width="20%">
																			<xsl:value-of select="document_number"/>
																		</td>
																		<td width="10%"></td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'TKT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'TKT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'TKT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd = 'TKT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
												</xsl:when>
												<!---End TK-->
												<!---CRAGT-->
												<xsl:when test="$form_of_payment_rcd_1 = 'CRAGT'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td align="left" width="20%">Name</td>
															<td align="right" width="20%">Voucher Number</td>
															<td width="10%"></td>
															<td align="right" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>

														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'CRAGT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>

																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="left" width="20%">
																			<xsl:value-of select="name_on_card"/>
																		</td>
																		<td align="right" width="20%">
																			<xsl:value-of select="document_number"/>
																		</td>
																		<td width="10%"></td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CRAGT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CRAGT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CRAGT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd = 'CRAGT'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
													<br></br>
												</xsl:when>
												<!---End CRAGT-->
												<!---MANUAL-->
												<xsl:when test="$form_of_payment_rcd_1 = 'MANUAL'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td align="left" width="20%">Name</td>
															<td align="left" width="20%">Document Number</td>
															<td width="10%"></td>
															<td align="left" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>
														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'MANUAL'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>

																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="left" width="20%">
																			<xsl:value-of select="name_on_card"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:value-of select="document_number"/>
																		</td>
																		<td width="10%"></td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'MANUAL'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'MANUAL'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'MANUAL'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd = 'MANUAL'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
													<br></br>
												</xsl:when>
												<!---End MANUAL-->
												<!---CHEQUE-->
												<xsl:when test="$form_of_payment_rcd_1 = 'CHEQUE'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td align="left" width="20%">Name</td>
															<td align="left" width="20%">Voucher Number</td>
															<td width="10%"></td>
															<td align="left" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>
														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'CHEQUE'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>


																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="left" width="20%">
																			<xsl:value-of select="name_on_card"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:value-of select="document_number"/>
																		</td>
																		<td width="10%"></td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CHEQUE'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CHEQUE'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'CHEQUE'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd = 'CHEQUE'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
													<br></br>
												</xsl:when>
												<!---End CHEQUE-->
												<!---BANK-->
												<xsl:when test="$form_of_payment_rcd_1 = 'BANK'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td align="left" width="20%">Name</td>
															<td align="left" width="20%">Voucher Number</td>
															<td width="10%"></td>
															<td align="left" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>
														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'BANK'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>
																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="left" width="20%">
																			<xsl:value-of select="name_on_card"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:value-of select="document_number"/>
																		</td>
																		<td width="10%"></td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'BANK'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'BANK'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'BANK'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd = 'BANK'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
													<br></br>
												</xsl:when>
												<!---End BANK-->
												<!---INV-->
												<xsl:when test="$form_of_payment_rcd_1 = 'INV'">

													<table class="Table" width="100%" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
														<tr class="GridHeader" valign="top">
															<td align="left" width="15%">Number</td>
															<td align="left" width="20%">Date and Time</td>
															<td align="left" width="20%">Name</td>
															<td align="left" width="20%">Account</td>
															<td width="10%">Purchase Order</td>
															<td align="left" width="5%">Currency</td>
															<td align="right" width="10%">Amount</td>
														</tr>
														<xsl:for-each select="//CashbookPayments/Payment[count(. | key('form_of_payment_subtype_rcd_group', form_of_payment_subtype_rcd)[1]) = 1]">
															<xsl:sort select="form_of_payment_rcd"/>
															<xsl:sort select="form_of_payment_subtype_rcd"/>
															<xsl:sort select="receive_currency_rcd"/>
															<xsl:variable name="form_of_payment_subtype_rcd_2" select="form_of_payment_subtype_rcd"/>


															<xsl:for-each select="//CashbookPayments/Payment[count(. | key('payment_currency_rcd_group', receive_currency_rcd)[1])= 1]">

																<xsl:sort select="form_of_payment_rcd"/>
																<xsl:sort select="form_of_payment_subtype_rcd"/>
																<xsl:sort select="receive_currency_rcd"/>
																<xsl:variable name="currency_rcd_3" select="receive_currency_rcd"/>

																<xsl:for-each select="//CashbookPayments/Payment[form_of_payment_rcd = 'INV'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]">
																	<xsl:sort select="form_of_payment_rcd"/>
																	<xsl:sort select="form_of_payment_subtype_rcd"/>
																	<xsl:sort select="receive_currency_rcd"/>
																	<tr class="GridRemark" valign="bottom">
																		<td align="left" width="15%">
																			<xsl:value-of select="payment_number"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</td>

																		<td align="left" width="20%">
																			<xsl:value-of select="name_on_card"/>
																		</td>
																		<td align="left" width="20%">
																			<xsl:value-of select="document_number"/>
																		</td>
																		<td width="10%">
																			<xsl:value-of select="purchase_order"/>
																		</td>
																		<td align="left" width="5%">
																			<xsl:value-of select="receive_currency_rcd"/>
																		</td>
																		<td align="right" width="10%">
																			<xsl:value-of select="format-number(receive_payment_amount,'#,##0.00')"/>
																		</td>
																	</tr>
																	<xsl:if test="position() = last() ">
																		<tr class="GridFooter">
																			<td align="left" class="FooterTotalValue" width="15%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'INV'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_rcd"/>
																				</SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="20%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'INV'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/form_of_payment_subtype_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="20%"></td>
																			<td align="left" class="FooterTotalValue" width="10%">
																				<SPAN></SPAN>
																			</td>
																			<td align="left" class="FooterTotalValue" width="5%">
																				<SPAN>
																					<xsl:value-of select="//CashbookPayments/Payment[form_of_payment_rcd = 'INV'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_currency_rcd"/>
																				</SPAN>
																			</td>
																			<td align="right" class="FooterTotalValue" width="10%">
																				<SPAN>
																					<xsl:value-of select="format-number(sum(//CashbookPayments/Payment[form_of_payment_rcd = 'INV'][form_of_payment_subtype_rcd = $form_of_payment_subtype_rcd_2][receive_currency_rcd = $currency_rcd_3]/receive_payment_amount),'#,##0.00')"/>
																				</SPAN>
																			</td>
																		</tr>
																	</xsl:if>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</table>
													<br></br>
												</xsl:when>
												<!---INV-->
												<xsl:otherwise>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:for-each>
									</DIV>
								</td>
							</tr>
						</table>
					</DIV>
				</DIV>
				<DIV style="page-break-after:always">
					<br/>
				</DIV>
				<!-- End Payments-->

				<!-- Tickets-->

				<DIV style="width:100%;">
					<DIV class="CashbookSummary">
						<table class="Table" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
							<tr>
								<td>
									<xsl:if test="//CashbookHeader = true()">
										<DIV class="PanelContainer">
											<TABLE class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
												<tr class="GridType">
													<td align="left" width="5%">
														<SPAN class="GridBooking">From:</SPAN>
													</td>
													<td align="left" width="20%">
														<SPAN>
															<xsl:call-template name="formatdate">
																<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_from"/>
															</xsl:call-template>
														</SPAN>
													</td>

													<td align="left" width="5%">
														<SPAN class="GridBooking">To:</SPAN>
													</td>

													<td align="left" width="20%">
														<SPAN>
															<xsl:call-template name="formatdate">
																<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_to"/>
															</xsl:call-template>
														</SPAN>
													</td>
													<td align="left" width="30%"></td>

													<td align="left" width="10%">
														<SPAN class="GridBooking">Cashbook</SPAN>
													</td>
													<td align="right" width="10%">
														<SPAN>
															<xsl:value-of select="format-number(Cashbook/CashbookHeader/Header/cashbook_number,'00000000')"/>&#xA0;&#xA0;</SPAN>
													</td>
												</tr>
											</TABLE>
										</DIV>
									</xsl:if>
									<br></br>

									<DIV class="PanelContainer">
										<DIV class="PanelHeader" style="font-size: 15px;">
											<SPAN>TICKETS</SPAN>
											<br></br>
											<br></br>
										</DIV>

										<TABLE class="Table" rules="all" cellspacing="1" cellpadding="3" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
											<TR class="GridHeader" valign="top">
												<td align="left" width="15%">Number</td>
												<td align="left" width="15%">Date</td>
												<td align="left" width="5%">Filght</td>
												<td align="left" width="5%">Seg</td>
												<td align="left" width="15%">Name</td>
												<td align="left" width="5%">Cur</td>
												<td align="left" width="10%">Type</td>
												<td align="right" width="10%">Amount</td>
												<td align="right" width="10%">Total</td>
												<td align="right" width="10%">Paid</td>
											</TR>
											<xsl:for-each select="//CashbookCharges/Charge[type = 'TKT']">
												<xsl:sort select="currency_rcd"/>
												<xsl:sort select="flight_date"/>
												<xsl:sort select="flight"/>
												<xsl:sort select="origin"/>
												<xsl:sort select="destination"/>

												<xsl:variable name="flight_date" select="flight_date"/>
												<xsl:variable name="currency_rcd_fee" select="currency_rcd"/>
												<xsl:variable name="type" select="type"/>


												<xsl:if test="type = 'TKT'">



													<tr class="GridRemark" valign="top">
														<td align="left" width="15%">

															<xsl:value-of select="reference"/>
														</td>
														<td align="left" width="15%">
															<SPAN>
																<xsl:call-template name="format-date">
																	<xsl:with-param name="date" select="flight_date"/>
																</xsl:call-template>
															</SPAN>
														</td>
														<td align="left" width="5%">
															<xsl:value-of select="flight"/>
														</td>
														<td align="left" width="5%">
															<xsl:value-of select="origin"/>-<xsl:value-of select="destination"/></td>
														<td align="left" width="15%">
															<xsl:value-of select="passenger_name"/>
														</td>
														<td align="left" width="5%">
															<xsl:value-of select="currency_rcd"/>
														</td>

														<td align="left" width="10%">

															<table class="Table" rules="all" cellspacing="0" cellpadding="0" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">

																<xsl:choose>

																	<xsl:when test="charge_amount  != '' ">
																		<tr class="GridRemark" valign="top">
																			<td>

																				<span>Fare</span>
																			</td>
																		</tr>
																	</xsl:when>
																</xsl:choose>

																<xsl:choose>

																	<xsl:when test="surcharge_amount  != '' ">

																		<tr class="GridRemark" valign="top">
																			<td>

																				<span>Surch</span>
																			</td>
																		</tr>
																	</xsl:when>
																</xsl:choose>

																<xsl:choose>

																	<xsl:when test="tax_amount  != '' ">

																		<tr class="GridRemark" valign="top">
																			<td>

																				<span>Tax</span>
																			</td>
																		</tr>
																	</xsl:when>
																</xsl:choose>
																<xsl:choose>

																	<xsl:when test="vat_amount  != '' ">

																		<tr class="GridRemark" valign="top">
																			<td>

																				<span>Mwst</span>
																			</td>
																		</tr>
																	</xsl:when>
																</xsl:choose>
															</table>
														</td>

														<td align="right" width="10%">
															<table class="Table" rules="all" cellspacing="0" cellpadding="0" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
																<xsl:choose>

																	<xsl:when test="charge_amount  != '' ">
																		<tr class="GridRemark" valign="top">
																			<td align="right">

																				<span>
																					<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
																				</span>
																			</td>
																		</tr>
																	</xsl:when>
																</xsl:choose>
																<xsl:choose>

																	<xsl:when test="surcharge_amount  != '' ">
																		<tr class="GridRemark" valign="top">
																			<td align="right">

																				<span>
																					<xsl:value-of select="format-number(surcharge_amount,'#,##0.00')"/>
																				</span>
																			</td>
																		</tr>
																	</xsl:when>
																</xsl:choose>
																<xsl:choose>

																	<xsl:when test="tax_amount  != '' ">
																		<tr class="GridRemark" valign="top">
																			<td align="right">

																				<span>
																					<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>
																				</span>
																			</td>
																		</tr>
																	</xsl:when>
																</xsl:choose>
																<xsl:choose>

																	<xsl:when test="vat_amount  != '' ">
																		<tr class="GridRemark" valign="top">
																			<td align="right">

																				<span>
																					<xsl:value-of select="format-number(vat_amount,'#,##0.00')"/>
																				</span>
																			</td>
																		</tr>
																	</xsl:when>
																</xsl:choose>
															</table>
														</td>
														<td align="right" width="10%">
															<xsl:if test="total_amount != ' ' ">
																<xsl:value-of select="format-number(total_amount,'#,##0.00')"/>
															</xsl:if>
														</td>
														<td align="right" width="10%">
															<xsl:if test="payment_amount != ' ' ">
																<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>
															</xsl:if>
														</td>
													</tr>
													<tr valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												</xsl:if>
											</xsl:for-each>
										</TABLE>
									</DIV>
								</td>
							</tr>
						</table>
					</DIV>
				</DIV>
				<DIV style="page-break-after:always">
					<br/>
				</DIV>
				<br></br>
				<br></br>
				<!-- FEES-->

				<DIV style="width:100%;">
					<DIV class="CashbookSummary">
						<table class="Table" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
							<tr>
								<td>
									<xsl:if test="//CashbookHeader = true()">
										<DIV class="PanelContainer">
											<TABLE class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
												<tr class="GridType">
													<td align="left" width="5%">
														<SPAN class="GridBooking">From:</SPAN>
													</td>
													<td align="left" width="20%">
														<SPAN>
															<xsl:call-template name="formatdate">
																<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_from"/>
															</xsl:call-template>
														</SPAN>
													</td>

													<td align="left" width="5%">
														<SPAN class="GridBooking">To:</SPAN>
													</td>

													<td align="left" width="20%">
														<SPAN>
															<xsl:call-template name="formatdate">
																<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_to"/>
															</xsl:call-template>
														</SPAN>
													</td>
													<td align="left" width="30%"></td>

													<td align="left" width="10%">
														<SPAN class="GridBooking">Cashbook</SPAN>
													</td>
													<td align="right" width="10%">
														<SPAN>
															<xsl:value-of select="format-number(Cashbook/CashbookHeader/Header/cashbook_number,'00000000')"/>&#xA0;&#xA0;</SPAN>
													</td>
												</tr>
											</TABLE>
										</DIV>
									</xsl:if>
									<br></br>

									<DIV class="PanelContainer">
										<DIV class="PanelHeader" style="font-size: 15px;">
											<SPAN>FEES</SPAN>
											<br></br>
											<br></br>
										</DIV>

										<TABLE class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
											<TR class="GridHeader" valign="top">

												<td align="left" width="20%">Type</td>
												<td align="left" width="15%">Date</td>
												<td align="left" width="10%">Filght</td>
												<td align="left" width="15%">Seg</td>

												<td align="right" width="10%">Cur</td>
												<td align="right" width="15%">Total</td>
												<td align="right" width="15%">Paid</td>
											</TR>


											<xsl:for-each select="//CashbookCharges/Charge[type = 'FEE']">

												<xsl:sort select="flight_date"/>
												<xsl:sort select="flight"/>
												<xsl:sort select="origin"/>
												<xsl:sort select="destination"/>
												<xsl:sort select="currency_rcd"/>
												<xsl:variable name="flight_date" select="flight_date"/>
												<xsl:variable name="currency_rcd_fee" select="currency_rcd"/>
												<xsl:variable name="type" select="type"/>
												<xsl:if test="type = 'FEE'">

													<tr class="GridRemark" valign="middle">

														<td align="left" width="20%">
															<xsl:value-of select="passenger_name"/>
														</td>

														<td align="left" width="15%">

															<xsl:choose>
																<xsl:when test="create_date_time != ''">
																	<SPAN>
																		<xsl:call-template name="format-date">
																			<xsl:with-param name="date" select="create_date_time"/>
																		</xsl:call-template>
																	</SPAN>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>&#xA0;</xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</td>
														<td align="left" width="10%">
															<xsl:value-of select="flight"/>
														</td>
														<td align="left" width="15%">
															<SPAN>

																<xsl:value-of select="origin"/>
																<xsl:if test="origin != '' and destination != '' ">-</xsl:if>
																<xsl:value-of select="destination"/>
															</SPAN>
														</td>

														<td align="right" width="10%">
															<xsl:value-of select="currency_rcd"/>
														</td>
														<td align="right" width="15%">
															<xsl:if test="charge_amount_incl != ' ' ">
																<xsl:value-of select="format-number(charge_amount_incl,'#,##0.00')"/>
															</xsl:if>
														</td>
														<td align="right" width="15%">
															<xsl:if test="payment_amount != ' ' ">
																<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>
															</xsl:if>
														</td>
														<td align="right" width="10%">&#xA0;</td>
														<td align="right" width="10%">&#xA0;</td>
													</tr>

													<tr valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												</xsl:if>
											</xsl:for-each>
										</TABLE>
									</DIV>
								</td>
							</tr>
						</table>
					</DIV>
				</DIV>
				<DIV style="page-break-after:always">
					<br/>
				</DIV>
				<br></br>
				<br></br>
				<!-- End FEES-->
				<!-- VOUCHERS-->
				<DIV style="width:100%;">
					<DIV class="CashbookSummary">
						<table class="Table" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
							<tr>
								<td>
									<xsl:if test="//CashbookHeader = true()">
										<DIV class="PanelContainer">
											<TABLE class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
												<tr class="GridType">
													<td align="left" width="5%">
														<SPAN class="GridBooking">From:</SPAN>
													</td>
													<td align="left" width="20%">
														<SPAN>
															<xsl:call-template name="formatdate">
																<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_from"/>
															</xsl:call-template>
														</SPAN>
													</td>

													<td align="left" width="5%">
														<SPAN class="GridBooking">To:</SPAN>
													</td>

													<td align="left" width="20%">
														<SPAN>
															<xsl:call-template name="formatdate">
																<xsl:with-param name="date" select="Cashbook/CashbookHeader/Header/payment_date_to"/>
															</xsl:call-template>
														</SPAN>
													</td>
													<td align="left" width="30%"></td>

													<td align="left" width="10%">
														<SPAN class="GridBooking">Cashbook</SPAN>
													</td>
													<td align="right" width="10%">
														<SPAN>
															<xsl:value-of select="format-number(Cashbook/CashbookHeader/Header/cashbook_number,'00000000')"/>&#xA0;&#xA0;</SPAN>
													</td>
												</tr>
											</TABLE>
										</DIV>
									</xsl:if>
									<br></br>

									<DIV class="PanelContainer">
										<DIV class="PanelHeader" style="font-size: 15px;">
											<SPAN>VOUCHERS</SPAN>
											<br></br>
											<br></br>
										</DIV>

										<TABLE class="Table" cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12px;">
											<TR class="GridHeader" valign="top">
												<td align="left" width="15%">Number</td>
												<td align="left" width="10%">Type</td>
												<td align="left" width="15%">Date</td>
												<td align="left" width="20%">Name</td>
												<td align="left" width="10%">Cur</td>
												<td align="right" width="15%">Value</td>
												<td align="right" width="15%">Charge</td>
											</TR>

											<xsl:for-each select="//CashbookCharges/Charge[type = 'VOUCHER']">
												<xsl:sort select="flight_date"/>
												<xsl:sort select="currency_rcd"/>
												<xsl:variable name="flight_date" select="flight_date"/>
												<xsl:variable name="currency_rcd_vou" select="currency_rcd"/>

												<xsl:if test=" type ='VOUCHER'">

													<tr class="GridRemark" valign="middle">
														<td align="left" width="15%">
															<xsl:value-of select="reference"/>
														</td>
														<td align="left" width="10%">
															<xsl:value-of select="flight"/>
														</td>
														<td align="left" width="15%">

															<xsl:choose>
																<xsl:when test="create_date_time != ''">
																	<SPAN>
																		<xsl:call-template name="format-date">
																			<xsl:with-param name="date" select="create_date_time"/>
																		</xsl:call-template>
																	</SPAN>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>&#xA0;</xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</td>

														<td align="left" width="20%">
															<xsl:value-of select="passenger_name"/>
														</td>
														<td align="left" width="10%">
															<xsl:value-of select="currency_rcd"/>
														</td>
														<td align="right" width="15%">
															<xsl:if test="total_amount != ' ' ">
																<xsl:value-of select="format-number(total_amount,'#,##0.00')"/>
															</xsl:if>
														</td>
														<td align="right" width="15%">
															<xsl:if test="payment_amount != ' ' ">
																<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>
															</xsl:if>
														</td>
													</tr>

													<tr valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												</xsl:if>
											</xsl:for-each>
										</TABLE>
									</DIV>
								</td>
							</tr>
						</table>
					</DIV>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\cashbook_report.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->