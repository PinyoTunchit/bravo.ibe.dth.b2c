<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable> 
	<xsl:variable name="PageTitle">TikOPS Flight Irregularities</xsl:variable>
	<xsl:variable name="ColorSpan">Red</xsl:variable>		
	<xsl:template name="format-date">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<xsl:value-of select="concat($day, $month, $year)" />
	</xsl:template>	
	
	<xsl:template match="/">
		<HTML>
		<HEAD>
			<TITLE><xsl:value-of select="$PageTitle"/></TITLE>
				<STYLE>
				.PanelItinerary .BookingNumber{
						color: #C4102F;
					}
					.PanelDailySummary .PanelHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #C4102F;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 5px 0px 5px 5px;
						width: 100%;
					}
					.PanelItinerary .PanelHeader IMG{
						vertical-align: middle;
					}
					.PanelItinerary .PanelHeader SPAN{
						padding-left: 4;
					}
					.PanelItinerary .PanelContainer{
						padding: 10 0 10 5;
						width: 659px;
						display: block;
					}
					.GridHeader TD
					{
						background: #fdb813;
						color:  #003964;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						padding: 4px 2px 4px 5px;
					}
					.PanelItinerary TR.GridItems
					{
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					.GridItems TD
					{
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					
					.GridItems01 TD
					{
						
						color:#3D3D3D;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					.PanelItinerary .GridFooter TD SPAN
					{
						font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
						font-size: 11px;
						height: 18px;
						padding: 4 5 0 4;
					}
					.PanelItinerary .FooterTotal
					{
						border-bottom: solid 1px #dddddd;
					}
					.PanelItinerary SPAN.FooterTotalLabel
					{
							background-color: transparent;
					 		color: #666666;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							padding: 1px 0px 1px 0px;
					}
					.PanelItinerary SPAN.FooterTotalLabelRed
					{
							background-color: transparent;
					 		color: Red;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							padding: 1px 0px 1px 0px;
					}
					.PanelItinerary SPAN.FooterTotalValue
					{
						color: #666666;
						font-size: 11px;
					} 					
					
					.WelcomeText { 
						font-family: verdana, arial, helvetica, sans-serif; 
						font-size: 12px; 
						padding: 1 10 1 5; 
					} .
					
					.CommentText { 
					font-family: verdana, arial, helvetica, sans-serif; 
						font-size: 12px; 
						padding: 1 10 1 5; 
					}
					.BarcodeHeader
						{
							color: #666666;
							font-family: Tahoma, Arial, sans-serif, verdana;
							font-size: 7pt;
							font-style: normal;
							font-weight: bold;
							padding: 0px 0px 0px 12px;
							text-align: left;
							width: 659px;
						}

					.PanelItinerary .Barcode
							{
								padding: 1 5 1 5;
							} 
					SPAN.Barcode {
					
							}
							SPAN.Barcode SPAN{
								background : Black;
								height:30px;
								
							}
							SPAN.Barcode SPAN.11{
								width: 1px;
								margin: 0 1 0 0;
							}
							SPAN.Barcode SPAN.12{
								width: 1px;
								margin: 0 3 0 0;
							}
							SPAN.Barcode SPAN.21{
								width: 3px;
								margin: 0 1 0 0;
							}
							SPAN.Barcode SPAN.22{
								width: 3px;
								margin: 0 3 0 0;
							}
							SPAN.Barcode SPAN.1{
								width: 1px;
								
							}
							SPAN.Barcode SPAN.TicketNumber{
								background: #ffffff;
								color: Black;
								font-family: Tahoma, Arial, sans-serif, verdana;
								font-size: 8pt;
								width: 659px;
								padding-left: 10px;
								
							}
					 
				</STYLE>
		</HEAD>
		<BODY>
		<DIV >
			<TABLE class="Table" cellspacing="0" rules="rows" border="0" width="100%">
				<TR>
					<TD>
						<img src="{$BaseURL}ISK_logo.jpg"/>
					</TD>
				</TR>	
			</TABLE>
			 <DIV class="PanelDailySummary">
				<DIV class="PanelHeader"  style="font-size: 12px;">	
					<SPAN>Flight Irregularities &#160;&#160;
					<xsl:value-of select="substring(flight_information/irregularity/utc_departure_date,5,2)"/>/<xsl:value-of select="substring(flight_information/irregularity/utc_departure_date,7,2)"/>/<xsl:value-of select="substring(flight_information/irregularity/utc_departure_date,1,4)" />
					</SPAN>
				</DIV>
			</DIV>
					<table class="Table" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">						
					<TR class="GridHeader">							
							<TD align="Left" >Flight</TD>
							<TD align="Left" >From</TD>
							<TD align="Left" >To</TD>							
							<TD align="Left" >Flight Date</TD>
							<TD align="Left" >Flight Time</TD>
							<TD align="Left" >Irregularity</TD>
							<TD align="Left" >Comment</TD>
							<TD align="Left" >Aircraft Type</TD>
							<TD align="Left" >Matriculation</TD>
							<TD align="Left" >Flight Status</TD>
 						</TR>
					<xsl:for-each select="flight_information/irregularity">
					<tr class="GridItems">						
 					    <td align="Left">
							<span><xsl:value-of select="airline_rcd"/><xsl:text> </xsl:text><xsl:value-of select="flight_number"/></span>
						</td>
						<td align="Left">
							<span><xsl:value-of select="origin_rcd"/></span>
						</td>
						<td align="Left">
							<span><xsl:value-of select="destination_rcd"/></span>
						</td>												 
						<td align="Left">
							<span><xsl:value-of select="substring(departure_date,5,2)"/>/<xsl:value-of select="substring(departure_date,7,2)"/>/<xsl:value-of select="substring(departure_date,1,4)"/></span>
						</td>
						<td align="Left">
							<span><xsl:value-of select="substring(format-number(number(planned_departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_departure_time), '0000'),3,4)"/></span>
						</td>						
						<td align="Left">
							<span><xsl:value-of select="display_name"/></span>
						</td>
						<td aligh="Left">
							<span><xsl:value-of select="irregularity_comment"/></span>							
						</td>
						<td align="Left">
							<span><xsl:value-of select="aircraft_type_rcd"/></span>
						</td>
						<td aligh="Left">
							<span><xsl:value-of select="matriculation_rcd"/></span>							
						</td>
						<td aligh="Left">
							<span><xsl:value-of select="flight_status_rcd"/></span>							
						</td>
					</tr>						
					</xsl:for-each>					 				
				</table>				
			 </DIV>
		 
		</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\GetReportIrregularity11_152504.XML" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->