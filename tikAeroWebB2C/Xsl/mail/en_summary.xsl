<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="PageTitle">tikAERO  BookingSummary</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable> 
	<xsl:variable name="ColorSpan">Red</xsl:variable>
	<xsl:variable name="passenger_id" select="BookingSummary/Booking/Header/BookingHeader"/>
	<xsl:key name="passenger_type_rcd_group" match="//Passengers/Passenger" use="passenger_type_rcd" /> 
	<xsl:variable name="booking_segment_id_tax" select="Booking/Tickets/Ticket[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
	<xsl:variable name="booking_segment_id" select="Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
	<xsl:variable name="sum_net_total" select="sum(Booking/Tickets/Ticket/net_total)"/>
	<xsl:variable name="sum_payment_amount" select="sum(Booking/Payments/Payment/payment_amount)"/>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($day, '/', $month, '/', $year)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<xsl:value-of select="concat($day, $month, $year)" />
	</xsl:template>

	<xsl:template match="/">
		<HTML>
		<HEAD>
			<TITLE><xsl:value-of select="$PageTitle"/></TITLE>
			<STYLE>
				.PanelBookingSummary .BookingNumber{
					color: #C4102F;
				}

				.PanelBookingSummary .PanelHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #C4102F;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 3px 0px 5px 5px;
						width: 100%;
					}
				.PanelBookingSummary .PanelHeader IMG{
					vertical-align: middle;
				}

				.PanelBookingSummary .PanelHeader SPAN{
					padding-left: 4;
				}

				.PanelBookingSummary .PanelContainer{
					padding: 10 0 10 20;
					width: 100%;
					display: block;
				}
				.PanelBookingSummary .GridHeader
					{
						background: #fdb813;
						color:  #003964;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						padding: 2px 2px 2px 5px;
					}
				.PanelBookingSummary .GridBooking
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					color: #031668;
					height: 20px
				}
				.PanelBookingSummary TR.GridItems
					{
						 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}

				.PanelBookingSummary TR.GridType
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					color: #666666;
 					height: 40px;
				}
				.PanelBookingSummary .GridItems TD
					{
						 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					.PanelBookingSummary .GridItems01 TD
					{
						 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
						background: #ffffff;
					}
				.PanelBookingSummary .GridFooter TD SPAN
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					height: 18px;
					padding: 4 5 0 4;
				}
				.PanelBookingSummary TR.GridRemark
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 10px;
					color: #666666;
 					height: 20px;
				}
				.PanelBookingSummary .FooterTotal
				{
					border-bottom: solid 1px #fdb813;
				}

				.PanelBookingSummary SPAN.FooterTotalLabel
					{
							background-color: transparent;
					 		color: #003964;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							height: 22px;
							padding: 1px 2px 1px 5px;
					}
				.PanelBookingSummary SPAN.FooterTotalValue
					{
						color: #000066;
						font-size: 11px;
					}
				.PanelBookingSummary .BarcodeHeader
				{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 10px;
					font-weight: bold;
					color: #00529d;
					padding: 5 0 3 5;
					margin: 0 0 0 0;
					width: 100%;
					cursor: hand;
				}

				.PanelBookingSummary .Barcode
				{
					padding: 1 5 1 5;
				}

				.WelcomeText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				.CommentText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				SPAN.Barcode {
				}
				SPAN.Barcode SPAN{
					background: Black;
					height:30px;
				}
				SPAN.Barcode IMG{
					background: Black;
					height:30px;
				}
				SPAN.Barcode IMG.B11{
					width: 1px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B12{
					width: 1px;
					margin: 0px 2px 0px 0px;
				}
				SPAN.Barcode IMG.B21{
					width: 2px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B22{
					width: 2px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B1{
					width: 1px;
				}
				SPAN.Barcode SPAN.TicketNumber{
						background: White;
						font-family: tahoma, verdana, arial, helvetica, sans-serif;
						font-size: 8pt;
						width: 100%;
						padding-left: 10px;
				}
				.locale {
					width: 500;
					border: 1px;
					border-style: solid;
					margin: 10px
				}
					
				/* View \*/
				.view									{
					margin: 0px 0px 20px 0px;
					padding: 5px 0px 5px 0px;
					width: 100%;
				}
				
				.view DIV								{
					height: 25px;
					padding: 0px 10px 0px 10px;
				}
				
				.view DIV SPAN							{
					color: #031668;
					font-family: Tahoma, Arial, sans-serif, verdana;
					font-size: 11px;
					padding: 3px;
				}

				.view DIV SPAN.info{
					color: #031668;
					font: bold 12px,Tahoma, Arial, sans-serif, verdana;
					font-weight: bold;
					text-align: left;
					width: 200px;
				}
				
				.view DIV SPAN.label{
					width: 120px;
					font-size: 11px;
					font-family: Tahoma, Arial, sans-serif, verdana;
					color:#3D3D3D;
				}

			</STYLE>
		</HEAD>
		<BODY>
		<DIV style="width:100%;">
<DIV class="PanelBookingSummary">
	 
		<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
		<TR> 
		<TD> 
		 
		<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
					<TR class="GridType">					
						<TD align="left">					
						<DIV class="PanelHeader" style="font-size: 12px;width=100%;">Booking Header</DIV>
						</TD>
		 			</TR>
		</TABLE>
		<xsl:for-each select="Booking/Header/BookingHeader">
		<DIV class="PanelContainer">	
		<TABLE cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Booking No.:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="booking_number"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Booking Reference:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="record_locator"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Agency Code:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="agency_code"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Agency Name:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="agency_name"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Contact Name:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="contact_name"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Email:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="contact_email"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Mobile Phone:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="phone_mobile"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Home Phone:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="phone_home"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Business Phone:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="phone_business"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Fax:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="phone_fax"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Received from:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="received_from"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Create by:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="create_name"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Create Date:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20" >
					<xsl:if test ="create_date_time != ''">
					<xsl:value-of select="substring(create_date_time,7,2)"/>/<xsl:value-of select="substring(create_date_time,5,2)"/>/<xsl:value-of select="substring(create_date_time,1,4)"/>
					</xsl:if>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				 <SPAN class="GridBooking"></SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				 <SPAN class="GridBooking"></SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">

				</TD>
			</TR>
 		</TABLE>
	</DIV>
<TD> 

<TABLE width="100%;" class="Table" rules="none" cellspacing="0" border="0">
			<TR class="GridType">					
				<TD align="left">					
				<DIV class="PanelHeader" style="font-size: 12px;width=50%;">Client Profile</DIV>
				</TD>
 			</TR>
</TABLE>
<DIV class="PanelContainer">	
		<TABLE cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Client Number:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:if test="client_number > '0'">
						<xsl:value-of select="client_number"/>
					</xsl:if>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Member Number:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_member_number"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Client Name:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_firstname"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Client Contact:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_contact_name"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Address:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20" >
			<xsl:if test="client_address_line1 != ''">
			<xsl:value-of select="client_address_line1"/> &#160;
			</xsl:if>
			<xsl:if test="client_address_line2 != ''">
				<xsl:value-of select="client_address_line2"/>&#160;
			</xsl:if>
			<xsl:if test="client_street != ''">
				<xsl:value-of select="client_street"/> &#160;
			</xsl:if>
			<xsl:if test="client_state != ''">
				<xsl:value-of select="client_state"/>&#160;
			</xsl:if>
			<xsl:if test="client_district != ''">
				<xsl:value-of select="client_district"/>&#160;
			</xsl:if>
			<xsl:if test="client_province != ''">
				<xsl:value-of select="client_province"/>&#160;
			</xsl:if>
			<xsl:if test="client_city != ''">
				<xsl:value-of select="client_city"/>&#160;
			</xsl:if>
			<xsl:if test="client_zip_code != ''">
				<xsl:value-of select="client_zip_code"/>
			</xsl:if></TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Country:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_country_rcd"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">PO Box:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_po_box"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Email:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_contact_email"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Mobile Phone:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_phone_mobile"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Home Phone:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_phone_home"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Business Phone:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_phone_business"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Fax:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_phone_fax"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				<SPAN class="GridBooking">Language:</SPAN>
				</TD>
				<TD valign="middle" width="25%" height="20">
				<xsl:value-of select="client_language_rcd"/>
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				 
				</TD>
				<TD valign="middle" width="25%" height="20">
				 
				</TD>
			</TR>
			<TR class="GridType">
				<TD align="left" valign="middle" width="25%" height="20">
				 
				</TD>
				<TD valign="middle" width="25%" height="20">
				 
				</TD>
			</TR>
		</TABLE>
		</DIV>
</TD>
</xsl:for-each>
</TD>
</TR>
</TABLE>
				
				 
					<DIV class="PanelHeader" style="font-size: 12px;"> 
					<SPAN>Your Itinerary&#160;(<xsl:value-of select="count(//FlightSegment[not(segment_status_rcd = '' or segment_status_rcd = 'XX')])" />)</SPAN>
					<SPAN class="BookingNumber">&#160;<xsl:value-of select="Booking/Header/BookingHeader/record_locator"/></SPAN>
			</DIV>
				<DIV class="PanelContainer">
					<TABLE class="Table" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
						<TR class="GridHeader">
							<TD></TD>
							<TD>Flight</TD>
							<TD>From</TD>
							<TD>To</TD>
							<TD>Date</TD>
							<TD>Dep</TD>
							<TD>Arr</TD>
							<TD>Status</TD>
						</TR>
						<xsl:for-each select="Booking/Itinerary/FlightSegment[not(segment_status_rcd = '' or segment_status_rcd = 'XX' )]">
					<TR class="GridItems">
								<TD><xsl:number value="position()" format="001 "/></TD>
								<TD><SPAN><xsl:value-of select="airline_rcd"/>&#160;<xsl:value-of select="flight_number"/></SPAN></TD>
								<TD><xsl:value-of select="origin_name"/><xsl:if test="origin_rcd !=''">&#160;(<xsl:value-of select="origin_rcd"/>)</xsl:if></TD>
								<TD><xsl:value-of select="destination_name"/><xsl:if test="destination_rcd !=''">&#160;(<xsl:value-of select="destination_rcd"/>)</xsl:if></TD>
								<TD>
										<xsl:if test="string-length(departure_date) > '0'">
												<xsl:call-template name="formatdate">
													<xsl:with-param name="date" select="departure_date"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="string-length(departure_date) = '0'">
												<xsl:text>OPEN</xsl:text>
											</xsl:if>	</TD>
								<TD>
								<xsl:if test ="planned_arrival_time > 0">
								<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/>
								</xsl:if>
								</TD>
								<TD>
								<xsl:if test ="planned_arrival_time > 0">
								<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/>
								</xsl:if>
								</TD>
								<TD>
								<xsl:choose>
												<xsl:when test="flight_id != ''">
													<xsl:value-of select="status_name"/>
													<!--<xsl:value-of select="segment_status_rcd"/>-->
												</xsl:when>
												<xsl:when test="not(string-length(departure_date) > '0')">
													<xsl:text>&#160;</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text>Info</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
								</TD>
							</TR>
						</xsl:for-each>
					</TABLE>
				</DIV>
				<DIV class="PanelHeader" style="font-size: 12px;"> 
				<SPAN>Passengers&#160;(<xsl:value-of select="count(Booking/Passengers/Passenger)" />)</SPAN>
				</DIV>
				<DIV class="PanelContainer">
					<TABLE cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
						<TR class="GridHeader">
							<TD>&#160;</TD>
							<TD>Lastname</TD>
							<TD>Firstname</TD>
							<TD>Title</TD>
							<TD>Type</TD>
							<TD>Date of Birth</TD>
							<TD>Passenger Weight</TD>
						</TR>
						<xsl:for-each select="Booking/Passengers/Passenger">
						<xsl:variable name="date_of_birth" select="date_of_birth"/>
							<TR class="GridItems">
								<TD><xsl:number value="position()" format="001 "/></TD>
								<TD><xsl:value-of select="lastname"/></TD>
								<TD><xsl:value-of select="firstname"/></TD>
								<TD><xsl:value-of select="title_rcd"/></TD>
								<TD><xsl:value-of select="passenger_type_rcd"/></TD>
								<TD>
								<xsl:if test ="string($date_of_birth) != ''">
								<xsl:value-of select="substring(date_of_birth,5,2)"/>/<xsl:value-of select="substring(date_of_birth,7,2)"/>/<xsl:value-of select="substring(date_of_birth,1,4)"/>
								</xsl:if>
								</TD>
								<TD>
								<xsl:if test="passenger_weight > 0 ">
								<xsl:value-of select="passenger_weight"/>
								</xsl:if>
								</TD>
							</TR>
						</xsl:for-each>
					</TABLE>
				</DIV> 
				<DIV class="PanelHeader" style="font-size: 12px;"><SPAN>Tickets&#160;
				(<xsl:value-of select="count(Booking/Tickets/Ticket[not(passenger_status_rcd = 'XX')])" />) 
				</SPAN>			
				</DIV> 
				<DIV class="PanelContainer">
					<TABLE cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
						<TR class="GridHeader">
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>Ticket Number</TD>
							<TD>Passenger Name</TD>
							<TD>Flight</TD>
							<TD>Date</TD>
							<TD>Status</TD>
							<TD>Type</TD>
							<TD align="right">	Total Net</TD>
							<TD align="Center">eTKT</TD>
						</TR>
					
						<xsl:for-each select="Booking/Tickets/Ticket[not(passenger_status_rcd = 'XX')]">
												
							 
							<TR class="GridItems">
								<TD>&#160;</TD>
								<TD>
					
								<xsl:number value="position()" format="001  " />
 						
						 		</TD>
								<TD><xsl:value-of select="ticket_number"/></TD>
								<TD><SPAN><xsl:value-of select="lastname"/>/<xsl:value-of select="firstname"/></SPAN></TD>
								<TD><SPAN><xsl:value-of select="airline_rcd"/>&#160;<xsl:value-of select="flight_number"/></SPAN></TD>
								<TD>
								<xsl:if test ="departure_date != ''">
										<xsl:value-of select="substring(departure_date,7,2)"/>.<xsl:value-of select="substring(departure_date,5,2)"/>.<xsl:value-of select="substring(departure_date,1,4)"/>
									</xsl:if>
								</TD>
								<TD>
										
										 <xsl:value-of select="passenger_status_rcd"/>
									
								</TD>
								<TD><SPAN><xsl:value-of select="passenger_type_rcd"/></SPAN></TD>
								<TD align="Right">
								<SPAN>
								<xsl:if test="net_total > 0 "> 
								<xsl:value-of select="format-number(net_total,'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/><xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>&#160;
								</xsl:if>
								</SPAN>
								</TD>
								<TD align="Center">
									<xsl:choose>
										<xsl:when test="string(e_ticket_flag) = '1'">
											Y
										</xsl:when>
										<xsl:otherwise>
											N
										</xsl:otherwise>
									</xsl:choose>
								</TD>
							</TR>	
							
						</xsl:for-each>
						<TR class="GridFooter">
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD class="FooterTotal" ><SPAN class="FooterTotalLabel">Total Amount:</SPAN></TD>
							<TD class="FooterTotal" align="Right"><SPAN class="FooterTotalLabel"><xsl:value-of select="format-number(sum(Booking/Tickets/Ticket[not(passenger_status_rcd = 'XX')]/net_total),'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN></TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
						</TR>
					</TABLE>
				</DIV>
					<!--Quote-->
						<!--AAS, ABA, AEK, GMG, PMT, BEA, IRS, CTS -->
						<xsl:if test="//Booking/TicketQuotes = true()">
							<DIV class="PanelHeader" style="font-size: 12px;">
								<span>Quote&#160;(<xsl:value-of select="count(Booking/TicketQuotes/Flight)" />)</span>
							</DIV>
							<div class="PanelContainer">
								<table class="Table" width="100%" border="0" ellspacing="0" style="border-width:0px;width:100%;border-collapse:collapse;">
									<tbody>
										<tr class="GridHeader">
											<td>Passenger</td>
											<td align="center">Units</td>
											<td>Charge</td>
											<td align="right">Amount</td>
											<td align="right">MWST</td>
											<td align="right">Total</td>
										</tr>
										<xsl:for-each select="Booking/TicketQuotes/Flight">
											<xsl:if test="sort_sequence=0">
												<tr class="GridItems">
													<td>
														<xsl:value-of select="passenger_type_rcd"/>
													</td>
													<td align="center">
														<xsl:value-of select="passenger_count"/>
													</td>
													<td>
														<xsl:value-of select="charge_name"/>
													</td>
													<td align="right">
														<xsl:if test="charge_amount != 0 ">
														<xsl:value-of select="format-number(charge_amount,'###,##0.00')"/>
													</xsl:if>
													</td>
													<td align="right">
														 
													<xsl:if test="tax_amount > 0">
														<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
													</xsl:if>
													</td>
													<td align="right">
													<xsl:if test="total_amount > 0">
														<xsl:value-of select="format-number(total_amount,'#,##0.00')"/>&#160;&#160;<xsl:value-of select="currency_rcd"/>
													</xsl:if>
													</td>
												</tr>
											</xsl:if>
											<xsl:if test="sort_sequence >0">
												<tr class="GridItems">
													<td>&#160;</td>
													<td>&#160;</td>
													<td>
														<xsl:value-of select="charge_name"/>
													</td>
													<td align="right">
														<xsl:choose>
															<xsl:when test="(charge_type) = 'REFUND'">
																<!--(<xsl:value-of select="format-number(charge_amount,'###,##0.00')"/>)-->
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="format-number(charge_amount,'###,##0.00')"/>
															</xsl:otherwise>
														</xsl:choose>
													</td>
													<td>&#160;</td>
													<td align="right">
														<xsl:choose>
															<xsl:when test="(charge_type) = 'REFUND'">
																-<xsl:value-of select="format-number(charge_amount,'###,##0.00')"/>&#160;&#160;<xsl:value-of select="currency_rcd"/>
															</xsl:when>
															<xsl:otherwise>
																<!--<xsl:value-of select="format-number(charge_amount,'###,##0.00')"/>-->
															</xsl:otherwise>
														</xsl:choose>
													</td>
												</tr>
											</xsl:if>
										</xsl:for-each>
										<!--Fee-->
										<xsl:if test="string-length(//Booking/Fees/Fee/fee_amount_incl) > 0">
											<xsl:for-each select="/Booking/Fees/Fee[string-length(void_by)='0']">
												<xsl:choose>
													<xsl:when test="position()=1">
														<tr class="GridItems">
															<td>&#160;</td>
															<td>&#160;</td>
															<td align="left">
																<xsl:value-of select="display_name"/>
															</td>
															<td align="right">
																<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
															</td>
															<td align="right">
															</td>
															<td align="right">
																<xsl:value-of select="format-number((//Booking/Fees/Fee/fee_amount_incl),'#,##0.00')"/>&#160;&#160;<xsl:value-of select="//currency_rcd"/>
															</td>
														</tr>
													</xsl:when>
													<xsl:otherwise>
														<span>
															<tr class="GridItems">
																<td>&#160;</td>
																<td>&#160;</td>
																<td align="left">
																	<xsl:value-of select="display_name"/>
																</td>
																<td align="right">
																	<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
																</td>
																<td align="right">
																	
																</td>
																<td align="right">
																	<xsl:value-of select="format-number(fee_amount_incl,'#,##0.00')"/>&#160;&#160;<xsl:value-of select="currency_rcd"/>
																</td>
															</tr>
														</span>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</xsl:if>
										<!--End Fee-->
										<tr class="GridFooter">
											<td>&#160;</td>
											<td>&#160;</td>
											<td>&#160;</td>
											<td>&#160;</td>
											<td class="FooterTotal" align="right">
												<span class="FooterTotalLabel">Total</span>
											</td>
											<td class="FooterTotal" align="right">
												<span class="FooterTotalLabel">
													<xsl:value-of select="format-number((sum(//Booking/Fees/Fee[string-length(void_by)='0']/fee_amount_incl))+(sum(//Booking/TicketQuotes/Total[sort_sequence='0']/total_amount))-(sum(//Booking/TicketQuotes/Total[charge_type='REFUND']/total_amount)),'#,##0.00')"/>&#160;&#160;<xsl:value-of select="//currency_rcd"/>
												</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<br/>
						</xsl:if>
						<!--End Quote-->
				<DIV class="PanelHeader" style="font-size: 12px;"><SPAN>Construction / Endorsements&#160;(<xsl:value-of select="count(Booking/Tickets/Ticket)" />)</SPAN></DIV>
			<DIV class="PanelContainer">
					<TABLE cellspacing="0" rules="all" border="0" id="Itinerary1_grdFares" style="border-width:0px;width:100%;border-collapse:collapse;">
						<TR class="GridHeader">
							<TD>&#160;</TD>
							<TD align="left">Ticket Number</TD>
							<TD align="left">Fare Code</TD>
							<TD align="left">Status</TD>
							<TD align="right">Total</TD>
							<TD>&#160;</TD>
							<TD align="left">Endorsement</TD>
							<TD align="left">Restriction</TD>
						</TR>
						<xsl:for-each select="Booking/Tickets/Ticket">
							<TR class="GridItems">
								<TD align="left"><xsl:number value="position()" format="001 "/></TD>
								<TD align="left"><xsl:value-of select="ticket_number"/></TD>
								<TD align="left"><xsl:value-of select="fare_code"/></TD>
								<TD align="left"><xsl:value-of select="passenger_status_rcd"/></TD>
								<TD align="right"><xsl:value-of select="format-number(net_total,'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/><xsl:value-of select="/currency_rcd"/></TD>
								<TD>&#160;</TD>
								<TD align="left">
								<xsl:choose>
									<xsl:when test="endorsement_text = '0'">
										 <xsl:text>&#160;</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="endorsement_text"/>
									</xsl:otherwise>
								</xsl:choose>
								</TD>
								<TD align="left">
								<xsl:choose>
									<xsl:when test="restriction_text = '0'">
										 <xsl:text>&#160;</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="restriction_text"/>
									</xsl:otherwise>
								</xsl:choose>
								</TD>
							</TR>
						</xsl:for-each>
					</TABLE>
				</DIV>
				<BR></BR>
				
				<!--Payments-->
			<DIV class="PanelHeader" style="font-size: 12px;">
            <SPAN>Payments&#160;(<xsl:value-of select="count(Booking/Payments/Payment[(substring(void_date_time,5,2) = '')])" />)</SPAN>
           </DIV>
		    <DIV class="PanelContainer">
			<TABLE width="100%" border="0" ellspacing="0" rules="non" style="border-width:0px;border-collapse:collapse;" >
              <TR class="GridHeader">
                <TD align="left" width="30%">
                  <SPAN class="GridHeader">Description</SPAN>
                </TD>
                <TD align="left" width="30%">
                  <SPAN class="GridHeader">&#160;</SPAN>
                </TD>
                <TD align="left" width="10%">
                  <SPAN class="GridHeader">Status, Date</SPAN>
                </TD>
                <TD align="right" width="15%">
                  <SPAN class="GridHeader">Credit</SPAN>
                </TD>
                <TD align="right" width="15%">
                  <SPAN class="GridHeader">Debit</SPAN>
                </TD>
              </TR>
            <xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
			<xsl:variable name="Ticket_total" select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>
										<xsl:choose>
											<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">
												<tr class="gridItems" vAlign="middle" align="left">
													<TD align="left">Ticket Cost &amp; Fee</TD>
													<TD align="left">&#xA0;</TD>
													<TD align="left">&#xA0;</TD>
													<xsl:if test="starts-with(string($Ticket_total), '-')">
														<TD align="left">&#xA0;</TD>
														<TD align="right">
															<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></TD>
													</xsl:if>
													<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
														<TD align="right">
															<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></TD>
														<TD align="left">&#xA0;</TD>
													</xsl:if>
												</tr>
												<xsl:for-each select="Booking/Payments/Payment[void_date_time='']">
													<xsl:if test="form_of_payment_rcd !=''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems01">
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='CASH'">
																	<xsl:value-of select="form_of_payment_rcd"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='TKT'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CRAGT'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:choose>
																	<xsl:when test="void_date_time != ''">
																		<xsl:text>XX</xsl:text>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:if test="payment_date_time != ''">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</xsl:if>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;&#xA0;<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;&#xA0;<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
												</xsl:for-each>
												<tr class="gridItems" vAlign="middle" align="left">
													<TD align="left">
														<SPAN class="FooterTotalLabel">OUTSTANDING BALANCE</SPAN>
													</TD>
													<TD align="left">&#xA0;</TD>
													<TD align="left">&#xA0;</TD>
													<xsl:variable name="SubTotal" select="$Ticket_total - $Payment_total"/>
													<xsl:if test="number($SubTotal) = 0">
														<TD align="left">&#xA0;</TD>
														<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
															<SPAN class="FooterTotalLabel">0.00&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
														</TD>
													</xsl:if>
													<xsl:if test="number($SubTotal) != 0">
														<xsl:if test="number($SubTotal) &gt;= 0">
															<TD align="left">&#xA0;</TD>
															<TD align="right">
																<SPAN class="FooterTotalLabel">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
															</TD>
														</xsl:if>
														<xsl:if test="number($SubTotal) &lt; 0">
															<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
																<SPAN class="FooterTotalLabel">
																	<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
															</TD>
															<TD align="left">&#xA0;</TD>
														</xsl:if>
													</xsl:if>
												</tr>
											</xsl:when>
											<xsl:otherwise>
												<TR valign="middle" align="left" class="gridItems">
													<TD align="left">Ticket Cost &amp; Fee</TD>
													<TD align="left">&#xA0;</TD>
													<TD align="left">&#xA0;</TD>
													<xsl:if test="starts-with(string($Ticket_total), '-')">
														<TD align="left">&#xA0;</TD>
														<TD align="right">
															<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></TD>
													</xsl:if>
													<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
														<TD align="right">
															<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></TD>
														<TD align="left">&#xA0;</TD>
													</xsl:if>
												</TR>
												<xsl:for-each select="Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)]">
													<xsl:if test="form_of_payment_subtype !=''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems">
															<TD align="left">
																<xsl:value-of select="form_of_payment_subtype"/>
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:choose>
																	<xsl:when test="void_date_time != ''">
																		<xsl:text>XX</xsl:text>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:if test="payment_date_time != ''">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</xsl:if>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;&#xA0;<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;&#xA0;<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
													<xsl:if test="form_of_payment_subtype =''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems">
															<TD align="left">
																<xsl:value-of select="form_of_payment_rcd"/>
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:if test="payment_date_time != ''">
																	<xsl:call-template name="formatdate">
																		<xsl:with-param name="date" select="payment_date_time"/>
																	</xsl:call-template>
																</xsl:if>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;&#xA0;<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;&#xA0;<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
												</xsl:for-each>
												<TR valign="middle" align="left" class="gridItems">
													<TD align="left">
														<SPAN class="FooterTotalLabel">OUTSTANDING BALANCE</SPAN>
													</TD>
													<TD align="left">&#xA0;</TD>
													<TD align="left">&#xA0;</TD>
													<xsl:variable name="SubTotal" select="number($Ticket_total) - number($Payment_total)"/>
													<xsl:if test="number($SubTotal) = 0">
														<TD align="left">&#xA0;</TD>\
														<TD align="right">
															<SPAN class="FooterTotalLabel">0.00&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
														</TD>
													</xsl:if>
													<xsl:if test="number($SubTotal) != 0">
														<xsl:if test="number($SubTotal) &gt;= 0">
															<TD align="left">&#xA0;</TD>
															<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
																<SPAN class="FooterTotalLabel">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
															</TD>
														</xsl:if>
														<xsl:if test="number($SubTotal) &lt; 0">
															<TD align="right">
																<SPAN class="FooterTotalLabel">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
															</TD>
															<TD align="left">&#xA0;</TD>
														</xsl:if>
													</xsl:if>
                  </TR>
                </xsl:otherwise>
              </xsl:choose>
            </TABLE>
          </DIV>
        
		<!--End Payments-->
				<BR></BR>
				
				
				<DIV class="PanelHeader" style="font-size: 12px;"><SPAN>Remarks&#160;(<xsl:value-of select="count(Booking/Remarks/Remark[not(complete_flag = 0)])" />)</SPAN></DIV>
				<DIV class="PanelContainer">
						<TABLE width="100%" class="Table" cellspacing="0" rules="non" style="border-width:0px;border-collapse:collapse;" >
						<TR class="GridHeader">
							<TD></TD>
							<TD align="left"><SPAN class="GridHeader">Category</SPAN></TD>
							<TD align="left"><SPAN class="GridHeader">Time Limit</SPAN></TD>
							<TD align="left"><SPAN class="GridHeader">Text</SPAN></TD>
							<TD align="left"><SPAN class="GridHeader">Agent</SPAN></TD>
							<TD align="left"><SPAN class="GridHeader">Update On</SPAN></TD>
						</TR>
						
						<xsl:for-each select="Booking/Remarks/Remark[not(complete_flag = 0)]">
						 
							<TR valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
								<TD><xsl:number value="position()" format="001 "/></TD>
								<TD align="left"><xsl:value-of select="display_name"/></TD>
								<TD align="left">
								<xsl:choose>
										<xsl:when test="timelimit_date_time=''">
											<xsl:value-of select="timelimit_date_time"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="substring(timelimit_date_time,5,2)"/>/<xsl:value-of select="substring(timelimit_date_time,7,2)"/>/<xsl:value-of select="substring(timelimit_date_time,1,4)"/>
										</xsl:otherwise>
									</xsl:choose>
								</TD>
								<TD align="left"><xsl:value-of select="remark_text"/></TD>
								<TD align="left"><xsl:value-of select="added_by"/></TD>
								<TD align="left"><xsl:value-of select="substring(update_date_time,5,2)"/>/<xsl:value-of select="substring(update_date_time,7,2)"/>/<xsl:value-of select="substring(update_date_time,1,4)"/></TD>
							</TR>
						</xsl:for-each>
						
					</TABLE>
				</DIV>
				  <xsl:if test="//SpecialService = true()">
				<DIV  class="PanelHeader" style="font-size: 12px;"> <SPAN>Special Services&#160;(<xsl:value-of select="count(Booking/SpecialServices/SpecialService)" />)</SPAN></DIV>
				<DIV class="PanelContainer">
						<TABLE width="100%" class="Table" cellspacing="0" rules="non" style="border-width:0px;border-collapse:collapse;" >
						<TR class="GridHeader">
							<TD>&#160;</TD>
							<TD>Filght</TD>
							<TD>Passenger Name</TD>
							<TD>Special Service</TD>
							 
						</TR>
						
						<xsl:for-each select="Booking/SpecialServices/SpecialService">
							<TR class="GridItems">
								<TD><xsl:value-of select="concat('00','',string(position()))"/></TD>
								<TD><xsl:value-of select="airline_rcd"/>&#160;<xsl:value-of select="flight_number"/></TD>
								<TD><xsl:value-of select="lastname" />/<xsl:value-of select="firstname" /></TD>
								<TD><xsl:value-of select="special_service_rcd" />&#160;&#160;<xsl:value-of select="display_name" /> </TD>
 							</TR>
						</xsl:for-each>
					</TABLE>
 				</DIV>
		  		</xsl:if>
		</DIV>
		</DIV>
		</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\a\EN_EMAIL_LIGHT_May25_184219_A033O7.XML" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->