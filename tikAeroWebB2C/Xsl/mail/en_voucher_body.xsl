<?xml version="1.0" encoding="utf-8"?>
<!-- 
Project: ECA
File name: en_voucher_body.XSL

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:key name="passenger_type_rcd_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<!--Variable-->
	<xsl:variable name="BarPDF417URL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcodePDF417.ashx?</xsl:variable>
	<xsl:variable name="BarURL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
	<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/eca</xsl:variable>
	<xsl:variable name="Title">tikAERO Invoice</xsl:variable>
	<xsl:variable name="passenger_id" select="Booking/Tickets/Ticket/passenger_id"/>
	<xsl:variable name="booking_segment_id_tax" select="Booking/Tickets/Ticket[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
	<xsl:variable name="booking_segment_id" select="Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
	<xsl:variable name="sum_net_total" select="sum(Booking/Tickets/Ticket/net_total)"/>
	<xsl:variable name="sum_payment_amount" select="sum(Booking/Payments/Payment/payment_amount)"/>

	<xsl:template name="CDate2Julian">
		<xsl:param name="date"/>
		<xsl:param name="format" select="000"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="format-number(substring($date,3,2),'00')"/>
		<xsl:variable name="month" select="substring(substring-after('01/00002/03103/05904/09005/12006/15107/18108/21209/24310/27311/30412/334', concat($month1,'/')), 1, 3)"/>
		<xsl:value-of select="$day + $month"/>
	</xsl:template>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>

		<xsl:if test="string-length($date)!=0">
			<xsl:variable name="day" select="substring($date, 7,2)"/>
			<xsl:variable name="month1" select="substring($date,5,2)"/>
			<xsl:variable name="year" select="substring($date,3,2)"/>
			<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
			<xsl:value-of select="concat($day, '/',$month, '/', $year)"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:value-of select="concat($day, '/',$month, '/', $year)"/>
	</xsl:template>

	<!-- Define keys used to group elements -->
	<xsl:key name="keyorigin" match="FlightSegment[segment_status_rcd != 'XX']" use="origin_rcd"/>
	<xsl:key name="keydestination" match="FlightSegment[segment_status_rcd != 'XX']" use="destination_rcd"/>

	<xsl:template name="AddrFormat">
		<xsl:param name="value"></xsl:param>
		<xsl:choose>
			<xsl:when test="string-length($value) != 0">
				<xsl:value-of select="$value"/>&#xA0;</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="DateFormat">
		<xsl:param name="Date"></xsl:param>
		<xsl:if test="string-length($Date) =  3 ">0<xsl:value-of select="substring($Date,1,1)"/>:<xsl:value-of select="substring($Date,2,2)"/></xsl:if>
		<xsl:if test="string-length($Date) = 4 ">
			<xsl:value-of select="substring($Date,1,2)"/>:<xsl:value-of select="substring($Date,3,2)"/></xsl:if>
		<xsl:if test="string-length($Date) = 2 ">00:<xsl:value-of select="$Date"/></xsl:if>
		<xsl:if test="string-length($Date) = 1 ">0<xsl:value-of select="$Date"/>:00</xsl:if>
	</xsl:template>

	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$Title"/>
				</TITLE>
				<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
				<!--Style Sheet-->
				<STYLE>.welcometext, .commenttext {
	font-family: verdana,arial,helvetica,sans-serif;
	font-size: 12px;
	padding: 1 10 1 5;
}</STYLE>
				<!--End Style Sheet-->
			</HEAD>
			<BODY leftmargin="10px;" topmargin="5" rightmargin="5" bottommargin="5" marginwidth="5" marginheight="5">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<!--<img src="{$BaseURL}Images/hl0.jpg" border="0" />-->
							<!--<img src="http://www.tiksystems.com/tikaero/Images/header.png" height="176" width="684" />-->
						</td>
					</tr>
					<tr>
						<td>
						</td>
					</tr>
					<tr>
						<td height="30px"></td>
					</tr>
					<tr class="commenttext">
						<td>
							<br/>
							<br/>
							<br/>Dear

							<xsl:choose>
								<xsl:when test="string-length(/Voucher/Details/recipient_name) &gt; '0'">
									<strong>
										<xsl:value-of select="/Voucher/Details/recipient_name"/>
									</strong>
								</xsl:when>
								<xsl:otherwise>
									<strong>
										<xsl:value-of select="/Voucher/Details/lastname"/>
										<xsl:text>&#x20;</xsl:text>
										<xsl:value-of select="/Voucher/Details/firstname"/>
									</strong>
								</xsl:otherwise>
							</xsl:choose>
							<br/>
							<br/>
							<br/>Please find attached to this e-mail your personal InterSky voucher.

							<br/>
							<br/>To redeem this voucher, you will have the following possibilities:

							<br/>
							<br/>-Online at www.intersky.biz

							<br/>
							<br/>-Via phone: +43 5574 48800 46 (Mon-Fri 08:00 - 18:00 hrs)

							<br/>
							<br/>-Via E-Mail: reservation@intersky.biz

							<br/>
							<br/>This voucher is non-refundable.

							<br/>
							<br/>We look forward to welcoming you onboard an InterSky Flight in the near future.
							<br/>
							<br/>Best rgds<p/>


							<br/>InterSky Luftfahrt GmbH
							<br/>Reservation / Sales
							<br/>Bahnhofstrasse 10
							<br/>6900 Bregenz, Austria
							<br/>Tel: +43 5574 48800 46
							<br/>Fax: +43 5574 48800 8
							<br/>Mail: <a title="blocked::reservation@intersky.biz" style="color: blue; text-decoration: underline; text-underline: single" href="mailto:reservation@intersky.biz">reservation@intersky.biz</a>
							<br/>
							Web: <a title="blocked::http://www.intersky.biz" style="color: blue; text-decoration: underline; text-underline: single" href="http://www.intersky.biz">www.intersky.biz</a>

							<p/>
							<br/>Firmenbuch-Nr. FN 215648 f
							<br/>UID No. ATU 53122909

							<br/>
							<br/>Geschäftsführung: Claus Bernatzik


							<br/>
							<br/>Diese E-Mail kann vertrauliche und/oder rechtlich geschützte Informationen enthalten. Wenn Sie nicht der richtige Adressat sind oder diese E-Mail irrtümlich erhalten haben, informieren Sie bitte sofort den Absender und vernichten Sie diese E-Mail. Das unerlaubte Kopieren sowie die unbefugte Weitergabe dieser E-Mail und ihrer Inhalte ist nicht gestattet.


							<br/>
							<br/>This e-mail may contain confidential and/or privileged information. If you are not the intended addressee or have received this e-mail in error, please notify the sender immediately and destroy this e-mail. Any unauthorized copying, disclosure or distribution of the material in this e-mail is strictly forbidden.</td>
					</tr>
				</table>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\a\EN_EMAIL_LIGHT_May25_184236_A033O9.XML" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->