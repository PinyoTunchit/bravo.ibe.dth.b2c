<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="PageTitle">tikAERO BookingHistory</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable> 
	<xsl:variable name="ColorSpan">Red</xsl:variable>
	<xsl:variable name="passenger_id" select="BookingHistory/Booking/Header/BookingHeader"/>

	<xsl:template name="format-date">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<xsl:value-of select="concat($day, $month, $year)" />
	</xsl:template>

	<xsl:template match="/">
		<HTML>
		<HEAD>
			<TITLE><xsl:value-of select="$PageTitle"/></TITLE>
			<STYLE>
				.PanelBookingHistory .BookingNumber{
					color: #C4102F;
				}

				.PanelBookingHistory .PanelHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #C4102F;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 5px 0px 5px 5px;
						width: 100%;
					}
				.PanelBookingHistory .PanelHeader01
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #C4102F;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 5px 0px 5px 5px;
						 
					}
				.PanelBookingHistory .PanelHeader IMG{
					vertical-align: middle;
				}

				.PanelBookingHistory .PanelHeader SPAN{
					padding-left: 4;
				}

				.PanelBookingHistory .PanelContainer{
					padding: 10 0 10 20;
					width: 100%;
					display: block;
				}
				.PanelBookingHistory .GridHeader{
					{
						background:  #fdb813;
						color: #003964;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						padding: 2px 2px 2px 5px;
					}
				.PanelBookingHistory .GridBooking
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					color: #031668;
					height: 20px
				}
				.PanelBookingHistory TR.GridItems
					{
					 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}

				.PanelBookingHistory TR.GridType
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					color:  #003964;
 					height: 40px;
				}
				.PanelBookingHistory TR.GridRemark
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 10px;
					color:  #003964;
 					height: 20px;
				}
				.PanelBookingHistory TR.GridTR
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					color:  #003964;
 					height: 20px;
				}
				.PanelBookingHistory .GridItems TD
					{
						 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}

				.PanelBookingHistory .GridFooter TD SPAN
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					height: 18px;
					padding: 4 5 0 4;
				}

				.PanelBookingHistory .FooterTotal
				{
					border-bottom: solid 1px #fdb813;
				}

				.PanelBookingHistory SPAN.FooterTotalLabel
					{
							background-color: transparent;
							color:  #003964;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							height: 22px;
							padding: 1px 2px 1px 5px;
					}

				.PanelBookingHistory SPAN.FooterTotalValue
					{
						color: #000066;
						font-size: 11px;
					}
				.PanelBookingHistory .BarcodeHeader
				{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 10px;
					font-weight: bold;
					color: #00529d;
					padding: 5 0 3 5;
					margin: 0 0 0 0;
					width:  100%;
					cursor: hand;
				}

				.PanelBookingHistory .Barcode
				{
					padding: 1 5 1 5;
				}

				.WelcomeText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				.CommentText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				SPAN.Barcode {
				}
				SPAN.Barcode SPAN{
					background: Black;
					height:30px;
				}
				SPAN.Barcode IMG{
					background: Black;
					height:30px;
				}
				SPAN.Barcode IMG.B11{
					width: 1px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B12{
					width: 1px;
					margin: 0px 2px 0px 0px;
				}
				SPAN.Barcode IMG.B21{
					width: 2px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B22{
					width: 2px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B1{
					width: 1px;
				}
				SPAN.Barcode SPAN.TicketNumber{
					background: White;
					font-family: tahoma, verdana, arial, helvetica, sans-serif;
					font-size: 8pt;
					width:  100%;
					padding-left: 10px;
				}
				.locale {
				width: 500;
				border: 1px;
				border-style: solid;
				margin: 10px;
				}
				
			</STYLE>
		</HEAD>
		<BODY>
	 <DIV style="width:100%;">
		<DIV class="PanelBookingHistory">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
 
<tr>
	<td>
		<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
			<TR class="GridType">					
				<TD align="left">					
					<DIV class="PanelHeader" style="font-size: 12px;width=100%;">Booking Header</DIV>
				</TD>
 			</TR>
			<TR>
			
			</TR>
		</TABLE>
	</td>
	<td>
		<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
			<TR class="GridType">					
				<TD align="left">					
					<DIV class="PanelHeader" style="font-size: 12px;width=50%;">Client Profile</DIV>
				</TD>
				</TR>
				
		</TABLE>
	</td>
</tr>
<tr>
	<td>
	<xsl:for-each select="Booking/Header/BookingHeader">
								<DIV class="PanelContainer">	
								<TABLE cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Booking No.:</SPAN>
										</TD>
										<TD valign="middle"  height="20">
										<xsl:value-of select="booking_number"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Booking Reference:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="record_locator"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Agency Code:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="agency_code"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Agency Name:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="agency_name"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Contact Name:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="contact_name"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Email:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="contact_email"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Mobile Phone:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="phone_mobile"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Home Phone:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="phone_home"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Business Phone:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="phone_business"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Fax:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="phone_fax"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Received from:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="received_from"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Create by:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20">
										<xsl:value-of select="create_name"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										<SPAN class="GridBooking">Create Date:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20" >
											<xsl:if test ="create_date_time != ''">
											<xsl:value-of select="substring(create_date_time,7,2)"/>/<xsl:value-of select="substring(create_date_time,5,2)"/>/<xsl:value-of select="substring(create_date_time,1,4)"/>
											</xsl:if>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										 <SPAN class="GridBooking">Update by:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20" >
										 <xsl:value-of select="update_name"/>
										</TD>
									</TR>
									<TR class="GridType">
										<TD align="left" valign="middle" width="25%" height="20">
										 <SPAN class="GridBooking">Update Date:</SPAN>
										</TD>
										<TD valign="middle" width="25%" height="20" >
										<xsl:if test ="update_date_time != ''">
											<xsl:value-of select="substring(update_date_time,7,2)"/>/<xsl:value-of select="substring(create_date_time,5,2)"/>/<xsl:value-of select="substring(create_date_time,1,4)"/>
											</xsl:if>
										</TD>
									</TR>
						 		</TABLE>
							</DIV>
							
							</xsl:for-each>
	</td>
	<td>
	<xsl:for-each select="Booking/Header/BookingHeader">
					<DIV class="PanelContainer">	
						<TABLE cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Client Number:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:if test="client_number > '0'">
								<xsl:value-of select="client_number"/>
								</xsl:if>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Member Number:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_member_number"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Client Name:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_firstname"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Client Contact:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_contact_name"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Address:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
							<xsl:if test="client_address_line1 != ''">
							<xsl:value-of select="client_address_line1"/> &#160;
							</xsl:if>
							<xsl:if test="client_address_line2 != ''">
								<xsl:value-of select="client_address_line2"/>&#160;
							</xsl:if>
							<xsl:if test="client_street != ''">
								<xsl:value-of select="client_street"/> &#160;
							</xsl:if>
							<xsl:if test="client_state != ''">
								<xsl:value-of select="client_state"/>&#160;
							</xsl:if>
							<xsl:if test="client_district != ''">
								<xsl:value-of select="client_district"/>&#160;
							</xsl:if>
							<xsl:if test="client_province != ''">
								<xsl:value-of select="client_province"/>&#160;
							</xsl:if>
							<xsl:if test="client_city != ''">
								<xsl:value-of select="client_city"/>&#160;
							</xsl:if>
							<xsl:if test="client_zip_code != ''">
								<xsl:value-of select="client_zip_code"/>
							</xsl:if>
							</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Country:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_country_rcd"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">PO Box:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_po_box"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Email:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_contact_email"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Mobile Phone:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_phone_mobile"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Home Phone:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_phone_home"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Business Phone:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_phone_business"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Fax:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_phone_fax"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								<SPAN class="GridBooking">Language:</SPAN>
								</TD>
								<TD valign="middle" width="25%" height="20" >
								<xsl:value-of select="client_language_rcd"/>
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								 &#160;
								</TD>
								<TD valign="middle" width="25%" height="20" >
								 &#160;
								</TD>
							</TR>
							<TR class="GridType">
								<TD align="left" valign="middle" width="25%" height="20">
								 &#160;
								</TD>
								<TD valign="middle" width="25%" height="20" >
								 &#160;
								</TD>
							</TR>
							
						</TABLE>
						</DIV>
						</xsl:for-each>
	</td>
</tr>
</table>
			<DIV class="PanelHeader" style="font-size: 12px;"> 
					<SPAN>Your Itinerary&#160;(<xsl:value-of select="count(Booking/Itinerary/FlightSegment)" />)</SPAN>
			</DIV>
				<DIV class="PanelContainer">
					<TABLE class="Table" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
						<TR class="GridHeader">
							<TD></TD>
							<TD>Flight</TD>
							<TD>From</TD>
							<TD>To</TD>
							<TD>Date</TD>
							<TD>Dep</TD>
							<TD>Arr</TD>
							<TD>Class</TD>
							<TD>Status</TD>
							<TD>Unit</TD>
						</TR>
						<xsl:for-each select="Booking/Itinerary/FlightSegment">
					<TR align="left" valign="middle" bgcolor="#ffffff" class="GridTR">
								<TD>
								<xsl:number value="position()" format="001 "/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<SPAN><xsl:value-of select="airline_rcd"/>&#160;<xsl:value-of select="flight_number"/></SPAN>
									<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" >Update by:</TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left">Update Date:</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">Create by:</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">Create Date:</TD>
										</TR>
										
									 </TABLE>
								</TD>
								<TD>
								<xsl:value-of select="origin_name"/><xsl:if test="origin_rcd !=''">&#160;(<xsl:value-of select="origin_rcd"/>)</xsl:if>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN><xsl:value-of select="update_name"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ><xsl:value-of select="update_date_time"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN><xsl:value-of select="create_name"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN><xsl:value-of select="create_date_time"/></SPAN></TD>
										</TR>
										
									 </TABLE>
								</TD>
								<TD><xsl:value-of select="destination_name"/><xsl:if test="destination_rcd !=''">&#160;(<xsl:value-of select="destination_rcd"/>)</xsl:if>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<xsl:if test ="departure_date != ''">
								<xsl:value-of select="substring(departure_date,7,2)"/>/<xsl:value-of select="substring(departure_date,5,2)"/>/<xsl:value-of select="substring(departure_date,1,4)"/>
								</xsl:if>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<xsl:if test ="departure_time != 0">
								<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/>
								</xsl:if>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<xsl:if test ="planned_arrival_time != 0">
								<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/>
								</xsl:if>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD><SPAN><xsl:value-of select="class_name"/></SPAN>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<xsl:choose>
										<xsl:when test="status_name =''">
											 &#160;
										</xsl:when>
										<xsl:otherwise>
										 <xsl:value-of select="status_name"/>
										</xsl:otherwise>
									</xsl:choose>
									<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD><xsl:value-of select="number_of_units"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
 							<TD>&#160;</TD>
 							<TD>&#160;</TD> 
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
						</TR>
						</xsl:for-each>
					</TABLE>
				</DIV>
				<DIV class="PanelHeader" style="font-size: 12px;"> 
				<SPAN>Passengers&#160;(<xsl:value-of select="count(Booking/Passengers/Passenger)" />)</SPAN>
				</DIV>
				<DIV class="PanelContainer">
					<TABLE cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
						<TR class="GridHeader">
							<TD>&#160;</TD>
							<TD>Lastname</TD>
							<TD>Firstname</TD>
							<TD>Title</TD>
							<TD>Type</TD>
							<TD>Date of Birth</TD>
						</TR>
						<xsl:for-each select="Booking/Passengers/Passenger">
							<TR align="left" valign="middle" bgcolor="#ffffff" class="GridTR">
								<TD><xsl:number value="position()" format="001 "/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
								<TD><xsl:value-of select="lastname"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" >Update by:</TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left">Update Date:</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">Create by:</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">Create Date:</TD>
										</TR>
										
									 </TABLE>
								</TD>
								<TD><xsl:value-of select="firstname"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN><xsl:value-of select="update_name"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ><xsl:value-of select="update_date_time"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN><xsl:value-of select="create_name"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN><xsl:value-of select="create_date_time"/></SPAN></TD>
										</TR>
										
									 </TABLE>
								</TD>
								<TD><xsl:value-of select="title_rcd"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
								<TD><xsl:value-of select="passenger_type_rcd"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
								<TD>
								<xsl:if test="date_of_birth != '' ">
								<xsl:value-of select="substring(date_of_birth,7,2)"/>/<xsl:value-of select="substring(date_of_birth,5,2)"/>/<xsl:value-of select="substring(date_of_birth,1,4)"/>
								</xsl:if>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						<TR valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
 							<TD>&#160;</TD>
 							<TD>&#160;</TD> 
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
			 			</TR>
						</xsl:for-each>
					</TABLE>
				</DIV>
				<DIV class="PanelHeader" style="font-size: 12px;"><SPAN>Tickets&#160;(<xsl:value-of select="count(Booking/Tickets/Ticket)" />)</SPAN></DIV>
				<DIV class="PanelContainer">
					<TABLE cellspacing="0" rules="all" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
						<TR class="GridHeader">
							<TD>&#160;</TD>
							<TD>Ticket Number</TD>
							<TD>Passenger Name</TD>
							<TD>Flight</TD>
							<TD>Date</TD>
							<TD>Status</TD>
							<TD>Type</TD>
							<TD align="Center">	Total Net</TD>
							<TD align="Center">eTKT</TD>
							<TD>Seat</TD>
						</TR>
						<xsl:for-each select="Booking/Tickets/Ticket">
							<TR align="left" valign="middle" bgcolor="#ffffff" class="GridTR">
								<TD><xsl:number value="position()" format="001 "/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD><xsl:value-of select="ticket_number"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" >Update by:</TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left">Update Date:</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">Create by:</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">Create Date:</TD>
										</TR>
										
									 </TABLE>
								</TD>
								<TD><SPAN><xsl:value-of select="lastname"/>/<xsl:value-of select="firstname"/></SPAN>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN><xsl:value-of select="update_name"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ><xsl:value-of select="update_date_time"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN><xsl:value-of select="create_name"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN><xsl:value-of select="create_date_time"/></SPAN></TD>
										</TR>
										
									 </TABLE>
								</TD>
								<TD><SPAN><xsl:value-of select="airline_rcd"/>&#160;<xsl:value-of select="flight_number"/></SPAN>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<xsl:value-of select="substring(departure_date,7,2)"/>/<xsl:value-of select="substring(departure_date,5,2)"/>/<xsl:value-of select="substring(departure_date,1,4)"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								
								<TD>
								<xsl:choose>
										<xsl:when test="passenger_status_rcd = ''">
											 &#160;
										</xsl:when>
										<xsl:otherwise>
										 <xsl:value-of select="passenger_status_rcd"/>
										</xsl:otherwise>
									</xsl:choose>
									<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD><SPAN><xsl:value-of select="passenger_type_rcd"/></SPAN>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD align="Right">
								<SPAN><xsl:value-of select="format-number(net_total,'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD align="Center">
									<xsl:choose>
										<xsl:when test="string(e_ticket_flag)='1'">
											Y
										</xsl:when>
										<xsl:otherwise>
											N
										</xsl:otherwise>
									</xsl:choose>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD><xsl:value-of select="seat_number"/></TD>
							</TR>
							<TR valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
	 							<TD>&#160;</TD>
	 							<TD>&#160;</TD> 
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD> 
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD> 
						</TR>
						</xsl:for-each>
						<TR class="GridFooter">
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD>
								<TD class="FooterTotal"><SPAN class="FooterTotalLabel">Total Amount:</SPAN></TD>
								<TD class="FooterTotal" align="Right"><SPAN class="FooterTotalValue"><xsl:value-of select="format-number(sum(Booking/Tickets/Ticket/net_total),'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN></TD>
								<TD>&#160;</TD>
								<TD>&#160;</TD>
						</TR>
					</TABLE>
				</DIV>
				<DIV class="PanelHeader" style="font-size: 12px;"><SPAN>Construction / Endorsements&#160;(<xsl:value-of select="count(Booking/Tickets/Ticket)" />)</SPAN></DIV>
			<DIV class="PanelContainer">
				<TABLE width="100%" class="Table" cellspacing="0" rules="none" border="0" style="border-width:0px;border-collapse:collapse;">
						<TR class="GridHeader">
							<TD>&#160;</TD>
							<TD>Ticket Number</TD>
							<TD>Fare Code</TD>
							<TD align="right">Total</TD>
							<TD><SPAN></SPAN></TD>
							<TD>Endorsement</TD>
							<TD>Restriction</TD>
						</TR>
						<xsl:for-each select="Booking/Tickets/Ticket">
							<TR align="left" valign="middle" bgcolor="#ffffff" class="GridTR">
								<TD>
								<xsl:number value="position()" format="001 "/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<xsl:value-of select="ticket_number"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" >Update by:</TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left">Update Date:</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">Create by:</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">Create Date:</TD>
										</TR>
										
									 </TABLE>
								</TD>
								<TD>
								<xsl:value-of select="fare_code"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN><xsl:value-of select="update_name"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ><xsl:value-of select="update_date_time"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN><xsl:value-of select="create_name"/></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN><xsl:value-of select="create_date_time"/></SPAN></TD>
										</TR>
										
									 </TABLE>
								</TD>
								<TD align="right"><xsl:value-of select="format-number(net_total,'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD><SPAN></SPAN>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<xsl:choose>
									<xsl:when test="endorsement_text = '0'">
										 <xsl:text>&#160;</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="endorsement_text"/>
									</xsl:otherwise>
								</xsl:choose>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD>
								<xsl:choose>
									<xsl:when test="restriction_text = '0'">
										 <xsl:text>&#160;</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="restriction_text"/>
									</xsl:otherwise>
								</xsl:choose>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
							</TR>
							<TR valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
 							<TD>&#160;</TD>
 							<TD>&#160;</TD> 
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD> 
						</TR>
						</xsl:for-each>
					</TABLE>
				</DIV>
				<BR></BR>
				<DIV class="PanelHeader" style="font-size: 12px;"><SPAN>Payments&#160;(<xsl:value-of select="count(Booking/Payments/Payment)" />)</SPAN></DIV>
				<DIV class="PanelContainer">
					<TABLE width="100%" class="Table" cellspacing="0" rules="non" style="border-width:0px;border-collapse:collapse;" >
						<TR class="GridHeader">
							<TD></TD>
							<TD align="left"><SPAN class="GridHeader">Form of Payment</SPAN></TD>
							<TD align="right"><SPAN class="GridHeader">Amount</SPAN></TD>
							<TD align="center"><SPAN class="GridHeader">Payment Date</SPAN></TD>
						</TR>
 						<xsl:for-each select="Booking/Payments/Payment">
							<TR valign="middle" align="left" bgcolor="#ffffff" class="GridTR">
								<TD><xsl:number value="position()" format="001 "/>
									<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
								<TD align="left"><xsl:value-of select="form_of_payment_rcd"/>
									<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD  align="left"><SPAN>Void by:</SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN >Void Date:</SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN>Update by:</SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN>Update Date:</SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN>Create by:</SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN>Create Date:</SPAN></TD>
										</TR>
										
									</TABLE>
								</TD>
								<TD align="right">
								<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">				
										<TD align="left"><xsl:value-of select="void_name"/></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"> 
										<xsl:choose>
										<xsl:when test="void_date_time = ''">
											 <SPAN><BR></BR></SPAN> 
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="void_date_time"/>
										</xsl:otherwise>
									</xsl:choose>
										
										</TD>
										</TR>

										<TR class="GridRemark">
										<TD align="left"><xsl:value-of select="update_name"/></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">
										<xsl:value-of select="update_date_time"/>
										</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><xsl:value-of select="create_name"/></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">
										<xsl:value-of select="create_date_time"/>
										</TD>
										</TR>
									</TABLE>
								</TD>
								<TD align="center">
								<xsl:if test="payment_date_time != ''">
								<xsl:value-of select="substring(payment_date_time,7,2)"/>/<xsl:value-of select="substring(payment_date_time,5,2)"/>/<xsl:value-of select="substring(payment_date_time,1,4)"/>
								</xsl:if>
									<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
 							</TR>
						<TR valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
 							<TD>&#160;</TD>
 							<TD>&#160;</TD> 
							<TD>&#160;</TD>
							<TD>&#160;</TD>
						</TR>
						</xsl:for-each>
						<TR valign="middle" align="left" bgcolor="#ffffff">
 							<TD>&#160;</TD>
  							<TD>&#160;</TD>
							<TD class="FooterTotal" align="Right">
							<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left"><SPAN class="FooterTotalLabel">Total Amount:</SPAN></TD>
										<TD align="right" ><SPAN class="FooterTotalValue"><xsl:value-of select="format-number(sum(Booking/Payments/Payment[not(substring(void_date_time,5,2))]/payment_amount),'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN></TD>
										</TR>
							</TABLE>
							</TD>
							</TR>
						<TR valign="middle" align="left" bgcolor="#ffffff">
 							<TD>&#160;</TD>
 							<TD>&#160;</TD>
							<TD class="FooterTotal" align="Right">
							<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
							<xsl:variable name="Ticket_total"   select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>
							<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left"><SPAN class="FooterTotalLabel">Balance:</SPAN></TD>
										<TD align="right" ><SPAN class="FooterTotalValue"><xsl:value-of select="format-number(select=$Ticket_total - $Payment_total,'###,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN></TD>
										</TR>
							</TABLE>
							</TD>
						</TR>
					</TABLE>
				</DIV>
				
				<BR></BR>
				<DIV class="PanelHeader" style="font-size: 12px;"><SPAN>Remarks&#160;(<xsl:value-of select="count(Booking/Remarks/Remark)" />)</SPAN></DIV>
				<DIV class="PanelContainer">
						<TABLE width="100%" class="Table" cellspacing="0" rules="non" style="border-width:0px;border-collapse:collapse;" >
						<TR class="GridHeader">
							<TD></TD>
							<TD align="left"><SPAN class="GridHeader">Category</SPAN></TD>
							<TD align="left"><SPAN class="GridHeader">Time Limit</SPAN></TD>
							<TD align="left"><SPAN class="GridHeader">Text</SPAN></TD>
							<TD align="left"><SPAN class="GridHeader">Agent</SPAN></TD>
						</TR>
						
						<xsl:for-each select="Booking/Remarks/Remark">
						 
							<TR align="left" valign="middle" bgcolor="#ffffff" class="GridTR">
								<TD><xsl:number value="position()" format="001 "/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
									<TD align="left"><xsl:value-of select="display_name"/>
									<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD  align="left" ><SPAN>Create by::</SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN >Create Date</SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN>Update by:</SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN>Update Date:</SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN>Complete:</SPAN></TD>
										</TR>
									</TABLE>
									</TD>
								<TD align="left">
									<xsl:choose>
										<xsl:when test="timelimit_date_time = ''">
											 <SPAN><BR></BR></SPAN> 
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="timelimit_date_time"/>
										</xsl:otherwise>
									</xsl:choose>
	 								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">				
										<TD align="left"><xsl:value-of select="create_name"/></TD>
										</TR>										
										<TR class="GridRemark">
										<TD align="left" >
											<xsl:value-of select="create_date_time"/>
		 								</TD>
										</TR>
										
										<TR class="GridRemark">
										<TD align="left"><xsl:value-of select="update_name"/></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">
											 <xsl:value-of select="update_date_time"/>
										</TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left">
									 
										<xsl:choose>
												<xsl:when test="string(complete_flag) = '1'">
													Yes
												</xsl:when>
												<xsl:otherwise>
													No
												</xsl:otherwise>
											</xsl:choose>
										</TD>								
										</TR>
								</TABLE>
								</TD>

								<TD align="left"><xsl:value-of select="remark_text"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
 								<TD align="left"><xsl:value-of select="added_by"/>
								<TABLE width="100%" class="Table" rules="none" cellspacing="0" border="0">
										<TR class="GridRemark">
										<TD align="left" ><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">				
										<TD align="left"><SPAN ></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
										<TR class="GridRemark">
										<TD align="left"><SPAN></SPAN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
					 <TR valign="middle" align="left" bgcolor="#ffffff" class="GridItems">
 							<TD>&#160;</TD>
 							<TD>&#160;</TD> 
							<TD>&#160;</TD>
							<TD>&#160;</TD>
							<TD>&#160;</TD>
						</TR>
						</xsl:for-each>
						
					</TABLE>
				</DIV>
				 
				 <xsl:if test="//SpecialService = true()">	
				<DIV  class="PanelHeader" style="font-size: 12px;"> <SPAN>Special Services&#160;(<xsl:value-of select="count(Booking/SpecialServices/SpecialService)" />)</SPAN></DIV>
				<DIV class="PanelContainer">
						<TABLE width="100%" class="Table" cellspacing="0" rules="non" style="border-width:0px;border-collapse:collapse;" >
						<TR class="GridHeader">
							<TD>&#160;</TD>
							<TD>Filght</TD>
							<TD>Passenger Name</TD>
							<TD>Special Service</TD>
							 
						</TR>
						
						<xsl:for-each select="Booking/SpecialServices/SpecialService">
							<TR class="GridItems">
								<TD><xsl:value-of select="concat('00','',string(position()))"/></TD>
								<TD><xsl:value-of select="airline_rcd"/>&#160;<xsl:value-of select="flight_number"/></TD>
								<TD><xsl:value-of select="lastname" />/<xsl:value-of select="firstname" /></TD>
								<TD><xsl:value-of select="special_service_rcd" />&#160;&#160;<xsl:value-of select="display_name" /> </TD>
 							</TR>
						</xsl:for-each>
					</TABLE>
 				</DIV>
		 		</xsl:if>
		</DIV> 
		</DIV>
		</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\a\yed1.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->