﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				                      xmlns:tikLanguage="tik:Language">
	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; UTF-8" />
				<title>Peach | Peach</title>
				<style>
					html, body {	height:auto;margin: 0;	padding: 0;	background-color: #ffffff;	font: 11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial;height:100%;}
					*html html, body {	margin: 0;	padding: 0;	background-color:#ffffff;	height: 100%;	FONT:11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial;height:100%;}
					IMG {BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; BORDER-BOTTOM: 0px}

					.ImgHeader{ background-color:#72c367; width:860px; height:120px;}
					
					.WrapperBody{ padding:20px 20px 50px 20px; width:720px; min-height:180px; _height:620px; background-color:#FFFFFF;}

					/*Footer*/
					/*.Footer{ background-image: url(../Images/ImgFooter.jpg); width:860px; height:24px; font:11px Meiryo,MS PGothic,ヒラギノ角ゴ Pro W3,Osaka,Tahoma,Arial; }*/
					.Footer li{ padding-top:13px; list-style:none; color:#FFFFFF; float:left;}
					.EmailBodyText{height:22px; color:#174a7c; padding-bottom:10px;}
					.EmailUser { height:22px; color:#174a7c;}
					.EmailBody{height:22px;color:#174a7c;}
					
					/*********************************/
					.Wrapper { WIDTH:760px; TEXT-ALIGN: left; margin: 0 auto; }
					.mainlogo {margin-bottom: 20px; margin-left: 15px;}
					.emailregistercontent {margin-left: 15px;}
					.emailregisterfooter {background: #b634bb; float: left; width: 100%; color: #FFF;}
					.emailregisterfooter td, .emailregisterfooter td a {color: #FFF; text-decoration: none;}
					.emailregisterfooter .COL1 {width: 50%; margin-left: 15px; float: left; text-decoration: none;}
					.emailregisterfooter .COL2 {float: right; margin-right: 15px; text-align: right;}
					.emailregisterfooter .copyright {float: right; margin-right: 15px;}
					.clear-all {clear: both;}
				</style>
			</head>
			<body>
				<div class="Wrapper">
					<div class="mainlogo">
						<img>
							<xsl:attribute name="src">
								<xsl:value-of select="MailDetail/HeaderURL" />/App_Themes/Default/Images/mainlogo.png
							</xsl:attribute>
						</img>
					</div>
					
					<xsl:if test="MailDetail/Action = 'create'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="MailDetail/Title" />&#160;
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />&#160;先生/小姐 您好：
							</div>
					
							<p>
								感謝您在本網站(www.flypeach.com)註冊Peach帳號。  
							</p>
					
							<p>
								註冊Peach帳號可讓您同時管理複數訂單的行程內容。同時您也可以輕鬆的更改您的個人資料，讓您可以更輕易的在Peach訂購您的行程。
							</p>
					
							<p>
								此外，註冊帳號的同時選擇訂閱Peach電子報的客戶，還能享有優先獲得包括Peach的最新消息、優惠或特價活動等第一手消息的權益。
								若您尚未訂閱電子報，歡迎您到www.flypeach.com進行訂閱。
							</p>
					
							<p>
								以下是您在註冊時輸入的帳號和密碼，請您妥善保管，避免遺失。
							</p>
					
							<p>
								<div>用戶名稱： <span><xsl:value-of select="MailDetail/UserName" /></span></div>
								<div>密碼： 
									<span>
										<xsl:if test="MailDetail/Password = ''">
											Password
										</xsl:if>
										<xsl:if test="MailDetail/Password != ''">
											<xsl:value-of select="MailDetail/Password" />
										</xsl:if>
									</span>
								</div>
							</p>
					
							<p>
								若您要修正帳戶資料，請連結以下網址
								<br/>
								<a href="http://book.flypeach.com/default.aspx?cl=l&#38;langculture=zh-hk">http://book.flypeach.com/default.aspx?cl=l&#38;langculture=zh-hk</a>
							</p>
					
							<p>
								我們衷心期待和您在空中會面。
							</p>
							
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<xsl:if test="MailDetail/Action = 'reset'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="MailDetail/Title" />&#160;
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />&#160;<xsl:value-of select="tikLanguage:get('Email_13','Dear')" />
							</div>
					
							<p>
								Your password has been successfully reset.
							</p>
					
							<p>
								<div>Username: <span><xsl:value-of select="MailDetail/UserName" /></span></div>
								<div>New Password: 
									<span>
										<xsl:value-of select="MailDetail/Password" />
									</span>
								</div>
							</p>
					
							<p>
								If you'd like to amend your profile details, please visit 
								<br/>
								<a href="http://www.flypeach.com/profile">http://www.flypeach.com/profile</a>
							</p>
					
							<p>
								Sincerely yours,
							</p>
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<xsl:if test="MailDetail/Action = 'change'">
						<div class="emailregistercontent">
							<div>
								<xsl:value-of select="MailDetail/Title" />&#160;
								<xsl:value-of select="MailDetail/FirstName" />&#160;
								<xsl:value-of select="MailDetail/LastName" />&#160;<xsl:value-of select="tikLanguage:get('Email_13','Dear')" />
							</div>
					
							<p>
								Your password has been successfully changed.
							</p>
					

							<p>
								If you'd like to amend your profile details, please visit 
								<br/>
								<a href="http://www.flypeach.com/profile">http://www.flypeach.com/profile</a>
							</p>
					
							<p>
								Sincerely yours,
							</p>
					
							<p>
								Peach Aviation Limited
								<br/>
								<a href="https://book.flypeach.com/default.aspx?rg=l">www.flypeach.com</a>
							</p>
					
						</div>
					</xsl:if>
					
					<div class="clear-all"></div>
					
					<table class="emailregisterfooter">
						<tr>
							<td class="COL1"><a href="http://www.flypeach.com/hk/corporate/termsandconditions.aspx">網站服務條款</a> | <a href="http://www.flypeach.com/hk/corporate/privacypolicy.aspx">隱私權聲明</a></td>
							<td class="COL2">Copyright © Peach Aviation Limited</td>
						</tr>
						
					</table>
				</div>

			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>