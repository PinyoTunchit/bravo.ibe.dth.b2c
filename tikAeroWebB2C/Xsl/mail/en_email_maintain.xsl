<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				                      xmlns:tikLanguage="tik:Language">
	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
				<title>aurigny.com – channel islands</title>
				<style>
					html, body {	height:auto;margin: 0;	padding: 0;	background-color: #ffffff;	FONT-FAMILY: Tahoma, Arial, sans-serif, verdana;	font-size:12px;height:100%;}
					*html html, body {	margin: 0;	padding: 0;	background-color:#ffffff;	height: 100%;	FONT-FAMILY: Tahoma, Arial, sans-serif, verdana;	font-size:12px;height:100%;}
					IMG {BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; BORDER-BOTTOM: 0px}

					.ImgHeader{ background-color:#72c367; width:860px; height:120px;}
					.Wrapper { background-color:#FFFFFF; WIDTH:760px; height:auto; TEXT-ALIGN: left; margin:0px auto; }
					.WrapperBody{ padding:20px 20px 50px 20px; width:720px; min-height:180px; _height:620px; background-color:#FFFFFF;}

					/*Footer*/
					.Footer{ background-image: url(../Images/ImgFooter.jpg); width:860px; height:24px; font-family:Verdana,Arial, Helvetica, sans-serif; font-size:9px; }
					.Footer li{ padding-top:13px; list-style:none; color:#FFFFFF; float:left;}
					.EmailBodyText{height:22px; color:#174a7c; padding-bottom:10px;}
					.EmailUser { height:22px; color:#174a7c;}
					.EmailBody{height:22px;color:#174a7c;}

				</style>
			</head>
			<body>

				<table cellpadding="0" cellspacing="0"  class="Wrapper">
					<tr>
						<td>
							<img>
								<xsl:attribute name="src">
									<xsl:value-of select="MailDetail/HeaderURL" />/App_Themes/Default/Images/EmailImgHeader.jpg
								</xsl:attribute>
							</img>
						</td>
					</tr>

					<xsl:if test="MailDetail/Action = 'create'">
						<tr>
							<td class="WrapperBody">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="EmailBody">
											<xsl:value-of select="tikLanguage:get('Email_13','Dear')" />&#160; <xsl:value-of select="MailDetail/Title" />&#160;
											<xsl:value-of select="MailDetail/LastName" />
										</td>
									</tr>
									<tr>
										<td class="EmailBody">
											&#160;
										</td>
									</tr>
									<tr>
										<td class="EmailBodyText">
											<p>
												<!--Thank you for booking with Merpati Nusantara Airlines. Your user name and password have been established and you are now ready to book through the Login menu at <a href="http://www.merpati.co.id" target="_blank">www.merpati.co.id</a>. Please select the Corporate link, enter the following details, and then LOGIN. -->
											<xsl:value-of select="tikLanguage:get('Email_2','Thank you for registering as a member of our programme. 
														  Your Frequent Flyer Number and password have been created and you can now use these to log on the next time you 
														  visit')" /><a href="http://www.aurigny.com" target="_blank">aurigny.com</a>
											</p>
										</td>
									</tr>
									<tr>
										<td class="EmailBody">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td style="width:250px;" class="EmailBodyText">
														<!--User Name:--><xsl:value-of select="tikLanguage:get('Email_4','Your Frequent Flyer Number is')" />:
													</td>
													<td class="EmailBodyText">
														<xsl:value-of select="MailDetail/UserName" />
													</td>
												</tr>
												<tr>
													<td style="width:250px;" class="EmailBodyText">
														<xsl:value-of select="tikLanguage:get('Email_5','Your password is')" />:
													</td>
													<td class="EmailBodyText">
														<xsl:if test="MailDetail/Password = ''">
															Password
														</xsl:if>
														<xsl:if test="MailDetail/Password != ''">
															<xsl:value-of select="MailDetail/Password" />
														</xsl:if>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="EmailBodyText">
											<p>
												<xsl:value-of select="tikLanguage:get('Email_6','From the Frequent Flyer Team')" />
												<!--If you have any questions, or wish to change your password, please contact Call Center 021-6546789.-->
											</p>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td>&#160;</td>
						</tr>
					</xsl:if>

					<xsl:if test="MailDetail/Action = 'reset'">
						<tr>
							<td class="WrapperBody">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="EmailBody">
											<xsl:value-of select="tikLanguage:get('Email_13','Dear')" /> &#160;<xsl:value-of select="MailDetail/Title" />&#160;
											<xsl:value-of select="MailDetail/LastName" />
										</td>
									</tr>
									<tr>
										<td class="EmailBody">
											&#160;
										</td>
									</tr>
									<tr>
										<td class="EmailBodyText">
											<p>
												<!--Thank you for booking with Merpati Nusantara Airlines. Your user name and password have been reset and you are now ready to book through the Login menu at <a href="http://www.merpati.co.id" target="_blank">www.merpati.co.id</a>. Please select the Corporate link, enter the following details, and then LOGIN.-->
												<xsl:value-of select="tikLanguage:get('Email_12','Your password has been successfully reset, 
															  the next time you visit')" /><a href="http://www.aurigny.com" target="_blank">aurigny.com</a>
												please enter your membership number.
											</p>
										</td>
									</tr>
									<tr>
										<td class="EmailBody">
											<strong>
												<xsl:value-of select="tikLanguage:get('Email_13','New Password')" />:
											</strong>&#160;
											<xsl:value-of select="MailDetail/Password" />
										</td>
									</tr>
									<tr>
										<td class="EmailBodyText">
											<p>
												<br/>
												<!--If you have any questions, or wish to change your password, please contact Call Center 021-6546789.-->
												<xsl:value-of select="tikLanguage:get('Email_16','You will then be able to change your password to something more memorable using the edit facility on the “my profile” page.')" />
											</p>
										</td>
									</tr>
									<tr>
										<td class="EmailBodyText">
											<p>
												<br/>
												<xsl:value-of select="tikLanguage:get('Email_6','Aurigny.com')" />
												<!--If you have any questions, or wish to change your password, please contact Call Center 021-6546789.-->
											</p>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td>&#160;</td>
						</tr>
					</xsl:if>

					<xsl:if test="MailDetail/Action = 'change'">
						<tr>
							<td class="WrapperBody">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="EmailBody">
											<xsl:value-of select="tikLanguage:get('Email_13','Dear')" />&#160; <xsl:value-of select="MailDetail/Title" />&#160;
											<xsl:value-of select="MailDetail/LastName" />
										</td>
									</tr>
									<tr>
										<td class="EmailBody">
											&#160;
										</td>
									</tr>
									<tr>
										<td class="EmailBodyText">
											<xsl:value-of select="tikLanguage:get('Email_13','Your password has been successfully changed, 
														  and can be used the next time you visit')" /><a href="http://www.aurigny.com" target="_blank">aurigny.com</a>
										</td>
									</tr>
									<tr>
										<td class="EmailBodyText">
											<p>
												<br/>
												<xsl:value-of select="tikLanguage:get('Email_6','From the Frequent Flyer Team')" />
											</p>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td>&#160;</td>
						</tr>
					</xsl:if>

					<!-- end wrapper -->

					<tr>
						<td>
							<div class="Footer">
								<img>
									<xsl:attribute name="src">
										<xsl:value-of select="MailDetail/FooterURL" />/App_Themes/Default/Images/EmailImgFooter.jpg
									</xsl:attribute>
								</img>
							</div>
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>