<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:output method="html" indent="no" encoding="UTF-8" omit-xml-declaration="yes"/>

	<xsl:key name="passenger_id_group" match="//Passengers/Passenger" use="passenger_id"/>
	<xsl:key name="passenger_type_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<!--Variable-->
	<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>
	<xsl:variable name="crlf" select="'&#xD;&#xA;'"/>
	<xsl:variable name="cr" select="'&#xA;'"/>
	<xsl:variable name="passenger_id" select="Booking/Tickets/Ticket/passenger_id"/>
	<xsl:variable name="booking_segment_id_tax" select="Booking/Tickets/Ticket[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
	<xsl:variable name="booking_segment_id" select="Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
	<xsl:variable name="sum_net_total" select="sum(Booking/Tickets/Ticket/net_total)"/>
	<xsl:variable name="sum_payment_amount" select="sum(Booking/Payments/Payment/payment_amount)"/>
	<!--Template-->

	<!-- Format String -->
	<xsl:template name="CDate2Julian">
		<xsl:param name="date"/>
		<xsl:param name="format" select="000"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="format-number(substring($date,3,2),'00')"/>
		<xsl:variable name="month" select="substring(substring-after('01/00002/03103/05904/09005/12006/15107/18108/21209/24310/27311/30412/334', concat($month1,'/')), 1, 3)"/>
		<xsl:value-of select="$day + $month"/>
	</xsl:template>
	<xsl:template name="format_date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date,7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($year, '/', $month, '/', $day)"/>
	</xsl:template>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($year , '/',$month, '/', $day)"/>
	</xsl:template>
	<xsl:template name="formattime">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('     01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1), 1, 3)"/>
		<xsl:variable name="hours" select="substring($date, 10,2)"/>
		<xsl:variable name="minutes" select="substring($date,13,2)"/>
		<xsl:value-of select="concat($hours ,':', $minutes)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:value-of select="concat($day, '.', $month, '.', $year)"/>
	</xsl:template>
	<xsl:template match="/">

		<xsl:text>PEACH AVIATION ITINERARY</xsl:text>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>
		<!--<xsl:text>ALL GUESTS SHOULD RETAIN A</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>COPY FOR THEIR RECORDS.</xsl:text>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>

		<xsl:text>TRAVEL DETAILS FOR:</xsl:text>
		<xsl:value-of select="$crlf"/>-->

		<xsl:text>BOOKING REF:</xsl:text>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
		<xsl:value-of select="$crlf"/>

		<xsl:text>BOOKING DATE:</xsl:text>
		<xsl:text> </xsl:text>
		<xsl:call-template name="formatdate">
			<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
		</xsl:call-template>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>

		<!--Flights-->
		<xsl:text>YOUR ITINERARY AND TRAVEL DETAILS:</xsl:text>
		
		<xsl:for-each select="Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
			<xsl:choose>
				<!--Transit-->
				<xsl:when test="(string-length(transit_points) &gt; '0')">
					<xsl:text>FLIGHT</xsl:text>
					<xsl:text> </xsl:text>
					<xsl:value-of select="airline_rcd"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="flight_number"/>
					<xsl:text> </xsl:text>
					<xsl:if test="string-length(departure_date) &gt; '0'">
						<xsl:call-template name="formatdate">
							<xsl:with-param name="date" select="departure_date"/>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="string-length(departure_date) = '0'">
						<xsl:text>OPEN</xsl:text>
					</xsl:if>
					<xsl:value-of select="$crlf"/>
					
					
					<xsl:text>DEP</xsl:text>
					<xsl:text> </xsl:text>
					<xsl:value-of select="origin_name"/>
					<xsl:text> </xsl:text>
					<xsl:text>(</xsl:text>
					<xsl:value-of select="origin_rcd"/>
					<xsl:text>)</xsl:text>
					<xsl:text> </xsl:text>
					<xsl:if test="string(departure_time) != '0'">
						<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
					<xsl:value-of select="$cr"/>
					
					<xsl:text>ARR</xsl:text>
					<xsl:text> </xsl:text>
					<xsl:value-of select="destination_name"/>
					<xsl:text> </xsl:text>
					<xsl:text>(</xsl:text>
					<xsl:value-of select="destination_rcd"/>
					<xsl:text>)</xsl:text>
					<xsl:text> </xsl:text>
					<xsl:text>Transit in</xsl:text>
					<xsl:text> </xsl:text>
					<xsl:value-of select="transit_points"/>
					<xsl:text> </xsl:text>
					<xsl:if test="string(planned_arrival_time) != '0'">
						<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/></xsl:if>
					<xsl:value-of select="$cr"/>
					
					<xsl:text>Fare Type:</xsl:text>
					<xsl:text> </xsl:text>
						<xsl:for-each select="//Tickets/Ticket[not(passenger_status_rcd='XX')][passenger_id=$passenger_id][booking_segment_id=$booking_segment_id]">
							<xsl:if test="position()=last()">
							<xsl:value-of select="fare_code"/>
							<xsl:text></xsl:text>
							</xsl:if>
						</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$cr"/>
							
					<!--Transfer-->
					<xsl:choose>
						<xsl:when test="not(./od_destination_rcd = ./destination_rcd)">
							<xsl:choose>
								<xsl:when test="(string-length(od_destination_rcd) &gt; '0')">
									<xsl:text>FLIGHT</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:value-of select="airline_rcd"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="flight_number"/>
									<xsl:text> </xsl:text>
									<xsl:if test="string-length(departure_date) &gt; '0'">
										<xsl:call-template name="formatdate">
											<xsl:with-param name="date" select="departure_date"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:if test="string-length(departure_date) = '0'">
										<xsl:text>OPEN</xsl:text>
									</xsl:if>
									<xsl:value-of select="$crlf"/>

									<xsl:text>DEP</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:value-of select="origin_name"/>
									<xsl:value-of select="$cr"/>(<xsl:value-of select="origin_rcd"/><xsl:text>)</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:if test="string(departure_time) != '0'">
										<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
									<xsl:value-of select="$cr"/>

									<xsl:text>ARR</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:value-of select="destination_name"/>
									<xsl:text> </xsl:text>
									<xsl:text>(</xsl:text>
									<xsl:value-of select="destination_rcd"/>
									<xsl:text>)</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:if test="string(planned_arrival_time) != '0'">
										<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/></xsl:if>
									<xsl:value-of select="$cr"/>
									
									<xsl:text>Fare Type:</xsl:text>
									<xsl:text> </xsl:text>
										<xsl:for-each select="//Tickets/Ticket[not(passenger_status_rcd='XX')][passenger_id=$passenger_id][booking_segment_id=$booking_segment_id]">
											
											<xsl:if test="position()=last()"><xsl:value-of select="fare_code"/>
											<xsl:text></xsl:text>
											</xsl:if>
										</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$cr"/>
							
									<xsl:text>FLIGHT</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:value-of select="airline_rcd"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="flight_number"/>
									<xsl:text> </xsl:text>
									<xsl:if test="string-length(departure_date) &gt; '0'">
										<xsl:call-template name="formatdate">
											<xsl:with-param name="date" select="departure_date"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:if test="string-length(departure_date) = '0'">
										<xsl:text>OPEN</xsl:text>
									</xsl:if>
									<xsl:value-of select="$crlf"/>

									<xsl:text>DEP</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:value-of select="origin_name"/>
									<xsl:text> </xsl:text>
									<xsl:text>(</xsl:text>
									<xsl:value-of select="origin_rcd"/>
									<xsl:text>)</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:if test="string(departure_time) != '0'">
										<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
									<xsl:value-of select="$cr"/>

									<xsl:text>ARR</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:value-of select="destination_name"/>
									<xsl:text> </xsl:text>
									<xsl:text>(</xsl:text>
									<xsl:value-of select="destination_rcd"/>
									<xsl:text>)</xsl:text>
									<xsl:text> </xsl:text>
									<xsl:if test="string(planned_arrival_time) != '0'">
										<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/></xsl:if>
									<xsl:value-of select="$cr"/>
									
									<xsl:text>Fare Type:</xsl:text>
									<xsl:text> </xsl:text>
										<xsl:for-each select="//Tickets/Ticket[not(passenger_status_rcd='XX')][passenger_id=$passenger_id][booking_segment_id=$booking_segment_id]">
											<xsl:if test="position()=last()">
											<xsl:value-of select="fare_code"/>
											<xsl:text></xsl:text>
											</xsl:if>
										</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$cr"/>

							<xsl:text>FLIGHT</xsl:text>
							<xsl:text> </xsl:text>
							<xsl:value-of select="airline_rcd"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="flight_number"/>
							<xsl:text> </xsl:text>
							<xsl:if test="string-length(departure_date) &gt; '0'">
								<xsl:call-template name="formatdate">
									<xsl:with-param name="date" select="departure_date"/>
								</xsl:call-template>
							</xsl:if>
							<xsl:if test="string-length(departure_date) = '0'">
								<xsl:text>OPEN</xsl:text>
							</xsl:if>
							<xsl:value-of select="$crlf"/>

							<xsl:text>DEP</xsl:text>
							<xsl:text> </xsl:text>
							<xsl:value-of select="origin_name"/>
							<xsl:text> </xsl:text>
							<xsl:text>(</xsl:text>
							<xsl:value-of select="origin_rcd"/>
							<xsl:text>)</xsl:text>
							<xsl:text> </xsl:text>
							<xsl:if test="string(departure_time) != '0'">
								<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
							<xsl:value-of select="$cr"/>

							<xsl:text>ARR</xsl:text>
							<xsl:text> </xsl:text>
							<xsl:value-of select="destination_name"/>
							<xsl:text> </xsl:text>
							<xsl:text>(</xsl:text>
							<xsl:value-of select="destination_rcd"/>
							<xsl:text>)</xsl:text>
							<xsl:text> </xsl:text>
							<xsl:if test="string(planned_arrival_time) != '0'">
								<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/></xsl:if>
							<xsl:value-of select="$cr"/>
							
							<xsl:text>Fare Type:</xsl:text>
							<xsl:text> </xsl:text>
								<xsl:for-each select="//Tickets/Ticket[not(passenger_status_rcd='XX')][passenger_id=$passenger_id][booking_segment_id=$booking_segment_id]">
								<xsl:if test="position()=last()">
								<xsl:value-of select="fare_code"/>
								<xsl:text></xsl:text>
								</xsl:if>
						</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>

		<!--End Flights-->

		<xsl:text>CONTACT NAME:</xsl:text>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/Booking/Header/BookingHeader/contact_name"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$crlf"/>
		
		<xsl:text>GUEST DETAILS:</xsl:text>

		<xsl:for-each select="//Passengers/Passenger[count(. | key('passenger_id_group', passenger_id)[1]) = 1]">
			<xsl:value-of select="$crlf"/>
			<xsl:value-of select="concat('0','',string(position()))"/>
			<xsl:text>.</xsl:text>
			<xsl:text> </xsl:text>
			<xsl:variable name="passenger_id" select="passenger_id"/>


			<xsl:for-each select="//Tickets/Ticket[passenger_id=$passenger_id]">
				<xsl:variable name="booking_id" select="booking_id"/>
				<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
				<xsl:if test="position()=1">
					<xsl:value-of select="lastname"/>
					<xsl:text>/</xsl:text>
					<xsl:value-of select="firstname"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="title_rcd"/>
					<xsl:value-of select="$cr"/>

					<xsl:value-of select="airline_rcd"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="flight_number"/>
					<xsl:text> </xsl:text>
					<xsl:text>SEAT</xsl:text>
					<xsl:text> </xsl:text>
					<xsl:if test="seat_number != ''">
						<xsl:value-of select="seat_number"/>
					</xsl:if>
					<xsl:if test="seat_number = ''">
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:text> </xsl:text>
					<xsl:text> </xsl:text>
					<xsl:text>BAGS</xsl:text>
					<xsl:text> </xsl:text>
					<xsl:for-each select="//Fees/Fee[not(passenger_status_rcd='XX')][passenger_id=$passenger_id][booking_segment_id=$booking_segment_id]">

						<xsl:if test="fee_category_rcd = 'BAGSALES'">
							<xsl:value-of select="number_of_units"/>
							<xsl:text> </xsl:text>
						</xsl:if>
						
					</xsl:for-each>

					<xsl:value-of select="$cr"/>
				</xsl:if>
				<xsl:if test="position()!=1">
					<xsl:if test="position()=last()">

						<xsl:value-of select="airline_rcd"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="flight_number"/>
						<xsl:text> </xsl:text>
						<xsl:text>SEAT</xsl:text>
						<xsl:text> </xsl:text>
						<xsl:if test="seat_number != ''">
							<xsl:value-of select="seat_number"/>
						</xsl:if>
						<xsl:if test="seat_number = ''">
							<xsl:text>-</xsl:text>
						</xsl:if>
						<xsl:text> </xsl:text>
						<xsl:text> </xsl:text>
						<xsl:text>BAGS</xsl:text>
 						<xsl:text> </xsl:text>
						<xsl:for-each select="//Fees/Fee[not(passenger_status_rcd='XX')][passenger_id=$passenger_id][booking_segment_id=$booking_segment_id]">

							<xsl:if test="fee_category_rcd = 'BAGSALES'">
								<xsl:value-of select="number_of_units"/>
								<xsl:text> </xsl:text>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>

		<xsl:for-each select="//Passengers/Passenger[count(. | key('passenger_type_group', passenger_type_rcd)[1]) = 1]">
			<xsl:variable name="passenger_type_rcd" select="passenger_type_rcd"/>
			<xsl:for-each select="//TicketQuotes/Total[passenger_type_rcd=$passenger_type_rcd]">

				<xsl:variable name="TotalCharge">
					<xsl:if test="charge_type != 'REFUND'">
						<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
					</xsl:if>
					<xsl:if test="charge_type = 'REFUND'">
						<xsl:value-of select="sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
					</xsl:if>
				</xsl:variable>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>


		<xsl:text>YOUR PAYMENT:</xsl:text>
		<xsl:value-of select="$crlf"/>

		<xsl:text>FARE:</xsl:text>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
		<xsl:text> </xsl:text>
		<xsl:if test="//TicketQuotes/Total/charge_name = 'Fare'">
			<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_name = 'Fare'][charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_name = 'Fare'][charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_name = 'Fare'][charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_name = 'Fare'][charge_type = 'REFUND']/tax_amount)),'#,##0')"/>
		</xsl:if>
		<xsl:value-of select="$cr"/>

		<xsl:text>CHARGES/TAXES:</xsl:text>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
		<xsl:text> </xsl:text>
		<xsl:if test="//TicketQuotes/Total/charge_name = 'Fare'">
			<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_name != 'Fare'][charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_name != 'Fare'][charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_name != 'Fare'][charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_name != 'Fare'][charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0')"/>
		</xsl:if>
		<xsl:value-of select="$cr"/>

		<xsl:text>TOTAL:</xsl:text>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0')"/>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>

		<xsl:text>FORM OF PAYMENT:</xsl:text>
		<xsl:value-of select="$crlf"/>
		<!--Payments IT-->
		<xsl:if test="//Payments/Payment = true()">

			<xsl:variable name="Payment_total" select="sum((//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)) - sum((//Booking/Payments/Payment[string-length(void_by)!=38]/fee_amount))"/>
			<xsl:variable name="Ticket_total" select="(sum(//Booking/TicketQuotes/Total[sort_sequence='0']/total_amount))"/>
			<xsl:variable name="Refund_total" select="(sum(//Booking/TicketQuotes/Total[charge_type='REFUND']/total_amount))"/>
			<xsl:variable name="Fee_total" select="sum(//Booking/Fees/Fee[string-length(void_by)!=38]/fee_amount_incl)"/>
			<xsl:choose>
				<xsl:when test="($Payment_total - $Refund_total) &gt;=  ($Ticket_total + $Fee_total)">
					<xsl:if test="(position()&gt;1)=true() ">
					</xsl:if>
					<xsl:for-each select="Booking/Payments/Payment[void_date_time='']">
						<xsl:choose>
							<xsl:when test="form_of_payment_subtype !=''">
								<xsl:value-of select="form_of_payment_subtype"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="form_of_payment"/>
							</xsl:otherwise>
						</xsl:choose><xsl:value-of select="$cr"/>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="(position()&gt;1)=true() ">
					</xsl:if>
					<xsl:for-each select="Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)]">

						<xsl:choose>
							<xsl:when test="form_of_payment_subtype !=''">
								<xsl:value-of select="form_of_payment_subtype"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="form_of_payment"/>
							</xsl:otherwise>
						</xsl:choose><xsl:value-of select="$cr"/>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<!--End Payments IT-->
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>



		<xsl:text>ALL GUESTS SHOULD RETAIN</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>A COPY FOR THEIR RECORDS.</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$crlf"/>

		<xsl:text>THERE MAY BE CHARGES FOR</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>CHANGING OR CANCELLING</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>YOUR RESERVATION.</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$crlf"/>

		<xsl:text>DETAILS AVAILABLE AT</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>FLYPEACH.COM</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$crlf"/>

		<xsl:text>PLEASE READ THE PEACH AVIATION</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>CONDITIONS OF CARRIAGE.</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>TERMS AND CONDITIONS OF</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>CARRIAGE ARE AVAILABLE AT</xsl:text><xsl:text> </xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>WWW.FLYPEACH.COM</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$crlf"/>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\itinerary\LVMCP5_xx.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no">
			<SourceSchema srcSchemaPath="..\_CASE\LVLJG2_intinerary_DE.xml" srcSchemaRoot="Booking" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/>
		</MapperInfo>
		<MapperBlockPosition>
			<template name="CDate2Julian"></template>
			<template match="/">
				<block path="HTML/HEAD/TITLE/xsl:value-of" x="232" y="54"/>
				<block path="HTML/BODY/DIV/table/tr/td/table/tr/td/xsl:call-template" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/DIV/TABLE[1]/xsl:for-each" x="192" y="114"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each" x="152" y="114"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[4]/xsl:if/&gt;[0]" x="36" y="162"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[4]/xsl:if/&gt;[0]/string-length[0]" x="0" y="156"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[4]/xsl:if" x="82" y="164"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[4]/xsl:if/xsl:call-template" x="272" y="194"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[4]/xsl:if[1]/=[0]" x="0" y="162"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[4]/xsl:if[1]/=[0]/string-length[0]" x="0" y="156"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[4]/xsl:if[1]" x="42" y="164"/>
				<block path="" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if/!=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if/!=[0]/string[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if/xsl:value-of/format-number[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if/xsl:value-of/format-number[0]/number[0]" x="140" y="102"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if/xsl:value-of[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if/xsl:value-of[1]/format-number[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[5]/xsl:if/xsl:value-of[1]/format-number[0]/number[0]" x="140" y="102"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if/!=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if/!=[0]/string[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if/xsl:value-of/format-number[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if/xsl:value-of/format-number[0]/number[0]" x="140" y="102"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if/xsl:value-of[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if/xsl:value-of[1]/format-number[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/DIV[1]/TABLE[1]/xsl:for-each/TR/TD[6]/xsl:if/xsl:value-of[1]/format-number[0]/number[0]" x="140" y="102"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each" x="102" y="24"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each" x="172" y="54"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if" x="42" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if" x="112" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[1]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[2]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[3]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[4]/xsl:if/!=[0]" x="166" y="12"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[4]/xsl:if/!=[0]/number[0]" x="120" y="6"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[4]/xsl:if" x="212" y="14"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[4]/xsl:if/xsl:if/&gt;=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[4]/xsl:if/xsl:if/&gt;=[0]/number[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[4]/xsl:if/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[4]/xsl:if/xsl:if/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[5]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if/tr/td[5]/xsl:value-of[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]" x="272" y="74"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[1]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[2]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[3]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[4]/xsl:if/!=[0]" x="126" y="52"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[4]/xsl:if/!=[0]/number[0]" x="80" y="46"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[4]/xsl:if" x="172" y="54"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[4]/xsl:if/xsl:if/&gt;=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[4]/xsl:if/xsl:if/&gt;=[0]/number[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[4]/xsl:if/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[4]/xsl:if/xsl:if/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[5]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if/xsl:if[1]/tr/td[5]/xsl:value-of[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]" x="172" y="14"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if" x="42" y="44"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/!=[0]" x="66" y="72"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if" x="112" y="74"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[2]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[3]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[4]/xsl:if/!=[0]" x="126" y="52"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[4]/xsl:if/!=[0]/number[0]" x="80" y="46"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[4]/xsl:if" x="172" y="54"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[4]/xsl:if/xsl:if/&gt;=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[4]/xsl:if/xsl:if/&gt;=[0]/number[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[4]/xsl:if/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if/tr/td[4]/xsl:if/xsl:if/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if[1]/=[0]" x="226" y="32"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if[1]" x="272" y="34"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if[1]/tr/td[2]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if[1]/tr/td[5]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if/xsl:if[1]/tr/td[5]/xsl:value-of[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]" x="42" y="124"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/!=[0]" x="186" y="152"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if" x="232" y="154"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[2]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[3]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[4]/xsl:if/!=[0]" x="126" y="52"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[4]/xsl:if/!=[0]/number[0]" x="80" y="46"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[4]/xsl:if" x="172" y="54"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[4]/xsl:if/xsl:if/&gt;=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[4]/xsl:if/xsl:if/&gt;=[0]/number[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[4]/xsl:if/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if/tr/td[4]/xsl:if/xsl:if/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if[1]/=[0]" x="226" y="152"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if[1]" x="272" y="154"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if[1]/tr/td[2]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if[1]/tr/td[5]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each/xsl:for-each/xsl:if[1]/xsl:if[1]/xsl:if[1]/tr/td[5]/xsl:value-of[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]" x="122" y="164"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if" x="192" y="154"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if/tr/td[3]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if/tr/td[4]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if/tr/td[4]/xsl:choose/=[0]" x="156" y="78"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if/tr/td[4]/xsl:choose/=[0]/format-number[0]" x="110" y="72"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if/tr/td[4]/xsl:choose/xsl:otherwise/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if/tr/td[5]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if/tr/td[5]/xsl:value-of/sum[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if[1]" x="232" y="194"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if[1]/tr/td[3]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if[1]/tr/td[4]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if[1]/tr/td[4]/xsl:choose/=[0]" x="156" y="78"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if[1]/tr/td[4]/xsl:choose/=[0]/format-number[0]" x="110" y="72"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/xsl:for-each[1]/xsl:if[1]/tr/td[4]/xsl:choose/xsl:otherwise/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if/DIV/TABLE[1]/tr/td[5]/SPAN/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]" x="272" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/&gt;=[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr/xsl:if/starts-with[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr/xsl:if/starts-with[0]/string[0]" x="140" y="106"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr/xsl:if/TD[1]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr/xsl:if/TD[1]/xsl:value-of/substring[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr/xsl:if/TD[1]/xsl:value-of/substring[0]/string-length[2]" x="140" y="138"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr/xsl:if[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr/xsl:if[1]/TD/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/!=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD/xsl:choose" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD/xsl:choose/!=[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose/!=[0]" x="156" y="78"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose/xsl:otherwise/xsl:if/!=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose/xsl:otherwise/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose/xsl:otherwise/xsl:if/xsl:call-template" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[3]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of/substring[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of[2]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[4]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[4]/xsl:choose/&gt;[0]" x="156" y="78"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[4]/xsl:choose/xsl:when/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/xsl:for-each/xsl:if/TR/TD[4]/xsl:choose/xsl:when/xsl:value-of[2]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/TD/xsl:choose" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/TD/xsl:choose/&lt;[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/TD/xsl:choose/&lt;[0]/number[0]" x="140" y="102"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if/=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if/=[0]/number[0]" x="140" y="106"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/!=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/!=[0]/number[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if/&gt;=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if/&gt;=[0]/number[0]" x="140" y="106"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if/TD[1]/SPAN/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if[1]/&lt;[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if[1]/&lt;[0]/number[0]" x="140" y="106"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if[1]/TD/SPAN/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if[1]/TD/SPAN/xsl:value-of/substring[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:when/tr[1]/xsl:if[1]/xsl:if[1]/TD/SPAN/xsl:value-of/substring[0]/string-length[2]" x="140" y="138"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR/xsl:if/starts-with[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR/xsl:if/starts-with[0]/string[0]" x="140" y="106"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR/xsl:if/TD[1]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR/xsl:if/TD[1]/xsl:value-of/substring[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR/xsl:if/TD[1]/xsl:value-of/substring[0]/string-length[2]" x="140" y="138"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR/xsl:if[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR/xsl:if[1]/TD/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/!=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD/xsl:choose" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD/xsl:choose/!=[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose/!=[0]" x="156" y="78"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose/xsl:otherwise/xsl:if/!=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose/xsl:otherwise/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[2]/xsl:choose/xsl:otherwise/xsl:if/xsl:call-template" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[3]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of/substring[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of[2]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[4]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[4]/xsl:choose/&gt;[0]" x="156" y="78"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[4]/xsl:choose/xsl:when/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if/TR/TD[4]/xsl:choose/xsl:when/xsl:value-of[2]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if/=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[1]/=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[2]/=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[2]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[3]/=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[3]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[4]/=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[4]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[5]/=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[5]" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[5]/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[5]/xsl:value-of/substring[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[1]/xsl:if[5]/xsl:value-of/substring[2]" x="186" y="144"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[2]/xsl:if/!=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[2]/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[2]/xsl:if/xsl:call-template" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[3]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of/substring[0]" x="186" y="108"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[3]/xsl:choose/xsl:when/xsl:value-of[2]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[4]/xsl:choose" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[4]/xsl:choose/&gt;[0]" x="156" y="78"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[4]/xsl:choose/xsl:when/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/xsl:for-each/xsl:if[1]/TR/TD[4]/xsl:choose/xsl:when/xsl:value-of[2]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if/=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if/=[0]/number[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/!=[0]" x="156" y="82"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/!=[0]/number[0]" x="110" y="76"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]" x="202" y="84"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/xsl:if/&gt;=[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/xsl:if/&gt;=[0]/number[0]" x="140" y="106"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/xsl:if" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/xsl:if/TD[1]/SPAN/xsl:value-of" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/xsl:if[1]/&lt;[0]" x="186" y="112"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/xsl:if[1]/&lt;[0]/number[0]" x="140" y="106"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/xsl:if[1]" x="232" y="114"/>
				<block path="HTML/BODY/DIV/DIV/xsl:if[1]/div/table/tbody/xsl:choose/xsl:otherwise/TR[1]/xsl:if[1]/xsl:if[1]/TD/SPAN/xsl:value-of" x="232" y="114"/>
			</template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->