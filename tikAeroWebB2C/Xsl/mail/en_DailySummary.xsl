<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes" exclude-result-prefixes="dt ms">
<xsl:output method="html"/>
	<xsl:variable name="PageTitle">tikAERO DailySummary</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable> 
	<xsl:variable name="ColorSpan">Red</xsl:variable>
	<xsl:variable name="booking_count" select="DailySummaries/BookingCount/BookingCount/booking_count"/>
	<xsl:variable name="passenger_count" select="DailySummaries/PassengerCount/PassengerCount/passenger_count"/>
	
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($day, '/', $month, '/', $year)"/>
	</xsl:template>
	
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:value-of select="concat($day, $month, $year)"/>
	</xsl:template>
 
	<xsl:template match="/">
	<html xmlns:dt="urn:schemas-microsoft-com:datatypes" xmlns:ms="urn:schemas-microsoft-com:xslt"><HEAD>
			<TITLE><xsl:value-of select="$PageTitle"/></TITLE>
			<STYLE>
				.PanelDailySummary .BookingNumber{
					color: #C4102F;
				}

				.PanelDailySummary .PanelHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #C4102F;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 5px 0px 5px 5px;
						width: 100%;
					}
				.PanelDailySummary .PanelHeader IMG{
					vertical-align: middle;
				}

				.PanelDailySummary .PanelHeader SPAN{
					padding-left: 4;
				}

				.PanelDailySummary .PanelContainer{
					padding: 10 0 10 20;
					width: 100%;
					display: block;
				}
				.PanelDailySummary .GridHeader
					{
						background:#fdb813;
						color:  #003964;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						padding: 2px 2px 2px 5px;
					}
				.PanelDailySummary TR.GridItems
					{
						 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}

				.PanelDailySummary .GridItems TD
					{
						 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}

				.PanelDailySummary .GridFooter TD SPAN
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					height: 18px;
					padding: 4 5 0 4;
				}

				.PanelDailySummary .FooterTotal
				{
					border-bottom: solid 1px #dddddd;
				}

				.PanelDailySummary SPAN.FooterTotalLabel
					{
							background-color: transparent;
					 		color: #666666;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							height: 22px;
							padding: 1px 2px 1px 5px;
					}

				.PanelDailySummary SPAN.FooterTotalValue
					{
						color: #000066;
						font-size: 11px;
					}
				.PanelDailySummary .BarcodeHeader
				{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 10px;
					font-weight: bold;
					color: #00529d;
					padding: 5 0 3 5;
					margin: 0 0 0 0;
					width: 100%;
					cursor: hand;
				}

				.PanelDailySummary .Barcode
				{
					padding: 1 5 1 5;
				}

				.WelcomeText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				.CommentText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
			
			</STYLE>
		</HEAD>
		<body>
			<div class="PanelDailySummary">
				<img src="{$BaseURL}ISK_logo.jpg"/>
				<p>&#160;</p>
				<div class="PanelHeader" style="font-size: 12px">
					<span>BOOKING SUMMARY&#160;</span>
				</div>
				<div class="PanelContainer">
					<table class="Table" cellSpacing="0" rules="rows" border="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 50%; border-collapse: collapse; border-right-width: 0px">
						<tbody>
							<tr class="GridHeader">
								<td align="left">Summary</td>
								<td align="right">Count</td>
							</tr>
							<tr class="GridItems">
								<xsl:for-each select="DailySummaries/BookingCount/BookingCount">
									<td align="left">Total new Bookings</td>
									<td align="right">
										<xsl:value-of select="format-number(booking_count,'##,###')"/>
									</td>
								</xsl:for-each>
							</tr>
							<tr class="GridItems">
								<xsl:for-each select="DailySummaries/PassengerCount/PassengerCount">
									<td align="left">Total new Passengers</td>
									<td align="right">
										<xsl:value-of select="format-number(passenger_count,'##,###')"/>
									</td>
								</xsl:for-each>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="PanelHeader" style="font-size: 12px">
					<span>FLIGHT SUMMARY BOOKED&#160;(<xsl:value-of select="count(DailySummaries/BookedSummary/Flight)"/>)
					</span>
				</div>
				<div class="PanelContainer">
					<table class="Table" cellSpacing="0" rules="rows" border="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
						<tbody>
							<tr class="GridHeader">
								<td align="left" width="20"/>
								<td align="left" width="60">Flight</td>
								<td align="left" width="70">From</td>
								<td align="left" width="70">To</td>
								<td align="right" width="50">Pax</td>
								<td align="right" width="140">Revenue Net</td>
								<td align="right" width="140">Fare Amount</td>
							</tr>
							<xsl:for-each select="DailySummaries/BookedSummary/Flight">
								<xsl:sort select="origin_rcd"/>
								 
								<tr class="GridItems">
									<td align="left" width="20">
										<xsl:number value="position()" format="001 "/>
									</td>
									<td align="left" width="60">
										<span>
											<xsl:value-of select="airline_rcd"/>&#160; 
											<xsl:value-of select="flight_number"/>
										</span>
									</td>
									<td align="left" width="70">
										<span>
											<xsl:value-of select="origin_rcd"/>
										</span>
									</td>
									<td align="left" width="70">
										<span>
											<xsl:value-of select="destination_rcd"/>
										</span>
									</td>
									<td align="right" width="50">
									<xsl:if test="segment_booked_seats >'0'">
										<span>
											<xsl:value-of select="segment_booked_seats"/>
										</span>
										</xsl:if>
									</td>
									<td align="right" width="140">
										<xsl:variable name="sumRevenue" select="format-number((acct_fare_amount + acct_yq_amount + acct_ticketing_fee_amount + acct_reservation_fee_amount),'#,##0.00')"/>
											<xsl:if test="format-number((acct_fare_amount + acct_yq_amount + acct_ticketing_fee_amount + acct_reservation_fee_amount),'#,##0.00') > '0'">
											 		<span><xsl:value-of select="format-number((acct_fare_amount + acct_yq_amount + acct_ticketing_fee_amount + acct_reservation_fee_amount),'#,##0.00')"/></span>
										  </xsl:if>
											 
	 									 
									</td>
									<td align="right" width="140">
										<xsl:if test="string(acct_fare_amount)> '0'">
											<span>
												<xsl:value-of select="format-number(acct_fare_amount,'#,##0.00')"/>&#160;
											</span>
										</xsl:if>
									</td>
								</tr>
							</xsl:for-each>
						 
							<tr class="GridItems">
								<td align="left" width="20" style="font-weight: bold;">Total </td>
								<td align="left" width="60" style="font-weight: bold;">
									<span>&#160;</span>
								</td>
								<td align="left" width="70" style="font-weight: bold;">
									<span>&#160;</span>
								</td>
								<td align="left" width="70" style="font-weight: bold;">
									<span>&#160;</span>
								</td>
								<td align="right" width="50" style="font-weight: bold;">
									<xsl:if test="sum(DailySummaries/BookedSummary/Flight/segment_booked_seats) >'0'">
									<span>
										<xsl:value-of select="format-number(sum(DailySummaries/BookedSummary/Flight/segment_booked_seats),'##,###')"/>
									</span>
									</xsl:if>
								</td>
								<td align="right" width="140" style="font-weight: bold;">
									<xsl:if test="sum(DailySummaries/BookedSummary/Flight/acct_fare_amount) + sum(DailySummaries/BookedSummary/Flight/acct_yq_amount) + sum(DailySummaries/BookedSummary/Flight/acct_ticketing_fee_amount) + sum(DailySummaries/BookedSummary/Flight/acct_reservation_fee_amount) > '0'">
										<span><xsl:value-of select="format-number(sum(DailySummaries/BookedSummary/Flight/acct_fare_amount) + sum(DailySummaries/BookedSummary/Flight/acct_yq_amount) + sum(DailySummaries/BookedSummary/Flight/acct_ticketing_fee_amount) + sum(DailySummaries/BookedSummary/Flight/acct_reservation_fee_amount),'#,##0.00')"/></span>
									</xsl:if>
								</td>
								<td align="right" width="140" style="font-weight: bold;">
									<xsl:if test="(DailySummaries/BookedSummary/Flight/acct_fare_amount)> '0'">
									 
											<span><xsl:value-of select="format-number(sum(DailySummaries/BookedSummary/Flight/acct_fare_amount),'#,##0.00')"/></span>
										</xsl:if>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="PanelHeader" style="font-size: 12px">
					<span>FLIGHT SUMMARY BOARDED&#160;(<xsl:value-of select="count(DailySummaries/BookedSummary/Flight)"/>)
					</span>
				</div>
				<div class="PanelContainer">
					<table class="Table" cellSpacing="0" rules="rows" border="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
						<tbody>
							<tr class="GridHeader">
								<td align="left" width="10"/>
								<td align="left" width="50">Flight</td>
								<td align="left" width="20">From</td>
								<td align="left" width="20">To</td>
								<td align="right" width="90">Physical</td>
								<td align="right" width="105">Bookable</td>
								<td align="right" width="80">Boarded</td>
								<td align="right" width="50">Load</td>
								<td align="right" width="75">Revenue Net</td>
								<td align="right" width="75">Fare</td>
							</tr>

							<xsl:for-each select="DailySummaries/BookedSummary/Flight">
								<xsl:sort select="origin_rcd"/>
								 
								<tr class="GridItems">
									<td align="left" width="10">
										<xsl:number value="position()" format="001 "/>
									</td>
									<td align="left" width="50">
										<span>
											<xsl:value-of select="airline_rcd"/>&#160; 
											<xsl:value-of select="flight_number"/>
										</span>
									</td>
									<td align="left" width="20">
										<span>
											<xsl:value-of select="origin_rcd"/>
										</span>
									</td>
									<td align="left" width="20">
										<span>
											<xsl:value-of select="destination_rcd"/>
										</span>
									</td>
									<td align="right" width="90">
										<xsl:if test="string(physical_capacity) > '0'">
										
										<span>

											<xsl:value-of select="format-number(physical_capacity,'##,###')"/>
										</span>
									</xsl:if>
									</td>
									<td align="right" width="105">
									<xsl:if test="string(bookable_capacity) > '0'">
										
										<span>
											<xsl:value-of select="format-number(bookable_capacity,'##,###')"/>&#160;
										</span>
										</xsl:if>
									</td>
									<td align="right" width="80">
										<span>
										<xsl:if test="string(total_pax_booked) > '0'">
											<xsl:value-of select="format-number(total_pax_booked,'##,###')"/>
										</xsl:if>
										</span>
									 
										</td><td align="right" width="50">
										 
													<xsl:if test="string(total_pax_booked)>'0'">
						
													<span><xsl:value-of select="format-number(total_pax_booked div bookable_capacity * 100,'0.0') "/>&#160;%</span>
						 					</xsl:if>
										 
										</td>

									<td align="right" width="75">
										 
											<xsl:variable name="sumRevenue" select="format-number(acct_fare_amount + acct_yq_amount + acct_ticketing_fee_amount + acct_reservation_fee_amount,'#,##0.00')"/>
											<xsl:if test="$sumRevenue > '0'">
											 		<span><xsl:value-of select="$sumRevenue"/></span>
										  </xsl:if>
											 
									</td>
									 
									<td align="right" width="75">
										<xsl:if test="string(acct_fare_amount) > '0'">
											<span><xsl:value-of select="format-number(acct_fare_amount,'#,##0.00')"/></span>
										</xsl:if>
									</td>
								</tr>
							</xsl:for-each>
						 
								<tr class="GridItems">
									<td align="left" width="10" style="font-weight: bold;">
										<span>Total</span>
									</td>
									<td align="left" width="50" style="font-weight: bold;">
										<span>&#160;</span>
									</td>
									<td align="left" width="20" style="font-weight: bold;">
										<span>&#160;</span>
									</td>
									<td align="left" width="20" style="font-weight: bold;">
										<span>&#160;</span>
									</td>
									<td align="right" width="90" style="font-weight: bold;">
										<xsl:if test="sum(DailySummaries/BookedSummary/Flight/physical_capacity) >'0' ">
										<span>
											<xsl:value-of select="format-number(sum(DailySummaries/BookedSummary/Flight/physical_capacity),'##,###')"/>
										</span>
										</xsl:if>
									</td>
									<td align="right" width="105" style="font-weight: bold;" >
										<xsl:if test="sum(DailySummaries/BookedSummary/Flight/bookable_capacity) >'0'">
										<span>
											<xsl:value-of select="format-number(sum(DailySummaries/BookedSummary/Flight/bookable_capacity),'###,###')"/>&#160;
										</span>
										</xsl:if>
									</td>
									<td align="right" width="80" style="font-weight: bold;">
										<xsl:if test="sum(DailySummaries/BookedSummary/Flight/segment_booked_seats) >'0'">
										<span>
											<xsl:value-of select="format-number(sum(DailySummaries/BookedSummary/Flight/total_pax_booked),'###,###')"/>
										</span>
										</xsl:if>
									</td>
									<td align="right" width="50" style="font-weight: bold;">
										<span>&#160; </span>
						 		    </td>
									<td align="right" width="50" style="font-weight: bold;">
										  <xsl:if test="sum(DailySummaries/BookedSummary/Flight/acct_fare_amount) + sum(DailySummaries/BookedSummary/Flight/acct_yq_amount) + sum(DailySummaries/BookedSummary/Flight/acct_ticketing_fee_amount) + sum(DailySummaries/BookedSummary/Flight/acct_reservation_fee_amount) > '0'">
											 		<span><xsl:value-of select="format-number(sum(DailySummaries/BookedSummary/Flight/acct_fare_amount) + sum(DailySummaries/BookedSummary/Flight/acct_yq_amount) + sum(DailySummaries/BookedSummary/Flight/acct_ticketing_fee_amount) + sum(DailySummaries/BookedSummary/Flight/acct_reservation_fee_amount),'#,##0.00')"/></span>
										  </xsl:if>
									</td>
									<td align="right" width="75" style="font-weight: bold;">
												<xsl:if test="DailySummaries/BookedSummary/Flight/acct_fare_amount>'0'">
												 	<span><xsl:value-of select="format-number(sum(DailySummaries/BookedSummary/Flight/acct_fare_amount),'#,##0.00')"/></span>
												</xsl:if>
									</td>
								</tr>
						 
						</tbody>
					</table>

				</div>
				<div class="PanelHeader" style="font-size: 12px">
					<span>SALES REPORT (tickets issued)&#160;(<xsl:value-of select="count(DailySummaries/CounterSales/Item)"/>)
					</span>
				</div>
				<div class="PanelContainer">
					<table class="Table" cellSpacing="0" rules="rows" border="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
						<tbody>
							<tr class="GridHeader">
								<td align="left" width="20"/>
								<td align="left" width="89">PaxType</td>
								<td align="left" width="20">From</td>
								<td align="left" width="20">To</td>
								<td align="right" width="59">Pax</td>
								<td align="left" width="80">Currency</td>
								<td align="right" width="89">Ticket Amount</td>
								<td align="right" width="130">Accounting Amount</td>
								<td align="left" width="130">Agency</td>
							</tr>
							<xsl:for-each select="DailySummaries/CounterSales/Item">
								<xsl:sort select="origin_rcd"/>
								<xsl:sort select="destination_rcd"/>
								<tr class="GridItems">
									<td align="left" width="20">
										<xsl:number value="position()" format="001 "/>
									</td>
									<td align="left" width="89">
										<span>
											<xsl:value-of select="passenger_type_rcd"/>
										</span>
									</td>
									<td align="left" width="20">
										<span>
											<xsl:value-of select="origin_rcd"/>
										</span>
									</td>
									<td align="left" width="20">
										<span>
											<xsl:value-of select="destination_rcd"/>
										</span>
									</td>
									<td align="right" width="55">
									<xsl:if test="ticket_count >'0'">
										<span>
											<xsl:value-of select="format-number(ticket_count,'##,###')"/>
										</span>
										</xsl:if>
									</td>
									<td align="left" width="80">
										<span>
											<xsl:value-of select="currency_rcd"/>
										</span>
									</td>
									<td align="right" width="89">
										 
											<xsl:if test="string(net_total)>'0'">
												 
												<span>
													<xsl:value-of select="format-number(net_total,'#,##0.00')"/>&#160;
												</span>
											</xsl:if>
										 
									</td>
									<td align="right" width="130">
									 
											<xsl:if test="string(accounting_net_total) > '0'">
											 
												<span>
													<xsl:value-of select="format-number(accounting_net_total,'#,##0.00')"/>&#160;
												</span>
											</xsl:if>
										 
									</td>
									<td align="left" width="130">
									 		<span>
												<xsl:value-of select="agency_code"/>&#160;
											</span>
								 	</td>
								</tr>
							</xsl:for-each>
						 
								<tr class="GridItems">
									<td align="left" width="20" style="font-weight: bold;">
										<span>Total</span>
									 									 
									</td>
									<td align="left" width="89" style="font-weight: bold;">
										<span>
											&#160;
										</span>
									</td>
									<td align="left" width="20" style="font-weight: bold;">
										<span>
											&#160;
										</span>
									</td>
									<td align="left" width="20" style="font-weight: bold;">
										<span>
											&#160;
										</span>
									</td>
									<td align="right" width="55" style="font-weight: bold;">
									
									<xsl:if test="sum(DailySummaries/CounterSales/Item/ticket_count) >'0' ">

										<span>
											<xsl:value-of select="format-number(sum(DailySummaries/CounterSales/Item/ticket_count),'##,###')"/>
										</span>
										</xsl:if>
									</td>
									<td align="left" width="80" style="font-weight: bold;">
										<span>
											 &#160;
										</span>
									</td>
									<td align="right" width="89" style="font-weight: bold;">
										 
											<xsl:if test="sum(DailySummaries/CounterSales/Item/net_total) >'0'">
												 
													<xsl:value-of select="format-number(sum(DailySummaries/CounterSales/Item/net_total),'#,##0.00')"/>&#160;
												 
											</xsl:if>
										 
									</td>
									<td align="right" width="130" style="font-weight: bold;">
										 
											<xsl:if test="sum(DailySummaries/CounterSales/Item/accounting_net_total) > '0'">
												 <span>
													<xsl:value-of select="format-number(sum(DailySummaries/CounterSales/Item/accounting_net_total),'#,##0.00')"/>&#160;
												</span>
											</xsl:if>
									</td>
									<td align="left" width="130" style="font-weight: bold;">
										<span>
											 &#160;
										</span>
									</td>
								</tr>
							 
						</tbody>
					</table>
				</div>
				<div class="PanelHeader" style="font-size: 12px">
					<span>CASHBOOK (payment summary)&#160;(<xsl:value-of select="count(DailySummaries/Cashbook/Payment)"/>)
					</span>
				</div>
				<div class="PanelContainer">
					<table class="Table" cellSpacing="0" rules="rows" border="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
						<tbody>
							<tr class="GridHeader">
								<td align="left" width="20"/>
								<td align="left" width="115">Payment Type</td>
								<td align="left" width="115">Currency</td>
								<td align="right" width="115">Payment Amount</td>
								<td align="right" width="115">Accounting Amount</td>
								<td align="right" width="115">Agency</td>
							</tr>
							<xsl:for-each select="DailySummaries/Cashbook/Payment">
								<tr class="GridItems">
									<td align="left" width="20">
										<xsl:number value="position()" format="001 "/>
									</td>
									<td align="left" width="115">
										<span>
											<xsl:value-of select="form_of_payment_rcd"/>
										</span>
									</td>
									<td align="left" width="115">
										<span>
											<xsl:value-of select="currency_rcd"/>
										</span>
									</td>
									<td align="right" width="115">
										<xsl:if test="string(payment_amount) > '0'">
											<span>
													<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#160;
												</span>
										</xsl:if>
									</td>
									<td align="right" width="115">
										 
											<xsl:if test="string(accounting_payment_amount) > '0'">
												 
												<span><xsl:value-of select="format-number(accounting_payment_amount,'#,##0.00')"/></span>
											
										</xsl:if>
 
									</td>
									<td align="left" width="115">
										 <span><xsl:value-of select="agency_code"/></span>
									</td>
								</tr>
							</xsl:for-each>
								<tr class="GridItems">
									<td align="left" width="20" style="font-weight: bold;">
										<span>Total</span>
									</td>
									<td align="left" width="115" style="font-weight: bold;">
										<span>&#160;</span>
									</td>
									<td align="left" width="115" style="font-weight: bold;">
										<span>
											&#160;
										</span>
									</td>
									<td align="right" width="115" style="font-weight: bold;">
										<xsl:if test="sum(DailySummaries/Cashbook/Payment/payment_amount) > '0'">
											<span>
													<xsl:value-of select="format-number(sum(DailySummaries/Cashbook/Payment/payment_amount),'#,##0.00')"/>&#160;
												</span>
										</xsl:if>
									</td>
									<td align="right" width="115" style="font-weight: bold;">
										 
											<xsl:if test="string(DailySummaries/Cashbook/Payment/accounting_payment_amount) > '0'">
												 
												<span><xsl:value-of select="format-number(sum(DailySummaries/Cashbook/Payment/accounting_payment_amount),'#,##0.00')"/></span>
											
										</xsl:if>
 
									</td>
									<td align="right" width="115" style="font-weight: bold;">
										 <span>&#160;</span>
									</td>
								</tr>
						</tbody>
					</table>
				</div>
				<div class="PanelHeader" style="font-size: 12px">
					<span>FLIGHT TRACKING&#160;(<xsl:value-of select="count(DailySummaries/FlightOperations/Flight)"/>)
					</span>
				</div>
			 <div class="PanelContainer">
					<table class="Table" cellSpacing="0" rules="rows" border="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
						<tbody>
							<tr class="GridHeader">
								<td align="left" width="10"/>
								<td align="left" width="45">Flight</td>
								<td align="left" width="20">From</td>
								<td align="left" width="20">To</td>
								<td align="left" width="40">STD</td>
								<td align="left" width="40">STA</td>
								<td align="right" width="40">ATA</td>
								<td align="left" width="45">Aircraft</td>
								<td align="right" width="30">Irregularity</td>
								<td align="left" width="65">Description</td>
								<td align="left" width="45">Status</td>
								<td align="left" width="70">Matriculation</td>
							</tr>
							<xsl:for-each select="DailySummaries/FlightOperations/Flight">
								<xsl:sort select="origin_rcd"/>
								<xsl:sort select="destination_rcd"/>
								<tr class="GridItems">
									<td align="left" width="10">
										<xsl:number value="position()" format="001 "/>
									</td>
									<td align="left" width="45">
										<span>
											<xsl:value-of select="airline_rcd"/>&#160; 
											<xsl:value-of select="flight_number"/>
										</span>
									</td>
									<td align="left" width="20">
										<span>
											<xsl:value-of select="origin_rcd"/>
										</span>
									</td>
									<td align="left" width="20">
										<span>
											<xsl:value-of select="destination_rcd"/>
										</span>
									</td>
									<td align="left" width="40">
										<xsl:if test="string(planned_departure_time) > '0'">
											<xsl:value-of select="substring(format-number(number(planned_departure_time), '0000'),1,2)"/>:
											<xsl:value-of select="substring(format-number(number(planned_departure_time), '0000'),3,4)"/>
										</xsl:if>
									</td>
									<td align="left" width="40">
										<xsl:if test="string(planned_arrival_time)> '0'">
											<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:
											<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/>
										</xsl:if>
									</td>
									<td align="left" width="40">
										<xsl:if test="string(actual_departure_time) > ''">
											<xsl:value-of select="substring(format-number(number(actual_departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(actual_departure_time), '0000'),3,4)"/>	
										</xsl:if>
									</td>
									<td align="left" width="45">
										<span>
											<xsl:value-of select="aircraft_type_rcd"/>
										</span>
									</td>
									<td align="right" width="30">
										<span>
											<xsl:value-of select="irregularity_rcd"/>
										</span>
									</td>
									<td align="left" width="65">
										<span>
											<xsl:value-of select="display_name"/> 
										</span>
									 
										<span>
											<xsl:value-of select="irregularity_comment"/> 
										</span>
										 
									</td>
									<td align="left" width="45">
										<span>
											<xsl:value-of select="flight_check_in_status_rcd"/>
										</span>
									</td>
									<td align="left" width="70">
										<span>
											<xsl:value-of select="matriculation_rcd"/>
										</span>
									</td>
								</tr>
							</xsl:for-each>
					 	</tbody>
					</table>
				</div>
			 </div>
		</body>
	</html>
</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\DAILYSUMMARY_Sep04_1831.XML" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->