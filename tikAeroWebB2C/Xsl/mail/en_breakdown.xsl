<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
  <xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/PMT</xsl:variable> 
  <xsl:variable name="Titel">TikAero Itinerary</xsl:variable>
  <xsl:key name="passenger_type_rcd_group" match="//Passengers/Passenger" use="passenger_type_rcd" />
  <xsl:variable name="passenger_id" select="Booking/Tickets/Ticket/passenger_id"/>
  <xsl:variable name="booking_segment_id_tax" select="Booking/Tickets/Ticket[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
  <xsl:variable name="booking_segment_id" select="Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
  <xsl:variable name="sum_net_total" select="sum(Booking/Tickets/Ticket/net_total)"/>
  <xsl:variable name="sum_payment_amount" select="sum(Booking/Payments/Payment/payment_amount)"/>

  <xsl:template name="format-date">
    <xsl:param name="date" />
    <xsl:param name="format" select="0" />
    <xsl:variable name="day" select="substring($date, 7,2)" />
    <xsl:variable name="month" select="substring($date,5,2)" />
    <xsl:variable name="year" select="substring($date,1,4)" />
    <xsl:value-of select="concat($month, $day, $year)" />
  </xsl:template>

  <xsl:template  name="BarcodeInterleaved2of5">
    <xsl:param name="ticket_number"/>
    <xsl:variable name="IfValid">
      <xsl:call-template name="Interleaved2ofValid">
        <xsl:with-param name="ticket_number" select="ticket_number"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="string-length($IfValid) = 0">
      <xsl:variable name="Number">
        <xsl:call-template name="CheckNumber">
          <xsl:with-param name="ticket_number" select="ticket_number"/>
        </xsl:call-template>
      </xsl:variable>
      <SPAN class="Barcode">
        <SPAN class="11"></SPAN>
        <SPAN class="11"></SPAN>
        <xsl:call-template name="Interleaved2of5Render">
          <xsl:with-param name="Char1" select="substring($Number,1,1)"/>
          <xsl:with-param name="Char2" select="substring($Number,2,1)"/>
        </xsl:call-template>
        <xsl:call-template name="Interleaved2of5Render">
          <xsl:with-param name="Char1" select="substring($Number,3,1)"/>
          <xsl:with-param name="Char2" select="substring($Number,4,1)"/>
        </xsl:call-template>
        <xsl:call-template name="Interleaved2of5Render">
          <xsl:with-param name="Char1" select="substring($Number,5,1)"/>
          <xsl:with-param name="Char2" select="substring($Number,6,1)"/>
        </xsl:call-template>
        <xsl:call-template name="Interleaved2of5Render">
          <xsl:with-param name="Char1" select="substring($Number,7,1)"/>
          <xsl:with-param name="Char2" select="substring($Number,8,1)"/>
        </xsl:call-template>
        <xsl:call-template name="Interleaved2of5Render">
          <xsl:with-param name="Char1" select="substring($Number,9,1)"/>
          <xsl:with-param name="Char2" select="substring($Number,10,1)"/>
        </xsl:call-template>
        <xsl:call-template name="Interleaved2of5Render">
          <xsl:with-param name="Char1" select="substring($Number,11,1)"/>
          <xsl:with-param name="Char2" select="substring($Number,12,1)"/>
        </xsl:call-template>
        <xsl:call-template name="Interleaved2of5Render">
          <xsl:with-param name="Char1" select="substring($Number,13,1)"/>
          <xsl:with-param name="Char2" select="substring($Number,14,1)"/>
        </xsl:call-template>
        <SPAN class="21"></SPAN>
        <SPAN class="1"></SPAN>
        <br/>
        <SPAN class="TicketNumber">
          <xsl:value-of select="ticket_number"/>
        </SPAN>
      </SPAN>
    </xsl:if>
    <xsl:if test="string-length($IfValid) != 0">
      <xsl:variable name="Number">
        <xsl:call-template name="CheckNumber">
          <xsl:with-param name="ticket_number" select="ticket_number"/>
        </xsl:call-template>
      </xsl:variable>
      <SPAN class="Barcode">
        <SPAN style="color: red;" class="TicketNumber">No Ticket Number</SPAN>
        <br/>
        <SPAN class="TicketNumber">
          <xsl:value-of select="ticket_number"/>
        </SPAN>
      </SPAN>
    </xsl:if>
  </xsl:template>
  <xsl:template  name="Interleaved2ofValid">
    <xsl:param name="ticket_number"/>
    <xsl:if test="string-length($ticket_number) = 0">
      X
    </xsl:if>
  </xsl:template>
  <xsl:template  name="CheckNumber">
    <xsl:param name="ticket_number"/>
    <xsl:variable name="i" select="round(string-length($ticket_number) div 2)"/>
    <xsl:if test="string-length($ticket_number) div 2 != $i">
      <xsl:value-of select="'0'"/>
    </xsl:if>
    <xsl:value-of select="$ticket_number"/>
  </xsl:template>
  <xsl:template  name="Interleaved2of5Render">
    <xsl:param name="Char1"/>
    <xsl:param name="Char2"/>
    <xsl:variable name="Bar1">
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char1"/>
        <xsl:with-param name="Pos" select="'1'"/>
      </xsl:call-template>
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char2"/>
        <xsl:with-param name="Pos" select="'1'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Bar2">
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char1"/>
        <xsl:with-param name="Pos" select="'2'"/>
      </xsl:call-template>
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char2"/>
        <xsl:with-param name="Pos" select="'2'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Bar3">
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char1"/>
        <xsl:with-param name="Pos" select="'3'"/>
      </xsl:call-template>
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char2"/>
        <xsl:with-param name="Pos" select="'3'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Bar4">
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char1"/>
        <xsl:with-param name="Pos" select="'4'"/>
      </xsl:call-template>
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char2"/>
        <xsl:with-param name="Pos" select="'4'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Bar5">
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char1"/>
        <xsl:with-param name="Pos" select="'5'"/>
      </xsl:call-template>
      <xsl:call-template name="Interleaved2of5Encoding">
        <xsl:with-param name="Character" select="$Char2"/>
        <xsl:with-param name="Pos" select="'5'"/>
      </xsl:call-template>
    </xsl:variable>
    <SPAN class="{$Bar1}"></SPAN>
    <SPAN class="{$Bar2}"></SPAN>
    <SPAN class="{$Bar3}"></SPAN>
    <SPAN class="{$Bar4}"></SPAN>
    <SPAN class="{$Bar5}"></SPAN>
  </xsl:template>
  <xsl:template  name="Interleaved2of5Encoding">
    <xsl:param name="Character"/>
    <xsl:param name="Pos"/>
    <xsl:choose>
      <xsl:when test="$Character = '0'">
        <xsl:value-of select="substring('11221',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '1'">
        <xsl:value-of select="substring('21112',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '2'">
        <xsl:value-of select="substring('12112',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '3'">
        <xsl:value-of select="substring('22111',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '4'">
        <xsl:value-of select="substring('11212',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '5'">
        <xsl:value-of select="substring('21211',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '6'">
        <xsl:value-of select="substring('12211',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '7'">
        <xsl:value-of select="substring('11122',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '8'">
        <xsl:value-of select="substring('21121',number($Pos),1)"/>
      </xsl:when>
      <xsl:when test="$Character = '9'">
        <xsl:value-of select="substring('12121',number($Pos),1)"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <TITLE>
          <xsl:value-of select="$Titel"/>
        </TITLE>
        <STYLE>
          .PanelItinerary .BookingNumber{
          color: Red;
          }
          .PanelItinerary.PanelHeader
          {
          font: bold 11pt;
          color: #C4102F;
          font-family: Tahoma, Arial, sans-serif, verdana;
          /*background: url(../Images/0/H/hbh.jpg);*/
          margin: 5px 0px 5px 5px;
          width: 100%;
          }
          .PanelItinerary .PanelHeader IMG{
          vertical-align: middle;
          }
          .PanelItinerary .PanelHeader SPAN{
          padding-left: 1;
          }
          .PanelItinerary .PanelContainer{
          padding: 10 0 10 20;
          width: 100%;
          display: block;
          }
          .GridHeader TD
          {
          background: #fdb813;
          color:  #003964;
          font-family: Tahoma, Arial, sans-serif, verdana;
          font-size: 11px;
          font-weight: bold;
          padding: 4px 2px 4px 5px;
          }
          .PanelItinerary TR.GridItems
          {
          border-bottom: 1px solid #DAE1E6;
          color:  #003964;
          cursor: pointer;
          font-family: Tahoma, Arial, sans-serif, verdana;
          font-size: 11px;
          font-weight: normal;
          height: 22px;
          padding: 1px 1px 1px 5px;
          }
          .GridItems TD
          {
          border-bottom: 1px solid #fdb813;
          color:  #003964;
          cursor: pointer;
          font-family: Tahoma, Arial, sans-serif, verdana;
          font-size: 11px;
          font-weight: normal;
          height: 22px;
          padding: 1px 1px 1px 5px;
          }
          .GridItemsTop TD
          {
          border-top: 1px solid #fdb813;
          color:  #003964;
          cursor: pointer;
          font-family: Tahoma, Arial, sans-serif, verdana;
          font-size: 11px;
          font-weight: normal;
          height: 22px;
          padding: 1px 1px 1px 5px;
          }
          .GridItemsBlank TD
          {
          color:  #003964;
          cursor: pointer;
          font-family: Tahoma, Arial, sans-serif, verdana;
          font-size: 11px;
          font-weight: normal;
          height: 22px;
          padding: 1px 1px 1px 5px;
          }
          .GridItemsTopButton TD
          {
          border-top: 1px solid #fdb813;
          border-bottom: 1px solid #fdb813;
          color:  #003964;
          cursor: pointer;
          font-family: Tahoma, Arial, sans-serif, verdana;
          font-size: 11px;
          font-weight: normal;
          height: 22px;
          padding: 1px 1px 1px 5px;
          }
          .PanelItinerary .GridFooter TD SPAN
          {
          font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
          font-size: 11px;
          height: 18px;
          padding: 4 5 0 4;
          }
          .PanelItinerary .FooterTotal
          {
          border-bottom: solid 1px #dddddd;
          }
          .PanelItinerary SPAN.FooterTotalLabel
          {
          background-color: transparent;
          color:  #003964;
          font: bold 8pt Tahoma, Arial, sans-serif, verdana;
          padding: 1px 0px 1px 0px;
          }
          .PanelItinerary SPAN.FooterTotalLabelRed
          {
          background-color: transparent;
          color: Red;
          font: bold 8pt Tahoma, Arial, sans-serif, verdana;
          padding: 1px 0px 1px 0px;
          }
          .PanelItinerary SPAN.FooterTotalValue
          {
          color:  #003964;
          font-size: 11px;
          }
          .PanelItinerary .BarcodeHeader
          {
          font-family: verdana, arial, helvetica, sans-serif;
          font-size: 10px;
          font-weight: bold;
          color: #00529d;
          padding: 5 0 3 5;
          margin: 0 0 0 0;
          width: 100%;
          cursor: hand;
          }
          .PanelItinerary .Barcode
          {
          padding: 1 5 1 5;
          }
          .WelcomeText{
          font-family: verdana, arial, helvetica, sans-serif;
          font-size: 12px;
          padding: 1 10 1 5;
          }
          .CommentText{
          font-family: verdana, arial, helvetica, sans-serif;
          font-size: 12px;
          padding: 1 10 1 5;
          }
          SPAN.Barcode {
          }
          SPAN.Barcode SPAN{
          background : Black;
          height:30px;
          }
          SPAN.Barcode SPAN.11{
          width: 1px;
          margin: 0 1 0 0;
          }
          SPAN.Barcode SPAN.12{
          width: 1px;
          margin: 0 2 0 0;
          }
          SPAN.Barcode SPAN.21{
          width: 2px;
          margin: 0 1 0 0;
          }
          SPAN.Barcode SPAN.22{
          width: 2px;
          margin: 0 1 0 0;
          }
          SPAN.Barcode SPAN.1{
          width: 1px;
          }
          SPAN.Barcode SPAN.TicketNumber{
          background: White;
          font-family: tahoma, verdana, arial, helvetica, sans-serif;
          font-size: 8pt;
          width: 100%;
          padding-left: 10px;
          }
        </STYLE>
      </HEAD>
      <BODY>
        <!--<img src="{$BaseURL}Images/WCA_corp.logo.gif" />-->
        <DIV class="PanelItinerary">
          <xsl:if test="//TicketQuotes/Total = true()">
            <DIV class="PanelHeader">
              <SPAN style="font-size: 10pt">Quotes</SPAN>
              <TABLE cellspacing="0" rules="all" border="0"  class="Grid" style="border-width:0px;width:100%;border-collapse:collapse;">
                <TR class="GridHeader">
                  <TD>Passenger</TD>
                  <td align="center">Units</td>
                  <TD>Charge</TD>
                  <td align="right">Amount</td>
                  <td align="right">MWST</td>
                  <td align="right">Total</td>
                </TR>
                <xsl:for-each select="//Passengers/Passenger[count(. | key('passenger_type_rcd_group', passenger_type_rcd)[1]) = 1]">

                  <xsl:variable name="passenger_type_rcd" select="passenger_type_rcd" />
                  <xsl:for-each select="//TicketQuotes/Total[passenger_type_rcd=$passenger_type_rcd]">
                    <xsl:variable name="TotalCharge">
                      <xsl:if test="charge_type != 'REFUND'">
                        <xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
                      </xsl:if>
                      <xsl:if test="charge_type = 'REFUND'">
                        <xsl:value-of select="sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
                      </xsl:if>
                    </xsl:variable>
                    <xsl:if test="position()=1">
                      <xsl:if test="position()!=last()">
                        <tr class="GridItemsBlank">
                          <td>
                            <xsl:value-of select="passenger_type_rcd" />
                          </td>
                          <td align="center">
                            <xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)" />
                          </td>
                          <td>
                            <xsl:value-of select="charge_name" />
                          </td>
                          <td align="right">
                            <xsl:value-of select="format-number(charge_amount,'#,##0.00')" />
                          </td>
                          <td align="right">
                            <xsl:choose>
                              <xsl:when test="format-number(tax_amount,'0.00') = 0">
                                &#160;
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="format-number(tax_amount,'#,##0.00')" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td align="right">
                            <xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#160;&#160;<xsl:value-of select="currency_rcd"/>
                          </td>
                        </tr>
                      </xsl:if>
                      <xsl:if test="position()=last()">
                        <tr class="GridItems">
                          <td>
                            <xsl:value-of select="passenger_type_rcd" />
                          </td>
                          <td align="center">
                            <xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)" />
                          </td>
                          <td>
                            <xsl:value-of select="charge_name" />
                          </td>
                          <td align="right">
                            <xsl:value-of select="format-number(charge_amount,'#,##0.00')" />
                          </td>
                          <td align="right">
                            <xsl:choose>
                              <xsl:when test="format-number(tax_amount,'0.00') = 0">
                                &#160;
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="format-number(tax_amount,'#,##0.00')" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td align="right">
                            <xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#160;&#160;<xsl:value-of select="currency_rcd"/>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:if>

                    <xsl:if test="position()!=1">
                      <xsl:if test="position()=last()">
                        <xsl:if test="charge_type != 'REFUND'">
                          <tr class="GridItems">
                            <td>&#160;</td>
                            <td>&#160;</td>
                            <td>
                              <xsl:value-of select="charge_name" />
                            </td>
                            <td align="right">
                              <xsl:value-of select="format-number(charge_amount,'#,##0.00')" />
                            </td>
                            <td align="right">
                              <xsl:choose>
                                <xsl:when test="format-number(tax_amount,'0.00') = 0">
                                  &#160;
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="format-number(tax_amount,'#,##0.00')" />
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td>&#160;</td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="charge_type = 'REFUND'">
                          <tr class="GridItems">
                            <td>&#160;</td>
                            <td align="center">&#160;</td>
                            <td>
                              <xsl:value-of select="charge_name" />
                            </td>
                            <td align="right">&#160;</td>
                            <td align="right">&#160;</td>
                            <td align="right">
                              -<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#160;&#160;<xsl:value-of select="currency_rcd"/>
                            </td>
                          </tr>
                        </xsl:if>
                      </xsl:if>

                      <xsl:if test="position()!=last()">
                        <xsl:if test="charge_type != 'REFUND'">
                          <tr class="GridItemsBlank">
                            <td>&#160;</td>
                            <td>&#160;</td>
                            <td>
                              <xsl:value-of select="charge_name" />
                            </td>
                            <td align="right">
                              <xsl:value-of select="format-number(charge_amount,'#,##0.00')" />
                            </td>
                            <td align="right">
                              <xsl:choose>
                                <xsl:when test="format-number(tax_amount,'0.00') = 0">
                                  &#160;
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="format-number(tax_amount,'#,##0.00')" />
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td>&#160;</td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="charge_type = 'REFUND'">
                          <tr class="GridItemsBlank">
                            <td>&#160;</td>
                            <td align="center">&#160;</td>
                            <td>
                              <xsl:value-of select="charge_name" />
                            </td>
                            <td align="right">&#160;</td>
                            <td align="right">&#160;</td>
                            <td align="right">
                              -<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#160;&#160;<xsl:value-of select="currency_rcd"/>
                            </td>
                          </tr>
                        </xsl:if>
                      </xsl:if>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:for-each>
                <xsl:for-each select="//Fees/Fee[void_date_time = '']">
                  <xsl:if test="position()=1">
                    <tr class="GridItemsTop">
                      <td>&#160;</td>
                      <td>&#160;</td>
                      <td>
                        <xsl:value-of select="display_name" />
                      </td>
                      <td align="right">
                        <xsl:value-of select="format-number(fee_amount,'#,##0.00')" />
                      </td>
                      <td align="right">
                        <xsl:choose>
                          <xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">
                            &#160;
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </td>
                      <td align="right">
                        <xsl:value-of select="format-number(sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/>&#160;&#160;<xsl:value-of select="currency_rcd"/>
                      </td>
                    </tr>
                  </xsl:if>
                  <xsl:if test="position()!=1">
                    <tr class="GridItemsBlank">
                      <td>&#160;</td>
                      <td>&#160;</td>
                      <td>
                        <xsl:value-of select="display_name" />
                      </td>
                      <td align="right">
                        <xsl:value-of select="format-number(fee_amount,'#,##0.00')" />
                      </td>
                      <td align="right">
                        <xsl:choose>
                          <xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">
                            &#160;
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </td>
                      <td>&#160;</td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
                <tr class="GridItemsTopButton">
                  <td>&#160;</td>
                  <td>&#160;</td>
                  <td>
                    <SPAN class="FooterTotalLabel">Total</SPAN>
                  </td>
                  <td align="right">
                    <SPAN class="FooterTotalLabel">
                      &#160;<!--<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount)- sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount),'#,##0.00')"/>-->
                    </SPAN>
                  </td>
                  <td align="right">
                    <SPAN class="FooterTotalLabel">
                      &#160;<!--<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount))+ (sum(//Fees/Fee[void_date_time = '']/fee_amount_incl) - sum(//Fees/Fee[void_date_time = '']/fee_amount)),'#,##0.00')"/>-->
                    </SPAN>
                  </td>
                  <td align="right">
                    <SPAN class="FooterTotalLabel">
                      <xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/>&#160;&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                    </SPAN>
                  </td>
                </tr>
              </TABLE>
            </DIV>
          </xsl:if>

          <DIV class="PanelHeader">
            <SPAN style="font-size: 10pt">Payments</SPAN>
            <TABLE width="100%" border="0" ellspacing="0" rules="non" style="border-width:0px;border-collapse:collapse;" >
              <TR class="GridHeader">
                <TD align="left" width="30%">
                  <SPAN class="GridHeader">Description</SPAN>
                </TD>
                <TD align="left" width="30%">
                  <SPAN class="GridHeader">&#160;</SPAN>
                </TD>
                <TD align="left" width="10%">
                  <SPAN class="GridHeader">Status, Date</SPAN>
                </TD>
                <TD align="right" width="15%">
                  <SPAN class="GridHeader">Credit</SPAN>
                </TD>
                <TD align="right" width="15%">
                  <SPAN class="GridHeader">Debit</SPAN>
                </TD>
              </TR>
              <xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)" />
              <xsl:variable name="Ticket_total" select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)" />
              <xsl:choose>
                <xsl:when test="$Ticket_total &gt;=  $Payment_total">
                  <TR valign="middle" align="left" class="GridItems">
                    <TD align="left">Ticket Cost &amp; Fee</TD>
                    <TD align="left">&#160;</TD>
                    <TD align="left">&#160;</TD>
                    <xsl:if test="starts-with(string($Ticket_total), '-')">
                      <TD align="left">&#160;</TD>
                      <TD align="right">
                        <xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                      </TD>
                    </xsl:if>
                    <xsl:if test="not(starts-with(string($Ticket_total), '-'))">
                      <TD align="right">
                        <xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                      </TD>
                      <TD align="left">&#160;</TD>
                    </xsl:if>
                  </TR>
                  <xsl:for-each select="Booking/Payments/Payment[(substring(void_date_time,5,2) = '')]">
                    <xsl:sort select="payment_date_time"/>
                    <xsl:if test="form_of_payment_subtype_rcd !=''">
                      <TR valign="middle" align="left" class="GridItems">
                        <TD align="left">
                          <xsl:if test="form_of_payment_rcd ='CASH'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='MANUAL'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='VOUCHER'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CC'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='TKT'">
                            <xsl:value-of select="form_of_payment"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CRAGT'">
                            <xsl:value-of select="form_of_payment"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='INV'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CHEQUE'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='BANK'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                        </TD>
                        <xsl:if test="form_of_payment_rcd ='CC'">
                          <TD align="left">
                            <xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
                          </TD>
                        </xsl:if>
                        <xsl:if test="form_of_payment_rcd !='CC'">
                          <TD align="left">
                            <xsl:value-of select="document_number"/>
                          </TD>
                        </xsl:if>
                        <TD align="left">
                          <xsl:if test="payment_date_time != ''">
                            <xsl:value-of select="substring(payment_date_time,5,2)"/>/<xsl:value-of select="substring(payment_date_time,7,2)"/>/<xsl:value-of select="substring(payment_date_time,1,4)"/>
                          </xsl:if>
                        </TD>
                        <xsl:if test="starts-with(string(payment_amount), '-')">
                          <TD align="right">
                            <xsl:value-of select="format-number(substring(payment_amount,2,string-length(payment_amount)),'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
                          </TD>
                          <TD align="left">&#160;</TD>
                        </xsl:if>
                        <xsl:if test="not(starts-with(string(payment_amount), '-'))">
                          <TD align="left">&#160;</TD>
                          <TD align="right">
                            <xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
                          </TD>
                        </xsl:if>
                      </TR>
                    </xsl:if>
                    <xsl:if test="form_of_payment_subtype_rcd =''">
                      <TR valign="middle" align="left"  class="GridItems">
                        <TD align="left">
                          <xsl:if test="form_of_payment_rcd ='CASH'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='MANUAL'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='VOUCHER'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CC'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='TKT'">
                            <xsl:value-of select="form_of_payment"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CRAGT'">
                            <xsl:value-of select="form_of_payment"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='INV'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CHEQUE'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='BANK'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                        </TD>
                        <xsl:if test="form_of_payment_rcd ='CC'">
                          <TD align="left">
                            <xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
                          </TD>
                        </xsl:if>
                        <xsl:if test="form_of_payment_rcd !='CC'">
                          <TD align="left">
                            <xsl:value-of select="document_number"/>
                          </TD>
                        </xsl:if>
                        <TD align="left">
                          <xsl:if test="payment_date_time != ''">
                            <xsl:value-of select="substring(payment_date_time,5,2)"/>/<xsl:value-of select="substring(payment_date_time,7,2)"/>/<xsl:value-of select="substring(payment_date_time,1,4)"/>
                          </xsl:if>
                        </TD>
                        <xsl:if test="starts-with(string(payment_amount), '-')">
                          <TD align="right">
                            <xsl:value-of select="format-number(substring(payment_amount,2,string-length(payment_amount)),'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
                          </TD>
                          <TD align="left">&#160;</TD>
                        </xsl:if>
                        <xsl:if test="not(starts-with(string(payment_amount), '-'))">
                          <TD align="left">&#160;</TD>
                          <TD align="right">
                            <xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
                          </TD>
                        </xsl:if>
                      </TR>
                    </xsl:if>
                  </xsl:for-each>
                  <TR valign="middle" align="left" class="GridItems">
                    <TD align="left">
                      <SPAN class="FooterTotalLabel">OUTSTANDING BALANCE</SPAN>
                    </TD>
                    <TD align="left">&#160;</TD>
                    <TD align="left">&#160;</TD>
                    <xsl:variable name="SubTotal" select="format-number($Ticket_total - $Payment_total,'0.00')" />
                    <xsl:if test="number($SubTotal) = 0">
                      <TD align="left">&#160;</TD>
                      <TD align="right">
                        <SPAN class="FooterTotalLabel">
                          0.00&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                        </SPAN>
                      </TD>
                    </xsl:if>
                    <xsl:if test="number($SubTotal) != 0">
                      <xsl:if test="number($SubTotal) >= 0">
                        <TD align="left">&#160;</TD>
                        <TD align="right">
                          <SPAN class="FooterTotalLabel">
                            <xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                          </SPAN>
                        </TD>
                      </xsl:if>
                      <xsl:if test="number($SubTotal) &lt; 0">
                        <TD align="right">
                          <SPAN class="FooterTotalLabel">
                            <xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                          </SPAN>
                        </TD>
                        <TD align="left">&#160;</TD>
                      </xsl:if>
                    </xsl:if>

                  </TR>
                </xsl:when>
                <xsl:otherwise>
                  <TR valign="middle" align="left" class="GridItems">
                    <TD align="left">Ticket Cost &amp; Fee</TD>
                    <TD align="left">&#160;</TD>
                    <TD align="left">&#160;</TD>
                    <xsl:if test="starts-with(string($Ticket_total), '-')">
                      <TD align="left">&#160;</TD>
                      <TD align="right">
                        <xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                      </TD>
                    </xsl:if>
                    <xsl:if test="not(starts-with(string($Ticket_total), '-'))">
                      <TD align="right">
                        <xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                      </TD>
                      <TD align="left">&#160;</TD>
                    </xsl:if>
                  </TR>
                  <xsl:for-each select="Booking/Payments/Payment[(substring(void_date_time,5,2) = '')]">
                    <xsl:if test="form_of_payment_subtype_rcd !=''">
                      <TR valign="middle" align="left"  class="GridItems">
                        <TD align="left">
                          <xsl:if test="form_of_payment_rcd ='CASH'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='MANUAL'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='VOUCHER'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CC'">
                            <xsl:value-of select="form_of_payment_subtype_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='TKT'">
                            <xsl:value-of select="form_of_payment"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CRAGT'">
                            <xsl:value-of select="form_of_payment"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='INV'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='CHEQUE'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                          <xsl:if test="form_of_payment_rcd ='BANK'">
                            <xsl:value-of select="form_of_payment_rcd"/>
                          </xsl:if>
                        </TD>
                        <xsl:if test="form_of_payment_rcd ='CC'">
                          <TD align="left">
                            <xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
                          </TD>
                        </xsl:if>
                        <xsl:if test="form_of_payment_rcd !='CC'">
                          <TD align="left">
                            <xsl:value-of select="document_number"/>
                          </TD>
                        </xsl:if>
                        <TD align="left">
                          <xsl:if test="payment_date_time != ''">
                            <xsl:value-of select="substring(payment_date_time,5,2)"/>/<xsl:value-of select="substring(payment_date_time,7,2)"/>/<xsl:value-of select="substring(payment_date_time,1,4)"/>
                          </xsl:if>
                        </TD>
                        <xsl:if test="starts-with(string(payment_amount), '-')">
                          <TD align="right">
                            <xsl:value-of select="format-number(substring(payment_amount,2,string-length(payment_amount)),'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
                          </TD>
                          <TD align="left">&#160;</TD>
                        </xsl:if>
                        <xsl:if test="not(starts-with(string(payment_amount), '-'))">
                          <TD align="left">&#160;</TD>
                          <TD align="right">
                            <xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
                          </TD>
                        </xsl:if>
                      </TR>
                    </xsl:if>
                    <xsl:if test="form_of_payment_subtype_rcd =''">
                      <TR valign="middle" align="left"  class="GridItems">
                        <TD align="left">
                          <xsl:value-of select="form_of_payment_rcd"/>
                        </TD>
                        <TD align="left">&#160;</TD>
                        <TD align="left">
                          <xsl:if test="payment_date_time != ''">
                            <xsl:value-of select="substring(payment_date_time,5,2)"/>/<xsl:value-of select="substring(payment_date_time,7,2)"/>/<xsl:value-of select="substring(payment_date_time,1,4)"/>
                          </xsl:if>
                        </TD>
                        <xsl:if test="starts-with(string(payment_amount), '-')">
                          <TD align="right">
                            <xsl:value-of select="format-number(substring(payment_amount,2,string-length(payment_amount)),'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
                          </TD>
                          <TD align="left">&#160;</TD>
                        </xsl:if>
                        <xsl:if test="not(starts-with(string(payment_amount), '-'))">
                          <TD align="left">&#160;</TD>
                          <TD align="right">
                            <xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#160;<xsl:value-of select="currency_rcd"/>
                          </TD>
                        </xsl:if>
                      </TR>
                    </xsl:if>
                  </xsl:for-each>
                  <TR valign="middle" align="left" class="GridItems">
                    <TD align="left">
                      <SPAN class="FooterTotalLabel">OUTSTANDING BALANCE</SPAN>
                    </TD>
                    <TD align="left">&#160;</TD>
                    <TD align="left">&#160;</TD>
                    <xsl:variable name="SubTotal" select="format-number($Ticket_total - $Payment_total,'0.00')" />
                    <xsl:if test="number($SubTotal) = 0">
                      <TD align="left">&#160;</TD>
                      <TD align="right">
                        <SPAN class="FooterTotalLabel">
                          0.00&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                        </SPAN>
                      </TD>
                    </xsl:if>
                    <xsl:if test="number($SubTotal) != 0">
                      <xsl:if test="number($SubTotal) >= 0">
                        <TD align="left">&#160;</TD>
                        <TD align="right">
                          <SPAN class="FooterTotalLabel">
                            <xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                          </SPAN>
                        </TD>
                      </xsl:if>
                      <xsl:if test="number($SubTotal) &lt; 0">
                        <TD align="right">
                          <SPAN class="FooterTotalLabel">
                            <xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#160;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
                          </SPAN>
                        </TD>
                        <TD align="left">&#160;</TD>
                      </xsl:if>
                    </xsl:if>

                  </TR>
                </xsl:otherwise>
              </xsl:choose>
            </TABLE>
          </DIV>
        </DIV>
      </BODY>
    </HTML>
  </xsl:template>
</xsl:stylesheet>
