
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://xsltsl.org/xsl/documentation/1.0" xmlns:dt="http://xsltsl.org/date-time" xmlns:str="http://xsltsl.org/string" extension-element-prefixes="doc str">
	<xsl:import href="string.xsl"/>
	<xsl:import href="date-time.xsl"/>
	<xsl:variable name="BarURL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
	<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>


	<xsl:template name="AddrFormat">
		<xsl:param name="value"></xsl:param>
		<xsl:choose>
			<xsl:when test="string-length($value) != 0">
				<xsl:value-of select="$value"/>&#xA0;</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--Header Section-->
	<xsl:template match="Booking/Header">
		<!-- variable Header-->
		<xsl:variable name="BarURL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
		<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>
		<table style="width: 100%" cellspacing="0">
			<tr>
				<td>
					<table class="tabledetails" cellspacing="0">
						<tr>
							<td class="tdmargin"/>
							<td class="documentheader" style="width: 120px">Passenger Name&#xA0;</td>
							<td>
								<span class="tableheader" style="padding-left: 0px;">
									<strong>
										<xsl:value-of select="/Booking/Passengers/Passenger/lastname"/>/<xsl:value-of select="/Booking/Passengers/Passenger/firstname"/>&#xA0;<xsl:value-of select="/Booking/Passengers/Passenger/title"/></strong>
								</span>
							</td>
							<td class="documentheader">&#xA0;</td>
							<td>&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td class="documentheader">Reservation-Nbr:</td>
							<td>
								<span class="tableheader" style="padding-left: 0px;">
									<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>&#xA0;</span>
							</td>
							<td>&#xA0;</td>
							<td>&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td class="documentheader"/>
							<td>
								<span class="tableheader" style="padding-left: 0px;">
									<!--<xsl:call-template name="dt:formatdate">
										<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
									</xsl:call-template>-->&#xA0;</span>
							</td>
							<td>&#xA0;</td>
							<td>&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>&#xA0;</td>
							<td>&#xA0;</td>
							<td>&#xA0;</td>
							<td>&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
					</table>
					<table class="tabledetails" cellspacing="0">
						<tr>
							<td class="tdmargin"/>
							<td>All times noted are local times.  Please keep this receipt with you throughout your journey.&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>Your  InterSky ticket is stored  in our reservation system. Find fare rules at the bottom of this page&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>and payment conditions overleaf on page 2.&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
					</table>
				</td>
				<td style="font-size: 9pt;" align="right" width="164">
					<!--<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="164" height="57"/>-->InterSky Call-Center <br/>Intl: +43 5574 48800 46 <br/>DE: +49 7541 286 96 84 <br/>AT: +43 5574 48800 46 <br/>CH: +41 31 819 72 22 <br/>
					<a href="mailto:reservation@intersky.biz">reservation@intersky.biz</a>
					<br/>Mo - Fr: 08:00 - 18:00 Uhr</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:template>
	<!--End Header Section-->


	<!--Passenger Section-->
	<xsl:template match="Booking/Passengers">
		<!--Passengers-->
		<span class="tableheader">Passengers</span>
		<xsl:copy-of select="$PassengerRowsHeader"/>
		<xsl:for-each select="/Booking/Passengers/Passenger">
			<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
				<TR>
					<td class="tdmargin"/>
					<TD align="right" class="blueline" style="width: 25px">
						<xsl:choose>
							<xsl:when test="position()&gt;9">
								<xsl:value-of select="concat('0','',string(position()))"/>&#xA0;</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="concat('00','',string(position()))"/>&#xA0;</xsl:otherwise>
						</xsl:choose>
					</TD>
					<TD align="left" class="blueline">
						<xsl:value-of select="lastname"/>
						<xsl:text>&#xA0;</xsl:text>
					</TD>
					<TD align="left" class="blueline">
						<xsl:value-of select="firstname"/>
						<xsl:text>&#xA0;</xsl:text>
					</TD>
					<TD align="center" class="blueline">
						<xsl:value-of select="title_rcd"/>
						<xsl:text>&#xA0;</xsl:text>
					</TD>
					<TD align="center" class="blueline">
						<xsl:value-of select="passenger_type_rcd"/>
						<xsl:text>&#xA0;</xsl:text>
					</TD>
					<TD align="center" class="blueline">
						<xsl:value-of select="passport_number"/>
						<xsl:text>&#xA0;</xsl:text>
					</TD>
					<td class="tdmargin"/>
				</TR>
			</table>
			<xsl:if test="(position() mod 9) = 0 ">
				<!--Count1=<xsl:value-of select="position()"/>-->
				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="1"/>
				</xsl:call-template>
				<!--Count2=<xsl:value-of select="position()"/>-->
				<xsl:copy-of select="$ReportFooter"/>
				<br class="pagebreak"/>
				<!--Fix Header Section-->
				<xsl:copy-of select="$ReportHeader"/>
				<br/>
				<xsl:copy-of select="$ReportHeader1"/>
				<!--				"position() mod 9="<xsl:value-of select="(position() mod 9) "/>
				"Cout="<xsl:value-of select="(count(/Booking/Passengers/Passenger)) "/>
				"Cout  mod 9="<xsl:value-of select="(count(/Booking/Passengers/Passenger) mod 9) "/>-->
				<xsl:if test="(count(/Booking/Passengers/Passenger) mod 9) &gt; 0 ">
					<br/>
					<span class="tableheader">Passengers</span>
					<xsl:copy-of select="$PassengerRowsHeader"/>
				</xsl:if>
				<!--End Fix Header Section-->
			</xsl:if>
		</xsl:for-each>
		<!--PassengerFiller -->
		<xsl:choose>
			<!-- case of only one page-->
			<xsl:when test="count(/Booking/Passengers/Passenger) &lt;= 9">
				<xsl:call-template name="Filler">
					<!--<xsl:with-param name="fillercount" select="9 - (count(/Booking/Passengers/Passenger))"/>-->
					<xsl:with-param name="fillercount" select="1"/>
				</xsl:call-template>
			</xsl:when>
			<!-- case of more than one page-->
			<xsl:otherwise>
				<xsl:call-template name="Filler">
					<!--(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)-->
					<xsl:with-param name="fillercount" select="12 - ( ( count(/Booking/Passengers/Passenger)-9 ) mod 9 ) - 3 + 1"/>
					<!--<xsl:with-param name="fillercount" select="0"/>-->
				</xsl:call-template>
				<!--Count1=<xsl:value-of select="position()"/>-->
				<!--				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="1"/>
				</xsl:call-template>-->
				<!--Count2=<xsl:value-of select="position()"/>-->
				<xsl:copy-of select="$ReportFooter"/>
				<br class="pagebreak"/>
				<!--Fix Header Section-->
				<xsl:copy-of select="$ReportHeader"/>
				<br/>
				<xsl:copy-of select="$ReportHeader1"/>
				<br/>
				<!--<span class="tableheader">Passengers</span>
				<xsl:copy-of select="$PassengerRowsHeader"/>-->
				<!--End Fix Header Section-->
			</xsl:otherwise>
		</xsl:choose>
		<!--End PassengerFiller -->
		<!--End Passengers-->
	</xsl:template>
	<xsl:variable name="PassengerRowsHeader">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th style="width: 25px">&#xA0;</th>
				<th align="left">Lastname</th>
				<th align="left">Firstname</th>
				<th>Title</th>
				<th>Type</th>
				<th>ID Number</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<!--End Passenger Section-->


	<!--Flights Section-->
	<xsl:template match="Booking/Itinerary">
		<!--<span class="tableheader">Flights</span>-->
		<!--<xsl:copy-of select="$FlightRowsHeader"/>-->
		<xsl:for-each select="/Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
			<xsl:variable name="FlightID" select="flight_id"/>
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
			<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
				<TR>
					<td class="tdmargin"/>
					<table cellspacing="0" style="width: 100%">
						<tr>
							<td class="tdmargin"/>
							<td class="auto-style2" colspan="3" style="font-size: 13pt;font-weight: bold;">
								<xsl:value-of select="position()"/>
								<xsl:text>) From</xsl:text>&#xA0; <xsl:value-of select="origin_name"/>&#xA0;(<xsl:value-of select="origin_rcd"/>) 	to <xsl:value-of select="destination_name"/>&#xA0;(<xsl:value-of select="destination_rcd"/>)</td>
							<td class="auto-style2" colspan="3" style="text-align: right;font-size: 14pt;font-weight: bold;">Flight&#xA0;<xsl:value-of select="airline_rcd"/>
								<xsl:value-of select="flight_number"/>
							</td>
							<td class="tdmargin"/>
						</tr>
					</table>
					<table cellspacing="0" style="width: 100%">
						<tr>
							<td class="tdmargin"/>
							<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 2px">Departure Date</td>
							<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1px">Departure</td>
							<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1px">Arrival</td>
							<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1px">Class of Travel</td>
							<td class="auto-style3" style="font-weight: bold;border-width: 2px 1px 1px 1pxwidth: 98px">Status</td>
							<td class="auto-style3" style="font-weight: bold;border-width: 2px 2px 1px 1px">Baggage Allowance</td>
							<!--<td class="tdmargin"/>-->
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td class="auto-style3" style="font-weight: bold;border-width: 1px 1px 1px 2px">
								<xsl:if test="string-length(departure_date) &gt; '0'">
									<xsl:call-template name="dt:formatdate">
										<xsl:with-param name="date" select="departure_date"/>
									</xsl:call-template>
								</xsl:if>
								<xsl:if test="string-length(departure_date) = '0'">
									<xsl:text>OPEN</xsl:text>
								</xsl:if>
							</td>
							<td class="auto-style3">
								<xsl:if test="string(departure_time) != '0'">
									<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/>
									&#xA0;Terminal:&#xA0;<xsl:value-of select="origin_terminal"/></xsl:if>
							</td>
							<td class="auto-style3">
								<xsl:if test="string(planned_arrival_time) != '0'">
									<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/>
									&#xA0;Terminal:&#xA0;<xsl:value-of select="destination_terminal"/></xsl:if>
							</td>
							<td class="auto-style3">
								<xsl:value-of select="class_name"/>&#xA0;(<xsl:value-of select="booking_class_rcd"/>)&#xA0;</td>
							<td class="auto-style3" style="width: 98px">
								<xsl:value-of select="status_name"/>
							</td>
							<td class="auto-style3" style="border-width: 1px 2px 1px 1px">
								<xsl:value-of select="/Booking/Tickets/Ticket/baggage_weight"/>&#xA0;Kgs.</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td class="auto-style3" style="font-weight: bold; border-width: 1px 1px 1px 2px">Operated by</td>
							<td class="auto-style3" style="font-weight: bold;">On behalf of</td>
							<td class="auto-style3" style="font-weight: bold;">Not valid before</td>
							<td class="auto-style3" style="font-weight: bold;">Not valid after</td>
							<td class="auto-style3" style="font-weight: bold; border-width: 1px 2px 2px 1px;" colspan="2" rowspan="2">
								<div class="Barcode" style="height: 19px">
									<table width="100%" border="0">
										<tbody>
											<tr>
												<td align="center" width="100%">
													<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="164" height="40"/>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td class="auto-style3" style="border-width: 1px 1px 2px 2px;">
								<xsl:value-of select="flight_information_1"/>&#xA0;</td>
							<td class="auto-style3" style="border-width: 1px 1px 2px 1px;">&#xA0;</td>
							<td class="auto-style3" style="border-width: 1px 1px 2px 1px;">&#xA0;</td>
							<td class="auto-style3" style="border-width: 1px 1px 2px 1px;">&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
					</table>
					<table cellspacing="0" style="width: 100%">
						<tr>
							<td class="tdmargin"/>
							<td class="auto-style2" colspan="4">*Information ca be obtained locally</td>
							<td class="auto-style2" colspan="2" align="right">Barcode for check-in</td>
							<td class="tdmargin"/>
						</tr>
					</table>
					<td class="tdmargin"/>
				</TR>
			</table>
			<!--Flights-->
			<!--FlightTickets-->
			<table class="tabledetails" cellspacing="1" style="table-layout:fixed">
				<xsl:variable name="h" select="round(count(/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)]) div 1)"/>
				<tr>
					<td class="tdmargin">&#xA0;</td>
					<td class="blankline">Fare basis:&#xA0;</td>
					<td class="blankline">
						<xsl:value-of select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)]/fare_code"/>&#xA0;</td>
				</tr>
				<tr>
					<td class="tdmargin">&#xA0;</td>
					<td class="blankline">Endorsement:&#xA0;</td>
					<td class="blankline">
						<xsl:value-of select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)]/endorsement_text"/>&#xA0;</td>
				</tr>
				<tr>
					<td class="tdmargin">&#xA0;</td>
					<td class="blankline">Restrictions:&#xA0;</td>
					<td class="blankline">
						<xsl:value-of select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)]/restriction_text"/>&#xA0;</td>
				</tr>
				<xsl:for-each select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)]">
					<xsl:variable name="p" select="position()"/>
					<xsl:variable name="p2" select="($p+$h)"/>
					<!--variable $ticketnumber-->
					<xsl:variable name="ticketnumber" select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)][position()=$p]/ticket_number"/>
					<xsl:variable name="passengerid" select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)][ticket_number=$ticketnumber]/passenger_id"/>
					<xsl:variable name="bookingsegmentid" select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)][ticket_number=$ticketnumber]/booking_segment_id"/>
					<xsl:if test="$h&gt;=$p">
						<TR>
							<td class="tdmargin"/>
							<TD class="blankline">
								<!--<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p]/passenger_id"/>-->
								<xsl:choose>
									<xsl:when test="not(string-length(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)][position()=$p]/lastname) &gt; '0')">
										<xsl:value-of select="$p"/>. Passenger Name:a1&#xA0;<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)][position()=$p]/title_rcd"/>&#xA0;<xsl:value-of select="seat_number"/>
										<xsl:text>&#xA0;</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$p"/>. Ticket No:
										<xsl:value-of select="$ticketnumber"/>&#xA0;&#xA0;/&#xA0;Seat No:&#xA0;<xsl:value-of select="seat_number"/></xsl:otherwise>
								</xsl:choose>
							</TD>
							<TD class="blankline">
								<table cellpadding="0" cellspacing="0" style="width: 100%">
									<xsl:for-each select="//Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')][(passenger_id = $passengerid)][(booking_segment_id = $bookingsegmentid)]">
										<!--<xsl:sort select="departure_date" order="descending"/>-->
										<!--<xsl:if test="position()=1">-->
										<TR>
											<td>
												<xsl:value-of select="special_service_rcd"/>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:value-of select="display_name"/>
											</td>
										</TR>
										<!--</xsl:if>-->
									</xsl:for-each>
								</table>
							</TD>
						</TR>
					</xsl:if>
				</xsl:for-each>

				<xsl:if test="((9*position())+(count(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')] [(booking_segment_id = (//Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']/booking_segment_id))]))) &gt; 43">
					<xsl:call-template name="Filler">
						<xsl:with-param name="fillercount" select="1"/>
					</xsl:call-template>Count2=<xsl:value-of select="position()"/>
					<xsl:copy-of select="$ReportFooterIntinerary"/>a<br class="pagebreak"/>
					<!--Fix Header Section-->


					<!--					<xsl:copy-of select="$ReportHeader"/>
					<br/>
					<xsl:copy-of select="$ReportHeader1"/>-->



					<!--<xsl:if test="(6 + (count((/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)])) ) mod 30) &gt; 0 ">
						<br/>
						<span class="tableheader">Passengers</span>
						<xsl:copy-of select="$FlightRowsHeader"/>
					</xsl:if>-->
					<!--End Fix Header Section--></xsl:if>

				<!-- Page <xsl:value-of select="(6*count(/Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX'])) + (count((/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)])) div 2) mod 30"/>-->
				<!--Filler -->
				<xsl:choose>
					<!-- case of only one page-->
					<xsl:when test="((9*position())+(count(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')] [(booking_segment_id = (//Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']/booking_segment_id))]))) &lt;= 43">
						<xsl:choose>
							<xsl:when test="count(//Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX'])=position()">
								<xsl:call-template name="Filler">
									<xsl:with-param name="fillercount"
									                select="43 - ((9*position())+(count(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')] [(booking_segment_id = (//Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']/booking_segment_id))])))"/>
									<!--<xsl:with-param name="fillercount" select="1"/>-->
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="Filler">
									<!--<xsl:with-param name="fillercount" select="47 - ((9*position())+(count(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')])) )"/>-->
									<xsl:with-param name="fillercount" select="1"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<!-- End case of more than one page-->
					<xsl:otherwise>
						<xsl:call-template name="Filler">
							<!--(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)-->
							<xsl:with-param name="fillercount"
							                select="43 -((9*position())+(count(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')] [(booking_segment_id = (//Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']/booking_segment_id))])) ) mod 43"/>
							<!--<xsl:with-param name="fillercount" select="0"/>-->
						</xsl:call-template>
						<!--<xsl:value-of select="(39 - (6+(count((/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)])) div 2)) ) mod 30"/>-->
						<!--				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="1"/>
				</xsl:call-template>-->
						<!--Count2=<xsl:value-of select="position()"/>-->
						<xsl:copy-of select="$ReportFooter"/>b<br class="pagebreak"/>
						<!--Fix Header Section-->
						<xsl:copy-of select="$ReportHeader"/>
						<br/>
						<xsl:copy-of select="$ReportHeader1"/>
						<br/>
						<span class="tableheader">Flights</span>
						<!--<span class="tableheader">Passengers</span>
				<xsl:copy-of select="$FlightRowsHeader"/>-->
						<!--End Fix Header Section-->
					</xsl:otherwise>
				</xsl:choose>
			</table>
		</xsl:for-each>
	</xsl:template>
	<xsl:variable name="FlightRowsHeader">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th style="width: 25px">&#xA0;</th>
				<th align="left" style="width: 50px">Flight</th>
				<th align="left">From</th>
				<th align="left">To</th>
				<th style="width: 90px">Date</th>
				<th style="width: 50px">Dep</th>
				<th style="width: 50px">Arr</th>
				<!--<th>Class</th>-->
				<th style="width: 90px">Status</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<!--End Flights Section-->



	<!--Header PaymentReceipt-->
	<xsl:template match="Booking/Header/BookingHeader/booking_id">
		<!-- variable Header-->
		<br/>
		<!--		<xsl:variable name="BarURL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
		<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>-->
		<table style="width: 100%" cellspacing="0">
			<tr>
				<td>
					<table class="tabledetails" cellspacing="0">
						<tr>
							<td class="tdmargin"/>
							<td>Bregenz (Austria)&#xA0;<span class="tableheader" style="padding-left: 0px;">
								<xsl:if test="string-length(/Booking/Header/BookingHeader/create_date_time) &gt; '0'">
									<xsl:call-template name="dt:formatdate">
										<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
									</xsl:call-template>
								</xsl:if>							
							</span>
							</td> 
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>Invoice-Nbr:&#xA0;								<span class="tableheader" style="padding-left: 0px;">
									<xsl:value-of select="/Booking/Header/BookingHeader/invoice_receiver"/>&#xA0;</span></td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>Reservation-Nbr:&#xA0;								<span class="tableheader" style="padding-left: 0px;">
									<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>&#xA0;</span>
							</td>
							<td class="tdmargin"/>
						</tr>
					</table>
					<!--<br/>-->
					<table class="tabledetails" cellspacing="0">
						<tr>
							<td class="tdmargin"/>
							<td class="documentheader" style="width: 120px">Recipient&#xA0;</td>
							<td class="documentheader" style="width: 120px">Agent/Broker&#xA0;</td>
							<td class="documentheader" style="width: 120px">Sender/Airline&#xA0;</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td rowspan="5">

								<font color="#118A56">
									<xsl:choose>
										<xsl:when test="string-length(//Booking/Header/BookingHeader/client_profile_id) &gt; '0'">
											<xsl:choose>
												<xsl:when test="(string-length(//Booking/Header/BookingHeader/client_contact_name) &gt; '0') and not (string-length(//Booking/Header/BookingHeader/lastname) &gt; '0')">
													<!--<xsl:text>CASE A - </xsl:text>-->
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_number"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_contact_name"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_firstname"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_lastname"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_address_line1"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_address_line2"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_po_box"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_street"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_state"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_district"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_province"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_city"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_zip_code"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_country_rcd"/>
													</xsl:call-template>
												</xsl:when>

												<xsl:when test="string-length(//Booking/Header/BookingHeader/lastname) &gt; '0'">
													<!--<xsl:text>CASE B - </xsl:text>-->
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_number"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/title_rcd"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/firstname"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/lastname"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/address_line1"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/address_line2"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/po_box"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/street"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/state"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/district"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/province"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/city"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/zip_code"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/country_rcd"/>
													</xsl:call-template>
												</xsl:when>

												<xsl:otherwise>
													<!--<xsl:text>CASE C - </xsl:text>-->
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_number"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_firstname"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_lastname"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_address_line1"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_address_line2"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_po_box"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_street"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_state"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_district"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_province"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_city"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_zip_code"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_country_rcd"/>
													</xsl:call-template>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise>
											<!--<xsl:text>CASE D - </xsl:text>-->
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/title_rcd"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/lastname"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/firstname"/>
											</xsl:call-template>
											<br/>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/address_line1"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/address_line2"/>
											</xsl:call-template>
											<br/>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/po_box"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/street"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/state"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/district"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/province"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/city"/>
											</xsl:call-template>
											<br/>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/zip_code"/>
											</xsl:call-template>
											<xsl:call-template name="AddrFormat">
												<xsl:with-param name="value" select="//Booking/Header/BookingHeader/country_rcd"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</font>
							</td>
							<td rowspan="5">


								<font color="#118A56">

													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/agency_name"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_firstname"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_lastname"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_address_line1"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_address_line2"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_po_box"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_street"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_state"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_district"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_province"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_city"/>
													</xsl:call-template>
													<br/>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_zip_code"/>
													</xsl:call-template>
													<xsl:call-template name="AddrFormat">
														<xsl:with-param name="value" select="//Booking/Header/BookingHeader/client_country_rcd"/>
													</xsl:call-template>

								</font>
							</td>
							<td>InterSky Luftfahrt GmbH</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>Bahnhofstrasse 10</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>6900 Bregenz</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>Austria</td>
							<td class="tdmargin"/>
						</tr>
						<tr>
							<td class="tdmargin"/>
							<td>VAT Nbr.:ATU 53122909</td>
							<td class="tdmargin"/>
						</tr>
					</table>
				</td>
				<td style="font-size: 9pt;" align="right" width="164">
					<!--<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="164" height="57"/>-->InterSky Call-Center <br/>Intl: +43 5574 48800 46 <br/>DE: +49 7541 286 96 84 <br/>AT: +43 5574 48800 46 <br/>CH: +41 31 819 72 22 <br/>
					<a href="mailto:reservation@intersky.biz">reservation@intersky.biz</a>
					<br/>Mo - Fr: 08:00 - 18:00 Uhr</td>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:template>
	<!--End Header PaymentReceipt-->


	<!--PaymentReceipt Section-->
	<xsl:template match="Booking/Tickets">
		<!--Ticket-->
		<!--<span class="tableheader">Fare Details</span>-->
		<xsl:copy-of select="$TicketRowsHeader"/>
		<xsl:for-each select="//Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
			<xsl:variable name="FlightID" select="flight_id"/>
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
			<xsl:variable name="strOri" select="origin_name"/>
			<xsl:variable name="strDesi" select="destination_name"/>FlightSegment<xsl:value-of select="position()"/><xsl:value-of select="$booking_segment_id"/> 

			<table class="tabledetails" cellspacing="1" style="table-layout:fixed">
				<!--				<xsl:sort select="seat_number" order="descending"/>
				<xsl:sort select="ticket_number" order="descending"/>
				<xsl:sort select="restriction_text" order="descending"/>
				<xsl:sort select="endorsement_text" order="descending"/>-->
				<!--TicketByPassenger<xsl:value-of select="position()"/>-->
				<table class="tabledetails" cellspacing="1" style="table-layout:fixed">

					<!--<xsl:variable name="h" select="round(count(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]) div 2)"/>-->
					<xsl:for-each select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][(flight_id = $FlightID)]">
						<xsl:variable name="p" select="position()"/>
						<!--<xsl:variable name="p2" select="($p+$h)"/>-->
						<!--<xsl:if test="$h&gt;=$p">-->
						
						<TR>
							<td class="tdmargin" style="width: 10px" />
							<TD align="left" style="width:245px; white-space: nowrap;">
								<xsl:value-of select="$strOri"/>&#xA0;(<xsl:value-of select="origin_rcd"/>) 	to <xsl:value-of select="$strDesi"/>&#xA0;(<xsl:value-of select="destination_rcd"/>)</TD>
							<TD align="left" style="width: 50px; white-space: nowrap;">
								<xsl:value-of select="airline_rcd"/>
								<xsl:value-of select="flight_number"/>
							</TD>
							<td align="left" style="width: 60px; white-space: nowrap;">
								<xsl:call-template name="dt:formatdate">
									<xsl:with-param name="date" select="departure_date"/>
								</xsl:call-template>
							</td>
							<td align="left" style="width: 160px; white-space: nowrap;">
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="lastname"/>
								</xsl:call-template>
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="firstname"/>
								</xsl:call-template>
								<xsl:call-template name="AddrFormat">
									<xsl:with-param name="value" select="title_rcd"/>
								</xsl:call-template>
								<br/>
								<xsl:value-of select="passenger_id"/> 
							</td>
							<td align="left" style="width: 75px; white-space: nowrap;">
								<xsl:value-of select="fare_type_rcd"/>
							</td>
							<td align="right" style="width: 75px; white-space: nowrap;">
								<xsl:if test="number(tax_amount) != 0">
									<xsl:if test="number(tax_amount) &gt;= 0">
										<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
									</xsl:if>
								</xsl:if>
							</td>
							<td align="right" style="width: 75px; white-space: nowrap;">
								<xsl:if test="number(net_total) != 0">
									<xsl:if test="number(net_total) &gt;= 0">
										<xsl:value-of select="format-number(net_total,'###,##0.00')"/>
									</xsl:if>
								</xsl:if>
							</td>
							<td class="tdmargin" style="width: 10px" />
						</TR>
						<!--</xsl:if>-->
					</xsl:for-each>
				</table>
				<!--					<TR>
						<td class="tdmargin"/>
						<td style="width: 50px" align="left" valign="top" class="blueline">
							<xsl:value-of select="airline_rcd"/>
							<xsl:value-of select="flight_number"/>&#xA0;</td>
						<td align="left" valign="top" class="blueline">
							<xsl:choose>
								<xsl:when test="not(string-length(lastname) &gt; '0')">
									<xsl:text>&#xA0;</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="lastname"/>&#xA0;/&#xA0;<xsl:value-of select="firstname"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td align="left" valign="top" class="blueline" style="width: 100px">
							<xsl:value-of select="ticket_number"/>&#xA0;</td>
						<td align="left" valign="top">
																			<xsl:value-of select="seat_number"/>
																		</td>
						<td align="left" valign="top" class="blueline">
							<xsl:choose>
								<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) &gt;0)">
									<xsl:value-of select="endorsement_text"/>
									<br/>
									<xsl:value-of select="restriction_text"/>
								</xsl:when>
								<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) =0)">
									<xsl:value-of select="restriction_text"/>
								</xsl:when>
								<xsl:when test="(string-length(restriction_text) =0) and (string-length(endorsement_text) &gt;0)">
									<xsl:value-of select="endorsement_text"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="restriction_text"/>
								</xsl:otherwise>
							</xsl:choose>&#xA0;
							<td class="tdmargin"/>
						</td>
					</TR>-->
			</table>
			<xsl:if test="(position() mod 35) = 0 ">35 rows per page
				<br class="pagebreak"/>
				<xsl:copy-of select="$ReportFooter"/>
				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="1"/>
				</xsl:call-template>
				<xsl:copy-of select="$TicketRowsHeader"/>
			</xsl:if>
			<!--Filler -->
			<xsl:choose>
				<!-- case of only one page-->
				<xsl:when test="count(/Booking/Passengers/Passenger) &lt;= 40">
					<xsl:call-template name="Filler">
						<!--<xsl:with-param name="fillercount" select="40 - (count(/Booking/Passengers/Passenger))"/>-->
						<xsl:with-param name="fillercount" select="0"/>
					</xsl:call-template>
				</xsl:when>
				<!-- case of more than one page-->
				<xsl:otherwise>
					<xsl:call-template name="Filler">
						<!--(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)-->
						<!--<xsl:with-param name="fillercount" select="40 - ( ( count(/Booking/Passengers/Passenger)-40 ) mod 40 ) - 3 + 1"/>-->
						<xsl:with-param name="fillercount" select="0"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			<!--End Filler -->
			<!--End Passengers-->
		</xsl:for-each>
	</xsl:template>


	<xsl:variable name="TicketRowsHeaderA1">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th align="left" style="width: 50px">Flight</th>
				<th align="left">Passenger Name</th>
				<th align="left" style="width: 100px">Ticket</th>
				<th align="left">Endorsement &amp; Restrictions</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<!--End PaymentReceipt Section-->




	<!--Auxiliaries Section-->
	<xsl:template match="Booking/Remarks">
		<span class="tableheader">Auxiliaries</span>
		<xsl:copy-of select="$RemarkRowsHeader"/>
		<xsl:for-each select="/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
			<table class="tabledetails" cellspacing="1" style="table-layout:fixed">
				<TR>
					<td class="tdmargin"/>
					<TD align="left" class="blueline">
						<xsl:value-of select="display_name"/>
						<xsl:text>&#xA0;</xsl:text>
					</TD>
					<TD align="left" class="blueline">
						<xsl:value-of select="remark_text"/>
						<xsl:text>&#xA0;</xsl:text>
					</TD>
					<td class="tdmargin"/>
				</TR>
			</table>
			<xsl:if test="(position() mod 20) = 0 ">
				<xsl:value-of select="position()"/>
				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="1"/>
				</xsl:call-template>
				<xsl:value-of select="position()"/>
				<xsl:copy-of select="$ReportFooter"/>
				<br class="pagebreak"/>
				<span class="tableheader">Auxiliaries</span>
				<xsl:copy-of select="$RemarkRowsHeader"/>
			</xsl:if>
		</xsl:for-each>
		<!--FlightFiller -->
		<xsl:choose>
			<!-- case of only one page-->
			<xsl:when test="count(/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX'])  &lt;= 47">
				<xsl:call-template name="Filler">
					<!--<xsl:with-param name="fillercount" select="47 - (count(/Booking/Passengers/Passenger))"/>-->
					<xsl:with-param name="fillercount" select="0"/>
				</xsl:call-template>
			</xsl:when>
			<!-- case of more than one page-->
			<xsl:otherwise>
				<xsl:call-template name="Filler">
					<!--(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)-->
					<!--<xsl:with-param name="fillercount" select="20 - ( ( count(/Booking/Passengers/Passenger)-20 ) mod 20 ) - 3 + 1"/>-->
					<xsl:with-param name="fillercount" select="0"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<!--End Filler -->
		<!--End Auxiliaries-->
	</xsl:template>
	<xsl:variable name="RemarkRowsHeader">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th align="left">Code</th>
				<th align="left">Description</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<!--End Auxiliaries Section-->


	<!--Special Services Section-->
	<xsl:template match="Booking/SpecialServices">
		<span class="tableheader">Special Services</span>
		<xsl:copy-of select="$ServicesRowsHeader"/>
		<table class="tabledetails" cellspacing="1" style="table-layout:fixed">
			<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')]">
				<xsl:sort select="departure_date" order="descending"/>
				<xsl:if test="position()=1">
					<TR>
						<td class="tdmargin"/>
						<td class="blueline" style="width: 25px">
							<xsl:value-of select="concat('00','',string(position()))"/>&#xA0;</td>
						<td class="blueline" style="width: 60px">
							<xsl:value-of select="airline_rcd"/>&#xA0;<xsl:value-of select="flight_number"/></td>
						<td class="blueline">
							<xsl:value-of select="lastname"/>/&#xA0;<xsl:value-of select="firstname"/></td>
						<td class="blueline" align="center" style="width: 60px">
							<xsl:value-of select="special_service_status_rcd"/>
						</td>
						<td class="blueline">
							<xsl:value-of select="special_service_rcd"/>
							<xsl:text>&#xA0;</xsl:text>
							<xsl:value-of select="display_name"/>
						</td>
						<td class="blueline">
							<xsl:value-of select="service_text"/>&#xA0;</td>
						<td class="tdmargin"/>
					</TR>
				</xsl:if>
			</xsl:for-each>
		</table>
		<xsl:if test="(position() mod 35) = 0 ">35 rows per page
			<br class="pagebreak"/>
			<xsl:copy-of select="$ReportFooter"/>
			<xsl:call-template name="Filler">
				<xsl:with-param name="fillercount" select="1"/>
			</xsl:call-template>
			<xsl:copy-of select="$ServicesRowsHeader"/>
		</xsl:if>
		<!--Filler -->
		<xsl:choose>
			<!-- case of only one page-->
			<xsl:when test="count(/Booking/Passengers/Passenger) &lt;= 40">
				<xsl:call-template name="Filler">
					<!--<xsl:with-param name="fillercount" select="40 - (count(/Booking/Passengers/Passenger))"/>-->
					<xsl:with-param name="fillercount" select="0"/>
				</xsl:call-template>
			</xsl:when>
			<!-- case of more than one page-->
			<xsl:otherwise>
				<xsl:call-template name="Filler">
					<!--(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)-->
					<!--<xsl:with-param name="fillercount" select="40 - ( ( count(/Booking/Passengers/Passenger)-40 ) mod 40 ) - 3 + 1"/>-->
					<xsl:with-param name="fillercount" select="0"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<!--End Filler -->
		<!--End Passengers-->
	</xsl:template>
	<xsl:variable name="ServicesRowsHeader">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th style="width: 25px">&#xA0;</th>
				<th align="left" style="width: 60px">Flight</th>
				<th align="left">Passenger Name</th>
				<th style="width: 60px">Status</th>
				<th align="left">Special Service</th>
				<th>Text</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<!--End Special Services-->


	<!--	do Not modify this part-->
	<!--Ticket Section-->
	<!--	<xsl:template match="Booking/Tickets">
		Ticket
				<span class="tableheader">Tickets</span>
		<xsl:copy-of select="$TicketRowsHeader"/>
		<table class="tabledetails" cellspacing="1" style="table-layout:fixed">
			<xsl:for-each select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
				<xsl:sort select="seat_number" order="descending"/>
				<xsl:sort select="ticket_number" order="descending"/>
				<xsl:sort select="restriction_text" order="descending"/>
				<xsl:sort select="endorsement_text" order="descending"/>
				<xsl:if test="(string-length(seat_number) &gt;0) or (string-length(ticket_number) &gt;0) or (string-length(restriction_text) &gt;0) or (string-length(endorsement_text) &gt;0)">
					<table class="tabledetails" cellspacing="1" style="table-layout:fixed">
						<xsl:variable name="h" select="round(count(/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]) div 2)"/>
						<xsl:for-each select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
							<xsl:variable name="p" select="position()"/>
							<xsl:variable name="p2" select="($p+$h)"/>
							<xsl:if test="$h&gt;=$p">
								<TR>
									<td class="tdmargin"/>
									<TD>
										<xsl:value-of select="$p"/>. Passenger Name:
										<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p]/passenger_id"/>
										<xsl:choose>
											<xsl:when test="not(string-length(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p]/lastname) &gt; '0')">&#xA0;<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p]/title_rcd"/>
									&#xA0;<xsl:value-of select="seat_number"/>
												<xsl:text>&#xA0;</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p]/lastname"/>&#xA0;/&#xA0;<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p]/firstname"/>
									&#xA0;<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p]/title_rcd"/>
									&#xA0;<xsl:value-of select="seat_number"/>
											</xsl:otherwise>
										</xsl:choose>
									</TD>
									<TD>
										<xsl:value-of select="$p2"/>. Passenger Name:
										<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p2]/passenger_id"/>
										<xsl:choose>
											<xsl:when test="not(string-length(//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p2]/lastname) &gt; '0')">&#xA0;<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p2]/title_rcd"/>
									&#xA0;<xsl:value-of select="seat_number"/>
												<xsl:text>&#xA0;</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p2]/lastname"/>&#xA0;/&#xA0;<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p2]/firstname"/>
									&#xA0;<xsl:value-of select="//Booking/Tickets/Ticket[not(passenger_status_rcd='XX')][position()=$p2]/title_rcd"/>
									&#xA0;<xsl:value-of select="seat_number"/>
											</xsl:otherwise>
										</xsl:choose>
									</TD>
								</TR>
							</xsl:if>
						</xsl:for-each>
					</table>
										<TR>
						<td class="tdmargin"/>
						<td style="width: 50px" align="left" valign="top" class="blueline">
							<xsl:value-of select="airline_rcd"/>
							<xsl:value-of select="flight_number"/>&#xA0;</td>
						<td align="left" valign="top" class="blueline">
							<xsl:choose>
								<xsl:when test="not(string-length(lastname) &gt; '0')">
									<xsl:text>&#xA0;</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="lastname"/>&#xA0;/&#xA0;<xsl:value-of select="firstname"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td align="left" valign="top" class="blueline" style="width: 100px">
							<xsl:value-of select="ticket_number"/>&#xA0;</td>
						<td align="left" valign="top">
																			<xsl:value-of select="seat_number"/>
																		</td>
						<td align="left" valign="top" class="blueline">
							<xsl:choose>
								<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) &gt;0)">
									<xsl:value-of select="endorsement_text"/>
									<br/>
									<xsl:value-of select="restriction_text"/>
								</xsl:when>
								<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) =0)">
									<xsl:value-of select="restriction_text"/>
								</xsl:when>
								<xsl:when test="(string-length(restriction_text) =0) and (string-length(endorsement_text) &gt;0)">
									<xsl:value-of select="endorsement_text"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="restriction_text"/>
								</xsl:otherwise>
							</xsl:choose>&#xA0;
							<td class="tdmargin"/>
						</td>
					</TR>
				</xsl:if>
			</xsl:for-each>
		</table>
		<xsl:if test="(position() mod 35) = 0 ">35 rows per page
			<br class="pagebreak"/>
			<xsl:copy-of select="$ReportFooter"/>
			<xsl:call-template name="Filler">
				<xsl:with-param name="fillercount" select="1"/>
			</xsl:call-template>
			<xsl:copy-of select="$TicketRowsHeader"/>
		</xsl:if>
		Filler 
		<xsl:choose>
			 case of only one page
			<xsl:when test="count(/Booking/Passengers/Passenger) &lt;= 40">
				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="40 - (count(/Booking/Passengers/Passenger))"/>
					<xsl:with-param name="fillercount" select="0"/>
				</xsl:call-template>
			</xsl:when>
			 case of more than one page
			<xsl:otherwise>
				<xsl:call-template name="Filler">
					(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)
					<xsl:with-param name="fillercount" select="40 - ( ( count(/Booking/Passengers/Passenger)-40 ) mod 40 ) - 3 + 1"/>
					<xsl:with-param name="fillercount" select="0"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		End Filler 
		End Passengers
	</xsl:template>
	<xsl:variable name="TicketRowsHeader">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th align="left" style="width: 50px">Flight</th>
				<th align="left">Passenger Name</th>
				<th align="left" style="width: 100px">Ticket</th>
				<th align="left">Endorsement &amp; Restrictions</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>-->
	<!--End Ticket Section-->


	<!--	do Not modify this part-->
	<!--Quote Section-->
	<xsl:key name="passenger_type_rcd_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<xsl:template match="Booking/TicketQuotes">
		<xsl:if test="position()=1">
			<span class="tableheader">Quotes</span>
			<xsl:copy-of select="$QuoteRowsHeader"/>
			<xsl:if test="//TicketQuotes/Total = true()">
				<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
					<xsl:for-each select="//Passengers/Passenger[count(. | key('passenger_type_rcd_group', passenger_type_rcd)[1]) = 1]">
						<xsl:variable name="passenger_type_rcd" select="passenger_type_rcd"/>
						<xsl:for-each select="//TicketQuotes/Total[passenger_type_rcd=$passenger_type_rcd]">
							<xsl:variable name="TotalCharge">
								<xsl:if test="charge_type != 'REFUND'">
									<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
								</xsl:if>
								<xsl:if test="charge_type = 'REFUND'">
									<xsl:value-of select="sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
								</xsl:if>
							</xsl:variable>
							<xsl:if test="position()=1">
								<xsl:if test="position()!=last()">
									<tr>
										<td class="tdmargin"/>
										<td class="bluelinetop" style="width: 75px">
											<xsl:value-of select="passenger_type_rcd"/>
										</td>
										<td class="bluelinetop" align="center" style="width: 40px">
											<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)"/>
										</td>
										<td class="bluelinetop" style="width: 220px">
											<xsl:value-of select="charge_name"/>
										</td>
										<td class="bluelinetop" align="right">
											<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
										</td>
										<td class="bluelinetop" align="right" style="width: 75px">
											<xsl:if test="number(tax_amount) != 0">
												<xsl:if test="number(tax_amount) &gt;= 0">
													<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
												</xsl:if>
											</xsl:if>&#xA0;</td>
										<td class="bluelinetop" align="right">
											<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>
											<xsl:text>&#x20;</xsl:text>
											<xsl:value-of select="currency_rcd"/>
										</td>
										<td class="tdmargin"/>
									</tr>
								</xsl:if>
								<xsl:if test="position()=last()">
									<tr>
										<td class="tdmargin"/>
										<td class="blankline" style="width: 75px">
											<xsl:value-of select="passenger_type_rcd"/>
										</td>
										<td align="center" class="blankline" style="width: 40px">
											<xsl:value-of select="passenger_count"/>
										</td>
										<td class="blankline" style="width: 220px">
											<xsl:value-of select="charge_name"/>
										</td>
										<td align="right" class="blankline">
											<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
										</td>
										<td align="right" class="blankline" style="width: 75px">
											<xsl:if test="number(tax_amount) != 0">
												<xsl:if test="number(tax_amount) &gt;= 0">
													<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
												</xsl:if>
											</xsl:if>&#xA0;</td>
										<td align="right" class="blankline">
											<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>
											<xsl:text>&#x20;</xsl:text>
											<xsl:value-of select="currency_rcd"/>
										</td>
										<td class="tdmargin"/>
									</tr>
								</xsl:if>
							</xsl:if>
							<xsl:if test="position()!=1">
								<xsl:if test="position()=last()">
									<xsl:if test="charge_type != 'REFUND'">
										<tr>
											<td class="tdmargin"/>
											<td class="blankline">&#xA0;</td>
											<td class="blankline">&#xA0;</td>
											<td class="blankline">
												<xsl:value-of select="charge_name"/>
											</td>
											<td class="blankline" align="right">
												<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
											</td>
											<td class="blankline" align="right">
												<xsl:if test="number(tax_amount) != 0">
													<xsl:if test="number(tax_amount) &gt;= 0">
														<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
													</xsl:if>
												</xsl:if>&#xA0;</td>
											<td class="blankline">&#xA0;</td>
										</tr>
									</xsl:if>
									<xsl:if test="charge_type = 'REFUND'">
										<tr>
											<td>&#xA0;</td>
											<td align="center">&#xA0;</td>
											<td>
												<xsl:value-of select="charge_name"/>
											</td>
											<td align="right">&#xA0;</td>
											<td align="right">&#xA0;</td>
											<td align="right">-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:value-of select="currency_rcd"/>
											</td>
										</tr>
									</xsl:if>
								</xsl:if>
								<xsl:if test="position()!=last()">
									<xsl:if test="charge_type != 'REFUND'">
										<tr>
											<td class="tdmargin"/>
											<td class="blankline" style="width: 90px">&#xA0;</td>
											<td class="blankline">&#xA0;</td>
											<td class="blankline">
												<xsl:value-of select="charge_name"/>
											</td>
											<td class="blankline" align="right">
												<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
											</td>
											<td class="blankline" align="right">
												<xsl:if test="number(tax_amount) != 0">
													<xsl:if test="number(tax_amount) &gt;= 0">
														<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
													</xsl:if>
												</xsl:if>&#xA0;</td>
											<td class="blankline">&#xA0;</td>
											<td class="tdmargin"/>
										</tr>
									</xsl:if>
									<xsl:if test="charge_type = 'REFUND'">
										<tr>
											<td class="tdmargin"/>
											<td>&#xA0;</td>
											<td align="center">&#xA0;</td>
											<td>
												<xsl:value-of select="charge_name"/>
											</td>
											<td align="right">&#xA0;</td>
											<td align="right">&#xA0;</td>
											<td align="right">-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:value-of select="currency_rcd"/>
											</td>
											<td class="tdmargin"/>
										</tr>
									</xsl:if>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
					</xsl:for-each>
					<xsl:for-each select="//Fees/Fee[void_date_time = '']">
						<xsl:if test="position()=1">
							<tr>
								<td class="tdmargin"/>
								<td class="blankline">&#xA0;</td>
								<td class="blankline">&#xA0;</td>
								<td class="blankline">
									<xsl:value-of select="display_name"/>
								</td>
								<td class="blankline" align="right">
									<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
								</td>
								<td class="blankline" align="right">
									<xsl:choose>
										<xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">&#xA0;</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>&#xA0;</xsl:otherwise>
									</xsl:choose>
								</td>
								<td class="blankline" align="right">
									<xsl:value-of select="format-number(sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/>
									<xsl:text>&#xA0;</xsl:text>
									<xsl:value-of select="currency_rcd"/>
								</td>
								<td class="tdmargin"/>
							</tr>
						</xsl:if>
						<xsl:if test="position()!=1">
							<tr>
								<td class="tdmargin"/>
								<td class="blankline">&#xA0;</td>
								<td class="blankline">&#xA0;</td>
								<td class="blankline">
									<xsl:value-of select="display_name"/>
								</td>
								<td class="blankline" align="right">
									<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
								</td>
								<td class="blankline" align="right">
									<xsl:choose>
										<xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">&#xA0;</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
								<td class="blankline">&#xA0;</td>
								<td class="tdmargin"/>
							</tr>
						</xsl:if>
					</xsl:for-each>
					<tr>
						<td class="tdmargin"/>
						<td class="bluelinetotal">&#xA0;</td>
						<td class="bluelinetotal">&#xA0;</td>
						<td class="bluelinetotal">&#xA0;</td>
						<td class="bluelinetotal">&#xA0;</td>
						<td class="bluelinetotal" align="right">
							<span>Total</span>
						</td>
						<td class="bluelinetotal" align="right">
							<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/>
							<xsl:text>&#xA0;</xsl:text>
							<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
						</td>
						<td class="tdmargin"/>
					</tr>
				</table>
			</xsl:if>
			<xsl:if test="(position() mod 35) = 0 ">35 rows per page
				<br class="pagebreak"/>
				<xsl:copy-of select="$ReportFooter"/>
				<xsl:call-template name="Filler">
					<xsl:with-param name="fillercount" select="1"/>
				</xsl:call-template>
				<xsl:copy-of select="$PassengerRowsHeader"/>
			</xsl:if>
			<!--Filler -->
			<xsl:choose>
				<!-- case of only one page-->
				<xsl:when test="count(/Booking/Passengers/Passenger) &lt;= 40">
					<xsl:call-template name="Filler">
						<!--<xsl:with-param name="fillercount" select="40 - (count(/Booking/Passengers/Passenger))"/>-->
						<xsl:with-param name="fillercount" select="0"/>
					</xsl:call-template>
				</xsl:when>
				<!-- case of more than one page-->
				<xsl:otherwise>
					<xsl:call-template name="Filler">
						<!--(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)-->
						<!--<xsl:with-param name="fillercount" select="40 - ( ( count(/Booking/Passengers/Passenger)-40 ) mod 40 ) - 3 + 1"/>-->
						<xsl:with-param name="fillercount" select="0"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			<!--End Filler -->
			<!--End Quotes-->
		</xsl:if>
	</xsl:template>
	<xsl:variable name="QuoteRowsHeader">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th align="left" style="width: 75px">Passenger</th>
				<th style="width: 40px" align="center">Units</th>
				<th align="left" style="width: 220px">Charge</th>
				<th align="right">Amount</th>
				<th style="width: 75px" align="right">FET</th>
				<th align="right">Total</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<!--End Quote Section-->


	<!--Payment Section-->
	<xsl:template match="Booking/Payments">
		<span class="tableheader">Payments</span>
		<xsl:copy-of select="$PaymentRowsHeader"/>
		<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
		<xsl:variable name="Ticket_total"
		              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>
		<table class="tabledetails" cellspacing="1" style="table-layout:fixed">
			<xsl:choose>
				<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">
					<tr>
						<td class="tdmargin"/>
						<TD align="left" class="blueline" style="width: 200px">Ticket Cost &amp; Fee</TD>
						<TD align="left" class="blueline">&#xA0;</TD>
						<TD align="left" class="blueline" style="width: 90px">&#xA0;</TD>
						<xsl:if test="starts-with(string($Ticket_total), '-')">
							<TD align="left" class="blueline">&#xA0;</TD>
							<TD align="right" class="blueline">
								<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
						</xsl:if>
						<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
							<TD align="right" class="blueline">
								<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
							<TD align="left" class="blueline">&#xA0;</TD>
						</xsl:if>
						<td class="tdmargin"/>
					</tr>
					<xsl:for-each select="/Booking/Payments/Payment[void_date_time='']">
						<xsl:if test="form_of_payment_rcd !=''">
							<TR>
								<td class="tdmargin"/>
								<TD align="left" class="blueline">
									<xsl:if test="form_of_payment_rcd ='CASH'">
										<xsl:value-of select="form_of_payment_rcd"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='VOUCHER'">
										<xsl:value-of select="form_of_payment_subtype"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CC'">
										<xsl:value-of select="form_of_payment_subtype"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='TKT'">
										<xsl:value-of select="form_of_payment"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CRAGT'">
										<xsl:value-of select="form_of_payment"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='MANUAL'">
										<xsl:value-of select="form_of_payment_subtype"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CHEQUE'">
										<xsl:value-of select="form_of_payment_subtype"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='BANK'">
										<xsl:value-of select="form_of_payment"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='INV'">
										<xsl:value-of select="form_of_payment"/>
									</xsl:if>&#xA0;</TD>
								<TD align="left" class="blueline">
									<xsl:if test="form_of_payment_rcd ='INV'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='BANK'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CHEQUE'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='MANUAL'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='VOUCHER'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CC'">
										<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
									</xsl:if>&#xA0;</TD>
								<TD align="left" class="blueline">&#xA0;
									<xsl:choose>
										<xsl:when test="void_date_time != ''">
											<xsl:text>XX</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="payment_date_time != ''">
												<xsl:call-template name="dt:formatdate">
													<xsl:with-param name="date" select="payment_date_time"/>
												</xsl:call-template>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</TD>
								<TD align="right" class="blueline">
									<xsl:choose>
										<xsl:when test="not(payment_amount &gt;= 0)">
											<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>&#xA0;</TD>
								<TD align="right" class="blueline">
									<xsl:choose>
										<xsl:when test="payment_amount &gt; 0">
											<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;	<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</TD>
								<td class="tdmargin"/>
							</TR>
						</xsl:if>
					</xsl:for-each>
					<tr>
						<td class="tdmargin"/>
						<TD align="left" class="blueline">OUTSTANDING BALANCE</TD>
						<TD align="left" class="blueline">&#xA0;</TD>
						<TD align="left" class="blueline">&#xA0;</TD>
						<xsl:variable name="SubTotal" select="$Ticket_total - $Payment_total"/>
						<xsl:if test="number($SubTotal) = 0">
							<TD align="left" class="blueline">&#xA0;</TD>
							<TD align="right" class="blueline">0.00&#xA0;&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
						</xsl:if>
						<xsl:if test="number($SubTotal) != 0">
							<xsl:if test="number($SubTotal) &gt;= 0">
								<TD align="left" class="blueline">&#xA0;</TD>
								<TD align="right" class="blueline">
									<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
							</xsl:if>
							<xsl:if test="number($SubTotal) &lt; 0">
								<TD align="right" class="blueline">
									<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
								<TD align="left" class="blueline">&#xA0;</TD>
							</xsl:if>
						</xsl:if>
						<td class="tdmargin"/>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<TR>
						<td class="tdmargin"/>
						<TD align="left" class="blueline" style="width: 200px">Ticket Cost &amp; Fee</TD>
						<TD align="left" class="blueline">&#xA0;</TD>
						<TD align="left" class="blueline" style="width: 90px">&#xA0;</TD>
						<xsl:if test="starts-with(string($Ticket_total), '-')">
							<TD align="left" class="blueline">&#xA0;</TD>
							<TD align="right" class="blueline">
								<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
						</xsl:if>
						<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
							<TD align="right" class="blueline">
								<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
							<TD align="left" class="blueline">&#xA0;</TD>
						</xsl:if>
						<td class="tdmargin"/>
					</TR>
					<xsl:for-each select="Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)]">
						<xsl:if test="form_of_payment_subtype !=''">
							<TR>
								<td class="tdmargin"/>
								<TD align="left" class="blueline">
									<xsl:value-of select="form_of_payment_subtype"/>
								</TD>
								<TD align="left" class="blueline">
									<xsl:if test="form_of_payment_rcd ='INV'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='BANK'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CHEQUE'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='MANUAL'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='VOUCHER'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CC'">
										<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
									</xsl:if>
								</TD>
								<TD align="left" class="blueline">
									<xsl:choose>
										<xsl:when test="void_date_time != ''">
											<xsl:text>XX</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="payment_date_time != ''">
												<xsl:call-template name="dt:formatdate">
													<xsl:with-param name="date" select="payment_date_time"/>
												</xsl:call-template>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</TD>
								<TD align="right" class="blueline">
									<xsl:choose>
										<xsl:when test="not(payment_amount &gt;= 0)">
											<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</TD>
								<TD align="right" class="blueline">
									<xsl:choose>
										<xsl:when test="payment_amount &gt; 0">
											<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</TD>
								<td class="tdmargin"/>
							</TR>
						</xsl:if>
						<xsl:if test="form_of_payment_subtype =''">
							<TR>
								<td class="tdmargin"/>
								<TD align="left" class="blueline">
									<xsl:value-of select="form_of_payment_rcd"/>
								</TD>
								<TD align="left" class="blueline">
									<xsl:if test="form_of_payment_rcd ='INV'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='BANK'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CHEQUE'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='MANUAL'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='VOUCHER'">
										<xsl:value-of select="document_number"/>
									</xsl:if>
									<xsl:if test="form_of_payment_rcd ='CC'">
										<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
									</xsl:if>
								</TD>
								<TD align="left" class="blueline">
									<xsl:if test="payment_date_time != ''">
										<xsl:call-template name="dt:formatdate">
											<xsl:with-param name="date" select="payment_date_time"/>
										</xsl:call-template>
									</xsl:if>
								</TD>
								<TD align="right" class="blueline">
									<xsl:choose>
										<xsl:when test="not(payment_amount &gt;= 0)">
											<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</TD>
								<TD align="right" class="blueline">
									<xsl:choose>
										<xsl:when test="payment_amount &gt; 0">
											<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</TD>
								<td class="tdmargin"/>
							</TR>
						</xsl:if>
					</xsl:for-each>
					<TR>
						<td class="tdmargin"/>
						<TD align="left" class="blueline" style="width: 200px">OUTSTANDING BALANCE</TD>
						<TD align="left" class="blueline">&#xA0;</TD>
						<TD align="left" class="blueline">&#xA0;</TD>
						<xsl:variable name="SubTotal" select="number($Ticket_total) - number($Payment_total)"/>
						<xsl:if test="number($SubTotal) = 0">
							<TD align="left" class="blueline">&#xA0;</TD>\
							<TD align="right" class="blueline">0.00&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
						</xsl:if>
						<xsl:if test="number($SubTotal) != 0">
							<xsl:if test="number($SubTotal) &gt;= 0">
								<TD align="left" class="blueline">&#xA0;</TD>
								<TD align="right" class="blueline">
									<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
							</xsl:if>
							<xsl:if test="number($SubTotal) &lt; 0">
								<TD align="right" class="blueline">
									<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></TD>
								<TD align="left" class="blueline">&#xA0;</TD>
							</xsl:if>
						</xsl:if>
						<td class="tdmargin"/>
					</TR>
				</xsl:otherwise>
			</xsl:choose>
		</table>
		<xsl:if test="(position() mod 35) = 0 ">35 rows per page
			<br class="pagebreak"/>
			<xsl:copy-of select="$ReportFooter"/>
			<xsl:call-template name="Filler">
				<xsl:with-param name="fillercount" select="1"/>
			</xsl:call-template>
			<xsl:copy-of select="$RemarkRowsHeader"/>
		</xsl:if>
		<!--Filler -->
		<xsl:choose>
			<!-- case of only one page-->
			<xsl:when test="count(/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']) &lt;= 40">
				<xsl:call-template name="Filler">
					<!--<xsl:with-param name="fillercount" select="40 - (count(/Booking/Passengers/Passenger))"/>-->
					<xsl:with-param name="fillercount" select="0"/>
				</xsl:call-template>
			</xsl:when>
			<!-- case of more than one page-->
			<xsl:otherwise>
				<xsl:call-template name="Filler">
					<!--(Rows per page = 40) -  (Rows in current page) - (Total section rows = 1 ) + (Filler Row = 1)-->
					<!--<xsl:with-param name="fillercount" select="40 - ( ( count(/Booking/Passengers/Passenger)-40 ) mod 40 ) - 3 + 1"/>-->
					<xsl:with-param name="fillercount" select="0"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<!--End Filler -->
		<!--End Payments-->
	</xsl:template>
	<xsl:variable name="PaymentRowsHeader">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin"/>
				<th align="left" style="width: 200px">Description</th>
				<th>&#xA0;</th>
				<th align="left" style="width: 90px">Status, Date</th>
				<th align="right">Credit</th>
				<th align="right">Debit</th>
				<td class="tdmargin"/>
			</tr>
		</table>
	</xsl:variable>
	<!--End Payment Section-->


	<!--En Terms and Condition-->
	<xsl:template match="Booking/Header/BookingHeader">
		<table style="width: 100%">
			<!--<table border="0" width="100%" cellspacing="0" cellpadding="0">-->
			<tr>
				<td class="tdmargin"/>
				<td class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal">
					<p class="MsoNormal" align="center" style="text-align:center">
						<b>
							<font size="2">*****Check-in for all flights is 30 minutes prior to departure time.*****</font>
						</b>
					</p>
					<br/>
					<p class="MsoNormal">Please reconfirm travel at least 24 hours in advance; passengers not checked in 15 minutes prior to scheduled departure may be denied boarding without compensation by Wings of Alaska. All flights subject to change without notice. Tickets are non-transferable.</p>
					<br/>
					<p class="MsoNormal">
						<b>Reservation Changes:</b>
					</p>
					<p class="MsoNormal">Cancellations or changes may be made up to 12 hours prior to your departure time without any service fee. If the request is not made at least 12 hours prior to departure, or a passenger fails to check-in for their confirmed flight, a $25 fee will be charged. Please note that failure to check-in for, or cancel, a confirmed reservation will also result in the cancellation of any remaining flights. This is based on the logic that if you did not fly on one leg of your trip, you can't be in the departure city of your next leg. If you decide to change your travel plans, please let us know so we can protect your reservation.</p>
					<br/>
					<p class="MsoNormal">
						<b>Flight Delays:</b>
					</p>
					<p class="MsoNormal">Arrival and departure times are not guaranteed. We strive to be punctual, but due to varied routings and weather, there are occasional delays. All Wings of Alaska flights are conducted weather permitting. If your flight is cancelled due to weather, you will have the choice of obtaining reserved seating on the next available scheduled flight to the same destination or obtaining a full refund for that flight.</p>
					<br/>
					<p class="MsoNormal">
						<b>Baggage:</b>
					</p>
					<p class="MsoNormal">For reasons of safety each passenger is limited to 50 lbs of baggage. Excess baggage is subject to a charge of $0.50 per lb per flight (roundtrips = 2 flights). Excess baggage is accommodated as freight, and may travel on a later flight. Identification is required on all items of baggage. Wings of Alaska will not be liable for damage to perishable items or fragile articles in checked baggage. Carry on baggage is not permitted aboard any Wings aircraft. Hazardous materials must be declared.
Packing suggestions: Use several soft-sided small bags instead of one large suitcase. Bulky items such as large suitcases, trunks, or ice chests are difficult to load. Single pieces of baggage weighing over 50 pounds are subject to an overweight baggage fee.</p>
					<br/>
					<p class="MsoNormal">
						<b>Hazardous Materials &amp;  Firearms:</b>
					</p>
					<p class="MsoNormal">For your safety, we will not carry the following on passenger flights: compressed gases or corrosives, such as acids and wet batteries; explosives, such as fireworks and munitions; flammables, such as gasoline, matches and lighter fuels; poisons; magnetic and radioactive materials; and all other items restricted by government regulations. Bear mace must be declared and transported in a container provided by Wings of Alaska.</p>
					<br/>
					<p class="MsoNormal">
						<b>Security:</b>
					</p>
					<p class="MsoNormal">All persons, baggage, and cargo traveling on any flight or within the confines of a Wings of Alaska location are subject to search at the discretion of Wings of Alaska. Wings of Alaska has the right to refuse service to anyone if it is believed they present a threat to security.</p>
					<br/>
					<p class="MsoNormal">
						<b>Refunds:</b>
					</p>
					<p class="MsoNormal">This purchase is refundable. Refunds must be requested within one year of the original purchase date. Refunds will be credited back to the credit card used to purchase the reservation. Roundtrip fares may be subject to additional fees if partial itinerary is canceled. A refund may be requested at any Wings office or from our accounting office and will be processed within 10 business days from receipt of request. Wings of Alaska, Attn: Refunds, 8421 Livingston Way, Juneau, AK 99801 Email:<a href="mailto:acct@wingsofalaska.com?subject=Refunds">acct@wingsofalaska.com</a></p>
					<br/>
					<p class="MsoNormal">
						<b>Privacy Policy:</b>
					</p>
					<p class="MsoNormal">Wings of Alaska is concerned about your right to privacy. When you purchase a service from us, we request certain personally identifiable information from you during the purchase process. You must provide contact information (such as name, phone number, e-mail, and mailing address) and financial information (such as credit card number and expiration date) in order to complete a purchase. We utilize Secure Socket Layer (SSL) technology to protect the submission of your personal information. This technology provides a certain level of protection when transmitting your credit card numbers and other personal information over the Internet. All personally identifiable information collected via our Web site is encrypted before it is transmitted to our storage servers using SSL. Except as allowed or required by law, Wings of Alaska will not share any personal or private information with anybody outside Wings of Alaska.</p>
					<br/>
				</td>
				<td class="tdmargin"/>
			</tr>
			<!--</table>-->
		</table>
	</xsl:template>
	<!--End en_terms-->

	<xsl:variable name="TicketRowsHeader">
		<table class="tabledetails" cellspacing="0" style="table-layout:fixed">
			<tr>
				<td class="tdmargin" style="width: 10px" />
				<!--<th align="left" style="width: 50px">Description</th>-->
				<th align="left" style="width: 245px">Description</th>
				<th align="left" style="width: 50px">Flight</th>
				<th align="left" style="width: 60px">Date</th>
				<th align="left" style="width: 160px">PassengerName</th>
				<th align="left" style="width: 75px">Type</th>
				<th align="right" style="width: 75px">VAT</th>
				<th align="right" style="width: 75px">EUR</th>
				<td class="tdmargin" style="width: 10px" />
			</tr>
		</table>
	</xsl:variable>
</xsl:stylesheet>