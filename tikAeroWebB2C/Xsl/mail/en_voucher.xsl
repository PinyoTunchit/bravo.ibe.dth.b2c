<?xml version="1.0" encoding="utf-8"?>
<!-- 
Project: ISK
File name: en_voucher.xsl

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<!--Variable-->
	<!--<xsl:variable name="BaseURL">http://www.tiksystems.com/tikaero/</xsl:variable>-->
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable>
	<xsl:variable name="Title">GIFT VOUCHER</xsl:variable>
	<xsl:template name="CDate2Julian">
		<xsl:param name="date"/>
		<xsl:param name="format" select="000"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="format-number(substring($date,3,2),'00')"/>
		<xsl:variable name="month" select="substring(substring-after('01/00002/03103/05904/09005/12006/15107/18108/21209/24310/27311/30412/334', concat($month1,'/')), 1, 3)"/>
		<xsl:value-of select="$day + $month"/>
	</xsl:template>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1), 1, 3)"/>
		<xsl:value-of select="concat($day, '/', $month, '/', $year)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:value-of select="concat($day, '.', $month, '.', $year)"/>
	</xsl:template>

	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>&lt;xsl:value-of select=&quot;$Title&quot;/&gt;</TITLE>
				<!--<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>-->
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<!--Style Sheet-->
				<STYLE>.base { color: ; font-family: 12pt helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 9pt; width: 100%; }
					.base .content{ color: ; float: left;  font-size: 9pt; }
					.grid TR.gridHeader	{background:#EAEEF8;color:#031668;;font-size:11px;font-weight:bold;padding:4px 2px 4px 5px;}
					.grid TR.gridHeader TD	{ padding: 7px 2px 1px 5px; border-bottom: 1px solid #B9CAEB; }
					.grid TR.gridItems0 TD { border-bottom: 1px solid #B9CAEB; color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems1 TD { border-top: 1px solid  #B9CAEB; color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems0 TD { color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; }
					.grid TR.gridItems1 TD {border-bottom: 2px solid #B9CAEB;	cursor: pointer;font-size: 11px;font-weight: normal;height: 15px;	padding: 0px 1px 0px 5px;border-bottom-width: medium;border-bottom-style: double;}
					p.msonormal {font-family:Tahoma;font-size:12.0pt;margin-bottom:.0001pt;margin-left:0in;margin-right:0in;margin-top:0in;mso-style-parent:"";color: "#808080";}
					.style1 {
						color: #FF0000;
						font-family: Verdana;
					}
					.style2 {
						color: #808080;
						font-family: Verdana;
					}
					.style3 {
						color: #0000FF;
					}
					.style4 {
						color: #008000;
					}
					.style5 {
						font-size: 9.5px;
					}
					.style6 {
						font-family: Verdana;
						font-size: 36.5px;
						color: #808080;

					}</STYLE>
				<!--End Style Sheet-->
			</HEAD>
			<BODY leftmargin="10px;" topmargin="5" rightmargin="5" bottommargin="5" marginwidth="5" marginheight="5">
				<table border="0" cellpadding="0" cellspacing="0" width="759" align="left">

					<tr>
						<td>

							<xsl:if test="/Voucher/Details/form_of_payment_subtype_rcd = 'GIFT'">
								<div id="layer3">
									<img src="{$BaseURL}en_giftv.jpg" height="1073" width="759"></img>
								</div>
							</xsl:if>
							<xsl:if test="/Voucher/Details/form_of_payment_subtype_rcd = 'RTVC'">
								<xsl:choose>
									<xsl:when test="/Voucher/Details/fare_only_flag = 1">

										<div id="layer2" class="style1" style="font-family:Tahoma 25pt; font-size:25px;color : #ffffff;position: absolute; width: 367px; height: 32px; z-index: 3; left: 247px; top:290px">

											<font style="font-weight:bold;">I</font>ncl</div>
									</xsl:when>
									<xsl:otherwise>
										<div id="layer2" class="style1" style="font-family:Tahoma 25pt; font-size:25px;color : #ffffff;position: absolute; width: 367px; height: 32px; z-index: 3; left: 247px; top: 290px">

											<font style="font-weight:bold;">E</font>xcl</div>
									</xsl:otherwise>
								</xsl:choose>
								<div id="layer3">
									<img src="{$BaseURL}en_roundtripv.jpg" height="1073" width="759"></img>
								</div>
							</xsl:if>
							<xsl:if test="/Voucher/Details/form_of_payment_subtype_rcd = 'OWVC'">
								<xsl:choose>
									<xsl:when test="/Voucher/Details/fare_only_flag = 1">

										<div id="layer2" class="style1" style="font-family:Tahoma 25pt; font-size:25px;color : #ffffff;position: absolute; width: 367px; height: 32px; z-index: 3; left: 247px; top: 291px">

											<font style="font-weight:bold;">I</font>ncl</div>
									</xsl:when>
									<xsl:otherwise>
										<div id="layer2" class="style1" style="font-family:Tahoma 25pt; font-size:25px;color : #ffffff;position: absolute; width: 307px; height: 32px; z-index: 3; left: 247px; top: 291px">

											<font style="font-weight:bold;">E</font>xcl</div>
									</xsl:otherwise>
								</xsl:choose>
								<div id="layer3">
									<img src="{$BaseURL}en_onewayv.jpg" height="1073" width="759"></img>
								</div>
							</xsl:if>


							<!--for VC-->

							<xsl:if test="/Voucher/Details/form_of_payment_subtype_rcd = 'GIFT'">
								<div id="layer2" class="style1" style="font-size:25px;color : #00366e;position: absolute; width: 367px; height: 20px; z-index: 3;left: 250px; top:299px">
									<strong>
										<xsl:value-of select="format-number(/Voucher/Details/charge_amount,'#,##0.00')"/>&#xA0;&#xA0;<xsl:value-of select="/Voucher/Details/currency_rcd"/></strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 25px;color : #00366e;position: absolute; width: 367px; height: 32px; z-index: 3;left: 250px; top: 338px">
									<strong>
										<xsl:value-of select="/Voucher/Details/destinations"/> 
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 20px;color : #00366e;position: absolute; width: 390px; height: 32px; z-index: 3;left: 250px; top: 393px">
									<strong>
										<xsl:value-of select="/Voucher/Details/voucher_text"/>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 20px;color : #00366e;position: absolute; width: 367px; height: 32px; z-index: 3;left: 250px; top: 431px">
									<strong>
										<xsl:choose>
											<xsl:when test="/Voucher/Details/recipient_name != ''">
												<xsl:value-of select="/Voucher/Details/recipient_name"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="/Voucher/Details/lastname"/>
												<xsl:value-of select="/Voucher/Details/firstname"/>
											</xsl:otherwise>
										</xsl:choose>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 20px;color : #00366e;position: absolute; width: 367px; height: 32px; z-index: 3;left: 250px; top: 478px">
									<strong>
										<xsl:value-of select="/Voucher/Details/voucher_number"/>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 20px;color : #00366e;position: absolute; width: 367px; height: 32px; z-index: 3;left: 250px; top:499">
									<strong>
										<xsl:value-of select="/Voucher/Details/voucher_password"/>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 18px;color : #00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250px; top:695">
									<strong>
										<xsl:if test="/Voucher/Details/create_date_time != '' ">
											<xsl:call-template name="formatdate">
												<xsl:with-param name="date" select="/Voucher/Details/create_date_time"/>
											</xsl:call-template>
										</xsl:if>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 18px;color : #00366e;position: absolute; width: 367px; height: 32px; z-index: 3;left: 250px; top: 716">
									<strong>
										<xsl:if test="/Voucher/Details/expiry_date_time !='' ">
											<xsl:call-template name="formatdate">
												<xsl:with-param name="date" select="/Voucher/Details/expiry_date_time"/>
											</xsl:call-template>
										</xsl:if>
									</strong>
								</div>
							</xsl:if>
							<!--end VC-->
							<!--for RTVC-->
							<xsl:if test="/Voucher/Details/form_of_payment_subtype_rcd = 'RTVC'">
								<div id="layer2" class="style1" style="font-size: 25px;color :#00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250; top:350px">
									<strong>
										<xsl:value-of select="/Voucher/Details/destinations"/>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 25px;color :  #00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250px; top: 390px">
									<strong>

										<!--<xsl:choose>
											<xsl:when test="/Voucher/Details/recipient_name != ''">
												<xsl:value-of select="/Voucher/Details/recipient_name"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="/Voucher/Details/client_profile_id"/>
											</xsl:otherwise>
										</xsl:choose>-->
										<xsl:choose>
											<xsl:when test="/Voucher/Details/recipient_name != ''">
												<xsl:value-of select="/Voucher/Details/recipient_name"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="/Voucher/Details/lastname"/>
												<xsl:value-of select="/Voucher/Details/firstname"/>
											</xsl:otherwise>
										</xsl:choose>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 20px;color : #00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250px; top:433px">
									<strong>
										<xsl:value-of select="/Voucher/Details/voucher_number"/>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 20px;color : #00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250px; top: 455">
									<strong>
										<xsl:value-of select="/Voucher/Details/voucher_password"/>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 18px;color : #00366e;position: absolute; width: 400px; height: 30px; z-index: 3;left: 250px; top: 672">
									<strong>
										<xsl:if test="/Voucher/Details/update_date_time !='' ">
											<xsl:call-template name="formatdate">
												<xsl:with-param name="date" select="/Voucher/Details/update_date_time"/>
											</xsl:call-template>
										</xsl:if>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 18px;color :  #00366e;position: absolute; width: 400px; height: 30px; z-index: 3;left: 250px; top: 692">
									<strong>
										<xsl:if test="/Voucher/Details/expiry_date_time !='' ">
											<xsl:call-template name="formatdate">
												<xsl:with-param name="date" select="/Voucher/Details/expiry_date_time"/>
											</xsl:call-template>
										</xsl:if>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 18px;color :  #00366e;position: absolute; width: 400px; height: 30px; z-index: 3;left: 250px; top: 712">
									<strong>
										<xsl:value-of select="/Voucher/Details/valid_for_class"/>
									</strong>
								</div>
							</xsl:if>
							<!--end RTVC-->
							<!--for OWVC-->
							<xsl:if test="/Voucher/Details/form_of_payment_subtype_rcd = 'OWVC'">
								<div id="layer2" class="style1" style="font-size: 25px;color :#00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250px; top: 350px">
									<strong>
										<xsl:value-of select="/Voucher/Details/destinations"/> 
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 25px;color :  #00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250px; top:389px">
									<strong>

										<xsl:choose>
											<xsl:when test="/Voucher/Details/recipient_name != ''">
												<xsl:value-of select="/Voucher/Details/recipient_name"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="/Voucher/Details/lastname"/>
												<xsl:value-of select="/Voucher/Details/firstname"/>
											</xsl:otherwise>
										</xsl:choose>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 20px;color : #00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250px; top: 433px">
									<strong>
										<xsl:value-of select="/Voucher/Details/voucher_number"/>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 20px;color : #00366e;position: absolute; width: 400px; height: 32px; z-index: 3;left: 250px; top: 455">
									<strong>
										<xsl:value-of select="/Voucher/Details/voucher_password"/>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 18px;color : #00366e;position: absolute; width: 400px; height: 30px; z-index: 3;left: 250px; top:672">
									<strong>
										<xsl:if test="/Voucher/Details/update_date_time !='' ">
											<xsl:call-template name="formatdate">
												<xsl:with-param name="date" select="/Voucher/Details/update_date_time"/>
											</xsl:call-template>
										</xsl:if>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 18px;color :  #00366e;position: absolute; width: 400px; height: 30px; z-index: 3;left: 250px; top:692">
									<strong>
										<xsl:if test="/Voucher/Details/expiry_date_time !='' ">
											<xsl:call-template name="formatdate">
												<xsl:with-param name="date" select="/Voucher/Details/expiry_date_time"/>
											</xsl:call-template>
										</xsl:if>
									</strong>
								</div>
								<div id="layer2" class="style1" style="font-size: 18px;color :  #00366e;position: absolute; width: 400px; height: 30px; z-index: 3;left: 250px; top:712">
									<strong>
										<xsl:value-of select="/Voucher/Details/valid_for_class"/>
									</strong>
								</div>
							</xsl:if>
							<!-- end OWVC-->
						</td>
					</tr>
				</table>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="no" externalpreview="no" url="file:///c:/Development/XSL/_CASE/Voucher.xml" htmlbaseurl="" outputurl="" processortype="msxml4" useresolver="no" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->