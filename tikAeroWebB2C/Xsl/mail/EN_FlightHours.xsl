<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="PageTitle">tikAERO Crew</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable>
	<xsl:variable name="ColorSpan">Red</xsl:variable>

	<xsl:key name="currency_rcd_group" match="/CrewAllowance/Details" use="currency_rcd"/>


	<xsl:variable name="MaleWeight">87.5</xsl:variable>
	<xsl:variable name="FemaleWeight">71.6</xsl:variable>
	<xsl:variable name="ChildWeight">34.0</xsl:variable>
	<xsl:variable name="InfantWeight">13.6</xsl:variable>
	<xsl:template name="formatmonth">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('     01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1), 3, 3)"/>
		<xsl:value-of select="concat($day, ' ', $month, ' ', $year)"/>
	</xsl:template>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('     01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1), 1, 3)"/>
		<xsl:variable name="hours" select="substring($date, 10,2)"/>
		<xsl:variable name="minutes" select="substring($date,13,2)"/>
		<xsl:value-of select="concat($day, ' ', $month, ' ', $year ,' / ',$hours ,':', $minutes)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:value-of select="concat($day, $month, $year)"/>
	</xsl:template>

	<xsl:template name="jadate">
		<xsl:param name="date"/>
		<script type="text/javascript">function checkTime(i)
{
if (i &lt; 10)
  
  i="0" + i;
  
return i;
}	
		
{var a_p = "";
	 var d = new Date();
	 var month=new Array(12);
month[0]="January";
month[1]="February";
month[2]="March";
month[3]="April";
month[4]="May";
month[5]="June";
month[6]="July";
month[7]="August";
month[8]="September";
month[9]="October";
month[10]="November";
month[11]="December";

										 
														var curr_hour = d.getHours();
														var curr_min = d.getMinutes();
														curr_min =checkTime(curr_min);
														var curr_Date = d.getDate();
														var curr_Month = month[d.getMonth()];
														var curr_Year = d.getYear();
														 
												         

														document.write(curr_Date + " " + curr_Month + " " +curr_Year + " " + curr_hour + ":"+ curr_min)
													}</script>
	</xsl:template>
	<xsl:template name="minutesdate">
		<xsl:param name="date"/>
		<script type="text/javascript">{var a_p = "";
	 var d = new Date();
	 
 
														var curr_hour = d.getHours();
														var curr_min = d.getMinutes();
														 
														document.write( curr_hour + ":"+ curr_min  )
														}</script>
	</xsl:template>
	<xsl:template name="makeTime">
		<xsl:param name="data"/>
		<xsl:variable name="hours" select="(floor($data div 60))"/>
		<xsl:variable name="minutes" select="(floor($data) mod 60)"/>
		<xsl:value-of select="concat(format-number($hours,'#0'),'.',format-number($minutes,'00'))"/>
	</xsl:template>
	<xsl:template match="/">
		<HTML>
			<HEAD>

				<STYLE>.HeaderBackground { background: url(http://www.tikaero.com/xslimages/AAS/Images/0/H/hhe.gif) fixed no-repeat left center; }
					.base { color: ; font-family: 12pt Tahoma, Arial, sans-serif, verdana; font-size: 9pt; width: 100%; height:100%}
					.base .content{ color: ; float: left;  font-size: 9pt; }
					.dummy{ }
					.base .content .contentHeader{ margin: 5px 0px 0px 0px; padding: 0px 0px 0px 5px; }
					.base .quickSearch { float: left; width: 170px; }					
					.base .quickSearch .quickSearchHeader{ background: url(http://www.tikaero.com/xslimages/AAS/Images/0/H/hqs.gif) fixed no-repeat left center; padding: 10px 0px 5px 0px; }
					.base .quickSearch DIV.input { font-size: 8pt; float: left; padding: 2px 4px 2px 5px; text-align: left; }
					.base .quickSearch DIV.label{ float: left; padding: 2px 4px 2px 5px; text-align: right; vertical-align: middle; }
					.BarcodeHeader	{ color: ; font-size: 7pt; font-style: normal; font-weight: bold; padding: 0px 0px 0px 12px; text-align: left; width: 100%; }
					.PanelItinerary .Barcode	{font: bold 15pt Tahoma, Arial, sans-serif, verdana;padding: 1 5 1 5; }
					.footer	{ background: url(http://www.tikaero.com/xslimages/AAS/Images/0/I/bgb.gif); color: #201384; font-size: 8pt; padding: 15px 5px 0px 5px; width: 100%; }
					.footer A	{ height: 20px; text-align: right; width: 50%; }
					.footer	SPAN	{ height: 20px; padding-top: 3px; width: 50%; }
					.grid TD.gridItems0Button	{ padding: 1px 5px 1px 1px; text-align: right; }
					.grid TR.GridFooter TD	{ background-color: transparent; border-bottom: 1px solid  #B9CAEB; color: #666666; font: bold 8pt ; border-top: 1px solid #B9CAEB; height: 22px;  }
					.grid TR.GridFooter TD.Summary	{ background-color: transparent; }
					 
					.grid TR.gridHeader	{border-right:1px solid #B9CAEB;border-left:1px solid #B9CAEB;background:#1278CA;color:#ffffff;;font-size:11px;font-weight:bold;padding: 4px 2px 4px 5px;font-family: Tahoma, Arial, sans-serif, verdana;}
					.grid TR.GridPrice	{ border-right:1px solid #B9CAEB;border-left:1px solid #B9CAEB;background-color: tranparent; color: ; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridHeader TD	{border-right:1px solid #B9CAEB;border-left:1px solid #B9CAEB; padding: 4px 2px 4px 5px; border-bottom: 1px solid #B9CAEB; }
					.grid TR.gridHeaderFight { background-color: #FED684; color: ; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridItems0 TD {font-family: Tahoma, Arial, sans-serif, verdana; border-bottom: 1px solid  #DAE1E6; color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems1 TD { border-top: 1px solid  #B9CAEB; color: #4A4949 ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid tr.griditems2 td{color:;cursor:pointer;font-size:11px;font-weight:normal;height:15px;padding:0px 1px 0px 5px;}
					.grid TR.gridItems02 TD { color:  ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems0 TD { color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; }
					.grid TR.gridItems01 TD { color:#4A4949; ; cursor: pointer;  font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0o TD{ background-color: #FFBD3B; border-bottom: 1px solid  #DAE1E6; color: ; cursor: pointer;  font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01 TD	{border-right:1px solid #F7C07D;border-left:1px solid #F7C07D;border-bottom: 1px solid #F7C07D; color:#4A4949 ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; font-family: Tahoma, Arial, sans-serif, verdana;}
					.grid TR.gridItems01o TD	{ background-color: #CEE6F7; color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0Input	{ color: ;font-size: 11px; font-weight: normal; height: 22px; padding: 5px 1px 1px 5px; }
					.header	{ width: 100%; }
					.header .subHeader	{ background-color: ; }
					/* End Forms \*/
					/* Headers \*/
					.headers	{color:#C4102F; font: bold 12pt /*background: url(../Images/0/H/hbh.jpg);*/ margin: 5px 0px 5px 5px; width: 100%; }
					.headers .ItineraryGrid	{ background: url(http://www.tikaero.com/xslimages/Aurigny/Images/0/I/iin.gif) fixed no-repeat left center; }
					.headers .SearchResultGrid{ background: url(http://www.tikaero.com/xslimages/AAS/Images/0/I/isr.gif) fixed no-repeat left center; }
					.headers DIV	{ font: bold 8pt  margin: 3px 0px 3px 0px; padding: 5px 0px 5px 17px; }
					.headers DIV SPAN.S0{ color: #FF6600; }
					.headers SPAN.Button { width: 49%; }
					.Itinerary	{ color: ; font-size: 8pt; font-style: normal; padding: 10px 12px 0px 12px; text-align: left; width: 100%; }
					.Itinerary Span.O	{ color: #FF6600; font-weight: bold; }
					.Itinerary Span.1	{ color: #FF6600; font-weight: bold; text-decoration: underline; }
					.Itinerary Span.2 { color: ; font-weight: bold; }
					.welcometext,.
					.commenttext{font-family:verdana,arial,helvetica,sans-serif;font-size:12px;padding:1 10 1 5;}
	 				table.MsoNormalTable{mso-style-parent:"";	font-size:10.0pt;	font-family:Times New Roman";	}
 					p.MsoNormal{mso-style-parent:"";	margin-bottom:.0001pt;	font-size:8.0pt;	font-family:Arial;	color:navy;	font-weight:bold;	margin-left:0in; margin-right:0in; margin-top:0in}</STYLE>

				<!--End Style Sheet-->
			</HEAD>
			<BODY leftmargin="10px;" topmargin="5" rightmargin="5" bottommargin="5" marginwidth="5" marginheight="5">
				<DIV class="PanelFlightDispatch">
					<img src="{$BaseURL}ISK_logo.jpg"/>
				</DIV>
				<p/>



				<table class="Grid" rules="all" cellpadding="3" border="0" style="width:100%;font: bold 12pt Tahoma, Arial, sans-serif, verdana; color:#C4102F;font-size: 12px; ">
					<tr>
						<td align="left" width="20%">
							<xsl:text>Flight Hours</xsl:text>
						</td>
						<td align="left" width="30%"></td>
						<td align="right" width="50%">Print date : &#xA0;&#xA0;


							<xsl:call-template name="jadate">
								<xsl:with-param name="date"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
				<DIV class="PanelItinerary">


					<table class="Grid" rules="all" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12pt;">
						<tr class="gridHeader" valign="middle">
							<td align="center" width="20%">A/C Type</td>
							<td align="center" width="20%">Average</td>
							<td align="center" width="15%">Max</td>
							<td align="center" width="15%">Min</td>
							<td align="center" width="15%">Total</td>
							<td align="center" width="15%">Crew on Duty</td>
						</tr>

						<xsl:for-each select="/FlightHours/Details[pilot_flag = 1]">
							<tr class="GridItems01">
								<td align="Left" bgcolor="#FFFFFF" width="20%">
									<span>

										<xsl:value-of select="aircraft_type_rcd"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="20%">
									<span>
										<xsl:value-of select="average"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="15%">
									<span>
										<xsl:value-of select="maximum"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="15%">
									<span>
										<xsl:value-of select="minimum"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="15%">
									<span>
										<xsl:value-of select="total"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="15%">
									<span>
										<xsl:value-of select="crew_on_duty"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</table>
					<br/>
					<table class="Grid" rules="all" style="border-width:0px;width:100%;border-collapse:collapse;font-size: 12pt ;">
						<tr class="gridHeader" valign="middle">
							<td align="center" width="20%">A/C Type</td>
							<td align="center" width="20%">Average</td>
							<td align="center" width="15%">Max</td>
							<td align="center" width="15%">Min</td>
							<td align="center" width="15%">Total</td>
							<td align="center" width="15%">Crew on Duty</td>
						</tr>
						<xsl:for-each select="FlightHours/Details[pilot_flag = ''] ">
							<tr class="GridItems01">
								<td align="Left" bgcolor="#FFFFFF" width="20%">
									<span>

										<xsl:value-of select="aircraft_type_rcd"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="20%">
									<span>
										<xsl:value-of select="average"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="15%">
									<span>
										<xsl:value-of select="maximum"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="15%">
									<span>
										<xsl:value-of select="minimum"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="15%">
									<span>
										<xsl:value-of select="total"/>
									</span>
								</td>
								<td align="Left" bgcolor="#FFFFFF" width="15%">
									<span>
										<xsl:value-of select="crew_on_duty"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\Untitled1teas_flight.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->