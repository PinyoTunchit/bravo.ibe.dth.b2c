<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:ms="urn:schemas-microsoft-com:xslt" 
	xmlns:dt="urn:schemas-microsoft-com:datatypes" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:VBDateDiff="http://tikSystems/script" >
	
	<xsl:variable name="PageTitle">tikAERO FlightManifest</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable> 
	<xsl:variable name="ColorSpan">Red</xsl:variable>
	<xsl:key name="boarding_class_rcd_group" match="//Passengers/Passenger" use="boarding_class_rcd"/>
	<xsl:variable name="MaleWeight">87.5</xsl:variable>
	<xsl:variable name="FemaleWeight">71.6</xsl:variable>
	<xsl:variable name="ChildWeight">34.0</xsl:variable>
	<xsl:variable name="InfantWeight">13.6</xsl:variable>
	<xsl:template name="formatmonth">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('     01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1), 3, 3)"/>
		<xsl:value-of select="concat($day, ' ', $month, ' ', $year)"/>
	</xsl:template>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:variable name="month" select="substring(substring-after('     01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1), 1, 3)"/>
		<xsl:value-of select="concat($day, ' ', $month, ' ', $year)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,1,4)"/>
		<xsl:value-of select="concat($day, $month, $year)"/>
	</xsl:template>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$PageTitle"/>
				</TITLE>
				<STYLE>
					.PanelItinerary .BookingNumber{
						color: #C4102F;
					}
					.PanelItinerary.PanelHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #000000;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 5px 0px 5px 5px;
						width: 650px;
					}
					.PanelItinerary .PanelHeader IMG{
						vertical-align: middle;
					}
					.PanelItinerary .PanelHeader SPAN{
						padding-left: 4;
					}
					.PanelItinerary .PanelContainer{
						padding: 10 0 10 5;
						width: 650px;
						display: block;
					}
					.GridHeader TD
					{
						background: D0D0D0;
						color: #000000;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						padding: 4px 2px 4px 5px;
					}
					.PanelItinerary TR.GridItems
					{
						border-bottom: 1px solid #B9CAEB;
						color: #3D3D3D;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					.GridItems TD
					{
						border-bottom: 1px solid #B9CAEB;
						color:#3D3D3D;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					
					.GridItems01 TD
					{
						
						color:#3D3D3D;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					.PanelItinerary .GridFooter TD SPAN
					{
						font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
						font-size: 11px;
						height: 18px;
						padding: 4 5 0 4;
					}
					.PanelItinerary .FooterTotal
					{
						border-bottom: solid 1px #dddddd;
					}
					.PanelItinerary SPAN.FooterTotalLabel
					{
							background-color: transparent;
					 		color: #666666;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							padding: 1px 0px 1px 0px;
					}
					.PanelItinerary SPAN.FooterTotalLabelRed
					{
							background-color: transparent;
					 		color: Red;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							padding: 1px 0px 1px 0px;
					}
					.PanelItinerary SPAN.FooterTotalValue
					{
						color: #666666;
						font-size: 11px;
					} 					
					
					.WelcomeText { 
						font-family: verdana, arial, helvetica, sans-serif; 
						font-size: 12px; 
						padding: 1 10 1 5; 
					} .
					
					.CommentText { 
					font-family: verdana, arial, helvetica, sans-serif; 
						font-size: 12px; 
						padding: 1 10 1 5; 
					}
					.BarcodeHeader
						{
							color: #666666;
							font-family: Tahoma, Arial, sans-serif, verdana;
							font-size: 7pt;
							font-style: normal;
							font-weight: bold;
							padding: 0px 0px 0px 12px;
							text-align: left;
							width: 650px;
						}

					.PanelItinerary .Barcode
							{
								padding: 1 5 1 5;
							} 
					SPAN.Barcode {
					
							}
							SPAN.Barcode SPAN{
								background : Black;
								height:30px;
								
							}
							SPAN.Barcode SPAN.11{
								width: 1px;
								margin: 0 1 0 0;
							}
							SPAN.Barcode SPAN.12{
								width: 1px;
								margin: 0 3 0 0;
							}
							SPAN.Barcode SPAN.21{
								width: 3px;
								margin: 0 1 0 0;
							}
							SPAN.Barcode SPAN.22{
								width: 3px;
								margin: 0 3 0 0;
							}
							SPAN.Barcode SPAN.1{
								width: 1px;
								
							}
							SPAN.Barcode SPAN.TicketNumber{
								background: #ffffff;
								color: Black;
								font-family: Tahoma, Arial, sans-serif, verdana;
								font-size: 8pt;
								width: 650px;
								padding-left: 10px;
								
							}
					 
					 
				</STYLE>
		</HEAD>
			<BODY>
				<DIV>
					<DIV class="PanelItinerary">
						<TABLE class="Table" cellspacing="0" rules="rows" border="0" width="650px;">
							<TR>
								<TD align="right">
									<img src="{$BaseURL}ISK_logo.jpg"/>
								</TD>
							</TR>
						</TABLE>
						<BR/>												
						<DIV class="PanelContainer">
							<table class="Table" cellspacing="1px" bgcolor="#B9CAEB" style="border-width:0px;width:650px;border-collapse:collapse;font-size: 10pt;">
								<tr>
									<SPAN class="GridHeader">
										<td align="left" style="width:20px;"> Flight </td>
									</SPAN>
									<SPAN class="GridItems01">
										<td align="left" bgcolor="#FFFFFF" style="width:100px;">
											<xsl:value-of select="FlightDispatch/Flight/Flight/airline_rcd"/>
											<SCRIPT LANGUAGE="JavaScript">document.write(" ");
							</SCRIPT>
											<xsl:value-of select="FlightDispatch/Flight/Flight/flight_number"/>&#160;&#160; 
							<SCRIPT LANGUAGE="JavaScript">document.write(" ");</SCRIPT>
										</td>
									</SPAN>
									<SPAN class="GridHeader">
										<td align="left" cellspacing="1px" bgcolor="#D0D0D0" style="width:20px;"> 
							From 
						</td>
									</SPAN>
									<SPAN class="GridItems01">
										<td align="left" bgcolor="#FFFFFF" style="width:100px;">
							&#160;&#160;<xsl:value-of select="FlightDispatch/Flight/Flight/origin_rcd"/>&#160;&#160; 
							<SCRIPT LANGUAGE="JavaScript">document.write(" ");</SCRIPT>
										</td>
									</SPAN>
									<SPAN class="GridHeader">
										<td align="left" cellspacing="1px" bgcolor="#D0D0D0" style="width:20px;"> 
							To 
						</td>
									</SPAN>
									<SPAN class="GridItems01">
										<td align="left" bgcolor="#FFFFFF" style="width:100px;">
											<xsl:value-of select="FlightDispatch/Flight/Flight/destination_rcd"/>
											<SCRIPT LANGUAGE="JavaScript">document.write(" ");</SCRIPT>
										</td>
									</SPAN>
								</tr>
								<tr>
									<SPAN class="GridHeader">
										<td align="left" bgcolor="#D0D0D0" style="width:20px;"> 
						Date 
					</td>
									</SPAN>
									<SPAN class="GridItems01">
										<td colspan="6" align="left" bgcolor="#FFFFFF">
											<xsl:call-template name="formatdate">
												<xsl:with-param name="date" select="//utc_departure_date_time"/>
											</xsl:call-template>
										</td>
									</SPAN>
								</tr>
							</table>
						</DIV>
			<DIV class="PanelHeader">Frequent Flyer</DIV>
			<DIV class="PanelContainer">				
				<table class="Table" cellspacing="1px" bgcolor="#B9CAEB" style="border-width:0px;width:650px;border-collapse:collapse;font-size: 10pt;">
					<TR class="GridHeader">							
						<TD align="Left" >Passenger Name</TD>
						<TD align="Left" >Frequent Flyer No.</TD>
						<TD align="Left" >Seat No.</TD>						
						<TD align="Left" >Class</TD>
					</TR>															
					<xsl:for-each select="FlightDispatch/Passengers/Passenger">
						<xsl:if test="member_number > '0'">     
							<tr class="GridItems01">					
								<td align="Left" bgcolor="#FFFFFF">
									<span><xsl:value-of select="lastname"/><xsl:if test="lastname != '' or firstname != '' "><xsl:text>/ </xsl:text></xsl:if>
									<xsl:value-of select="firstname"/>
									<xsl:if test="title_rcd!=''">
										<xsl:text>  </xsl:text>
										<xsl:value-of select="title_rcd"/>
									</xsl:if>
								 </span>
								</td>
								<td align="Left" bgcolor="#FFFFFF">
									<span><xsl:value-of select="member_number"/></span>
								</td>
								<td align="Left" bgcolor="#FFFFFF">
									<span><xsl:value-of select="seat_number"/></span>
								</td>								
								<td align="Left" bgcolor="#FFFFFF">
									<span><xsl:value-of select="boarding_class_rcd"/></span>
								</td>	
							</tr>
						</xsl:if>														
					</xsl:for-each>					 				
				</table>							 
			</DIV>
			<DIV class="PanelHeader">Special Service</DIV>
			<DIV class="PanelContainer">				
				<table class="Table" cellspacing="1px" bgcolor="#B9CAEB" style="border-width:0px;width:650px;border-collapse:collapse;font-size: 10pt;">						
					<TR class="GridHeader">							
						<TD align="Left" >Special Service</TD>
						<TD align="Left" >Passenger Name</TD>
						<TD align="Left" >Seat No.</TD>
						<TD align="Left" >Class</TD>
					</TR>			
					<xsl:for-each select="FlightDispatch/SSR/SSR">
						<tr class="GridItems01">	
							<td align="Left" bgcolor="#FFFFFF">
								<span><xsl:value-of select="display_special_service_rcd"/></span>
							</td>											
							<td align="Left" bgcolor="#FFFFFF">
								<span><xsl:value-of select="lastname"/><xsl:if test="lastname != '' or firstname != '' "><xsl:text>/ </xsl:text></xsl:if>
									<xsl:value-of select="firstname"/>
									<xsl:if test="title_rcd!=''">
										<xsl:text>  </xsl:text>
										<xsl:value-of select="title_rcd"/>
									</xsl:if>
								 </span>
							</td>
							<td align="Left" bgcolor="#FFFFFF">
								<span><xsl:value-of select="seat_number"/></span>
							</td>		
							<td align="Left" bgcolor="#FFFFFF">
								<span><xsl:value-of select="boarding_class_rcd"/></span>
							</td>	
						</tr>
						<!--xsl:if test="check_no_line!=1">
							<tr>																
								<TD></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</tr>
						</xsl:if>
						<xsl:if test="check_no_line=1">
							<tr>																
								<TD style="border-bottom: #FFCC00 1px solid;"></TD>
								<TD style="border-bottom: #FFCC00 1px solid;"></TD>
								<TD style="border-bottom: #FFCC00 1px solid;"></TD>
								<TD style="border-bottom: #FFCC00 1px solid;"></TD>
							</tr>
						</xsl:if-->
					</xsl:for-each>
				</table>
			</DIV>			
			
			<DIV class="PanelHeader">Infant</DIV>
			<DIV class="PanelContainer">	
				<table class="Table" cellspacing="1px" bgcolor="#B9CAEB" style="border-width:0px;width:650px;border-collapse:collapse;font-size: 10pt;">
					<TR class="GridHeader">							
						<TD align="Left" >Passenger Name</TD>
						<TD align="Left" >Age(Months)</TD>
						<TD align="Left" >Seat No.</TD>
   						<TD align="Left" >Class</TD>
					</TR>						
					<xsl:for-each select="FlightDispatch/INFs/INF">
						<tr class="GridItems01">												
							<td align="Left" bgcolor="#FFFFFF">
								<span><xsl:value-of select="lastname"/><xsl:if test="lastname != '' or firstname != '' "><xsl:text>/ </xsl:text></xsl:if>
									<xsl:value-of select="firstname"/>
									<xsl:if test="title_rcd!=''">
										<xsl:text>  </xsl:text>
										<xsl:value-of select="title_rcd"/>
									</xsl:if>
								 </span>
							</td>			
							<xsl:if test="date_of_birth!=''">
								<td align="Left" bgcolor="#FFFFFF">
									<xsl:value-of select="age"/>
								</td>																	
							</xsl:if>
							<xsl:if test="date_of_birth=''">
								<td align="Left" bgcolor="#FFFFFF">
									<xsl:text> </xsl:text>
								</td>									
							</xsl:if>
							<td align="Left" bgcolor="#FFFFFF">
								<span><xsl:value-of select="seat_number"/></span>
							</td>
							<td align="Left" bgcolor="#FFFFFF">
								<span><xsl:value-of select="boarding_class_rcd"/></span>									
							</td>	
						</tr>														
					</xsl:for-each>
				</table>
			</DIV>
					</DIV>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
	
	<msxml:script language="VBScript" implements-prefix="VBDateDiff">

		function VBDateDiff(strDate1, strDate2)			
			
	     	strDate1 = format(strDate1,"dd/mm/yyyy")
			strDate2 = format(strDate2,"dd/mm/yyyy")
			VBDateDiff = DateDiff("d", CDate(strDate1), CDate(strDate2)) 
		end function

	</msxml:script>
	
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\a\PAXSSRPOST_Mar29.XML" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->