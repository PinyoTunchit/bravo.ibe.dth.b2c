<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="PageTitle">tikAERO FlightManifest</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable> 
	<xsl:variable name="ColorSpan">Red</xsl:variable>
		<xsl:key name="boarding_class_rcd_group" match="//Passengers/Passenger" use="boarding_class_rcd" /> 
		
	<xsl:variable name="MaleWeight">87.5</xsl:variable>
	<xsl:variable name="FemaleWeight">71.6</xsl:variable>
	<xsl:variable name="ChildWeight">34.0</xsl:variable>
	<xsl:variable name="InfantWeight">13.6</xsl:variable>

	<xsl:template name="format-date">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<xsl:value-of select="concat($day, $month, $year)" />
	</xsl:template>
	<xsl:template match="/">
		<HTML>
		<HEAD>
			<TITLE><xsl:value-of select="$PageTitle"/></TITLE>
				<STYLE>
					.PanelItinerary .BookingNumber{
						color: #C4102F;
					}
					.PanelItinerary.PanelHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #000000;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 5px 0px 5px 5px;
						width: 659px;
					}
					.PanelItinerary .PanelHeader IMG{
						vertical-align: middle;
					}
					.PanelItinerary .PanelHeader SPAN{
						padding-left: 4;
					}
					.PanelItinerary .PanelContainer{
						padding: 10 0 10 5;
						width: 659px;
						display: block;
					}
					.GridHeader TD
					{
						background: #fdb813;
						color:  #003964;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						padding: 4px 2px 4px 5px;
					}
					.PanelItinerary TR.GridItems
					{
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					.GridItems TD
					{
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					
					.GridItems01 TD
					{
						
						color:#3D3D3D;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}
					.PanelItinerary .GridFooter TD SPAN
					{
						font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
						font-size: 11px;
						height: 18px;
						padding: 4 5 0 4;
					}
					.PanelItinerary .FooterTotal
					{
						border-bottom: solid 1px #dddddd;
					}
					.PanelItinerary SPAN.FooterTotalLabel
					{
							background-color: transparent;
					 		color: #666666;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							padding: 1px 0px 1px 0px;
					}
					.PanelItinerary SPAN.FooterTotalLabelRed
					{
							background-color: transparent;
					 		color: Red;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							padding: 1px 0px 1px 0px;
					}
					.PanelItinerary SPAN.FooterTotalValue
					{
						color: #666666;
						font-size: 11px;
					} 					
					
					.WelcomeText { 
						font-family: verdana, arial, helvetica, sans-serif; 
						font-size: 12px; 
						padding: 1 10 1 5; 
					} .
					
					.CommentText { 
					font-family: verdana, arial, helvetica, sans-serif; 
						font-size: 12px; 
						padding: 1 10 1 5; 
					}
					
					.PanelItinerary .PanelTopHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #C4102F;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 5px 0px 5px 5px;
						width: 100%;
					}
				</STYLE>
		</HEAD>
		<BODY>
		 <DIV>	
		<DIV class="PanelItinerary">
				<TABLE class="Table" cellspacing="0" rules="rows" border="0" width="659px;">
						<TR>
						<TD align="left">
							<img src="{$BaseURL}ISK_logo.jpg"/>
						</TD>
						</TR>
				</TABLE>
			<DIV class="PanelHeader">
				 Flight Manifest:  
				<SPAN><xsl:value-of select="FlightDispatch/Flight/Flight/airline_rcd"/></SPAN>
				<SCRIPT LANGUAGE="JavaScript">document.write(" ");
				</SCRIPT>
				<SPAN><xsl:value-of select="FlightDispatch/Flight/Flight/flight_number"/></SPAN>
				<SCRIPT LANGUAGE="JavaScript">document.write(" ");
				</SCRIPT>
				<SPAN><xsl:value-of select="FlightDispatch/Flight/Flight/origin_rcd"/>   -   </SPAN>
				<SCRIPT LANGUAGE="JavaScript">document.write(" ");
				</SCRIPT>
				<SPAN><xsl:value-of select="FlightDispatch/Flight/Flight/destination_rcd"/>&#160;</SPAN>
				<BR/>Date: <SPAN>
				<xsl:if test ="string(utc_departure_date_time) != '0'">
				<xsl:value-of select="substring(//utc_departure_date_time,5,2)"/>/<xsl:value-of select="substring(//utc_departure_date_time,7,2)"/>/<xsl:value-of select="substring(//utc_departure_date_time,1,4)"/>
				</xsl:if>
				</SPAN>
				
			</DIV>
			 
			 
				<DIV class="PanelTopHeader" style="font-size: 12px;">
					Passenger&#160;
				</DIV>
			<DIV class="PanelContainer">
				<table class="Table" cellspacing="1px" bgcolor="#DAE1E6" style="border-width:0px;width:659px;border-collapse:collapse;">
					<tr class="GridHeader">
						<td align="center" >S.No</td>
						<td align="center">Passenger'sName</td>
						<td align="center">Nationality</td>
						<td align="center">Pcs/Weight</td>
						<td align="center">For official Use</td>
					</tr>
					<xsl:for-each select="FlightDispatch/Passengers/Passenger">
					<tr class="GridItems01">
						<td align="center" bgcolor="#FFFFFF"><xsl:number value="position()" format="001 "/></td>
						<td align="left" bgcolor="#FFFFFF"><xsl:value-of select="firstname"/>&#160;&#160;<xsl:value-of select="lastname"/></td>
						<td align="left" bgcolor="#FFFFFF"><xsl:value-of select="nationality_rcd"/></td>
						<td align="right" bgcolor="#FFFFFF">
						<xsl:if test="hand_number_of_pieces != 0 ">
						<xsl:value-of select="hand_number_of_pieces"/>
						</xsl:if>
						<xsl:if test="hand_number_of_pieces != 0 or hand_baggage_weight != 0">/</xsl:if>
						<xsl:if test="hand_baggage_weight != 0 ">
						<xsl:value-of select="hand_baggage_weight"/>
						</xsl:if>
						</td>
						<td bgcolor="#FFFFFF">
						<xsl:value-of select="check_in_comment"/>
						</td>
					</tr>
					</xsl:for-each>
				</table>
				<BR></BR>
				<TABLE class="Table" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:659px;border-collapse:collapse;">
					<TR align="left" class="GridHeader">
						<TD>Class</TD>
						<TD>Passenger</TD>
						<TD align="right">Count</TD>
						<TD align="right">Total Weight</TD>
					</TR>
					<xsl:for-each select="FlightDispatch/Passengers/Passenger[count(. | key('boarding_class_rcd_group', boarding_class_rcd)[1]) = 1]">
						<xsl:variable name="boarding_class_rcd" select="boarding_class_rcd" /> 
						<TR align="right" class="GridItems">
							<TD align="left">&#160;<xsl:value-of select="boarding_class_rcd"/></TD>
							<TD align="left">Adult Male</TD>
							<TD>
							<xsl:if test="//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][gender_type_rcd != 'F' or gender_type_rcd = ''][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR'] != 0">
								<xsl:value-of select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][gender_type_rcd != 'F' or gender_type_rcd = ''][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR'])" /> 
							</xsl:if>
							</TD>
							<TD>
							<xsl:if test="//Passenger[boarding_class_rcd=$boarding_class_rcd][gender_type_rcd != 'F' or gender_type_rcd = ''][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR']/pax_weight > 0">
							
								<xsl:value-of select="format-number(sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][gender_type_rcd != 'F' or gender_type_rcd = ''][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR']/pax_weight),'#,##0.00')" /> 
							</xsl:if>
							</TD>
						</TR>
						<TR align="right" class="GridItems">
							<TD align="left">&#160;<xsl:value-of select="boarding_class_rcd"/></TD>
							<TD align="left">Adult Female</TD>
							<TD>
							<xsl:if test="//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][gender_type_rcd = 'F'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR'] != 0">
								<xsl:value-of select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][gender_type_rcd = 'F'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR'])" /> 
							</xsl:if>
							</TD>
							<TD>
							<xsl:if test="//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][gender_type_rcd = 'F'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR']/pax_weight > 0">
							
							<xsl:value-of select="format-number(sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][gender_type_rcd = 'F'][passenger_type_rcd != 'CHD' and passenger_type_rcd != 'INF' and passenger_type_rcd != 'UMNR']/pax_weight),'#,##0.00')" /> 
							</xsl:if>
							</TD>
						</TR>
						<TR align="right" class="GridItems">
							<TD align="left">&#160;<xsl:value-of select="boarding_class_rcd"/></TD>
							<TD align="left">Children</TD>
							<TD>
							<xsl:if test="//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd ='CHD' or passenger_type_rcd = 'UMNR'] != 0">
								<xsl:value-of select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd ='CHD' or passenger_type_rcd = 'UMNR'])" /> 
							</xsl:if>
							</TD>
							<TD>
							<xsl:if test="//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd = 'CHD' or passenger_type_rcd = 'UMNR']/pax_weight > 0">
							
							<xsl:value-of select="format-number(sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd = 'CHD' or passenger_type_rcd = 'UMNR']/pax_weight),'#,##0.00')" /> 
							</xsl:if>
							</TD>
						</TR>
						<TR align="right" class="GridItems">
							<TD align="left">&#160;<xsl:value-of select="boarding_class_rcd"/></TD>
							<TD align="left">Infant</TD>
							
							<TD>
							<xsl:if test="//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd = 'INF'] != 0">
								<xsl:value-of select="count(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd = 'INF'])" /> 
							</xsl:if>
							</TD>
							<TD>
							<xsl:if test="//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd = 'INF']/pax_weight != 0">
								<xsl:value-of select="format-number(sum(//Passenger[boarding_class_rcd=$boarding_class_rcd][passenger_check_in_status_rcd = 'BOARDED' or passenger_check_in_status_rcd = 'CHECKED'][passenger_type_rcd = 'INF']/pax_weight),'#,##0.00')" /> 
							</xsl:if>
							</TD>
						</TR>
					</xsl:for-each>
					 
				</TABLE>
			</DIV>
			<p/><p/>
			<DIV class="PanelTopHeader" style="font-size: 12px;">&#160;Notes</DIV>
			<DIV class="PanelContainer">
				<table class="Grid" width="659px;">
					<TR class="GridItems">
						<TD>&#160;</TD>
					</TR>
					<TR class="GridItems">
						<TD>&#160;</TD>
					</TR>
					<TR class="GridItems">
						<TD>&#160;</TD>
					</TR>
					<TR class="GridItems">
						<TD>&#160;</TD>
					</TR>
					<TR class="GridItems">
						<TD>&#160;</TD>
					</TR>
				</table>
			</DIV>
		</DIV>
		</DIV>
		</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\Voucher.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->