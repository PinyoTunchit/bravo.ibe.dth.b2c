<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:variable name="PageTitle">tikAERO Sales Revenue Summary</xsl:variable>
	<xsl:variable name="BaseURL">http://www.tikaero.com/xslimages/isk/</xsl:variable> 
	<xsl:variable name="ColorSpan">Red</xsl:variable>
	<xsl:template name="format-date">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<xsl:value-of select="concat($day, $month, $year)" />
	</xsl:template>
 
	<xsl:template match="/">
		<HTML>
		<HEAD>
			<TITLE><xsl:value-of select="$PageTitle"/></TITLE>
			<STYLE>
				.PanelDailySummary .BookingNumber{
					color: #C4102F;
				}

				.PanelDailySummary .PanelHeader
					{
						font: bold 12pt Tahoma, Arial, sans-serif, verdana;
						color: #C4102F;
						/*background: url(../Images/0/H/hbh.jpg);*/
						margin: 5px 0px 5px 5px;
						width: 100%;
					}
				.PanelDailySummary .PanelHeader IMG{
					vertical-align: middle;
				}

				.PanelDailySummary .PanelHeader SPAN{
					padding-left: 4;
				}

				.PanelDailySummary .PanelContainer{
					padding: 10 0 10 20;
					width: 100%;
					display: block;
				}
				.PanelDailySummary .GridHeader
					{
						background: #fdb813;
						color:  #003964;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: bold;
						padding: 2px 2px 2px 5px;
					}
				.PanelDailySummary TR.GridItems
					{
						 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}

				.PanelDailySummary .GridItems TD
					{
						 
						border-bottom: 1px solid #fdb813;
						color:  #003964;
						cursor: pointer;
						font-family: Tahoma, Arial, sans-serif, verdana;
						font-size: 11px;
						font-weight: normal;
						height: 22px;
						padding: 1px 1px 1px 5px;
					}

				.PanelDailySummary .GridFooter TD SPAN
				{
					font-family: Verdana, Arial, Helvetica, "Microsoft Sans Serif";
					font-size: 11px;
					height: 18px;
					padding: 4 5 0 4;
				}

				.PanelDailySummary .FooterTotal
				{
					border-bottom: solid 1px #dddddd;
				}

				.PanelDailySummary SPAN.FooterTotalLabel
					{
							background-color: transparent;
					 		color: #666666;
							font: bold 8pt Tahoma, Arial, sans-serif, verdana;
							height: 22px;
							padding: 1px 2px 1px 5px;
					}

				.PanelDailySummary SPAN.FooterTotalValue
					{
						color: #000066;
						font-size: 11px;
					}
				.PanelDailySummary .BarcodeHeader
				{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 10px;
					font-weight: bold;
					color: #00529d;
					padding: 5 0 3 5;
					margin: 0 0 0 0;
					width: 100%;
					cursor: hand;
				}

				.PanelDailySummary .Barcode
				{
					padding: 1 5 1 5;
				}

				.WelcomeText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				.CommentText{
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 12px;
					padding: 1 10 1 5;
				}
				SPAN.Barcode {
				}
				SPAN.Barcode SPAN{
					background : Black;
					height:30px;
				}
				SPAN.Barcode IMG{
					background : Black;
					height:30px;
				}
				SPAN.Barcode IMG.B11{
					width: 1px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B12{
					width: 1px;
					margin: 0px 2px 0px 0px;
				}
				SPAN.Barcode IMG.B21{
					width: 2px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B22{
					width: 2px;
					margin: 0px 1px 0px 0px;
				}
				SPAN.Barcode IMG.B1{
					width: 1px;
				}
				SPAN.Barcode SPAN.TicketNumber{
						background: White;
						font-family: tahoma, verdana, arial, helvetica, sans-serif;
						font-size: 8pt;
						width: 100%;
						padding-left: 10px;
				}
			</STYLE>
		</HEAD>
		<BODY>
		<DIV class="PanelDailySummary">
		<img src="{$BaseURL}ISK_logo.jpg"/><p/>
		<DIV class="PanelHeader"  style="font-size: 12px;">
		 <SPAN>Sales Revenue Summary for&#160;&#160;<xsl:value-of select="substring(SalesRevenueSummary/Details/report_date,7,2)"/>/<xsl:value-of select="substring(SalesRevenueSummary/Details/report_date,5,2)"/>/<xsl:value-of select="substring(SalesRevenueSummary/Details/report_date,1,4)" />
		 &#160;&#160;to&#160;&#160;<xsl:value-of select="substring(SalesRevenueSummary/Details/report_date_to,7,2)"/>/<xsl:value-of select="substring(SalesRevenueSummary/Details/report_date_to,5,2)"/>/<xsl:value-of select="substring(SalesRevenueSummary/Details/report_date_to,1,4)" />
		 </SPAN>
		</DIV>
		<DIV class="PanelContainer">
					<table class="Table" cellspacing="0"  rules="rows" border="0"  style="border-width:0px;width:100%;border-collapse:collapse;">
						
						<TR  class="GridHeader">
							<TD align="left" width="150">Year</TD>
							<TD align="left" width="150">Month</TD>
							<TD align="Right" width="150">Passengers</TD>
							<TD align="Right" width="150">Gross Revenue</TD>
							<TD align="Right" width="150">Net Revenue</TD>
							<TD align="Right" width="150">Average Fare</TD>
						</TR>
						<xsl:for-each select="SalesRevenueSummary/Details">
						<TR align="left" class="GridItems">
							<td align="Left" width="150">
							<xsl:if test="string(sales_year) > '0'">
								<span><xsl:value-of select="format-number(sales_year,'0000')"/>&#160;</span>
 							</xsl:if>
							</td>
							<td align="Left" width="150">
							<xsl:if test="string(sales_month) > '0'">
								<span><xsl:value-of select="format-number(sales_month,'00')"/>&#160;</span>
							</xsl:if>
							</td>
							<td align="Right" width="150">
							<xsl:if test="string(passenger_count) > '0'">
								<span><xsl:value-of select="format-number(passenger_count,'##,###')"/>&#160;</span>
							</xsl:if>
							</td>
							<td align="Right" width="150">
								 
									<xsl:if test="string(gross_revenue)>'0'">
										 
										<span><xsl:value-of select="format-number(gross_revenue,'#,##0.00')"/>&#160;</span>
									</xsl:if>
							 
							</td>
							<td align="Right" width="150">
								 
									<xsl:if test="string(net_revenue) > '0'">
										 <span><xsl:value-of select="format-number(net_revenue,'#,##0.00')"/>&#160;</span>
									</xsl:if>
								 
							</td>
							<td align="Right" width="150">
								 
									<xsl:if test="string(average_fare) > '0'">
										 
										<span><xsl:value-of select="format-number(average_fare,'#,##0.00')"/>&#160;</span>
									</xsl:if>
								 
							</td>
						</TR>
						</xsl:for-each>
						<TR align="left" class="GridItems">
							<td align="Left" width="150" style="font-weight: bold;">
								<span>Total </span>
							</td>
							 <td align="Left" width="150" style="font-weight: bold;">
								<span>&#160;</span>
							</td>
							<td align="Right" width="150"  style="font-weight: bold;">
							<xsl:if test="string(SalesRevenueSummary/Details/passenger_count) > '0'">
									
								<span><xsl:value-of select="format-number(sum(SalesRevenueSummary/Details/passenger_count),'##,###')"/>&#160;</span>
							</xsl:if>
							</td>
							 
							<td align="Right" width="150" style="font-weight: bold;">
								 
									<xsl:if test="string(SalesRevenueSummary/Details/gross_revenue) > '0'">
										 
										<span><xsl:value-of select="format-number(sum(SalesRevenueSummary/Details/gross_revenue),'#,##0.00')"/>&#160;</span>
									</xsl:if>
						 
							</td>
							<td align="Right" width="150" style="font-weight: bold;">
								 
									<xsl:if test="string(SalesRevenueSummary/Details/net_revenue) > '0'">
										 
										<span><xsl:value-of select="format-number(sum(SalesRevenueSummary/Details/net_revenue),'#,##0.00')"/>&#160;</span>
									</xsl:if>
								 
							</td>
							<td align="Right" width="150" style="font-weight: bold;">
									<xsl:if test="(sum(//Details/net_revenue) div sum(//Details/passenger_count)) > 0">
										<span><xsl:value-of select="format-number(sum(//net_revenue) div sum(//passenger_count),'#,##0.00')"/>&#160;</span>
								</xsl:if>
							 
							</td>
						</TR>
				</table>
			</DIV>
		</DIV>
		</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\SalesRevenueSummary_Sep04_1846.XML" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->