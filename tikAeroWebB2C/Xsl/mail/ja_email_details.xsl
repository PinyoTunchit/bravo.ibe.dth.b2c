<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://xsltsl.org/xsl/documentation/1.0" xmlns:dt="http://xsltsl.org/date-time" xmlns:str="http://xsltsl.org/string" xmlns:CharCode="http://www.tikaero.com/Printing">
	<xsl:key name="flight_id_group" match="//Booking/Tickets/Ticket" use="flight_id"/>
	<xsl:key name="booking_segment_id_group" match="//Booking/TicketQuotes/Flight" use="booking_segment_id"/>
	<xsl:key name="flight_group" match="/Booking/Itinerary/FlightSegment" use="concat(od_origin_rcd, '|', od_destination_rcd)"/>
	<xsl:key name="booking_id_group" match="//Booking/Itinerary/FlightSegment" use="booking_id"/>
	<xsl:key name="flight_number_group" match="//Booking/Itinerary/FlightSegment" use="flight_number"/>
	<xsl:key name="flight_connection_id_group" match="//Booking/Itinerary/FlightSegment" use="flight_connection_id"/>
	<xsl:variable name="booking_source_rcd" select="//Booking/Header/BookingHeader/booking_source_rcd"/>
	<xsl:variable name="client_profile_id" select="//Booking/Header/BookingHeader/client_profile_id"/>
	<xsl:variable name="language_rcd" select="//Booking/Header/BookingHeader/language_rcd"/>
	<xsl:variable name="od_destination_rcd" select="//Booking/Itinerary/FlightSegment/od_destination_rcd"/>
	<xsl:variable name="booking_source">
		<xsl:choose>
			<xsl:when test="$booking_source_rcd = 'INT' ">INT</xsl:when>
			<xsl:when test="$booking_source_rcd = 'B2C' ">B2C</xsl:when>
			<xsl:when test="$booking_source_rcd = 'B2B' ">B2B</xsl:when>
			<xsl:when test="$booking_source_rcd = 'B2B' and  $client_profile_id != '' ">B2E</xsl:when>
			<xsl:when test="$booking_source_rcd = 'int'">INT</xsl:when>
			<xsl:when test="$booking_source_rcd = 'b2c'">B2C</xsl:when>
			<xsl:when test="$booking_source_rcd = 'b2b' ">B2B</xsl:when>
			<xsl:when test="$booking_source_rcd = 'b2b' and  $client_profile_id != '' ">B2E</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="language_check">
		<xsl:choose>
			<xsl:when test="$language_rcd = 'EN' ">ENUS</xsl:when>
			<xsl:when test="$language_rcd = 'JA' ">JAJP</xsl:when>
			<xsl:when test="$language_rcd = 'KO' ">KOKR</xsl:when>
			<xsl:when test="$language_rcd = 'TW'  ">ZHTW</xsl:when>
			<xsl:when test="$language_rcd = 'HK'  ">ZHHK</xsl:when>
			<xsl:when test="$language_rcd = 'CN' ">ZHCN</xsl:when>
			<xsl:when test="$language_rcd = 'en' ">ENUS</xsl:when>
			<xsl:when test="$language_rcd = 'ja' ">JAJP</xsl:when>
			<xsl:when test="$language_rcd ='ko' ">KOKR</xsl:when>
			<xsl:when test="$language_rcd = 'tw' ">ZHTW</xsl:when>
			<xsl:when test="$language_rcd = 'hk' ">ZHHK</xsl:when>
			<xsl:when test="$language_rcd = 'cn' ">ZHCN</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="RowsPerPage">32</xsl:variable>




	<xsl:template name="styleColor">
		<!--Do not  use "Import" or "Include" for StyleSheet (CSS) because it error on EXE, but Web work fine. -->
		<STYLE TYPE="text/css">/*DO NOT DELETE COMMENT BELOW*/
	  /*Modify Date: 18MAR2011*/
	  /**********************************************/
	  body 
	  {
	  color: 
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					8.5pt;
	  }
	  
      table
      {
      border-spacing: 		0px;
      empty-cells: 				show;
      margin: 						0px;
      padding: 					0px;
      }
      
      td								
      {
      vertical-align: 			top;
      }

      th								
      {
      border: 						.09mm;
      border-left: 				0px;
      border-right: 				0px;
      color: 						#e5e5e5;
      text-align: 					center;
      }

      .blueline					
      {
      border: 						.09mm;
      border-bottom: 			solid #e5e5e5 1px;
      }

      .documentheader	
      { 
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					8.5pt;
	  font-weight: 				bold;	
	  height: 						6mm;
	  vertical-align: 			middle;
	  color: 						#e5e5e5; 
      }

      .documenttotal		
      {
      border-bottom: 			solid #e5e5e5 1px;
      color: 						#000000;
      font-weight: 				bold;
      }

      .imgfooter 				
      {
      border-left-color: 		#FFFFFF;
      border-left-style: 		solid;
      border-top-style: 		solid;
      border-width: 			0 0 0 15px;
      left: 							0px;
      }

      .imglogo					
      {
      border-color: 				White;
      border-style: 				none;
      vertical-align: 			top;
      }

      .pagebreak 
      { 
      page-break-after: 		always; 
      }

      .tabledetails				
      {
      border-left: 				solid #ffffff 0px;
      border-right: 				solid #ffffff 0px;
      color: 						#000000;
      width: 						759px;
	  height: 						6mm;
      }

      .tablereportfooter		
      {
      border-bottom: 			solid #e5e5e5 0px;
      border-left: 				solid #e5e5e5 0px;
      border-right: 				solid #e5e5e5 0px;
	  margin: 						0px;
      }

      .tablereportheader	
      {
      border-left: 				solid #ffffff 0px;
      border-right: 				solid #ffffff 0px;
      border-top: 				solid #ffffff 0px;
      width: 						759px;
      }

      .tdheader					
      {
      border-left: 				0px;
      border-right: 				0px;
      border-style: 				none;
      color: 						#031668;
      font-weight: 				bold;
      text-align: 					left;
      }

      .tdmargin					
      { 
      width: 						20px; 
      }

      .tdorderheader			
      {
      border: 						solid 1px #e5e5e5;
      border-left: 				0px;
      border-right: 				0px;
      }

      .tdtotalmargin			
      { width: 						450px; 
      }

      .grid-main 
      {
	  
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					9pt;
	  height: 						4.25mm;
	  color: 						#000000;
	 
     
	 
   
      }

      .grid-top
      {
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					8.5pt;
 	  color: 						#000000;
	  vertical-align: 			middle;
	   border-right: 				.0pt solid;
       background-color: 	white;
	 
     
      }

      .grid-even
      {
	  font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	  font-size: 					8.5pt;
	  color: 						#000000;
	  height: 						6mm;
	  vertical-align: 			middle;
      border-left-style: 		none;
      border-right: 				.0pt solid;
      border-top: 				.0pt solid;
      border-bottom-style: none;
      background-color: 	white;
        
      }

	 p.msonormal	
	 { 
	 color: 							#000000; 
	 font-family: 				"Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial";
	 font-size: 					9pt; 
	 margin-bottom: 			.0001pt; 
	 margin-left: 				0in; 
	 margin-right: 				0in; 
	 margin-top: 				0in; 
	 mso-style-parent: 		""; 
	 text-align: 					justify; 
	 }
	p.msonormal1	
	 { 
	 color: 							#000000; 
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					7pt; 
	 margin-bottom: 			.0001pt; 
	 margin-left: 				0in; 
	 margin-right: 				0in; 
	 margin-top: 				0in; 
	 mso-style-parent: 		""; 
	 text-align: 					justify; 
	 } 
	  
	 h1 
	 {
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					14pt; 
	 color: 						#ffffff; 
	 margin: 						0px; 
	 line-height:					6mm;
	  width: 						673px; 
	  margin: 						0px 0px 0px 36px;
	 background-color: 			#e5e5e5;

	 }
	 
	 h2 
	 {
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					8.5pt; 
	 line-height:					5mm;
	 }
	 
	 h3 
	 {
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					8.5pt;
	 }
	 
	 h4 
	 {
	 font-family: 				Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; 
	 font-size: 					8.5pt;
	 }

	h1+p 
	{
  	margin-top: 					0;
	height: 							6mm;
	}</STYLE>
	</xsl:template>


	<!--HeaderSection-->
	<xsl:template match="Booking/Header">
		<!-- variable Header-->
		<xsl:variable name="BarURL">http://www.avantikjp.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
		<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>

		<br/>
		<table border="0" style=" width:673px; margin: 0px 0px 0px 36px;">
			<tr>
				<td>
					<table border="0" style=" width:300px;">
						<tr>
							<td class="documentheader" style="color: #000000; width: 300px;font-size:8pt;">
								<img alt="PEACH LOGO" src="http://www.avantikjp.com/XSLImages/jad/JAD_Logo.jpg" width="205"/>
								<xsl:choose>
									<xsl:when test="/Booking/Header/BookingHeader[own_agency_flag =1]">

										<br/>flypeach.com にてご予約いただきましてありがとうございます。
										ご予約内容や規約等をご確認ください。</xsl:when>
									<xsl:otherwise>
										<br/>ご予約いただきましてありがとうございます。
										<br/>ご予約内容や規約等をご確認ください。</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table border="0" style="width:365px;color: #000000;border-right:solid #5f497a 2px;border-left:solid #5f497a 2px;border-top:solid #5f497a 2px;border-bottom:solid #5f497a 2px;">
						<tr>
							<td align="left" class="documentheader" style="color: #000000;font-size:11pt;font-weight: bold;">チェックイン用バーコード</td>
							<td>
								<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="150"/>
							</td>
						</tr>
						<tr>
							<td>&#xA0;</td>
							<td class="documentheader" style="color: #000000;font-size:7.5pt;">このバーコードをかざしてください。</td>
						</tr>
						<tr>
							<td class="documentheader" style="color: #000000;font-size:11pt;font-weight: bold;">予約番号　:</td>
							<td class="documentheader" align="left" style="color: #b634bb;font-size:26pt;">
								<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br/>

		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:665px;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;">
			<xsl:choose>
				<xsl:when test="/Booking/Header/BookingHeader[own_agency_flag =1]">

					<tr>
						<td>
							<table cellspacing="0" style="color:#000000;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;color: white; background-color: #b634bb;font-size:10pt;width: 300px;">

								<tr style="color:#ffffff;">
									<td>予約詳細</td>
								</tr>
							</table>
							<table cellspacing="0" style=" border-style: none; font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial; table-layout:fixed;width: 300px;">
								<tr>
									<td>
										<table cellspacing="0" style="width: 300px;">
											<tr>
												<td>
													<table cellspacing="0" style="width: 300px;">
														<tr>
															<td class="documentheader" style="color: #000000;width: 100px;font-size:10pt;">申込者:&#xA0;</td>
															<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">

																<xsl:value-of select="/Booking/Header/BookingHeader/contact_name"/>
															</td>
														</tr>
														<tr>
															<tr>
																<td class="documentheader" style="color: #000000;width: 100px;font-size:10pt;">連絡先1:&#xA0;</td>
																<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">

																	<xsl:value-of select="/Booking/Header/BookingHeader/phone_home"/>
																</td>
															</tr>
															<tr>
																<td class="documentheader" style="color: #000000;width: 100px;font-size:10pt;">連絡先2:&#xA0;</td>
																<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">

																	<xsl:value-of select="/Booking/Header/BookingHeader/phone_mobile"/>
																</td>
															</tr>
															<tr>
																<td class="documentheader" style="color: #000000;width:100px;font-size:10pt;">Eメール:&#xA0;</td>
																<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">

																	<xsl:value-of select="/Booking/Header/BookingHeader/contact_email"/>
																</td>
															</tr>
															<tr>
																<td class="documentheader" style="color: #000000;width:100px;font-size:10pt;">予約日:</td>

																<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">
																	<xsl:call-template name="dt:formatdate">
																		<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
																	</xsl:call-template>&#xA0;</td>
															</tr>
															<tr>
																<td class="documentheader" style="color: #000000;width:100px;font-size:10pt;">最終変更日:&#xA0;</td>
																<td class="documentheader" style="color: #000000;width: 200px;font-size:10pt;">

																	<xsl:call-template name="dt:formatdate">
																		<xsl:with-param name="date" select="/Booking/Header/BookingHeader/update_date_time"/>
																	</xsl:call-template>&#xA0;</td>
															</tr>
														</tr>
													</table>
												</td>
												<td align="right" width="300px">
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table cellspacing="0" style="color:#000000;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;color: white; background-color: #b634bb;font-size:10pt;width: 350px;">

								<tr style="color:#ffffff;">
									<td>Peach のおすすめコンテンツ</td>
								</tr>
							</table>


							<xsl:choose>
								<xsl:when test="/Booking/Itinerary/FlightSegment[domestic_flag = 1]">
									<table border="0" cellspacing="0" style=" border-style: none; table-layout:fixed;width: 350px;">
										<tr>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/domestic_hotel/redirect.html">
													<img alt="ホテル" src="http://www.flypeach.com/itinerary/pc/icon/01.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/jp/ja-jp/rentcar.aspx?pc_Itinerary">
													<img alt="レンタカー" src="http://www.flypeach.com/itinerary/pc/icon/02.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/insulance/redirect.html">
													<img alt="旅行保険" src="http://www.flypeach.com/itinerary/pc/icon/03.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">ホテル</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">レンタカー</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">旅行保険</td>
										</tr>
										<tr>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/baggage/redirect.html?jp">
													<img alt="追加手荷物" src="http://www.flypeach.com/itinerary/pc/icon/04.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/seat/redirect.html?jp">
													<img alt="座席指定" src="http://www.flypeach.com/itinerary/pc/icon/05.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/domestic/change/redirect.html?jp">
													<img alt="予約変更" src="http://www.flypeach.com/itinerary/pc/icon/06.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">追加手荷物</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">座席指定</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">予約変更</td>
										</tr>
									</table>
								</xsl:when>
								<xsl:otherwise>
									<table border="0" cellspacing="0" style=" border-style: none; table-layout:fixed;">
										<tr>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/domestic_hotel/redirect.html">
													<img alt="ホテル" src="http://www.flypeach.com/itinerary/pc/icon/01.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/jp/ja-jp/rentcar.aspx?pc_Itinerary">
													<img alt="レンタカー" src="http://www.flypeach.com/itinerary/pc/icon/02.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/fuchsia/redirect.html">
													<img alt="免税店" src="http://www.flypeach.com/itinerary/pc/icon/03.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">ホテル</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">レンタカー</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">免税店</td>
										</tr>
										<tr>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/baggage/redirect.html?jp">
													<img alt="追加手荷物" src="http://www.flypeach.com/itinerary/pc/icon/04.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/seat/redirect.html?jp">
													<img alt="座席指定" src="http://www.flypeach.com/itinerary/pc/icon/05.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
											<td align="center">
												<a target="_blank" href="http://www.flypeach.com/external/pc/itinerary/international/change/redirect.html?jp">
													<img alt="予約変更" src="http://www.flypeach.com/itinerary/pc/icon/06.png" border="0" style="border-color:#de81d3; "/>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">追加手荷物</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">座席指定</td>
											<td align="center" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;font-size:10pt;">予約変更</td>
										</tr>
									</table>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<td>
							<table border="0" cellspacing="0"
							       style="table-layout:fixed; color:#000000;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;color: white; background-color: #b634bb;font-size:10pt;width: 665px;">
								<tr>
									<td style="color:#ffffff;">予約詳細</td>
								</tr>
							</table>
							<table border="0" cellspacing="0" style="table-layout:fixed; font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;font-size:10pt;width: 665px;">
								<tr>
									<td style="width: 300px;">代理店名 ： &#xA0;<xsl:value-of select="/Booking/Header/BookingHeader/agency_name"/></td>

									<td style="width: 350px;">予約日: &#xA0;<xsl:call-template name="dt:formatdate">
											<xsl:with-param name="date" select="/Booking/Header/BookingHeader/update_date_time"/></xsl:call-template></td>
								</tr>
							</table>
						</td>
					</tr>
				</xsl:otherwise>
			</xsl:choose>
		</table>
		<br/>
		<!--</div>-->
	</xsl:template>
	<!--End HeaderSection-->

	<xsl:variable name="FlightRowsHeader">
		<table cellspacing="0" class="grid-main" style="width: 665px;">
			<tr>

				<td style="color: #000000; background-color: #de81d3; width: 65px" align="left">便名</td>
				<td style="color: #000000; background-color: #de81d3; width: 200px" align="left">出発</td>
				<td style="color: #000000; background-color: #de81d3; width: 200px" align="left">到着</td>
				<td style="color: #000000; background-color: #de81d3; width: 100px" align="left">運賃タイプ</td>
				<td style="color: #000000; background-color: #de81d3; width: 100px" align="left">状態</td>

				<!--<th>Class</th>-->
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Itinerary">

		<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table cellspacing="0" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">

						<tr>
							<td>旅程詳細</td>
						</tr>
					</table>
					<!--	<xsl:copy-of select="$FlightRowsHeader"/>-->
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:for-each select="/Booking/Itinerary/FlightSegment[count(. | key('flight_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">

						<xsl:variable name="od_origin_rcd" select="od_origin_rcd"/>
						<xsl:variable name="od_destination_rcd" select="od_destination_rcd"/>
						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="color: #000000; background-color: #ffffff;margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;font-size:10pt;font-weight:bold;">
							<tr>
								<td>

									<xsl:choose>
										<xsl:when test="position() = 1">

											<span>往路便</span>
											<xsl:copy-of select="$FlightRowsHeader"/>
										</xsl:when>

										<xsl:otherwise>

											<span>復路便</span>
											<xsl:copy-of select="$FlightRowsHeader"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
						</table>
						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;font-size:10pt;">

							<xsl:for-each select="/Booking/Itinerary/FlightSegment[od_origin_rcd = $od_origin_rcd][od_destination_rcd =$od_destination_rcd]">
								<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
								<xsl:variable name="passenger_id" select="passenger_id"/>

								<xsl:value-of select="CharCode:increment()"/>

								<xsl:if test="segment_status_rcd !='US'">
									<tr>

										<td bgcolor="#FFFFFF" style="width:65px;">
											<SPAN>

												<xsl:value-of select="airline_rcd"/>
												<xsl:value-of select="flight_number"/>
												<xsl:if test="string-length(departure_date) = '0'">
													<xsl:text>OPEN</xsl:text>
												</xsl:if>
											</SPAN>
										</td>
										<td bgcolor="#FFFFFF" style="width:200px;">

											<xsl:value-of select="origin_name"/>&#xA0;(<xsl:value-of select="origin_rcd"/>)

											<p class="msonormal"/>
											<xsl:if test="string-length(departure_date) &gt; '0'">
												<xsl:call-template name="dt:formatdate">
													<xsl:with-param name="date" select="departure_date"/>
												</xsl:call-template>&#xA0;
												<xsl:call-template name="get_day_name">
													<xsl:with-param name="utc" select="departure_date"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="string-length(departure_date) = '0'">
												<xsl:text>OPEN</xsl:text>
											</xsl:if>&#xA0;
											<xsl:if test="string(departure_time) != '0'">
												<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
											<p class="msonormal"/> ターミナル： &#xA0;&#xA0;<xsl:value-of select="origin_terminal"/><p/></td>

										<td bgcolor="#FFFFFF" style="width:200px;">
											<xsl:value-of select="destination_name"/>&#xA0;(<xsl:value-of select="destination_rcd"/>)
											<p class="msonormal"/>
											<xsl:if test="string-length(arrival_date) &gt; '0'">
												<xsl:call-template name="dt:formatdate">
													<xsl:with-param name="date" select="arrival_date"/>
												</xsl:call-template>&#xA0;
												<xsl:call-template name="get_day_name">
													<xsl:with-param name="utc" select="arrival_date"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="string-length(arrival_date) = '0'">
												<xsl:text>OPEN</xsl:text>
											</xsl:if>&#xA0;<xsl:if test="string(arrival_time) != '0'">
												<xsl:value-of select="substring(format-number(number(arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(arrival_time), '0000'),3,4)"/></xsl:if>
											<p class="msonormal"/> ターミナル： &#xA0;&#xA0;<xsl:value-of select="destination_terminal"/><p/></td>

										<td bgcolor="#FFFFFF" align="left" style="width:100px;">
											<xsl:for-each select="//Tickets/Ticket[booking_segment_id=$booking_segment_id]">
												<xsl:if test="position()=last()">
													<xsl:value-of select="fare_code"/>
												</xsl:if>
											</xsl:for-each>
										</td>
										<td bgcolor="#FFFFFF" style="width:100px;">

											<xsl:value-of select="status_name"/>
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>

							<!--<xsl:for-each select="/Booking/Itinerary/FlightSegment">-->
						</table>
					</xsl:for-each>
				</td>
			</tr>
		</table>
		<br/>
	</xsl:template>
	<!--End FlightsSection-->
	<!--TicketSection-->
	<xsl:variable name="TicketRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
			<tr bgcolor="#de81d3">

				<td align="left" style="color: #000000;width: 50px;">便名</td>
				<td align="left" style="color: #000000;width: 180px">名前</td>
				<td align="left" style="color: #000000;width: 50px;">性別</td>
				<td align="left" style="color: #000000;width: 120">航空券番号</td>
				<td align="left" style="color: #000000;width: 50px">区分</td>
				<td align="left" style="color: #000000;width: 50px;">座席</td>
				<td align="left" style="color: #000000;width: 80px;">手荷物</td>
				<td align="left" style="color: #000000;width: 85px;">状態</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Tickets">
		<!--Ticket-->
		<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table cellspacing="0" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


						<tr>
							<td>搭乗者情報</td>
						</tr>
					</table>
					<!--<h1>TICKETS</h1>-->
					<xsl:copy-of select="$TicketRowsHeader"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:value-of select="CharCode:increment()"/>

					<xsl:for-each select="/Booking/Tickets/Ticket">
						<xsl:sort select="departure_date" order="ascending"/>


						<xsl:variable name="booking_id" select="booking_id"/>
						<xsl:variable name="booking_segment_id_tax" select="booking_segment_id"/>

						<xsl:variable name="booking_segment_id" select="//Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
						<xsl:variable name="passenger_id" select="passenger_id"/>


						<!-- <xsl:variable name="booking_segment_id" select="booking_segment_id"/> -->

						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
							<xsl:value-of select="CharCode:increment()"/>
							<tr bgcolor="#FFFFFF">
								<td align="left" style="width: 50px;">



									<xsl:value-of select="airline_rcd"/>

									<xsl:value-of select="flight_number"/>
									<xsl:if test="string-length(departure_date) = '0'">
										<xsl:text>OPEN</xsl:text>
									</xsl:if>
								</td>
								<td align="left" style="width: 180px;">
									<xsl:choose>
										<xsl:when test="not(string-length(lastname) &gt; '0')">
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="lastname"/>
											<xsl:text> / </xsl:text>
											<xsl:value-of select="firstname"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
								<td align="left" style="width: 50px;">
									<xsl:value-of select="title"/>
								</td>

								<td align="left" style="width: 120px;">
									<xsl:value-of select="ticket_number"/>
								</td>
								<td style="width:50px;" align="left" valign="top">
									<xsl:value-of select="passenger_type_rcd"/>
								</td>
								<td align="left" style="width: 50px;">
									<xsl:value-of select="seat_number"/>
								</td>
								<td align="left" style="width: 80px;">

									<xsl:value-of select="(sum(//Fees/Fee[passenger_id=$passenger_id][booking_segment_id=$booking_segment_id_tax][void_date_time=''][fee_category_rcd = 'BAGSALES']/number_of_units)+ sum(//Tickets/Ticket[passenger_id=$passenger_id][booking_segment_id=$booking_segment_id_tax][void_date_time='']/piece_allowance))"/>
								</td>
								<td align="left" style="width: 85px;">

									<xsl:for-each select="//Itinerary/FlightSegment[booking_segment_id=$booking_segment_id_tax]">

										<xsl:value-of select="status_name"/>
									</xsl:for-each>
								</td>
							</tr>
						</table>
					</xsl:for-each>
				</td>
			</tr>
		</table>
		<br/>
	</xsl:template>
	<!--End TicketSection-->

	<!--Auxiliaries-->
	<xsl:variable name="RemarkRowsHeader">
		<table cellspacing="0" class="grid-main" style="width: 665px;">
			<tr>
				<td style="color: #000000; background-color: #de81d3;width: 265px;" align="left">Code</td>
				<td style="color: #000000; background-color: #de81d3;width: 400px;" align="left">Description</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Remarks">

		<xsl:if test="count(/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX'])&gt;0">
			<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
				<tr>
					<td>
						<table style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


							<tr>
								<td>Auxiliaries</td>
							</tr>
						</table>
						<!--<h1>AUXILIARIES</h1>-->
						<xsl:copy-of select="$RemarkRowsHeader"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:for-each select="/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
							<xsl:value-of select="CharCode:increment()"/>
							<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">
								<tr bgcolor="#FFFFFF">
									<td align="left" style="width: 265px;">
										<xsl:value-of select="display_name"/>
									</td>
									<td align="left" style="width: 400px;">
										<xsl:value-of select="remark_text"/>
									</td>
								</tr>
							</table>
						</xsl:for-each>
					</td>
				</tr>
			</table>
			<br/>
		</xsl:if>
	</xsl:template>
	<!--End Auxiliaries-->

	<!--Special Services-->
	<xsl:variable name="ServicesRowsHeader">
		<table cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;table-layout:fixed;  width: 665px;">
			<tr>
				<td style="color: #000000; background-color: #de81d3;width: 65px" align="left">便名</td>
				<td style="color: #000000; background-color: #de81d3;width: 300px" align="left">名前</td>
				<td style="color: #000000; background-color: #de81d3;width: 300px" align="left">特別なサポート詳細</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/SpecialServices">
		<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


						<tr>
							<td>特別なサポート</td>
						</tr>
					</table>
					<!--<h1>SPECIAL SERVICES</h1>-->
					<xsl:copy-of select="$ServicesRowsHeader"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')]">
						<xsl:sort select="departure_date" order="ascending"/>
						<!--<xsl:if test="position()=1">-->
						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">
							<xsl:value-of select="CharCode:increment()"/>

							<tr bgcolor="#FFFFFF">
								<td style="width: 65px; border-right: .0pt;">
									<xsl:value-of select="airline_rcd"/>&#xA0;<xsl:value-of select="flight_number"/></td>
								<td style=" border-right: .0pt;width: 300px">
									<xsl:value-of select="lastname"/>/&#xA0;<xsl:value-of select="firstname"/></td>

								<td style=" border-right: .0pt;width: 300px">
									<xsl:value-of select="special_service_rcd"/>
									<xsl:text>&#xA0;</xsl:text>
									<xsl:value-of select="display_name"/>
								</td>
							</tr>
						</table>
						<!--<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
							<xsl:copy-of select="$ReportFooter"/>
							<div style="page-break-after:always">
								<br/>
							</div>
							<xsl:copy-of select="$ReportHeader"/>
							<xsl:copy-of select="$ServicesRowsHeader"/>
						</xsl:if>-->
						<!--</xsl:if>-->
					</xsl:for-each>
					<!--Filler -->
					<xsl:choose>
						<!-- case of only one page-->
						<xsl:when test="CharCode:getCounter() &lt;= $RowsPerPage">
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
		</table>
		<br/>
		<!--End Filler -->
	</xsl:template>
	<!--End Special Services-->


	<!--Passport Information-->

	<xsl:variable name="PassengersRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
			<tr>
				<td align="left" style="width: 165px; color: #000000; background-color: #de81d3;">名前</td>
				<td align="left" style="color: #000000; background-color: #de81d3;width: 150px;">国籍</td>
				<td align="left" style="width: 100px; color: #000000; background-color: #de81d3;">旅券番号</td>
				<td align="left" style="width: 150px;color: #000000; background-color: #de81d3;">発行国</td>
				<td align="left" style="width: 100px; color: #000000; background-color: #de81d3;">有効期間</td>
			</tr>
		</table>
	</xsl:variable>

	<xsl:template match="Booking/Passengers">


		<xsl:if test="count(/Booking/Itinerary/FlightSegment[domestic_flag != 1])">

			<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
				<tr>
					<td>
						<table style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


							<tr>
								<td>パスポート情報</td>
							</tr>
						</table>
						<!--<h1>SPECIAL SERVICES</h1>-->
						<xsl:copy-of select="$PassengersRowsHeader"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:value-of select="CharCode:increment()"/>
						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">
							<xsl:for-each select="//Passengers/Passenger[passport_number !='']">
								<xsl:variable name="passenger_type" select="passenger_type_rcd"/>

								<tr bgcolor="#FFFFFF">
									<td align="left" style="width:165px;">
										<xsl:value-of select="lastname"/>&#xA0;<xsl:value-of select="firstname"/></td>
									<td align="left" style="width: 150px;">
										<xsl:value-of select="nationality_display_name"/>
									</td>
									<td align="left" style="width: 100px;">
										<xsl:if test="document_type_rcd = 'P'">
											<xsl:value-of select="passport_number"/>
										</xsl:if>
									</td>

									<td align="left" style="width: 150px;">
										<xsl:value-of select="passport_issue_country_display_name"/>
									</td>

									<td align="left" style="width: 100px;">
										<xsl:call-template name="dt:formatdate">
											<xsl:with-param name="date" select="passport_expiry_date"/>
										</xsl:call-template>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</td>
				</tr>
			</table>
		</xsl:if>
		<br/>
	</xsl:template>

	<!--End Passport Information-->

	<!--QuoteSection-->
	<xsl:variable name="QuoteRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
			<tr>
				<td align="left" style="width: 465px; color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">項目</td>
				<td align="right" style="width: 200px;color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">計</td>
			</tr>
		</table>
	</xsl:variable>

	<xsl:key name="passenger_type_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<xsl:key name="charge_type_group" match="//TicketQuotes/Total" use="charge_type"/>
	<xsl:template match="Booking/TicketQuotes">

		<xsl:if test="position()=1">
			<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
				<tr>
					<td>
						<table cellspacing="1px" bgcolor="#e5e5e5" style="font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


							<tr>
								<td>お支払内訳</td>
							</tr>
						</table>
						<!--<h1>QUOTES</h1>-->
						<xsl:copy-of select="$QuoteRowsHeader"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:value-of select="CharCode:increment()"/>

						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">

							<xsl:value-of select="CharCode:increment()"/>
							<xsl:for-each select="/Booking/TicketQuotes/Flight[count(. | key('booking_segment_id_group',booking_segment_id)[1]) = 1]">

								<xsl:variable name="booking_segment_id" select="booking_segment_id"/>

								<tr bgcolor="#FFFFFF">

									<td align="left" style="width: 465px;">
										<!--<xsl:value-of select="fare_type_rcd"/>&#xA0;&#xA0;-->
										<xsl:text>Flight&#xA0;-&#xA0;Fare&#xA0;</xsl:text>
										<xsl:value-of select="airline_rcd"/>&#xA0;
										<xsl:value-of select="flight_number"/>
									</td>

									<td style="width: 200px;" align="right">
										<xsl:value-of select="currency_rcd"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="format-number(sum(//TicketQuotes/Flight[booking_segment_id=$booking_segment_id]/charge_amount),'#,###')"/>
									</td>
								</tr>
							</xsl:for-each>


							<xsl:for-each select="//Fees/Fee">

								<xsl:sort select="display_name" order="ascending"/>

								<xsl:if test="position()=1">
									<xsl:value-of select="CharCode:increment()"/>
									<tr bgcolor="#FFFFFF">

										<td align="left" style="width: 465px;">
											<xsl:value-of select="display_name"/>
										</td>

										<td align="right" style="width: 200px;">
											<xsl:value-of select="currency_rcd"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="format-number((fee_amount_incl),'#,##0')"/>
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="position()!=1">
									<xsl:value-of select="CharCode:increment()"/>
									<tr bgcolor="#FFFFFF">
										<td style="width: 465px; border-right: .0pt;">
											<xsl:value-of select="display_name"/>
										</td>

										<td style="width: 200px;" align="right" bgcolor="#FFFFFF">
											<xsl:value-of select="currency_rcd"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="format-number((fee_amount_incl),'#,##0')"/>
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>

							<xsl:value-of select="CharCode:increment()"/>
							<tr bgcolor="#FFFFFF">

								<th colspan="1" style="color:#000000;width: 465px;text-align: left;font-size:10pt;font-weight : bold;">
									<span>合計</span>
								</th>
								<th colspan="1" style="text-align: right;color:#000000;width: 200px;">
									<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
									<xsl:text>&#xA0;</xsl:text>
									<xsl:value-of select="format-number((sum(//TicketQuotes/Flight/charge_amount)) + sum(//Fees/Fee/fee_amount_incl),'#,##0')"/>
								</th>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br/>
		</xsl:if>
	</xsl:template>
	<!--End QuoteSection-->


	<!--PaymentSection-->
	<xsl:variable name="PaymentRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="width: 665px;">
			<tr>
				<td align="left" style="color: #000000; background-color: #de81d3;width: 200px;font-size:10pt;font-weight : bold;">項目</td>
				<td align="left" style="color: #000000;background-color: #de81d3; width: 200px;font-size:10pt;font-weight : bold;">詳細</td>
				<td align="center" style="color: #000000; background-color: #de81d3;width: 65px;font-size:10pt;font-weight : bold;">日付</td>
				<td align="right" style="color: #000000; background-color: #de81d3;width: 100px;font-size:10pt;font-weight : bold;">買上金額</td>
				<td align="right" style="color: #000000; background-color: #de81d3;width: 100px;font-size:10pt;font-weight : bold;">支払金額</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Payments">
		<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table cellspacing="1px" bgcolor="#e5e5e5" style="font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


						<tr>
							<td>お支払状況</td>
						</tr>
					</table>
					<!--<h1>PAYMENTS</h1>-->
					<xsl:copy-of select="$PaymentRowsHeader"/>
					<xsl:value-of select="CharCode:increment()"/>
					<xsl:value-of select="CharCode:increment()"/>
					<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px; width: 665px;">
						<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
						<xsl:variable name="Ticket_total"
						              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee/fee_amount_incl)"/>


						<xsl:choose>
							<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">
								<!--<table cellspacing="0" class="grid-main" style="border-bottom: .0pt;border-top: none;border-left: none; border-right: none; table-layout:fixed; width: 673px; margin: 0px 0px 0px 36px;">-->
								<xsl:value-of select="CharCode:increment()"/>
								<tr bgcolor="#FFFFFF">
									<td align="left" style="width:200px;;">航空券料金と手数料</td>
									<td align="left" style="width:200px;">&#xA0;</td>
									<td align="center" style="width:65px;">&#xA0;</td>
									<td align="right" style="width:100px;">
										<xsl:if test="starts-with(string($Ticket_total), '-')">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0')"/></xsl:if>
										<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
											<xsl:value-of select="format-number($Ticket_total,'#,##0')"/>
										</xsl:if>
									</td>
									<td align="right" style="width:100px;">&#xA0;</td>
								</tr>

								<xsl:for-each select="/Booking/Payments/Payment[void_date_time=''][form_of_payment_subtype_rcd !='REFUND']">
									<xsl:if test="form_of_payment !=''">
										<xsl:value-of select="CharCode:increment()"/>
										<tr bgcolor="#FFFFFF">
											<td align="left" style="width: 200px; ">
												<xsl:choose>
													<xsl:when test="form_of_payment_subtype !=''">
														<xsl:value-of select="form_of_payment_subtype"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="form_of_payment"/>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="left" style="width: 200px;">
												<xsl:value-of select="document_number"/>
											</td>
											<td align="left" style="width: 65px;">
												<xsl:choose>
													<xsl:when test="void_date_time != ''">
														<xsl:text>XX</xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:if test="payment_date_time != ''">
															<xsl:call-template name="dt:formatdate">
																<xsl:with-param name="date" select="payment_date_time"/>
															</xsl:call-template>
														</xsl:if>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" style="width: 100px;">
												<xsl:choose>
													<xsl:when test="not(payment_amount &gt;= 0)">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
														<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0')"/>
													</xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" style="width: 100px;">
												<xsl:choose>
													<xsl:when test="payment_amount &gt; 0">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
														<xsl:value-of select="format-number(payment_amount,'#,##0')"/>
													</xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</xsl:if>
								</xsl:for-each>
								<xsl:value-of select="CharCode:increment()"/>
								<tr bgcolor="#FFFFFF">

									<xsl:variable name="SubTotal" select="$Ticket_total - $Payment_total"/>
									<xsl:choose>
										<xsl:when test="number($SubTotal) &lt; 0">

											<th colspan="3" style="text-align: left;color:#000000;width: 465px;font-size:10pt;font-weight : bold;;">不足金額</th>
											<xsl:if test="number($SubTotal) != 0">
												<xsl:if test="number($SubTotal) &gt;= 0">

													<th colspan="1" style="text-align: right;color:#000000;width: 100px;">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
														<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0')"/>
													</th>
													<th style="color:#000000;width: 100px;">&#xA0;</th>
												</xsl:if>
												<xsl:if test="number($SubTotal) &lt; 0">
													<th colspan="1" style="text-align: right;color:#000000;width: 100px;">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
														<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0')"/>
													</th>
													<th style="color:#000000;width: 100px;">&#xA0;</th>
												</xsl:if>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>

											<th colspan="4" style="text-align: left;color:#000000;border-top:none; border-right-style: none; width: 565px;;font-size:10pt;font-weight : bold;">不足金額</th>
											<th colspan="1" style="text-align: right;color:#000000;border-top:none; border-right-style: none;width: 100px;">
												<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;0&#xA0;</th>
										</xsl:otherwise>
									</xsl:choose>
								</tr>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="CharCode:increment()"/>
								<tr bgcolor="#FFFFFF">
									<td align="left" style="border-top:none; border-right-style: none; width: 200px;;font-size:10pt;font-weight : bold;">航空券料金と手数料</td>
									<td align="left" style="border-top:none; border-right-style: none;width: 200px;">&#xA0;</td>
									<td align="left" style="border-top:none; border-right-style: none; width: 65px;">&#xA0;</td>
									<xsl:if test="starts-with(string($Ticket_total), '-')">
										<td align="left" style="border-top:none; border-right-style: none;width: 100px;">&#xA0;</td>
										<td align="right" style="border-top:none; border-right-style: none;width: 100px;">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
											<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0')"/>
										</td>
									</xsl:if>
									<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
										<td align="right" style="border-top:none; border-right-style: none;width: 100px;">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number($Ticket_total,'#,##0')"/></td>
										<td align="left" style="border-top:none; border-right-style: none;width: 100px;">&#xA0;</td>
									</xsl:if>
								</tr>


								<xsl:for-each select="/Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)][form_of_payment_subtype_rcd !='REFUND'][(payment_amount &gt;= 0)]">
									<xsl:if test="form_of_payment_subtype !=''">
										<xsl:value-of select="CharCode:increment()"/>
										<tr bgcolor="#FFFFFF">
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 200px;">
												<xsl:choose>
													<xsl:when test="form_of_payment_subtype !=''">
														<xsl:value-of select="form_of_payment_subtype"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="form_of_payment"/>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none;width: 200px;">
												<xsl:value-of select="document_number"/>
											</td>
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 65px;">
												<xsl:choose>
													<xsl:when test="void_date_time != ''">
														<xsl:text>XX</xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:if test="payment_date_time != ''">
															<xsl:call-template name="dt:formatdate">
																<xsl:with-param name="date" select="payment_date_time"/>
															</xsl:call-template>
														</xsl:if>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" class="grid-even" style="border-top:none; border-right-style: none;width: 100px;">
												<xsl:choose>
													<xsl:when test="not(payment_amount &gt;= 0)">
														<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" class="grid-even" style="border-top:none; border-right-style: none;text-align: right;width: 100px;">
												<xsl:choose>
													<xsl:when test="payment_amount &gt; 0">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number(payment_amount,'#,##0')"/></xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</xsl:if>

									<xsl:if test="form_of_payment_subtype =''">
										<xsl:value-of select="CharCode:increment()"/>
										<tr bgcolor="#FFFFFF">
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 200px">
												<xsl:value-of select="form_of_payment"/>
											</td>
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none;">
												<xsl:value-of select="document_number"/>
											</td>
											<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 90px">
												<xsl:if test="payment_date_time != ''">
													<xsl:call-template name="dt:formatdate">
														<xsl:with-param name="date" select="payment_date_time"/>
													</xsl:call-template>
												</xsl:if>
											</td>
											<td align="right" class="grid-even" style="border-top:none; border-right-style: none;">
												<xsl:choose>
													<xsl:when test="not(payment_amount &gt;= 0)">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0')"/></xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="right" class="grid-even" style="border-top:none; border-right-style: none;">
												<xsl:choose>
													<xsl:when test="payment_amount &gt; 0">
														<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number(payment_amount,'#,##0')"/></xsl:when>
													<xsl:otherwise>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</xsl:if>
								</xsl:for-each>
								<xsl:value-of select="CharCode:increment()"/>
								<tr bgcolor="#FFFFFF">
									<td align="left" style="border-top:none; border-right-style: none; width: 200px;">不足金額</td>
									<td align="left" style="border-top:none; border-right-style: none;">&#xA0;</td>
									<td align="left" style="border-top:none; border-right-style: none; width: 90px">&#xA0;</td>

									<xsl:variable name="SubTotal" select="number($Ticket_total) - number(sum(//Booking/Payments/Payment[string-length(void_by)!=38][form_of_payment_subtype_rcd !='REFUND'][not(payment_amount &lt; 0)] /payment_amount))"/>

									<xsl:if test="number($SubTotal) = 0">
										<td align="left" style="border-top:none; border-right-style: none;">&#xA0;</td>
										<td align="right" style="border-top:none; border-right-style: none;">
											<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;0&#xA0;</td>
									</xsl:if>
									<xsl:if test="number($SubTotal) != 0">
										<xsl:if test="number($SubTotal) &gt;= 0">
											<td align="left" style="border-top:none; border-right-style: none;">&#xA0;</td>
											<td align="right" style="border-top:none; border-right-style: none;">
												<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;<xsl:value-of select="format-number($SubTotal,'#,##0')"/></td>
										</xsl:if>
										<xsl:if test="number($SubTotal) &lt; 0">
											<td align="right" style="border-top:none; border-right-style: none;">
												<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>&#xA0;
												<xsl:value-of select="format-number($SubTotal,'#,##0')"/>
											</td>
											<td align="left" style="border-top:none; border-right-style: none;">&#xA0;</td>
										</xsl:if>
									</xsl:if>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</table>
				</td>
			</tr>
		</table>

		<br/>
	</xsl:template>
	<!--End Payments-->


	<!--Peach Point-->
	<xsl:variable name="PeachRowsHeader">
		<table border="0" cellspacing="0" class="grid-main" style="margin: 0px 0px 0px 0px;width: 665px;">
			<tr>
				<td align="left" style="width:215px; color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">項目</td>
				<td align="left" style="width:350px; color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">詳細</td>
				<td align="left" style="width: 100px; color: #000000; background-color: #de81d3;font-size:10pt;font-weight : bold;">付与ポイント</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="/Booking">

		<!--
						<xsl:variable name="Refund_total" select="sum(//TicketQuotes/Total[charge_type = 'REFUND']/total_amount)"/>
 						<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
						<xsl:variable name="Ticket_total" select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee/fee_amount_incl)"/>
						<xsl:variable name="SubTotal" select="number($Ticket_total) - number($Payment_total)"/>
												<xsl:choose>
													<xsl:when test="not(number($SubTotal) = 0)">-->

		<xsl:if test="/Booking/Tickets/Ticket[passenger_status_rcd='XX'][e_ticket_status ='R']">
			<table style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
				<tr>
					<td>
						<table style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #d900a6;font-size:12pt;">


							<tr>
								<td>ピーチポイント付与予定<p/></td>
							</tr>
						</table>
						<xsl:copy-of select="$PeachRowsHeader"/>
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:value-of select="CharCode:increment()"/>



						<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style="margin: 0px 0px 0px 0px;border-top: .0pt; border-left: none; width: 665px;">
							<xsl:value-of select="CharCode:increment()"/>
							<xsl:for-each select="/Booking/Tickets/Ticket[passenger_status_rcd ='XX'][e_ticket_status ='R']">
								<tr bgcolor="#FFFFFF">

									<td style="width:215px; border-right: .0pt;">
										<xsl:text>ピーチポイント（付与）</xsl:text>
										<!--<xsl:value-of select="e_ticket_status"/><xsl:value-of select="passenger_status_rcd"/>-->
									</td>
									<td style=" border-right: .0pt;width: 350px">
										<xsl:text>予約番号 :</xsl:text>
										<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text>便名 :</xsl:text>
										<xsl:value-of select="airline_rcd"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="flight_number"/>
									</td>
									<td align="right" style=" border-right: .0pt;width: 100px">

										<xsl:value-of select="currency_rcd"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="format-number(sum(net_total),'#,##0')"/>
									</td>
								</tr>
								<tr bgcolor="#FFFFFF">
									<td style="width:215px; border-right: .0pt;">
										<xsl:text>取消手数料</xsl:text>
									</td>
									<td style=" border-right: .0pt;width: 350px">
										<xsl:text>予約番号 :</xsl:text>
										<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text> </xsl:text>
										<xsl:text>便名 :</xsl:text>
										<xsl:value-of select="airline_rcd"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="flight_number"/>
									</td>

									<td align="right" style="border-right: .0pt;width: 100px">
										<xsl:if test="refund_charge != ''">
											<xsl:text> - </xsl:text>
											<xsl:value-of select="currency_rcd"/>
											<xsl:text> </xsl:text>
											<xsl:value-of select="format-number(sum(refund_charge),'#,##0')"/>&#x20;&#x20;
										</xsl:if>
										<xsl:if test="refund_charge = ''">
											<xsl:value-of select="currency_rcd"/>
											<xsl:text> </xsl:text>
											<xsl:text> 0</xsl:text>
										</xsl:if>
									</td>
								</tr>
							</xsl:for-each>
							<tr bgcolor="#FFFFFF">
								<td style="width:215px; border-right: .0pt;">
									<xsl:text>&#xA0;</xsl:text>
								</td>
								<td style=" border-right: .0pt;width: 350px">
									<xsl:text>&#xA0;</xsl:text>
								</td>

								<td align="right" style=" border-right: .0pt;width: 100px">
									<xsl:text>&#xA0;</xsl:text>
								</td>
							</tr>

							<tr bgcolor="#FFFFFF">
								<td style="width:215px; border-right: .0pt;font-size:10pt;font-weight : bold;">
									<xsl:text>合計付与ポイント</xsl:text>
								</td>
								<td style=" border-right: .0pt;width: 350px">
									<xsl:text> </xsl:text>
								</td>
								<td align="right" style=" font: bold; border-right: .0pt;width: 100px">
									<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="format-number(sum(/Booking/Tickets/Ticket[passenger_status_rcd ='XX'][e_ticket_status ='R']/refundable_amount),'#,##0')"/>&#x20;&#x20;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>


	<!--End Peach Point-->
	<!--FARE RULES-->
	<xsl:template match="//BookingHeader">

		<xsl:copy-of select="$Change_Fiight"/>
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:673px;">
			<tr>
				<td>
					<table border="0" cellspacing="0" style="font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #b634bb;font-size:10pt;">


						<tr>
							<td>ご注意</td>
						</tr>
					</table>
					<table class="grid-main" style="border-top: none;border-left: none; table-layout:fixed; width: 665px;margin: 0px 0px 0px 0px;">
						<xsl:choose>
							<xsl:when test="/Booking/Header/BookingHeader[own_agency_flag =1]">

								<tr bgcolor="#FFFFFF">
									<td>
										<p class="msonormal" style="font-size:9pt;">Peach ではハッピーピーチとハッピーピーチ プラス2種類 の運賃タイプがございます。</p>
										<p class="msonormal" style="font-size:9pt;">予約内容の変更には運賃によって手数料がかかる場合がございます。</p>
										<p class="msonormal" style="font-size:9pt;">コンビニ及びその他支払い方法を選択し、入金期限までに決済されなかった場合、上記予約は自動的にキャンセルとなり、この旅程表は無効となります。</p>

										<p class="msonormal" style="font-size:9pt;">Peach 旅客運送約款及びPeachコンタクトセンターに関しては www.flypeach.com をご覧ください。</p>
										<br/>
										<br/>
										<p class="msonormal" style="font-size:10pt;">Peach 一同、お会いできることを楽しみにしております。</p>
										<br/>
										<br/>
										<p class="msonormal" style="font-size:10pt;">Peach Aviation Limited</p>
										<p class="msonormal" style="font-size:10pt;">
											<a target="_blank" href="http://www.flypeach.com">www.flypeach.com</a>
										</p>
										<p class="msonormal">
										</p>
									</td>
								</tr>
							</xsl:when>
							<xsl:otherwise>
								<tr bgcolor="#FFFFFF">
									<td>
										<p class="msonormal" style="font-size:10pt;">予約内容の変更には運賃タイプによって手数料がかかる場合がございます。詳しくはご予約いただいた代理店へお問い合わせください。</p>
										<p class="msonormal" style="font-size:9pt;">Peach 旅客運送約款に関してはwww.flypeach.comをご覧ください。</p>
										<br/>
										<br/>
										<p class="msonormal" style="font-size:10pt;">Peach 一同、お会いできることを楽しみにしております。</p>
										<br/>
										<br/>
										<p class="msonormal" style="font-size:10pt;">Peach Aviation Limited</p>
										<p class="msonormal" style="font-size:10pt;">
											<a target="_blank" href="http://www.flypeach.com">www.flypeach.com</a>
										</p>
										<p class="msonormal">
										</p>
									</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</table>
					<br/>
					<table cellspacing="0" style="margin: 0px 0px 0px 0px;font-weight: bold;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff; background-color: #b634bb;font-size:8pt;">


						<tr>
							<td align="left">
								<a target="_blank" href="http://www.flypeach.com/jp/ja-jp/termsandconditions.aspx?&amp;utm_source=JAJP&amp;utm_medium=footer_Terms_of_Conditions&amp;utm_campaign=itinerary">
									<font style="color: #ffffff;">利用規約</font>
								</a>|  <a target="_blank" href="http://www.flypeach.com/jp/ja-jp/privacypolicy.aspx?&amp;utm_source=JAJP&amp;utm_medium=footer_privacypolicy&amp;utm_campaign=itinerary"><font style="color: #ffffff;">プライパシーポリシー</font></a></td>
							<td align="right">Copyright© Peach Aviation Limited</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table cellspacing="0" style="margin: 0px 0px 0px 36px;font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;width:665px;color: #ffffff ;font-size:8pt;">

			<tr>
				<td>

					<a target="_blank" href="http://www.flypeach.com/ibe/{$booking_source}/redirect_ad1/{$language_check}/{$od_destination_rcd}.aspx">

						<img border="0" src="http://www.flypeach.com/Portals/0/Images/ibe/{$booking_source}/itinerary/ad1/{$language_check}/{$od_destination_rcd}.jpg"/>
					</a>
				</td>
			</tr>
		</table>
		<br/>
		<br/>



		<div style="page-break-after:always"></div>


		<xsl:if test="/Booking/Header/BookingHeader[own_agency_flag =1]">
			<xsl:copy-of select="$R_OrderRecipient"/>
		</xsl:if>
		<!--<xsl:copy-of select="$R_OrderRowsHeader"/> -->
	</xsl:template>

	<xsl:variable name="R_OrderRecipient">
		<xsl:if test="/Booking/Payments/Payment !=''">
			<table border="0" class="grid-main" style="width:673px; margin: 0px 0px 0px 36px;">
				<tr>
					<td align="right">RECEIPT No.</td>
					<td align="right">
						<xsl:value-of select="/Booking/Payments/Payment/payment_number"/>
					</td>
				</tr>
				<tr>
					<td align="right">表示日</td>
					<td align="right">年   月　日</td>
				</tr>
				<tr>
					<td align="right">DATE OF DISPLAY</td>
					<td align="right">
						<xsl:call-template name="dt:formatdate">
							<xsl:with-param name="date" select="/Booking/Payments/Payment/payment_date_time"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr>
					<td align="center" style="font-size:14pt;color: #000000;">領収書 <p/>RECEIPT</td>
				</tr>
				<tr>
					<td align="left" style="font-size:8pt;color: #000000;">下記、正に領収致しました。<p/>This is to certify that Peach Aviation has received the following.</td>
				</tr>
			</table>
			<br/>
			<table border="0" cellspacing="0" style="color: #000000;width:673px; margin: 0px 0px 0px 36px;">
				<tr>

					<td style="width:100px;font-size:9pt;color: #000000;">宛名 <p/>RECEIVED FROM</td>
					<td align="right" style="font-size:9pt;color: #000000;border-bottom:1px solid #002256;">様</td>
				</tr>
				<tr>

					<td style="width:100px;color: #000000;border-bottom:1px solid #e5e5e5;">&#xA0;</td>
					<td style="font-size:9pt;color: #000000;border-bottom:1px solid #e5e5e5;">&#xA0;</td>
				</tr>
			</table>
			<br/>
			<table class="tableReportHeader" cellspacing="0" style="color: #000000;width:673px; margin: 0px 0px 0px 36px;">

				<tr>
					<td align="left" style="font-size:10pt;color: #000000;">金額 <p class="msonormal"/>THE SUM OF</td>
					<td/>
					<td>

						<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>

						<xsl:variable name="Ticket_total"
						              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = ''][fee_rcd !='INSU']/fee_amount_incl)"/>

						<xsl:value-of select="/Booking/TicketQuotes/Total/currency_rcd"/>&#xA0;

						<xsl:choose>
							<xsl:when test="//Fees/Fee/fee_rcd ='INSU' ">
								<xsl:value-of select="format-number($Ticket_total,'#,##0')"/>
							</xsl:when>

							<xsl:otherwise>
								<!--<xsl:value-of select="format-number(sum(//Payments/Payment/payment_amount),'#,##0')"/>-->
								<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee/fee_amount_incl),'#,##0')"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right" style="font-size:10pt;color: #000000;">(税込/inc)</td>
					<td/>
				</tr>
			</table>
			<br/>

			<table border="0" cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style=" margin: 0px 0px 0px 150px; table-layout:fixed; width: 400px;">


				<tr align="left" bgcolor="#FFFFFF" style="border-top:none; border-right-style: none; width: 200px;">
					<td>明細 / FORM OF PAYMENT</td>
					<!--<td>
                        <xsl:call-template name="dt:formatdate">
                        <xsl:with-param name="date" select="/Booking/Payments/Payment/payment_date_time"/>
                    </xsl:call-template>
                        </td>-->
				</tr>
			</table>
			<xsl:for-each select="/Booking/Payments/Payment[void_date_time='']">
				<xsl:variable name="booking_id" select="booking_id"/>

				<xsl:variable name="booking_payment_id" select="booking_payment_id"/>
				<xsl:variable name="payment_visa" select="/Booking/Payments/Payment[form_of_payment_rcd='CC']/payment_amount"/>


				<xsl:variable name="fee_amount_incl" select="//Fees/Fee[booking_id=$booking_id][fee_rcd ='INSU']/fee_amount_incl"/>


				<xsl:variable name="total_fee_amount_incl" select="(sum($payment_visa))- $fee_amount_incl"/>
				<!--<xsl:variable name="Ticket_total"
        select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = ''][fee_rcd !='INSU']/fee_amount_incl)"/>-->

				<table cellspacing="1px" bgcolor="#e5e5e5" class="grid-main" style=" margin: 0px 0px 0px 150px; table-layout:fixed; width: 400px;">

					<tr bgcolor="#ffffff">
						<td align="left" style="border-top:none; border-right-style: none; width: 200px; ">
							<xsl:choose>
								<xsl:when test="form_of_payment_subtype !=''">
									<xsl:value-of select="form_of_payment_subtype"/>
								</xsl:when>

								<xsl:otherwise>
									<xsl:value-of select="form_of_payment"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td>
							<!--<xsl:call-template name="dt:formatdate">
                        <xsl:with-param name="date" select="/Booking/Payments/Payment/payment_date_time"/>
                    </xsl:call-template> -->

							<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
							<xsl:text> </xsl:text>
							<xsl:choose>
								<xsl:when test="form_of_payment_rcd='CC' and $fee_amount_incl !='' and position()=1">

									<xsl:value-of select="format-number((payment_amount)- $fee_amount_incl,'#,##0')"/>
								</xsl:when>
								<xsl:when test="form_of_payment_rcd='CC' and $fee_amount_incl =''">
									<xsl:value-of select="format-number($payment_visa,'#,##0')"/>
								</xsl:when>

								<xsl:otherwise>
									<xsl:value-of select="format-number(payment_amount,'#,##0')"/>
								</xsl:otherwise>
							</xsl:choose>

							<!-- <xsl:value-of select="format-number($fee_amount_incl,'#,##0')"/> -->
						</td>
					</tr>
				</table>
			</xsl:for-each>
			<br/>

			<table border="0" class="tableReportHeader" cellspacing="0" style="margin: 0px 0px 0px 36px; width: 673px;">
				<tr class="grid-main">
					<td valign="middle" style="text-align: left;height:60px;color:#000000;background-color: white; border-bottom:1px solid #e5e5e5;border-top: solid 1px #e5e5e5;width: 200px;">但し</td>
					<td valign="middle" style="text-align: left;height:60px;color:#000000;background-color: white; border-bottom:1px solid #e5e5e5;border-top: solid 1px #e5e5e5;">運賃および税金・料金等
						<p class="msonormal1">（ただし、上記領収金額には旅行保険料は含まれておりません。旅行保険料の領収書をご入用の場合は、エース保険変更受付センターまでご連絡ください。）</p>
						<p class="msonormal1">エース保険変更受付センターの連絡先は「保険契約手続き完了のご案内」メールをご覧ください。</p>
					</td>
				</tr>
				<tr class="grid-main">
					<td valign="middle" style="text-align: left;height:60px;color:#000000;background-color: white; border-bottom:1px solid #e5e5e5;border-top: solid 1px #e5e5e5;width: 200px;">IN PAYMENT OF</td>
					<td valign="middle" style="text-align: left;height:60px;color:#000000;background-color: white; border-bottom:1px solid #e5e5e5;border-top: solid 1px #e5e5e5;">AIR FARE and TAX/FEE/CHARGE FOR THE FOLLOWING
						<p class="msonormal1">(However, the amount above does not include travel insurance fee. For the receipt of travel insurance, please contact to ACE Travel Insurance Call Center.)</p>
						<p class="msonormal1">Please refer to a thank-you mail from ACE Insurance</p>
					</td>
				</tr>
				<tr valign="middle" class="grid-main">
					<th style="text-align: left;color:#000000;height:40px;background-color: white; border-bottom:1px solid #e5e5e5;">予約番号<p class="msonormal"/>BOOKING NUMBER</th>
					<th align="left" style="text-align: left;color:#000000;height:40px;background-color: white; border-bottom:1px solid #e5e5e5;">
						<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>
					</th>
				</tr>
				<tr valign="middle" class="grid-main">
					<th style="text-align: left;color:#000000;height:40px;background-color: white; border-bottom:1px solid #e5e5e5;">予約日<p class="msonormal"/>BOOKING DATE</th>
					<th style="text-align: left;color:#000000;height:40px;background-color: white; border-bottom:1px solid #e5e5e5;">
						<xsl:call-template name="dt:formatdate">
							<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
						</xsl:call-template>
					</th>
				</tr>
			</table>
			<br/>
			<table border="0" class="tableReportHeader" cellspacing="0" style=" margin: 0px 0px 0px 36px;table-layout:fixed;width: 673px;">

				<tr>
					<td style="height:90px;width: 350px;">
						<img alt="Smile" src="http://www.avantikjp.com/XSLImages/jad/JAD_Logo.jpg" width="150"/>
					</td>
					<td style="height:90px;">
						<td class="grid-main" style="text-align: left;color:#000000; background-color: white;">Peach Aviation  
					株式会社<p class="msonormal"/>Peach Aviation Limited</td>
					</td>
				</tr>
				<tr>
					<td class="grid-main" style="height:90px;text-align: left;color:#000000 ;background-color: white;">本紙は電子的に保持している領収データを画面表示したものです <p class="msonormal"/>
				This is an electronic display of receipt data.</td>
				</tr>
			</table>
		</xsl:if>
		<br/>
	</xsl:variable>
	<!--End FARE RULES-->
	<xsl:variable name="Important">
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:665px;border-right:solid #000000 1px;border-left:solid #000000 1px;border-top:solid #000000 1px;border-bottom:solid #000000 1px;">
			<tr>
				<td>
					<p style="width:660px;color: #ffffff; background-color: #a8b400;font-size:10pt;font-weight : bold; font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;">
						<strong>搭乗にあたって</strong>
					</p>

					<p class="msonormal">搭乗手続きには、本紙のバーコードが必要です。必ず印刷したもの、もしくは携帯電話の旅程表画面をご用意ください。</p>

					<p class="msonormal">バーコード部分は折り曲げたり、雨など水に濡れると正しく認識できない場合がありますので、ご注意ください。</p>
					<br/>
					<p class="msonormal">Peachのチェックインカウンターや到着口は、従来の航空会社と異なる場合がございます。</p>
					<p class="msonormal">大阪（関西）発の国内線・国際線はすべて<strong>関西国際空港の第2ターミナル</strong>で手続きを行っています。</p>
					<p class="msonormal">沖縄（那覇）発の国内線は<strong>LCCターミナル</strong>で、国際線は<strong>国際線旅客ターミナル</strong>で手続きを行っています。</p>
					<p class="msonormal">必ず事前にご確認の上、余裕を持ってお越しください。</p>
					<br/>
					<p class="msonormal">なお、定時出発のため、指定時間内での搭乗手続きをお願いいたします。</p>
					<p class="msonormal">
						<strong>国内線の場合は、出発時刻の</strong><font style="color:red">30分前</font><strong>までにお済ませください。（関西国際空港と那覇空港は25分前まで）</strong>
					</p>
					<p class="msonormal">
						<strong>国際線の場合は、出発時刻の</strong><font style="color:red">50分前</font><strong>までにお済ませください。</strong>
					</p>
					<p class="msonormal">それ以降はご搭乗いただけませんのでご注意ください。また他の便への振替や払戻は致しておりません。</p>
					<br/>
					<p class="msonormal">ご搭乗の際、座席番号をご確認ください。12・13列目は非常口座席のため制限がございます。</p>
					<p class="msonormal">また、<u>11・12列目はリクライニングができません。</u>ご了承ください。</p>
				</td>
			</tr>
		</table>
		<br/>
		<!--<xsl:for-each select="/Booking/Itinerary/FlightSegment[flight_connection_id !=''][count(. | key('flight_connection_id_group',flight_connection_id)[1]) = 1]">

			<xsl:variable name="flight_connection_id" select="flight_connection_id"/>

			<xsl:choose>
				<xsl:when test="position() != 0 ">
					<xsl:if test="position()=last()">-->
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:665px;border-right:solid #000000 1px;border-left:solid #000000 1px;border-top:solid #000000 1px;border-bottom:solid #000000 1px;">
			<tr>
				<td>
					<p style="width:660px;color: #ffffff; background-color: #a8b400;font-size:10pt;font-weight : bold; font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;">
						<strong>乗り継ぎに関して: </strong>
					</p>
					<p class="msonormal">Peachでは、「スルーバゲージサービス、スルーチェックインサービス」は提供しておりません。</p>
					<p class="msonormal">乗継空港到着時	<font style="color:red">にお客様ご自身で</font>次のフライトを<font style="color:red">再度チェックイン</font>して頂く必要があります。</p>
					<p class="msonormal">また、手荷物をお預けの際には、乗継空港到着時に手荷物を受け取り、乗り継ぎの際に<font style="color:red">再度手荷物をお預け頂く</font>必要があります。国際線の乗継便をご利用のお客様は、乗継空港にて出入国及び通関手続きをして頂く必要があります。渡航書類（パスポート、ビザなど）に関する責任はお客様ご自身にありますので、必ず事前に必要な渡航書類をご確認ください。 
</p>
				</td>
			</tr>
		</table>
		<br/>
		<!--</xsl:if>
				</xsl:when>
				<xsl:otherwise>&#xA0;</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>-->
	</xsl:variable>
	<xsl:variable name="Change_Fiight">
		<table border="0" style="margin: 0px 0px 0px 36px;color: #000000; width:665px;border-right:solid #000000 1px;border-left:solid #000000 1px;border-top:solid #000000 1px;border-bottom:solid #000000 1px;">
			<tr>
				<td>
					<p style="width:660px;color: #ffffff; background-color: #a8b400;font-size:10pt;font-weight : bold; font-family: Meiryo, MS PGothic, ヒラギノ角ゴ Pro W3, Osaka, Tahoma, Arial;">
						<strong>変更及びキャンセルについて</strong>
					</p>
					<p class="msonormal">
						<font style="color:red">1）フライト変更をした場合、座席指定または受託手荷物については、改めて手続きを行ってください。</font>
					</p>
					<p class="msonormal">また、その場合は別途座席指定料金、受託手荷物料金がかかりますのでご了承ください。</p>

					<p class="msonormal">
						<font style="color:red">2）お客様都合によるキャンセルは払い戻し不可となっております。</font>
					</p>
					<p class="msonormal">ハッピーピーチ プラスの場合は、ピーチポイントを付与いたします。（要取消手数料）</p>

					<p class="msonormal">発行するまで一週間程度かかりますのでご了承ください。</p>
				</td>
			</tr>
		</table>
		<br/>
	</xsl:variable>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="no" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\_CASE\LVLJG2_intinerary_DE.xml" htmlbaseurl="" outputurl="" processortype="msxmldotnet" useresolver="no" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
		<scenario default="yes" name="Scenario2" userelativepaths="yes" externalpreview="no" url="" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline=""
		          additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->