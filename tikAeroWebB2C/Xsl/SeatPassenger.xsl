<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:tikLanguage="tik:Language">
  <xsl:output method="html" indent="no" encoding="utf-8" omit-xml-declaration="yes"/>
  
	<xsl:template match="/">
		<div class="boxborder-nolinebottom bookinginform">
			<div class="boxheader">Passenger</div>
			<table border="0" class="TBLSeatPerson">
				<tr class="TableHeader">
					<td class="HeadCOL1">
						  <xsl:value-of select="tikLanguage:get('Seat_Reservation_9','Title')" />
					</td>
					
					<td class="HeadCOL2">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_8','Firstname')" />
					</td>
				
					<td class="HeadCOL3">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_7','Lastname')" />
					</td>

					<td class="HeadCOL4">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_10','Type')" />
					</td>
					
					<td class="HeadCOL5">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_11','Seat')" />
					</td>
				</tr>
				
				<xsl:for-each select="Booking/Mapping[flight_id = ../Setting/selected_flight_id]">
					<tr class="NotSelected">
						<td class="BodyCOL1">
							<xsl:value-of select="title_rcd"/>
						</td>
						
						<td class="BodyCOL2">
							<xsl:value-of select="firstname"/>
						</td>
					
						<td class="BodyCOL3">
							<xsl:value-of select="lastname"/>
						</td>

						<td class="BodyCOL4">
							<div id="{concat('dvPaxType',position())}">
								<xsl:value-of select="passenger_type_rcd"/>
							</div>
						</td>
						
						<td class="BodyCOL5">
							<input id="{concat('hdPassengerId',position())}" type="hidden" name="hdSeatPaxId" value="{passenger_id}"/>
							<input id="{concat('hdSegmentId',position())}" type="hidden" name="hdSeatSegmentId" value="{booking_segment_id}"/>
							<input id="{concat('hdSeatRow',position())}" type="hidden" name="hdSeatRow" value="{seat_row}"/>
							<input id="{concat('hdSeatCol',position())}" type="hidden" name="hdSeatCol" value="{seat_column}"/>
							<input id="{concat('hdFeeRcd',position())}" type="hidden" name="hdFeeRcd" value="{seat_fee_rcd}"/>
							<div id="{concat('dvSeatNumber',position())}">
								<xsl:if test="seat_number != 0">
									<xsl:value-of select="seat_number"/>
								</xsl:if>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>