<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt"
	xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:template name="formatdate">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 9,2)" />
		<xsl:variable name="month" select="substring($date,6,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<!--	<xsl:variable name="month" select="substring(substring-after('01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1), 1, 3)" />-->
		<xsl:value-of select="concat($day, '/', $month, '/', $year)" />
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<xsl:value-of select="concat($day, $month, $year)" />
	</xsl:template>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE></TITLE>
          <STYLE>
            .headerWithSub{
            background-color: #F9D43D;
            color: #000000;
            font-family: Tahoma, Arial, sans-serif, verdana;
            font-size: 12px;
            font-weight: bold;
            padding-left: 15px;
            padding-top: 2px;
            padding-bottom: 2px;
            text-align: left;
            }
            .groupForm{
            color			: #575756;
            font			: normal normal normal 11px Verdana, Geneva, Arial, Helvetica, sans-serif;
            margin			: 0px 0px 10px 10px;
            padding			: 2px 4px 0px 15px;
            vertical-align	: middle;
            height			: 20px;
            }

            .groupForm INPUT{
            Width:200px;
            }

            .groupForm SELECT{
            Width:200px;
            }

            .groupFormClear{
            color			: #575756;
            font			: normal normal normal 11px Verdana, Geneva, Arial, Helvetica, sans-serif;
            margin			: 0px 0px 10px 10px;
            padding			: 2px 4px 0px 15px;
            vertical-align	: middle;
            height			: 15px;
            }
          </STYLE>
			</HEAD>
			<BODY leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
				<xsl:for-each select="BonVoyageBreak">
					<table width="760" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
						<tr>
							<td colspan="2" class="headerWithSub">BON VOYAGE BREAK REQUEST</td>
						</tr>
						<tr>
							<td width="220" height="10"></td>
							<td width="540"></td>
						</tr>
						<tr>
							<td class="groupForm">Title:</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ddlTitle" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Lead Name:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="txtLeadName" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Telephone No.:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="txtTelephone" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Email:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="txtEmailAddress" />
								</span>
							</td>
						</tr>
						<tr>
							<td colspan="2" height="10"></td>
						</tr>
						<tr>
							<td colspan="2" class="headerWithSub">Quote &amp; Availability / New Booking</td>
						</tr>
						<tr>
							<td colspan="2" height="10"></td>
						</tr>
						<tr>
							<td class="groupForm">
								Travelling from:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ddlTravelingFrom" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Destination:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ddlDestination" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Hotel: 1st Choice<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ddlHotel1"/>
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">       2nd Choice</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ddlHotel2" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Type of Room:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ddlRoomType" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">Options:</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ddlOptions" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Number of Adults:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="txtAdultNumber" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">Number of Children under 12 Years:</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="txtChildrenNumber" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Please provide ages:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="txtChildrenAge" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Number of Nights:
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="txtNightNumber" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Date of Arrival:<font color="#FF0000">*</font>
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ctlArrivalDate" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Preferred Flight times:
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="cmbPreferedArrivalTime" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Date of Departure:
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="ctlDepatureDate" />
								</span>
							</td>
						</tr>
						<tr>
							<td class="groupForm">
								Preferred Flight times:
							</td>
							<td class="groupForm">
								<span class="groupFormResult">
									<xsl:value-of select="cmbPreferedDepartureTime" />
								</span>
							</td>
						</tr>
						<tr>
							<td colspan="2" height="10"></td>
						</tr>
						
						<tr>
						<td></td>
						<td class="groupFormClear">
							Additional Information:<br /><br />
							<span class="groupFormResult">
								<xsl:value-of select="txtAdditionalInformation" />
							</span>
						</td>
						</tr>
						<tr style="visibility:hidden;">
							<td>
								<table>
									<tr>
										<td colspan="2" class="headerWithSub">Car Hire</td>
									</tr>
									<tr>
										<td colspan="2" height="10"></td>
									</tr>
									<tr>
										<td class="groupForm">
											Car Hire Group:
										</td>
										<td class="groupForm">
											<span class="groupFormResult">
												<xsl:value-of select="ddlCarHireGroup" />
											</span>
										</td>
									</tr>
									<tr>
										<td class="groupForm">
											Rental Period:
										</td>
										<td class="groupForm">
											<span class="groupFormResult">
												<xsl:value-of select="ddlRentalPeriod" />
											</span>
										</td>
									</tr>
									<tr>
										<td class="groupForm">
											Pick Up:
										</td>
										<td class="groupForm">
											<span class="groupFormResult">
												<xsl:value-of select="ddlPickUp" />
											</span>
										</td>
									</tr>
									<tr>
										<td class="groupForm">
											Drop Off:
										</td>
										<td class="groupForm">
											<span class="groupFormResult">
												<xsl:value-of select="ddlDropOff" />
											</span>
										</td>
									</tr>
								</table>
							</td>
						</tr>

					</table>
        </xsl:for-each>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>