<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>
  <xsl:key name="fee_rcd_group" match="Booking/ServiceFees/*" use="fee_rcd"/>
  <xsl:key name="passenger_id_group" match="Booking/Mapping" use="passenger_id"/>
  <xsl:key name="route_group" match="Booking/FlightSegment" use="concat(od_origin_rcd, '|', od_destination_rcd)"/>
  <xsl:template match="/">
    <xsl:variable name="selected_passenger_id" select="Booking/Setting/selected_passenger_id"/>
    <table class="PassengerDetails_Itinerary">
      <tr class="TableHead">
        <td class="HeadCOL1">
          <xsl:value-of select="tikLanguage:get('Booking_Step_4_46','Flight')" />
        </td>
        <td class="HeadCOL2">
          <xsl:value-of select="tikLanguage:get('Booking_Step_4_47','From')" />
        </td>
        <td class="HeadCOL3">
          <xsl:value-of select="tikLanguage:get('Booking_Step_4_48','To')" />
        </td>
        <td class="HeadCOL4">
          <xsl:value-of select="tikLanguage:get('Booking_Step_4_49','Date')" />
        </td>
        <td class="HeadCOL5">
          <xsl:value-of select="tikLanguage:get('Booking_Step_4_50','Dep')" />
        </td>
        <td class="HeadCOL6">
          <xsl:value-of select="tikLanguage:get('Booking_Step_4_51','Arr')" />
        </td>
        <td class="HeadCOL7">
          <xsl:value-of select="tikLanguage:get('Booking_Step_4_52','Seat')" />
        </td>
        <td class="HeadCOL8">&#160;</td>
      </tr>
      <xsl:for-each select="Booking/FlightSegment">
        <xsl:variable name="booking_segment_id" select="booking_segment_id"/>
        <tr>
          <td class="BodyCOL1">
            <xsl:value-of select="airline_rcd" />&#160;
            <xsl:value-of select="flight_number" />
          </td>
          <td class="BodyCOL2">
            <span class="BodyCOL1">
              <xsl:value-of select="origin_name" />
            </span>
          </td>
          <td class="BodyCOL3">
            <span class="BodyCOL1">
              <xsl:value-of select="destination_name" />
            </span>
          </td>
          <td class="BodyCOL4">
            <span class="BodyCOL1">
              <xsl:if test="position() = 1">
                <xsl:attribute name="id">spnFareSummaryDeptDate</xsl:attribute>
              </xsl:if>
              <xsl:call-template name="DateFormat">
                <xsl:with-param name="Date" select="departure_date"></xsl:with-param>
              </xsl:call-template>
            </span>
          </td>
          <td class="BodyCOL5">
            <span class="BodyCOL1">
              <xsl:call-template name="TimeFormat">
                <xsl:with-param name="Time" select="departure_time"></xsl:with-param>
              </xsl:call-template>
            </span>
          </td>
          <td class="BodyCOL6">
            <span class="BodyCOL1">
              <xsl:call-template name="TimeFormat">
                <xsl:with-param name="Time" select="arrival_time"></xsl:with-param>
              </xsl:call-template>
            </span>
          </td>
          <td class="BodyCOL7">
            <xsl:variable name="seat_count" select="count(../Mapping[seat_number != ''][booking_segment_id = $booking_segment_id][passenger_type_rcd != 'INF'])" />
            &#160;
            <xsl:if test="$seat_count != 0">
              <img src="App_Themes/Default/Images/selected_seat_display.png" alt="" title="" />
              <xsl:value-of select="$seat_count"/>
            </xsl:if>
          </td>
          <td class="BodyCOL8">
            <xsl:choose>
              <xsl:when test="temp_seatmap_flag = 0 and seatmap_flag = 0">
                <!--button-->
                <div class="ButtonAlignRight">
                  <div class="buttonCornerLeftGray"></div>
                  <div class="buttonContentGray">
                    <xsl:value-of select="tikLanguage:get('Booking_Step_4_53','Select Seat')" />
                  </div>
                  <div class="buttonCornerRightGray"></div>
                </div>
                <!-- end button-->
              </xsl:when>
              <xsl:otherwise>
                <!--button-->
                <xsl:choose>
                  <xsl:when test="count(../Mapping[passenger_status_rcd = 'OK'][booking_segment_id = $booking_segment_id][seat_number != '']) = count(../Mapping[passenger_status_rcd = 'OK'][booking_segment_id = $booking_segment_id]) and (segment_status_rcd != 'NN')">
                    <div class="ButtonAlignRight">
                      <div class="buttonCornerLeft"></div>
                      <div class="buttonContent">
                        <xsl:value-of select="tikLanguage:get('Booking_Step_4_53','Select Seat')" />
                      </div>
                      <div class="buttonCornerRight"></div>
                    </div>
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="ButtonTable">
                      <a href="javascript:GetSeatMap('{flight_id}', '{origin_rcd}', '{destination_rcd}', '{boarding_class_rcd}', '{booking_class_rcd}')" title="Select Seat" class="defaultbutton">
                        <span>
                          <xsl:value-of select="tikLanguage:get('Booking_Step_4_53','Select Seat')" />
                        </span>
                      </a>
                    </div>
                  </xsl:otherwise>
                </xsl:choose>
                <!-- end button-->
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
      </xsl:for-each>
    </table>
    <div id="dvSsrWrapper">
      <xsl:if test="count(Booking/ServiceFees/*) > 0">

        <li class="header2">
          <xsl:value-of select="tikLanguage:get('Booking_Step_4_71','Special services')" />
        </li>
        <table class="PaxServiceDetail" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="HeadCOL1">
              <xsl:value-of select="tikLanguage:get('Booking_Step_4_72','Service name')" />
            </td>
            <xsl:for-each select="Booking/FlightSegment[count(. | key('route_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">
              <td class="HeadCOL3">
                <xsl:value-of select="od_origin_rcd" />-<xsl:value-of select="od_destination_rcd" />
              </td>
              <td class="HeadCOL2">
                <xsl:value-of select="tikLanguage:get('Booking_Step_4_73','Service cost')" />
              </td>
            </xsl:for-each>
            <td class="HeadCOL5">
              <xsl:value-of select="tikLanguage:get('Booking_Step_4_74','Total cost')" />
            </td>
          </tr>
          <xsl:variable name="currency_rcd" select="Booking/ServiceFees/*[count(currency_rcd) > 0][position() = 1]/currency_rcd" />
          <xsl:for-each select="Booking/ServiceFees/*[count(. | key('fee_rcd_group', fee_rcd)[1]) = 1]">
            <xsl:variable name="row_position" select="position()" />
            <xsl:variable name="fee_rcd" select="fee_rcd" />

            <tr>
              <input type="hidden" name="hdSsrFeeId" value="{fee_id}" />
              <input type="hidden" name="hdSsrCode" value="{$fee_rcd}" />
              <input type="hidden" name="hdSsrOnRequestFlag" value="{service_on_request_flag}" />
              <td class="BodyCOL1">
                <span id="spnSsrDisplayName_{position()}">
                  <xsl:value-of select="display_name"/>
                </span>
              </td>
              <xsl:for-each select="../../FlightSegment[count(. | key('route_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">

                <xsl:variable name="origin_rcd" select="od_origin_rcd" />
                <xsl:variable name="destination_rcd" select="od_destination_rcd" />
                <xsl:variable name="booking_segment_id" select="booking_segment_id" />
                <xsl:variable name="col_position" select="position()" />
                <xsl:choose>
                  <xsl:when test="count(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]) > 0">
                    <td class="BodyCOL3">
                      <xsl:for-each select="../Mapping[count(. | key('passenger_id_group', passenger_id)[1]) = 1]">

                        <xsl:variable name="passenger_id" select="passenger_id" />
                        <xsl:variable name="SsrParam">
                          <xsl:for-each select="../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]">
                            <xsl:variable name="param_origin_rcd" select="origin_rcd" />
                            <xsl:variable name="param_destination_rcd" select="destination_rcd" />

                            {&apos;booking_segment_id&apos; : &apos;<xsl:value-of select="../../FlightSegment[origin_rcd = $param_origin_rcd][destination_rcd = $param_destination_rcd]/booking_segment_id"/>&apos;,
                            &apos;origin_rcd&apos; : &apos;<xsl:value-of select="$param_origin_rcd"/>&apos;,
                            &apos;destination_rcd&apos; : &apos;<xsl:value-of select="$param_destination_rcd"/>&apos;,
                            &apos;passenger_id&apos; : &apos;<xsl:value-of select="$passenger_id"/>&apos;}
                            <xsl:if test="position() != last()">
                              &#44;
                            </xsl:if>
                          </xsl:for-each>
                        </xsl:variable>

                        <xsl:variable name="fee_amount" select="sum(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]/fee_amount_incl)" />
                        <xsl:if test="number($fee_amount) > 0">
                          <select id="seSsrAmount_{concat($row_position, '_', $col_position, '_', position())}"  name="seSsrAmount_{$row_position}" onchange="FillSpecialService();">
                            <xsl:if test="position() > 1">
                              <xsl:attribute name="style">display:none;</xsl:attribute>
                            </xsl:if>
                            <option value="{$SsrParam}">
                              <xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 0">
                                <xsl:attribute name="selected">selected</xsl:attribute>
                              </xsl:if>
                              0
                            </option>
                            <option value="{$SsrParam}">
                              <xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 1">
                                <xsl:attribute name="selected">selected</xsl:attribute>
                              </xsl:if>
                              1
                            </option>
                            <option value="{$SsrParam}">
                              <xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 2">
                                <xsl:attribute name="selected">selected</xsl:attribute>
                              </xsl:if>
                              2
                            </option>
                            <option value="{$SsrParam}">
                              <xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 3">
                                <xsl:attribute name="selected">selected</xsl:attribute>
                              </xsl:if>
                              3
                            </option>
                          </select>
                        </xsl:if>
                      </xsl:for-each>
                    </td>
                    <td class="BodyCOL2">
                      <span id="spnSsrFeeAmount_{$row_position}_{$col_position}">
                        <xsl:value-of select="format-number(sum(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]/fee_amount_incl),'#,##0.00')"/>&#160;
                      </span>
                      <xsl:value-of select="$currency_rcd"/>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="BodyCOL3">
                      &#160;
                    </td>
                    <td class="BodyCOL2">
                      &#160;
                    </td>
                  </xsl:otherwise>
                </xsl:choose>

              </xsl:for-each>
              <td class="BodyCOL5">
                <span id="spnSsrFeeTotal_{$row_position}">
                  0
                </span>
                <xsl:value-of select="$currency_rcd"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </xsl:if>
    </div>
  </xsl:template>
</xsl:stylesheet>