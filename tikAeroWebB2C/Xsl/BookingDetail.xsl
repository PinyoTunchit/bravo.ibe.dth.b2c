<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:asp="remove"
				xmlns:tikLanguage="tik:Language"
				xmlns:JCode="http://www.tiksystems.com/B2C"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt">

	<xsl:output method="html" indent="no" encoding="utf-8" omit-xml-declaration="yes"/>
	<xsl:key name="passenger_type_rcd_group" match="Booking/Passengers/Passenger" use="passenger_type_rcd"/>
	<xsl:key name="fee_id_group" match="//Fees/Fee[string-length(void_by) = 0]" use="fee_id"/>
	<!--Variable-->
	<xsl:variable name="passenger_id" select="//Tickets/Ticket/passenger_id"/>
	<xsl:variable name="booking_segment_id_tax" select="//Tickets/Ticket[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
	<xsl:variable name="booking_segment_id" select="//TicketTaxes/Tax[booking_segment_id = $booking_segment_id_tax]"/>
	<xsl:variable name="Ticket_total" select="(sum(Booking/TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(Booking/TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(Booking/TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(Booking/TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(Booking/Fees/Fee[string-length(void_by) = 0]/fee_amount_incl)"/>
	<xsl:variable name="Payment_total" select="sum(Booking/Payments/Payment[string-length(void_by) = 0]/payment_amount)"/>
	<xsl:variable name="currency_rcd" select="Booking/Header/BookingHeader/currency_rcd" />
	<!-- Define keys used to group elements -->

	<xsl:template name="DateToNumber">
		<xsl:param name="Date">
		</xsl:param>

		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,0,5)"/>
			<xsl:value-of select="substring($Date,5,2)"/>
			<xsl:value-of select="substring($Date,7,2)"/>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="TimeFormat">
		<xsl:param name ="Time"></xsl:param>

		<xsl:if test="string-length($Time) =  3 ">
			0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 4 ">
			<xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 2 ">
			00:<xsl:value-of select="$Time"  />
		</xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
	</xsl:template>
	<xsl:template name="DateFormat">
		<xsl:param name="Date">
		</xsl:param>

		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,7,2)"/>/<xsl:value-of select="substring($Date,5,2)"/>/<xsl:value-of select="substring($Date,0,5)"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="PriceFormat">
		<xsl:param name="Input" />
		<xsl:variable name="temppoint">
			<xsl:value-of select="substring-after($Input,'.')" />
		</xsl:variable>

		<xsl:if test="string-length( substring-before($Input,'.'))=0">
			<xsl:value-of select="$Input"/>.00
		</xsl:if>
		<xsl:if test=" string-length(substring-before($Input,'.'))> 0">
			<xsl:choose>
				<xsl:when test="string-length($temppoint) = 0">
					<xsl:value-of select="substring-before($Input,'.')" />.00
				</xsl:when>
				<xsl:when test="string-length($temppoint) = 1">
					<xsl:value-of select="substring-before($Input,'.')" />.<xsl:value-of select="$temppoint" />0
				</xsl:when>
				<xsl:when test="string-length($temppoint) = 2">
					<xsl:value-of select="$Input" />
				</xsl:when>
				<xsl:when test="string-length($temppoint) = 3">
					<xsl:value-of select="$Input" />
				</xsl:when>
				<xsl:when test="string-length($temppoint) =4">
					<xsl:value-of select="$Input" />
				</xsl:when>
				<xsl:when test="string-length($temppoint) = 5">
					<xsl:value-of select="$Input" />
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>


	<xsl:template name="Itinerary" match="/" >
		<table border="0" class="TBLYourItinerary">
			<tr>
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_7','Flight')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_8','From')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_9','To')" />
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_10','Date')" />
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_11','Dep')" />
				</td>
				<td class="HeadCOL6">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_12','Arr')" />
				</td>
				<td class="HeadCOL7">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_13','Status')" />
				</td>
			</tr>
		</table>
		<!--Show Cancel Segment-->
		<xsl:for-each select="Booking/Itinerary/FlightSegment[segment_status_rcd != 'HK']">

			<xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
			<xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
			<xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
			<xsl:sort select="departure_time" order="ascending" data-type="number"/>

			<xsl:variable name="origin_rcd" select="origin_rcd"/>
			<xsl:variable name="destination_rcd" select="destination_rcd"/>
			<xsl:variable name="flight_connection_id" select="flight_connection_id"/>
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
			<table border="0" class="TBLYourItinerary">
				<tr>
					<td class="BodyCOL1">
						<span>
							<xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
						</span>
					</td>
					<td class="BodyCOL2">
						<span>
							<xsl:choose>
								<xsl:when test="string-length(origin_name) != 0">
									<xsl:value-of select="origin_name" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="origin_name" />
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</td>
					<td class="BodyCOL3">
						<span>
							<xsl:choose>
								<xsl:when test="string-length(destination_name) != 0">
									<xsl:value-of select="destination_name" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="destination_name" />
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</td>
					<td class="BodyCOL4">
						<span>
							<xsl:call-template name="DateFormat">
								<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
							</xsl:call-template>
						</span>
					</td>
					<td class="BodyCOL5">
						<span>
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="departure_time"></xsl:with-param>
							</xsl:call-template>
						</span>
					</td>
					<td class="BodyCOL6">
						<span>
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="arrival_time"></xsl:with-param>
							</xsl:call-template>

						</span>
					</td>
					<td class="BodyCOL7">
						<span>
							<xsl:if test="segment_status_rcd = 'HK'">
								<xsl:value-of select="status_name" />
							</xsl:if>
							<xsl:if test="segment_status_rcd = 'XK'">
								Original Flight
							</xsl:if>
							<xsl:if test="segment_status_rcd = 'NN'">
								New Flight
							</xsl:if>
							<xsl:if test="segment_status_rcd = 'XX'">
								Cancelled
							</xsl:if>
						</span>
					</td>
				</tr>
			</table>
			<!-- TBL Passenger -->
		</xsl:for-each>
		<!--Show Active Segment-->
		<xsl:for-each select="Booking/Itinerary/FlightSegment[segment_status_rcd = 'HK']">

			<xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
			<xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
			<xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>

			<xsl:variable name="origin_rcd" select="origin_rcd"/>
			<xsl:variable name="destination_rcd" select="destination_rcd"/>
			<xsl:variable name="flight_connection_id" select="flight_connection_id"/>
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>

			<table border="0" class="TBLYourItinerary">
				<tr>
					<td class="BodyCOL1">
						<span>
							<xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
						</span>
					</td>
					<td class="BodyCOL2">
						<span>
							<xsl:choose>
								<xsl:when test="string-length(origin_name) != 0">
									<xsl:value-of select="origin_name" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="origin_name" />
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</td>
					<td class="BodyCOL3">
						<span>
							<xsl:choose>
								<xsl:when test="string-length(destination_name) != 0">
									<xsl:value-of select="destination_name" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="destination_name" />
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</td>
					<td class="BodyCOL4">
						<span>
							<xsl:call-template name="DateFormat">
								<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
							</xsl:call-template>
						</span>
					</td>
					<td class="BodyCOL5">
						<span>
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="departure_time"></xsl:with-param>
							</xsl:call-template>
						</span>
					</td>
					<td class="BodyCOL6">
						<span>
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="arrival_time"></xsl:with-param>
							</xsl:call-template>

						</span>
					</td>
					<td class="BodyCOL7">
						<span>
							<xsl:if test="segment_status_rcd = 'HK'">
								<xsl:value-of select="status_name" />
							</xsl:if>
							<xsl:if test="segment_status_rcd = 'XK'">
								Original Flight
							</xsl:if>
							<xsl:if test="segment_status_rcd = 'NN'">
								New Flight
							</xsl:if>
							<xsl:if test="segment_status_rcd = 'XX'">
								Cancelled
							</xsl:if>
						</span>
					</td>
				</tr>
			</table>
			<!-- TBL Passenger -->
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="Passenger" match="/">
		<table border="0" class="TBLPassenger3">
			<tr>
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_15','Client Number')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_16','Lastname')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_17','Firstname')" />
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_18','Title')" />
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_19','Type')" />
				</td>
				<td class="HeadCOL6">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_20','Date of Birth')" />
				</td>
				<td class="HeadCOL7"></td>
			</tr>
			<xsl:for-each select="Booking/Passengers/Passenger">
				<xsl:variable name="passenger_id" select="passenger_id"/>
				<tr>
					<td class="BodyCOL1">
						<xsl:choose>
							<xsl:when test="number(client_number) = 0">
								&#160;
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="client_number"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="BodyCOL2">
						<xsl:value-of select="lastname"/>
					</td>
					<td class="BodyCOL3">
						<xsl:value-of select="firstname"/>
					</td>
					<td class="BodyCOL4">
						<xsl:value-of select="title_rcd"/>
					</td>
					<td class="BodyCOL5">
						<xsl:value-of select="passenger_type_rcd"/>
					</td>
					<td class="BodyCOL6">
						<xsl:choose>
							<xsl:when test="date_of_birth != '0001-01-01T00:00:00'">
								<xsl:call-template name="DateFormat">
									<xsl:with-param name="Date" select="date_of_birth"></xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="BodyCOL7">
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="TicketsAndSeat" match="Booking/Tickets">
		<table border="0" class="TBLTikets">
			<tr>
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_22','E Ticket Number')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_23','ET Status')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_24','Passenger Name')" />
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_7','Flight')" />
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_10','Date')" />
				</td>
				<td class="HeadCOL6">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_19','Type')" />
				</td>
				<td class="HeadCOL7">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_25','Total net')" />
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL8">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_26','Seat')" />
				</td>
			</tr>
			<xsl:for-each select="Booking/Tickets/Ticket">
				<xsl:sort select="ticket_number" order ="ascending"/>
				<xsl:sort select="departure_date" order ="ascending"/>
				<xsl:sort select="firstname" order ="ascending"/>
				<tr>
					<td class="BodyCOL1">
						<xsl:choose>
							<xsl:when test="string-length(ticket_number)!=0">
								<xsl:value-of select="ticket_number"/>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="BodyCOL2">
						<xsl:choose>
							<xsl:when test="count(e_ticket_status) = 0">
								&#160;
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="e_ticket_status = 'U'">
									<xsl:value-of select="'Unavailable'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'R'">
									<xsl:value-of select="'Refunded'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'V'">
									<xsl:value-of select="'Void'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'E'">
									<xsl:value-of select="'Exchange'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'X'">
									<xsl:value-of select="'Print Exchange'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'O'">
									<xsl:value-of select="'Open'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'C'">
									<xsl:value-of select="'Checked'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'L' or e_ticket_status = 'B'">
									<xsl:value-of select="'Boarded'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'F'">
									<xsl:value-of select="'Flown'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'A'">
									<xsl:value-of select="'Airport Control'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'P'">
									<xsl:value-of select="'FIM'"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="BodyCOL3">
						<xsl:value-of select="lastname"/>&#160;<xsl:value-of select="firstname"/>
					</td>
					<td class="BodyCOL4">
						<xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
					</td>
					<td class="BodyCOL5">
						<xsl:call-template name="DateFormat">
							<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
						</xsl:call-template>
					</td>
					<td class="BodyCOL6">
						<xsl:value-of select="passenger_type_rcd"/>
					</td>
					<td class="BodyCOL7">
						&#160;<span>
              <xsl:value-of select="format-number(net_total,'#,##0.00')"/>
						</span>
					</td>
					<td class="BodyCOL8">
						<xsl:if test="seat_number != '0'">
							<xsl:value-of select="seat_number"/>
						</xsl:if>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="CostBreakdown" >
		<table border="0" class="TBLCostBreakDown">
			<tr>
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_28','Passenger')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_29','Charges')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_30','Price Per Person')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_41','VAT')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_31','Total Price')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
			</tr>
			<xsl:for-each select="//Passengers/Passenger[count(. | key('passenger_type_rcd_group', passenger_type_rcd)[1]) = 1]">
				<xsl:variable name="passenger_type_rcd" select="passenger_type_rcd"/>
				<xsl:for-each select="//TicketQuotes/Total[passenger_type_rcd=$passenger_type_rcd]">
					<xsl:variable name="TotalCharge">
						<xsl:if test="charge_type != 'REFUND'">
							<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
						</xsl:if>
						<xsl:if test="charge_type = 'REFUND'">
							<xsl:value-of select="sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
						</xsl:if>
					</xsl:variable>
					<xsl:if test="position()=1">
						<xsl:if test="position()!=last()">
							<tr>
								<td class="BodyCOL1">
									<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)"/>
									&#160;<xsl:value-of select="passenger_type_rcd"/>
								</td>
								<td class="BodyCOL2">
									<xsl:value-of select="charge_name"/>
								</td>
								<td class="BodyCOL3">
									<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
								</td>
								<td class="BodyCOL4">
									<xsl:if test="tax_amount > 0">
										<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
									</xsl:if>
								</td>
								<td class="BodyCOL5">
									<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="position()=last()">
							<tr>
								<td class="BodyCOL1">
									<xsl:value-of select="passenger_count"/>
									&#160;<xsl:value-of select="passenger_type_rcd"/>
								</td>
								<td class="BodyCOL2">
									<xsl:value-of select="charge_name"/>
								</td>
								<td class="BodyCOL3">
									<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
								</td>
								<td class="BodyCOL4">
									<xsl:if test="tax_amount > 0">
										<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;&#xA0;
									</xsl:if>
								</td>
								<td class="BodyCOL5">
									<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
								</td>
							</tr>
						</xsl:if>
					</xsl:if>
					<xsl:if test="position()!=1">
						<xsl:if test="position()=last()">
							<xsl:if test="charge_type != 'REFUND'">
								<tr>
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#xA0;</td>
								</tr>
							</xsl:if>
							<xsl:if test="charge_type = 'REFUND'">
								<tr>
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#xA0;</td>
								</tr>
							</xsl:if>
						</xsl:if>
						<xsl:if test="position()!=last()">
							<xsl:if test="charge_type != 'REFUND'">
								<tr>
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#xA0;</td>
								</tr>
							</xsl:if>
							<xsl:if test="charge_type = 'REFUND'">
								<tr>
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">&#xA0;</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">
										-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
									</td>
								</tr>
							</xsl:if>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>
			<xsl:for-each select="//Fees/Fee[string-length(void_by) = 0][count(. | key('fee_id_group', fee_id)[1]) = 1]">
				<xsl:sort select="fee_id"/>
				<xsl:variable name="fee_id" select="fee_id"/>
				<xsl:for-each select="//Fees/Fee[string-length(void_by) = 0][fee_id = $fee_id]">
					<xsl:if test="position() = 1">
						<tr>
							<td class="BodyCOL1">&#160;</td>
							<td class="BodyCOL2">
								<xsl:value-of select="display_name"/>
							</td>
							<td class="BodyCOL3">
								<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>&#xA0;
							</td>
							<td class="BodyCOL4">
								<xsl:variable name="vat" select="fee_amount_incl - fee_amount" />
								<xsl:if test="$vat > 0">
									<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>&#xA0;
								</xsl:if>
							</td>
							<td class="BodyCOL5">
								<xsl:value-of select="format-number(sum(//Fees/Fee[string-length(void_by) = 0][fee_id = $fee_id]/fee_amount_incl),'#,##0.00')"/>&#xA0;
							</td>
						</tr>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>
			<!--Summary-->
			<tr>
				<td class="BodyCOL1">&#xA0;</td>
				<td class="BodyCOL2">&#xA0;</td>
				<td class="BodyCOL3">&#xA0;</td>
				<td class="BodyCOL4">&#xA0;</td>
				<td class="BodyCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_31','Total')" />
					&#160;&#160;<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;&#xA0;
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="Payment" match="Booking/Payments/Payment">
		<table border="0" class="TBLPaymentReceived ">
			<tr>
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_33','Description')" />
				</td>
				<td class="HeadCOL2">&#xA0;</td>
				<td class="HeadCOL3">&#xA0;</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_34','Credit')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_35','Debit')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
			</tr>
			<tr>
				<td class="BodyCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_36','Ticket Cost &amp; Fee')" />&#160;
				</td>
				<td class="BodyCOL2"></td>
				<td class="BodyCOL3">&#xA0;</td>
				<xsl:if test="starts-with(string($Payment_total), '-')">
					<td class="BodyCOL4">&#xA0;</td>
					<td class="BodyCOL5">
						<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#xA0;
					</td>
				</xsl:if>
				<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
					<td class="BodyCOL4">
						<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;
					</td>
					<td class="BodyCOL5">&#xA0;</td>
				</xsl:if>
			</tr>
			<xsl:for-each select="Booking/Payments/Payment[string-length(void_by) = 0]">
				<xsl:if test="form_of_payment_rcd !=''">
					<tr>
						<td class="BodyCOL1">
							<xsl:variable name="FormOfPayment">
								<xsl:if test="form_of_payment_rcd ='CASH'">
									<xsl:value-of select="form_of_payment_rcd"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='VOUCHER'">
									<xsl:if test="form_of_payment_subtype_rcd = 'GIFT'">
										Gift Certificate
									</xsl:if>
									<xsl:if test="form_of_payment_subtype_rcd = 'PPAID'">
										Prepaid Voucher
									</xsl:if>
									<xsl:if test="form_of_payment_subtype_rcd = 'SPLIT'">
										Booking Split
									</xsl:if>
									<xsl:if test="form_of_payment_subtype_rcd = 'VOUCHER'">
										Voucher
									</xsl:if>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='CC'">
									<xsl:value-of select="form_of_payment_subtype_rcd"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='TKT'">
									<xsl:value-of select="form_of_payment"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='CRAGT'">
									<xsl:value-of select="form_of_payment"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='MANUAL'">
									<xsl:value-of select="form_of_payment_subtype_rcd"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='CHEQUE'">
									<xsl:value-of select="form_of_payment_subtype_rcd"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='BANK'">
									<xsl:value-of select="form_of_payment"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='INV'">
									<xsl:value-of select="form_of_payment_rcd"/>
								</xsl:if>
							</xsl:variable>
							<xsl:if test="$FormOfPayment=''">
								<xsl:if test="form_of_payment_rcd='CRAGT'">
									Credit Agency Account
								</xsl:if>
								<xsl:if test="form_of_payment_rcd!='CRAGT'">
									<xsl:value-of select="form_of_payment_rcd"/>
								</xsl:if>
							</xsl:if>

							<xsl:if test="$FormOfPayment!=''">
								<!--Credit Agency Account -->
								<xsl:value-of select="$FormOfPayment"/>
							</xsl:if>
						</td>
						<td class="BodyCOL2">
							<xsl:if test="form_of_payment_rcd ='INV'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='BANK'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='CHEQUE'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='MANUAL'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='VOUCHER'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='CC'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
						</td>
						<td class="BodyCOL3">
							<xsl:if test="string-length(void_by) > 0">
								<xsl:if test="count(payment_date_time) != 0">
									<xsl:call-template name="DateFormat">
										<xsl:with-param name="Date" select="payment_date_time"/>
									</xsl:call-template>
								</xsl:if>
							</xsl:if>
						</td>
						<td class="BodyCOL4">
							<xsl:choose>
								<xsl:when test="not(payment_amount &gt;= 0)">
									<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;

								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td class="BodyCOL5">
							<xsl:choose>
								<xsl:when test="payment_amount &gt; 0">
									<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;

								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:if>
			</xsl:for-each>

			<tr>
				<td class="BodyCOL1Total">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_37','OUTSTANDING BALANCE')" /> &#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="BodyCOL2Total">&#xA0;</td>
				<td class="BodyCOL3Total">&#xA0;</td>
				<xsl:variable name="SubTotal" select="number(format-number($Ticket_total,'###0.00')) - number(format-number($Payment_total,'###0.00'))"/>
				<xsl:if test="number($SubTotal) = 0">
					<td class="BodyCOL4Total">&#xA0;</td>
					<td class="BodyCOL5Total">
						<xsl:if test="count(//Payments/Payment)=0">
							0.00&#xA0;
						</xsl:if>
						<xsl:if test="count(//Payments/Payment)!=0">
							0.00&#xA0;
						</xsl:if>
					</td>
				</xsl:if>
				<xsl:if test="number($SubTotal) != 0">
					<xsl:if test="number($SubTotal) &gt;= 0">
						<td class="BodyCOL4Total">&#xA0;</td>
						<td class="BodyCOL5Total">
							<xsl:if test="count(//Payments/Payment)=0">
								<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;
							</xsl:if>
							<xsl:if test="count(//Payments/Payment)!=0">
								<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;
							</xsl:if>
						</td>
					</xsl:if>
					<xsl:if test="number($SubTotal) &lt; 0">
						<td class="BodyCOL4Total">
							<xsl:if test="count(//Payments/Payment)=0">
								<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#xA0;
							</xsl:if>
							<xsl:if test="count(//Payments/Payment)!=0">
								<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#xA0;
							</xsl:if>
						</td>
						<td class="BodyCOL5Total">&#xA0;</td>
					</xsl:if>
				</xsl:if>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="/">
		<xsl:variable name="isDateOver">
			<xsl:for-each select="//Itinerary/FlightSegment">
				<xsl:variable name="CurrentDate">
					<xsl:value-of select="JCode:CurrentDate()"/>
				</xsl:variable>
				<xsl:variable name="DepartDate">
					<xsl:call-template name="DateToNumber">
						<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
					</xsl:call-template>
				</xsl:variable>
				<xsl:if test="$CurrentDate > $DepartDate">0</xsl:if>
				<xsl:if test="$DepartDate > $CurrentDate">1</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		
		<div class="WrapperNptext">
			<xsl:if test="Booking/Itinerary/FlightSegment/flight_check_in_status_rcd = 'OPEN' and Booking/Itinerary/FlightSegment/allow_web_checkin_flag = '1'">
				<div class="NB">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_1','NB: This Booking is available for online check-in')" />
				</div>
				<!--button-->
				<div>
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent">
						<a href="../webcheckin/default.aspx?webcheckin={Booking/Header/BookingHeader/booking_id}">
							<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_2','Web Check in')" />
						</a>
					</div>
					<div class="buttonCornerRight"></div>
				</div>
				<!--end button-->
			</xsl:if>
		</div>
		<div class="clearboth"></div>
		<div class="BookingRefItenerary">
			<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_3','Booking Ref')" />&#xA0;
			<span class="BookingRefId">
				<xsl:value-of select="Booking/Header/BookingHeader/record_locator"/>
			</span>
		</div>
		<div class="clearboth"></div>
		<!--button-->
		<div class="StepIndex">
			<div class="buttonStepLeft"></div>
			<div class="buttonStepContent">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_42','Your Confirmed Flight Details')" />
			</div>
			<div class="buttonStepRight"></div>
			<div class="clearboth"></div>
		</div>
		<!-- end button-->
		<!-- TBL Your Itenerary -->
		<div class="WrapperTBLYourItinerary">
			<div class="TabDesc">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_6','Your Itinerary')" />
			</div>
		</div>
		<!--Call Itinerary-->
		<div class="COB" onclick="LoadCob(true, '{Booking/BookingHeader/booking_id}');">
			<xsl:if test="Booking/Header/BookingHeader/booking_source_rcd = 'B2C' or Booking/Header/BookingHeader/booking_source_rcd = ''">
				<xsl:if test="Booking/ClientOnHold != 'T'">
						<xsl:if test="number($isDateOver) != 0">
							<xsl:if test="//Itinerary/FlightSegment[(segment_status_rcd != 'HK')]">
								<xsl:attribute name="style">display:none;</xsl:attribute>
							</xsl:if>
							<xsl:if test="//Itinerary/FlightSegment[(segment_status_rcd = 'HK')] ">
								<xsl:attribute name="style">display:block;</xsl:attribute>
							</xsl:if>
							<div class="buttonCornerLeft"></div>
							<div class="buttonContent">
								<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_5','Change of Booking')" />
							</div>
							<div class="buttonCornerRight"></div>
						</xsl:if>
				</xsl:if>
			</xsl:if>
		</div>
		<div class="clearboth"></div>
		<xsl:call-template name="Itinerary"></xsl:call-template>
		<div class="clearboth"></div>
		<div class="ButtonYourItinerary">
			<div class="clearboth"></div>
			<div class="WrapperTBLYourItinerary">
				<div class="TabDesc">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_14','Passenger')" />
				</div>
			</div>
			<div class="clearboth"></div>
			<!--Call Passenger-->
			<xsl:call-template name="Passenger"></xsl:call-template>
			<!-- TBL Tickets and Seats  -->
			<div class="WrapperTBLYourItinerary">
				<div class="TabDesc">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_21','Tickets and Seats')" />
				</div>
			</div>
			<div class="clearboth"></div>
			<!--Ticket And Seat-->
			<xsl:call-template name="TicketsAndSeat"></xsl:call-template>
			<!-- TBL Cost Breakdown  -->
			<div class="WrapperTBLYourItinerary">
				<div class="TabDesc">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_27','Cost Breakdown')" />
				</div>
			</div>
			<div class="clearboth"></div>
			<!--Call Cost Breakdown-->
			<xsl:call-template name="CostBreakdown"></xsl:call-template>

			<!-- TBL Payment Received   -->
			<div class="WrapperTBLYourItinerary">
				<div class="TabDesc">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_32','Payments Received')" />
				</div>
			</div>
			<div class="clearboth"></div>
			<!--Call Payment-->
			<xsl:call-template name="Payment"></xsl:call-template>
			<!-- End Table-->
		</div>
		<!-- end wrapper -->
		<div class="clearboth"></div>
		<div class="ButtonBelow">
			<div class="BacktoHomepageButton">
				<div class="buttonCornerLeft"></div>
				<xsl:choose>
					<xsl:when test="Booking/Source = 'MM'">
						<div class="buttonContent" onclick="LoadMilleageDetail();">
							<xsl:value-of select="tikLanguage:get('','Back')" />
						</div>
					</xsl:when>
					<xsl:otherwise>
						<div class="buttonContent" onclick="LoadMyBooking();">
							<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_38','Back to Homepage')" />
						</div>
					</xsl:otherwise>
				</xsl:choose>


				<div class="buttonCornerRight"></div>
				<div class="CancelFlight">
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent" onclick="printReport('IF', 'Itinerary');">
						<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_39','Print Itinearary')" />
					</div>
					<div class="buttonCornerRight"></div>
				</div>
				<div class="CancelFlight">
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent" onclick="EmailItineraryInput('{Booking/Header/BookingHeader/contact_email}');">
						<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_40','Email Itinearary')" />
					</div>
					<div class="buttonCornerRight"></div>
				</div>
			</div>
		</div>
	</xsl:template>
	<msxsl:script implements-prefix="JCode" language="JavaScript">
		<![CDATA[    	 
			function CurrentDate()
			{
				var today = new Date();
				return today.getFullYear() + '' + padLeft((today.getMonth() + 1),'0',2) + padLeft(today.getDate(),'0',2);
			}

			function padLeft(str,chr,len)
			{

				if(str.toString().length<len)
				{
					var nChr='';
					for(var i=0;i<(len-str.toString().length);i++)
					{
						nChr=nChr+chr;
					}
					return(nChr+str.toString())
				}
				else
				{
					return str.toString();
				}
			}
		]]>
	</msxsl:script>
</xsl:stylesheet>