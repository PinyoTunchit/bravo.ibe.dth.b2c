<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:tikLanguage="tik:Language">
	
	<xsl:output method="html" indent="no"/>
	<xsl:template name="DateFormat">
		<xsl:param name="Date">
		</xsl:param>
		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
		</xsl:if>
	</xsl:template>

	<xsl:template name="TimeFormat">
		<xsl:param name ="Time"></xsl:param>

		<xsl:if test="string-length($Time) =  3 ">
			0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 4 ">
			<xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 2 ">
			00:<xsl:value-of select="$Time"  />
		</xsl:if>
	</xsl:template>

	<xsl:template match="/">
		<div class="boxborder-nolinebottom">
			<table class="PassengerList">
				<tr class="TableHead">
					<td class="HeadCOL1">&#160;</td>
					<td class="HeadCOL2">
						<xsl:value-of select="tikLanguage:get('Passenger_List_4','Title')" />
					</td>
					<td class="HeadCOL3">
						<xsl:value-of select="tikLanguage:get('Passenger_List_3','Firstname')" />
					</td>
					<td class="HeadCOL4">
						<xsl:value-of select="tikLanguage:get('Passenger_List_2','Lastname')" />
					</td>
					<td class="HeadCOL5">
						<xsl:value-of select="tikLanguage:get('Passenger_List_5','Type')" />
					</td>
					<td class="HeadCOL6">
						<xsl:value-of select="tikLanguage:get('Passenger_List_6','Pax Role')" />
					</td>
				</tr>
				<xsl:for-each select="TikAero/Passenger">
					
					<input id="hdListPassengerId_{position()}" type="hidden" value="{passenger_id}" name="hdListPassengerId" />
					<input id="hdListPassengerProfileId_{position()}" type="hidden" value="{passenger_profile_id}" name="hdListPassengerProfileId" />
					<input id="hdListClientNo_{position()}" type="hidden" value="{client_number}" name="hdListClientNo" />
					<input id="hdListClientProfileId_{position()}" type="hidden" value="{client_profile_id}" name="hdListClientProfileId" />
					<input id="hdListNationality_{position()}" type="hidden" value="{nationality_rcd}" name="hdListNationality" />
					<input id="hdListDocumentType_{position()}" type="hidden" value="{document_type_rcd}" name="hdListDocumentType" />
					<input id="hdListDocumentNo_{position()}" type="hidden" value="{passport_number}" name="hdListDocumentNo" />
					<input id="hdListIssueDate_{position()}" type="hidden" value="{passport_issue_date}" name="hdListIssueDate" />
					<input id="hdListIssuePlace_{position()}" type="hidden" value="{passport_issue_place}" name="hdListIssuePlace" />
					<input id="hdListExpiryDate_{position()}" type="hidden" value="{passport_expiry_date}" name="hdListExpiryDate" />
					<input id="hdListBirthPlace{position()}" type="hidden" value="{passport_birth_place}" name="hdListBirthPlace" />
					<input id="hdListDateOfBirth_{position()}" type="hidden" value="{date_of_birth}" name="hdListDateOfBirth" />
					<input id="hdListGender_{position()}" type="hidden" value="{concat(title_rcd, '|', gender_type_rcd)}" name="hdListDateOfBirth" />
					<input id="hdListWeight_{position()}" type="hidden" value="{passenger_weight}" name="hdListDateOfBirth" />
					<input id="hdListVipFlag_{position()}" type="hidden" value="{vip_flag}" name="hdListVipFlag" />
					<input id="hdListMemberLevel_{position()}" type="hidden" value="{member_level_rcd}" name="hdListMemberLevel" />
					<tr>
						<td class="BodyCOL1">
							<input id="chkClient_{position()}" type="checkbox" name="chkClient" value="{position()}" onclick="CheckPassengerSelect('chkClient_{position()}');" />
						</td>
						<td class="BodyCOL2">
							<div id="dvListTitle_{position()}">
								<xsl:value-of select="title_rcd"/>
							</div>
							
						</td>
						<td class="BodyCOL3">
							<div id="dvListFirstName_{position()}">
								<xsl:value-of select="firstname"/>
							</div>
						</td>
						<td class="BodyCOL4">
							<div id="dvListLastName_{position()}">
								<xsl:value-of select="lastname"/>
							</div>
						</td>
						<td class="BodyCOL5">
							<div id="dvListPaxType_{position()}">
								<xsl:value-of select="passenger_type_rcd"/>
							</div>
						</td>
						<td class="BodyCOL6">
							<div id="dvListPaxRole_{position()}">
								<xsl:value-of select="passenger_role_rcd"/>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>