<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt"
	xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:template name="formatdate">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 9,2)" />
		<xsl:variable name="month" select="substring($date,6,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<!--	<xsl:variable name="month" select="substring(substring-after('01JAN02FEB03MAR04APR05MAY06JUN07JUL08AUG09SEP10OCT11NOV12DEC', $month1), 1, 3)" />-->
		<xsl:value-of select="concat($day, '/', $month, '/', $year)" />
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date" />
		<xsl:param name="format" select="0" />
		<xsl:variable name="day" select="substring($date, 7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,1,4)" />
		<xsl:value-of select="concat($day, $month, $year)" />
	</xsl:template>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE></TITLE>
          <STYLE>
            .headerWithSub{
            background-color: #F9D43D;
            color: #000000;
            font-family: Tahoma, Arial, sans-serif, verdana;
            font-size: 12px;
            font-weight: bold;
            padding-left: 15px;
            padding-top: 2px;
            padding-bottom: 2px;
            text-align: left;
            }
            .groupForm{
            color			: #575756;
            font			: normal normal normal 11px Verdana, Geneva, Arial, Helvetica, sans-serif;
            margin			: 0px 0px 10px 10px;
            padding			: 2px 4px 0px 15px;
            vertical-align	: middle;
            height			: 20px;
            }

            .groupForm INPUT{
            Width:200px;
            }

            .groupForm SELECT{
            Width:200px;
            }

            .groupFormClear{
            color			: #575756;
            font			: normal normal normal 11px Verdana, Geneva, Arial, Helvetica, sans-serif;
            margin			: 0px 0px 10px 10px;
            padding			: 2px 4px 0px 15px;
            vertical-align	: middle;
            height			: 15px;
            }
          </STYLE>
			</HEAD>
			<BODY leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
				<xsl:for-each select="CharterFlights/CharterFlight">
          <table width="760" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
            <tr>
              <td colspan="3" class="headerWithSub">Charter Flight</td>
            </tr>
            <tr>
              <td colspan="3" height="10"></td>
            </tr>
            <tr>
              <td width="60"></td>
              <td width="180"></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2" height="10"></td>
            </tr>
            <tr>
              <td class="groupForm">Title:</td>
              <td class="groupForm">
                  <span class="groupFormResult">
                    <xsl:value-of select="ddlLeadPassengerTitle" />
                  </span>
                </td>
            </tr>
            <tr>
              <td class="groupForm">
                First name:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerFirstName" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">
                Surname:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerLastName" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">Company Name:</td>
              <td class="groupForm">
                  <span class="groupFormResult">
                    <xsl:value-of select="tboCompanyName" />
                  </span>
                </td>
            </tr>
            <tr>
              <td class="groupForm">Mobile No.:</td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerContactMobile"/>
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">
                Home/Business No.:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerContactPhoneNumber" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">
                Email:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerEmailAddress" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">
                Address 1:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerAddress1" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">Address 2:</td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerAddress2" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">
                Town/City:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerPostalTown" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">County:</td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerContactCounty" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">
                Postal Code:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="tbLeadPassengerPostcode" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">
                Country:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                <span class="groupFormResult">
                  <xsl:value-of select="ddlLeadPassengerCountry" />
                </span>
              </td>
            </tr>
            <tr>
              <td class="groupForm">
                Type of Request:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                  <span class="groupFormResult">
                    <xsl:value-of select="cmbTypeOfRequest" />
                  </span>
                </td>
            </tr>
            <tr>
              <td class="groupForm">
                Total Passenger Numbers:<font color="#FF0000">*</font><br/>
                Including Infants
              </td>
              <td class="groupForm">
                  <span class="groupFormResult">
                    <xsl:value-of select="tboTotalPassenger" />
                  </span>
                </td>
            </tr>
            <tr>
              <td class="groupForm">
                Type of Trip:<font color="#FF0000">*</font>
              </td>
              <td class="groupForm">
                  <span class="groupFormResult">
                    <xsl:value-of select="cmbTypeOfTrip" />
                  </span>
               </td>
            </tr>
            <tr>
              <td colspan="3" height="10"></td>
            </tr>
            <tr>
              <td colspan="3" class="headerWithSub">Flight Details</td>
            </tr>
            <tr>
              <td colspan="3" height="10"></td>
            </tr>
            <tr>
              <td colspan="2">&#160;</td>
              <td>
                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                  <tr>
                    <td class="groupForm" width="45%">From:</td>
                    <td class="groupForm" width="55%">To:</td>
                  </tr>
                  <tr>
                    <td class="groupForm">
                      <span class="groupFormResult">
                        <xsl:value-of select="tboOrigin" />
                      </span>
                    </td>
                    <td class="groupForm">
                      <span class="groupFormResult">
                        <xsl:value-of select="tboDestination" />
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td class="groupForm">Date of Departure:</td>
                    <td id="ra" class="groupForm">Date of Return:</td>
                  </tr>
                  <tr>
                    <td class="groupForm">
                      <span class="groupFormResult">
                        <xsl:value-of select="ctlDepatureDate" />
                      </span>
                    </td>
                    <td id="rb" class="groupForm">
                      <span class="groupFormResult">
                        <xsl:value-of select="ctlReturnDate" />
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td class="groupForm">Prefered Departure Time:</td>
                    <td class="groupForm">Prefered Return Departure Time:</td>
                  </tr>
                  <tr>
                    <td class="groupForm">
                      <span class="groupFormResult">
                        <xsl:value-of select="cmbPreferedDepartureTime" />
                      </span>
                    </td>
                    <td class="groupForm">
                      <span class="groupFormResult">
                        <xsl:value-of select="cmbPreferedReturnDepartureTime" />
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" class="groupForm">
                      Additional Information:<br /><br />
                      <span class="groupFormResult">
                        <xsl:value-of select="txtAdditionalInformation" />
                      </span>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </xsl:for-each>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>