<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no"/>
	<xsl:template name="DateFormat">
		<xsl:param name="Date">
		</xsl:param>
		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
		</xsl:if>
	</xsl:template>

	<xsl:template name="TimeFormat">
		<xsl:param name ="Time"></xsl:param>

		<xsl:if test="string-length($Time) =  3 ">
			0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 4 ">
			<xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 2 ">
			00:<xsl:value-of select="$Time"  />
		</xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
	</xsl:template>

	<xsl:template match="/">
		<xsl:variable name="origin_rcd" select="FlightGroup/origin_rcd" />
		<xsl:variable name="destination_rcd" select="FlightGroup/destination_rcd" />
		<xsl:variable name="flight_type" select="FlightGroup/flight_type" />

		<xsl:if test="count(FlightGroup/flight) > 0">
			<div class="WrapperTBLYourFlight">
				<div class="TextFlight">
					<xsl:value-of select="tikLanguage:get('Booking_Step_2_2','Your Flights Options')" />&#160;
					<span class="TextFlightName">
						<xsl:value-of select="concat(FlightGroup/flight/origin_name, ' (', $origin_rcd, ')')" />
					</span>
					&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_10','To')" />&#160;
					<span class="TextFlightName">
						<xsl:value-of select="concat(FlightGroup/flight/destination_name, ' (', $destination_rcd, ')')" />
					</span>
				</div>
			</div>
		</xsl:if>

		<div class="clearboth"></div>
		<div class="LoadingDisable" style="display: block;">
			<div id="{$flight_type}_loading" class="loadingImage">
				<img src="App_Themes/Default/Images/loading.gif" />
			</div>
		</div>
		<div class="WrapperTBLSelectNewFlight">
			<div class="WrapperFlightDate">
				<xsl:variable name="tab_departure_date" select="concat(substring(FlightGroup/departure_date,1,4), substring(FlightGroup/departure_date,6,2), substring(FlightGroup/departure_date,9,2))" />

				<xsl:variable name="DateM2" select="JCode:getDateAdd($tab_departure_date, -2)" />
				<div class="flighttime" onClick="SearchSingleFlight('{$origin_rcd}', '{$destination_rcd}', '{$DateM2}', '{$flight_type}');">
					<xsl:value-of select="JCode:getFormatSelectdate($DateM2)"/>
				</div>

				<xsl:variable name="DateM1" select="JCode:getDateAdd($tab_departure_date, -1)" />
				<div class="flighttime" onClick="SearchSingleFlight('{$origin_rcd}', '{$destination_rcd}', '{$DateM1}', '{$flight_type}');">
					<xsl:value-of select="JCode:getFormatSelectdate($DateM1)"/>
				</div>

				<xsl:variable name="Date1" select="JCode:getDateAdd($tab_departure_date, 0)" />
				<div class="flighttimeSelect">
					<xsl:value-of select="JCode:getFormatSelectdate($Date1)"/>
				</div>

				<xsl:variable name="DateA1" select="JCode:getDateAdd($tab_departure_date, 1)" />
				<div class="flighttime" onClick="SearchSingleFlight('{$origin_rcd}', '{$destination_rcd}', '{$DateA1}', '{$flight_type}');">
					<xsl:value-of select="JCode:getFormatSelectdate($DateA1)"/>
				</div>

				<xsl:variable name="DateA2" select="JCode:getDateAdd($tab_departure_date, 2)" />
				<div class="flighttime" onClick="SearchSingleFlight('{$origin_rcd}', '{$destination_rcd}', '{$DateA2}', '{$flight_type}');">
					<xsl:value-of select="JCode:getFormatSelectdate($DateA2)"/>
				</div>
			</div>
		</div>
		<div class="clearboth"></div>
		<table class="TBLYourFlight">
			<tr>
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Booking_Step_2_3','Route')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Booking_Step_2_4','Date')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Booking_Step_2_5','Departure')" />
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Booking_Step_2_6','Arrival')" />
				</td>
				<td class="HeadCOL5" colspan="2">
					<xsl:value-of select="tikLanguage:get('Booking_Step_2_11','Fare')" />
					&#160;<xsl:value-of select ="concat('(', FlightGroup/currency_rcd, ')')"/>
				</td>
			</tr>
			<xsl:for-each select="FlightGroup/flight">

				<xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
				<xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
				<xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
				<xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>

				<xsl:variable name = "flight_position" select="position()" />

				<xsl:variable name="flight_id" select="flight_id" />
				<xsl:variable name="transit_flight_id" select="transit_flight_id" />
				<xsl:variable name="departure_date" select="concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2))" />
				<xsl:variable name="planned_departure_time" select="planned_departure_time" />
        <xsl:variable name="transit_airport_rcd" select="transit_airport_rcd" />
        <xsl:variable name="transit_departure_date" select="concat(substring(transit_departure_date,1,4), substring(transit_departure_date,6,2), substring(transit_departure_date,9,2))" />
				<tr>
					<td class="BodyCOL1" title="{flight_comment}">
						<xsl:choose>
							<xsl:when test="count(transit_flight_id) = 0 or string-length(transit_flight_id) = 0">
								<xsl:value-of select="concat(origin_name, ' - ', destination_name)" />
							</xsl:when>
							<xsl:otherwise>
								<!--Show Transit Flight-->
								<xsl:value-of select="concat(origin_name,' - ',transit_name)"/>
								<br/>
								<xsl:value-of select="concat(transit_name,' - ',destination_name)"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="BodyCOL2">
						<xsl:call-template name="DateFormat">
							<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
						</xsl:call-template>
					</td>
					<td class="BodyCOL3">
						<xsl:call-template name="TimeFormat">
							<xsl:with-param name="Time" select="planned_departure_time"></xsl:with-param>
						</xsl:call-template>

						<!--Show Transit Flight-->
						<xsl:if test="count(transit_flight_id) > 0">
							<br/>
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="transit_planned_departure_time"></xsl:with-param>
							</xsl:call-template>
						</xsl:if>
					</td>
					<td class="BodyCOL4">
						<xsl:call-template name="TimeFormat">
							<xsl:with-param name="Time" select="planned_arrival_time"></xsl:with-param>
						</xsl:call-template>

						<!--Show Transit Flight-->
						<xsl:if test="count(transit_flight_id) > 0">
							<br/>
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="transit_planned_arrival_time"></xsl:with-param>
							</xsl:call-template>
						</xsl:if>
					</td>
					<td class="BodyCOL5">
						<xsl:choose>
							<xsl:when test="fare/group[position()=1]/full_flight_flag = 1">
								Full
							</xsl:when>
							<xsl:when test="fare/group[position()=1]/class_open_flag = 0">
								Closed
							</xsl:when>
							<xsl:when test="fare/group[position()=1]/close_web_sales = 1">
								Called
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="string-length(flight_id) > 0">
                  <xsl:variable name="flight_param" select="concat('flight_id:',$flight_id,'|',
                                                                      'fare_id:',fare/group[position()=1]/fare_id,'|',
                                                                      'boarding_class_rcd:',fare/group[position()=1]/boarding_class_rcd,'|',
                                                                      'booking_class_rcd:',fare/group[position()=1]/booking_class_rcd,'|',
                                                                      'origin_rcd:',$origin_rcd,'|',
                                                                      'destination_rcd:',$destination_rcd,'|',
                                                                      'departure_date:',$departure_date,'|',
                                                                      'planned_departure_time:',$planned_departure_time,'|',
                                                                      'transit_airport_rcd:',$transit_airport_rcd,'|',
                                                                      'transit_boarding_class_rcd:',fare/group[position()=1]/transit_boarding_class_rcd,'|',
                                                                      'transit_booking_class_rcd:',fare/group[position()=1]/transit_booking_class_rcd,'|',
                                                                      'transit_flight_id:',fare/group[position()=1]/transit_flight_id,'|',
                                                                      'transit_fare_id:',fare/group[position()=1]/transit_fare_id,'|',
                                                                      'transit_departure_date:',$transit_departure_date)"/>
									<input id="opt{concat($flight_type, $flight_position, '_1')}" name="{$flight_type}" type="radio" value="{$flight_param}"/>
									<xsl:value-of select="format-number(fare/group[position()=1]/total_adult_fare,'#,##0.00')"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<xsl:for-each select="fare/group">
					<xsl:if test="position() > 1">
						<tr>
							<td class="BodyCOL1">
								&#160;
							</td>
							<td class="BodyCOL2">
								&#160;
							</td>
							<td class="BodyCOL3">
								&#160;
							</td>
							<td class="BodyCOL4">
								&#160;
							</td>
							<td class="BodyCOL5">
								<xsl:choose>
									<xsl:when test="full_flight_flag = 1">
										Full
									</xsl:when>
									<xsl:when test="class_open_flag = 0">
										Closed
									</xsl:when>
									<xsl:when test="close_web_sales = 1">
										Called
									</xsl:when>
									<xsl:otherwise>
										<xsl:if test="string-length(flight_id) > 0">
                      <xsl:variable name="flight_param" select="concat('flight_id:',$flight_id,'|',
                                                                      'fare_id:',fare_id,'|',
                                                                      'boarding_class_rcd:',boarding_class_rcd,'|',
                                                                      'booking_class_rcd:',booking_class_rcd,'|',
                                                                      'origin_rcd:',$origin_rcd,'|',
                                                                      'destination_rcd:',$destination_rcd,'|',
                                                                      'departure_date:',$departure_date,'|',
                                                                      'planned_departure_time:',$planned_departure_time,'|',
                                                                      'transit_airport_rcd:',$transit_airport_rcd,'|',
                                                                      'transit_boarding_class_rcd:',transit_boarding_class_rcd,'|',
                                                                      'transit_booking_class_rcd:',transit_booking_class_rcd,'|',
                                                                      'transit_flight_id:',transit_flight_id,'|',
                                                                      'transit_fare_id:',transit_fare_id,'|',
                                                                      'transit_departure_date:',$transit_departure_date)"/>
											<input id="opt{concat($flight_type, $flight_position, '_', position())}" name="{$flight_type}" type="radio" value="{$flight_param}"/>
											<xsl:value-of select="format-number(total_adult_fare,'#,##0.00')"/>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>
			<tr>
				<td>
					<xsl:if test="count(FlightGroup/flight) = 0">
						<div class="noflight">No flight found.</div>
					</xsl:if>
				</td>
			</tr>
		</table>

	</xsl:template>
	<msxsl:script implements-prefix="JCode" language="JavaScript">
		<![CDATA[
     
     function getFormatSelectdate(strDate)
     {
        var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
        var weekday = new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");

        return weekday[dtDate.getDay()] + ', ' + strDate.substring(6,8) + '/' + strDate.substring(4,6);
     }
     
     function getDateAdd(strDate, addValue)
     {
        var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
        var strMonth;
        var strday;
        
        dtDate.setDate(dtDate.getDate()+(addValue))

        if ((dtDate.getMonth() + 1).toString().length == 1)
        {
          strMonth = '0' + (dtDate.getMonth() + 1).toString()
        }
        else
        {
          strMonth = (dtDate.getMonth() + 1).toString()
        }

        if (dtDate.getDate().toString().length == 1)
        {
          strday = '0' + dtDate.getDate().toString()
        }
        else
        {
          strday = dtDate.getDate().toString()
        }

        return (dtDate.getYear().toString() + '' + strMonth + '' + strday);         
     }
		]]>
	</msxsl:script>
</xsl:stylesheet>