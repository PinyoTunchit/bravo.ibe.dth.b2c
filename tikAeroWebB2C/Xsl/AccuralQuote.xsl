<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no"/>

	<xsl:key name="booking_segment_id_group" match="NewDataSet/AccuralQuote" use="booking_segment_id"/>

	<xsl:template name="DateFormat">
		<xsl:param name="Date">
		</xsl:param>
		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="TimeFormat">
		<xsl:param name ="Time"></xsl:param>

		<xsl:if test="string-length($Time) =  3 ">
			0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 4 ">
			<xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 2 ">
			00:<xsl:value-of select="$Time"  />
		</xsl:if>
	</xsl:template>

	<xsl:template match="/">
		<div class="TabCostSummaryHeader"></div>
		<div class="TextCostSummary">
			<div class="TextCostSummaryRight">Points</div>
		</div>
		<xsl:for-each select="NewDataSet/AccuralQuote[count(. | key('booking_segment_id_group', booking_segment_id)[1]) = 1]">
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
			<div class="clearboth"></div>
			<div class="TabCostRoute">
				<xsl:value-of select="origin_rcd" />
				to
				<xsl:value-of select="destination_rcd" />
			</div>
			<xsl:for-each select="../AccuralQuote[$booking_segment_id = booking_segment_id]">
				<div class="clearboth"></div>
				<div class="TabCostPointsLeft">
					<xsl:value-of select="display_name" />	
				</div>
				<div class="TabCostPointsRight">
					<xsl:value-of select="ffp_collect"/>
				</div>
				
			</xsl:for-each>
		</xsl:for-each>
		<div class="TabCostRemark ">*Points will be awarded when flights have been completed.</div>
		<div class="TabCostSummaryFooter"></div>
	</xsl:template>
</xsl:stylesheet>