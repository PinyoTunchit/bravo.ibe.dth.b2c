<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no" encoding="utf-8" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:for-each select="ArrayOfAgent/Agent">

			<xsl:if test="issue_ticket_flag = 1">
				<!--Order Default Credit Card-->
				<xsl:if test="agency_payment_type_rcd = 'CC'">

					<xsl:if test="b2b_credit_card_payment_flag = 1 and ../Setting/FormOfPayment/CC/timelimit_flag != 0">
						<div class="PaymentTab first" id="dvCCTab">
							<div id="dvcTCreditCard" class="CreditCard" onclick="javascript:showhidelayer('CreditCard');activateCreditCardControl();">
								<div id="crTabCard">
									<xsl:value-of select="tikLanguage:get('Booking_Step_5_2','Credit Card')" />
								</div>
							</div>
						</div>
                        
                        <div class="PaymentTab" id="dvESewaTab">
                            <div id="dvcTESewa" class="ExternalDisable" onclick="javascript:showhidelayer('ESewa');">
                                <div id="crTabESewa">
                                    <xsl:value-of select="tikLanguage:get('Booking_Step_5_267','ESewa')" />
                                </div>
                            </div>
                        </div>
					</xsl:if>

					<xsl:if test="b2b_voucher_payment_flag = 1  and ../Setting/FormOfPayment/VOUCHER/timelimit_flag != 0">
						<div class="PaymentTab" id="dvVoucherTab">
							<div id="dvcTVoucher" class="VoucherDisable" onclick="javascript:showhidelayer('Voucher')">
								<div id ="crTabVoucher">
									<xsl:value-of select="tikLanguage:get('Booking_Step_5_26','Voucher')" />
								</div>
							</div>	
						</div>
					</xsl:if>

                </xsl:if>
				<!--Order Default Voucher-->
				<xsl:if test="agency_payment_type_rcd = 'VOUCHER' and ../Setting/FormOfPayment/VOUCHER/timelimit_flag != 0">
					<xsl:if test="b2b_voucher_payment_flag = 1">
						<div class="PaymentTab" id="dvVoucherTab">
							<div id="dvcTVoucher" class="Voucher" onclick="javascript:showhidelayer('Voucher')">
								<div id ="crTabVoucher">
									<xsl:value-of select="tikLanguage:get('Booking_Step_5_26','Voucher')" />
								</div>
							</div>
						</div>
					</xsl:if>
					
					<xsl:if test="b2b_credit_card_payment_flag = 1 and ../Setting/FormOfPayment/CC/timelimit_flag != 0">
						<div class="PaymentTab" id="dvCCTab">
							<div id="dvcTCreditCard" class="CreditCardDisable" onclick="javascript:showhidelayer('CreditCard');activateCreditCardControl();">
								<div id="crTabCard">
									<xsl:value-of select="tikLanguage:get('Booking_Step_5_2','Credit Card')" />
								</div>
							</div>
						</div>
					</xsl:if>
				</xsl:if>
			</xsl:if>
			
			<xsl:if test="b2b_post_paid_flag = 1 and ../Setting/FormOfPayment/ATM/timelimit_flag != 0" >
				<div class="PaymentTab" id="dvBookNowTab">
					<div id="dvcTLater" class="BookNowPayLaterDisable" onclick="javascript:showhidelayer('BookNow');">
						<div id="crTabNoPayment">
							<xsl:value-of select="tikLanguage:get('Booking_Step_5_33','Book Now Pay Later')" />
						</div>
					</div>
				</div>
			</xsl:if>
			
			<!-- Start Add External Payment -->
			<div class="PaymentTab" id="dvExternal">
				<div id="dvcTExternal" class="ExternalDisable" onclick="javascript:showhidelayer('External');">
					<div id="crTabNoPayment">
						<xsl:value-of select="tikLanguage:get('Booking_Step_5_57','External')" />
					</div>
				</div>		
			</div>
			<!-- End Add External Payment -->
			
			<!-- Redeem Point-->
			
			<!-- Redeem Point-->
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>

