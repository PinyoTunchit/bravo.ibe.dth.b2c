<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>

  <xsl:template match="/">
    <table class="TBLYourItineraryStep3">
      <tr>
        <td class="HeadCOL1">
			<xsl:value-of select="tikLanguage:get('Booking_Step_3_3','Flight')" />
		</td>
        <td class="HeadCOL2">
			<xsl:value-of select="tikLanguage:get('Booking_Step_3_4','From')" />
		</td>
        <td class="HeadCOL3">
			<xsl:value-of select="tikLanguage:get('Booking_Step_3_5','To')" />
		</td>
        <td class="HeadCOL4">
			<xsl:value-of select="tikLanguage:get('Booking_Step_3_6','Date')" />
		</td>
        <td class="HeadCOL5">
			<xsl:value-of select="tikLanguage:get('Booking_Step_3_7','Dep')" />
		</td>
        <td class="HeadCOL6">
			<xsl:value-of select="tikLanguage:get('Booking_Step_3_8','Arr')" />
		</td>
      </tr>
      <xsl:for-each select="Booking/FlightSegment">
        <tr>
          <td class="BodyCOL1">
            <xsl:value-of select="airline_rcd" />&#160;
            <xsl:value-of select="flight_number" />
          </td>
          <td class="BodyCOL2">
            <xsl:value-of select="origin_name" />
          </td>
          <td class="BodyCOL3">
            <xsl:value-of select="destination_name" />
          </td>
          <td class="BodyCOL4">
            <xsl:call-template name="DateFormat">
              <xsl:with-param name="Date" select="departure_date"></xsl:with-param>
            </xsl:call-template>
          </td>
          <td class="BodyCOL5">
            <xsl:call-template name="TimeFormat">
              <xsl:with-param name="Time" select="departure_time"></xsl:with-param>
            </xsl:call-template>
          </td>
          <td class="BodyCOL6">
            <xsl:call-template name="TimeFormat">
              <xsl:with-param name="Time" select="arrival_time"></xsl:with-param>
            </xsl:call-template>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>
</xsl:stylesheet>