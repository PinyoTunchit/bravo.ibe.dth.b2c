<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                      xmlns:JCode="http://www.tiksystems.com/B2C"
                      xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							        xmlns:tikLanguage="tik:Language">
  <xsl:output method="html" indent="no"/>
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:if test="$Date != '0001-01-01T00:00:00'">
        <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>
  <xsl:template match="/">

    <xsl:for-each select="Passenger/NewDataSet/Passenger">
      <li class="Right">
        <div class="GrayButon" id="bttPassenEdit_{position()}">
          <div id="bttPassEdit_{position()}"  class="GrayButtonLeft"></div>
          <div class="GrayButtonContent"  onclick="enableFieldPassenger('{position()}');">
            <xsl:value-of select="tikLanguage:get('Registration_51','Edit')" />
          </div>
          <div class="GrayButtonShowAll"></div>
        </div>
        <div class="GrayButon" id="bttPassenSave_{position()}">
          <div id="bttPassSave_{position()}"  class="GrayButtonLeft"></div>
          <div class="GrayButtonContent"  onclick="ClientProfileSave('ctl00_','{position()}');">
            <xsl:value-of select="tikLanguage:get('Registration_50','Save')" />
          </div>
          <div class="GrayButtonShowAll"></div>
        </div>
      </li>
      <div class="BoxGrayContentHeaderPassenger">
        <input id="hdPassengerId_{position()}" type="hidden" value="{passenger_id}" name="hdPassengerId" />
        <input id="hdpassenger_profile_id_{position()}" type="hidden" value="{passenger_profile_id}" name="hdpassenger_profile_id" />
        <input id="hdclient_profile_id_{position()}" type="hidden" value="{client_profile_id}" name="hdclient_profile_id" />
        <input id="hdclient_number_{position()}" type="hidden" value="{client_number}" name="hdclient_number" />
        <input id="hdwheelchair_flag_{position()}" type="hidden" value="{wheelchair_flag}" name="hdwheelchair_flag" />
        <input id="hdvip_flag_{position()}" type="hidden" value="{vip_flag}" name="hdvip_flag" />
        <input id="hdpassenger_weight_{position()}" type="hidden" value="{passenger_weight}" name="hdpassenger_weight" />
      </div>
      <ul id="ulPassenger_{position()}">
        <li>
          <img src="App_Themes/Default/Images/expand.gif" />
          <xsl:value-of select="tikLanguage:get('Registration_52','Passenger')" />
          <xsl:value-of select="format-number(position(),'000')"/>
          <ul>
            <li>
              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_27','Title')" />
              </div>
              <div class="Detail">
                <select id="ddlPsgTitle_{position()}" class="title">
                  <option value="" class="watermarkOn"></option>
                  <xsl:variable name="title_rcd" select="title_rcd"/>
                  <xsl:for-each select="//Passenger/setup/PassengerTitels">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="concat(title_rcd, '|', gender_type_rcd)"/>
                      </xsl:attribute>
                      <xsl:choose>
                        <xsl:when test="string-length($title_rcd) > 0">
                          <xsl:if test="$title_rcd = title_rcd">
                            <xsl:attribute name="selected">selected</xsl:attribute>
                          </xsl:if>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:if test="../Setting/DefaultTitle = title_rcd">
                            <xsl:attribute name="selected">selected</xsl:attribute>
                          </xsl:if>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:value-of select="display_name"/>
                    </option>
                  </xsl:for-each>
                </select>
              </div>

              <div class="clearboth"></div>

              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_9','Passenger Role')" />
              </div>
              <div class="Detail">
                <select id="ddlPassengerRole_{position()}">
                  <xsl:variable name="passenger_role_rcd" select="passenger_role_rcd"/>
                  <xsl:variable name="passenger_position" select="position()"/>
                  <option value="" class="watermarkOn"></option>
                  <xsl:for-each select="../../setup/PassengerRole">
                    <xsl:choose>
                      <xsl:when test="$passenger_position != 1 and passenger_role_rcd = 'MYSELF'">
                      </xsl:when>
                      <xsl:otherwise>
                        <option value="{passenger_role_rcd}">
                          <xsl:if test="passenger_role_rcd = $passenger_role_rcd">
                            <xsl:attribute name="selected">selected</xsl:attribute>
                          </xsl:if>
                          <xsl:value-of select="display_name" />
                        </option>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </select>
              </div>


              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_3','Passenger Type')" />
              </div>
              <div class="Detail">
                <select id="ddlPassengerType_{position()}">
                  <option value="" class="watermarkOn"></option>
                  <xsl:variable name="passenger_type_rcd" select="passenger_type_rcd"/>
                  <option value="ADULT">
                    <xsl:if test="'ADULT' = passenger_type_rcd">
                      <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="tikLanguage:get('Registration_15','Adult')" />
                  </option>
                  <option value="CHD">
                    <xsl:if test="'CHD' = passenger_type_rcd">
                      <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="tikLanguage:get('Registration_16','Child')" />
                  </option>
                  <option value="INF">
                    <xsl:if test="'INF' = passenger_type_rcd">
                      <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="tikLanguage:get('Registration_17','Infant')" />
                  </option>
                  <option value="STAFF">
                    <xsl:if test="'STAFF' = passenger_type_rcd">
                      <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="tikLanguage:get('Registration_18','Staff')" />
                  </option>
                </select>

              </div>

              <div class="clearboth"></div>

              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_28','Last Name')" />
                <span id="spPsgLastName_{position()}" class="RequestStar">*</span>
              </div>
              <div class="Detail">
                <input id="txtPsgLastName_{position()}"  type="text" value="{lastname}" onkeypress="return CheckCharacter();" name="txtPsgLastName" />
              </div>


              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_29','First Name')" />
                <span id="spPsgFirstName_{position()}" class="RequestStar">*</span>
              </div>
              <div class="Detail">
                <input id="txtPsgFirstName_{position()}"  type="text" value="{firstname}" onkeypress="return CheckCharacter();" name="txtPsgFirstName" />
              </div>

              <div class="clearboth"></div>

              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_30','Date of Birth')" />
                <span id="spbirth_date_{position()}" class="RequestStar">*</span>
              </div>
              <div class="Detail">
                <xsl:variable name="birth_date">
                  <xsl:choose>
                    <xsl:when test="date_of_birth = '0001-01-01T00:00:00'">
                      <xsl:value-of select="tikLanguage:get('default_value_1','DD/MM/YYYY')" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="DateFormat">
                        <xsl:with-param name="Date" select="date_of_birth"></xsl:with-param>
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <input id="TxtPsgDateofBirth_{position()}"  type="text" value="{$birth_date}" name="TxtPsgDateofBirth" />
              </div>
              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_31','Nationality')" />
              </div>
              <div class="Detail">
                <select id="optNationality_{position()}" class="nationality">
                  <option value="" class="watermarkOn"></option>
                  <xsl:variable name="nationality_rcd" select="nationality_rcd"/>
                  <xsl:for-each select="//Passenger/setup/Countrys">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="country_rcd"/>
                      </xsl:attribute>
                      <xsl:if test="$nationality_rcd = country_rcd">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="display_name"/>
                    </option>
                  </xsl:for-each>
                </select>
              </div>

              <div class="clearboth"></div>

              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_32','Document Type')" />
              </div>
              <div class="Detail">

                <select id="optDocumentType_{position()}" class="doctype" >
                  <option value="" class="watermarkOn"></option>
                  <xsl:variable name="document_type_rcd" select="document_type_rcd"/>
                  <xsl:for-each select="//Passenger/setup/DocumentType">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="document_type_rcd"/>
                      </xsl:attribute>
                      <xsl:if test="$document_type_rcd = document_type_rcd">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="display_name"/>
                    </option>
                  </xsl:for-each>
                </select>
              </div>
              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_33','Document Number')" />
                <span id="spPsgDocumentNumber_{position()}" class="RequestStar">*</span>
              </div>
              <div class="Detail">
                <input id="txtPsgDocumentNumber_{position()}"  type="text" value="{passport_number}" name="txtPsgDocumentNumber" />
              </div>

              <div class="clearboth"></div>

              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_34','Place of Issue')" />
                <span id="spPsgPlaceOfIssue_{position()}" class="RequestStar">*</span>
              </div>
              <div class="Detail">
                <input id="txtPsgPlaceOfIssue_{position()}"  type="text" value="{passport_issue_place}" name="txtPsgPlaceOfIssue" />
              </div>
              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_35','Place of Birth')" />
                <span id="spPsgPlaceOfBirth_{position()}" class="RequestStar">*</span>
              </div>
              <div class="Detail">
                <input id="txtPsgPlaceOfBirth_{position()}"  type="text" value="{passport_birth_place}" name="txtPsgPlaceOfBirth" />
              </div>

              <div class="clearboth"></div>

              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_36','Issue Date')" />
                <span id="spissue_date_{position()}" class="RequestStar">*</span>
              </div>
              <div class="Detail">
                <xsl:variable name="issue_date">
                  <xsl:choose>
                    <xsl:when test="passport_issue_date = '0001-01-01T00:00:00'">
                      <xsl:value-of select="tikLanguage:get('default_value_1','DD/MM/YYYY')" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="DateFormat">
                        <xsl:with-param name="Date" select="passport_issue_date"></xsl:with-param>
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <input id="txtPsgIssueDate_{position()}"  type="text" value="{$issue_date}" name="txtPsgIssueDate" />
              </div>
              <div class="Title">
                <xsl:value-of select="tikLanguage:get('Registration_37','Expiry Date')" />
                <span id="spexpiry_date_{position()}" class="RequestStar">*</span>
              </div>
              <div class="Detail">
                <xsl:variable name="expiry_date">
                  <xsl:choose>
                    <xsl:when test="passport_expiry_date = '0001-01-01T00:00:00'">
                      <xsl:value-of select="tikLanguage:get('default_value_1','DD/MM/YYYY')" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="DateFormat">
                        <xsl:with-param name="Date" select="passport_expiry_date"></xsl:with-param>
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <input id="txtPsgExpiryDate_{position()}"  type="text" value="{$expiry_date}" name="txtPsgExpiryDate" />
              </div>

              <div class="clearboth"></div>
              <div class="ErrorList">
                <span id="txtPsgError_{position()}" class="RequestStar"></span>
              </div>      
            </li>
          </ul>
        </li>
      </ul>
      <br />


    </xsl:for-each >


    <div class="clearboth"></div>
  </xsl:template>
</xsl:stylesheet>