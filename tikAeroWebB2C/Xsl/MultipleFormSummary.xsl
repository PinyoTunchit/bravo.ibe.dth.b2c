﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>

  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>
      <xsl:choose>
        <xsl:when test="substring($Date,6,2) = '01'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(0)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '02'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(1)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '03'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(2)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '04'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(3)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '05'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(4)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '06'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(5)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '07'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(6)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '08'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(7)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '09'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(8)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '10'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(9)"/>
        </xsl:when>
        <xsl:when test="substring($Date,6,2) = '11'">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(10)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(11)"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="substring($Date,1,4)"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>

  <xsl:template match="/">
    <xsl:variable name="total_voucher" select="sum(multiple_form/voucher/voucher_amount)" />
    <xsl:variable name="additional_fee_total" select="number(multiple_form/total_amount) - number($total_voucher)" />
    <div>
      <div class="boxheader">
        <xsl:value-of select="tikLanguage:get('Booking_Step_5_86','Additinal Payment Summary')" />
      </div>

        <div class="YourFlightSelected">
          <div class="charges">
            <ul id="ulMultipleVcSummary">
              <li>
                <div class="Taxes_Fees">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_87','Total Payment')" />
                </div>
                
                <div class="charges_price_total">
                  <xsl:value-of select="format-number(multiple_form/total_amount,'#,##0.00')"/>
                </div>
                
                <div class="Taxes_Fees">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_88','Voucher Summary')" />
                </div>
                <div class="collapse">
                  <img src="App_Themes/Default/Images/expand.gif" />
                </div>
                <div class="charges_price_total">
                  <xsl:value-of select="format-number($total_voucher,'#,##0.00')"/>
                </div>
                <div class="clear-all"></div>
                <ul>
                  <xsl:for-each select="multiple_form/voucher">
                    <li>
                      <div class="charges_name">
                        - <xsl:value-of select="voucher_number" />
                      </div>
                      <div class="charges_price">
                        <xsl:value-of select="format-number(voucher_amount, '#,##0.00')"/>
                      </div>
                    </li>
                  </xsl:for-each>
                </ul>
              </li>
            </ul>
            <ul>
              <li>
                <div class="Taxes_Fees">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_89','Remaining Amount')" />
                </div>
                <div class="charges_price_total">
                  <xsl:value-of select="format-number(number(multiple_form/total_amount) - number($total_voucher),'#,##0.00')"/>
                </div>
              </li>
            </ul>
            <ul>
              <li id="liMultiCreditCardFee">
                <div class="Taxes_Fees">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_90','Credit Card Fee')" />
                </div>
                <div id="dvMultiCreditCardValue" class="charges_price_total">
                  0.00
                </div>
              </li>
            </ul>
          </div>
          <div class="clear-all"></div>

      </div>

      <input type="hidden" id="hdTotalAdditional" value="{format-number($additional_fee_total,'0.00')}" />
      <div class="summary">
        <div class="summary_total">
          <xsl:value-of select="tikLanguage:get('Booking_Step_5_91','Total Additional payment')" />
        </div>
        <div id="dvTotalAdditional" class="summary_price">
          <xsl:value-of select="format-number($additional_fee_total,'#,##0.00')"/>
        </div>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>