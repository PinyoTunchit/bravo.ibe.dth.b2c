<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">
	<xsl:template name="TicketDetails" match="object">
		<xsl:variable name="TaxesTotal">
			<total_amount>
				<xsl:for-each select="NewDataSet/Taxes">
					<item>
						<xsl:value-of select="sales_amount_incl - sales_amount"/>
					</item>
				</xsl:for-each>
			</total_amount>
		</xsl:variable>


		<table border="0" class="TBLPopupTicket">
			<tr>
				<td class="header" style="font-family:Tahoma;">
					<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_1','Ticket Details')" />
				</td>
			</tr>
		</table>
		<FIELDSET style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; WIDTH: 100%; PADDING-TOP: 5px; margin:10px; width:780px;">
			<LEGEND style="font-family:Tahoma;">
				<B>
					<font color="red">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_2','Passenger Ticket')" />
					</font>
				</B>
			</LEGEND>
			<br/>
			<TABLE cellSpacing="0" cellPadding="0" border="0" class="TBLTicketDetailsHeader">
				<TR>
					<TD width="173" class="textname">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_3','Passenger Name')" />:
					</TD>
					<TD colspan="2" class="textbold">
						<xsl:value-of select="NewDataSet/Ticket/lastname"/>/<xsl:value-of select="NewDataSet/Ticket/firstname"/>&#160;<xsl:value-of select="NewDataSet/Ticket/title_rcd"/>
					</TD>
				</TR>
				<TR>
					<TD class="textname">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_4','Ticket Number')" />:
					</TD>
					<TD width="110" class="textbold">
						<xsl:value-of select="NewDataSet/Ticket/ticket_number"/>
					</TD>
					<TD width="485" class="textbold">
						<div class="Tiketbox">
							<xsl:if test="NewDataSet/Ticket/e_ticket_flag != '0'">
								1
							</xsl:if>
							<xsl:if test="NewDataSet/Ticket/coupon_number != ''">
								<xsl:value-of select="NewDataSet/Ticket/coupon_number"/>
							</xsl:if>
						</div>
					</TD>
				</TR>
				<TR>
					<TD class="textname">&#160;</TD>
					<TD class="textbold">&#160;</TD>
					<TD class="textbold">&#160;</TD>
				</TR>
			</TABLE>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="29%" valign="top">
						<TABLE cellSpacing="0" cellPadding="0" border="0" class="TBLTicketDetails">
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_5','Fare Code')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/fare_code"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_6','Flight Number')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/airline_rcd"/>&#160;<xsl:value-of select="NewDataSet/Ticket/flight_number"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_7','From')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/origin_rcd"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_8','To')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/destination_rcd"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_9','Flight Date')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/departure_date"/>
								</TD>
							</TR>
							<TR>
								<TD>&#160;</TD>
								<TD>&#160;</TD>
							</TR>
						</TABLE>
					</td>
					<td width="23%" valign="top">

						<TABLE  border="0" cellPadding="0" cellSpacing="0" class="TBLTicketDetails">
							<TR>
								<TD width="55%" class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_10','Passenger Type')" />:
								</TD>
								<TD width="45%" class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/passenger_type_rcd"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_11','Check-In Status')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/passenger_check_in_status_rcd"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_12','Passenger Status')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/passenger_status_rcd"/>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</td>
					<td width="26%" valign="top">
						<TABLE cellSpacing="0" cellPadding="0" width="45%" border="0" class="TBLTicketDetails">
							<TR>
								<TD width="59%" class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_13','Currency')" />:
								</TD>
								<TD width="41%" class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/currency_rcd"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_14','Agency Code')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/agency_code"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_15','Baggage Allowance')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/baggage_weight"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_16','Seat Selection')" />:
								</TD>
								<TD class="textbold"></TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_17','Classes')" />:
								</TD>
								<TD class="textbold">
									<div class="Tiketbox1">C</div>
									<div class="Tiketbox">
										<xsl:value-of select="NewDataSet/Ticket/boarding_class_rcd"/>
									</div>
									<div class="Tiketbox1">B</div>
									<div class="Tiketbox">
										<xsl:value-of select="NewDataSet/Ticket/booking_class_rcd"/>
									</div>
								</TD>
							</TR>
							<TR>
								<TD>&#160;</TD>
								<TD></TD>
							</TR>
						</TABLE>
					</td>
					<td width="22%" valign="top">
						<TABLE cellSpacing="0" cellPadding="0"  border="0" class="TBLTicketDetails">
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_18','Fare Quote Date')" /> :
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/fare_date_time"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_19','Ticketing Date')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/ticketing_date_time"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_20','Not Valid Before')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/not_valid_before_date"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">
									<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_21','Not Valid After')" />:
								</TD>
								<TD class="textbold">
									<xsl:value-of select="NewDataSet/Ticket/not_valid_after_date"/>
								</TD>
							</TR>
							<TR>
								<TD class="textname">&#160;</TD>
								<TD class="textbold"></TD>
							</TR>
							<TR>
								<TD>&#160;</TD>
								<TD  class="textbold"></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</table>

			<table  border="0" cellpadding="2" cellspacing="1" bgcolor="#013668" class="TBLTicketDetails2">
				<tr>
					<td width="4%" rowspan="2" valign="middle" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_22','Type')" />
					</td>
					<td colspan="2" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_23','Tax')" />
					</td>
					<td width="4%" rowspan="2" valign="middle" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_26','VAT/GST')" />
					</td>
					<td colspan="3" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_27','Charge Amount')" />
					</td>
					<td colspan="3" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_30','Sales Amount')" />
					</td>
				</tr>
				<tr>
					<td width="3%" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_24','Code')" />
					</td>
					<td width="12%" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_25','Name')" />
					</td>
					<td width="6%" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_13','Currency')" />
					</td>
					<td width="6%" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_28','Charge')" />
					</td>
					<td width="5%" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_29','Total')" />
					</td>
					<td width="6%" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_28','Charge')" />
					</td>
					<td width="5%" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_29','Total')" />
					</td>
					<td width="6%" bgcolor="#CCCCCC" class="textbold">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_26','VAT/GST')" />
					</td>
				</tr>
				<tr>
					<td valign="middle" bgcolor="#FFFFFF">&#160;</td>
					<td bgcolor="#FFFFFF">&#160;</td>
					<td bgcolor="#FFFFFF">
						<div align="left">
							<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_29','Total')" />
						</div>
					</td>
					<td valign="middle" bgcolor="#FFFFFF">
						<div align="right"></div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="center"></div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right"></div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right"></div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(sum(NewDataSet/Ticket/acct_fare_amount) + sum(NewDataSet/Taxes/sales_amount),'#,###,###,##0.00') " />
						</div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(sum(NewDataSet/Ticket/acct_fare_amount_incl) + sum(NewDataSet/Taxes/sales_amount_incl),'#,###,###,##0.00') " />
						</div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(sum(msxsl:node-set($TaxesTotal)/total_amount/item) + (NewDataSet/Ticket/acct_fare_amount_incl - NewDataSet/Ticket/acct_fare_amount),'#,###,###,##0.00')" />
						</div>
					</td>
				</tr>
				<tr>
					<td valign="middle" bgcolor="#FFFFFF">
						<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_5','Fare Code')" />
					</td>
					<td bgcolor="#FFFFFF">&#160;</td>
					<td bgcolor="#FFFFFF">
						<div align="left">
							<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_37','Fare Amount')" />
						</div>
					</td>
					<td valign="middle" bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(NewDataSet/Ticket/fare_vat,'#,###,###,##0.00')"/>
						</div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="left">
							<xsl:value-of select="NewDataSet/Ticket/currency_rcd"/>
						</div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(NewDataSet/Ticket/fare_amount,'#,###,###,##0.00')"/>
						</div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(NewDataSet/Ticket/fare_amount_incl,'#,###,###,##0.00')"/>
						</div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(NewDataSet/Ticket/acct_fare_amount,'#,###,###,##0.00')"/>
						</div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(NewDataSet/Ticket/acct_fare_amount_incl,'#,###,###,##0.00')"/>
						</div>
					</td>
					<td bgcolor="#FFFFFF">
						<div align="right">
							<xsl:value-of select="format-number(NewDataSet/Ticket/acct_fare_amount_incl - NewDataSet/Ticket/acct_fare_amount,'#,###,###,##0.00')"/>
						</div>
					</td>
				</tr>
				<xsl:if test="count(NewDataSet/Taxes)!= 0">
					<xsl:for-each select="NewDataSet/Taxes">
						<tr>
							<td valign="middle" bgcolor="#FFFFFF">
								<xsl:value-of select="summarize_up"/>
							</td>
							<td bgcolor="#FFFFFF">
								<xsl:value-of select="tax_rcd"/>
							</td>
							<td bgcolor="#FFFFFF">
								<div align="left">
									<xsl:value-of select="display_name"/>
								</div>
							</td>
							<td valign="middle" bgcolor="#FFFFFF">
								<div align="right">
									<xsl:value-of select="vat_percentage"/>
								</div>
							</td>
							<td bgcolor="#FFFFFF">
								<div align="left">
									<xsl:value-of select="sales_currency_rcd"/>
								</div>
							</td>
							<td bgcolor="#FFFFFF">
								<div align="right">
									<xsl:value-of select="format-number(tax_amount,'#,###,###,##0.00')"/>
								</div>
							</td>
							<td bgcolor="#FFFFFF">
								<div align="right">
									<xsl:value-of select="format-number(tax_amount_incl,'#,###,###,##0.00')"/>
								</div>
							</td>
							<td bgcolor="#FFFFFF">
								<div align="right">
									<xsl:value-of select="format-number(sales_amount,'#,###,###,##0.00')"/>
								</div>
							</td>
							<td bgcolor="#FFFFFF">
								<div align="right">
									<xsl:value-of select="format-number(sales_amount_incl,'#,###,###,##0.00')"/>
								</div>
							</td>
							<td bgcolor="#FFFFFF">
								<div align="right">
									<xsl:value-of select="format-number(sales_vat,'#,###,###,##0.00')"/>
								</div>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:if>
			</table>
			<br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				<tr>
					<td width="32%" valign="top" bgcolor="#FFFFFF">


						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" class="TBLTicketDetails3">
							<TR>
								<TD width="50%" valign="top" class="textbold">
									<div align="left">
										<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_31','Refund with Charge')" />:
									</div>
								</TD>
								<TD width="50%" valign="top">
									<div align="left">
										<xsl:value-of select="NewDataSet/Ticket/refund_with_charge_hours"/>
										<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_38','Hours prior to Flight')" />
									</div>
								</TD>
							</TR>
							<TR>
								<TD width="50%" valign="top" class="textbold">
									<div align="left">
										<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_32','Refund not possible')" />:
									</div>
								</TD>
								<TD width="50%" valign="top">
									<div align="left">
										<xsl:value-of select="NewDataSet/Ticket/refund_not_possible_hours"/>
										<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_38','Hours prior to Flight')" />
									</div>
								</TD>
							</TR>
						</TABLE>
					</td>
					<td width="28%" valign="top" bgcolor="#ffffff">
						<TABLE cellSpacing="0" cellPadding="0" border="0" class="TBLTicketDetails4">
							<TR>
								<TD width="67%" class="textbold">
									<div align="left">
										<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_33','Refund Charge')" /> :
									</div>
								</TD>
								<TD width="33%">
									<div align="right">
										<xsl:value-of select="format-number(NewDataSet/Ticket/refund_charge,'#,###,###,##0.00')"/>
									</div>
								</TD>
							</TR>

							<TR>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</td>
					<td width="25%" valign="top" bgcolor="#ffffff">
						<TABLE cellSpacing="0" cellPadding="0" width="122%" border="0" class="TBLTicketDetails3">
							<TR>
								<TD width="65%" valign="top" class="textbold">
									<div align="left">
										<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_34','Refundable Amount')" />:
									</div>
								</TD>
								<TD width="35%" valign="top">
									<div align="right">
										<xsl:if test="NewDataSet/Ticket/refundable_amount = '0'">
											0.00
										</xsl:if>
										<xsl:if test="NewDataSet/Ticket/refundable_amount != '0'">
											<xsl:value-of select="format-number(NewDataSet/Ticket/refundable_amount,'#,###,###,##0.00')"/>
										</xsl:if>
									</div>
								</TD>
							</TR>

							<TR>
								<TD>&#160;</TD>
								<TD></TD>
							</TR>
						</TABLE>
					</td>
					<td width="15%" valign="top" bgcolor="#ffffff">&#160;</td>
				</tr>
			</table>
			<br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				<tr>
					<td width="28%" valign="top" bgcolor="#FFFFFF">
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" class="TBLTicketDetails3">
							<TR>
								<TD width="123" class="textbold">
									<div align="left">
										<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_35','Endorsement')" />:
									</div>
								</TD>
								<TD width="645">
									<div align="left">
										<xsl:value-of select="NewDataSet/Ticket/endorsement_text"/>
									</div>
								</TD>
							</TR>
							<TR>
								<TD width="123" class="textbold">
									<div align="left">
										<xsl:value-of select="tikLanguage:get('Ticket_Popup_page_36','Restrictions')" />:
									</div>
								</TD>
								<TD width="645">
									<div align="left">
										<xsl:value-of select="NewDataSet/Ticket/restriction_text"/>
									</div>
								</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">&#160;</td>
				</tr>
			</table>
			<br/>

		</FIELDSET>
	</xsl:template>
</xsl:stylesheet>