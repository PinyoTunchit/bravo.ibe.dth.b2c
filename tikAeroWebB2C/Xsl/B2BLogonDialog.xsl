<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">
	<xsl:template match="/">
		<div class="TBLloginBox">
			<div class="BoxHeader">
				<div class="TicketText">Agency Login</div>
			</div>
			<ul>
				<li class="Text">
					Agency Code<br />
					<input id="txtDialogAgencyCode" type="text" onkeypress="return SubmitEnterAgencyLogon(this,event,'true')" />
				</li>
				<li class="Text">
					User Login<br />
					<input id="txtDialogLogonID" type="text" onkeypress="return SubmitEnterAgencyLogon(this,event,'true')" />
				</li>
				<li class="Text">
					Password<br />
					<input id="txtDialogPassword" type="password" onkeypress="return SubmitEnterAgencyLogon(this,event,'true')" />
				</li>
			</ul>
			<div class="ButtonAlignMiddleUserInfo">
				<div class="buttonCornerLeft"></div>
				<div class="buttonContent" onclick="AgencyLogonDialog();">LOGIN</div>
				<div class="buttonCornerRight"></div>
			</div>
			<div class="clearboth"></div>
			<div class="line"></div>
			<div class="clearboth"></div>
			<div class="forgetID" onclick="LoadB2BAdminLogonDialog();">Administrator Login</div>
			<div class="clearboth"></div>
			<div class="forgetID" onclick="CloseDialog();">Close</div>
			<div class="clearboth"></div>
			<div class="BlueBoxFooter"></div>
		</div>
		<div class="clearboth"></div>
	</xsl:template>

</xsl:stylesheet>