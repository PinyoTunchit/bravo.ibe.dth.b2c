<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no"/>

	<xsl:key name="fare_id_group" match="Booking/Mapping[passenger_status_rcd = 'OK']" use="fare_id"/>
	<xsl:key name="passenger_type_rcd_group" match="Booking/Mapping[passenger_status_rcd = 'OK']" use="passenger_type_rcd"/>

	<xsl:template name="DateFormat">
		<xsl:param name="Date">
		</xsl:param>
		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
		</xsl:if>
	</xsl:template>

	<xsl:template name="TimeFormat">
		<xsl:param name ="Time"></xsl:param>

		<xsl:if test="string-length($Time) =  3 ">
			0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 4 ">
			<xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 2 ">
			00:<xsl:value-of select="$Time"  />
		</xsl:if>
	</xsl:template>

	<xsl:template match="/">

		<div class="RulesBoxTextHeader">
      <xsl:value-of select="tikLanguage:get('Booking_Step_3_15','Rules')" /></div>
    <xsl:for-each select="Booking/Mapping[count(. | key('fare_id_group', fare_id)[1]) = 1][booking_segment_id = ../FlightSegment[segment_status_rcd != 'XX']/booking_segment_id]">
			<xsl:if test="position() &lt;= 2">
				<div class="SummaryRulesBox">
					<ul>
						<li class="header">
							<xsl:choose>
								<xsl:when test="position() = 1">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_16','Outbound')" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_17','Inbound')" />
								</xsl:otherwise>
							</xsl:choose>
						</li>
						<xsl:if test="string-length(restriction_text) > 0">
							<li>
								- <xsl:value-of select="restriction_text"/>
							</li>
						</xsl:if>
						<xsl:if test="string-length(baggage_weight) > 0">
							<li>
								<xsl:variable name="booking_segment_id" select="booking_segment_id" />
								- <xsl:value-of select="tikLanguage:get('Booking_Step_3_21','Baggage Allowance')" /><br />
								<xsl:for-each select="../Mapping[count(. | key('passenger_type_rcd_group', passenger_type_rcd)[1]) = 1]">
									<xsl:variable name="passenger_type_rcd" select="passenger_type_rcd" />	
									<xsl:for-each select="../Mapping[booking_segment_id = $booking_segment_id][passenger_type_rcd = $passenger_type_rcd][position() = 1]">
                    <xsl:choose>
                      <xsl:when test="passenger_type_rcd = 'ADULT'">
                        <xsl:value-of select="tikLanguage:get('Booking_Step_3_24','ADULT')" />
                      </xsl:when>
                      <xsl:when test="passenger_type_rcd = 'CHD'">
                        <xsl:value-of select="tikLanguage:get('Booking_Step_3_25','CHD')" />
                      </xsl:when>
                      <xsl:when test="passenger_type_rcd = 'INF'">
                        <xsl:value-of select="tikLanguage:get('Booking_Step_3_26','INF')" />
                      </xsl:when>
                    </xsl:choose>:&#160;&#160;
										<xsl:value-of select="baggage_weight"/>&#160;
										<xsl:value-of select="tikLanguage:get('Booking_Step_3_23','kg')" />
										<br />
									</xsl:for-each>
								</xsl:for-each>
							</li>
						</xsl:if>
					</ul>
				</div>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
