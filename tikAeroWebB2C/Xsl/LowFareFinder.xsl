<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							                xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="GetDay">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="number(substring($Date,9,2))"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>
  <xsl:template name="month_name">
    <xsl:param name="Date" />
    <xsl:variable name="month" select="number(substring($Date,6,2))"/>
    <xsl:choose>
      <xsl:when test="$month=1">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_36','January')" />
      </xsl:when>
      <xsl:when test="$month=2">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_37','February')" />
      </xsl:when>
      <xsl:when test="$month=3">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_38','March')" />
      </xsl:when>
      <xsl:when test="$month=4">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_39','April')" />
      </xsl:when>
      <xsl:when test="$month=5">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_40','May')" />
      </xsl:when>
      <xsl:when test="$month=6">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_41','June')" />
      </xsl:when>
      <xsl:when test="$month=7">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_42','July')" />
      </xsl:when>
      <xsl:when test="$month=8">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_43','August')" />
      </xsl:when>
      <xsl:when test="$month=9">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_44','September')" />
      </xsl:when>
      <xsl:when test="$month=10">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_45','October')" />
      </xsl:when>
      <xsl:when test="$month=11">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_46','November')" />
      </xsl:when>
      <xsl:when test="$month=12">
        <xsl:value-of select="tikLanguage:get('Flight_Selection_47','December')" />
      </xsl:when>
      <xsl:otherwise>INVALID MONTH</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Get Low Fare Finder Time-->
  <xsl:template match="flight">
    <!--Display fare group information-->
    <xsl:variable name="flight_id" select="flight_id" />
    <xsl:variable name="flight_number" select="flight_number" />
    <xsl:variable name="transit_flight_id" select="transit_flight_id" />
    <xsl:variable name="transit_departure_date" select="concat(substring(transit_departure_date,1,4), substring(transit_departure_date,6,2), substring(transit_departure_date,9,2))" />
    <xsl:variable name="transit_airport_rcd" select="transit_airport_rcd" />
    <xsl:variable name="transit_fare_id" select="transit_fare_id" />
    <xsl:variable name="departure_date" select="concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2))" />
    <xsl:variable name="planned_departure_time" select="planned_departure_time" />
    <xsl:variable name="planned_arrival_time" select="planned_arrival_time" />

    <li>
      <span>
        <xsl:call-template name="TimeFormat">
          <xsl:with-param name="Time" select="planned_departure_time"></xsl:with-param>
        </xsl:call-template>
        -
        <xsl:call-template name="TimeFormat">
          <xsl:with-param name="Time" select="$planned_arrival_time"></xsl:with-param>
        </xsl:call-template>
      </span>
      <xsl:choose>
        <xsl:when test="full_flight_flag = 1">
          <b>
            -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_21','Full')" />
          </b>
        </xsl:when>
        <xsl:when test="class_open_flag = 0 and waitlist_open_flag = 0">
          <b>
            -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_21','Full')" />
          </b>
        </xsl:when>
        <xsl:when test="class_open_flag = 0">
          <b>
            -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_22','Closed')" />
          </b>
        </xsl:when>
        <xsl:when test="close_web_sales = 1">
          <b>
            -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_23','Call')" />
          </b>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="flight_param_fri">
            {&#034;flight_id&#034;:&#034;<xsl:value-of select="$flight_id" />&#034;,
            &#034;flight_number&#034;:&#034;<xsl:value-of select="$flight_number" />&#034;,
            &#034;origin_rcd&#034;:&#034;<xsl:value-of select="origin_rcd" />&#034;,
            &#034;airline_rcd&#034;:&#034;<xsl:value-of select="airline_rcd" />&#034;,
            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="destination_rcd" />&#034;,
            &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="booking_class_rcd" />&#034;,
            &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="$transit_flight_id" />&#034;,
            &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
            &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="$planned_departure_time" />&#034;,
            &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="$planned_arrival_time" />&#034;,
            &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
            &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="$transit_airport_rcd" />&#034;,
            &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="$transit_fare_id" />&#034;,
            &#034;fare_id&#034;:&#034;<xsl:value-of select="fare_id" />&#034;}
          </xsl:variable>

          <input type="hidden" id="spnTime_{$flight_id}" value="{$flight_param_fri}"/>
          <b onclick="SelectTime('spnTime_{$flight_id}');">
            -&#160;<xsl:value-of select="format-number(total_adult_fare,'#,##0.00')"/>
          </b>
        </xsl:otherwise>
      </xsl:choose>
    </li>
  </xsl:template>
  
  <xsl:template match="/">
    <xsl:variable name="origin_name" select="flights/setting/OriginName" />
    <xsl:variable name="destination_name" select="flights/setting/DestinationName" />
    <xsl:variable name="flight_type" select="flights/setting/flight_type" />
    <xsl:variable name="LowFareFinderRange" select="flights/setting/LowFareFinderRange" />
    <xsl:if test="count(flights/week) > 0">
      <div class="FareCalender">
        <div class="CalenderBase">

          <div class="WrapperTBLYourFlight">
            <div class="RedTopic">
              <xsl:value-of select="tikLanguage:get('Booking_Step_2_2','Your Flights Options')" />&#160;
              <span class="TextFlightName">
                <xsl:value-of select="flights/setting/OriginName" />
              </span>
              &#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_10','To')" />&#160;
              <span class="TextFlightName">
                <xsl:value-of select="flights/setting/DestinationName" />
              </span>
            </div>
          </div>
          <div class="CalenderControl"></div>

          <div class="LowFareBox">
            <div class="boxheader">
              <xsl:variable name="start_month">
                <xsl:call-template name="month_name">
                  <xsl:with-param name="Date" select="flights/week[position()=1]/flight[position()=1]/departure_date"></xsl:with-param>
                </xsl:call-template>
              </xsl:variable>
              <xsl:variable name="end_month">
                <xsl:call-template name="month_name">
                  <xsl:with-param name="Date" select="flights/week[position()=last()]/flight[position()=last()]/departure_date"></xsl:with-param>
                </xsl:call-template>
              </xsl:variable>
              <xsl:value-of select="$start_month"/>
              <xsl:if test="$start_month != $end_month">
                /<xsl:value-of select="$end_month"/>
              </xsl:if>
            </div>
            <xsl:variable name="start_date" select="concat(substring(flights/week[position()=1]/flight[position()=1]/departure_date,1,4), substring(flights/week[position()=1]/flight[position()=1]/departure_date,6,2), substring(flights/week[position()=1]/flight[position()=1]/departure_date,9,2))" />
            <xsl:variable name="back_date" select="JCode:getDateAdd($start_date, -1)" />
            <div class="ButtonAlignLeft" onclick="LowfarefinderChangeDate('{$back_date}','{$flight_type}')">
              <div class="buttonCornerLeft"></div>
              <div class="buttonContent">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_8','Back')" />
              </div>
              <div class="buttonCornerRight"></div>
            </div>
            <xsl:variable name="end_date" select="concat(substring(flights/week[position()=last()]/flight[position()=last()]/departure_date,1,4), substring(flights/week[position()=last()]/flight[position()=last()]/departure_date,6,2), substring(flights/week[position()=last()]/flight[position()=last()]/departure_date,9,2))" />
            <xsl:variable name="forward_date" select="JCode:getDateAdd($end_date, number(1))" />
            <div class="ButtonAlignRight" onclick="LowfarefinderChangeDate('{$forward_date}','{$flight_type}')">
              <div class="buttonCornerLeft"></div>
              <div class="buttonContent">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_9','Next')" />
              </div>
              <div class="buttonCornerRight"></div>
            </div>
            <table class="FareCalenderTable" cellpadding="0" cellspacing="0">
              <tr>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_48','Sun')" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_49','Mon')" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_50','Tue')" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_51','Wed')" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_52','Thurs')" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_53','Fri')" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_54','Sat')" />
                </td>
              </tr>
              <xsl:for-each select="flights/week">
                <tr>
                  <td id="td_{../setting/flight_type}_SUN{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Sunday']) > 0">
                        <a class="Low" onclick="SelectLlf('{../setting/flight_type}_SUN{position()}');
                                                ViewFareLowFareFinder('optLowfare_{../setting/flight_type}_SUN{position()}');
                                                GetFlightTime('{../setting/flight_type}_SUN{position()}');" 
                                       onmouseout="ClearShowTime('dvTime_{../setting/flight_type}_SUN{position()}', event);">

                          <div class="ShowTime" id="dvTime_{../setting/flight_type}_SUN{position()}" style="display:none;">
                            <ul class="Lowfarepriceselect">
                              <xsl:apply-templates select="flight[day_of_week = 'Sunday']">
                                <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>
                              </xsl:apply-templates>
                            </ul>
                          </div>

                          <xsl:variable name="flight_param_sun">
                            {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/origin_rcd" />&#034;,
                            &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/origin_name" />&#034;,
                            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/destination_rcd" />&#034;,
                            &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/departure_date" />&#034;}
                          </xsl:variable>

                          <div class="ShowLowfaredate">
                            <input id="optLowfare_{../setting/flight_type}_SUN{position()}"  name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_sun)}" />

                            <label for="optLowfare_{../setting/flight_type}_SUN{position()}">
                              <xsl:call-template name="GetDay">
                                <xsl:with-param name="Date" select="flight[day_of_week = 'Sunday'][position() = 1]/departure_date"></xsl:with-param>
                              </xsl:call-template>
                            </label>
                          </div>

                          <div id="spnLowfare_{../setting/flight_type}_SUN{position()}" class="Lowfareprice">
                            <xsl:value-of select="format-number(flight[day_of_week = 'Sunday'][position() = 1]/total_adult_fare,'#,##0.00')"/>
                          </div>
                        </a>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_MON{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Monday']) > 0">
                        <a class="Low" onclick="SelectLlf('{../setting/flight_type}_MON{position()}');
                                                ViewFareLowFareFinder('optLowfare_{../setting/flight_type}_MON{position()}');
                                                GetFlightTime('{../setting/flight_type}_MON{position()}');" 
                                       onmouseout="ClearShowTime('dvTime_{../setting/flight_type}_MON{position()}', event);">
                          
                          <div class="ShowTime" id="dvTime_{../setting/flight_type}_MON{position()}" style="display:none;">
                            <ul class="Lowfarepriceselect">
                              <xsl:apply-templates select="flight[day_of_week = 'Monday']">
                                <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>
                              </xsl:apply-templates>
                            </ul>
                          </div>

                          <xsl:variable name="flight_param_mon">
                            {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/origin_rcd" />&#034;,
                            &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/origin_name" />&#034;,
                            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/destination_rcd" />&#034;,
                            &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/departure_date" />&#034;}
                          </xsl:variable>

                          <div class="ShowLowfaredate">
                            <input id="optLowfare_{../setting/flight_type}_MON{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_mon)}" />

                            <label for="optLowfare_{../setting/flight_type}_MON{position()}">
                              <xsl:call-template name="GetDay">
                                <xsl:with-param name="Date" select="flight[day_of_week = 'Monday'][position() = 1]/departure_date"></xsl:with-param>
                              </xsl:call-template>
                            </label>
                          </div>

                          <div id="spnLowfare_{../setting/flight_type}_MON{position()}" class="Lowfareprice">
                            <xsl:value-of select="format-number(flight[day_of_week = 'Monday'][position() = 1]/total_adult_fare, '#,##0.00')"/>
                          </div>
                        </a>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_TUE{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Tuesday']) > 0">
                        <a class="Low" onclick="SelectLlf('{../setting/flight_type}_TUE{position()}');
                                                ViewFareLowFareFinder('optLowfare_{../setting/flight_type}_TUE{position()}');
                                                GetFlightTime('{../setting/flight_type}_TUE{position()}');" 
                                       onmouseout="ClearShowTime('dvTime_{../setting/flight_type}_TUE{position()}', event);">

                          <div class="ShowTime" id="dvTime_{../setting/flight_type}_TUE{position()}" style="display:none;">
                            <ul class="Lowfarepriceselect">
                              <xsl:apply-templates select="flight[day_of_week = 'Tuesday']">
                                <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>
                              </xsl:apply-templates>
                            </ul>
                          </div>

                          <xsl:variable name="flight_param_tue">
                            {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/origin_rcd" />&#034;,
                            &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/origin_name" />&#034;,
                            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/destination_rcd" />&#034;,
                            &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/departure_date" />&#034;}
                          </xsl:variable>

                          <div class="ShowLowfaredate">
                            <input id="optLowfare_{../setting/flight_type}_TUE{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_tue)}" />

                            <label for="optLowfare_{../setting/flight_type}_TUE{position()}">
                              <xsl:call-template name="GetDay">
                                <xsl:with-param name="Date" select="flight[day_of_week = 'Tuesday'][position() = 1]/departure_date"></xsl:with-param>
                              </xsl:call-template>
                            </label>
                          </div>

                          <div id="spnLowfare_{../setting/flight_type}_TUE{position()}" class="Lowfareprice">
                            <xsl:value-of select="format-number(flight[day_of_week = 'Tuesday'][position() = 1]/total_adult_fare, '#,##0.00')"/>
                          </div>
                        </a>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_WED{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Wednesday']) > 0">
                        <a class="Low" onclick="SelectLlf('{../setting/flight_type}_WED{position()}');
                                                ViewFareLowFareFinder('optLowfare_{../setting/flight_type}_WED{position()}');
                                                GetFlightTime('{../setting/flight_type}_WED{position()}');" 
                                      onmouseout="ClearShowTime('dvTime_{../setting/flight_type}_WED{position()}', event);">

                          <div class="ShowTime" id="dvTime_{../setting/flight_type}_WED{position()}" style="display:none;">
                            <ul class="Lowfarepriceselect">
                              <xsl:apply-templates select="flight[day_of_week = 'Wednesday']">
                                <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>
                              </xsl:apply-templates>
                            </ul>
                          </div>

                          <xsl:variable name="flight_param_wed">
                            {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/origin_rcd" />&#034;,
                            &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/origin_name" />&#034;,
                            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/destination_rcd" />&#034;,
                            &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/departure_date" />&#034;}
                          </xsl:variable>

                          <div class="ShowLowfaredate">
                            <input id="optLowfare_{../setting/flight_type}_WED{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_wed)}" />

                            <label for="optLowfare_{../setting/flight_type}_WED{position()}">
                              <xsl:call-template name="GetDay">
                                <xsl:with-param name="Date" select="flight[day_of_week = 'Wednesday'][position() = 1]/departure_date"></xsl:with-param>
                              </xsl:call-template>
                            </label>
                          </div>

                          <div id="spnLowfare_{../setting/flight_type}_WED{position()}" class="Lowfareprice">
                            <xsl:value-of select="format-number(flight[day_of_week = 'Wednesday'][position() = 1]/total_adult_fare, '#,##0.00')"/>
                          </div>
                        </a>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_THU{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Thursday']) > 0">
                        <a class="Low" onclick="SelectLlf('{../setting/flight_type}_THU{position()}');
                                                ViewFareLowFareFinder('optLowfare_{../setting/flight_type}_THU{position()}');
                                                GetFlightTime('{../setting/flight_type}_THU{position()}');" 
                                       onmouseout="ClearShowTime('dvTime_{../setting/flight_type}_THU{position()}', event);">

                          <div class="ShowTime" id="dvTime_{../setting/flight_type}_THU{position()}" style="display:none;">
                            <ul class="Lowfarepriceselect">
                              <xsl:apply-templates select="flight[day_of_week = 'Thursday']">
                                <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>
                              </xsl:apply-templates>
                            </ul>
                          </div>

                          <xsl:variable name="flight_param_thu">
                            {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/origin_rcd" />&#034;,
                            &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/origin_name" />&#034;,
                            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/destination_rcd" />&#034;,
                            &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/departure_date" />&#034;}
                          </xsl:variable>

                          <div class="ShowLowfaredate">
                            <input id="optLowfare_{../setting/flight_type}_THU{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_thu)}" />

                            <label for="optLowfare_{../setting/flight_type}_THU{position()}">
                              <xsl:call-template name="GetDay">
                                <xsl:with-param name="Date" select="flight[day_of_week = 'Thursday'][position() = 1]/departure_date"></xsl:with-param>
                              </xsl:call-template>
                            </label>
                          </div>

                          <div id="spnLowfare_{../setting/flight_type}_THU{position()}" class="Lowfareprice">
                            <xsl:value-of select="format-number(flight[day_of_week = 'Thursday'][position() = 1]/total_adult_fare, '#,##0.00')"/>
                          </div>
                        </a>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_FRI{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Friday']) > 0">
                        <a class="Low" onclick="SelectLlf('{../setting/flight_type}_FRI{position()}');
                                                ViewFareLowFareFinder('optLowfare_{../setting/flight_type}_FRI{position()}');
                                                GetFlightTime('{../setting/flight_type}_FRI{position()}');" 
                                       onmouseout="ClearShowTime('dvTime_{../setting/flight_type}_FRI{position()}', event);">

                          <div class="ShowTime" id="dvTime_{../setting/flight_type}_FRI{position()}" style="display:none;">
                            <ul class="Lowfarepriceselect">
                              <xsl:apply-templates select="flight[day_of_week = 'Friday']">
                                <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>
                              </xsl:apply-templates>
                            </ul>
                          </div>

                          <xsl:variable name="flight_param_fri">
                            {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/origin_rcd" />&#034;,
                            &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/origin_name" />&#034;,
                            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/destination_rcd" />&#034;,
                            &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/departure_date" />&#034;}
                          </xsl:variable>

                          <div class="ShowLowfaredate">
                            <input id="optLowfare_{../setting/flight_type}_FRI{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_fri)}" />

                            <label for="optLowfare_{../setting/flight_type}_FRI{position()}">
                              <xsl:call-template name="GetDay">
                                <xsl:with-param name="Date" select="flight[day_of_week = 'Friday'][position() = 1]/departure_date"></xsl:with-param>
                              </xsl:call-template>
                            </label>
                          </div>

                          <div id="spnLowfare_{../setting/flight_type}_FRI{position()}" class="Lowfareprice">
                            <xsl:value-of select="format-number(flight[day_of_week = 'Friday'][position() = 1]/total_adult_fare, '#,##0.00')"/>
                          </div>
                        </a>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_SAT{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Saturday']) > 0">
                        <a class="Low" onclick="SelectLlf('{../setting/flight_type}_SAT{position()}');
                                                ViewFareLowFareFinder('optLowfare_{../setting/flight_type}_SAT{position()}');
                                                GetFlightTime('{../setting/flight_type}_SAT{position()}');" 
                                        onmouseout="ClearShowTime('dvTime_{../setting/flight_type}_SAT{position()}', event);">
                          <div class="ShowTime" id="dvTime_{../setting/flight_type}_SAT{position()}" style="display:none;">
                            <ul class="Lowfarepriceselect">
                              <xsl:apply-templates select="flight[day_of_week = 'Saturday']">
                                <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>
                              </xsl:apply-templates>
                            </ul>
                          </div>
                          <xsl:variable name="flight_param_sat">
                            {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/origin_rcd" />&#034;,
                            &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/origin_name" />&#034;,
                            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/destination_rcd" />&#034;,
                            &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/departure_date" />&#034;}
                          </xsl:variable>

                          <div class="ShowLowfaredate">
                            <input id="optLowfare_{../setting/flight_type}_SAT{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_sat)}" />

                            <label for="optLowfare_{../setting/flight_type}_SAT{position()}">
                              <xsl:call-template name="GetDay">
                                <xsl:with-param name="Date" select="flight[day_of_week = 'Saturday'][position() = 1]/departure_date"></xsl:with-param>
                              </xsl:call-template>
                            </label>
                          </div>

                          <div id="spnLowfare_{../setting/flight_type}_SAT{position()}" class="Lowfareprice">
                            <xsl:value-of select="format-number(flight[day_of_week = 'Saturday'][position() = 1]/total_adult_fare, '#,##0.00')"/>
                          </div>
                        </a>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </div>
          <div class="clearboth"></div>
        </div>
        <div class="clearboth"></div>
      </div>
    </xsl:if>
  </xsl:template>
  <msxsl:script implements-prefix="JCode" language="JavaScript">
    function getDateAdd(strDate, addValue)
    {
    var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
    var strMonth;
    var strday;

    dtDate.setDate(dtDate.getDate()+(addValue));

    if ((dtDate.getMonth() + 1).toString().length == 1)
    {
    strMonth = '0' + (dtDate.getMonth() + 1).toString()
    }
    else
    {
    strMonth = (dtDate.getMonth() + 1).toString()
    }

    if (dtDate.getDate().toString().length == 1)
    {
    strday = '0' + dtDate.getDate().toString()
    }
    else
    {
    strday = dtDate.getDate().toString()
    }

    return (dtDate.getYear().toString() + '' + strMonth + '' + strday);
    }

  </msxsl:script>
</xsl:stylesheet>