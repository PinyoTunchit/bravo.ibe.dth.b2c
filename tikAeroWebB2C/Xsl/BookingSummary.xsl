<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:asp="remove"
				xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no" encoding="utf-8" omit-xml-declaration="yes"/>
	<xsl:key name="passenger_type_rcd_group" match="Booking/Passenger" use="passenger_type_rcd"/>
	<xsl:key name="fee_id_group" match="Booking/Fee[string-length(void_by) = 0]" use="fee_id"/>
	<!--Variable-->
	<xsl:variable name="passenger_id" select="Booking/Mapping/passenger_id"/>
	<xsl:variable name="booking_segment_id_tax" select="Booking/Mapping[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
	<xsl:variable name="booking_segment_id" select="Booking/Tax[booking_segment_id = $booking_segment_id_tax]"/>
	<xsl:variable name="Ticket_total" select="(sum(Booking/Quote[charge_type != 'REFUND']/charge_amount) - sum(Booking/Quote[charge_type = 'REFUND']/charge_amount)) + (sum(Booking/Quote[charge_type != 'REFUND']/tax_amount) - sum(Booking/Quote[charge_type = 'REFUND']/tax_amount)) + sum(Booking/Fee[string-length(void_by) = 0]/fee_amount_incl)"/>
	<xsl:variable name="Payment_total" select="sum(Booking/Payment[string-length(void_by) = 0]/payment_amount)"/>
	<xsl:variable name="currency_rcd" select="Booking/BookingHeader/currency_rcd" />
	<!-- Define keys used to group elements -->

	<xsl:template name="TimeFormat">
		<xsl:param name ="Time"></xsl:param>

		<xsl:if test="string-length($Time) =  3 ">
			0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 4 ">
			<xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 2 ">
			00:<xsl:value-of select="$Time"  />
		</xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
	</xsl:template>
	<xsl:template name="DateFormat">
		<xsl:param name="Date">
		</xsl:param>

		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="PriceFormat">
		<xsl:param name="Input" />
		<xsl:variable name="temppoint">
			<xsl:value-of select="substring-after($Input,'.')" />
		</xsl:variable>

		<xsl:if test="string-length( substring-before($Input,'.'))=0">
			<xsl:value-of select="$Input"/>.00
		</xsl:if>
		<xsl:if test=" string-length(substring-before($Input,'.'))> 0">
			<xsl:choose>
				<xsl:when test="string-length($temppoint) = 0">
					<xsl:value-of select="substring-before($Input,'.')" />.00
				</xsl:when>
				<xsl:when test="string-length($temppoint) = 1">
					<xsl:value-of select="substring-before($Input,'.')" />.<xsl:value-of select="$temppoint" />0
				</xsl:when>
				<xsl:when test="string-length($temppoint) = 2">
					<xsl:value-of select="$Input" />
				</xsl:when>
				<xsl:when test="string-length($temppoint) = 3">
					<xsl:value-of select="$Input" />
				</xsl:when>
				<xsl:when test="string-length($temppoint) =4">
					<xsl:value-of select="$Input" />
				</xsl:when>
				<xsl:when test="string-length($temppoint) = 5">
					<xsl:value-of select="$Input" />
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<xsl:template name="Itinerary" match="/" >
		<table border="0" class="TBLYourItinerary">
			<tr class="TableHead">
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_7','Flight')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_8','From')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_9','To')" />
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_10','Date')" />
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_11','Dep')" />
				</td>
				<td class="HeadCOL6">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_12','Arr')" />
				</td>
				<td class="HeadCOL7">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_13','Status')" />
				</td>
			</tr>
		</table>
		<!--Show Cancel Segment-->
		<xsl:for-each select="Booking/FlightSegment[segment_status_rcd != 'HK']">

			<xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
			<xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
			<xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
			<xsl:sort select="departure_time" order="ascending" data-type="number"/>

			<xsl:variable name="origin_rcd" select="origin_rcd"/>
			<xsl:variable name="destination_rcd" select="destination_rcd"/>
			<xsl:variable name="flight_connection_id" select="flight_connection_id"/>
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
			<table border="0" class="TBLYourItinerary">
				<tr class="FlightInformation1">
					<td class="BodyCOL1">
						<span>
							<xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
						</span>
					</td>
					<td class="BodyCOL2">
						<span>
							<xsl:choose>
								<xsl:when test="string-length(origin_name) != 0">
									<xsl:value-of select="origin_name" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="origin_name" />
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</td>
					<td class="BodyCOL3">
						<span>
							<xsl:choose>
								<xsl:when test="string-length(destination_name) != 0">
									<xsl:value-of select="destination_name" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="destination_name" />
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</td>
					<td class="BodyCOL4">
						<span>
							<xsl:call-template name="DateFormat">
								<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
							</xsl:call-template>
						</span>
					</td>
					<td class="BodyCOL5">
						<span>
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="departure_time"></xsl:with-param>
							</xsl:call-template>
						</span>
					</td>
					<td class="BodyCOL6">
						<span>
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="arrival_time"></xsl:with-param>
							</xsl:call-template>
                            <xsl:if test="day_change_value != '0'">
                                <xsl:if test="day_change_value != '-1'">+</xsl:if>
                                <xsl:value-of select="day_change_value"/>
                            </xsl:if>
						</span>
					</td>
					<td class="BodyCOL7">
						<span>
                            <xsl:value-of select="status_name" />
                        </span>
                    </td>
                </tr>
            </table>

        </xsl:for-each>
        <!--Show Active Segment-->
        <xsl:for-each select="Booking/FlightSegment[segment_status_rcd = 'HK']">

            <xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
            <xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
            <xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
            <xsl:sort select="departure_time" order="ascending" data-type="number"/>

            <xsl:variable name="origin_rcd" select="origin_rcd"/>
            <xsl:variable name="destination_rcd" select="destination_rcd"/>
            <xsl:variable name="flight_connection_id" select="flight_connection_id"/>
            <xsl:variable name="booking_segment_id" select="booking_segment_id"/>

            <table border="0" class="TBLYourItinerary">
                <tr class="FlightInformation1">
                    <td class="BodyCOL1">
                        <span>
                            <xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
                        </span>
                    </td>
                    <td class="BodyCOL2">
                        <span>
                            <xsl:choose>
                                <xsl:when test="string-length(origin_name) != 0">
                                    <xsl:value-of select="origin_name" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="origin_name" />
                                </xsl:otherwise>
                            </xsl:choose>
                        </span>
                    </td>
                    <td class="BodyCOL3">
                        <span>
                            <xsl:choose>
                                <xsl:when test="string-length(destination_name) != 0">
                                    <xsl:value-of select="destination_name" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="destination_name" />
                                </xsl:otherwise>
                            </xsl:choose>
                        </span>
                    </td>
                    <td class="BodyCOL4">
                        <span>
                            <xsl:call-template name="DateFormat">
                                <xsl:with-param name="Date" select="departure_date"></xsl:with-param>
                            </xsl:call-template>
                        </span>
                    </td>
                    <td class="BodyCOL5">
                        <span>
                            <xsl:call-template name="TimeFormat">
                                <xsl:with-param name="Time" select="departure_time"></xsl:with-param>
                            </xsl:call-template>
                        </span>
                    </td>
                    <td class="BodyCOL6">
                        <span>
                            <xsl:call-template name="TimeFormat">
                                <xsl:with-param name="Time" select="arrival_time"></xsl:with-param>
                            </xsl:call-template>
                            <xsl:if test="day_change_value != '0'">
                                <xsl:if test="day_change_value != '-1'">+</xsl:if>
                                <xsl:value-of select="day_change_value"/>
                            </xsl:if>
                        </span>
                    </td>
                    <td class="BodyCOL7">
                        <span>
                            <xsl:value-of select="status_name" />
						</span>
					</td>
				</tr>
			</table>
			<!-- TBL Passenger -->
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="Passenger" match="/">
		<table border="0" class="TBLPassenger3">
			<tr class="TableHead">
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_15','Client Number')" />
				</td>
				
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_18','Title')" />
				</td>
				
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_17','Firstname')" />
				</td>
				
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_16','Lastname')" />
				</td>
				
				
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_19','Type')" />
				</td>
				
				<td class="HeadCOL6">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_20','Date of Birth')" />
				</td>
				
				<td class="HeadCOL7"></td>
			</tr>
			
			<xsl:for-each select="Booking/Passenger">
				<xsl:variable name="passenger_id" select="passenger_id"/>
				<tr class="FlightInformation1">
					<td class="BodyCOL1">
						<xsl:choose>
							<xsl:when test="number(client_number) = 0">
								&#160;
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="client_number"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					
					<td class="BodyCOL2">
						<xsl:value-of select="title_rcd"/>
					</td>
					
					<td class="BodyCOL3">
						<xsl:value-of select="firstname"/>
					</td>
					
					<td class="BodyCOL4">
						<xsl:value-of select="lastname"/>
					</td>

					<td class="BodyCOL5">
						<xsl:value-of select="passenger_type_rcd"/>
					</td>
					
					<td class="BodyCOL6">
						<xsl:choose>
							<xsl:when test="date_of_birth != '0001-01-01T00:00:00'">
								<xsl:call-template name="DateFormat">
									<xsl:with-param name="Date" select="date_of_birth"></xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					
					<td class="BodyCOL7"></td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="TicketsAndSeat" match="Booking/Tickets">
		<table border="0" class="TBLTikets">
			<tr class="TableHead">
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_22','E Ticket Number')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_23','ET Status')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_24','Passenger Name')" />
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_7','Flight')" />
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_10','Date')" />
				</td>
				<td class="HeadCOL6">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_19','Type')" />
				</td>
				<td class="HeadCOL7">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_25','Total net')" />
				   (<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL8">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_26','Seat')" />
				</td>
			</tr>
			<xsl:for-each select="Booking/Mapping">
				<xsl:sort select="departure_date" order ="ascending"/>
				<xsl:sort select="firstname" order ="ascending"/>
				<xsl:sort select="ticket_number" order ="ascending"/>
				<tr class="FlightInformation1">
					<td class="BodyCOL1">
						<xsl:choose>
							<xsl:when test="string-length(ticket_number)!=0">
								<xsl:value-of select="ticket_number"/>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="BodyCOL2">
						<xsl:choose>
							<xsl:when test="count(e_ticket_status) = 0">
								&#160;
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="e_ticket_status = 'U'">
									<xsl:value-of select="'Unavailable'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'R'">
									<xsl:value-of select="'Refunded'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'V'">
									<xsl:value-of select="'Void'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'E'">
									<xsl:value-of select="'Exchange'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'X'">
									<xsl:value-of select="'Print Exchange'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'O'">
									<xsl:value-of select="'Open'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'C'">
									<xsl:value-of select="'Checked'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'L' or e_ticket_status = 'B'">
									<xsl:value-of select="'Boarded'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'F'">
									<xsl:value-of select="'Flown'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'A'">
									<xsl:value-of select="'Airport Control'"/>
								</xsl:if>
								<xsl:if test="e_ticket_status = 'P'">
									<xsl:value-of select="'FIM'"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="BodyCOL3">
						<xsl:value-of select="lastname"/>&#160;<xsl:value-of select="firstname"/>
					</td>
					<td class="BodyCOL4">
						<xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
					</td>
					<td class="BodyCOL5">
						<xsl:call-template name="DateFormat">
							<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
						</xsl:call-template>
					</td>
					<td class="BodyCOL6">
						<xsl:value-of select="passenger_type_rcd"/>
					</td>
					<td class="BodyCOL7">
						&#160;<span>
							<xsl:call-template name="PriceFormat">
								<xsl:with-param name="Input" select="net_total"></xsl:with-param>
							</xsl:call-template>
						</span>
					</td>
					<td class="BodyCOL8">
						<xsl:if test="seat_number != '0'">
							<xsl:value-of select="seat_number"/>
						</xsl:if>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="CostBreakdown" >
		<table border="0" class="TBLCostBreakDown">
			<tr class="TableHead">
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_28','Passenger')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_29','Charges')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_30','Price Per Person')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_41','VAT')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_31','Total Price')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
			</tr>
			<xsl:for-each select="Booking/Passenger[count(. | key('passenger_type_rcd_group', passenger_type_rcd)[1]) = 1]">
				<xsl:variable name="passenger_type_rcd" select="passenger_type_rcd"/>
				<xsl:for-each select="../Quote[passenger_type_rcd=$passenger_type_rcd]">
					<xsl:variable name="TotalCharge">
						<xsl:if test="charge_type != 'REFUND'">
							<xsl:value-of select="sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
						</xsl:if>
						<xsl:if test="charge_type = 'REFUND'">
							<xsl:value-of select="sum(../Quote[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
						</xsl:if>
					</xsl:variable>
					<xsl:if test="position()=1">
						<xsl:if test="position()!=last()">
							<tr class="FlightInformation1">
								<td class="BodyCOL1">
									<xsl:value-of select="sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)"/>
									&#160;<xsl:value-of select="passenger_type_rcd"/>
								</td>
								<td class="BodyCOL2">
									<xsl:value-of select="charge_name"/>
								</td>
								<td class="BodyCOL3">
									<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
								</td>
								<td class="BodyCOL4">
									<xsl:if test="tax_amount > 0">
										<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
									</xsl:if>
								</td>
								<td class="BodyCOL5">
									<xsl:value-of select="format-number($TotalCharge + sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="position()=last()">
							<tr class="FlightInformation1">
								<td class="BodyCOL1">
									<xsl:value-of select="passenger_count"/>
									&#160;<xsl:value-of select="passenger_type_rcd"/>
								</td>
								<td class="BodyCOL2">
									<xsl:value-of select="charge_name"/>
								</td>
								<td class="BodyCOL3">
									<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
								</td>
								<td class="BodyCOL4">
									<xsl:if test="tax_amount > 0">
										<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;&#xA0;
									</xsl:if>
								</td>
								<td class="BodyCOL5">
									<xsl:value-of select="format-number($TotalCharge + sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
								</td>
							</tr>
						</xsl:if>
					</xsl:if>
					<xsl:if test="position()!=1">
						<xsl:if test="position()=last()">
							<xsl:if test="charge_type != 'REFUND'">
								<tr class="FlightInformation1">
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#xA0;</td>
								</tr>
							</xsl:if>
							<xsl:if test="charge_type = 'REFUND'">
								<tr class="FlightInformation1">
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										-<xsl:value-of select="format-number($TotalCharge + sum(../Quote[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#xA0;</td>
								</tr>
							</xsl:if>
						</xsl:if>
						<xsl:if test="position()!=last()">
							<xsl:if test="charge_type != 'REFUND'">
								<tr class="FlightInformation1">
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#xA0;</td>
								</tr>
							</xsl:if>
							<xsl:if test="charge_type = 'REFUND'">
								<tr class="FlightInformation1">
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">&#xA0;</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">
										-<xsl:value-of select="format-number($TotalCharge + sum(../Quote[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
									</td>
								</tr>
							</xsl:if>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>

			<xsl:for-each select="Booking/Fee[string-length(void_by) = 0][count(. | key('fee_id_group', fee_id)[1]) = 1]">
				<xsl:sort select="fee_id"/>
				<xsl:variable name="fee_id" select="fee_id"/>
				<xsl:for-each select="../Fee[string-length(void_by) = 0][fee_id = $fee_id]">
					<xsl:if test="position() = 1">
						<tr class="FlightInformation1">
							<td class="BodyCOL1">&#160;</td>
							<td class="BodyCOL2">
								<xsl:value-of select="display_name"/>
							</td>
							<td class="BodyCOL3">
								<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>&#xA0;
							</td>
							<td class="BodyCOL4">
								<xsl:variable name="vat" select="fee_amount_incl - fee_amount" />
								<xsl:if test="$vat > 0">
									<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>&#xA0;
								</xsl:if>
							</td>
							<td class="BodyCOL5">
								<xsl:value-of select="format-number(sum(../Fee[string-length(void_by) = 0][fee_id = $fee_id]/fee_amount_incl),'#,##0.00')"/>&#xA0;
							</td>
						</tr>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>
			<!--Summary-->
			<tr class="FlightInformation1">
				<td class="BodyCOL1">&#xA0;</td>
				<td class="BodyCOL2">&#xA0;</td>
				<td class="BodyCOL3">&#xA0;</td>
				<td class="BodyCOL4">&#xA0;</td>
				<td class="BodyCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_31','Total')" />
					&#160;&#160;<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;&#xA0;
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="Payment" match="Booking/Payment">
		<table border="0" class="TBLPaymentReceived ">
			<tr class="TableHead">
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_33','Description')" />
				</td>
				<td class="HeadCOL2">&#xA0;</td>
				<td class="HeadCOL3">&#xA0;</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_34','Credit')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_35','Debit')" />&#160;
					(<xsl:value-of select="$currency_rcd"/>)
				</td>
			</tr>
			<tr class="FlightInformation1">
				<td class="BodyCOL1">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_36','Ticket Cost &amp; Fee')" />&#160;
				</td>
				<td class="BodyCOL2"></td>
				<td class="BodyCOL3">&#xA0;</td>
				<xsl:if test="starts-with(string($Payment_total), '-')">
					<td class="BodyCOL4">&#xA0;</td>
					<td class="BodyCOL5">
						<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#xA0;
					</td>
				</xsl:if>
				<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
					<td class="BodyCOL4">
						<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;
					</td>
					<td class="BodyCOL5">&#xA0;</td>
				</xsl:if>
			</tr>
			<xsl:for-each select="Booking/Payment[string-length(void_by) = 0]">
				<xsl:if test="form_of_payment_rcd !=''">
					<tr class="FlightInformation1">
						<td class="BodyCOL1">
							<xsl:variable name="FormOfPayment">
								<xsl:if test="form_of_payment_rcd ='CASH'">
									<xsl:value-of select="form_of_payment_rcd"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='VOUCHER'">
									<xsl:if test="form_of_payment_subtype_rcd = 'GIFT'">
										Gift Certificate
									</xsl:if>
									<xsl:if test="form_of_payment_subtype_rcd = 'PPAID'">
										Prepaid Voucher
									</xsl:if>
									<xsl:if test="form_of_payment_subtype_rcd = 'SPLIT'">
										Booking Split
									</xsl:if>
									<xsl:if test="form_of_payment_subtype_rcd = 'VOUCHER'">
										Voucher
									</xsl:if>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='CC'">
									<xsl:value-of select="form_of_payment_subtype_rcd"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='TKT'">
									<xsl:value-of select="form_of_payment"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='CRAGT'">
									<xsl:value-of select="form_of_payment"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='MANUAL'">
									<xsl:value-of select="form_of_payment_subtype_rcd"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='CHEQUE'">
									<xsl:value-of select="form_of_payment_subtype_rcd"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='BANK'">
									<xsl:value-of select="form_of_payment"/>
								</xsl:if>
								<xsl:if test="form_of_payment_rcd ='INV'">
									<xsl:value-of select="form_of_payment_rcd"/>
								</xsl:if>
							</xsl:variable>
							<xsl:if test="$FormOfPayment=''">
								<xsl:if test="form_of_payment_rcd='CRAGT'">
									Credit Agency Account
								</xsl:if>
								<xsl:if test="form_of_payment_rcd!='CRAGT'">
									<xsl:value-of select="form_of_payment_rcd"/>
								</xsl:if>
							</xsl:if>

							<xsl:if test="$FormOfPayment!=''">
								<!--Credit Agency Account -->
								<xsl:value-of select="$FormOfPayment"/>
							</xsl:if>
						</td>
						<td class="BodyCOL2">
							<xsl:if test="form_of_payment_rcd ='INV'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='BANK'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='CHEQUE'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='MANUAL'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='VOUCHER'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
							<xsl:if test="form_of_payment_rcd ='CC'">
								<xsl:value-of select="document_number"/>
							</xsl:if>
						</td>
						<td class="BodyCOL3">
							<xsl:if test="string-length(void_by) > 0">
								<xsl:if test="count(payment_date_time) != 0">
									<xsl:call-template name="DateFormat">
										<xsl:with-param name="Date" select="payment_date_time"/>
									</xsl:call-template>
								</xsl:if>
							</xsl:if>
						</td>
						<td class="BodyCOL4">
							<xsl:choose>
								<xsl:when test="not(payment_amount &gt;= 0)">
									<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;

								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td class="BodyCOL5">
							<xsl:choose>
								<xsl:when test="payment_amount &gt; 0">
									<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;

								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:if>
			</xsl:for-each>

			<tr class="FlightInformation1 RedFontB">
				<td class="BodyCOL1Total">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_37','OUTSTANDING BALANCE')" /> 
				</td>
				<td class="BodyCOL2Total">&#xA0;</td>
				<td class="BodyCOL3Total">&#xA0;</td>
				<xsl:variable name="SubTotal" select="number(format-number($Ticket_total,'###0.00')) - number(format-number($Payment_total,'###0.00'))"/>
				<xsl:if test="number($SubTotal) = 0">
					<td class="BodyCOL4Total">&#xA0;</td>
					<td class="BodyCOL5Total">
						<xsl:if test="count(../Payment)=0">
							0.00&#xA0;
						</xsl:if>
						<xsl:if test="count(../Payment)!=0">
							0.00&#xA0;
						</xsl:if>
					</td>
				</xsl:if>
				<xsl:if test="number($SubTotal) != 0">
					<xsl:if test="number($SubTotal) &gt;= 0">
						<td class="BodyCOL4Total">&#xA0;</td>
						<td class="BodyCOL5Total">
							<xsl:if test="count(../Payment)=0">
								<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;
							</xsl:if>
							<xsl:if test="count(../Payment)!=0">
								<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;
							</xsl:if>
						</td>
					</xsl:if>
					<xsl:if test="number($SubTotal) &lt; 0">
						<td class="BodyCOL4Total">
							<xsl:if test="count(../Payment)=0">
								<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#xA0;
							</xsl:if>
							<xsl:if test="count(../Payment)!=0">
								<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#xA0;
							</xsl:if>
						</td>
						<td class="BodyCOL5Total">&#xA0;</td>
					</xsl:if>
				</xsl:if>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="/">
	
		<div class="StepContent">
			<ul>
				<li>
					<li class="StepNumber">1</li>
					<li class="StepName">Search Result</li>
				</li>
				
				<li>
					<li class="StepNumber">2</li>
					<li class="StepName">Your Details</li>
				</li>
				
				<li>
					<li class="StepNumber">3</li>
					<li class="StepName">Payment</li>
				</li>
				
				<li>
					<li class="StepNumberActive">4</li>
					<li class="StepName">Booking Summary</li>
				</li>
			</ul>
			<div class="clear-all"></div>
	    </div>
	
		<div class="WrapperNptext">
			<xsl:if test="Booking/FlightSegment/flight_check_in_status_rcd = 'OPEN' and Booking/FlightSegment/allow_web_checkin_flag = '1'">
				<div class="NB">
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_1','NB: This Booking is available for online check-in')" />
				</div>
				<!--button-->
				<div>
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent">
						<a href="/webcheckin/default.aspx?webcheckin={concat(Booking/BookingHeader/booking_id, '|', Booking/Setting/user_id)}">
							<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_2','Web Check in')" />
						</a>
					</div>
					<div class="buttonCornerRight"></div>
				</div>
				<!--end button-->
			</xsl:if>
		</div>
		<div class="clear-all"></div>

		<div id="Bookingconfirmation">
			<div class="RedTopic">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_42','Your Confirmed Flight Details')" />
				<div class="clear-all"></div>
			</div>
		
			<div class="RedTopic">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_3','Booking Ref')" />
				<span class="BookingRefId">
					<xsl:value-of select="Booking/BookingHeader/record_locator"/>
				</span>
			</div>
		</div>
		
		<div class="clear-all"></div>
		
		<div class="COB bookinginform">
			<a href="javascript:LoadCob(true, '{Booking/BookingHeader/booking_id}');" title="Change of Booking" class="defaultbutton">
				<span>
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_5','Change of Booking')" />
				</span>
			</a>
		</div>
		
		<div class="clear-all"></div>
		
		<!-- TBL Your Itenerary -->
		<div class="boxborder-nolinebottom bookinginform">
			<div class="boxheader">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_6','Your Itinerary')" />
			</div>
			
			<div class="clear-all"></div>
			
			<!--Call Itinerary-->
			<xsl:call-template name="Itinerary"></xsl:call-template>
		</div>
		
		<div class="clear-all"></div>
		
	
		<div class="boxborder-nolinebottom bookinginform">
			<div class="boxheader">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_14','Passenger')" />
			</div>
	
			<div class="clear-all"></div>
			
			<!--Call Passenger-->
			<xsl:call-template name="Passenger"></xsl:call-template>
		</div>
		
		<!-- TBL Tickets and Seats  -->
		<div class="boxborder-nolinebottom bookinginform">
			<div class="boxheader">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_21','Tickets and Seats')" />
			</div>
			
			<div class="clear-all"></div>
			
			<!--Ticket And Seat-->
			<xsl:call-template name="TicketsAndSeat"></xsl:call-template>
		</div>
		
		<!-- TBL Cost Breakdown  -->
		<div class="boxborder-nolinebottom bookinginform">
			<div class="boxheader">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_27','Cost Breakdown')" />
			</div>
			
			<div class="clear-all"></div>
			
			<!--Call Cost Breakdown-->
			<xsl:call-template name="CostBreakdown"></xsl:call-template>
		</div>
		
		<div class="clear-all"></div>
		
		<!-- TBL Payment Received   -->
		<div class="boxborder-nolinebottom bookinginform">
			<div class="boxheader">
				<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_32','Payments Received')" />
			</div>
		
			<div class="clear-all"></div>
			
			<!--Call Payment-->
			<xsl:call-template name="Payment"></xsl:call-template>
		</div>
		<!-- End Table-->
		
		<!-- end wrapper -->
		<div class="clear-all"></div>
		<div class="BTN-Search">
			<div class="BacktoHomepageButton">
				<a href="javascript:loadHome();" class="defaultbutton">
					<span>
						<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_38','Back to Homepage')" />
					</span>
				</a>
			</div>
				
			<div class="CancelFlight">
				<a href="javascript:printReport('IF', 'Itinerary');" class="defaultbutton">
					<span>
						<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_39','Print Itinearary')" />
					</span>
				</a>
			</div>
			
			<div class="CancelFlight">
				<a href="javascript:EmailItineraryInput();" class="defaultbutton">
				<span>
					<xsl:value-of select="tikLanguage:get('Passenger_Itinerary_40','Email Itinearary')" />
				</span>
				</a>
			</div>
			
		</div>

    <xsl:choose>
      <xsl:when test="/Booking/InsuaranceNumber != '' ">
        <div id="hdInsuarance">
          <div>
            <xsl:value-of select="tikLanguage:get('','Mondial Assistance will send the policy number and receipt to your e-mail.')" />
          </div>
          <div class="ButtonAlignRight">
            <a href="" title="{tikLanguage:get('Booking_Step_2_13','Closed')}" onclick="ClosePopup();return false;">
              <div class="BTN-Left"></div>
              <div class="BTN-Middle">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_13','Closed')" />
              </div>
              <div class="BTN-Right"></div>
            </a>
          </div>
        </div>
      </xsl:when>
      <xsl:when  test="/Booking/InsuaranceNumber = ''">
        <div id="hdInsuarance">
          <div>
            <xsl:value-of select="tikLanguage:get('','Please contact Mondial Assistance for further information of your insurance.')" />
          </div>
          <div class="ButtonAlignRight">
            <a href="" title="{tikLanguage:get('Booking_Step_2_13','Closed')}" onclick="ClosePopup();return false;">
              <div class="BTN-Left"></div>
              <div class="BTN-Middle">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_13','Closed')" />
              </div>
              <div class="BTN-Right"></div>
            </a>
          </div>
        </div>
      </xsl:when>
    </xsl:choose>
	</xsl:template>
</xsl:stylesheet>