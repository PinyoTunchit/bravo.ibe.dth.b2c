<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">
	<xsl:template match="Client">
		<!-- Start Frequent Traveller by hnin -->
		<input type="hidden" id="hdClientId" name="hdClientId" value="{client_profile_id}"/>
		<input type="hidden" id="hdClientNumber" value="{client_number}"/>
		<div class="TBLFrequentFlyerBox">
			<div class="BoxHeader">
				<div class="TicketText">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_15','FREQUENT TRAVELLER')" />
        </div>
			</div>
			<div class="TBLFrequentFlyerUser">User <xsl:value-of select="firstname"/>&#160;<xsl:value-of select="lastname"/></div>
			<div class="TBLFrequentFlyerID">
				<xsl:value-of select="client_number"/>
			</div>
			<div class="TBLFrequentFlyerPoints">
				<ul>
					<li>
						<div class="PointsLeft">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_16','Member Tier')" />
            </div>
						<div class="PointsRight">
							<xsl:value-of select="member_level_display_name"/>
						</div>
					</li>
					<li>
						<div class="PointsLeft">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_17','Award Points')" />
            </div>
						<div class="PointsRight">
							<xsl:value-of select="format-number(ffp_total,'#,##0')"/>
						</div>
					</li>
					<li>
						<div class="PointsLeft">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_18','Status Points')" />
            </div>
						<div class="PointsRight">
							<span id="dvCurrentPoint">
								<xsl:value-of select="format-number(ffp_balance,'#,##0')"/>
							</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="clearboth"></div>
			<div class="line"></div>
			<div class="clearboth"></div>
			<div class="TBLFrequentFlyerDetails">
				<ul>
					<li>
						<a href="Javascript:LoadMilleageDetail();">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_19','Milleage account in detail')" />
            </a>
					</li>
					<li>
						<a href="Javascript:LoadRegistration_Edit();">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_20','My Profile')" />
            </a>
					</li>
					<li>
						<a href="Javascript:LoadRegistration_EditPassword();">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_21','Change Password')" />
            </a>
					</li>
					<li>
						<a href="Javascript:LoadMyBooking();">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_22','My Booking')" />
            </a>
					</li>
					<li>
						<a href="Javascript:ClientLogOff();">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_23','Log off')" />
            </a>
					</li>
				</ul>
			</div>
			<div class="clearboth"></div>
			<div class="line"></div>
			<div class="clearboth"></div>
			<div class="BlueBoxFooter"></div>
		</div>

		<div class="clearboth"></div>
		<!-- End Frequent Traveller by hnin -->
	</xsl:template>
</xsl:stylesheet>