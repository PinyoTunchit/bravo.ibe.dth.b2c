<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no"/>
	<xsl:template name="DateFormat">
		<xsl:param name="Date"></xsl:param>
		<xsl:if test="string-length($Date)!=0">
			<xsl:if test="$Date != '0001-01-01T00:00:00'">
				<xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template name="TimeFormat">
		<xsl:param name ="Time"></xsl:param>
	
		<xsl:if test="string-length($Time) =  3 ">
			0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
		</xsl:if>
	
		<xsl:if test="string-length($Time) = 4 ">
			<xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
		</xsl:if>
	
		<xsl:if test="string-length($Time) = 2 ">
			00:<xsl:value-of select="$Time"  />
		</xsl:if>
	</xsl:template>

	<xsl:template match="/">
	
		<div class="PaxAll">
			<div class="Paxicon">
				<xsl:for-each select="Booking/Passenger">
					<ul onclick="GetPassengerValue({position()});">
						<xsl:attribute name="class">
							<xsl:if test="passenger_type_rcd = 'ADULT'">PaxAdult</xsl:if>
							<xsl:if test="passenger_type_rcd = 'CHD'">PaxChildren</xsl:if>
							<xsl:if test="passenger_type_rcd = 'INF'">PaxInfant</xsl:if>
						</xsl:attribute>
						<li id="ulPaxTab_{position()}">
							<xsl:attribute name="class">
								<xsl:choose>
									<xsl:when test="position() = 1">active</xsl:when>
									<xsl:otherwise>disable</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</li>
						<!-- Passenger information -->
						<input id="hdPassengerId_{position()}" type="hidden" value="{passenger_id}" name="hdPassengerId" />
						<input id="hdPassengerProfileId_{position()}" type="hidden" value="{passenger_profile_id}" name="hdPassengerProfileId" />
						<input id="hdClientProfileId_{position()}" type="hidden" value="{client_profile_id}" name="hdClientProfileId" />
						<input id="hd_Vip_flag_{position()}" type="hidden" value="{vip_flag}" name="hdVip_flag" />
						<input id="hdMemberLevelRcd_{position()}" type="hidden" value="{member_level_rcd}" name="hdMemberLevelRcd" />
						<input id="hdTitle_{position()}" type="hidden" value="{concat(title_rcd, '|', gender_type_rcd)}" />
						
						<xsl:variable name="birth_date">
							<xsl:choose>
								<xsl:when test="date_of_birth = '0001-01-01T00:00:00'">
									<xsl:value-of select="tikLanguage:get('default_value_1','DD.MM.YYYY')" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="DateFormat">
										<xsl:with-param name="Date" select="date_of_birth"></xsl:with-param>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						
						<input id="hdBirthDate_{position()}" class="date" type="hidden" value="{$birth_date}" />
						<input id="hdClientNo_{position()}" class="clientno" type="hidden" value="{client_number}"/>
						<input id="hdLastname_{position()}" class="name" type="hidden" value="{lastname}" name="txtLastname" />
						<input id="hdName_{position()}" class="name" type="hidden" value="{firstname}" name="txtName"/>
						<input id="hdPaxType_{position()}" class="name" type="hidden" value="{passenger_type_rcd}" />
						
						<input id="hdNational_{position()}" class="name" type="hidden" value="{nationality_rcd}" />
						<input id="hdDocumentType_{position()}" class="name" type="hidden" value="{document_type_rcd}" />
						<input id="hdWeight_{position()}" class="name" type="hidden" value="{passenger_weight}" />
						<input id="hdBirthPlace_{position()}" class="name" type="hidden" value="{passport_birth_place}" />
            <input id="hdIssueCountry_{position()}" class="name" type="hidden" value="{passport_issue_country_rcd}" />
						
						<xsl:variable name="expiry_date">
							<xsl:choose>
								<xsl:when test="passport_expiry_date = '0001-01-01T00:00:00'">
									<xsl:value-of select="tikLanguage:get('default_value_1','DD.MM.YYYY')" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="DateFormat">
										<xsl:with-param name="Date" select="passport_expiry_date"></xsl:with-param>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						
						<input id="hdExpiryDate_{position()}" class="date" type="hidden" value="{$expiry_date}"/>
						
						<xsl:variable name="issue_date">
							<xsl:choose>
								<xsl:when test="passport_issue_date = '0001-01-01T00:00:00'">
									<xsl:value-of select="tikLanguage:get('default_value_1','DD.MM.YYYY')" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="DateFormat">
										<xsl:with-param name="Date" select="passport_issue_date"></xsl:with-param>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						
						<input id="hdIssueDate_{position()}" class="date" type="hidden" value="{$issue_date}"/>
						<input id="hdIssuePlace_{position()}" class="issueplace" type="hidden" value="{passport_issue_place}"/>
						<input id="hdDocNumber_{position()}" class="docnumber" type="hidden" value="{passport_number}"/>
		
					</ul>
				</xsl:for-each>
			</div>
		
			<div class="PaxInputDetail">
				<div class="PaxNameOuter">
					<div class="PaxInnerContent">
						<div id="dvPaxTabName" class="PaxName">
							<xsl:value-of select="tikLanguage:get('Booking_Step_3_27','Firstname')" />&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_3_28','Lastname')" />
						</div>
					</div>
				</div>
			
				<ul class="PaxDetail1">
					<li class="header1">
						<xsl:value-of select="tikLanguage:get('Booking_Step_3_10','Passenger details')" />
					</li>
				
					<!--Passenger Type-->
					<div id="dvPaxType"></div>
				
					<div class="Passengerdetailinput">
						
						<!--Title-->
						<div class="ContactDetail">
							<label for="stTitleInput" class="topname">
                <xsl:value-of select="tikLanguage:get('Booking_Step_4_4','Title')" />
							</label>
						
							<div class="InformationInput">
								<select id="stTitleInput" class="title" onchange="SetPassengerValue();">
									<option value="" class="watermarkOn"></option>
									<xsl:variable name="title_rcd" select="title_rcd"/>
									<xsl:for-each select="Booking/PassengerTitels">
										<option>
											<xsl:attribute name="value">
												<xsl:value-of select="concat(title_rcd, '|', gender_type_rcd)"/>
											</xsl:attribute>
											<xsl:choose>
												<xsl:when test="string-length($title_rcd) > 0">
													<xsl:if test="$title_rcd = title_rcd">
														<xsl:attribute name="selected">"selected"</xsl:attribute>
													</xsl:if>
												</xsl:when>
												<xsl:otherwise>
													<xsl:if test="../Setting/DefaultTitle = title_rcd">
														<xsl:attribute name="selected">"selected"</xsl:attribute>
													</xsl:if>
												</xsl:otherwise>
											</xsl:choose>
											<xsl:value-of select="display_name"/>
										</option>
									</xsl:for-each>
								</select>
							</div>
							
							<div class="clear-all"></div>
						
						</div>
					
						<!--Firstname-->
						<div class="ContactDetail">
							<label for="txtFirstNameInput" class="paxdetailtopname">
								<xsl:value-of select="tikLanguage:get('Booking_Step_3_27','First Name')" />
							</label>
						
							<div class="InformationInput">
								<input id="txtFirstNameInput" type="text" onkeyup="SetPassengerValue();"/>
							</div>
							<div class="clear-all"></div>
						</div>
					
						<!--LastName-->
						<div class="ContactDetail">
							<label for="txtLastnameInput" class="paxdetailtopname">
								<xsl:value-of select="tikLanguage:get('Booking_Step_3_28','Last Name')" />
							</label>
						
							<div class="InformationInput">
								<input id="txtLastnameInput" type="text" onkeyup="SetPassengerValue();"/>
							</div>
							<div class="clear-all"></div>
						</div>
				
						<div class="clear-all"></div>
				
						<!--Date of birth-->
						<xsl:if test="Booking/Setting/require_date_of_birth_flag = 1">
						
							<div class="ContactDetail">
								<label for="txtBirthDateInput" class="birthdate">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_29','Date of birth')" />
								</label>
							
								<div class="InformationInput">
									<input id="txtBirthDateInput" type="text" onblur="SetPassengerValue();"/>
								</div>
								<div class="clear-all"></div>
							</div>
						
						</xsl:if>
				
						<div class="ClientNumberDetail">
							<!--Client Number-->
							<div class="ContactDetail">
								<label for="txtClientNoInput" class="paxdetailtopname">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_37','Client Number')" />
								</label>
							
								<div class="InformationInput">
									<input id="txtClientNoInput" type="text" onkeyup="SetPassengerValue();" />
								</div>
								<div class="clear-all"></div>
							</div>
							
							<!--Varify client number button-->
							<div class="getclient">
								<div class="ButtonAlignLeft" onclick="ClearPassengerList();">
									<xsl:if test="count(Booking/Passenger[client_number > 0]) = 0">
										<xsl:attribute name="style">display:none;</xsl:attribute>
									</xsl:if>
								
									<a class="defaultbutton" title="Clear Passenger">
										<span><xsl:value-of select="tikLanguage:get('Booking_Step_3_38','Clear Passenger')" /></span>
									</a>
								</div>
							
								<div class="ButtonAlignLeft" onclick="GetClient(0, false);">
									<a class="defaultbutton" title="Get Client">
										<span><xsl:value-of select="tikLanguage:get('Booking_Step_3_39','Get Client')" /></span>
									</a>
								</div>
							</div>
						</div>

						<div class="clear-all"></div>
					
					</div>
				
					<div class="clear-all"></div>
				
					<xsl:if test="Booking/Setting/require_document_details_flag = 1">	  
						<div class="passport">
							<!-- Document Number -->
							<div class="ContactDetail passportnumberfield">
								<label for="txtDocumentNumber">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_33','Document Number')" />
								</label>
							
								<div class="InformationInput">
									<input id="txtDocumentNumber" type="text" onblur="SetPassengerValue();" maxlength="60" />
									<div class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
							
							<!--Expiry Date-->
							<div class="ContactDetail">
								<label for="txtExpiryDate">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_36','Expiry Date')" />
								</label>

								<div class="InformationInput">
									<!--<select id="passportexpiry" class="PassportExpiryDate"></select>
									<select id="passportexpirymonth" class="PassportExpiryDate"></select>
									<select id="passportexpiryyear" class="PassportExpiryDate"></select>-->
									<input id="txtExpiryDate" type="text" onblur="SetPassengerValue();" /> 
								</div>
								<div class="clear-all"></div>
							</div>
							
							<!-- Place of Issue -->
							<div class="ContactDetail">
								<label for="txtPlaseOfIssue">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_34','Place of Issue')" />
								</label>
							
								<div class="InformationInput">
									<input id="txtPlaseOfIssue" type="text" onblur="SetPassengerValue();" maxlength="60" class="issuecountry" />
									<div class="mandatory"></div>
								</div>
								
								<div class="clear-all"></div>
							
							</div>
							
							<div class="clear-all"></div>

							<!--Place Of Birth-->
							<div class="ContactDetail">
								<label for="txtPlaseOfBirth">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_30','Place of Birth')" />
								</label>
								
								<div class="InformationInput">
									<input id="txtPlaseOfBirth" type="text" onblur="SetPassengerValue();" />
								</div>
								<div class="clear-all"></div>
							</div>
						
							<!--Nationality-->
							<div class="ContactDetail">
								<label for="stNational">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_31','Nationality')" />
								</label>
								
								<div class="InformationInput">
									<select id="stNational" class="nationality" onchange="SetPassengerValue();">
										<option value="" class="watermarkOn"></option>
										<xsl:variable name="nationality_rcd" select="nationality_rcd"/>
										<xsl:for-each select="Booking/Countrys[nationality_country_flag = 1]">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="country_rcd"/>
												</xsl:attribute>
												<xsl:if test="$nationality_rcd = country_rcd">
													<xsl:attribute name="selected">selected</xsl:attribute>
												</xsl:if>
												<xsl:value-of select="display_name"/>
											</option>
										</xsl:for-each>
									</select>
								</div>
								<div class="clear-all"></div>
							</div>
							
							<!--Document Type-->
							<div class="ContactDetail">
								<label for="stDocumentType">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_32','Document Type')" />
								</label>
								
								<div class="InformationInput">
									<select id="stDocumentType" class="doctype" onchange="SetPassengerValue();">
										<option value="" class="watermarkOn"></option>
										<xsl:variable name="document_type_rcd" select="document_type_rcd"/>
										<xsl:for-each select="Booking/DocumentType">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="document_type_rcd"/>
												</xsl:attribute>
												<xsl:if test="$document_type_rcd = document_type_rcd">
													<xsl:attribute name="selected">selected</xsl:attribute>
												</xsl:if>
												<xsl:value-of select="display_name"/>
											</option>
										</xsl:for-each>
									</select>
								</div>
								<div class="clear-all"></div>
							</div>
							
							<!--Issue Date-->
							<div class="ContactDetail">
								<label for="txtIssueDate">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_35','Issue Date')" />
								</label>
								
								<div class="InformationInput">
									<input id="txtIssueDate" type="text" onblur="SetPassengerValue();" />
								</div>
								<div class="clear-all"></div>
							</div>
						
							<!--Expiry Date-->
							<div class="ContactDetail">
								<label for="txtWeight">
									<xsl:value-of select="tikLanguage:get('Booking_Step_3_22','Passenger Weight')" />
								</label>
								
								<div class="InformationInput">
									<input id="txtWeight" type="text" onblur="SetPassengerValue();" />
								</div>
								<div class="clear-all"></div>
							</div>

              <!--Issue Country-->
              <div class="ContactDetail">
                <label for="stIssueCountry">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_3_47','Issue Country')" />
                </label>

                <div class="InformationInput">
                  <select id="stIssueCountry" class="nationality" onchange="SetPassengerValue();">
                    <option value="" class="watermarkOn"></option>
                    <xsl:variable name="passport_issue_country_rcd" select="passport_issue_country_rcd"/>
                    <xsl:for-each select="Booking/Countrys[issue_country_flag = 1]">
                      <option>
                        <xsl:attribute name="value">
                          <xsl:value-of select="country_rcd"/>
                        </xsl:attribute>
                        <xsl:if test="$passport_issue_country_rcd = country_rcd">
                          <xsl:attribute name="selected">selected</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="display_name"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </div>
                <div class="clear-all"></div>
              </div>
							<div class="clear-all"></div>

							<div class="SelectSeatButton">
								<div id="dvFlightInformation" runat="server"></div>
							</div>
						
							<div class="clear-all"></div>
						</div>
					</xsl:if>
				
				</ul>
			</div>
		</div>
	
		<div class="clear-all"></div>
	
	</xsl:template>
</xsl:stylesheet>