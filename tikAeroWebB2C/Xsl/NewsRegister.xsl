<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no"/>
	<xsl:template name="DateFormat">
		<xsl:param name="Date">
		</xsl:param>
		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="/">

		<div class="clearboth"></div>
		<table class="GridNewsRegister" cellspacing="0" cellpadding="0" rules="all" border="1" style="border-collapse:collapse;">
		<xsl:if test="count(NewDataSet/Table) > 0">				
			
			<tr class="GridNewsRegisterHeader" style="border-color:#F9D43D;">
				<td class="RegHeadCOL1" >
					<input id="chkSearchAll" type="checkbox" name="chkSearchAll" onclick="javascript:validSearchAll(this.checked);" />
				</td>
				<td >
					<xsl:value-of select="tikLanguage:get('NewRegister_1_0','No.')" />
				</td>
				<td >
					<xsl:value-of select="tikLanguage:get('NewRegister_1_1','Register Date')" />
				</td>
				<td >
					<xsl:value-of select="tikLanguage:get('NewRegister_1_1_4','Name')" />
				</td>
				<td>
					<xsl:value-of select="tikLanguage:get('NewRegister_1_1_5','Email')" />
				</td>
				<td >
					<xsl:value-of select="tikLanguage:get('NewRegister_1_1_6','Post code')" />
				</td>
				<td >
					<xsl:value-of select="tikLanguage:get('NewRegister_1_1_11','Country')" />
				</td>
				<td>
					<xsl:value-of select="tikLanguage:get('NewRegister_1_1_12','Travel for')" />
				</td>
				<td>
					<xsl:value-of select="tikLanguage:get('NewRegister_1_1_13','Children')" />
				</td>
				<td>
					<xsl:value-of select="tikLanguage:get('NewRegister_1_1_14','Status')" />
				</td>
			</tr>
			<xsl:for-each select="NewDataSet/Table">
				<tr>
					<td class="RegBodyCOL1">
						<input type ="checkbox" id="chkSearchAll_{position()}" name="chkRegister_id" value="{Register_id}"></input>
					</td>
					<td class="RegBodyCOL2">
						<a class="tooltip" href="javascript:void(0)" onclick="return false;">
							<xsl:value-of select="(pageStart+position())"/>.
							<span>
								<xsl:value-of select="concat(Departure_name, ' ==> ', Destination_name)"/>
							</span>
						</a>						
					</td>
					<td class="RegBodyCOL3">
						<xsl:call-template name="DateFormat">
							<xsl:with-param name="Date" select="Register_date"></xsl:with-param>
						</xsl:call-template>
					</td>
					<td class="RegBodyCOL4">
						<xsl:value-of select="Register_name"/>						
					</td>
					<td class="RegBodyCOL5">
						<xsl:value-of select="Register_email"/>
					</td>
					<td class="RegBodyCOL6">
						<xsl:value-of select="Register_postcode"/>
					</td>
					<td class="RegBodyCOL7">
						<xsl:value-of select="Country_name"/>
					</td>
					<td class="RegBodyCOL8">
						<xsl:value-of select="Travel_for"/>
					</td>
					<td class="RegBodyCOL9">
						<xsl:value-of select="Have_childern"/>
					</td>
					<td class="RegBodyCOL10">
						<xsl:if test="(count(Email_flag) > 0 ) and (string-length(Email_flag) > 0 )">
							<span>							
								<div class="deleteicon"></div>									
							</span>
						</xsl:if>
					</td>					
				</tr>

			</xsl:for-each>
			<tr>
				<td colspan="10" style="height: 29px; text-align:right;">
					Page
				<xsl:for-each select="NewDataSet/result_Page">   
					<xsl:choose>
						<xsl:when test="(link_Page = pageNumber)"> 
							<a href="javascript:SearchNewsRegister('{link_Page}');">
								<strong>
									<font color="red">
										{<xsl:value-of select="link_Page"/>}
									</font>
								</strong>
							</a>
						</xsl:when>
						<xsl:otherwise> 
							<a href="javascript:SearchNewsRegister('{link_Page}');">
								{<xsl:value-of select="link_Page"/>}
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>					
					<xsl:for-each select="NewDataSet/Summary">	  					
						<strong>
							<xsl:value-of select="record_Count"/>
						</strong> Records
					</xsl:for-each>
				</td>
			</tr>
			<tr>
				<td colspan="10" class="groupFormClear">					
					<div class="RegisterCancelButton" onclick="DeleteNewsRegister();">
						<div class="buttonCornerLeft"></div>
						<div class="buttonContent">Delete</div>
						<div class="buttonCornerRight"></div>
					</div>
					<div class="RegisterSubmitButton" onclick="GetNewsMailForm()">
						<div class="buttonCornerLeft"></div>
						<div class="buttonContent">Send mail
						</div>
						<div class="buttonCornerRight"></div>
					</div>
					</td>
			</tr>
		</xsl:if>
			<tr>
				<td class="BodyNoRegister">
					<xsl:if test="count(NewDataSet/Table) = 0">
						<div class="noflight">Not found Register data.</div>
					</xsl:if>
				</td>
			</tr>
		</table>

	</xsl:template>
</xsl:stylesheet>