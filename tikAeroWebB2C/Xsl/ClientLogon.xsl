<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">

	<xsl:template match="/">
		<div class="TBLloginBox">
			<div class="Header">LOGIN</div>
			<ul>
				<li class="Text">
					Client ID/ Email
					<input id="txtClientID" type="text" onkeypress="return SubmitEnterUser(this,event,'false')" /> 
				</li>
				<li class="Text">
					Password
					<input id="txtPassword" type="password" onkeypress="return SubmitEnterUser(this,event,'false')" />
				</li>
			</ul>
            <div class="forgetID" onclick="LoadForgetPassword();">Forget Password</div>
			<div class="BTN-FFP">
				<div class="buttonCornerLeft"></div>
				<div class="buttonContent" onclick="ClientLogon();">LOGIN</div>
				<div class="buttonCornerRight"></div>
			</div>
		</div>
        <div class="clearboth"><!-- control FFP HOME --></div>
	</xsl:template>
	
</xsl:stylesheet>