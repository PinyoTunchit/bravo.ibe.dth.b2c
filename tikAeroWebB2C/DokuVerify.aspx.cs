﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Specialized;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;
using System.Collections.Specialized;

namespace tikAeroB2C
{
    public partial class DokuVerify : System.Web.UI.Page
    {
        private string strRequestValue = "";

        private string RequestStream()
        {
            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("DokuPaymentSetting");
            StringBuilder stbHtml = new StringBuilder();
            stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["ActionURL"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='CHANNEL' value='" + Setting["CHANNEL"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='TYPE' value='" + Setting["TYPE"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='BASKET' value='" + "%BASKET%" + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='MERCHANTID' value='" + Setting["MERCHANTID"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='CHAINNUM' value='" + Setting["CHAINNUM"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='TRANSIDMERCHANT' value='" + "%TRANSIDMERCHANT%" + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='AMOUNT' value='" + "%AMOUNT%" + "' >" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='CURRENCY' value='" + "%CURRENCY%" + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='PurchaseCurrency' value='" + "%CURRENCY%" + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='acquirerBIN' value='" + Setting["acquirerBIN"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='password' value='" + Setting["password"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='URL' value='" + Setting["URL"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='MALLID' value='" + Setting["MALLID"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='WORDS' value='" + "%WORDS%" + "' >" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='SESSIONID' value='" + "%SESSIONID%" + "' >" + Environment.NewLine);
            stbHtml.Append("</form>" + Environment.NewLine);

            return stbHtml.ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    strRequestValue = "DokuVerify.aspx?";
                    int i = 0;
                    for (i = 0; i < Request.QueryString.Count - 1; i++)
                    {
                        strRequestValue += Request.QueryString.Keys[i] + "=" + Request.QueryString[i] + "&";
                    }
                    strRequestValue += Request.QueryString.Keys[i] + "=" + Request.QueryString[i];//last value no add "&"

                    writeToLogFile(strRequestValue + "::" + Session.SessionID);

                    if (Request.QueryString["TRANSIDMERCHANT"] != null && Request.QueryString["AMOUNT"] != null)
                    {
                        VerifyPayment();
                    }
                    else if (Request.QueryString["OrderNumber"] != null && Request.QueryString["RESPONSECODE"] != null)
                    {
                        if (NotifyPayment() == true)
                        {
                            DokuPaymentSave(null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                    }
                }
                else
                    Response.Write("Stop");//Request.QueryString is Null
            }

        }

        private void VerifyPayment()
        {
            //TRANSIDMERCHANT=120127184154792&AMOUNT=810000.00&CURRENCY=360&SESSIONID=kuy2ehzudws5qz45qj04iz45&WORDS=331fbcf2c3653355fa60170b863a23b8121ed18a
            if (Request.QueryString["TRANSIDMERCHANT"] == "")
            {
                Response.Write("Stop 1-1");//(Request.QueryString["TRANSIDMERCHANT"] == ""
                return;
            }

            string TRANSIDMERCHANT = Request.QueryString["TRANSIDMERCHANT"].ToString();

            ServiceClient objService = new ServiceClient();
            Agents objAgent = new Agents();
            Payments objPayment = new Payments();

            objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgent);
            objPayment.objService = (TikAeroXMLwebservice)objService.objService;

            DataSet ds = objPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, "", "", "",
                                                                    TRANSIDMERCHANT, "", "", "", "", "", "", 0, 0, false);

            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                Response.Write("Stop 1-2");//Not Found TRANSIDMERCHANT in Database
                return;
            }

            string strColumns = "";
            foreach (DataColumn dc in ds.Tables[0].Columns)
            {
                strColumns += dc.ColumnName + "=" + ds.Tables[0].Rows[0][dc] + "&";
            }
            writeToLogFile(strColumns);


            if (Request.QueryString["AMOUNT"] == "")
            {
                Response.Write("Stop 1-3");//(Request.QueryString["AMOUNT"] == ""
                return;
            }
            if (Convert.ToString(ds.Tables[0].Rows[0]["payment_amount"]) == "")
            {
                Response.Write("Stop 1-4");//Request AMOUNT not correct
                return;
            }
            Double payment_amount = Convert.ToDouble(ds.Tables[0].Rows[0]["payment_amount"]);
            if (Convert.ToDouble(Request.QueryString["AMOUNT"].Trim()) != payment_amount)
            {
                Response.Write("Stop 1-4");//Request AMOUNT not correct
                return;
            }

            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("DokuPaymentSetting");
            string strTransCurrency = Setting[Convert.ToString(ds.Tables[0].Rows[0]["currency_rcd"])];
            if (Request.QueryString["CURRENCY"].Trim() != strTransCurrency)
            {
                Response.Write("Stop 1-5");//Request CURRENCY not correct
                return;
            }

            string strTransactionReference = Convert.ToString(ds.Tables[0].Rows[0]["transaction_reference"]).ToLower().Trim();
            if (Request.QueryString["SESSIONID"].ToLower().Trim() != strTransactionReference)
            {
                Response.Write("Stop 1-6");//Request SESSIONID not correct
                return;
            }

            string strEncryptText = payment_amount.ToString("#0.00") +
                                             Setting["MERCHANTID"] +
                                             Setting["SharedKey"] +
                                             TRANSIDMERCHANT;
            string strEncryptResult = "";

            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1CryptoServiceProvider.Create();
            byte[] hash2 = sha1.ComputeHash(Encoding.ASCII.GetBytes(strEncryptText));
            StringBuilder digest = new StringBuilder();
            foreach (byte n in hash2)
                digest.Append(n.ToString("x2"));
            strEncryptResult = digest.ToString();

            if (Request.QueryString["WORDS"].ToLower().Trim() != strEncryptResult.ToLower())
            {
                Response.Write("Stop 1-7");//Request WORDS not correct
                return;
            }

            if (Convert.ToString(ds.Tables[0].Rows[0]["response_code"]) != "")
            {
                Response.Write("Stop 1-8");//Status of Transactoin is expired
                return;
            }

            string strRequestStream = RequestStream();
            strRequestStream = strRequestStream.Replace("%BASKET%", Convert.ToString(ds.Tables[0].Rows[0]["transaction_description"]) + "," + payment_amount.ToString("#0.00") + ",1," + payment_amount.ToString("#0.00"));
            strRequestStream = strRequestStream.Replace("%TRANSIDMERCHANT%", TRANSIDMERCHANT);
            strRequestStream = strRequestStream.Replace("%AMOUNT%", payment_amount.ToString("#0.00"));
            strRequestStream = strRequestStream.Replace("%CURRENCY%", strTransCurrency);
            strRequestStream = strRequestStream.Replace("%WORDS%", strEncryptResult);
            strRequestStream = strRequestStream.Replace("%SESSIONID%", strTransactionReference);

            objPayment.objService.UpdatePaymentApproval("",
                                                        TRANSIDMERCHANT,
                                                        strTransactionReference,
                                                        Convert.ToString(ds.Tables[0].Rows[0]["transaction_description"]),
                                                        "", "", "", "", "",
                                                        "",
                                                        "Verified",
                                                        "",
                                                        "",
                                                        "",
                                                        Convert.ToString(ds.Tables[0].Rows[0]["payment_approval_id"]),
                                                        strRequestStream,
                                                        strRequestValue,
                                                        "",
                                                        "");
            Response.Write("Continue");
        }


        private Boolean NotifyPayment()
        {
            //OrderNumber=120127184154792&RESULTMSG=&RESPONSECODE=00&APPROVALCODE=38&SESSIONID=kuy2ehzudws5qz45qj04iz45&CARDNUMBER=5***********8754&BANK=Bank BNI&RESULT=Success&WORDS=6294a9084a65c363cc1036ea16568d1e731550c3
            if (Request.QueryString["OrderNumber"] == "")
            {
                Response.Write("Stop 2-1");//(Request.QueryString["OrderNumber"] == ""
                return false;
            }

            string OrderNumber = Request.QueryString["OrderNumber"].ToString();

            ServiceClient objService = new ServiceClient();
            Agents objAgent = new Agents();
            Payments objPayment = new Payments();

            objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgent);
            objPayment.objService = (TikAeroXMLwebservice)objService.objService;

            DataSet ds = objPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, "", "", "",
                                                                    OrderNumber, "", "", "", "", "", "", 0, 0, false);

            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                Response.Write("Stop 2-2");//Not Found OrderNumber in Database
                return false;
            }

            string strColumns = "";
            foreach (DataColumn dc in ds.Tables[0].Columns)
            {
                strColumns += dc.ColumnName + "=" + ds.Tables[0].Rows[0][dc] + "&";
            }
            writeToLogFile(strColumns);


            if (Convert.ToString(ds.Tables[0].Rows[0]["payment_state_code"]) != "Verified")
            {
                Response.Write("Stop 2-3");//Payment Status not Verified
                return false;
            }


            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("DokuPaymentSetting");
            string strTransCurrency = Setting[Convert.ToString(ds.Tables[0].Rows[0]["currency_rcd"])];

            string strTransactionReference = Convert.ToString(ds.Tables[0].Rows[0]["transaction_reference"]).ToLower().Trim();
            if (Request.QueryString["SESSIONID"].ToLower().Trim() != strTransactionReference)
            {
                Response.Write("Stop 2-4");//Request SESSIONID not correct
                return false;
            }

            Double payment_amount = Convert.ToDouble(ds.Tables[0].Rows[0]["payment_amount"]);
            string strEncryptText = payment_amount.ToString("#0.00") +
                                             Setting["MERCHANTID"] +
                                             Setting["SharedKey"] +
                                             OrderNumber +
                                             Request.QueryString["RESULT"].ToString().Trim();
            string strEncryptResult = "";

            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1CryptoServiceProvider.Create();
            byte[] hash2 = sha1.ComputeHash(Encoding.ASCII.GetBytes(strEncryptText));
            StringBuilder digest = new StringBuilder();
            foreach (byte n in hash2)
                digest.Append(n.ToString("x2"));
            strEncryptResult = digest.ToString();

            if (Request.QueryString["WORDS"].ToLower().Trim() != strEncryptResult.ToLower())
            {
                Response.Write("Stop 2-5");//Request WORDS not correct
                return false;
            }

            if (Convert.ToString(ds.Tables[0].Rows[0]["response_code"]) != "")
            {
                Response.Write("Stop 2-6");//Status of Transactoin is expired
                return false;
            }

            string strRequestStream = RequestStream();
            strRequestStream = strRequestStream.Replace("%BASKET%", Convert.ToString(ds.Tables[0].Rows[0]["transaction_description"]) + "," + payment_amount.ToString("#0.00") + ",1," + payment_amount.ToString("#0.00"));
            strRequestStream = strRequestStream.Replace("%TRANSIDMERCHANT%", OrderNumber);
            strRequestStream = strRequestStream.Replace("%AMOUNT%", payment_amount.ToString("#0.00"));
            strRequestStream = strRequestStream.Replace("%CURRENCY%", strTransCurrency);
            strRequestStream = strRequestStream.Replace("%WORDS%", strEncryptResult);
            strRequestStream = strRequestStream.Replace("%SESSIONID%", strTransactionReference);

            string PaymentState = "DECLINED";
            string ResponseCode = "DECLINED";
            if (Request.QueryString["RESPONSECODE"].ToString().Trim() == "00")
            {
                PaymentState = "PAID";
                ResponseCode = "APPROVED";
            }


            objPayment.objService.UpdatePaymentApproval(Request.QueryString["APPROVALCODE"],
                                                        OrderNumber,
                                                        strTransactionReference,
                                                        Convert.ToString(ds.Tables[0].Rows[0]["transaction_description"]),
                                                        "", "", "", Request.QueryString["BANK"], "",
                                                        Request.QueryString["RESULTMSG"],
                                                        PaymentState,
                                                        ResponseCode,
                                                        "",
                                                        Request.QueryString["RESULT"],
                                                        Convert.ToString(ds.Tables[0].Rows[0]["payment_approval_id"]),
                                                        strRequestStream,
                                                        strRequestValue,
                                                        Request.QueryString["CARDNUMBER"],
                                                        "");
            Response.Write("Continue");
            return true;

        }

        private string DokuPaymentSave(B2CVariable objUv,
                                            BookingHeader bookingHeader,
                                            Itinerary itinerary,
                                            Passengers passengers,
                                            Quotes quotes,
                                            Fees fees,
                                            Fees objPaymentFees,
                                            Mappings mappings,
                                            Services services,
                                            Remarks remarks,
                                            Payments payments,
                                            Payments ccPayment,
                                            Taxes taxes)
        {
            string strResult = string.Empty;

            try
            {


                if (ccPayment == null)
                {
                    ccPayment = new Payments();
                }
                else
                {
                    ccPayment.Clear();
                }

                if (B2CSession.AgentService == null)
                {
                    //Initial service
                    Helper objHelper = new Helper();
                    objHelper.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;

                    ccPayment.objService = B2CSession.AgentService;
                }

                DataSet ds = ccPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, "", "", "",
                Request.Form["TRANSIDMERCHANT"], "", "", "", "", "", "", 0, 0, false);

                Library objLi = new Library();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    //check status response
                    string chkResponse = Convert.ToString(ds.Tables[0].Rows[0]["response_text"]).Trim();
                    if (chkResponse == "Success")
                    {
                        string transid_Merchant = Convert.ToString(ds.Tables[0].Rows[0]["payment_reference"]).Trim();
                        string RefID = Convert.ToString(ds.Tables[0].Rows[0]["transaction_description"]).Split(' ')[6];
                        Guid booking_id = new Guid(RefID);
                        string Refbooking_payment_id = Convert.ToString(ds.Tables[0].Rows[0]["booking_payment_id"]).Trim();
                        Guid booking_payment_id = new Guid(Refbooking_payment_id);

                        //Fill credit card payment object.
                        Payment p = new Payment(); NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("DokuPaymentSetting");
                        p.form_of_payment_subtype_rcd = Setting["form_of_payment_subtype_rcd"].ToString();
                        p.form_of_payment_rcd = Setting["form_of_payment_rcd"].ToString();
                        p.name_on_card = Setting["NameOnCard"].ToString();
                        p.document_number = Setting["CreditCardNumber"].ToString();
                        p.booking_payment_id = booking_payment_id;
                        p.booking_id = booking_id;
                        ccPayment.Add(p);

                        ccPayment[0].document_amount = ccPayment[0].payment_amount;
                        ccPayment[0].document_number = Convert.ToString(ds.Tables[0].Rows[0]["card_number"]).Trim().Replace("*", "0");

                        Booking objBooking = new Booking();
                        objBooking = ccPayment.SavePaymentRedirect(booking_id, ccPayment);
                    }
                    else
                    {
                        Response.Write("Stop 3-1"); //Response is not Success
                    }
                }
                else
                {
                    Response.Write("Stop 3-2"); //Dataset is null
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.StackTrace);
            }
            return strResult;
        }

        public static void writeToLogFile(string logMessage)
        {
            try
            {
                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("DokuPaymentSetting");
                string strLogMessage = string.Empty;
                string strLogFile = string.Empty;
                StreamWriter swLog;

                if (Setting != null && Setting["logFilePath"] != null)
                {
                    string serverpath = HttpContext.Current.Server.MapPath("~") + @Setting["logFilePath"];
                    strLogFile = serverpath + "DokuVerify_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                    strLogMessage = string.Format("{0} {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), logMessage);//+Environment.NewLine

                    if (Directory.Exists(serverpath))
                    {
                        if (!File.Exists(strLogFile))
                        {
                            swLog = new StreamWriter(strLogFile);
                        }
                        else
                        {
                            swLog = File.AppendText(strLogFile);
                        }

                        swLog.WriteLine(strLogMessage);
                        swLog.Flush();
                        swLog.Close();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}