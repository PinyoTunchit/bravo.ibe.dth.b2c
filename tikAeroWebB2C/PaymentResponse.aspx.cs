﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using System.Configuration;
using System.Xml;
using System.Text;
//
using tikSystem.Web.Contact;
using tikSystem.Web.PaymentGateway;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;
using System.Collections.Specialized;
using System.Reflection;
using System.ComponentModel;
//using Avantik.Web.Service.Helpers;

namespace tikAeroB2C
{
    class PaymentApproval
    {
        public Guid booking_payment_id { get; set; }
        public string payment_amount { get; set; }
        public string transaction_description { get; set; }
        public string card_rcd { get; set; }
        public string card_number { get; set; }
        public string approval_code { get; set; }
        public string error_code { get; set; }
        public string error_response { get; set; }
        public string response_code { get; set; }
        public string response_text { get; set; }
        public string return_code { get; set; }

        public PaymentApproval(DataSet dataSet)
        {
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                GeneratePaymentApproval(dataSet.Tables[0].Rows[0]);
        }

        public PaymentApproval(DataRow dataRow)
        {
            GeneratePaymentApproval(dataRow);
        }

        private void GeneratePaymentApproval(DataRow dataRow)
        {
            PropertyInfo[] props = this.GetType().GetProperties();
            for (int i = 0; i < props.Length; i++)
            {
                PropertyInfo prop = this.GetType().GetProperty(props[i].Name);
                string value = dataRow[prop.Name] as string;
                if (value != null)
                {
                    object t = (object)TypeDescriptor.GetConverter(prop.PropertyType).ConvertFromInvariantString(value);
                    prop.SetValue(this, Convert.ChangeType(t, prop.PropertyType), null);
                }
            }
        }
    }

    public partial class PaymentResponse : System.Web.UI.Page
    {
        private string _strReplyStream = string.Empty;
        public string ReplyStream
        {
            get { return _strReplyStream; }
            set { _strReplyStream = value; }
        }

        private Guid _bookingID = Guid.Empty;
        public Guid BookingID
        {
            get { return _bookingID; }
            set { _bookingID = value; }
        }

        private string _strVerifyRestult = string.Empty;
        public string VerifyRestult
        {
            get { return _strVerifyRestult; }
            set { _strVerifyRestult = value; }
        }

        PaymentGatewayFactory.GateWayType gateway;// = PaymentGatewayFactory.GateWayType.Ogone3D;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    NameValueCollection query = Request.QueryString;
                    NameValueCollection form = Request.Form;
                    OGONE3DResponses response = null;
                    if (query.Count > 0 || form.Count > 0)
                    {
                        ReplyStream = "Querystring-> " + Request.Url.AbsoluteUri + ", Form-> " + form.ToString();
                        LogsManagement.SaveNotifyLogs(ReplyStream);
                        response = new OGONE3DResponses();
                        string type = !string.IsNullOrEmpty(response.type) ? response.type : "Ogone3D";
                        gateway = (PaymentGatewayFactory.GateWayType)Enum.Parse(typeof(PaymentGatewayFactory.GateWayType), type, true);
                        LoadGateway(gateway);
                    }
                    else
                    {
                        LogsManagement.SaveNotifyLogs("No parameters passed to the page.");
                        gateway = (PaymentGatewayFactory.GateWayType)Enum.Parse(typeof(PaymentGatewayFactory.GateWayType), "Ogone3D", true);
                        LoadGateway(gateway);
                    }
                }
            }
            catch (Exception ex)
            {
                LogsManagement.SaveErrorLogs(ex, Request.Url.AbsoluteUri);
                Response.Write(ex.StackTrace);
            }
        }

        private void VerifyPayment()
        {
            string result = VerifyPayment(gateway, "Continue");
            //Write Log
            LogsManagement.SaveNotifyLogs(result);
            Response.Write(result);
        }

        public string Ref(PaymentGatewayFactory.GateWayType gatewayType)
        {
            tikSystem.Web.Contact.PaymentGateway objGateway = PaymentGatewayFactory.GetGatePaymentGateway(gatewayType);
            return objGateway.GetReferenceID();
        }

        public string VerifyPayment(PaymentGatewayFactory.GateWayType gatewayType, string successResponse)
        {
            string result = string.Empty;
            tikSystem.Web.Contact.PaymentGateway objGateway = PaymentGatewayFactory.GetGatePaymentGateway(gatewayType);

            ServiceClient objService = new ServiceClient();
            Agents objAgent = new Agents();
            Payments objPayment = new Payments();
            string strRequestStream = string.Empty;
            DataSet ds = new DataSet();
            PaymentApproval paymentApproval = null;

            try
            {
                objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgent);
                objPayment.objService = (TikAeroXMLwebservice)objService.objService;

                //Validate ref id
                if (objGateway.GetReferenceID() == string.Empty)
                    result = objGateway.VerifyPayment();

                ds = objPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, "", "", "",
                                                                        objGateway.GetReferenceID(), "", "", "", "", "", "", 0, 0, false);


                FillBooking(ds, objGateway, objService);
                objGateway.PaymentApprovals = ds;
                if (result == string.Empty)
                    result = objGateway.VerifyPayment();

                //Write Log
                LogsManagement.SaveNotifyLogs("VerifyResult : " + result);
                LogsManagement.SaveNotifyLogs("PaymentRequestStreamText : " + objGateway.PaymentRequestStreamText);
                LogsManagement.SaveNotifyLogs("ReplyStreamResultBefore : " + ReplyStream + "AuthId : " + objGateway.AuthId + "Ref : " + objGateway.GetReferenceID());
                if (ReplyStream == "")
                    ReplyStream = objGateway.ResponseStream + ": result : " + result;
                if (ReplyStream == "")
                    ReplyStream = Request.Url.AbsoluteUri;

                LogsManagement.SaveNotifyLogs("ReplyStreamResultAfter : " + ReplyStream + "AuthId : " + objGateway.AuthId + "Ref : " + objGateway.GetReferenceID());

                objPayment.objService.UpdatePaymentApproval(objGateway.AuthId,
                                                               objGateway.GetReferenceID(),
                                                               objGateway.PayRef,
                                                               Convert.ToString(ds.Tables[0].Rows[0]["transaction_description"]),
                                                               "", objGateway.AvsResponse,
                                                               "", objGateway.CvvResponse,
                                                               objGateway.ErrorCode, objGateway.ErrorResponse,
                                                               objGateway.PaymentState,
                                                               objGateway.SuccessCode,
                                                               "",
                                                               objGateway.Holder,
                                                               Convert.ToString(ds.Tables[0].Rows[0]["payment_approval_id"]),
                                                               objGateway.PaymentRequestStreamText,
                                                               ReplyStream,
                                                               objGateway.CardNo,
                                                               objGateway.CardType);

                if (result != "Error")
                    result = successResponse;

            }
            catch (Exception ex)
            {
                result = "[STEP] : Verify :" + objGateway.GetReferenceID() + ": " + ex.StackTrace.ToString();
                //Write Log
                LogsManagement.SaveErrorLogs(ex, "[STEP] : Verify :" + objGateway.GetReferenceID());
            }
            finally
            {
                //Dispose Object
                objGateway = null;
                objService = null;
                objAgent = null;
                objPayment = null;
                strRequestStream = string.Empty;
                ds.Dispose();
            }
            return result;
        }
        public string LoadGateway(PaymentGatewayFactory.GateWayType gatewayType)
        {
            try
            {
                PaymentGateway objGateway = PaymentGatewayFactory.GetGatePaymentGateway(gatewayType);
                ResponseStatus objStatus;
                objStatus = objGateway.PaymentResponse();
                ReplyStream = objGateway.ResponseStream;
                string result = string.Empty;
                switch (objStatus)
                {
                    case ResponseStatus.Success:
                        //Write Log
                        LogsManagement.SaveNotifyLogs("ResponseStatus: Success");

                        if (PaymentSave(objGateway.FormOfPayment, objGateway.FormOfPaymentSubtype, gatewayType))
                        {
                            Response.Write(GenerateReturnStream(Ref(gatewayType), ResponseStatus.Success));
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objGateway.PaymentState))
                            {
                                Response.Write(GenerateReturnStream(Ref(gatewayType), ResponseStatus.FailUpdateBooking));
                            }
                        }
                        break;
                    case ResponseStatus.SuccessSave:
                        //Write Log
                        LogsManagement.SaveNotifyLogs("ResponseStatus: SuccessSave");
                        Response.Write(GenerateReturnStream(Ref(gatewayType), ResponseStatus.Success));
                        break;
                    case ResponseStatus.VerifySave:
                        //Write Log
                        LogsManagement.SaveNotifyLogs("ResponseStatus: VerifySave");

                        result = VerifyPayment(gatewayType, "200");

                        if (result == "200")
                        {
                            if (!PaymentSave(objGateway.FormOfPayment, objGateway.FormOfPaymentSubtype, gatewayType))
                            {
                                result = "600";//fail save
                            }
                        }
                        else
                        {
                            result = "500";//fail verify
                        }
                        break;
                    case ResponseStatus.Verify:
                        //Write Log
                        LogsManagement.SaveNotifyLogs("ResponseStatus: Verify");
                        result = "Verify";
                        break;
                    case ResponseStatus.NotifySave:
                        //Write Log
                        LogsManagement.SaveNotifyLogs("ResponseStatus: NotifySave");
                        result = "NotifySave";
                        break;
                    case ResponseStatus.Cancel:
                        //Write Log
                        LogsManagement.SaveNotifyLogs("ResponseStatus: Cancel");
                        PaymentUpdate(objGateway, gatewayType);
                        Response.Write(GenerateReturnStream(Ref(gatewayType), ResponseStatus.Cancel));
                        result = "Cancel";
                        break;
                    case ResponseStatus.Error:
                        //Write Log
                        LogsManagement.SaveNotifyLogs("ResponseStatus: Error");
                        PaymentUpdate(objGateway, gatewayType);
                        Response.Write(GenerateReturnStream(Ref(gatewayType), ResponseStatus.Error));
                        result = "Error";
                        break;
                    default:
                        LogsManagement.SaveNotifyLogs("ResponseStatus: Idle");
                        Response.Write(GenerateReturnStream(Ref(gatewayType), objStatus));
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool PaymentSave(string formOfPaymentRCD, string formOfPaymentSubtypeRCD, PaymentGatewayFactory.GateWayType gatewayType)
        {
            bool result = false;
            ServiceClient objService = new ServiceClient();
            Agents objAgent = new Agents();
            Payments objPayment = new Payments();
            DataSet ds = new DataSet();
            Booking objBooking = new Booking();
            string xmlBooking = string.Empty;
            PaymentGateway objGateway = PaymentGatewayFactory.GetGatePaymentGateway(gatewayType);
            try
            {
                if (objGateway.AgencyCode != string.Empty)
                    objService.initializeWebService(objGateway.AgencyCode, ref objAgent);
                else
                    objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgent);

                LogsManagement.SaveNotifyLogs("URL : " + Request.Url.AbsoluteUri.ToString());
                objPayment.objService = (TikAeroXMLwebservice)objService.objService;

                ds = objPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, string.Empty, string.Empty, string.Empty,
                                                                objGateway.GetReferenceID(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0, false);

                if (ds.Tables.Count == 0)
                {
                    Response.Write("[STEP] : SAVE :" + objGateway.GetReferenceID() + ": Payment approvals not found");
                    result = false;
                }
                else if (ds.Tables[0].Rows.Count == 0)
                {
                    Response.Write("[STEP] : SAVE :" + objGateway.GetReferenceID() + ": Payment approvals rows not found");
                    result = false;
                }
                else if (ds.Tables[0].Rows[0]["transaction_description"] == null)
                {
                    Response.Write("[STEP] : SAVE :" + objGateway.GetReferenceID() + ": Transaction description not found");
                    result = false;
                }
                else if (ds.Tables[0].Rows[0]["payment_state_code"] == null)
                {
                    Response.Write("[STEP] : SAVE :" + objGateway.GetReferenceID() + ": Payment state code not found");
                    result = false;
                }
                else
                {
                    DataRow row = ds.Tables[0].Rows[0];

                    string sEngine = Convert.ToString(row["transaction_description"]).Split(',')[1];
                    string sPaymentStateCode = Convert.ToString(row["payment_state_code"]);
                    string responseCode = Convert.ToString(row["response_code"]);
                    objGateway.PayRef = Convert.ToString(row["transaction_reference"]);
                    ConfirmStatus objconfirm = objGateway.PaymentConfirm();
                    if (objconfirm == ConfirmStatus.FailConfirm)
                    {
                        Response.Write("[STEP] : SAVE :" + objGateway.GetReferenceID() + ": Payment cann't comfirm :" + objGateway.ResponseStream);
                        result = false;
                    }
                    else
                    {
                        if (ReplyStream == string.Empty)
                            ReplyStream = objGateway.ResponseStream;

                        ReplyStream = ReplyStream + ":" + objconfirm.ToString();
                        BookingID = new Guid(Convert.ToString(row["transaction_description"]).Split(',')[0]);

                        if (sEngine == "COB") { result = true; }
                        else if (sPaymentStateCode == string.Empty || sPaymentStateCode.ToUpper() == "VERIFY")
                        {

                            xmlBooking = objService.GetItinerary(BookingID.ToString(), string.Empty, string.Empty, string.Empty);
                            SavePayment(ds, objGateway.GetReferenceID(), xmlBooking, formOfPaymentRCD, formOfPaymentSubtypeRCD, ref objPayment);
                            objBooking = objPayment.SavePaymentRedirect(BookingID, objPayment);
                        }

                        if (objBooking.BookingHeader != null || sPaymentStateCode.ToUpper() == "PAID")
                        {
                            result = true;
                            objBooking.objService = (TikAeroXMLwebservice)objService.objService;
                            objBooking.BookingRead(BookingID);
                            if(objBooking.BookingHeader != null)
                            {

                            }
                            //update PaymentState to PAID and SuccessCode to SUCCESS
                            objGateway.PaymentState = "PAID";
                            if (string.IsNullOrEmpty(objGateway.SuccessCode))
                            {
                                objGateway.SuccessCode = "APPROVED";
                            }
                            if (string.IsNullOrEmpty(responseCode))
                            {
                                objPayment.objService.UpdatePaymentApproval(Convert.ToString(row["approval_code"]),
                                                                           objGateway.GetReferenceID(),
                                                                           objGateway.PayRef,
                                                                           Convert.ToString(row["transaction_description"]),
                                                                           "", objGateway.AvsResponse,
                                                                           "", objGateway.CvvResponse,
                                                                           objGateway.ErrorCode, objGateway.ErrorResponse,
                                                                           objGateway.PaymentState,
                                                                           objGateway.SuccessCode,
                                                                           "",
                                                                           objGateway.Holder,
                                                                           Convert.ToString(row["payment_approval_id"]),
                                                                           objGateway.PaymentRequestStreamText,
                                                                           ReplyStream,
                                                                           objGateway.CardNo,
                                                                           objGateway.CardType);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("[STEP] : SAVE :" + objGateway.GetReferenceID() + ": " + BookingID + "<br/>" + ex.Message);
                LogsManagement.SaveErrorLogs(ex, "[STEP] : SAVE :" + objGateway.GetReferenceID() + ": " +
                    BookingID + Environment.NewLine + ex.StackTrace + Environment.NewLine + xmlBooking);
                //Logger.Instance(Logger.LogType.Mail).WriteLog(ex, BookingID + Environment.NewLine + xmlBooking);
            }
            finally
            {
                //Dispose Object
                objService = null;
                objAgent = null;
                objPayment = null;
                ds.Dispose();
                objBooking = null;
            }
            return result;
        }
        public void PaymentUpdate(PaymentGateway gateway, PaymentGatewayFactory.GateWayType gatewayType)
        {
            ServiceClient objService = new ServiceClient();
            Agents objAgent = new Agents();
            Payments objPayment = new Payments();
            DataSet ds = new DataSet();
            string referenceId = gateway.PaymentResponseReference;
            try
            {

                objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgent);
                objPayment.objService = (TikAeroXMLwebservice)objService.objService;

                ds = objPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, string.Empty, string.Empty, string.Empty,
                                                                referenceId, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0, false);

                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow row = ds.Tables[0].Rows[0];
                        string transaction_description = Convert.ToString(row["transaction_description"]);
                        string[] transaction = transaction_description.Split(',');
                        string[] responseStream = gateway.ResponseStream.Split('|');
                        string sEngine = transaction[1];
                        string approval_code = Convert.ToString(row["approval_code"]);
                        string sPaymentStateCode = Convert.ToString(row["payment_state_code"]);
                        string payment_approval_id = Convert.ToString(row["payment_approval_id"]);
                        string ResponseStatus = string.Empty;
                        string ErrorCode = string.Empty;
                        BookingID = new Guid(transaction[0]);

                        gateway.PaymentState = sPaymentStateCode;
                        gateway.CardNo = Convert.ToString(row["card_number"]);
                        gateway.CardType = Convert.ToString(row["card_rcd"]);

                        switch (gatewayType)
                        {
                            case PaymentGatewayFactory.GateWayType.Credomatic:
                                string[] paymentForm = transaction_description.Split(';');
                                if (paymentForm.Length > 1)
                                {
                                    string[] form = paymentForm[1].Split(',');
                                    for (int i = 0; i < form.Length; i++)
                                    {
                                        string[] query = form[i].Split(':');
                                        if (query[0].Trim().ToLower() == "name_on_card")
                                            gateway.Holder = query[1];
                                    }
                                }

                                //response|responsetext|authcode|transactionid|avsresponse|cvvresponse|orderid|type|response_codeusername|time|amount|hash
                                for (int i = 0; i < responseStream.Length; i++)
                                {
                                    string[] query = responseStream[i].Split(':');
                                    if (query[0].Trim().ToLower() == "response_code")
                                        gateway.ErrorCode = query[1];
                                    else if (query[0].Trim().ToLower() == "responsetext")
                                        gateway.ErrorResponse = query[1];
                                    else if (query[0].Trim().ToLower() == "avsresponse")
                                        gateway.AvsResponse = query[1];
                                    else if (query[0].Trim().ToLower() == "cvvresponse")
                                        gateway.CvvResponse = query[1];
                                }
                                break;
                            case PaymentGatewayFactory.GateWayType.Ogone3D:
                                OGONE3DResponses response = new OGONE3DResponses();
                                ResponseStatus = response.STATUS;
                                gateway.ErrorCode = response.NCERROR;
                                break;
                            default:
                                break;
                        }

                        objPayment.objService.UpdatePaymentApproval(approval_code,
                                                                   referenceId,
                                                                   gateway.PayRef,
                                                                   transaction_description,
                                                                   string.Empty,
                                                                   gateway.AvsResponse,
                                                                   string.Empty,
                                                                   gateway.CvvResponse,
                                                                   gateway.ErrorCode,
                                                                   gateway.ErrorResponse,
                                                                   gateway.PaymentState,
                                                                   "DECLINED",
                                                                   ResponseStatus,
                                                                   string.Empty,
                                                                   payment_approval_id,
                                                                   string.Empty,
                                                                   gateway.ResponseStream,
                                                                   gateway.CardNo,
                                                                   gateway.CardType);

                    }
            }
            catch (Exception ex)
            {
                Response.Write("[STEP] : SAVE :" + referenceId + ": " + BookingID + "<br/>" + ex.Message);
                LogsManagement.SaveErrorLogs(ex, "[STEP] : SAVE :" + referenceId + ": " + BookingID + Environment.NewLine + ex.StackTrace);
                //Logger.Instance(Logger.LogType.Mail).WriteLog(ex, BookingID.ToString());
            }
            finally
            {
                //Dispose Object
                objService = null;
                objAgent = null;
                objPayment = null;
                ds.Dispose();
            }
        }

        public static void SavePayment(DataSet dsPaymentApprovals, string paymentRef, string xmlBooking,
            string formOfPaymentRCD, string formOfPaymentSubtypeRCD, ref Payments objPayment)
        {
            Payment p = new Payment();
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(xmlBooking);
            Guid userAccountID = Guid.Empty;
            string agencyCode = string.Empty;

            if (xDoc.SelectSingleNode("//BookingHeader/create_by") != null)
                userAccountID = new Guid(xDoc.SelectSingleNode("//BookingHeader/create_by").InnerText);
            if (xDoc.SelectSingleNode("//BookingHeader/agency_code") != null)
                agencyCode = xDoc.SelectSingleNode("//BookingHeader/agency_code").InnerText;

            Guid booking_id = Guid.Empty;
            if (dsPaymentApprovals.Tables.Count > 0)
            {
                if (dsPaymentApprovals.Tables[0].Rows.Count > 0)
                {
                    DataRow row = dsPaymentApprovals.Tables[0].Rows[0];
                    string[] transaction = Convert.ToString(row["transaction_description"]).Split(',');

                    p.booking_id = new Guid(transaction[0]);
                    p.approval_code = Convert.ToString(row["approval_code"]);
                    p.document_amount = Convert.ToDecimal(row["payment_amount"]);
                    p.document_number = Convert.ToString(row["card_number"]).Trim().Replace("*", "0");
                    p.name_on_card = Convert.ToString(row["response_text"]);
                    p.booking_payment_id = new Guid(Convert.ToString(row["booking_payment_id"]).Trim());
                    p.form_of_payment_rcd = !string.IsNullOrEmpty(formOfPaymentRCD) ? formOfPaymentRCD : transaction[2]; // "CC";
                    p.form_of_payment_subtype_rcd = !string.IsNullOrEmpty(formOfPaymentSubtypeRCD) ? formOfPaymentSubtypeRCD : Convert.ToString(row["card_rcd"]); // "BBL";
                }
            }
            p.transaction_reference = paymentRef;
            p.payment_reference = paymentRef;
            p.create_by = userAccountID;
            p.update_by = userAccountID;
            p.create_date_time = DateTime.Now;
            p.agency_code = agencyCode;

            objPayment.Add(p);
        }
        public static string GenerateReturnStream(string Ref, ResponseStatus objRes)
        {
            StringBuilder stbHtml = new StringBuilder();
            string linkURL = string.Empty;
            ServiceClient objService = new ServiceClient();
            Agents objAgent = new Agents();
            Payments objPayment = new Payments();
            DataSet ds = new DataSet();
            string bookingID = string.Empty;
            string engine = "B2C";
            string errorCode = string.Empty;
            string errorResponse = string.Empty;
            string responseCode = string.Empty;
            string responseText = string.Empty;
            string returnCode = string.Empty;
            try
            {
                if (objRes != ResponseStatus.Idle)
                {
                    if (Ref != string.Empty)
                    {
                        objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgent);
                        objPayment.objService = (TikAeroXMLwebservice)objService.objService;
                        ds = objPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, string.Empty, string.Empty, string.Empty,
                                                                        Ref, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0, false);
                        //LogsManagement.SaveNotifyLogs(ds.GetXml());

                        bool hasRows = false;
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                hasRows = true;
                                DataRow row = ds.Tables[0].Rows[0];
                                if (row.Table.Columns.Contains("transaction_description"))
                                {
                                    string[] transactions = Convert.ToString(row["transaction_description"]).Split(',');
                                    bookingID = transactions[0];
                                    engine = transactions[1];
                                }
                                if (row.Table.Columns.Contains("error_code"))
                                    errorCode = row["error_code"].ToString();
                                if (row.Table.Columns.Contains("error_response"))
                                    errorResponse = row["error_response"].ToString();
                                if (row.Table.Columns.Contains("response_code"))
                                    responseCode = row["response_code"].ToString();
                                if (row.Table.Columns.Contains("response_text"))
                                    responseText = row["response_text"].ToString();
                                if (row.Table.Columns.Contains("return_code"))
                                    returnCode = row["return_code"].ToString();

                            }
                        }

                        if (hasRows == false)
                        {
                            LogsManagement.SaveNotifyLogs(string.Format("Booking Id: {0} was not found.", Ref));
                        }
                    }

                    switch (engine)
                    {
                        case "B2B":
                            linkURL = ConfigurationManager.AppSettings["B2BRedirectURL"];
                            break;
                        case "B2C":
                            if (ConfigurationManager.AppSettings["B2CRedirectURL"] != null)
                            {
                                linkURL = ConfigurationManager.AppSettings["B2CRedirectURL"];
                            }
                            else
                            {
                                linkURL = HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Split('?')[0].Replace("paymentresponse.aspx", "Default.aspx");
                            }
                            break;
                        case "COB":
                            linkURL = ConfigurationManager.AppSettings["COBRedirectURL"];
                            break;
                    }

                    if (linkURL != string.Empty)
                    {
                        // query string parameter
                        //if (HttpContext.Current.Request.QueryString["AO"] != null && HttpContext.Current.Request.QueryString["AO"] != string.Empty)
                        //    linkURL = linkURL.Replace("{AO}", "ao=" + HttpContext.Current.Request.QueryString["AO"]);
                        //if (HttpContext.Current.Request.QueryString["langculture"] != null && HttpContext.Current.Request.QueryString["langculture"] != string.Empty)
                        //    linkURL = linkURL.Replace("{Lang}", "langculture=" + HttpContext.Current.Request.QueryString["langculture"]);
                        try
                        {
                            if (HttpContext.Current.Request.QueryString["qrb"] != null && HttpContext.Current.Request.QueryString["qrb"] != string.Empty)
                                linkURL += Encoding.UTF8.GetString(Convert.FromBase64String(HttpContext.Current.Request.QueryString["qrb"]));
                        }
                        catch (Exception)
                        {

                        }
                        //Submit information to gateway.    
                        string newline = Environment.NewLine;
                        stbHtml.Append("<form name='returnCard' method='post' action='" + linkURL + "'>" + newline);
                        stbHtml.Append("<input type='hidden' name='result' value='" + objRes.ToString() + "'>" + newline);
                        stbHtml.Append("<input type='hidden' name='bookingID' value='" + bookingID + "'>" + newline);
                        stbHtml.Append("<input type='hidden' name='Ref' value='" + Ref + "'>" + newline);
                        stbHtml.Append("<input type='hidden' name='errorCode' value='" + errorCode + "'>" + newline);
                        stbHtml.Append("<input type='hidden' name='errorResponse' value='" + errorResponse + "'>" + newline);
                        stbHtml.Append("<input type='hidden' name='responseCode' value='" + responseCode + "'>" + newline);
                        stbHtml.Append("<input type='hidden' name='responseText' value='" + responseText + "'>" + newline);
                        stbHtml.Append("<input type='hidden' name='returnCode' value='" + returnCode + "'>" + newline);
                        stbHtml.Append("</form>" + Environment.NewLine);
                        stbHtml.Append("<script>");
                        //if (objRes == ResponseStatus.Cancel || objRes == ResponseStatus.Error)
                        //    stbHtml.Append("alert('" + errorCode + "," + errorResponse + "');");
                        stbHtml.Append("document.returnCard.submit();");
                        stbHtml.Append("</script>");
                    }
                    else
                    {
                        stbHtml.Append("Link URL Retuen cann't found.");
                    }
                }
                else
                {
                    stbHtml.Append("Waiting for stream.");
                }
            }
            catch (Exception ex)
            {
                stbHtml.Append(ex.Message);
            }
            finally
            {
                //Dispose Object
                objService = null;
                objAgent = null;
                objPayment = null;
                linkURL = string.Empty;
                engine = string.Empty;
                ds.Dispose();
            }

            LogsManagement.SaveNotifyLogs(stbHtml.ToString());
            return stbHtml.ToString();
        }
        public static void FillBooking(DataSet dsPaymentApprovals, tikSystem.Web.Contact.PaymentGateway objGateway, ServiceClient objService)
        {
            BookingHeader bookingHeader = new BookingHeader();
            Itinerary itinerary = new Itinerary();
            Passengers passengers = new Passengers();
            Quotes quotes = new Quotes();
            Fees fees = new Fees();
            Mappings mappings = new Mappings();
            Services services = new Services();
            Remarks remarks = new Remarks();
            Payments payments = new Payments();
            Taxes taxes = new Taxes();
            Library objLi = new Library();

            string strGetBooking = string.Empty;
            string strBookingID = Convert.ToString(dsPaymentApprovals.Tables[0].Rows[0]["transaction_description"]).Split(',')[0];
            if (strBookingID != string.Empty)
            {
                strGetBooking = objService.GetBooking(new Guid(strBookingID));
                objLi.FillBooking(strGetBooking,
                                                      ref bookingHeader,
                                                      ref passengers,
                                                      ref itinerary,
                                                      ref mappings,
                                                      ref payments,
                                                      ref remarks,
                                                      ref taxes,
                                                      ref quotes,
                                                      ref fees,
                                                      ref services);
                objGateway.BookingHeader = bookingHeader;
                objGateway.Itinerary = itinerary;
                objGateway.Passengers = passengers;
                objGateway.Quotes = quotes;
                objGateway.Fees = fees;
                objGateway.Mappings = mappings;
                objGateway.Services = services;
                objGateway.Remarks = remarks;
                objGateway.Payments = payments;
                objGateway.Taxes = taxes;
            }
        }
    }
}