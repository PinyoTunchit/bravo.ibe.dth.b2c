﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using tikSystem.Web.Library;

namespace tikAeroB2C
{
    public partial class Maintain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                if (Request.Form.AllKeys.Length > 0)
                {
                    string strKey = Request.Form.ToString().Replace("%2f", "/");

                    if (IsTokenValid(strKey) == "reset")
                    {
                        ClearApplicationCache();
                    }
                }
            }
        }

        private void ClearApplicationCache()
        {
            List<string> keys = new List<string>();
            IDictionaryEnumerator enumerator = Cache.GetEnumerator();
             
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().IndexOf("System.") < 0)
                {
                    keys.Add(enumerator.Key.ToString());
                }
                
            }
            
            //Delete All cache
            for (int i = 0; i < keys.Count; i++)
            {
                Cache.Remove(keys[i]);
            }

            keys.Clear();
            enumerator.Reset();

            //Clear Application session.
            Application.Clear();
        }

        private string IsTokenValid(string strToken)
        {
            if (!string.IsNullOrEmpty(B2CSetting.AffixEncrypt) & !string.IsNullOrEmpty(B2CSetting.PrefixEncrypt))
            {
                string strDecryptValue = SecurityHelper.DecryptString(strToken, B2CSetting.PrefixEncrypt);
                string[] arrResult = strDecryptValue.Split('|');
                if (arrResult.Length > 0)
                {
                    if (arrResult[0].Equals(B2CSetting.AffixEncrypt) == true)
                    {
                        DateTime endTime = DateTime.Now;
                        TimeSpan span = endTime - Convert.ToDateTime(arrResult[1]);
                        if (span.TotalMinutes <= 5)
                        {
                            return arrResult[2]; 
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }
    }
}