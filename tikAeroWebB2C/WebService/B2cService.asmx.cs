﻿using CaptchaDLL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mail;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using tikAeroB2C.Classes;
using tikAeroB2C.Classes.ServiceMessage;
using tikSystem.Web.Library;
using tikSystem.Web.Library.SSRInventoryService;
using tikSystem.Web.PaymentGateway;

namespace tikAeroB2C.WebService
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]

    public class B2cService : Base.ServiceBase
    {
        public B2cService()
        {

        }
        #region Main function
        [WebMethod(EnableSession = true)]
        public void ClearSession()
        {
            try
            {
                base.BaseClearSession();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
            }
        }
        [WebMethod(EnableSession = true)]
        public void SendItineraryEmail()
        {
            try
            {
                base.BaseSendItineraryEmail();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
            }
        }
        [WebMethod(EnableSession = true)]
        public void SendItineraryEmailOptional(string strEmail)
        {
            try
            {
                base.BaseSendItineraryEmailOptional(strEmail);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
            }
        }
        [WebMethod(EnableSession = true)]
        public string LoadCob(bool bParameter, string bookingId)
        {
            try
            {
                ServiceResponse response = base.BaseLoadCob(bParameter, bookingId);
                if (response.Success == true)
                {
                    if (response.Code == "https")
                    {
                        return "{000}";
                    }
                    else
                    {
                        return response.HTML;
                    }
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetCobURL(bool bParameter, string bookingId)
        {
            try
            {
                ServiceResponse response = base.BaseGetCOBUrl(bParameter, bookingId);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Booking_id = " + bookingId);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string LoadManageBooking(string url)
        {
            try
            {
                ServiceResponse response = base.BaseLoadManageBooking(url);
                if (response.Success == true)
                {
                    if (response.Code == "https")
                    {
                        return "{000}";
                    }
                    else
                    {
                        return response.HTML;
                    }
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string LoadCms(string strUrl)
        {
            try
            {
                ServiceResponse response = base.BaseLoadCMS(strUrl);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strUrl);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string SetLanguage(string strLanguage)
        {
            try
            {
                ServiceResponse response = base.BasesSetLanguage(strLanguage);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strLanguage);
                throw ex;
            }

        }
        [WebMethod(EnableSession = true)]
        public string LoadSearchAvailability()
        {
            try
            {
                ServiceResponse response = base.BaseLoadSearchAvailability();
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string LoadHtml(string strFileName)
        {
            try
            {
                ServiceResponse response = base.BaseLoadHTML(strFileName);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strFileName);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string ShowEmailSender()
        {
            try
            {
                ServiceResponse response = base.BaseShowEmailSender();
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public short GetCurrentStep()
        {
            try
            {
                ServiceResponse response = base.BaseGetCurrentStep();
                return response.ToInt16;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetCurrentState()
        {
            try
            {
                ServiceResponse response = base.BaseGetCurrentState();
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string LoadHome()
        {
            try
            {
                ServiceResponse response = base.BaseLoadHome();
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        #endregion

        #region Availability
        [WebMethod(EnableSession = true)]
        [ScriptMethod]
        public string GetAvailability(string origin,
                                        string destination,
                                        string dateFrom,
                                        string dateTo,
                                        string iOneWay,
                                        string iFlightOnly,
                                        short iAdult,
                                        short iChild,
                                        short iInfant,
                                        string BoardingClass,
                                        string CurrencyCode,
                                        string strPromoCode,
                                        string SearchType,
                                        short iOther,
                                        string otherType,
                                        string strIpAddress)
        {

            try
            {

                ServiceResponse response = base.GetAvailability(origin,
                                                                destination,
                                                                CurrencyCode,
                                                                strIpAddress,
                                                                dateFrom,
                                                                dateTo,
                                                                iFlightOnly,
                                                                SearchType,
                                                                iOneWay,
                                                                iAdult,
                                                                iChild,
                                                                iInfant,
                                                                iOther,
                                                                otherType,
                                                                BoardingClass,
                                                                strPromoCode);

                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Helper objXsl = new Helper();
                objXsl.SendErrorEmail(ex, "origin : " + origin +
                                            "<br/>destination : " + destination +
                                            "<br/>Date From : " + dateFrom +
                                            "<br/>Date To : " + dateTo +
                                            "<br/>Oneway : " + iOneWay +
                                            "<br/>Only Flight : " + iFlightOnly +
                                            "<br/>Adult : " + iAdult +
                                            "<br/>Child : " + iChild +
                                            "<br/>Infant : " + iInfant +
                                            "<br/>Boarding Class : " + BoardingClass +
                                            "<br/>Currency Code : " + CurrencyCode +
                                            "<br/>Promocode : " + strPromoCode +
                                            "<br/>Search Type : " + SearchType +
                                            "<br/>Other Pax : " + iOther +
                                            "<br/>Other Type : " + otherType +
                                            "<br/>IpAddress : " + strIpAddress);
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string AvailabilityGetSession()
        {
            try
            {
                ServiceResponse response = base.BaseAvailabilityGetSession();
                if (response.Success == true)
                {
                    if (response.Code == "http")
                    {
                        return "{001}";
                    }
                    else
                    {
                        return response.HTML;
                    }

                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                return "{004}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string SearchSingleFlight(string flightDate, bool OutWard)
        {
            try
            {
                ServiceResponse response = base.BaseSearchSingleFlight(flightDate, OutWard);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, flightDate + "<br/>" + OutWard);
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string SearchSingleFlightSameDay(string flightDate)
        {
            try
            {
                ServiceResponse response = base.BaseSearchSingleFlightSameDay(flightDate);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, flightDate);
                return "{004}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddFlight(string strOutward, string strReturn, string strIpAddress)
        {
            Helper objHelper = new Helper();

            try
            {
                if (B2CSession.Variable != null)
                {
                    Flights objFlights = new Flights();
                    short adult = 0;
                    short child = 0;
                    short infant = 0;
                    short other = 0;
                    string otherType = string.Empty;
                    string currencyRcd = string.Empty;

                    if (strOutward.Length > 0)
                    {
                        InsertFlight(strOutward,
                                     ref objFlights,
                                     ref currencyRcd,
                                     ref adult,
                                     ref child,
                                     ref infant,
                                     ref other,
                                     ref otherType);
                    }
                    if (strReturn.Length > 0)
                    {
                        InsertFlight(strReturn,
                                     ref objFlights,
                                     ref currencyRcd,
                                     ref adult,
                                     ref child,
                                     ref infant,
                                     ref other,
                                     ref otherType);
                    }

                    List<string> lFare = (List<string>)Session["FareAllID"];
                    string validFare = "0";
                    foreach (Flight obj in objFlights)
                    {
                        bool found = false;
                        foreach (string str in lFare)
                        {
                            if (str.ToString().ToLower() == ("{" + obj.flight_id.ToString() + "}:{" + obj.fare_id.ToString() + "}:" + currencyRcd).ToLower())
                            {
                                found = true;
                                break;
                            }
                        }

                        if (adult != B2CSession.Variable.Adult || child != B2CSession.Variable.Child || infant != B2CSession.Variable.Infant || other != B2CSession.Variable.Other)
                            found = false;

                        if (found)
                            validFare += "0";
                        else
                            validFare += "1";
                    }

                    ServiceResponse response = new ServiceResponse();
                    if (Convert.ToInt16(validFare) == 0)
                        response = base.AddFlight(objFlights,
                                                            strIpAddress,
                                                            adult,
                                                            child,
                                                            infant,
                                                            other,
                                                            otherType,
                                                            currencyRcd);
                    else
                    {
                        response.Success = false;
                        response.Code = "202";
                    }
                    if (response.Success == true)
                    {
                        if (response.Code == "https")
                        {
                            return "{000}";
                        }
                        else
                        {
                            return "0{!}" + response.HTML;
                        }
                    }
                    else
                    {
                        if (response.Code == "101")
                        {
                            return "101{!}" + response.HTML;
                        }
                        else if (response.Code == "201")
                        {
                            return response.Code;
                        }
                        else
                        {
                            return response.Code + "{!}" + response.Message;
                        }
                    }
                }
                else
                {
                    return "{201}";
                }
            }
            catch (Exception ex)
            {
                objHelper.SendErrorEmail(ex, "Outward<br/>" + strOutward.Replace("<", "&lt;").Replace(">", "&gt;") +
                                             "<br/>Return<br/>" + strReturn.Replace("<", "&lt;").Replace(">", "&gt;"));

                //Select flight failed please try again.
                return "{204}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetLowFareFinderBeforeAvailability(string strFromAirport,
                                                        string strToAirport,
                                                        string strFlightDate,
                                                        string strReturnDate,
                                                        string iOneWay,
                                                        short iAdult,
                                                        short iChild,
                                                        short iInfant,
                                                        string BoardingClass,
                                                        string CurrencyCode,
                                                        string strPromoCode,
                                                        string SearchType,
                                                        short iOther,
                                                        string otherType,
                                                        string strIpAddress)
        {

            Library objLi = new Library();

            try
            {
                ServiceResponse response = base.GetLowFareFinderBeforeAvailability(strFromAirport,
                                                                                    strToAirport,
                                                                                    CurrencyCode,
                                                                                    strIpAddress,
                                                                                    strFlightDate,
                                                                                    strReturnDate,
                                                                                    "0",
                                                                                    SearchType,
                                                                                    iOneWay,
                                                                                    iAdult,
                                                                                    iChild,
                                                                                    iInfant,
                                                                                    iOther,
                                                                                    otherType,
                                                                                    BoardingClass,
                                                                                    strPromoCode);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "origin : " + strFromAirport +
                                        "<br/>destination : " + strToAirport +
                                        "<br/>flight date : " + strFlightDate +
                                        "<br/>return date : " + strReturnDate +
                                        "<br/>Oneway : " + iOneWay +
                                        "<br/>Adult : " + iAdult.ToString() +
                                        "<br/>Child : " + iChild.ToString() +
                                        "<br/>infant : " + iInfant.ToString() +
                                        "<br/>boarding class : " + BoardingClass +
                                        "<br/>currency : " + CurrencyCode +
                                        "<br/>promocode : " + strPromoCode +
                                        "<br/>search type : " + SearchType +
                                        "<br/>Other Pax Type : " + iOther.ToString() +
                                        "<br/>Other : " + otherType);

                return "{103}"; //Low Fare finder Exception error.
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetLowFareFinder(string strFromAirport, string strToAirport, string strFlightDate, string strReturnDate)
        {
            try
            {
                ServiceResponse response = base.BaseGetLowFareFinder(strFromAirport, strToAirport, strFlightDate, strReturnDate);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strFromAirport + "<br/>" + strToAirport + "<br/>" + strFlightDate + "<br/>" + strReturnDate);

                return "{103}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetAvailabilityFromLowFareFinder(string routeInformation,
                                                        string dateOutward,
                                                        string dateReturn,
                                                        bool bRouteDayRange,
                                                        string outwardFare,
                                                        string returnFare,
                                                        string strOutFlight,
                                                        string strRetFlight)
        {
            try
            {
                ServiceResponse response = base.BaseGetAvailabilityFromLowFareFinder(routeInformation,
                                                                                    dateOutward,
                                                                                    dateReturn,
                                                                                    bRouteDayRange,
                                                                                    outwardFare,
                                                                                    returnFare,
                                                                                    strOutFlight,
                                                                                    strRetFlight);
                if (response.Success == true)
                {
                    return "0{}" + response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Route Information  : " + routeInformation +
                                         "<br/>Date From : " + dateOutward +
                                         "<br/>Date To : " + dateReturn +
                                         "<br/>Date Range : " + bRouteDayRange.ToString() +
                                         "<br/>Outward Fare : " + outwardFare +
                                         "<br/>Return Fare : " + returnFare);

                return "{103}"; // LLF exception.
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetFlightTime(string originRcd, string destinationRcd, string strDate)
        {

            try
            {
                ServiceResponse response = base.BaseGetFlightTime(originRcd, destinationRcd, strDate);

                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Helper objXsl = new Helper();
                objXsl.SendErrorEmail(ex, originRcd + "<br/>" + destinationRcd + "<br/>" + strDate);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod]
        public string GetFullQuoteSummary(string strFlightXml)
        {
            Helper objXsl = new Helper();
            StringBuilder stb = new StringBuilder();

            try
            {
                if (strFlightXml.Length == 0)
                {
                    //Flight xml not found.
                    objXsl.SendErrorEmail("Flight Xml<br/>" + strFlightXml.Replace("<", "&lt;").Replace(">", "&gt;"),
                                            "Flight xml not found.",
                                            "Code : 100",
                                            "B2cService.asmx",
                                            "GetFullQuoteSummary");

                    return "{100}";
                }

                else
                {
                    //Fill Flight object.
                    Flights objFlights = new Flights(SecurityHelper.GenerateSessionlessToken());

                    FillFlight(strFlightXml, objFlights);
                    ServiceResponse response = base.GetFullQuoteSummary(objFlights);

                    if (response.Success == true)
                    {
                        return response.HTML;
                    }
                    else
                    {
                        return response.Code;
                    }
                }
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Flight Xml<br/>" + strFlightXml.Replace("<", "&lt;").Replace(">", "&gt;") +
                                          "<br/>Quote Xml<br/>" + stb.ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod]
        public string GetQuoteSummary(string strFlightXml, string strFlightType)
        {
            Helper objXsl = new Helper();
            StringBuilder stb = new StringBuilder();
            System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

            try
            {
                if (string.IsNullOrEmpty(strFlightXml))
                {
                    return "{100}";
                }
                else if (string.IsNullOrEmpty(strFlightType))
                {
                    return "{101}";
                }
                else
                {
                    //Fill Flight object.
                    Flights objFlights = new Flights(SecurityHelper.GenerateSessionlessToken());
                    FillFlight(strFlightXml, objFlights);
                    ServiceResponse response = base.GetQuoteSummary(objFlights, strFlightType);
                    if (response.Success == true)
                    {
                        return response.HTML;
                    }
                    else
                    {
                        return "{" + response.Code + "}";
                    }
                }
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Flight Xml<br/>" + strFlightXml.Replace("<", "&lt;").Replace(">", "&gt;") +
                                          "<br/>Quote Xml<br/>" + stb.ToString().Replace("<", "&lt;").Replace(">", "&gt;") +
                                          "<br/>Flight Type<br/>" + strFlightType);
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod]
        public string GetSessionQuoteSummary()
        {
            ServiceResponse response = base.BaseGetSessionQuoteSummary();
            if (response.Success == true)
            {
                return response.HTML;
            }
            else
            {
                return "{" + response.Code + "}";
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod]
        public string ShowAvailabilityPopup()
        {
            try
            {
                ServiceResponse response = base.BaseShowAvailabilityPopup();
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetLowFareFinderMonth(string strFromAirport,
                                            string strToAirport,
                                            string departMonth,
                                            string returnMonth,
                                            string iOneWay,
                                            short iAdult,
                                            short iChild,
                                            short iInfant,
                                            string BoardingClass,
                                            string CurrencyCode,
                                            string strPromoCode,
                                            string SearchType,
                                            short iOther,
                                            string otherType,
                                            string strIpAddress,
                                            string strCurrentDate)
        {

            try
            {
                ServiceResponse response = base.BaseGetLowFareFinderMonth(strFromAirport,
                                                                        strToAirport,
                                                                        departMonth,
                                                                        returnMonth,
                                                                        iOneWay,
                                                                        iAdult,
                                                                        iChild,
                                                                        iInfant,
                                                                        BoardingClass,
                                                                        CurrencyCode,
                                                                        strPromoCode,
                                                                        SearchType,
                                                                        iOther,
                                                                        otherType,
                                                                        strIpAddress,
                                                                        strCurrentDate);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "origin : " + strFromAirport +
                                        "<br/>destination : " + strToAirport +
                                        "<br/>flight date : " + departMonth +
                                        "<br/>return date : " + returnMonth +
                                        "<br/>Oneway : " + iOneWay +
                                        "<br/>Adult : " + iAdult.ToString() +
                                        "<br/>Child : " + iChild.ToString() +
                                        "<br/>infant : " + iInfant.ToString() +
                                        "<br/>boarding class : " + BoardingClass +
                                        "<br/>currency : " + CurrencyCode +
                                        "<br/>promocode : " + strPromoCode +
                                        "<br/>search type : " + SearchType +
                                        "<br/>Other Pax Type : " + iOther.ToString() +
                                        "<br/>Other : " + otherType);

                return "{103}"; //Low Fare finder Exception error.
            }
        }
        [WebMethod(EnableSession = true)]
        public string SearchLowFareSingleMonth(string strYearMonth, bool OutWard)
        {

            try
            {
                ServiceResponse response = base.BaseSearchLowFareSingleMonth(strYearMonth, OutWard);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strYearMonth + "<br/>" + OutWard);
                return "{004}";
            }
        }
        #endregion

        #region Flight Summary
        [WebMethod(EnableSession = true)]
        public string ShowFlightSummary()
        {
            try
            {
                ServiceResponse response = base.BaseShowFlightSummary();
                if (response.Success == true)
                {
                    if (response.Code == "https")
                    {
                        return "{000}";//Redirect to https:
                    }
                    else if (response.Code == "http")
                    {
                        return "{001}";//Redirect to http:
                    }
                    else
                    {
                        return response.HTML;
                    }
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPassengerDetail()
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                ServiceResponse response = base.BaseGetPassengerDetail();
                if (response.Success == true)
                {
                    if (response.Code == "https")
                    {
                        return "{000}";//Redirect to https:
                    }
                    else if (response.Code == "http")
                    {
                        return "{001}";//Redirect to http:
                    }
                    else
                    {
                        return response.HTML;
                    }
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        #endregion

        #region Passenger Detail
        [WebMethod(EnableSession = true)]
        public string GetClient(string clientNumber, string strLastName, bool bShowAll)
        {
            string strResult = string.Empty;
            try
            {
                ServiceResponse response = base.BaseGetClient(clientNumber, strLastName, bShowAll);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "<br/>" + clientNumber + "<br/>" + strLastName + "<br/>" + bShowAll + "<br/>Result<br/>" + strResult);
                return "{004}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetClientProfile(string clientNumber)
        {
            try
            {
                ServiceResponse response = base.BaseGetClientProfile(clientNumber);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, clientNumber);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetDocumentType(string language)
        {
            try
            {
                ServiceResponse response = base.BaseGetDocumentType(language);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, language);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPassengerTitles(string language)
        {
            try
            {
                ServiceResponse response = base.BaseGetPassengerTitles(language);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, language);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetCountry(string language)
        {
            try
            {
                ServiceResponse response = base.BaseGetCountry(language);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, language);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public void ClearPassengerData()
        {
            try
            {
                base.BaseClearPassengerData();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string FillPassengerData(string xml, bool generateControl)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Passengers passengers = B2CSession.Passengers;
                Mappings mappings = B2CSession.Mappings;
                Remarks remarks = B2CSession.Remarks ?? new Remarks();
                Agents objAgents = B2CSession.Agents;

                Routes destination = CacheHelper.CacheDestination();
                Route selectdRoute = destination.OfType<Route>().Where(r => r.origin_rcd == objUv.OriginRcd && r.destination_rcd == objUv.DestinationRcd).Single();
                bool require_date_of_birth_flag = selectdRoute != null ? selectdRoute.require_date_of_birth_flag : true;

                if (objUv == null ||
                    objAgents == null ||
                    objAgents.Count == 0 ||
                    bookingHeader == null ||
                    passengers == null ||
                    passengers.Count == 0 ||
                    mappings == null ||
                    mappings.Count == 0 ||
                    objHp.SessionTimeout() == true)
                {
                    return "{002}";
                }
                else
                {
                    using (StringReader srd = new StringReader(xml))
                    {
                        XPathDocument xmlDoc = new XPathDocument(srd);
                        XPathNavigator nv = xmlDoc.CreateNavigator();
                        if (ValidContactInput(nv) == false || ValidPassengerInput(nv) == false)
                        {
                            //Invalid Passenger Information.
                            LogsManagement.SaveNotifyLogs(string.Format("Error 405 FillPassengerData ValidContactInput ValidPassengerInput: {0}", xml));
                            return "{405}";
                        }
                        else
                        {
                            //Fill information from xml to object.
                            //=========================================================================

                            //Update seat information from temp.
                            UpdateSeatInfoFromTemp(mappings);

                            //Fill contact detail information.
                            FillContactInfo(bookingHeader, nv);

                            //Fill Passenger information.
                            FillPassengerDetail(passengers, mappings, nv);

                            //Fill Remark information.
                            FillRemark(bookingHeader.booking_id,
                                        passengers[0].client_profile_id,
                                        bookingHeader.agency_code,
                                        nv);

                            bool bGroupBooking = false;
                            //Validate Currency Code.
                            if (string.IsNullOrEmpty(bookingHeader.group_name) == false)
                            {
                                bGroupBooking = true;
                            }
                            //validate passenger and mapping information.
                            if (passengers.Valid(bGroupBooking) == false)
                            {
                                //Invalid Passenger Information.
                                LogsManagement.SaveNotifyLogs(string.Format("Error 405 FillPassengerData passengers.Valid(bGroupBooking): {0}", xml));
                                return "{405}";
                            }
                            if (require_date_of_birth_flag && passengers.ValidDateOfBirth(mappings[0].departure_date) == false)
                            {
                                //Invalid date of birth.
                                return "{404}";
                            }
                            else if (mappings.Valid(bGroupBooking, bookingHeader.currency_rcd) == false)
                            {
                                //Invalid Passenger Information.
                                LogsManagement.SaveNotifyLogs(string.Format("Error 405 FillPassengerData mappings.Valid(bGroupBooking, bookingHeader.currency_rcd): {0}", xml));
                                return "{405}";
                            }
                            else if (require_date_of_birth_flag && mappings.ValidDateOfBirth(mappings[0].departure_date) == false)
                            {
                                //Invalid date of birth.
                                return "{404}";
                            }
                            //Generate payment page.
                            if (generateControl == true)
                            {
                                ServiceResponse response = base.GetPaymentPage();
                                if (response.Success == true)
                                {
                                    if (response.Code == "https")
                                    {
                                        return "{000}";
                                    }
                                    else
                                    {
                                        return response.HTML;
                                    }

                                }
                                else
                                {
                                    return "{" + response.Code + "}";
                                }
                            }
                            else
                            {
                                return string.Empty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, "Passenger xml<br/>" + xml.Replace("<", "&lt;").Replace(">", "&gt;") +
                                         "<br/>GenerateControl<br/>" + generateControl.ToString());
                return "{004}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string FillValidatePassengerData(string xml)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Passengers passengers = B2CSession.Passengers;
                Mappings mappings = B2CSession.Mappings;
                Agents objAgents = B2CSession.Agents;
                StringBuilder stbErr = new StringBuilder();
                ServiceResponse response = new ServiceResponse();

                Routes destination = CacheHelper.CacheDestination();
                Route selectdRoute = destination.OfType<Route>().Where(r => r.origin_rcd == objUv.OriginRcd && r.destination_rcd == objUv.DestinationRcd).Single();
                bool require_date_of_birth_flag = selectdRoute != null ? selectdRoute.require_date_of_birth_flag : true;

                if (objUv == null ||
            objAgents == null ||
            objAgents.Count == 0 ||
            bookingHeader == null ||
            passengers == null ||
            passengers.Count == 0 ||
            mappings == null ||
            mappings.Count == 0 ||
            objHp.SessionTimeout() == true)
                {
                    return "{002}";
                }
                else
                {
                    XmlDocument document = new XmlDocument();
                    document.LoadXml(xml);
                    XPathNavigator nv = document.CreateNavigator();
                    if (ValidContactInput(nv) == false ||
                        ValidPassengerInput(nv) == false)
                    {
                        //Invalid Passenger Information.
                        LogsManagement.SaveNotifyLogs(string.Format("Error 405 ValidContactInput, ValidPassengerInput: {0}", xml));
                        return "{405}";
                    }
                    else
                    {
                        //Fill information from xml to object.
                        //=========================================================================

                        Clients ClientProfile = GetProfileInformation(nv);
                        ClientProfile.objService = B2CSession.AgentService;
                        if (ClientProfile == null ||
                            ClientProfile.Count == 0 ||
                            ClientProfile.FillClientInformation(bookingHeader.booking_id) == true)
                        {
                            if (ClientProfile.Count > 0)
                            {
                                nv.SelectSingleNode("booking/contact/client_number").SetValue(ClientProfile[0].client_number.ToString());
                                nv.SelectSingleNode("booking/contact/client_profile_id").SetValue(ClientProfile[0].client_profile_id.ToString());
                            }
                            response = base.FillValidatePassengerData(ClientProfile);

                        }

                        else
                        {
                            //Generate payment page.
                            response = GetPaymentPage();
                        }

                        //Update seat information from temp.
                        base.UpdateSeatInfoFromTemp(mappings);

                        //Fill contact detail information.
                        FillContactInfo(bookingHeader, nv);

                        //Fill Passenger information.
                        FillPassengerDetail(passengers, mappings, nv);

                        //Fill Remark information.
                        FillRemark(bookingHeader.booking_id,
                                   passengers[0].client_profile_id,
                                   bookingHeader.agency_code,
                                   nv);

                        bool bGroupBooking = false;
                        //Validate Currency Code.
                        if (string.IsNullOrEmpty(bookingHeader.group_name) == false)
                        {
                            bGroupBooking = true;
                        }
                        //validate passenger and mapping information.
                        if (passengers.Valid(bGroupBooking) == false)
                        {
                            //Invalid Passenger Information.
                            LogsManagement.SaveNotifyLogs(string.Format("Error 405 valid(bGroupBooking): {0}", xml));
                            return "{405}";
                        }
                        if (require_date_of_birth_flag && passengers.ValidDateOfBirth(mappings[0].departure_date) == false)
                        {
                            //Invalid date of birth.
                            return "{404}";
                        }
                        else if (mappings.Valid(bGroupBooking, bookingHeader.currency_rcd) == false)
                        {
                            //Invalid Passenger Information.
                            LogsManagement.SaveNotifyLogs(string.Format("Error 405 mappings.Valid(bGroupBooking, bookingHeader.currency_rcd): {0}", xml));
                            return "{405}";
                        }
                        else if (require_date_of_birth_flag && mappings.ValidDateOfBirth(mappings[0].departure_date) == false)
                        {
                            //Invalid date of birth.
                            return "{404}";
                        }
                        // response = base.FillValidatePassengerData(ClientProfile);
                        if (response.Success == true)
                        {
                            return response.HTML;
                        }
                        else
                        {
                            if (response.Code == "100")
                            {
                                return response.HTML;
                            }
                            else
                            {
                                return "{" + response.Code + "}";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, xml.Replace("<", "&lt;").Replace(">", "&gt;"));
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string ValidateAllClient(string strToValidXml)
        {
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                XPathDocument xmlDoc = new XPathDocument(new StringReader(strToValidXml));
                XPathNavigator nv = xmlDoc.CreateNavigator();

                Clients clientProfile = GetProfileInformation(nv);
                clientProfile.objService = B2CSession.AgentService;

                if (clientProfile == null ||
                    clientProfile.Count == 0 ||
                    clientProfile.FillClientInformation(bookingHeader.booking_id) == true)
                {

                    ServiceResponse response = base.ValidateAllClient(clientProfile);
                    if (response.Success == true)
                    {
                        return string.Empty;
                    }
                    else
                    {
                        return response.HTML;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strToValidXml.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }
        private string ValidateSpecialService(string ssrXml, Services sv)
        {
            try
            {
                //Mappings mappings = B2CSession.Mappings;
                Itinerary it = B2CSession.Itinerary;
                string special_service_group_Inventory_RCD = string.Empty;
                string boarding_class_RCD = string.Empty;
                string result = string.Empty;
                string flightID = string.Empty;
                int countSSR = 0;
                int bCapacity = 0;
                int booked_Count = 0;

                #region Get userCredential
                string agencyCode = B2CSetting.DefaultAgencyCode;
                string agencyPassword = B2CSetting.DefaultAgencyPassword;
                #endregion

                #region Get Intomation

                if (it != null && it.Count > 0)
                {
                    //Flight
                    for (int i = 0; i < it.Count; i++)
                    {
                        flightID = Convert.ToString(it[i].flight_id);
                        Guid bookingSegmentID = it[i].booking_segment_id;
                        boarding_class_RCD = it[i].boarding_class_rcd;

                        //Xml
                        if (!string.IsNullOrEmpty(ssrXml))
                        {
                            if (ssrXml == "<booking></booking>") return "";
                            XmlDocument XmlDoc = new XmlDocument();
                            XmlNodeList nl;
                            XmlDoc.LoadXml(ssrXml);
                            nl = XmlDoc.SelectNodes("booking/service");
                            string strBookingSegmentID = string.Empty;

                            foreach (XmlNode n in nl)
                            {
                                strBookingSegmentID = Convert.ToString(n["booking_segment_id"].InnerText);
                                if (Convert.ToString(bookingSegmentID) == strBookingSegmentID)
                                {
                                    countSSR += 1;
                                    special_service_group_Inventory_RCD = n["special_service_group_inventory_rcd"].InnerText;
                                }
                            }
                        }

                        //Object
                        if (sv != null && sv.Count > 0)
                        {
                            foreach (Service s in sv)
                            {
                                if (s.number_of_units > 0 && (Convert.ToString(bookingSegmentID) == Convert.ToString(s.booking_segment_id)))
                                {
                                    countSSR += 1;
                                    special_service_group_Inventory_RCD = s.special_service_group_inventory_rcd;
                                }
                            }
                        }

                        //SSRInventoryService
                        if (countSSR > 0)
                        {
                            Services objService = new Services();
                            SSRInventoryResponse ssrResponse = new SSRInventoryResponse();
                            if (!string.IsNullOrEmpty(agencyCode) && (!string.IsNullOrEmpty(agencyPassword) && (!string.IsNullOrEmpty(flightID))))
                            {
                                ssrResponse = objService.GetSSRInventoryService(agencyCode, agencyPassword, flightID);
                                List<SSRInventory> inventories = ssrResponse.SSRInventories.Where(s => s.Special_Service_Group_Inventory_Rcd == special_service_group_Inventory_RCD && s.Boarding_Class_Rcd == boarding_class_RCD).ToList<SSRInventory>();
                                //origin - destination       
                                if (inventories.Count() > 0)
                                {
                                    for (int j = 0; j < inventories.Count(); j++)
                                    {
                                        SSRInventory inventory = inventories[j];
                                        bCapacity = inventory.Bookable_Capacity;
                                        booked_Count += inventory.Booked_Count;
                                    }
                                    bCapacity = bCapacity - booked_Count;

                                    if (bCapacity != 0 && countSSR > bCapacity)
                                    {
                                        result = "{003_2}"; //Fully 
                                    }
                                    else if (bCapacity == 0)
                                    {
                                        result = "{003_1}"; //Fully 
                                    }
                                }
                            }
                            else
                            {
                                result = "{003}";
                            }

                            if (result.Length > 0)
                            {
                                return result;
                            }
                        }
                        countSSR = 0;
                        bCapacity = 0;
                        booked_Count = 0;
                    }
                }
                #endregion
                return result;

            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, ssrXml.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string FillSpecialService(string ssrXml, bool renderControl)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                //SSRInventoryService 
                if (B2CSetting.SSRInventoryService)
                {
                    string ssrResult = string.Empty;
                    string resultValidate = string.Empty;
                    Services s = null;
                    resultValidate = ValidateSpecialService(ssrXml, s);
                    if (!string.IsNullOrEmpty(resultValidate))
                    {
                        if (resultValidate == "{003}")
                        { ssrResult = "{003}"; }
                        else if (resultValidate == "{003_1}")
                        { ssrResult = "{003_1}"; }
                        else if (resultValidate == "{003_2}")
                        { ssrResult = "{003_2}"; }
                        return ssrResult;
                    }
                }

                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Taxes taxes = B2CSession.Taxes;
                Agents objAgents = B2CSession.Agents;

                Library objLi = new Library();

                if (bookingHeader == null ||
                    (itinerary == null || itinerary.Count == 0) ||
                    (passengers == null || passengers.Count == 0) ||
                    (quotes == null || quotes.Count == 0) ||
                    (mappings == null || mappings.Count == 0) ||
                    (objAgents == null || objAgents.Count == 0))
                {
                    //Session timeout.
                    return "{002}";
                }
                else
                {
                    using (StringReader sr = new StringReader(ssrXml))
                    {
                        XPathDocument xmlDoc = new XPathDocument(sr);
                        XPathNavigator nv = xmlDoc.CreateNavigator();

                        if (services == null)
                        { services = new Services(); }


                        #region Remove object collection
                        //Remove Service.
                        XPathNodeIterator nodes = nv.Select("booking/service");
                        if (nodes.Count > 0)
                        {
                            foreach (XPathNavigator n in nodes)
                            {
                                //Remove Existing service with the same fee code and passenger.
                                for (int i = 0; i < services.Count; i++)
                                {
                                    if (services[i].new_record == true &&
                                        services[i].passenger_id.Equals(XmlHelper.XpathValueNullToGUID(n, "passenger_id")))
                                    {
                                        if (fees != null && fees.Count > 0)
                                        {
                                            for (int j = 0; j < fees.Count; j++)
                                            {
                                                if (fees[j].passenger_segment_service_id.Equals(services[i].passenger_segment_service_id))
                                                {
                                                    //Remove Fee
                                                    fees.RemoveAt(j);
                                                    j--;
                                                }
                                            }
                                        }
                                        //Remove Service
                                        services.RemoveAt(i);
                                        i--;
                                    }
                                }
                            }
                        }
                        else
                        {
                            string ssrGroup = B2CSetting.SsrGroup;
                            string[] arr = !string.IsNullOrEmpty(ssrGroup) ? ssrGroup.Split(',') : new string[] { };
                            for (int i = services.Count - 1; i > -1; i--)
                            {
                                if (arr.Contains<string>(services[i].special_service_rcd))
                                {
                                    services.Remove(services[i]);
                                }
                            }

                            for (int j = fees.Count - 1; j > -1; j--)
                            {
                                if (arr.Contains<string>(fees[j].fee_rcd))
                                {
                                    fees.Remove(fees[j]);
                                }
                            }
                        }
                        #endregion

                        #region Fill Service object
                        //Fill Service information.
                        Service s;
                        foreach (XPathNavigator n in nv.Select("booking/service"))
                        {
                            if (XmlHelper.XpathValueNullToInt16(n, "number_of_units") > 0)
                            {
                                s = new Service();
                                s.passenger_segment_service_id = Guid.NewGuid();
                                s.fee_id = XmlHelper.XpathValueNullToEmpty(n, "fee_id");
                                s.passenger_id = XmlHelper.XpathValueNullToGUID(n, "passenger_id");
                                s.booking_segment_id = XmlHelper.XpathValueNullToGUID(n, "booking_segment_id");
                                s.origin_rcd = XmlHelper.XpathValueNullToEmpty(n, "origin_rcd");
                                s.destination_rcd = XmlHelper.XpathValueNullToEmpty(n, "destination_rcd");
                                s.special_service_rcd = XmlHelper.XpathValueNullToEmpty(n, "special_service_rcd");
                                s.service_text = XmlHelper.XpathValueNullToEmpty(n, "service_text");
                                s.number_of_units = XmlHelper.XpathValueNullToInt16(n, "number_of_units");
                                s.boarding_class_rcd = XmlHelper.XpathValueNullToEmpty(n, "boarding_class_rcd");
                                s.flight_id = XmlHelper.XpathValueNullToEmpty(n, "flight_id");
                                s.special_service_group_inventory_rcd = XmlHelper.XpathValueNullToEmpty(n, "special_service_group_inventory_rcd");
                                s.new_record = true;
                                services.Add(s, itinerary, XmlHelper.XpathValueNullToInt16(n, "service_on_request_flag"));
                                s = null;
                            }
                        }
                        #endregion

                        //Render control.
                        ServiceResponse response = base.FillSpecialService(renderControl,
                                                                            objAgents[0].agency_code,
                                                                            bookingHeader,
                                                                            itinerary,
                                                                            passengers,
                                                                            quotes,
                                                                            mappings,
                                                                            taxes,
                                                                            fees,
                                                                            services,
                                                                            remarks);

                        if (response.Success == true)
                        {
                            return response.HTML;
                        }
                        else
                        {
                            return "{" + response.Code + "}";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, ssrXml.Replace("<", "&lt;").Replace(">", "&gt;") + "<br/>RenderControl<br/>" + renderControl.ToString());
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string RemoveSpecialService(bool removeAll)
        {
            ServiceResponse response = base.BaseRemoveSpecialService(removeAll);
            return response.HTML;
        }
        [WebMethod(EnableSession = true)]
        public string ShowSpecialService(string strPassengerId)
        {
            try
            {
                ServiceResponse response = base.BaseShowSpecialService(strPassengerId);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Passenger Id" + strPassengerId.ToString());
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string ShowAdditionalFee()
        {
            try
            {
                ServiceResponse response = base.BaseShowAdditionalFee();
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Additional Fee");
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string FillAddtionalFee(List<Fee> fee_list, bool renderControl)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Taxes taxes = B2CSession.Taxes;
                Agents objAgents = B2CSession.Agents;

                Library objLi = new Library();

                if (bookingHeader == null ||
                    (itinerary == null || itinerary.Count == 0) ||
                    (passengers == null || passengers.Count == 0) ||
                    (quotes == null || quotes.Count == 0) ||
                    (mappings == null || mappings.Count == 0) ||
                    (objAgents == null || objAgents.Count == 0))
                {
                    //Session timeout.
                    return "{002}";
                }
                else
                {
                    fees.RemoveFeesByCode("KLIMA");
                    if (fee_list != null)
                    {
                        foreach (Fee fee in fee_list)
                        {
                            if (fee != null)
                            {
                                fee.booking_fee_id = Guid.NewGuid();
                                fee.booking_id = bookingHeader.booking_id;
                                fees.Add(fee);
                            }
                        }
                    }

                    ServiceResponse response = base.FillSpecialService(renderControl,
                                                                        objAgents[0].agency_code,
                                                                        bookingHeader,
                                                                        itinerary,
                                                                        passengers,
                                                                        quotes,
                                                                        mappings,
                                                                        taxes,
                                                                        fees,
                                                                        services,
                                                                        remarks);

                    if (response.Success == true)
                    {
                        return response.HTML;
                    }
                    else
                    {
                        return "{" + response.Code + "}";
                    }
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, "<br/>RenderControl<br/>" + renderControl.ToString());
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string CalculateBaggageFee(string strNumberOfUnit, string strBookingSegmentId, string strPassengerId, bool bReturn, bool bGenerateControl)
        {
            try
            {
                ServiceResponse response = base.BaseCalculateBaggageFee(Convert.ToDecimal(strNumberOfUnit),
                                                                           strBookingSegmentId,
                                                                           strPassengerId,
                                                                           bGenerateControl);
                if (response != null)
                {
                    if (response.Success == true)
                    {
                        return response.HTML;
                    }
                    else
                    {
                        return "{" + response.Code + "}";
                    }
                }
                else
                {
                    return string.Empty;
                }

            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Number of unit " + strNumberOfUnit + "<br/>" +
                                         "Booking Segment Id " + strBookingSegmentId + "<br/>" +
                                         "Passenger Id " + strPassengerId + "<br/>" +
                                         "bReturn" + bReturn.ToString() + "<br/>" +
                                         "bGenerateControl" + bGenerateControl.ToString());
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string ClearBaggageFee(string strSegmentId, string strPassengerId, bool bGenerateControl)
        {
            ServiceResponse response = base.BaseClearBaggageFee(strSegmentId, strPassengerId, bGenerateControl);
            if (response.Success == true)
            {
                return response.HTML;
            }
            else
            {
                return "{" + response.Code + "}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetBaggageFeeOptions(string passengerId)
        {
            try
            {
                if (string.IsNullOrEmpty(passengerId) == false)
                {
                    ServiceResponse response = base.GetBaggageFeeOptions(new Guid(passengerId));
                    if (response.Success == true)
                    {
                        return response.HTML;
                    }
                    else
                    {
                        return "{" + response.Code + "}";
                    }
                }
                else
                {
                    //Invalid require parameter.
                    return "{003}";
                }

            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Passenger ID " + passengerId);
                throw ex;
            }
        }
        #endregion

        #region Seat Map
        [WebMethod(EnableSession = true)]
        public string ShowSeatMap(string paxXml, string selectedFlightId, string originRcd, string destinationRcd, string boardingClass, string bookingClass)
        {
            try
            {
                Itinerary itinerary = B2CSession.Itinerary;
                Mappings mappings = B2CSession.Mappings;
                B2CVariable objUv = B2CSession.Variable;

                if (itinerary != null && itinerary.Count > 0 && mappings != null && mappings.Count > 0)
                {
                    objUv.CurrentStep = 11;
                    Library objLi = new Library();
                    FillPassengerData(paxXml, false);//Filled in passenger information
                    return objLi.GenerateControlString("UserControls/SeatMap.ascx",
                                                        selectedFlightId + "|" + originRcd + "|" + destinationRcd + "|" + boardingClass + "|" + bookingClass);
                }
                else
                {
                    //Important object not found in the session. Need to go back to home page.
                    return "{002}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Pax xml<br/>" + paxXml.Replace("<", "&lt;").Replace(">", "&gt;") +
                                         "<br/>FlightId<br/>" + selectedFlightId +
                                         "<br/>Origin<br/>" + originRcd +
                                         "<br/>destination<br/>" + destinationRcd +
                                         "<br/>Boarding class<br/>" + boardingClass +
                                         "<br/>Booking class<br/>" + bookingClass);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string BackToPassengerDetail()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/PassengerDetail.ascx", string.Empty);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string FillSeat(string seatXml, bool renderControl)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                if (bookingHeader == null || objHp.SessionTimeout() == true)
                {
                    return "{002}";
                }
                else
                {
                    B2CVariable objUv = B2CSession.Variable;

                    Itinerary itinerary = B2CSession.Itinerary;
                    Passengers passengers = B2CSession.Passengers;
                    Quotes quotes = B2CSession.Quotes;
                    Fees fees = B2CSession.Fees;
                    Mappings mappings = B2CSession.Mappings;
                    Services services = B2CSession.Services;
                    Remarks remarks = B2CSession.Remarks;
                    Payments payments = B2CSession.Payments;
                    Taxes taxes = B2CSession.Taxes;
                    Agents objAgents = B2CSession.Agents;

                    if (objAgents != null && objAgents.Count > 0 && passengers != null && passengers.Count > 0 && mappings != null && mappings.Count > 0)
                    {
                        Library objLi = new Library();

                        using (StringReader srd = new StringReader(seatXml))
                        {
                            XPathDocument xmlDoc = new XPathDocument(srd);
                            XPathNavigator nv = xmlDoc.CreateNavigator();

                            string result = string.Empty;

                            FillMapping(seatXml, renderControl);

                            if (renderControl == true)
                            {
                                //Confirm seat selection on call record.
                                for (int i = 0; i < mappings.Count; i++)
                                {
                                    mappings[i].SeatConfirm = true;
                                }

                                CalculateSeatFee();
                                // check set seat for SeatMandatory filed

                                if (B2CSetting.SetSeatMandatory)
                                {
                                    bool notAllSelect = false;
                                    foreach (XPathNavigator n in nv.Select("booking/mapping"))
                                    {
                                        for (int i = 0; i < mappings.Count; i++)
                                        {
                                            if (String.IsNullOrEmpty(mappings[i].seat_number))
                                            {
                                                notAllSelect = true; break;
                                            }
                                        }
                                    }
                                    if (notAllSelect)
                                    {
                                        //Invalid date of birth.
                                        return "{404}";
                                    }
                                }

                                if (objAgents != null && objAgents.Count > 0 && passengers != null && passengers.Count > 0)
                                {

                                    if (string.IsNullOrEmpty(B2CSetting.SeatToStep) || B2CSetting.SeatToStep == "4")
                                    {
                                        //Back to passenger detail
                                        result = objLi.GenerateControlString("UserControls/PassengerDetail.ascx", string.Empty);
                                    }
                                    else if (B2CSetting.SeatToStep == "5")
                                    {
                                        //Generate interface.
                                        objUv.CurrentStep = 5;
                                        string strHttp = HttpHelper.LoadSecurePage(B2CSession.Client, objUv.CurrentStep);
                                        if (strHttp == "https")
                                        {
                                            B2CSession.Https = true;
                                            result = "{000}";//Redirect to https.
                                        }
                                        else
                                        {
                                            result = objLi.GenerateControlString("UserControls/Payment.ascx", string.Empty);
                                        }
                                    }
                                    else
                                    {
                                        result = string.Empty;
                                    }
                                }
                                else
                                {
                                    result = string.Empty;
                                }
                            }
                            else
                            {
                                result = string.Empty;
                            }

                            return result;
                        }
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, seatXml.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }
        public void CalculateSeatFee()
        {
            BookingHeader bookingHeader = B2CSession.BookingHeader;
            Itinerary itinerary = B2CSession.Itinerary;
            Passengers passengers = B2CSession.Passengers;
            Quotes quotes = B2CSession.Quotes;
            Fees fees = B2CSession.Fees;
            Mappings mappings = B2CSession.Mappings;
            Services services = B2CSession.Services;
            Remarks remarks = B2CSession.Remarks;
            Payments payments = B2CSession.Payments;
            Taxes taxes = B2CSession.Taxes;
            Agents objAgents = B2CSession.Agents;

            //Calclate fee
            fees.objService = B2CSession.AgentService;
            fees.CalculateSeatFee(objAgents[0].agency_code,
                                    bookingHeader.currency_rcd,
                                    bookingHeader.booking_id.ToString(),
                                    bookingHeader,
                                    itinerary,
                                    passengers,
                                    services,
                                    remarks,
                                    mappings,
                                    Classes.Language.CurrentCode().ToUpper(),
                                    false);
            fees.objService = null;
        }
        public void FillMapping(string seatXml, bool renderControl)
        {
            Classes.Helper objHp = new Classes.Helper();
            Library objLi = new Library();
            Mappings mappings = B2CSession.Mappings;

            if (!string.IsNullOrEmpty(seatXml))
            {
                using (StringReader srd = new StringReader(seatXml))
                {
                    XPathDocument xmlDoc = new XPathDocument(srd);
                    XPathNavigator nv = xmlDoc.CreateNavigator();

                    string result = string.Empty;

                    foreach (XPathNavigator n in nv.Select("booking/mapping"))
                    {
                        for (int i = 0; i < mappings.Count; i++)
                        {
                            if (mappings[i].passenger_id.Equals(XmlHelper.XpathValueNullToGUID(n, "passenger_id")) &&
                                mappings[i].booking_segment_id.Equals(XmlHelper.XpathValueNullToGUID(n, "segment_id")))
                            {
                                int iSelectedRow = XmlHelper.XpathValueNullToInt(n, "seat_row");
                                string strSelectedColumn = XmlHelper.XpathValueNullToEmpty(n, "seat_col");
                                string strSelectedSeat = (strSelectedColumn == string.Empty) ? string.Empty : iSelectedRow + strSelectedColumn;
                                string strSelectedSeatFeeRcd = XmlHelper.XpathValueNullToEmpty(n, "fee_rcd");

                                if (renderControl == false)
                                {
                                    if (mappings[i].seat_row != iSelectedRow && mappings[i].seat_column != strSelectedColumn)
                                    {
                                        mappings[i].SeatConfirm = false;
                                    }
                                }

                                mappings[i].seat_row = iSelectedRow;
                                mappings[i].seat_column = strSelectedColumn;
                                mappings[i].seat_number = strSelectedSeat;
                                mappings[i].seat_fee_rcd = strSelectedSeatFeeRcd;
                                mappings[i].SeatConfirm = false;
                            }

                            // keep last state of seat to temp  if not confirm  for set temp to mapping again in UpdateSeatInfoFromTemp
                            if (renderControl == true)
                            {
                                mappings[i].temp_seat_row = mappings[i].seat_row;
                                mappings[i].temp_seat_column = mappings[i].seat_column;
                                mappings[i].temp_seat_number = mappings[i].seat_number;
                                mappings[i].temp_seat_fee_rcd = mappings[i].seat_fee_rcd;
                            }

                        }

                    }
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public string PreCalculateSeat(string seatXml)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                if (bookingHeader == null || objHp.SessionTimeout() == true)
                {
                    return "{002}";
                }
                else
                {
                    FillMapping(seatXml, false);
                    CalculateSeatFee();
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, seatXml.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string CancelSelectSeat(bool renderControl)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                Mappings mappings = B2CSession.Mappings;
                if (mappings == null || objHp.SessionTimeout() == true)
                {
                    return "{204}";
                }
                else
                {
                    UpdateSeatInfoFromTemp(mappings);
                    objHp.FillTempToBaggageFee();
                    CalculateSeatFee();
                    if (renderControl == true)
                    {
                        Library objLi = new Library();
                        return objLi.GenerateControlString("UserControls/PassengerDetail.ascx", string.Empty);
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, string.Empty);

                return "{600}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetMap(string selectedFlightId, string originRcd, string destinationRcd, string boardingClass, string bookingClass)
        {
            try
            {
                Itinerary itinerary = B2CSession.Itinerary;
                Mappings mappings = B2CSession.Mappings;

                if (itinerary != null && itinerary.Count > 0 && mappings != null && mappings.Count > 0)
                {
                    Library objLi = new Library();

                    //Get Seat map from user control
                    return objLi.GenerateControlString("UserControls/Map.ascx",
                                                        selectedFlightId + "|" + originRcd + "|" + destinationRcd + "|" + boardingClass + "|" + bookingClass);
                }
                else
                {
                    //Session timeout then load to home.
                    return "{002}";
                }

            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Flight Id<br/>" + selectedFlightId + "<br/>origin<br/>" + originRcd + "<br/>destination<br/>" + destinationRcd + "<br/>boarding class<br/>" + boardingClass + "<br/>booking class<br/>" + bookingClass);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPassengerSeatMap(string selectedFlightId)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                StringBuilder stb = new StringBuilder();
                Itinerary itinerary = B2CSession.Itinerary;
                Mappings mappings = B2CSession.Mappings;
                Passengers passengers = B2CSession.Passengers;
                Library objLi = new Library();
                string xmlResult = string.Empty;
                if (itinerary != null && itinerary.Count > 0 && mappings != null && mappings.Count > 0)
                {
                    using (StringWriter stw = new StringWriter(stb))
                    {
                        using (XmlWriter xtw = XmlWriter.Create(stw))
                        {
                            objLi.RetrivedSegmentMappingXml(xtw, itinerary, mappings, selectedFlightId, string.Empty);
                        }
                    }

                    // Get Baggage
                    using (StringWriter stw = new StringWriter())
                    {
                        XmlWriterSettings settings = new XmlWriterSettings();
                        settings.OmitXmlDeclaration = false;
                        settings.ConformanceLevel = ConformanceLevel.Fragment;
                        using (XmlWriter xtw = XmlWriter.Create(stw, settings))
                        {
                            objXsl.BuiltBaggage(xtw, selectedFlightId, passengers);
                        }
                        xmlResult = stb.ToString().Replace("</Booking>", stw.ToString() + "</Booking>");
                    }

                    //Passenger Information
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    objTransform = objXsl.GetXSLDocument("SeatPassenger");
                    return objLi.RenderHtml(objTransform, objArgument, xmlResult);
                }
                else
                {
                    //Session timeout then load home.
                    return "{002}";
                }

            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Selected FlightId - " + selectedFlightId);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string ReadSeatFee(string strFeeRcd, string strClass, string strOrigin, string strDestination)
        {
            BookingHeader bookingHeader = B2CSession.BookingHeader;
            Mappings mappings = B2CSession.Mappings;

            Helper objHelper = new Helper();
            Fees objFees = new Fees();

            string strFareCode = string.Empty;
            //Find fare Basis.
            if (mappings != null)
            {
                for (int i = 0; i < mappings.Count; i++)
                {
                    if (mappings[i].origin_rcd.Equals(strOrigin) && mappings[i].destination_rcd.Equals(strDestination))
                    {
                        strFareCode = mappings[i].fare_code;
                        break;
                    }
                }
            }

            objFees.objService = B2CSession.AgentService;
            objFees.GetFeeDefinition(strFeeRcd,
                                    bookingHeader.currency_rcd,
                                    bookingHeader.agency_code,
                                    strClass,
                                    strFareCode,
                                    strOrigin,
                                    strDestination,
                                    string.Empty,
                                    DateTime.MinValue,
                                    Classes.Language.CurrentCode(),
                                    false);

            for (int i = 0; i < objFees.Count; i++)
            {
                if (objFees[i].fee_rcd.Equals(strFeeRcd.ToUpper()))
                {
                    return "{\"fee_rcd\":\"" + objFees[i].fee_rcd + "\", \"fee_amount\":\"" + objFees[i].fee_amount + "\", \"fee_amount_incl\":\"" + objFees[i].fee_amount_incl + "\", \"currency_rcd\":\"" + objFees[i].currency_rcd + "\"}";
                }
            }
            return string.Empty;
        }
        #endregion

        #region Payment
        [WebMethod(EnableSession = true)]
        public string GetMyName()
        {
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                StringBuilder SelectName = new StringBuilder();
                if (bookingHeader != null)
                {
                    if (SelectName.Length == 0)
                    {
                        SelectName.Append(bookingHeader.firstname);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.lastname);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.address_line1);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.address_line2);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.street);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.state);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.city);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.province);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.country_rcd);
                        SelectName.Append("{}");
                        SelectName.Append(bookingHeader.zip_code);
                    }
                }

                return SelectName.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string bookNowPaylater(bool ticketCreate, string bookingDate)
        {
            B2CVariable objUv = B2CSession.Variable;
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Library objLi = new Library();

                if (objLi.ValidSave(bookingHeader, itinerary, passengers, mappings) == true)
                {
                    if (objUv == null || objHp.SessionTimeout() == true)
                    {
                        return "{002}";
                    }
                    else
                    {
                        //Clear Insurance Fee....
                        CalculateInsuranceFee(false, false);

                        //tikAERO-2010-0935 start
                        bookingHeader.approval_flag = 1;
                        //end

                        string result = string.Empty;

                        if (objUv != null)
                        {
                            if (ticketCreate == true)
                            {
                                foreach (Mapping mp in mappings)
                                {
                                    if (mp.ticket_number == string.Empty && mp.passenger_status_rcd == "OK" && objLi.CalOutStandingBalance(quotes, fees, payments) == 0)
                                    {
                                        mp.create_by = objUv.UserId;
                                        mp.create_date_time = DateTime.Now;
                                        mp.update_by = objUv.UserId;
                                        mp.update_date_time = DateTime.Now;
                                    }
                                }
                            }

                            //Set update information
                            SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);

                            //Save change information
                            Booking objBooking = new Booking();
                            objBooking.objService = B2CSession.AgentService;
                            result = objBooking.SaveBooking(ticketCreate,
                                                            ref bookingHeader,
                                                            ref itinerary,
                                                            ref passengers,
                                                            ref quotes,
                                                            ref fees,
                                                            ref mappings,
                                                            ref services,
                                                            ref remarks,
                                                            ref payments,
                                                            ref taxes,
                                                            Classes.Language.CurrentCode().ToUpper());
                            objBooking.objService = null;
                            if (result.Length > 0)
                            {
                                XElement elements = XElement.Parse(result);
                                if (elements != null)
                                {
                                    foreach (XElement x in elements.XPathSelectElements("Fee[fee_rcd='PAYPAL']"))
                                        x.RemoveAll();

                                    result = elements.ToString();
                                }

                                //Check Error Response
                                XPathDocument xmlDoc = new XPathDocument(new StringReader(result));
                                XPathNavigator nv = xmlDoc.CreateNavigator();
                                if (nv.Select("ErrorResponse/Error").Count == 0)
                                {
                                    //Fill new booking xml into booking object.
                                    objLi.FillBooking(result,
                                                        ref bookingHeader,
                                                        ref passengers,
                                                        ref itinerary,
                                                        ref mappings,
                                                        ref payments,
                                                        ref remarks,
                                                        ref taxes,
                                                        ref quotes,
                                                        ref fees,
                                                        ref services);

                                    B2CSession.BookingHeader = bookingHeader;

                                    //Send passenger itinerary by email.
                                    objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                                    if (B2CSession.Client != null)
                                    {
                                        objUv.CurrentStep = 7;
                                    }
                                    else
                                    {
                                        objUv.CurrentStep = 1;
                                    }

                                    result = objLi.GenerateControlString("UserControls/BookingSummary.ascx", result);

                                    objUv.FormOfPaymentFee = string.Empty;
                                    B2CSession.RemoveBookingSession();
                                }
                                else
                                {
                                    result = XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/ErrorCode") + "{!}" + XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/Message");
                                }
                            }

                        }
                        else
                        {
                            throw new Exception("Session Expired.");
                        }

                        return result;
                    }
                }
                else
                {
                    //Object validation failed.
                    return "{005}";
                }

            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string PaymentBookNowPayLater(bool ticketCreate, string bookingDate, string formOfPayment, string formOfPaymentSubType)
        {
            try
            {
                ServiceResponse response = base.BasePaymentBookNowPayLater(ticketCreate, bookingDate, formOfPayment, formOfPaymentSubType);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPaymentFee(string formOfPayment, string formOfPaymentSubType)
        {
            try
            {
                ServiceResponse response = base.BaseGetPaymentFee(formOfPayment, formOfPaymentSubType);
                if (response.Success == true)
                {
                    return response.HTML;
                }
                else
                {
                    return "{" + response.Code + "}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string WellnetPayment(bool ticketCreate, string bookingDate)
        {
            //start wellnet 
            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("WellnetPaymentSetting");
            string strURLPopup = string.Empty;
            bool bIsWellnetSuccess = false;
            bool bIsPaymentExipreDate = true;
            //bool bIsPaymentLimitNull = false;
            string recordLocator = string.Empty;
            long bookingNumber = 0;
            //end wellnet
            Library objLi = new Library();
            StringBuilder strLog = new StringBuilder();

            Classes.Helper objHp = new Classes.Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                AgentService objAgentService = new AgentService();
                Booking objBooking = new Booking();

                B2CVariable objUv = B2CSession.Variable;
                Agents objAgents = B2CSession.Agents;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                DateTime payLimit = DateTime.MinValue;

                string result = string.Empty;
                //Get Agency if not found take from cache.

                //Clear Insurance Fee....
                CalculateInsuranceFee(false, false);

                // check conv fee
                bool IsConv = false;

                if (fees != null && fees.Count > 0)
                {
                    for (int i = 0; i < fees.Count; i++)
                    {
                        if (fees[i].fee_rcd.ToUpper().Equals("CONV"))
                        {
                            IsConv = true;
                        }
                    }
                }


                if (IsConv && objUv != null &&
                    objLi.ValidSave(bookingHeader, itinerary, passengers, mappings) &&
                    bookingHeader != null &&
                    (itinerary != null && itinerary.Count > 0) &&
                    (passengers != null && passengers.Count > 0) &&
                    (mappings != null && mappings.Count > 0) &&
                    (objAgents != null && objAgents.Count > 0))
                {
                    //Read timelimit...
                    objAgentService.objService = B2CSession.AgentService;

                    DateTime departureDate = itinerary[0].departure_date;

                    string departureTime = itinerary[0].departure_time.ToString();
                    //string strXML = string.Empty;
                    string strLanguage = string.Empty;

                    int hourAdd = int.Parse(departureTime.PadLeft(4, '0').Substring(0, 2));
                    int minuteAdd = int.Parse(departureTime.PadLeft(4, '0').Substring(2, 2));
                    //int timelimit_hour = 0;

                    if (B2CSession.LanCode == null)
                    { strLanguage = "en-us"; }
                    else
                    { strLanguage = B2CSession.LanCode; }


                    //Recalculate departure date with time.
                    departureDate = new DateTime(departureDate.Year, departureDate.Month, departureDate.Day, hourAdd, minuteAdd, 0);

                    payLimit = objBooking.GetBookingTimeLimit(remarks,
                                                              objAgents[0].agency_timezone,
                                                              objAgents[0].system_setting_timezone,
                                                              departureDate);

                    if (payLimit.Equals(DateTime.MinValue))
                    {
                        //External Payment Timelimit not found.
                        return "{706}";
                    }
                    else if (payLimit >= departureDate)
                    {
                        //return error message for javascript
                        result = bIsPaymentExipreDate ? "overdue payment" : "paylimit null";
                    }
                    else
                    {
                        //tikAERO-2010-0935 start
                        bookingHeader.approval_flag = 1;
                        //end

                        if (ticketCreate == true)
                        {
                            foreach (Mapping mp in mappings)
                            {
                                if (mp.ticket_number == string.Empty && mp.passenger_status_rcd == "OK" && objLi.CalOutStandingBalance(quotes, fees, payments) == 0)
                                {
                                    mp.create_by = objUv.UserId;
                                    mp.create_date_time = DateTime.Now;
                                    mp.update_by = objUv.UserId;
                                    mp.update_date_time = DateTime.Now;
                                }
                            }
                        }

                        if (Setting != null)
                        {
                            //Set update information
                            SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);

                            if (objUv.PaymentGateway != string.Empty)
                            {
                                Remark objRemark = new Remark();
                                foreach (Remark obj in remarks)
                                {
                                    if (obj.remark_type_rcd == "TKTL")
                                    {
                                        objRemark.timelimit_date_time = obj.temp_timelimit_date_time;
                                        objRemark.remark_type_rcd = obj.remark_type_rcd;
                                        objRemark.agency_code = obj.agency_code;
                                        objRemark.utc_timelimit_date_time = obj.utc_timelimit_date_time;
                                        objRemark.booking_id = obj.booking_id;
                                        objRemark.remark_text = obj.remark_text;
                                        objRemark.booking_remark_id = Guid.NewGuid();
                                        break;
                                    }
                                }
                                remarks.Add(objRemark);
                            }
                            //Save change information
                            objBooking.objService = B2CSession.AgentService;
                            result = objBooking.SaveBooking(ticketCreate,
                                                            ref bookingHeader,
                                                            ref itinerary,
                                                            ref passengers,
                                                            ref quotes,
                                                            ref fees,
                                                            ref mappings,
                                                            ref services,
                                                            ref remarks,
                                                            ref payments,
                                                            ref taxes,
                                                            Classes.Language.CurrentCode().ToUpper());

                            //Write Log
                            strLog.Append("------------------- Save Booking" + Environment.NewLine);
                            strLog.Append(bookingHeader.record_locator + Environment.NewLine);

                            objBooking.objService = null;
                            if (result.Length > 0 && result != "<NewDataSet />")
                            {
                                //Check Error Response
                                XPathDocument xmlDocs = new XPathDocument(new StringReader(result));
                                XPathNavigator nv = xmlDocs.CreateNavigator();
                                if (nv.Select("ErrorResponse/Error").Count == 0)
                                {
                                    //Fill new booking xml into booking object.
                                    objLi.FillBooking(result,
                                                        ref bookingHeader,
                                                        ref passengers,
                                                        ref itinerary,
                                                        ref mappings,
                                                        ref payments,
                                                        ref remarks,
                                                        ref taxes,
                                                        ref quotes,
                                                        ref fees,
                                                        ref services);

                                    B2CSession.BookingHeader = bookingHeader;
                                    //get record_locator
                                    recordLocator = bookingHeader.record_locator;
                                    bookingNumber = bookingHeader.booking_number;

                                    if (recordLocator.Length > 0 && bookingNumber != 0)
                                    {
                                        //Send passenger itinerary by email.
                                        objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                                        //Write Log
                                        strLog.Append("------------------- Sent Mail" + Environment.NewLine);

                                        string freeArea = ConnectWellnet(ref strLog, Setting, payLimit);
                                        if (!string.IsNullOrEmpty(freeArea))
                                        {
                                            strURLPopup = string.Format(Setting["PopupURLPattern"].ToString(), freeArea);
                                            bIsWellnetSuccess = true;
                                        }
                                        else
                                        {
                                            //Write Log
                                            strLog.Append("------------------- Response Wellnet" + Environment.NewLine);
                                            strLog.Append(strURLPopup + Environment.NewLine);
                                        }
                                        ////end wellnet
                                        //#endregion
                                    }
                                    else
                                        //Write Log
                                        strLog.Append("------------------- recordLocator or bookingNumber Failed" + Environment.NewLine);

                                    if (!bIsWellnetSuccess)
                                        //Write Log
                                        strLog.Append("------------------- Wellnet Failed" + Environment.NewLine);


                                    if (B2CSession.Client != null)
                                    {
                                        objUv.CurrentStep = 7;
                                    }
                                    else
                                    {
                                        objUv.CurrentStep = 1;
                                    }

                                    result = objLi.GenerateControlString("UserControls/BookingSummary.ascx", result);

                                    //start wellnet3
                                    if (bIsWellnetSuccess && Setting["EnableREMWSDL"] != null && Setting["EnableREMWSDL"].ToString() == "Y" && strURLPopup != string.Empty)
                                    {
                                        result += "<!--urlPopup :" + strURLPopup + "-->";
                                    }
                                    else
                                        result += "<!--WellnetFailed :" + strURLPopup + "-->";
                                    //end wellnet3

                                    objUv.FormOfPaymentFee = string.Empty;
                                    B2CSession.RemoveBookingSession();

                                    // ClearSessionItinerary
                                    //if (tikSystem.Web.Library.ConfigurationHelper.ToBoolean("ClearSessionItinerary"))
                                    //{
                                    //    ClearSession();
                                    //}

                                }
                                else
                                {
                                    result = XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/ErrorCode") + "{!}" +
                                             XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/Message");
                                }
                            }
                            else
                            {
                                result = "{002}";
                                bIsWellnetSuccess = false;
                                //Write Log
                                strLog.Append("------------------- Savebooking Failed" + Environment.NewLine);
                                strLog.Append(strURLPopup + Environment.NewLine);
                            }

                        }//end wellnet2
                        else
                        {
                            result = "{002}";
                            bIsWellnetSuccess = false;
                            //Write Log
                            strLog.Append("------------------- Wellnet Setting is null" + Environment.NewLine);
                            strLog.Append(strURLPopup + Environment.NewLine);
                        }
                    }
                }
                else
                {
                    result = "{002}";
                    bIsWellnetSuccess = false;
                    //Write Log
                    strLog.Append("------------------- Validate condition Failed" + Environment.NewLine);
                    strLog.Append(strURLPopup + Environment.NewLine);
                }

                return result;
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, strLog.ToString());
                throw ex;
            }
        }

        private string ConnectWellnet(ref StringBuilder strLog, NameValueCollection Setting, DateTime payLimit)
        {
            Classes.Helper objHp = new Classes.Helper();
            //start wellnet
            tikSystem.Web.Library.RemWSDL.InParamSyunoBarCode objTranmission = new tikSystem.Web.Library.RemWSDL.InParamSyunoBarCode();
            string strLogLocation = string.Empty;
            Library objLi = new Library();
            try
            {
                string result = string.Empty;

                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Payments payments = B2CSession.Payments;

                if (string.IsNullOrEmpty(Setting["LogFilePath"]) == false)
                    strLogLocation = Setting["LogFilePath"] + @"\" + "Wellnet_" + String.Format("{0:yyyyMMdd}", DateTime.Now) + "_" + bookingHeader.record_locator + "_" + Guid.NewGuid() + ".log";

                //Write Log
                strLog.Append("------------------- Start wellnet <br>");


                //objBooking.GetRecordLocator(ref recordLocator, ref bookingNumber);
                string transactionNumber = bookingHeader.record_locator + GenerateTransactionNumber();
                string email = string.Empty;
                //string transactionNumber = bookingHeader.record_locator + GenerateTransactionNumber();

                //Write Log
                strLog.Append("------------------- Get record_locator <br>");
                strLog.Append(bookingHeader.record_locator + "<br>");

                //data type
                objTranmission.DataSyubetsu = Setting["DataSyubetsu"] ?? "4";
                //payment code
                objTranmission.SyunoPayCode = Setting["SyunoPayCode"] ?? "2121";
                //reception code
                objTranmission.SyunoRecvNum = transactionNumber;

                //Write Log
                strLog.Append("------------------- Reception code <br>");
                strLog.Append(transactionNumber + "<br>");

                //merchant no
                objTranmission.BcJigyosyaNo = Setting["BcJigyosyaNo"] ?? "PCA";
                //contract no
                objTranmission.BcAnkenNo = Setting["BcAnkenNo"] ?? "00001";
                //authentication key
                objTranmission.BcNinsyoKey = transactionNumber;

                //Write Log
                strLog.Append("------------------- Authentication key" + "<br>");
                strLog.Append(transactionNumber + "<br>");

                //data category
                objTranmission.SyunoOpCode = Setting["SyunoOpCode"] ?? "I";
                //company code
                objTranmission.SyunoCorpCode = Setting["SyunoCorpCode"] ?? "PCA";
                //telephone no
                if (bookingHeader.phone_home != string.Empty)
                {
                    if (bookingHeader.phone_home.Length > 13)
                        objTranmission.SyunoTel = bookingHeader.phone_home.Substring(0, 13);
                    else
                        objTranmission.SyunoTel = bookingHeader.phone_home;
                }
                else
                {
                    if (bookingHeader.phone_mobile.Length > 13)
                        objTranmission.SyunoTel = bookingHeader.phone_mobile.Substring(0, 13);
                    else
                        objTranmission.SyunoTel = bookingHeader.phone_mobile;
                }

                //contact name
                objTranmission.SyunoNameKanji = bookingHeader.contact_name;
                //Payment Timelimit
                objTranmission.SyunoPayLimit = payLimit.ToString("yyyyMMddHHmm");

                //Write Log
                strLog.Append("------------------- Payment Timelimit <br>");
                strLog.Append(payLimit.ToString("yyyyMMddHHmm") + "<br>");

                //PNR number
                objTranmission.SyunoReserveNum = bookingHeader.record_locator;

                //payment amount
                decimal OutStandingBalance = objLi.CalOutStandingBalance(quotes, fees, payments);

                //Write Log
                strLog.Append("------------------- Get payment amount <br>");
                strLog.Append(OutStandingBalance + "<br>");

                objTranmission.SyunoPayAmount = Math.Truncate(OutStandingBalance).ToString();

                //departure limit payment
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee.Index = 1;
                SyunoFee.SyunoFreeStr = string.Format(Setting["SyunoFee1Pattern"].ToString(), payLimit.ToString("yyyy/MM/dd HH:mm"));

                //departure date
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee2 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee2.Index = 2;
                SyunoFee2.SyunoFreeStr = string.Format(Setting["SyunoFee2Pattern"].ToString(), itinerary[0].departure_date.ToString("yyyy/MM/dd") + " " + itinerary[0].departure_time.ToString().PadLeft(4, '0').Insert(2, ":"));

                //flight number
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee3 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee3.Index = 3;
                SyunoFee3.SyunoFreeStr = string.Format(Setting["SyunoFee3Pattern"].ToString(), itinerary[0].airline_rcd + " " + itinerary[0].flight_number);

                //passenger
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee4 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee4.Index = 4;
                SyunoFee4.SyunoFreeStr = string.Format(Setting["SyunoFee4Pattern"].ToString(), passengers[0].firstname + " " + passengers[0].lastname);

                //constant description
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee5 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee5.Index = 5;
                SyunoFee5.SyunoFreeStr = Setting["SyunoFee5Pattern"];

                //constant description
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee6 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee6.Index = 6;
                SyunoFee6.SyunoFreeStr = Setting["SyunoFee6Pattern"];

                //constant description
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee7 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee7.Index = 7;
                SyunoFee7.SyunoFreeStr = Setting["SyunoFee7Pattern"];

                //Inquiry Contact
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee19 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee19.Index = 19;
                SyunoFee19.SyunoFreeStr = Setting["SyunoFee19Pattern"];

                //Inquiry contact phone no
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee20 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee20.Index = 20;
                SyunoFee20.SyunoFreeStr = Setting["SyunoFee20Pattern"];

                //Inquiry contact office hour
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee21 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee21.Index = 21;
                SyunoFee21.SyunoFreeStr = Setting["SyunoFee21Pattern"];

                //no. of tickets
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee30 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee30.Index = 30;
                SyunoFee30.SyunoFreeStr = Setting["SyunoFee30Pattern"];

                //new i/f recognition flag
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee39 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee39.Index = 39;
                SyunoFee39.SyunoFreeStr = Setting["SyunoFee39Pattern"];

                //customer guidance
                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee40 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee40.Index = 40;
                SyunoFee40.SyunoFreeStr = string.Format(Setting["SyunoFee40Pattern"].ToString(), bookingHeader.record_locator);

                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee41 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee41.Index = 41;
                SyunoFee41.SyunoFreeStr = Setting["SyunoFee41Pattern"];

                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee42 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee42.Index = 42;
                SyunoFee42.SyunoFreeStr = Setting["SyunoFee42Pattern"];

                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee43 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee43.Index = 43;
                SyunoFee43.SyunoFreeStr = Setting["SyunoFee43Pattern"];

                tikSystem.Web.Library.RemWSDL.SyunoFree SyunoFee29 = new tikSystem.Web.Library.RemWSDL.SyunoFree();
                SyunoFee29.Index = 29;

                if (!string.IsNullOrEmpty(bookingHeader.contact_email) && bookingHeader.contact_email.Length < 128)
                    email = bookingHeader.contact_email;
                if (!string.IsNullOrEmpty(bookingHeader.mobile_email))
                    email += "," + bookingHeader.mobile_email;
                if (email.Length > 128)
                    email = bookingHeader.contact_email;
                SyunoFee29.SyunoFreeStr = email;

                //Write Log
                strLog.Append("------------------- Add Email <br>");
                strLog.Append(email + "<br>");

                tikSystem.Web.Library.RemWSDL.SyunoFree[] SyunoFeeArrays = new tikSystem.Web.Library.RemWSDL.SyunoFree[15] { SyunoFee, SyunoFee2, SyunoFee3, SyunoFee4, SyunoFee5, SyunoFee19, SyunoFee20, SyunoFee21, SyunoFee30, SyunoFee39, SyunoFee40, SyunoFee41, SyunoFee42, SyunoFee43, SyunoFee29 };
                objTranmission.SyunoFreeArray = SyunoFeeArrays;

                tikSystem.Web.Library.RemWSDL.WellnetSoapHeader SOAPHeader = new tikSystem.Web.Library.RemWSDL.WellnetSoapHeader();
                SOAPHeader.UserId = Setting["SOAPUserId"];
                SOAPHeader.Password = Setting["SOAPPassword"];

                //Write Log
                strLog.Append("------------------- Start Time Request <br>");
                strLog.Append(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "<br>");


                tikSystem.Web.Library.RemWSDL.OutParamSyunoBarCode objReturn = null;
                using (tikSystem.Web.Library.RemWSDL.Yoyaku Yoyaku = new tikSystem.Web.Library.RemWSDL.Yoyaku())
                {
                    if (Setting["Timeout"] != null)
                        Yoyaku.Timeout = Convert.ToInt32(Setting["Timeout"]);
                    //Write Log
                    strLog.Append("------------------- YoyakuWSPath <br>");
                    Yoyaku.Url = Setting["YoyakuWSPath"];
                    //Write Log
                    strLog.Append("------------------- SOAPHeader <br>");
                    Yoyaku.WellnetSoapHeaderValue = SOAPHeader;

                    bool connect = false;
                    int loop = 0;
                    while (!connect && loop <= 2)
                    {
                        try
                        {
                            //Write Log
                            strLog.Append("------------------- Tranmission Step <br>");
                            objReturn = Yoyaku.YoyakuSyunoBarCode(objTranmission);
                            Yoyaku.Abort();

                            if (objReturn != null && (objReturn.Result == "W640" || objReturn.Result == "0000"))
                                connect = true;
                        }

                        catch (Exception wEx)
                        {
                            //Write Log
                            strLog.Append("------------------- Error Tranmission Step " + loop + " times <br>");
                            strLog.Append(wEx.Message + "<br>");
                            strLog.Append("Exception type :" + wEx.GetType().Name + "<br>");
                            System.Threading.Thread.Sleep(5000);
                        }

                        //objReturn.Result
                        loop++;
                    }

                    if (!connect && loop > 2)
                        throw new Exception("Error Connect Wellnet.");
                }

                if (objReturn != null && objReturn.FreeArea != "")
                    result = objReturn.FreeArea.Trim();
                else if (string.IsNullOrEmpty(strLogLocation) == false)
                    //write Log File.
                    objLi.WriteLog(strLogLocation, "<Error>" + XmlHelper.Serialize(objTranmission, false) + XmlHelper.Serialize(objReturn, false) + "</Error>");
                //end wellnet
                return result;
            }
            catch
            {
                //Write Log
                strLog.Append("------------------- Error Time Request <br>");
                strLog.Append(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "<br>");

                if (string.IsNullOrEmpty(strLogLocation) == false)
                    //write Log File.
                    objLi.WriteLog(strLogLocation, XmlHelper.Serialize(objTranmission, false));

                throw;
            }
        }

        [WebMethod(EnableSession = true)]
        public string ValidateVoucher(string voucherNumber, string password)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                B2CVariable objUv = B2CSession.Variable;
                Quotes quotes = B2CSession.Quotes;
                Mappings mappings = B2CSession.Mappings;
                Fees fees = B2CSession.Fees;
                Payments payments = B2CSession.Payments;
                Vouchers vouchers = B2CSession.Vouchers;
                Itinerary itinerary = B2CSession.Itinerary;
                Classes.Helper objHp = new Classes.Helper();
                Library objLi = new Library();
                Vouchers objVoucher = new Vouchers();

                string result = string.Empty;

                bool bWrite = false;

                if (objUv == null || objHp.SessionTimeout() == true || bookingHeader == null)
                {
                    return "{002}";
                }
                else
                {
                    try
                    {
                        objVoucher.objService = B2CSession.AgentService;
                        objVoucher.Read(string.Empty,
                                        voucherNumber,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        bookingHeader.currency_rcd,
                                        password,
                                        false,
                                        false,
                                        false,
                                        false,
                                        false,
                                        false,
                                        bWrite,
                                        mappings,
                                        fees,
                                        bookingHeader,
                                        itinerary,
                                        quotes,
                                        payments);

                        objVoucher.objService = null;

                        if (objVoucher.Count == 0)
                        {
                            result = string.Empty;
                        }
                        else if (objLi.isVoucherDuplicate(vouchers, objVoucher) == true)
                        {
                            result = "DUP";
                        }
                        else
                        {
                            StringBuilder objStr = new StringBuilder();

                            if (vouchers == null)
                            {
                                vouchers = new Vouchers();
                                B2CSession.Vouchers = vouchers;
                            }

                            foreach (Voucher v in objVoucher)
                            {
                                vouchers.Add(v);
                            }

                            string allowMultipleVoucher = B2CSetting.AllowMultipleVoucher;
                            string allowMultipleFOP = B2CSetting.AllowMultipleFOP;

                            if (vouchers.Count > 0)
                            {
                                objStr.Append("<Booking>");
                                objStr.Append("<Header>");
                                objStr.Append("<balance>" + objLi.CalOutStandingBalance(quotes, fees, payments) + "</balance>");
                                objStr.Append("<AllowMultipleVoucher_flag>" + allowMultipleVoucher + "</AllowMultipleVoucher_flag>");
                                objStr.Append("<AllowMultipleFOP>" + allowMultipleFOP + "</AllowMultipleFOP>");
                                objStr.Append("</Header>");
                                //Serialize voucher xml
                                objStr.Append(XmlHelper.Serialize(vouchers, false));
                                objStr.Append("</Booking>");
                            }

                            result = objStr.ToString();

                            //Generate Html
                            if (result.Length > 0)
                            {
                                System.Xml.Xsl.XslTransform objTransform = null;
                                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                                objTransform = objXsl.GetXSLDocument("VoucherDetail");
                                result = objLi.RenderHtml(objTransform, objArgument, result);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message + "\n" + e.StackTrace);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "voucher number<br/>" + voucherNumber + "<br/>password<br/>" + password);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string ClearVoucher()
        {
            Vouchers vouchers = B2CSession.Vouchers;
            if (vouchers != null)
            { vouchers.Clear(); }

            return "{000}";
        }

        [WebMethod(EnableSession = true)]
        public string PaymentVoucher(string VoucherId, string VoucherNumber, string formOfPayment, string formOfPaymentsubType)
        {
            Classes.Helper objHp = new Classes.Helper();
            B2CVariable objUv = B2CSession.Variable;
            System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

            if (objUv == null || objHp.SessionTimeout() == true)
            {
                return "{002}";
            }
            else
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Library objLi = new Library();

                //tikAERO-2010-0935 start
                bookingHeader.approval_flag = 1;
                //end
                Payments objPayment = new Payments();

                string result = string.Empty;

                try
                {
                    if (objLi.ValidSave(bookingHeader, itinerary, passengers, mappings) == true)
                    {
                        objPayment.objService = B2CSession.AgentService;

                        //Clear Insurance Fee....
                        CalculateInsuranceFee(false, false);

                        //Set update iniformation
                        SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);

                        result = objPayment.PaymentVoucher(ref bookingHeader,
                                                           ref itinerary,
                                                           ref passengers,
                                                           ref quotes,
                                                           ref fees,
                                                           ref mappings,
                                                           ref services,
                                                           ref remarks,
                                                           ref payments,
                                                           ref taxes,
                                                           VoucherId,
                                                           VoucherNumber,
                                                           formOfPayment,
                                                           formOfPaymentsubType,
                                                           Classes.Language.CurrentCode().ToUpper(),
                                                           objUv.UserId,
                                                           objUv.ip_address,
                                                           true);
                        if (result.Length > 0)
                        {
                            XElement elements = XElement.Parse(result);
                            if (elements != null)
                            {
                                foreach (XElement x in elements.XPathSelectElements("Fee[fee_rcd='PAYPAL']"))
                                    x.RemoveAll();

                                result = elements.ToString();
                            }

                            //Check Error Response
                            XPathDocument xmlDoc = new XPathDocument(new StringReader(result));
                            XPathNavigator nv = xmlDoc.CreateNavigator();

                            if (nv.Select("ErrorResponse/Error").Count == 0)
                            {
                                //Send passenger itinerary by email.
                                objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                                //Generate booking summary control.
                                if (B2CSession.Client != null)
                                {
                                    objUv.CurrentStep = 7;
                                }
                                else
                                {
                                    objUv.CurrentStep = 1;
                                }

                                result = objLi.GenerateControlString("UserControls/BookingSummary.ascx", result);
                                B2CSession.RemoveBookingSession();
                            }
                            else
                            {
                                result = XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/ErrorCode") + "{!}" +
                                         XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/Message");
                            }
                        }
                    }
                    else
                    {
                        result = "{005}";
                    }

                }
                catch (Exception ex)
                {
                    objHp.SendErrorEmail(ex, "voucher id<br/>" + VoucherId + "<br/>voucher number<br/>" + VoucherNumber + "<br/>form of payment<br/>" + formOfPayment + "<br/>sub type<br/>" + formOfPaymentsubType);
                    throw ex;
                }

                return result;
            }
        }
        [WebMethod(EnableSession = true)]
        public string PaymentCreditCard(string xmlPayment)
        {
            B2CVariable objUv = B2CSession.Variable;
            Classes.Helper objHp = new Classes.Helper();
            System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

            if (objUv == null || objHp.SessionTimeout() == true)
            {
                return "{002}";
            }
            else
            {

                Library objLi = new Library();

                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                //for insuarnace
                string strpassengers = XmlHelper.Serialize(passengers, false);

                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Agents objAgents = B2CSession.Agents;

                int iAgencyTimeZone = 0;
                int iSystemTimeZone = 0;

                // for insurance as pra
                if (!string.IsNullOrEmpty(B2CSetting.CreateACEPassengerSession))
                {
                    //Create temp passenger information for ACE Before Passenger objetc is sorted.
                    //ACE Required passenger inforamtion to be pass in exactly same order as input screen.
                    CreateACEPassengerSession(passengers);
                }

                //fill exchange information when exchange_fee_amount not empty
                if (!string.IsNullOrEmpty(B2CSetting.DefaultCurrency))
                {
                    xmlPayment = xmlPayment.Replace("</exchange_currency>", B2CSetting.DefaultCurrency + "</exchange_currency>");
                }

                //tikAERO-2010-0935 start
                bookingHeader.approval_flag = 1;
                //end
                Payments objPayment = new Payments();

                string result = string.Empty;

                if (objUv.booking_payment_id.Equals(Guid.Empty))
                {
                    objUv.booking_payment_id = Guid.NewGuid();
                }

                try
                {
                    if (objLi.ValidSave(bookingHeader, itinerary, passengers, mappings) == true)
                    {
                        if (PointValidatePass(B2CSession.Client, mappings) == true)
                        {
                            objPayment.objService = B2CSession.AgentService;
                            string strRequestSource = B2CSetting.Ogone3DRequestSource;

                            //Set update iniformation
                            SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                            result = objPayment.PaymentCreditCard(ref bookingHeader,
                                                                  ref itinerary,
                                                                  ref passengers,
                                                                  ref quotes,
                                                                  ref fees,
                                                                  ref mappings,
                                                                  ref services,
                                                                  ref remarks,
                                                                  ref payments,
                                                                  ref taxes,
                                                                  xmlPayment,
                                                                  strRequestSource,
                                                                  Classes.Language.CurrentCode().ToUpper(), objUv.UserId,
                                                                  objUv.booking_payment_id,
                                                                  objUv.ip_address,
                                                                  objUv.FormOfPaymentFee,
                                                                  objUv.Adult,
                                                                  objUv.Child,
                                                                  objUv.Infant,
                                                                  true);
                            if (result.Length > 0)
                            {
                                XElement elements = XElement.Parse(result);
                                if (elements != null)
                                {
                                    foreach (XElement x in elements.XPathSelectElements("Fee[fee_rcd='PAYPAL']"))
                                        x.RemoveAll();

                                    result = elements.ToString();
                                }

                                XPathDocument xmlDoc = new XPathDocument(new StringReader(result));
                                XPathNavigator nv = xmlDoc.CreateNavigator();

                                bool bSaved = false;
                                if (nv.Select("NewDataSet/Payments").Count > 0)
                                {
                                    foreach (XPathNavigator n in nv.Select("NewDataSet/Payments"))
                                    {
                                        //Check Response Data.
                                        if (XmlHelper.XpathValueNullToEmpty(n, "ResponseCode").ToUpper() == "APPROVED")
                                        {
                                            bSaved = true;
                                        }
                                        else if (n.SelectSingleNode("ResponseCode").InnerXml == "3D")
                                        {
                                            string strResult = string.Empty;
                                            Fees objPaymentFees = null;
                                            //Call Credit Card Payment.
                                            if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                                                 ref itinerary,
                                                                                 ref passengers,
                                                                                 ref quotes,
                                                                                 ref fees,
                                                                                 ref objPaymentFees,
                                                                                 ref mappings,
                                                                                 ref services,
                                                                                 ref remarks,
                                                                                 ref payments,
                                                                                 ref taxes,
                                                                                 xmlPayment,
                                                                                 objUv.FormOfPaymentFee,
                                                                                 objUv.UserId,
                                                                                 objUv.booking_payment_id,
                                                                                 objUv.ip_address) == true)
                                            {
                                                if (objPayment.Count > 0)
                                                {
                                                    if (bookingHeader.record_locator.Length == 0)
                                                    {
                                                        objUv.PaymentReference = "TRUE";
                                                        if (n.SelectSingleNode("record_locator") != null)
                                                        {
                                                            bookingHeader.record_locator = XmlHelper.XpathValueNullToEmpty(n, "record_locator");
                                                        }

                                                        if (n.SelectSingleNode("booking_number") != null)
                                                        {
                                                            bookingHeader.booking_number = XmlHelper.XpathValueNullToInt(n, "booking_number");
                                                        }

                                                        B2CSession.BookingHeader = bookingHeader;
                                                        B2CSession.Itinerary = itinerary;
                                                        B2CSession.Passengers = passengers;
                                                        B2CSession.Quotes = quotes;
                                                        B2CSession.Fees = fees;
                                                        B2CSession.Mappings = mappings;
                                                        B2CSession.Services = services;
                                                        B2CSession.Remarks = remarks;
                                                        B2CSession.Payments = payments;
                                                        B2CSession.Taxes = taxes;
                                                    }
                                                    //Save CCpayment object to Session
                                                    B2CSession.Https = true;
                                                    B2CSession.CcPayment = objPayment;
                                                    B2CSession.PaymentFees = objPaymentFees;
                                                    objUv.PaymentGateway = "OGN";
                                                }
                                            }
                                            result = XmlHelper.XpathValueNullToEmpty(n, "ResponseCode") + "{}" + XmlHelper.XpathValueNullToEmpty(n, "ResponseHtml");
                                        }
                                        else
                                        {
                                            if (n.SelectSingleNode("ErrorCode") == null)
                                            {
                                                result = XmlHelper.XpathValueNullToEmpty(n, "ResponseCode") + "{}" + XmlHelper.XpathValueNullToEmpty(n, "ResponseText");
                                            }
                                            else
                                            {
                                                result = XmlHelper.XpathValueNullToEmpty(n, "ErrorCode") + "{}" + XmlHelper.XpathValueNullToEmpty(n, "ResponseText");
                                            }

                                        }
                                    }
                                }
                                else if (nv.Select("ErrorResponse/Error").Count > 0)
                                {
                                    result = XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/ErrorCode") + "{}" + XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/Message");
                                }
                                else
                                { bSaved = true; }

                                //--------------------------------------------------------------------------------------------------------------
                                if (bSaved == true)
                                {
                                    // Find Insuarance ID And Save
                                    XPathDocument xmlDocPayment = new XPathDocument(new StringReader(xmlPayment));
                                    XPathNavigator nvPayment = xmlDocPayment.CreateNavigator();
                                    string productVariantID = "";
                                    if (nvPayment.Select("payment").Count > 0 && nvPayment.SelectSingleNode("payment/InsuranceID") != null
                                        && nvPayment.SelectSingleNode("payment/InsuranceID").InnerXml != "")
                                    {
                                        productVariantID = nvPayment.SelectSingleNode("payment/InsuranceID").InnerXml;
                                        xmlPayment = xmlPayment.Replace("</payment>", nv.SelectSingleNode("Booking/BookingHeader/record_locator").OuterXml + "</payment>");
                                        //Save Insuarance
                                        InsuaranceSave(productVariantID, xmlPayment, strpassengers);
                                    }
                                    else
                                    {
                                        //Ace insurance.
                                        Fee objInsFee = B2CSession.InsuaranceFee;
                                        Insurances objInsurances = null;
                                        if (objInsFee != null)
                                        {
                                            if (itinerary[0].od_origin_rcd.Equals("HKG"))
                                            {
                                                objInsurances = new Insurances(Insurances.InsuranceType.ACENonJapan);
                                            }
                                            else
                                            {
                                                objInsurances = new Insurances(Insurances.InsuranceType.ACEJapan);
                                            }

                                            Routes routes = CacheHelper.CacheDestination();

                                            DateTime dtDept = DateTime.MinValue;
                                            if (string.IsNullOrEmpty(objUv.DepartureDate) == false)
                                            {
                                                dtDept = new DateTime(Convert.ToInt16(objUv.DepartureDate.Substring(0, 4)),
                                                                      Convert.ToInt16(objUv.DepartureDate.Substring(4, 2)),
                                                                      Convert.ToInt16(objUv.DepartureDate.Substring(6, 2)));

                                            }
                                            DateTime dtReturn = DateTime.MinValue;
                                            if (string.IsNullOrEmpty(objUv.ReturnDate) == false)
                                            {
                                                dtReturn = new DateTime(Convert.ToInt16(objUv.ReturnDate.Substring(0, 4)),
                                                                        Convert.ToInt16(objUv.ReturnDate.Substring(4, 2)),
                                                                        Convert.ToInt16(objUv.ReturnDate.Substring(6, 2)));

                                            }

                                            DateTime dtClient = XmlHelper.XpathValueNullToDateTime(nvPayment, "payment/client_date_time");
                                            string strPurposeOfTravel = XmlHelper.XpathValueNullToEmpty(nvPayment, "payment/purpose_of_travel");
                                            if (string.IsNullOrEmpty(strPurposeOfTravel))
                                            {
                                                //Default value is 2
                                                strPurposeOfTravel = "2";
                                            }
                                            if (dtClient.Equals(DateTime.MinValue))
                                            {
                                                dtClient = DateTime.Now;
                                            }

                                            //Get agency timezone.
                                            if (objAgents != null && objAgents.Count > 0)
                                            {
                                                iAgencyTimeZone = objAgents[0].agency_timezone;
                                                iSystemTimeZone = objAgents[0].system_setting_timezone;
                                            }
                                            else
                                            {
                                                Agent objAgent = CacheHelper.CacheAgency(objUv.Agency_Code);
                                                iAgencyTimeZone = objAgent.agency_timezone;
                                                iSystemTimeZone = objAgent.system_setting_timezone;
                                            }

                                            if (objInsurances.RequestPolicy(bookingHeader,
                                                                            passengers,
                                                                            itinerary,
                                                                            dtClient,
                                                                            dtDept,
                                                                            dtReturn,
                                                                            fees,
                                                                            objUv.OriginRcd,
                                                                            objUv.DestinationRcd,
                                                                            Classes.Language.CurrentCode(),
                                                                            strPurposeOfTravel,
                                                                            objInsFee.fee_amount_incl,
                                                                            iAgencyTimeZone,
                                                                            iSystemTimeZone) == true)
                                            {
                                                //Perform insurance opertion
                                            }
                                        }

                                    }
                                    //Send passenger itinerary by email.
                                    objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                                    if (B2CSession.Client != null)
                                    {
                                        objUv.CurrentStep = 7;
                                    }
                                    else
                                    {
                                        objUv.CurrentStep = 1;
                                    }
                                    //Generate booking summary control.
                                    result = objLi.GenerateControlString("UserControls/BookingSummary.ascx", result);
                                    //Empty temp booking_payment_id
                                    objUv.booking_payment_id = Guid.Empty;
                                    B2CSession.RemoveBookingSession();
                                }
                            }
                            else
                            {
                                result = "{701}";
                            }
                        }
                        else
                        {
                            result = "{702}";
                        }
                    }
                    else
                    {
                        //object validation failed
                        result = "{005}";
                    }

                }
                catch (Exception ex)
                {
                    objHp.SendErrorEmail(ex, "payment xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                    throw ex;
                }
                return result;
            }
        }

        // Start Yai Add External Payment
        [WebMethod(EnableSession = true)]
        public bool CheckCaptchaSecurity(string strSecurityCode, bool caseSensitive)
        {
            try
            {
                //remove ToLower() if you want to make the comparison case sensitive
                if (caseSensitive)
                {
                    if (B2CSession.CaptchaImageText != null && string.Compare(strSecurityCode.Trim(), B2CSession.CaptchaImageText, false) == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (B2CSession.CaptchaImageText != null && strSecurityCode.ToLower() == B2CSession.CaptchaImageText.ToLower())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Code<br/>" + strSecurityCode + "<br/>case sensitive<br/>" + caseSensitive);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string RefreshCaptcha()
        {
            try
            {
                B2CSession.CaptchaImageText = CaptchaImage.GenerateRandomCode(CaptchaType.AlphaNumeric, 6); //generate new string
                return B2CSession.CaptchaImageText;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string ExternalPayment(bool ticketCreate, string strReferenceCode)
        {
            Classes.Helper objHp = new Classes.Helper();
            System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
            try
            {
                B2CVariable objUv = B2CSession.Variable;

                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Library objLi = new Library();
                //tikAERO-2010-0935 start
                bookingHeader.approval_flag = 1;
                //end
                // Fill External Reference code
                bookingHeader.external_payment_reference = strReferenceCode.Trim();
                string result = string.Empty;

                if (objUv == null ||
                    bookingHeader == null ||
                    passengers == null ||
                    passengers.Count == 0 ||
                    itinerary == null ||
                    itinerary.Count == 0 ||
                    mappings == null ||
                    mappings.Count == 0 ||
                    objHp.SessionTimeout() == true)
                {
                    return "{002}";
                }
                else
                {
                    if (ticketCreate == true)
                    {
                        foreach (Mapping mp in mappings)
                        {
                            if (mp.ticket_number == string.Empty && mp.passenger_status_rcd == "OK" && objLi.CalOutStandingBalance(quotes, fees, payments) == 0)
                            {
                                mp.create_by = objUv.UserId;
                                mp.create_date_time = DateTime.Now;
                                mp.update_by = objUv.UserId;
                                mp.update_date_time = DateTime.Now;
                            }
                        }
                    }

                    //Set update information
                    SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);

                    //Save change information
                    Booking objBooking = new Booking();
                    objBooking.objService = B2CSession.AgentService;
                    result = objBooking.SaveBooking(ticketCreate,
                                                    ref bookingHeader,
                                                    ref itinerary,
                                                    ref passengers,
                                                    ref quotes,
                                                    ref fees,
                                                    ref mappings,
                                                    ref services,
                                                    ref remarks,
                                                    ref payments,
                                                    ref taxes,
                                                    Classes.Language.CurrentCode().ToUpper());
                    if (result.Length > 0)
                    {
                        //Fill new booking xml into booking object.
                        objLi.FillBooking(result,
                                            ref bookingHeader,
                                            ref passengers,
                                            ref itinerary,
                                            ref mappings,
                                            ref payments,
                                            ref remarks,
                                            ref taxes,
                                            ref quotes,
                                            ref fees,
                                            ref services);

                        B2CSession.BookingHeader = bookingHeader;

                        //Send passenger itinerary by email.
                        objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                        if (B2CSession.Client != null)
                        {
                            objUv.CurrentStep = 7;
                        }
                        else
                        {
                            objUv.CurrentStep = 1;
                        }
                        result = objLi.GenerateControlString("UserControls/BookingSummary.ascx", result);
                        objUv.FormOfPaymentFee = string.Empty;
                        B2CSession.RemoveBookingSession();
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, "ref code<br/>" + strReferenceCode);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetPassengerIndex(int index)
        {
            try
            {
                Passengers passengers = B2CSession.Passengers;
                if (passengers != null)
                {
                    if (passengers.Count > 0 && passengers.Count > index)
                    {
                        StringBuilder SelectName = new StringBuilder();
                        if (SelectName.Length == 0)
                        {
                            SelectName.Append(passengers[index].title_rcd + "|" + passengers[index].gender_type_rcd);
                            SelectName.Append("{}");
                            SelectName.Append(passengers[index].firstname);
                            SelectName.Append("{}");
                            SelectName.Append(passengers[index].middlename);
                            SelectName.Append("{}");
                            SelectName.Append(passengers[index].lastname);
                        }
                        return SelectName.ToString();
                    }
                    else
                    { return string.Empty; }
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Pax Index<br/>" + index.ToString());
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string PaymentMultipleForm(string xmlPayment)
        {
            Classes.Helper objHp = new Classes.Helper();
            B2CVariable objUv = B2CSession.Variable;
            System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

            if (objUv == null || objHp.SessionTimeout() == true)
            {
                return "{002}";
            }
            else
            {
                Library objLi = new Library();

                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Vouchers vouchers = B2CSession.Vouchers;
                Agents objAgents = B2CSession.Agents;

                #region Validate SSRInventoryService
                if (B2CSetting.SSRInventoryService)
                {
                    string ssrResult = string.Empty;
                    string resultValidate = string.Empty;
                    resultValidate = ValidateSpecialService(string.Empty, services);
                    if (!string.IsNullOrEmpty(resultValidate))
                    {
                        if (resultValidate == "{003}")
                        { ssrResult = "{003}"; }
                        else if (resultValidate == "{003_1}")
                        { ssrResult = "{003_1}"; }
                        else if (resultValidate == "{003_2}")
                        { ssrResult = "{003_2}"; }

                        #region Remove SSR
                        if (ssrResult.Length > 0)
                        {
                            string ssrGroup = B2CSetting.SsrGroup;
                            string[] arr = !string.IsNullOrEmpty(ssrGroup) ? ssrGroup.Split(',') : new string[] { };
                            for (int i = services.Count - 1; i > -1; i--)
                            {
                                if (arr.Contains<string>(services[i].special_service_rcd))
                                {
                                    services.Remove(services[i]);
                                }
                            }

                            for (int j = fees.Count - 1; j > -1; j--)
                            {
                                if (arr.Contains<string>(fees[j].fee_rcd))
                                {
                                    fees.Remove(fees[j]);
                                }
                            }
                            //Fare summary
                            ServiceResponse response = base.FillSpecialService(true,
                                                                            objAgents[0].agency_code,
                                                                            bookingHeader,
                                                                            itinerary,
                                                                            passengers,
                                                                            quotes,
                                                                            mappings,
                                                                            taxes,
                                                                            fees,
                                                                            services,
                                                                            remarks);

                            if (response.Success == true)
                            {
                                return "SSRValidate{}" + ssrResult + "{}" + response.HTML + "{}";
                            }
                            else
                            {
                                return "{" + response.Code + "}";
                            }
                        }
                        #endregion
                        return ssrResult;
                    }
                }
                #endregion

                //Clear Insurance Fee....
                CalculateInsuranceFee(false, false);

                decimal dclOutStandingBalance = objLi.CalOutStandingBalance(quotes, fees, payments);

                if (!string.IsNullOrEmpty(B2CSetting.CreateACEPassengerSession))
                {
                    //Create temp passenger information for ACE Before Passenger objetc is sorted.
                    //ACE Required passenger inforamtion to be pass in exactly same order as input screen.
                    CreateACEPassengerSession(passengers);
                }

                //tikAERO-2010-0935 start
                bookingHeader.approval_flag = 1;
                //end
                Payments objPayment = new Payments();

                string result = string.Empty;

                try
                {
                    if (objLi.ValidSave(bookingHeader, itinerary, passengers, mappings) == true)
                    {
                        objPayment.objService = B2CSession.AgentService;

                        //Set update iniformation
                        SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);

                        Payments objPaymentsInput = new Payments();
                        Payment objPaymentInput;

                        using (StringReader std = new StringReader(xmlPayment))
                        {
                            XPathDocument xmlDoc = new XPathDocument(std);
                            XPathNavigator nv = xmlDoc.CreateNavigator();

                            foreach (XPathNavigator n in nv.Select("payments/payment[form_of_payment_rcd != 'CC']"))
                            {
                                objPaymentInput = new Payment();
                                if (XmlHelper.XpathValueNullToEmpty(n, "voucher_id").Length > 0)
                                {
                                    objPaymentInput.voucher_payment_id = XmlHelper.XpathValueNullToGUID(n, "voucher_id");
                                }
                                if (XmlHelper.XpathValueNullToEmpty(n, "voucher_number").Length > 0)
                                {
                                    objPaymentInput.document_number = XmlHelper.XpathValueNullToEmpty(n, "voucher_number");
                                }

                                objPaymentInput.form_of_payment_rcd = XmlHelper.XpathValueNullToEmpty(n, "form_of_payment_rcd");
                                objPaymentInput.form_of_payment_subtype_rcd = XmlHelper.XpathValueNullToEmpty(n, "form_of_payment_subtype_rcd");
                                objPaymentInput.payment_amount = objLi.ReadVoucherAmount(vouchers, objPaymentInput.voucher_payment_id);
                                objPaymentInput.fee_amount_incl = XmlHelper.XpathValueNullToDecimal(n, "fee_amount_incl");
                                objPaymentInput.fee_amount = XmlHelper.XpathValueNullToDecimal(n, "fee_amount");

                                objPaymentsInput.Add(objPaymentInput);

                                objPaymentInput = null;
                            }
                            //First Sort voucher payment from less amont to greater amount before Add credit payment.
                            objPaymentsInput.Sort("payment_amount", GenericComparer.SortOrderEnum.Ascending);

                            //Travel reason
                            foreach (XPathNavigator n in nv.Select("payments/payment[travelReason != '']"))
                            {
                                if (XmlHelper.XpathValueNullToEmpty(n, "travelReason").Length > 0)
                                {
                                    bookingHeader.business_flag = Convert.ToByte(XmlHelper.XpathValueNullToEmpty(n, "travelReason"));
                                }
                                if (XmlHelper.XpathValueNullToEmpty(n, "no_vat_flag").Length > 0)
                                {
                                    bookingHeader.no_vat_flag = Convert.ToByte(XmlHelper.XpathValueNullToEmpty(n, "no_vat_flag"));
                                }
                            }

                            //Filled credit card information.
                            foreach (XPathNavigator n in nv.Select("payments/payment[form_of_payment_rcd = 'CC']"))
                            {
                                objPaymentInput = new Payment();

                                objPaymentInput.name_on_card = XmlHelper.XpathValueNullToEmpty(n, "NameOnCard");
                                objPaymentInput.document_number = XmlHelper.XpathValueNullToEmpty(n, "CreditCardNumber");
                                objPaymentInput.form_of_payment_subtype_rcd = XmlHelper.XpathValueNullToEmpty(n, "form_of_payment_subtype_rcd");
                                objPaymentInput.form_of_payment_rcd = XmlHelper.XpathValueNullToEmpty(n, "form_of_payment_rcd");
                                objPaymentInput.fee_amount_incl = XmlHelper.XpathValueNullToDecimal(n, "fee_amount_incl");
                                objPaymentInput.fee_amount = XmlHelper.XpathValueNullToDecimal(n, "fee_amount");

                                if (XmlHelper.XpathValueNullToEmpty(n, "display_issue_date_flag") == "1")
                                {
                                    objPaymentInput.issue_month = XmlHelper.XpathValueNullToInt16(n, "IssueMonth");
                                    objPaymentInput.issue_year = XmlHelper.XpathValueNullToInt16(n, "IssueYear");
                                }

                                if (XmlHelper.XpathValueNullToEmpty(n, "display_issue_number_flag") == "1")
                                { objPaymentInput.issue_number = XmlHelper.XpathValueNullToEmpty(n, "IssueNumber"); }

                                if (XmlHelper.XpathValueNullToEmpty(n, "display_expiry_date_flag") == "1")
                                {
                                    objPaymentInput.expiry_month = XmlHelper.XpathValueNullToInt16(n, "ExpiryMonth");
                                    objPaymentInput.expiry_year = XmlHelper.XpathValueNullToInt16(n, "ExpiryYear");
                                }

                                if (XmlHelper.XpathValueNullToByte(n, "display_cvv_flag") == 1)
                                { objPaymentInput.cvv_code = XmlHelper.XpathValueNullToEmpty(n, "CVV"); }
                                if (XmlHelper.XpathValueNullToByte(n, "display_address_flag") == 1)
                                {
                                    objPaymentInput.address_line1 = XmlHelper.XpathValueNullToEmpty(n, "Addr1") + " " +
                                                                    XmlHelper.XpathValueNullToEmpty(n, "Addr2");
                                    objPaymentInput.street = XmlHelper.XpathValueNullToEmpty(n, "Street");
                                    objPaymentInput.state = XmlHelper.XpathValueNullToEmpty(n, "State");
                                    objPaymentInput.city = XmlHelper.XpathValueNullToEmpty(n, "City");
                                    objPaymentInput.zip_code = XmlHelper.XpathValueNullToEmpty(n, "ZipCode");
                                    objPaymentInput.country_rcd = XmlHelper.XpathValueNullToEmpty(n, "Country");
                                }

                                //ELV Section
                                objPaymentInput.bank_code = XmlHelper.XpathValueNullToEmpty(n, "bank_code");
                                objPaymentInput.bank_name = XmlHelper.XpathValueNullToEmpty(n, "bank_name");
                                objPaymentInput.bank_iban = XmlHelper.XpathValueNullToEmpty(n, "bank_iban");

                                //Amount
                                objPaymentInput.receive_currency_rcd = XmlHelper.XpathValueNullToEmpty(n, "exchange_currency");
                                objPaymentInput.receive_payment_amount = XmlHelper.XpathValueNullToZero(n, "exchange_fee_amount");
                                objPaymentInput.currency_rcd = bookingHeader.currency_rcd;
                                objPaymentInput.agency_code = bookingHeader.agency_code;
                                objPaymentInput.payment_amount = dclOutStandingBalance - TotalPayment(objPaymentsInput);
                                //Validate payment value.
                                if (objPaymentInput.name_on_card.Length == 0 ||
                                   objPaymentInput.document_number.Length == 0 ||
                                   objPaymentInput.form_of_payment_subtype_rcd.Length == 0 ||
                                   objPaymentInput.form_of_payment_rcd.Length == 0 ||
                                   objPaymentInput.currency_rcd.Length == 0 ||
                                   objPaymentInput.agency_code.Length == 0)
                                {
                                    result = "{704}";//Credit card information is invalid.
                                }
                                else
                                {
                                    objPaymentsInput.Add(objPaymentInput);
                                }
                                objPaymentInput = null;
                                break;
                            }
                        }

                        if (objPaymentsInput.Count > 0 && result.Length == 0)
                        {
                            if (TotalPayment(objPaymentsInput) >= dclOutStandingBalance)
                            {
                                result = objPayment.PaymentMultipleForm(ref bookingHeader,
                                                                       ref itinerary,
                                                                       ref passengers,
                                                                       ref quotes,
                                                                       ref fees,
                                                                       ref mappings,
                                                                       ref services,
                                                                       ref remarks,
                                                                       ref payments,
                                                                       ref taxes,
                                                                       objPaymentsInput,
                                                                       string.Empty,
                                                                       Classes.Language.CurrentCode().ToUpper(),
                                                                       objUv.UserId,
                                                                       objUv.ip_address,
                                                                       objUv.FormOfPaymentFee,
                                                                       objUv.Adult,
                                                                       objUv.Child,
                                                                       objUv.Infant,
                                                                       objUv.booking_payment_id);
                                if (result.Length > 0)
                                {
                                    XElement elements = XElement.Parse(result);
                                    if (elements != null)
                                    {
                                        foreach (XElement x in elements.XPathSelectElements("Fee[fee_rcd='PAYPAL']"))
                                            x.RemoveAll();

                                        result = elements.ToString();
                                    }

                                    XPathDocument xmlDoc = new XPathDocument(new StringReader(result));
                                    XPathNavigator nv = xmlDoc.CreateNavigator();

                                    bool bSaved = false;
                                    if (nv.Select("NewDataSet/Payments").Count > 0)
                                    {
                                        foreach (XPathNavigator n in nv.Select("NewDataSet/Payments"))
                                        {
                                            //Check Response Data.
                                            if (XmlHelper.XpathValueNullToEmpty(n, "ResponseCode").ToUpper() == "APPROVED")
                                            {
                                                bSaved = true;
                                            }
                                            else if (XmlHelper.XpathValueNullToEmpty(n, "ResponseCode") == "3D")
                                            {
                                                result = XmlHelper.XpathValueNullToEmpty(n, "ResponseCode") + "{}" +
                                                         XmlHelper.XpathValueNullToEmpty(n, "ResponseHtml");
                                            }
                                            else
                                            {
                                                if (n.SelectSingleNode("ErrorCode") == null)
                                                {
                                                    result = XmlHelper.XpathValueNullToEmpty(n, "ResponseCode") + "{}" +
                                                             XmlHelper.XpathValueNullToEmpty(n, "ResponseText");
                                                }
                                                else
                                                {
                                                    result = XmlHelper.XpathValueNullToEmpty(n, "ErrorCode") + "{}" +
                                                             XmlHelper.XpathValueNullToEmpty(n, "ResponseText");
                                                }

                                            }
                                        }
                                    }
                                    else if (nv.Select("ErrorResponse/Error").Count > 0)
                                    {
                                        result = XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/ErrorCode") + "{}" +
                                                 XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/Message");
                                    }
                                    else
                                    { bSaved = true; }

                                    //--------------------------------------------------------------------------------------------------------------
                                    if (bSaved == true)
                                    {
                                        // Find Insuarance ID And Save
                                        XPathDocument xmlDocPayment = new XPathDocument(new StringReader(xmlPayment));
                                        XPathNavigator nvPayment = xmlDocPayment.CreateNavigator();
                                        string productVariantID = "";
                                        if (nvPayment.Select("payment").Count > 0 && nvPayment.SelectSingleNode("payment/InsuranceID") != null
                                            && nvPayment.SelectSingleNode("payment/InsuranceID").InnerXml != "")
                                        {
                                            string strpassengers = XmlHelper.Serialize(passengers, false);
                                            productVariantID = nvPayment.SelectSingleNode("payment/InsuranceID").InnerXml;
                                            xmlPayment = xmlPayment.Replace("</payment>", nv.SelectSingleNode("Booking/BookingHeader/record_locator").OuterXml + "</payment>");
                                            //Save Insuarance
                                            InsuaranceSave(productVariantID, xmlPayment, strpassengers);
                                        }

                                        //Send passenger itinerary by email.
                                        objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                                        if (B2CSession.Client != null)
                                        {
                                            objUv.CurrentStep = 7;
                                        }
                                        else
                                        {
                                            objUv.CurrentStep = 1;
                                        }
                                        //Generate booking summary control.
                                        result = objLi.GenerateControlString("UserControls/BookingSummary.ascx", result);
                                        //Empty temp booking_payment_id
                                        objUv.booking_payment_id = Guid.Empty;
                                        B2CSession.RemoveBookingSession();
                                    }
                                }
                                else
                                {
                                    result = "{701}";
                                }
                            }
                            else
                            {
                                result = "{703}";//Payment not enough.
                            }
                        }
                    }
                    else
                    {
                        result = "{005}";
                    }

                }
                catch (Exception ex)
                {
                    objHp.SendErrorEmail(ex, "payment xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                    throw ex;
                }

                return result;
            }
        }
        [WebMethod(EnableSession = true)]
        public Double ExchangeRateRead(string strOriginCurrencyCode, string strDestCurrencyCode)
        {
            Double ret = 0D;
            try
            {
                ServiceClient SobjCient = new ServiceClient();
                SobjCient.objService = B2CSession.AgentService;
                ret = SobjCient.ExchangeRateRead(strOriginCurrencyCode, strDestCurrencyCode);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
            return ret;
        }
        [WebMethod(EnableSession = true)]
        public string LoadFormCreditCard(string strXml)
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/PaymentCreditCard.ascx", strXml);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "xml<br/>" + strXml.Replace("<", "&lt;").Replace(">", "&gt;"));
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string CalculateFee(string FeeRcd, bool renderControl)
        {

            Classes.Helper objHp = new Classes.Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library objLi = new Library();

                decimal dclFeeAmount = 0;
                decimal dclFeeAmountIncl = 0;

                if (bookingHeader == null || objHp.SessionTimeout() == true)
                {
                    //Calculate payment fee failed due to session timeout.
                    return "{002}";
                }
                else
                {
                    if (string.IsNullOrEmpty(FeeRcd) == false)
                    {
                        //Calclate fee
                        ServiceClient objCient = new ServiceClient();
                        string serviceFeesXml = string.Empty;

                        ClearFeeFromCode(FeeRcd, false);
                        objCient.objService = B2CSession.AgentService;

                        serviceFeesXml = objCient.AddFee(bookingHeader.booking_id.ToString(),
                                                         objUv.Agency_Code,
                                                         bookingHeader,
                                                         itinerary,
                                                         passengers,
                                                         FeeRcd,
                                                         fees,
                                                         bookingHeader.currency_rcd,
                                                         remarks,
                                                         payments,
                                                         mappings,
                                                         services,
                                                         taxes,
                                                         Classes.Language.CurrentCode(),
                                                         false);

                        if (string.IsNullOrEmpty(serviceFeesXml) == false)
                        {
                            fees.Clear();
                            fees.AddFees(serviceFeesXml, false);

                            //Find Fee Amount
                            for (int i = 0; i < fees.Count; i++)
                            {
                                if (fees[i].fee_rcd.Equals(FeeRcd))
                                {
                                    dclFeeAmount = fees[i].fee_amount;
                                    dclFeeAmountIncl = fees[i].fee_amount_incl;
                                    break;
                                }
                            }
                        }
                    }

                    if (renderControl == true)
                    {
                        //Show fare summary
                        StringBuilder stb = new StringBuilder();
                        System.Xml.Xsl.XslTransform objTransform = null;
                        System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                        objTransform = objHp.GetXSLDocument("FareSummary");

                        using (StringWriter stw = new StringWriter(stb))
                        {
                            using (XmlWriter xtw = XmlWriter.Create(stw))
                            {
                                xtw.WriteStartElement("Booking");
                                {
                                    xtw.WriteStartElement("setting");
                                    {
                                        xtw.WriteStartElement("booking_step");
                                        {
                                            xtw.WriteValue(objUv.CurrentStep);
                                        }
                                        xtw.WriteEndElement();
                                    }
                                    xtw.WriteEndElement();

                                    objLi.BuiltBookingXml(xtw, null, itinerary, passengers, quotes, fees, mappings, null, null, null, taxes);
                                }
                                xtw.WriteEndElement();
                            }
                        }
                        return objLi.RenderHtml(objTransform, objArgument, stb.ToString());
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, FeeRcd + "<br/>RenderControl<br/>" + renderControl.ToString());
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string ClearFeeFromCode(string FeeRcd, bool renderControl)
        {

            Classes.Helper objHp = new Classes.Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                Fees fees = B2CSession.Fees;
                Taxes taxes = B2CSession.Taxes;
                if (fees != null)
                {
                    if (FeeRcd.Length > 0)
                    {
                        //Find Fee Amount
                        for (int i = 0; i < fees.Count; i++)
                        {
                            if (fees[i].fee_rcd.Equals(FeeRcd))
                            {
                                fees.RemoveAt(i);
                                i--;
                            }
                        }
                    }

                    if (renderControl == true)
                    {
                        //Show fare summary
                        return GetSessionQuoteSummary();
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, FeeRcd + "<br/>RenderControl<br/>" + renderControl.ToString());
                return "{004}";
            }
        }
        #endregion

        #region Client
        [WebMethod(EnableSession = true)]
        public string ClientLogon(string userName, string password)
        {
            Classes.Helper objHp = new Classes.Helper();

            try
            {
                bool isMyself = false;
                //Initilaize webservice.
                if (B2CSession.AgentService == null)
                {
                    B2CVariable objUv = B2CSession.Variable;
                    if (objUv == null)
                    {
                        objUv = new B2CVariable();
                    }
                    objHp.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;
                }

                if (B2CSession.AgentService == null)
                {
                    return "{200}";
                }
                else
                {
                    ServiceClient obj = new ServiceClient();
                    obj.objService = B2CSession.AgentService;
                    DataSet ds = obj.ClientLogon(userName, password);

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        string xmlClient = "";
                        Client objClient = null;
                        if (ConfigurationManager.ConnectionStrings["Conn"] == null)
                        {
                            DataSet dsClient = obj.ClientRead(ds.Tables[0].Rows[0]["client_profile_id"].ToString());
                            ds.Dispose();

                            xmlClient = new Helper().ConvertDataTableToXML(dsClient.Tables["Client"]);
                            dsClient.Dispose();

                            xmlClient = xmlClient.Replace("<NewDataSet>", "");
                            xmlClient = xmlClient.Replace("</NewDataSet>", "");
                            objClient = (Client)XmlHelper.Deserialize(xmlClient, typeof(Client));

                            if (dsClient != null && dsClient.Tables.Count > 0)
                            {
                                for (int i = 0; i < dsClient.Tables["Passenger"].Rows.Count; i++)
                                {
                                    DataRow rw = dsClient.Tables["Passenger"].Rows[i];
                                    if (rw["passenger_role_rcd"].ToString().Equals("MYSELF") == true)
                                    {
                                        isMyself = true;
                                        if (rw["date_of_birth"] != null && rw["date_of_birth"].ToString().Length > 0)
                                        {
                                            objClient.date_of_birth = Convert.ToDateTime(rw["date_of_birth"]);
                                        }
                                        if (rw["vip_flag"] != null && rw["vip_flag"] != DBNull.Value)
                                        {
                                            objClient.vip_flag = Convert.ToByte(rw["vip_flag"]);
                                        }
                                        if (rw["passenger_profile_id"] != null && rw["passenger_profile_id"] != DBNull.Value)
                                        {
                                            objClient.passenger_profile_id = new Guid(rw["passenger_profile_id"].ToString());
                                        }
                                        if (rw["nationality_rcd"] != null && rw["nationality_rcd"] != DBNull.Value)
                                        {
                                            objClient.nationality_rcd = rw["nationality_rcd"].ToString();
                                        }
                                        if (rw["document_type_rcd"] != null && rw["document_type_rcd"] != DBNull.Value)
                                        {
                                            objClient.document_type_rcd = rw["document_type_rcd"].ToString();
                                        }
                                        if (rw["passport_number"] != null && rw["passport_number"] != DBNull.Value)
                                        {
                                            objClient.passport_number = rw["passport_number"].ToString();
                                        }
                                        if (rw["passport_issue_place"] != null && rw["passport_issue_place"] != DBNull.Value)
                                        {
                                            objClient.passport_issue_place = rw["passport_issue_place"].ToString();
                                        }
                                        if (rw["passport_issue_date"] != null && rw["passport_issue_date"].ToString().Length > 0)
                                        {
                                            objClient.passport_issue_date = Convert.ToDateTime(rw["passport_issue_date"]);
                                        }
                                        if (rw["passport_expiry_date"] != null && rw["passport_expiry_date"].ToString().Length > 0)
                                        {
                                            objClient.passport_expiry_date = Convert.ToDateTime(rw["passport_expiry_date"]);
                                        }
                                        if (rw["passenger_weight"] != null && rw["passenger_weight"].ToString().Length > 0)
                                        {
                                            objClient.passenger_weight = Convert.ToDecimal(rw["passenger_weight"]);
                                        }
                                        if (rw["gender_type_rcd"] != null && rw["gender_type_rcd"].ToString().Length > 0)
                                        {
                                            objClient.gender_type_rcd = rw["gender_type_rcd"].ToString();
                                        }
                                        if (rw["passport_birth_place"] != null && rw["passport_birth_place"].ToString().Length > 0)
                                        {
                                            objClient.passport_birth_place = rw["passport_birth_place"].ToString();
                                        }
                                        if (rw["passport_issue_country_rcd"] != null && rw["passport_issue_country_rcd"].ToString().Length > 0)
                                        {
                                            objClient.passport_issue_country_rcd = rw["passport_issue_country_rcd"].ToString();
                                        }
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                return "{201}";
                            }
                        }
                        else
                        {
                            DataTable tblClent = null;
                            DataTable tblPassenger = null;
                            try
                            {
                                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString))
                                {
                                    using (SqlCommand cmd = con.CreateCommand())
                                    {
                                        cmd.CommandType = CommandType.Text;
                                        cmd.CommandText = "SELECT * FROM nl_client_profile WHERE client_profile_id = @clientID";
                                        cmd.Parameters.Add(new SqlParameter("@clientID", ds.Tables[0].Rows[0]["client_profile_id"].ToString()));

                                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                                        {
                                            tblClent = new DataTable("Client");
                                            da.Fill(tblClent);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                //exception handling
                            }



                            xmlClient = new Helper().ConvertDataTableToXML(tblClent);

                            xmlClient = xmlClient.Replace("<NewDataSet>", "");
                            xmlClient = xmlClient.Replace("</NewDataSet>", "");
                            objClient = (Client)XmlHelper.Deserialize(xmlClient, typeof(Client));

                            if (tblClent.Rows.Count > 0)
                            {
                                try
                                {
                                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString))
                                    {
                                        using (SqlCommand cmd = con.CreateCommand())
                                        {
                                            cmd.CommandType = CommandType.Text;
                                            cmd.CommandText = "SELECT p.title_rcd, p.firstname, p.lastname, p.passenger_role_rcd, p.date_of_birth, p.passenger_type_rcd, p.passport_number, p.passport_expiry_date, p.nationality_rcd, p.vip_flag, p.passenger_profile_id, p.document_type_rcd, p.passport_issue_place, p.passenger_weight, p.gender_type_rcd, p.passport_birth_place, cp.member_level_rcd, cp.member_number, p.passport_issue_date, p.passport_issue_country_rcd, cp.country_rcd, cp.client_number ";
                                            cmd.CommandText += "FROM passenger_profile p WITH(NOLOCK) ";
                                            cmd.CommandText += "INNER JOIN client_profile cp WITH(NOLOCK) ON p.client_profile_id = cp.client_profile_id ";
                                            cmd.CommandText += "WHERE p.client_profile_id = @clientID";
                                            cmd.Parameters.Add(new SqlParameter("@clientID", ds.Tables[0].Rows[0]["client_profile_id"].ToString()));

                                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                                            {
                                                tblPassenger = new DataTable("Passenger");
                                                da.Fill(tblPassenger);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //exception handling
                                }

                                for (int i = 0; i < tblPassenger.Rows.Count; i++)
                                {
                                    DataRow rw = tblPassenger.Rows[i];
                                    if (rw["passenger_role_rcd"].ToString().Equals("MYSELF") == true)
                                    {
                                        isMyself = true;
                                        if (rw["date_of_birth"] != null && rw["date_of_birth"].ToString().Length > 0)
                                        {
                                            objClient.date_of_birth = Convert.ToDateTime(rw["date_of_birth"]);
                                        }
                                        if (rw["vip_flag"] != null && rw["vip_flag"] != DBNull.Value)
                                        {
                                            objClient.vip_flag = Convert.ToByte(rw["vip_flag"]);
                                        }
                                        if (rw["passenger_profile_id"] != null && rw["passenger_profile_id"] != DBNull.Value)
                                        {
                                            objClient.passenger_profile_id = new Guid(rw["passenger_profile_id"].ToString());
                                        }
                                        if (rw["nationality_rcd"] != null && rw["nationality_rcd"] != DBNull.Value)
                                        {
                                            objClient.nationality_rcd = rw["nationality_rcd"].ToString();
                                        }
                                        if (rw["document_type_rcd"] != null && rw["document_type_rcd"] != DBNull.Value)
                                        {
                                            objClient.document_type_rcd = rw["document_type_rcd"].ToString();
                                        }
                                        if (rw["passport_number"] != null && rw["passport_number"] != DBNull.Value)
                                        {
                                            objClient.passport_number = rw["passport_number"].ToString();
                                        }
                                        if (rw["passport_issue_place"] != null && rw["passport_issue_place"] != DBNull.Value)
                                        {
                                            objClient.passport_issue_place = rw["passport_issue_place"].ToString();
                                        }
                                        if (rw["passport_issue_date"] != null && rw["passport_issue_date"].ToString().Length > 0)
                                        {
                                            objClient.passport_issue_date = Convert.ToDateTime(rw["passport_issue_date"]);
                                        }
                                        if (rw["passport_expiry_date"] != null && rw["passport_expiry_date"].ToString().Length > 0)
                                        {
                                            objClient.passport_expiry_date = Convert.ToDateTime(rw["passport_expiry_date"]);
                                        }
                                        if (rw["passenger_weight"] != null && rw["passenger_weight"].ToString().Length > 0)
                                        {
                                            objClient.passenger_weight = Convert.ToDecimal(rw["passenger_weight"]);
                                        }
                                        if (rw["gender_type_rcd"] != null && rw["gender_type_rcd"].ToString().Length > 0)
                                        {
                                            objClient.gender_type_rcd = rw["gender_type_rcd"].ToString();
                                        }
                                        if (rw["passport_birth_place"] != null && rw["passport_birth_place"].ToString().Length > 0)
                                        {
                                            objClient.passport_birth_place = rw["passport_birth_place"].ToString();
                                        }
                                        if (rw["passport_issue_country_rcd"] != null && rw["passport_issue_country_rcd"].ToString().Length > 0)
                                        {
                                            objClient.passport_issue_country_rcd = rw["passport_issue_country_rcd"].ToString();
                                        }
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                return "{201}";
                            }
                        }

                        // Validate client don't have my self role
                        if (!isMyself)
                            return "{200}"; //"Error,Can't Logon.";
                        else
                        {
                            B2CSession.Client = objClient;
                            B2CSession.Variable.CurrentStep = 1;
                            return "{000}";
                        }
                    }
                    else
                    {
                        return "{200}"; //"Error,Can't Logon.";
                    }
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, "username<br/>" + userName);
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string ClientLoad(string clientProfileId)
        {
            B2CVariable objUv = B2CSession.Variable;
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                if (B2CSession.AgentService == null)
                {
                    if (objUv == null)
                    {
                        objUv = new B2CVariable();
                    }
                    objHp.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;
                }

                Agents objAgent = B2CSession.Agents;
                if (B2CSession.AgentService == null)
                {
                    return "{403}";
                }
                else
                {
                    ServiceClient obj = new ServiceClient();
                    obj.objService = B2CSession.AgentService;

                    string xmlClient = "";
                    DataSet dsClient = obj.ClientRead(clientProfileId);

                    xmlClient = new Helper().ConvertDataTableToXML(dsClient.Tables["Client"]);
                    xmlClient = xmlClient.Replace("<NewDataSet>", "");
                    xmlClient = xmlClient.Replace("</NewDataSet>", "");

                    Client objClient = (Client)XmlHelper.Deserialize(xmlClient, typeof(Client));
                    foreach (DataRow rw in dsClient.Tables["Passenger"].Select("passenger_role_rcd = 'MYSELF'"))
                    {
                        if (rw["date_of_birth"] != null && rw["date_of_birth"].ToString().Length > 0)
                        {
                            objClient.date_of_birth = Convert.ToDateTime(rw["date_of_birth"]);
                        }
                    }
                    B2CSession.Client = objClient;

                    dsClient.Dispose();
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    objTransform = objHp.GetXSLDocument("ClientInformation");
                    Library objLi = new Library();
                    return objLi.RenderHtml(objTransform,
                                            objArgument,
                                            XmlHelper.Serialize(objClient, false));
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, "client profile id<br/>" + clientProfileId);
                return "{403}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadMilleageDetail()
        {
            try
            {
                string strResult = string.Empty;
                B2CVariable objUv = B2CSession.Variable;
                objUv.CurrentStep = 8;

                Library objLi = new Library();
                strResult = objLi.GenerateControlString("UserControls/MilleageDetail.ascx", "");
                return strResult;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public bool ClientLogOff()
        {
            try
            {
                Session.Remove("AgentService");
                Session.Remove("Client");
                string strResult = string.Empty;
                B2CVariable objUv = B2CSession.Variable;
                Library objLi = new Library();
                if (objUv != null)
                {
                    //Clear Gateway value
                    objUv.PaymentGateway = string.Empty;
                    objUv.CurrentStep = 1;
                }

                return true;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadClientDialog()
        {
            try
            {
                string strResult = string.Empty;

                Library objLi = new Library();
                strResult = objLi.GenerateControlString("UserControls/ClientLogon.ascx", "Dialog");

                return strResult;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string AccuralQuote()
        {
            Helper objXsl = new Helper();
            B2CVariable objUv = B2CSession.Variable;
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                if (objUv == null || objXsl.SessionTimeout() == true)
                {
                    return "{002}";
                }
                else
                {

                    BookingHeader bookingHeader = B2CSession.BookingHeader;
                    Passengers passengers = B2CSession.Passengers;
                    Mappings mappings = B2CSession.Mappings;

                    if (ClientFound(passengers) == true && objUv.SearchType != "POINT")
                    {
                        Library objLi = new Library();
                        Clients objClient = new Clients();
                        objClient.objService = B2CSession.AgentService;
                        DataSet ds = objClient.AccuralQuote(passengers, mappings, bookingHeader.client_profile_id.ToString());

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            string serverPath = Server.MapPath("~") + @"\xsl\";
                            System.Xml.Xsl.XslTransform objTransform = null;
                            System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                            objTransform = objXsl.GetXSLDocument("AccuralQuote");
                            return objLi.RenderHtml(objTransform, objArgument, ds.GetXml());
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    return string.Empty;
                }

            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string ShowClientLogon()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/ClientLogon.ascx", string.Empty);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public bool IsClientLogon()
        {
            try
            {
                Client client = B2CSession.Client;
                if (client != null && client.client_number > 0)
                { return true; }
                else
                { return false; }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string LoadClientInformation()
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                if (B2CSession.Client != null)
                {
                    Library objLi = new Library();
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    objTransform = objHp.GetXSLDocument("ClientInformation");
                    return objLi.RenderHtml(objTransform,
                                            objArgument,
                                            XmlHelper.Serialize(B2CSession.Client, false));
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
            return string.Empty;
        }
        [WebMethod(EnableSession = true)]
        public string LoadClientLogon()
        {
            try
            {
                Library li = new Library();
                return li.GenerateControlString("UserControls/ClientLogon.ascx", string.Empty);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        #endregion

        #region Booking
        [WebMethod(EnableSession = true)]
        public string LoadBookingDetail(string bookingId)
        {
            try
            {
                string strResult = string.Empty;
                if (B2CSession.Client != null)
                {
                    ServiceClient obj = new ServiceClient();
                    obj.objService = B2CSession.AgentService;
                    string itinerary = obj.GetItinerary(bookingId, Classes.Language.CurrentCode().ToUpper(), string.Empty, string.Empty);

                    //Formatting XML                   
                    B2CVariable objUv = B2CSession.Variable;

                    if (objUv.CurrentStep == 8)
                    {
                        //From My Milleage
                        itinerary = itinerary.Replace("</Booking>", "<Source>MM</Source>" + "</Booking>");
                    }
                    objUv.CurrentStep = 13;

                    BookingHeader bookingHeader = B2CSession.BookingHeader;
                    if (bookingHeader == null)
                    {
                        bookingHeader = new BookingHeader();
                        B2CSession.BookingHeader = bookingHeader;
                    }

                    bookingHeader.booking_id = new Guid(bookingId);

                    Library objLi = new Library();
                    strResult = objLi.GenerateControlString("UserControls/BookingSummary.ascx", itinerary);
                }
                else
                    strResult = "{002}";

                return strResult;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadMyBooking()
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                if (objUv == null)
                {
                    return "{002}";
                }
                else
                {
                    objUv.CurrentStep = 7;

                    Library objLi = new Library();
                    return objLi.GenerateControlString("UserControls/MyBooking.ascx", string.Empty);
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                return "{004}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string MyBookingSearch(string bookingCode)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                if (B2CSession.Client != null)
                {
                    string strResult = GetBookingsList(bookingCode);

                    Library objLi = new Library();
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    objTransform = objXsl.GetXSLDocument("BookingList");
                    return objLi.RenderHtml(objTransform,
                                            objArgument,
                                            FilterByClientID(strResult, B2CSession.Client.client_profile_id.ToString()));
                }
                else
                    return "{002}";
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string MyBookingHistorySearch(string bookingCode)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                if (B2CSession.Client != null)
                {
                    string strResult = GetBookingsList(bookingCode);

                    Library objLi = new Library();
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    objTransform = objXsl.GetXSLDocument("BookingHistory");
                    return objLi.RenderHtml(objTransform,
                                            objArgument,
                                            FilterByClientID(strResult, B2CSession.Client.client_profile_id.ToString()));
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GridPagingWithXML(string xml, int rowPerPage, int selectpage, bool includepaging)
        {
            try
            {
                Library objLi = new Library();
                if (xml != "")
                {
                    System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
                    xdoc.LoadXml(xml);

                    StringBuilder strnxml = new StringBuilder("");
                    string subhead = "";


                    int nnd = xdoc.ChildNodes.Count;
                    int totalnode = xdoc.ChildNodes[0].ChildNodes.Count;
                    for (int i = (selectpage * rowPerPage); (i < rowPerPage * (selectpage + 1)); i++)
                    {
                        if (i < totalnode)
                        {
                            subhead = xdoc.ChildNodes[0].ChildNodes[i].Name;
                            strnxml.Append("<" + subhead + ">" + "<id>" + Convert.ToString("" + (i + 1)).PadLeft(3, '0') + "</id>" + xdoc.ChildNodes[0].ChildNodes[i].InnerXml + "</" + subhead + ">");
                        }
                    }
                    strnxml = strnxml.Replace("\r\n", "");
                    string head = xdoc.DocumentElement.Name;
                    string ret = "<" + head + ">" + strnxml.ToString() + "</" + head + ">";
                    string genindex = "";

                    if (includepaging)
                    {
                        int amoutpage = (int)(totalnode / rowPerPage);
                        if (((totalnode % rowPerPage) != 0) && (totalnode > rowPerPage)) amoutpage++;
                        for (int index = 0; index < amoutpage; index++)
                        {
                            genindex += "<pageindex>" + ((int)(index + 1)).ToString() + "</pageindex>";
                        }
                        genindex += "<selected>" + ((int)(selectpage + 1)).ToString() + "</selected>";
                    }
                    else
                    {
                        if (rowPerPage == 0)
                        {
                            ret = xml;
                        }
                    }
                    genindex = "<object>" + ret + "<paging>" + genindex + "</paging>" + "</object>";

                    return genindex;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "XML Input<br/>" + xml.Replace("<", "&lt;").Replace(">", "&gt;") +
                                         "<br/>RowPage<br/>" + rowPerPage +
                                         "<br/>Select Page<br/>" + selectpage +
                                         "<br/>IncludePage<br/>" + includepaging);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPagingBooking(string pageIndex, string isHistory)
        {
            Helper objXsl = new Helper();

            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                Library objLi = new Library();
                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                int rowPerPage = B2CSetting.RowsPerPageBooking;
                bool usePaging = B2CSetting.UsePaging;

                if (isHistory == "true")
                {
                    string xmlHistory = B2CSession.xmlHistory;
                    if (Convert.ToInt32(pageIndex) > 0)
                    {
                        xmlHistory = GridPagingWithXML(xmlHistory, rowPerPage, Convert.ToInt32(pageIndex) - 1, usePaging);
                    }

                    if (!string.IsNullOrEmpty(xmlHistory))
                    {
                        objTransform = objXsl.GetXSLDocument("BookingHistory");
                        return objLi.RenderHtml(objTransform, objArgument, xmlHistory);
                    }
                    else
                    {
                        return "{002}";
                    }
                }
                else
                {
                    string xmlLife = B2CSession.xmlLife;
                    if (Convert.ToInt32(pageIndex) > 0)
                    {
                        xmlLife = GridPagingWithXML(xmlLife, rowPerPage, Convert.ToInt32(pageIndex) - 1, usePaging);
                    }

                    if (!string.IsNullOrEmpty(xmlLife))
                    {
                        objTransform = objXsl.GetXSLDocument("BookingList");
                        return objLi.RenderHtml(objTransform, objArgument, xmlLife);
                    }
                    else
                    {
                        return "{002}";
                    }
                }
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Page Index<br/>" + pageIndex + "<br/>IsHistory<br/>" + isHistory.ToString());
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPagingFFP(string pageIndex)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                Library objLi = new Library();
                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                int rowPerPage = B2CSetting.RowsPerPageBooking;

                string FFPXml = B2CSession.xmlFFP;
                FFPXml = GridPagingWithXML(FFPXml, rowPerPage, Convert.ToInt32(pageIndex) - 1, true);

                objTransform = objXsl.GetXSLDocument("FFPList");
                return objLi.RenderHtml(objTransform, objArgument, FFPXml);
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Page Index<br/>" + pageIndex);
                throw ex;
            }
        }
        #endregion

        #region ForgetPassword

        [WebMethod(EnableSession = true)]
        public string LoadForgetPassword()
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                B2CVariable objUv = B2CSession.Variable;

                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library objLi = new Library();

                if (B2CSession.AgentService == null)
                {
                    if (objUv == null)
                    {
                        objUv = new B2CVariable();
                    }
                    objHp.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;
                }

                Agents objAgents = B2CSession.Agents;
                //Check if initialize web service is success.
                if (B2CSession.AgentService == null)
                {
                    //Load forget password failed.
                    return "{203}";
                }
                else
                {
                    //Clear temp availability
                    if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                    {
                        //Clear block inventory
                        ServiceClient objSvc = new ServiceClient();
                        objSvc.objService = B2CSession.AgentService;
                        objSvc.ReleaseFlightInventorySession(bookingHeader.booking_id.ToString(), string.Empty, string.Empty, string.Empty, false, true, true);
                        objSvc.objService = null;
                        objSvc = null;
                        //Clear booking in session
                        objLi.ClearBooking(ref bookingHeader,
                                            ref itinerary,
                                            ref passengers,
                                            ref quotes,
                                            ref fees,
                                            ref mappings,
                                            ref services,
                                            ref remarks,
                                            ref payments,
                                            ref taxes);
                    }

                    objUv.CurrentStep = 9;
                    return objLi.GenerateControlString("UserControls/ForgetPassword.ascx", "");
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, string.Empty);
                //Load forget password failed.
                return "{203}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string ForgetPassword(string clientNumber, string url)
        {
            Helper objHelp = new Helper();
            try
            {
                ServiceClient obj = new ServiceClient();
                Library objLi = new Library();
                B2CVariable objUv = B2CSession.Variable;

                bool success = false;
                string randomPassword = "";

                if (B2CSession.AgentService == null)
                {
                    if (objUv == null)
                    {
                        objUv = new B2CVariable();
                    }
                    objHelp.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;
                }

                Agents objAgents = B2CSession.Agents;
                if (B2CSession.AgentService == null ||
                    objAgents == null ||
                    objAgents.Count == 0)
                {
                    return "101";
                }
                else
                {
                    obj.objService = B2CSession.AgentService;

                    string xmlClient = obj.GetClient("", clientNumber, "", true);

                    xmlClient = xmlClient.Replace("<Booking>", "");
                    xmlClient = xmlClient.Replace("</Booking>", "");

                    Client objclient = (Client)XmlHelper.Deserialize(xmlClient, typeof(Client));
                    if (objclient != null)
                    {
                        DataSet ds = obj.ClientRead(objclient.client_profile_id.ToString());

                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            randomPassword = GeneratePassword();
                            ds.Tables["Client"].Rows[0]["client_password"] = randomPassword;
                            xmlClient = XmlHelper.Serialize(objHelp.ConvertDataTableObject(ds.Tables["Client"], typeof(Client), false), true);

                            string xmlPassenger = XmlHelper.Serialize(objHelp.ConvertDataTableObject(ds.Tables["Passenger"], typeof(Passengers), true), true);
                            string xmlRemark = XmlHelper.Serialize(objHelp.ConvertDataTableObject(ds.Tables["Remark"], typeof(Remarks), true), true);

                            objclient.update_by = objUv.UserId;

                            success = obj.ClientSave(xmlClient, xmlPassenger, xmlRemark);

                            if (success & url != "")
                                SendMaintainMail(objclient.contact_email,
                                                objclient.title_rcd,
                                                objclient.firstname,
                                                objclient.lastname,
                                                objclient.client_number.ToString(),
                                                "",
                                                "reset",
                                                url.Substring(0, url.LastIndexOf('/')),
                                                randomPassword,
                                                objUv.UserId);
                        }
                    }
                    else
                        return "100"; //Error,Client id isn't valid

                    if (success)
                    {
                        objUv.CurrentStep = 1;
                        return "000"; //Password has reset.
                    }
                    else
                        return "101"; //Error,Cann't reset password.
                }
            }
            catch (Exception ex)
            {
                objHelp.SendErrorEmail(ex, "Client Number<br/>" + clientNumber + "<br/>Url<br/>" + url);
                return "101";
            }
        }
        #endregion

        #region Passenger Detail Helper
        private bool ValidContactInput(XPathNavigator nv)
        {
            XPathNodeIterator ni = nv.Select("booking/contact");
            if (ni.Count > 0)
            {
                foreach (XPathNavigator n in ni)
                {
                    string[] strTitle = XmlHelper.XpathValueNullToEmpty(n, "title").Split('|');
                    if (strTitle != null && strTitle.Length == 2)
                    {
                        if (string.IsNullOrEmpty(XmlHelper.XpathValueNullToEmpty(n, "lastname")) || string.IsNullOrEmpty(XmlHelper.XpathValueNullToEmpty(n, "firstname")))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return true;
            }
        }
        private bool ValidPassengerInput(XPathNavigator nv)
        {
            XPathNodeIterator ni = nv.Select("booking/passenger");
            if (ni.Count > 0)
            {
                foreach (XPathNavigator n in ni)
                {
                    string[] strTitle = XmlHelper.XpathValueNullToEmpty(n, "title_rcd").Split('|');
                    if (strTitle != null && strTitle.Length == 2)
                    {
                        if (string.IsNullOrEmpty(XmlHelper.XpathValueNullToEmpty(n, "lastname")))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return true;
            }
        }

        private void FillContactInfo(BookingHeader header, string xml)
        {
            XElement elements = XElement.Parse(xml);
            if (elements != null)
            {
                elements = elements.XPathSelectElement("contact");
                header = XmlHelper.BindObject(header, elements) as BookingHeader;
                string[] arrtitle = elements.Element("title").Value.Split('|');
                header.title_rcd = arrtitle[0];
                header.contact_name = string.Format("{0} {1}", header.firstname, header.lastname);
                if (header.booking_date_time == DateTime.MinValue)
                {
                    header.booking_date_time = DateTime.Now;
                }
            }
        }

        private void FillContactInfo(BookingHeader bookingHeader, XPathNavigator nv)
        {
            try
            {
                string[] strTitle;
                foreach (XPathNavigator n in nv.Select("booking/contact"))
                {
                    strTitle = XmlHelper.XpathValueNullToEmpty(n, "title").Split('|');
                    bookingHeader.title_rcd = strTitle[0];
                    bookingHeader.firstname = XmlHelper.XpathValueNullToEmpty(n, "firstname");
                    bookingHeader.middlename = XmlHelper.XpathValueNullToEmpty(n, "middlename");
                    bookingHeader.lastname = XmlHelper.XpathValueNullToEmpty(n, "lastname");
                    bookingHeader.client_number = XmlHelper.XpathValueNullToInt(n, "client_number");
                    bookingHeader.client_profile_id = XmlHelper.XpathValueNullToGUID(n, "client_profile_id");
                    bookingHeader.phone_mobile = XmlHelper.XpathValueNullToEmpty(n, "mobile");
                    bookingHeader.phone_home = XmlHelper.XpathValueNullToEmpty(n, "home");
                    bookingHeader.phone_business = XmlHelper.XpathValueNullToEmpty(n, "business");
                    bookingHeader.contact_email = XmlHelper.XpathValueNullToEmpty(n, "email");
                    bookingHeader.address_line1 = XmlHelper.XpathValueNullToEmpty(n, "addresss_1");
                    bookingHeader.address_line2 = XmlHelper.XpathValueNullToEmpty(n, "addresss_2");
                    bookingHeader.street = XmlHelper.XpathValueNullToEmpty(n, "street");
                    bookingHeader.zip_code = XmlHelper.XpathValueNullToEmpty(n, "zip");
                    bookingHeader.city = XmlHelper.XpathValueNullToEmpty(n, "city");
                    bookingHeader.state = XmlHelper.XpathValueNullToEmpty(n, "state");
                    bookingHeader.country_rcd = XmlHelper.XpathValueNullToEmpty(n, "country");
                    bookingHeader.language_rcd = XmlHelper.XpathValueNullToEmpty(n, "language");
                    bookingHeader.contact_name = bookingHeader.firstname + " " + bookingHeader.lastname;

                    bookingHeader.invoice_receiver = XmlHelper.XpathValueNullToEmpty(n, "invoice_receiver");
                    bookingHeader.cost_center = XmlHelper.XpathValueNullToEmpty(n, "cost_center");
                    bookingHeader.project_number = XmlHelper.XpathValueNullToEmpty(n, "project_number");
                    bookingHeader.purchase_order = XmlHelper.XpathValueNullToEmpty(n, "purchase_order");
                    bookingHeader.tax_id = XmlHelper.XpathValueNullToEmpty(n, "tax_id");
                    bookingHeader.newsletter_flag = XmlHelper.XpathValueNullToByte(n, "newsletter_flag");
                    bookingHeader.contact_email_cc = XmlHelper.XpathValueNullToEmpty(n, "contact_email_cc");
                    bookingHeader.mobile_email = XmlHelper.XpathValueNullToEmpty(n, "mobile_email");
                    bookingHeader.booking_date_time = XmlHelper.XpathValueNullToDateTime(n, "booking_date_time");
                    bookingHeader.company_name = XmlHelper.XpathValueNullToEmpty(n, "company_name");

                    if (bookingHeader.booking_date_time == DateTime.MinValue)
                    {
                        bookingHeader.booking_date_time = DateTime.Now;
                    }
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                LogsManagement.SaveErrorLogs(ex, nv.InnerXml);
                if (nv == null || bookingHeader == null)
                { objHp.SendErrorEmail(ex, "Object Is Null"); }
                else
                { objHp.SendErrorEmail(ex, "Xml<br/>" + nv.OuterXml.Replace("<", "&lt;").Replace(">", "&gt;")); }
            }
        }
        private void FillPassengerDetail(Passengers passengers, Mappings mappings, XPathNavigator nv)
        {
            try
            {
                for (int i = 0; i < passengers.Count; i++)
                {
                    Passenger pax = passengers[i];
                    foreach (XPathNavigator n in nv.Select("booking/passenger[passenger_id = '" + pax.passenger_id.ToString() + "']"))
                    {
                        string strTempDateValue = string.Empty;
                        string[] strTitle = XmlHelper.XpathValueNullToEmpty(n, "title_rcd").Split('|');
                        pax.title_rcd = strTitle[0].ToUpper();
                        pax.gender_type_rcd = strTitle[1];

                        pax.firstname = XmlHelper.XpathValueNullToEmpty(n, "firstname").ToUpper();
                        pax.middlename = XmlHelper.XpathValueNullToEmpty(n, "middlename").ToUpper();
                        pax.lastname = XmlHelper.XpathValueNullToEmpty(n, "lastname").ToUpper();

                        pax.document_type_rcd = XmlHelper.XpathValueNullToEmpty(n, "document_type_rcd");
                        pax.nationality_rcd = XmlHelper.XpathValueNullToEmpty(n, "nationality_rcd");
                        pax.passport_number = XmlHelper.XpathValueNullToEmpty(n, "document_no");
                        pax.passport_issue_place = XmlHelper.XpathValueNullToEmpty(n, "issue_place");

                        strTempDateValue = XmlHelper.XpathValueNullToEmpty(n, "issue_date");
                        pax.passport_issue_date = (strTempDateValue.Length > 0) ? Convert.ToDateTime(strTempDateValue) : DateTime.MinValue;

                        strTempDateValue = XmlHelper.XpathValueNullToEmpty(n, "expiry_date");
                        pax.passport_expiry_date = (strTempDateValue.Length > 0) ? Convert.ToDateTime(strTempDateValue) : DateTime.MinValue;

                        strTempDateValue = XmlHelper.XpathValueNullToEmpty(n, "date_of_birth");
                        pax.date_of_birth = (strTempDateValue.Length > 0) ? Convert.ToDateTime(strTempDateValue) : DateTime.MinValue;

                        pax.passenger_weight = XmlHelper.XpathValueNullToZero(n, "passenger_weight");
                        pax.passport_birth_place = XmlHelper.XpathValueNullToEmpty(n, "passport_birth_place");
                        pax.vip_flag = XmlHelper.XpathValueNullToByte(n, "vip_flag");
                        pax.member_number = XmlHelper.XpathValueNullToEmpty(n, "member_number");
                        pax.redress_number = XmlHelper.XpathValueNullToEmpty(n, "redress_number");
                        pax.passport_issue_country_rcd = XmlHelper.XpathValueNullToEmpty(n, "passport_issue_country_rcd");

                        //Fill client profile information.
                        pax.passenger_profile_id = XmlHelper.XpathValueNullToGUID(n, "passenger_profile_id");
                        pax.client_number = Convert.ToInt64(0 + XmlHelper.XpathValueNullToEmpty(n, "client_no"));
                        pax.member_level_rcd = XmlHelper.XpathValueNullToEmpty(n, "member_level_rcd");

                        if (pax.passenger_profile_id.Equals(Guid.Empty) == false)
                        {
                            pax.client_profile_id = XmlHelper.XpathValueNullToGUID(n, "client_profile_id");
                        }
                    }

                    //Fill mapping information
                    foreach (Mapping mp in mappings)
                    {
                        if (mp.passenger_id.Equals(pax.passenger_id))
                        {
                            mp.firstname = pax.firstname;
                            mp.lastname = pax.lastname;
                            mp.middlename = pax.middlename;
                            mp.title_rcd = pax.title_rcd;
                            mp.date_of_birth = pax.date_of_birth;
                            mp.gender_type_rcd = pax.gender_type_rcd;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                LogsManagement.SaveErrorLogs(ex, nv.InnerXml);
                if (nv == null || passengers == null)
                    objHp.SendErrorEmail(ex, "Object Is Null");
                else
                    objHp.SendErrorEmail(ex, "Xml<br/>" + nv.OuterXml.Replace("<", "&lt;").Replace(">", "&gt;"));
            }
        }
        private void FillRemark(Guid bookingId,
                                Guid clientProfileId,
                                string agencyCode,
                                XPathNavigator nv)
        {
            try
            {
                XPathNodeIterator interator = nv.Select("booking/remark");
                if (interator != null)
                {
                    if (interator.Count > 0)
                    {
                        Remarks remarks = B2CSession.Remarks;
                        if (remarks == null)
                        {
                            remarks = new Remarks();
                            B2CSession.Remarks = remarks;
                        }

                        Remark remark = null;
                        foreach (XPathNavigator n in interator)
                        {
                            remark = new Remark();
                            remark.remark_type_rcd = XmlHelper.XpathValueNullToEmpty(n, "remark_type_rcd");
                            remark.remark_text = XmlHelper.XpathValueNullToEmpty(n, "remark_text");
                            if (remarks.HasRemark(remark, true) == false)
                            {
                                Guid remark_id = Guid.NewGuid();
                                remark.id = remark_id.ToString();
                                remark.booking_remark_id = remark_id;
                                remark.booking_id = bookingId;
                                remark.client_profile_id = clientProfileId;
                                remark.agency_code = agencyCode;

                                remarks.Add(remark);
                            }
                            remark = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private Clients GetProfileInformation(XPathNavigator nv)
        {
            Client p;
            Clients passengers = null;
            long clientNumber = 0;

            XPathNodeIterator ni = nv.Select("booking/passenger");
            if (ni.Count > 0)
            {
                passengers = new Clients();
                foreach (XPathNavigator n in ni)
                {
                    clientNumber = Convert.ToInt64(0 + XmlHelper.XpathValueNullToEmpty(n, "client_no"));
                    if (clientNumber > 0)
                    {
                        string[] strTitle = XmlHelper.XpathValueNullToEmpty(n, "title_rcd").Split('|');
                        if (strTitle != null && strTitle.Length == 2)
                        {
                            p = new Client();
                            p.client_number = clientNumber;

                            p.title_rcd = strTitle[0].ToUpper();
                            p.firstname = XmlHelper.XpathValueNullToEmpty(n, "firstname").ToUpper();
                            p.middlename = XmlHelper.XpathValueNullToEmpty(n, "middlename").ToUpper();
                            p.lastname = XmlHelper.XpathValueNullToEmpty(n, "lastname").ToUpper();

                            passengers.Add(p);
                            p = null;
                        }
                    }
                }
            }

            return passengers;
        }
        #endregion

        #region Helper
        private void SetCreateUpdateInformation(Guid guUserId,
                                                BookingHeader bookingHeader,
                                                Itinerary itinerary,
                                                Passengers passengers,
                                                Fees fees,
                                                Mappings mappings,
                                                Services services,
                                                Remarks remarks,
                                                Payments payments,
                                                Taxes taxes)
        {
            try
            {
                //Booking header
                if (bookingHeader.create_by == Guid.Empty)
                {
                    bookingHeader.create_by = guUserId;
                    bookingHeader.create_date_time = DateTime.Now;
                }
                bookingHeader.update_by = guUserId;
                bookingHeader.update_date_time = DateTime.Now;

                //Flight Segment
                foreach (FlightSegment f in itinerary)
                {
                    if (f.create_by == Guid.Empty)
                    {
                        f.create_by = guUserId;
                        f.create_date_time = DateTime.Now;
                    }
                    f.update_by = guUserId;
                    f.update_date_time = DateTime.Now;
                }

                //Passenger
                foreach (Passenger p in passengers)
                {
                    if (p.create_by == Guid.Empty)
                    {
                        p.create_by = guUserId;
                        p.create_date_time = DateTime.Now;
                    }
                    p.update_by = guUserId;
                    p.update_date_time = DateTime.Now;
                }

                foreach (Fee f in fees)
                {
                    if (f.create_by == Guid.Empty)
                    {
                        f.create_by = guUserId;
                        f.create_date_time = DateTime.Now;
                    }
                    f.update_by = guUserId;
                    f.update_date_time = DateTime.Now;
                }

                foreach (Mapping mp in mappings)
                {
                    if (mp.create_by == Guid.Empty)
                    {
                        mp.create_by = guUserId;
                        mp.create_date_time = DateTime.Now;
                    }
                    mp.update_by = guUserId;
                    mp.update_date_time = DateTime.Now;
                }

                foreach (Service s in services)
                {
                    if (s.create_by == Guid.Empty)
                    {
                        s.create_by = guUserId;
                        s.create_date_time = DateTime.Now;
                    }
                    s.update_by = guUserId;
                    s.update_date_time = DateTime.Now;
                }

                foreach (Remark r in remarks)
                {
                    if (r.create_by == Guid.Empty)
                    {
                        r.create_by = guUserId;
                        r.create_date_time = DateTime.Now;
                    }
                    r.update_by = guUserId;
                    r.update_date_time = DateTime.Now;
                }
                foreach (Payment p in payments)
                {
                    if (p.create_by == Guid.Empty)
                    {
                        p.create_by = guUserId;
                        p.create_date_time = DateTime.Now;
                    }
                    p.update_by = guUserId;
                    p.update_date_time = DateTime.Now;
                }

                foreach (Tax t in taxes)
                {
                    if (t.create_by == Guid.Empty)
                    {
                        t.create_by = guUserId;
                        t.create_date_time = DateTime.Now;
                    }
                    t.update_by = guUserId;
                    t.update_date_time = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();

                if (bookingHeader != null)
                { objHp.SendErrorEmail(ex, string.Empty); }
                else
                { objHp.SendErrorEmail(ex, "Object Is Null"); }

                throw ex;
            }
        }
        private DateTime DateFormatMMDDYYY(string strDate, bool usetime)
        {
            try
            {
                Array date = strDate.Split('/');
                string renew = "";
                if (date.Length == 3)
                {
                    if (usetime)
                        renew = date.GetValue(1).ToString() + "/" + date.GetValue(0).ToString() + "/" + date.GetValue(2).ToString() + " 23:59:59";
                    else
                        renew = date.GetValue(1).ToString() + "/" + date.GetValue(0).ToString() + "/" + date.GetValue(2).ToString();
                    return Convert.ToDateTime(renew, new System.Globalization.CultureInfo("en-US"));
                }
                else
                    return DateTime.MinValue;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Date<br/>" + strDate + "<br/>Used Time<br/>" + usetime);
                throw ex;
            }
        }
        private string HTMLEncodeSpecialChars(string text)
        {
            try
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (char c in text)
                {
                    if (c > 127 || c == 60 || c == 62) // special chars
                        sb.Append(String.Format("&#{0};", (int)c));
                    else
                        sb.Append(c);
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Html<br/>" + text.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }
        private bool ClientFound(Passengers p)
        {
            try
            {
                if (p != null)
                {
                    for (int i = 0; i < p.Count; i++)
                    {
                        if (p[i].passenger_profile_id.Equals(Guid.Empty) == false)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        private void InsertFlight(string strFlightInfo,
                                ref Flights objFlights,
                                ref string currencyRcd,
                                ref Int16 adult,
                                ref Int16 child,
                                ref Int16 infant,
                                ref Int16 other,
                                ref string otherDisplay)
        {
            try
            {
                Guid gFlightConnectionId = Guid.Empty;
                Guid flight_id = Guid.Empty;
                Guid fare_id = Guid.Empty;
                DateTime departure_date = DateTime.MinValue;
                Guid transit_flight_id = Guid.Empty;
                Guid transit_fare_id = Guid.Empty;
                DateTime transit_departure_date = DateTime.MinValue;

                string boarding_class_rcd = string.Empty;
                string booking_class_rcd = string.Empty;
                string origin_rcd = string.Empty;
                string destination_rcd = string.Empty;
                string transit_airport_rcd = string.Empty;
                string transit_boarding_class_rcd = string.Empty;
                string transit_booking_class_rcd = string.Empty;

                // Start add for Multi Stop
                string transit_points = string.Empty;
                string transit_points_name = string.Empty;

                string[] arrFlightInfo;
                string[] arrFlightDetail;

                if (strFlightInfo.Length > 0)
                {
                    arrFlightInfo = strFlightInfo.Split('|');
                    for (int i = 0; i < arrFlightInfo.Length; i++)
                    {
                        arrFlightDetail = arrFlightInfo[i].Split(':');
                        switch (arrFlightDetail[0])
                        {
                            case "flight_id":
                                if (string.IsNullOrEmpty(arrFlightDetail[1]) == false)
                                {
                                    flight_id = new Guid(arrFlightDetail[1]);
                                }
                                break;
                            case "fare_id":
                                if (string.IsNullOrEmpty(arrFlightDetail[1]) == false)
                                {
                                    fare_id = new Guid(arrFlightDetail[1]);
                                }
                                break;
                            case "boarding_class_rcd":
                                boarding_class_rcd = arrFlightDetail[1];
                                break;
                            case "booking_class_rcd":
                                booking_class_rcd = arrFlightDetail[1];
                                break;
                            case "origin_rcd":
                                origin_rcd = arrFlightDetail[1];
                                break;
                            case "destination_rcd":
                                destination_rcd = arrFlightDetail[1];
                                break;
                            case "departure_date":
                                if (string.IsNullOrEmpty(arrFlightDetail[1]) == false)
                                {
                                    departure_date = Convert.ToDateTime(arrFlightDetail[1].Substring(0, 4) + "-" + arrFlightDetail[1].Substring(4, 2) + "-" + arrFlightDetail[1].Substring(6, 2));
                                }
                                break;
                            case "transit_airport_rcd":
                                transit_airport_rcd = arrFlightDetail[1];
                                break;
                            case "transit_boarding_class_rcd":
                                transit_boarding_class_rcd = arrFlightDetail[1];
                                break;
                            case "transit_booking_class_rcd":
                                transit_booking_class_rcd = arrFlightDetail[1];
                                break;
                            case "transit_flight_id":
                                if (string.IsNullOrEmpty(arrFlightDetail[1]) == false)
                                {
                                    transit_flight_id = new Guid(arrFlightDetail[1]);
                                }
                                break;
                            case "transit_fare_id":
                                if (string.IsNullOrEmpty(arrFlightDetail[1]) == false)
                                {
                                    transit_fare_id = new Guid(arrFlightDetail[1]);
                                }
                                break;
                            case "transit_departure_date":
                                if (string.IsNullOrEmpty(arrFlightDetail[1]) == false)
                                {
                                    transit_departure_date = Convert.ToDateTime(arrFlightDetail[1].Substring(0, 4) + "-" + arrFlightDetail[1].Substring(4, 2) + "-" + arrFlightDetail[1].Substring(6, 2));
                                }
                                break;
                            case "transit_points":
                                // Start add for Multi Stop
                                if (arrFlightDetail[1].Length > 0)
                                {
                                    transit_points = arrFlightDetail[1];
                                }
                                break;
                            case "transit_points_name":
                                // Start add for Multi Stop
                                if (arrFlightDetail[1].Length > 0)
                                {
                                    transit_points_name = arrFlightDetail[1];

                                }
                                break;
                            case "number_of_adult":
                                // Start add for Multi Stop
                                if (arrFlightDetail[1].Length > 0)
                                {
                                    adult = Convert.ToInt16(arrFlightDetail[1]);

                                }
                                break;
                            case "number_of_child":
                                // Start add for Multi Stop
                                if (arrFlightDetail[1].Length > 0)
                                {
                                    child = Convert.ToInt16(arrFlightDetail[1]);

                                }
                                break;
                            case "number_of_infant":
                                // Start add for Multi Stop
                                if (arrFlightDetail[1].Length > 0)
                                {
                                    infant = Convert.ToInt16(arrFlightDetail[1]);

                                }
                                break;
                            case "number_of_other":
                                // Start add for Multi Stop
                                if (arrFlightDetail[1].Length > 0)
                                {
                                    other = Convert.ToInt16(arrFlightDetail[1]);

                                }
                                break;
                            case "other_type":
                                // Start add for Multi Stop
                                if (arrFlightDetail[1].Length > 0)
                                {
                                    otherDisplay = arrFlightDetail[1];

                                }
                                break;
                            case "currency_rcd":
                                // Start add for Multi Stop
                                if (arrFlightDetail[1].Length > 0)
                                {
                                    currencyRcd = arrFlightDetail[1];

                                }
                                break;

                        }
                    }
                    //Add flight information.
                    Flight f = new Flight();
                    f.boarding_class_rcd = boarding_class_rcd;
                    f.booking_class_rcd = booking_class_rcd;
                    f.flight_id = flight_id;
                    f.origin_rcd = origin_rcd;
                    f.departure_date = departure_date;
                    f.fare_id = fare_id;
                    f.eticket_flag = 1;
                    if (transit_flight_id != Guid.Empty)
                    {
                        gFlightConnectionId = Guid.NewGuid();
                        f.od_origin_rcd = origin_rcd;
                        f.od_destination_rcd = destination_rcd;
                        f.flight_connection_id = gFlightConnectionId;
                        f.destination_rcd = transit_airport_rcd;
                    }
                    else
                    { f.destination_rcd = destination_rcd; }
                    // Start add for Multi Stop
                    if (string.IsNullOrEmpty(transit_points) == false)
                    {
                        f.transit_points = transit_points;
                    }
                    if (string.IsNullOrEmpty(transit_points_name) == false)
                    {
                        f.transit_points_name = transit_points_name;
                    }

                    objFlights.Add(f);
                    f = null;
                    //Transit flight information
                    if (transit_flight_id != Guid.Empty)
                    {
                        Flight transitFlight = new Flight();
                        transitFlight.boarding_class_rcd = transit_boarding_class_rcd;
                        transitFlight.booking_class_rcd = transit_booking_class_rcd;
                        transitFlight.destination_rcd = destination_rcd;
                        transitFlight.flight_id = transit_flight_id;
                        transitFlight.origin_rcd = transit_airport_rcd;
                        transitFlight.departure_date = transit_departure_date;
                        transitFlight.fare_id = transit_fare_id;
                        transitFlight.eticket_flag = 1;
                        transitFlight.od_origin_rcd = origin_rcd;
                        transitFlight.od_destination_rcd = destination_rcd;
                        transitFlight.flight_connection_id = gFlightConnectionId;
                        objFlights.Add(transitFlight);
                        transitFlight = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                Library objLi = new Library();

                if (objFlights != null)
                { objHp.SendErrorEmail(ex, "Xml Input<br/>" + XmlHelper.Serialize(objFlights, false).Replace("<", "&lt;").Replace(">", "&gt;")); }
                else
                { objHp.SendErrorEmail(ex, "Object is Null."); }

                throw ex;
            }
        }

        private void FillSession(BookingHeader bookingHeader,
                                Itinerary itinerary,
                                Passengers passengers,
                                Fees fees,
                                Mappings mappings,
                                Services services,
                                Remarks remarks,
                                Payments payments,
                                Taxes taxes)
        {

            B2CSession.BookingHeader = bookingHeader;
            B2CSession.Itinerary = itinerary;
            B2CSession.Passengers = passengers;
            B2CSession.Fees = fees;
            B2CSession.Mappings = mappings;
            B2CSession.Services = services;
            B2CSession.Remarks = remarks;
            B2CSession.Payments = payments;
            B2CSession.Taxes = taxes;
        }
        private bool PointValidatePass(Client objClient, Mappings objMappings)
        {
            try
            {
                if (objMappings[0].fare_type_rcd == "POINT")
                {
                    if (objClient != null)
                    {
                        double TotalRedeem = 0;
                        foreach (Mapping m in objMappings)
                        {
                            TotalRedeem = TotalRedeem + m.redemption_points;
                        }
                        if (objClient.ffp_balance >= TotalRedeem)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public string GeneratePassword()
        {
            try
            {
                Random rand = new Random();
                int RandomNumber = rand.Next();

                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                UnicodeEncoding enc = new UnicodeEncoding();
                byte[] input = null;
                byte[] output = null;
                System.Text.StringBuilder hash = new StringBuilder(32);

                input = enc.GetBytes(RandomNumber.ToString());
                output = md5.ComputeHash(input);

                foreach (byte b in output)
                {
                    hash.Append(string.Format("{0:X2}", b));
                }
                return (hash.ToString().Substring(0, 9));
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public string FilterByClientID(string xmlBooking, string clientID)
        {
            try
            {
                StringBuilder bldXML = new StringBuilder();
                XmlDocument XmlDoc = new XmlDocument();
                XmlNodeList nl;

                XmlDoc.LoadXml(xmlBooking);
                bldXML.Append("<ClientBookings>");
                nl = XmlDoc.SelectNodes("ClientBookings/ClientBooking");

                foreach (XmlNode n in nl)
                {
                    if (n["client_profile_id"] != null)
                    {
                        string clientid = Convert.ToString(n["client_profile_id"].InnerText + "");
                        if (clientid == clientID)
                        {
                            bldXML.Append("<ClientBooking>");
                            bldXML.Append(n.InnerXml);
                            bldXML.Append("</ClientBooking>");
                        }
                    }
                }
                bldXML.Append("</ClientBookings>");
                return bldXML.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Booking Xml<br/>" + xmlBooking.Replace("<", "&lt;").Replace(">", "&gt;") +
                                         "<br/>Client Id<br/>" + clientID);
                throw ex;
            }
        }
        private Passenger FillPassenger(string userAccountId, Client objClient, string Action)
        {
            try
            {
                Passenger objPassenger = new Passenger();
                if (userAccountId != "")
                {
                    if (Action.ToUpper() == "ADD")
                    {
                        objPassenger.create_by = new Guid(userAccountId);
                    }
                    else
                    {
                        objPassenger.create_by = new Guid();
                    }

                    objPassenger.update_by = new Guid(userAccountId);
                }
                objPassenger.create_date_time = DateTime.Now;
                objPassenger.date_of_birth = objClient.date_of_birth;
                objPassenger.firstname = objClient.firstname.ToUpper();
                objPassenger.middlename = objClient.middlename.ToUpper();
                objPassenger.lastname = objClient.lastname.ToUpper();
                objPassenger.gender_type_rcd = objClient.gender_type_rcd;
                objPassenger.title_rcd = objClient.title_rcd;
                objPassenger.update_date_time = DateTime.Now;
                objPassenger.passenger_role_rcd = "MYSELF";
                objPassenger.client_number = objClient.client_number;
                objPassenger.passenger_type_rcd = objClient.passenger_type_rcd;
                objPassenger.nationality_rcd = objClient.nationality_rcd;
                objPassenger.passport_birth_place = objClient.passport_birth_place;
                objPassenger.passport_expiry_date = objClient.passport_expiry_date;
                objPassenger.passport_issue_date = objClient.passport_issue_date;
                objPassenger.passport_issue_place = objClient.passport_issue_place;
                objPassenger.passport_number = objClient.passport_number;
                objPassenger.document_type_rcd = objClient.document_type_rcd;
                objPassenger.passport_issue_country_rcd = objClient.passport_issue_country_rcd;

                return objPassenger;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        private Passenger FillPassengerEdit(string userAccountId, Client objClient, string Action, string PassengerRole)
        {
            try
            {
                Passenger objPassenger = new Passenger();
                if (userAccountId != "")
                {
                    if (Action.ToUpper() == "ADD")
                    {
                        objPassenger.create_by = new Guid(userAccountId);
                        objPassenger.create_date_time = DateTime.Now;
                    }
                    else
                    {
                        // objPassenger.create_by = new Guid();
                        objPassenger.create_by = new Guid(userAccountId);
                    }

                    objPassenger.update_by = new Guid(userAccountId);
                }

                objPassenger.firstname = objClient.firstname.ToUpper();
                objPassenger.middlename = objClient.middlename.ToUpper();
                objPassenger.lastname = objClient.lastname.ToUpper();
                objPassenger.title_rcd = objClient.title_rcd;
                objPassenger.update_date_time = DateTime.Now;
                objPassenger.passenger_role_rcd = PassengerRole;
                objPassenger.client_number = objClient.client_number;
                objPassenger.client_profile_id = objClient.client_profile_id;
                objPassenger.passenger_type_rcd = objClient.passenger_type_rcd;
                objPassenger.nationality_rcd = objClient.nationality_rcd;
                objPassenger.passport_birth_place = objClient.passport_birth_place;
                objPassenger.passport_expiry_date = objClient.passport_expiry_date;
                objPassenger.passport_issue_date = objClient.passport_issue_date;
                objPassenger.passport_issue_place = objClient.passport_issue_place;
                objPassenger.document_type_rcd = objClient.document_type_rcd;
                objPassenger.date_of_birth = objClient.date_of_birth;
                objPassenger.passport_number = objClient.passport_number;

                return objPassenger;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        private Passenger FillPassengerProfile(string userAccountId, string firstName,
                                               string lastName, string title, string Action,
                                               long clientNumber, string document_type_rcd, string date_of_birth,
                                               string passport_expiry_date, string passport_issue_date, string nationality_rcd,
                                               Guid client_profile_id, long client_number, string PassengerRole, string PassengerType,
                                               string issueCountry, string passportnumber, string passportBirthPlace, string passportIssuePlac)
        {
            try
            {
                Passenger objPassenger = new Passenger();
                Library objLi = new Library();
                if (userAccountId != "")
                {
                    if (Action.ToUpper() == "ADD")
                    {
                        objPassenger.create_by = new Guid(userAccountId);
                    }
                    else
                    {
                        objPassenger.create_by = new Guid();
                    }

                    objPassenger.update_by = new Guid(userAccountId);
                }
                objPassenger.create_date_time = DateTime.Now;
                if (firstName.Length > 0)
                {
                    objPassenger.firstname = firstName.ToUpper();
                }
                if (lastName.Length > 0)
                {
                    objPassenger.lastname = lastName.ToUpper();
                }
                if (title.Length > 0)
                {
                    objPassenger.title_rcd = title.Split('|')[0];
                    objPassenger.gender_type_rcd = title.Split('|')[1];
                }

                objPassenger.update_date_time = DateTime.Now;
                if (document_type_rcd.Length > 0)
                {
                    objPassenger.document_type_rcd = document_type_rcd;
                }
                if (date_of_birth.Length > 0)
                {
                    objPassenger.date_of_birth = DataHelper.ParseDateString(date_of_birth); //Convert.ToDateTime(date_of_birth);
                }
                if (passport_expiry_date.Length > 0)
                {
                    objPassenger.passport_expiry_date = DataHelper.ParseDateString(passport_expiry_date); // Convert.ToDateTime(passport_expiry_date);
                }
                if (passport_issue_date.Length > 0)
                {
                    objPassenger.passport_issue_date = DataHelper.ParseDateString(passport_issue_date); //Convert.ToDateTime(passport_issue_date);
                }
                if (nationality_rcd.Length > 0)
                {
                    objPassenger.nationality_rcd = nationality_rcd;
                }
                objPassenger.client_number = client_number;
                objPassenger.client_profile_id = client_profile_id;

                if (PassengerRole.Length > 0)
                {
                    objPassenger.passenger_role_rcd = PassengerRole;
                }
                if (PassengerType.Length > 0)
                {
                    objPassenger.passenger_type_rcd = PassengerType;
                }
                if (passportBirthPlace.Length > 0)
                {
                    objPassenger.passport_birth_place = passportBirthPlace;
                }
                if (passportIssuePlac.Length > 0)
                {
                    objPassenger.passport_issue_place = passportIssuePlac;
                }
                if (passportnumber.Length > 0)
                {
                    objPassenger.passport_number = passportnumber;
                }
                if (issueCountry.Length > 0)
                {
                    objPassenger.passport_issue_country_rcd = issueCountry;
                }

                return objPassenger;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "User Account Id : " + userAccountId +
                                         "<br/>Firstname : " + firstName +
                                         "<br/>Lastname : " + lastName +
                                         "<br/>Title : " + title +
                                         "<br/>Action : " + Action +
                                         "<br/>Client Number : " + clientNumber.ToString() +
                                         "<br/>Document Type : " + document_type_rcd +
                                         "<br/>Date Of Birth : " + date_of_birth +
                                         "<br/>Passport Expiry : " + passport_expiry_date +
                                         "<br/>Passport Issue Date : " + passport_issue_date +
                                         "<br/>Nationality : " + nationality_rcd +
                                         "<br/>Client profile Id : " + client_profile_id.ToString() +
                                         "<br/>Client Number : " + client_number.ToString() +
                                         "<br/>Passenger Role : " + PassengerRole +
                                         "<br/>Passenger Type : " + PassengerType +
                                         "<br/>Passport Number : " + passportnumber +
                                         "<br/>Birth Place : " + passportBirthPlace +
                                         "<br/>Issue Place : " + passportIssuePlac);
                throw ex;
            }
        }


        private decimal TotalPayment(Payments paymentsInput)
        {
            decimal dclTotalPayment = 0;

            for (int i = 0; i < paymentsInput.Count; i++)
            {
                dclTotalPayment = dclTotalPayment + paymentsInput[i].payment_amount;
            }

            return dclTotalPayment;
        }
        private void CreateACEPassengerSession(Passengers paxs)
        {
            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("InsuranceACESetting");

            if (Setting != null)
            {
                if (paxs != null && paxs.Count > 0)
                {
                    Passengers objPaxs = new Passengers();
                    for (int i = 0; i < paxs.Count; i++)
                    {
                        objPaxs.Add(paxs[i]);
                    }
                    B2CSession.ACEPassenger = objPaxs;
                }
            }
        }
        public Passengers CreateACEPassenger()
        {
            if (B2CSession.ACEQuotePassengers != null)
            {
                ACEQuotePassengers ACEQuotePassengers = B2CSession.ACEQuotePassengers;
                Passengers Passengers = new Passengers();
                int count = ACEQuotePassengers.Count;
                Passenger[] Passenger = new Passenger[count];
                for (int i = 0; i < count; i++)
                {
                    Passenger[i] = new tikSystem.Web.Library.Passenger();
                    Passenger[i].firstname = ACEQuotePassengers[i].firstname;
                    Passenger[i].middlename = ACEQuotePassengers[i].middlename;
                    Passenger[i].lastname = ACEQuotePassengers[i].lastname;
                    Passenger[i].title_rcd = ACEQuotePassengers[i].title_rcd;
                    Passenger[i].passenger_type_rcd = ACEQuotePassengers[i].passenger_type_rcd;
                    Passenger[i].passport_number = ACEQuotePassengers[i].passport_number;
                    Passenger[i].date_of_birth = ACEQuotePassengers[i].date_of_birth;
                    Passenger[i].gender_type_rcd = ACEQuotePassengers[i].gender_type_rcd;

                    Passengers.Add(Passenger[i]);
                }

                return Passengers;
            }
            else
            {
                return new Passengers();
            }
        }
        private Decimal ConvertToDecimal(string val)
        {
            Decimal ret = 0;

            if (val != "")
            {
                ret = Convert.ToDecimal(val);
            }
            return ret;
        }
        private bool HasRemark(Remarks remarks, string remark_type_rcd, ref Remark remark)
        {
            bool hasRemark = false;
            foreach (Remark r in remarks)
            {
                if (r.remark_type_rcd.ToUpper().Equals(remark_type_rcd.ToUpper()))
                {
                    remark = r;
                    hasRemark = true;
                    break;
                }
            }
            return hasRemark;
        }
        public XPathNodeIterator QuoteSummaryXmlIterator(StringReader srd, Library objLi)
        {
            try
            {
                XPathDocument xmlDoc = new XPathDocument(srd);
                XPathNavigator nv = xmlDoc.CreateNavigator();
                XPathNodeIterator interator = nv.Select("flights/flight");

                if (interator.Count == 1)
                {
                    foreach (XPathNavigator n in interator)
                    {
                        if (XmlHelper.XpathValueNullToGUID(n, "flight_id").Equals(Guid.Empty) == true ||
                            XmlHelper.XpathValueNullToGUID(n, "fare_id").Equals(Guid.Empty) == true ||
                            XmlHelper.XpathValueNullToEmpty(n, "origin_rcd").Equals(string.Empty) == true ||
                            XmlHelper.XpathValueNullToEmpty(n, "destination_rcd").Equals(string.Empty) == true ||
                            XmlHelper.XpathValueNullToEmpty(n, "planned_departure_time").Equals(string.Empty) == true ||
                            XmlHelper.XpathValueNullToEmpty(n, "planned_arrival_time").Equals(string.Empty) == true ||
                            XmlHelper.XpathValueNullToDateTime(n, "departure_date").Equals(DateTime.MinValue) == true)
                        {
                            return null;
                        }
                    }
                    return interator;
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }
        private string GetBookingsList(string bookingCode)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                if (B2CSession.Client != null)
                {
                    ServiceClient obj = new ServiceClient();
                    obj.objService = B2CSession.AgentService;

                    using (DataSet ds = obj.GetBookings("", "", "", DateTime.MinValue, DateTime.MinValue, bookingCode, ""
                                                , "", "", "", "", "", ""
                                                , "", "", B2CSession.Client.client_profile_id.ToString(), true, "EN", true, true, DateTime.MinValue, DateTime.MinValue))
                    {
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            return ds.GetXml();
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        private void FillFlight(string flightXML, Flights flights)
        {
            Library objLi = new Library();
            Flight f = null;
            Flight tF = null;

            string strDate = string.Empty;

            if (flights != null)
            {
                using (StringReader srd = new StringReader(flightXML))
                {
                    XPathNodeIterator interator = QuoteSummaryXmlIterator(srd, objLi);
                    foreach (XPathNavigator n in interator)
                    {
                        f = new Flight();

                        f.flight_id = XmlHelper.XpathValueNullToGUID(n, "flight_id");
                        f.airline_rcd = XmlHelper.XpathValueNullToEmpty(n, "airline_rcd");
                        f.flight_number = XmlHelper.XpathValueNullToEmpty(n, "flight_number");

                        f.fare_id = XmlHelper.XpathValueNullToGUID(n, "fare_id");
                        f.booking_class_rcd = XmlHelper.XpathValueNullToEmpty(n, "booking_class_rcd");
                        f.od_origin_rcd = XmlHelper.XpathValueNullToEmpty(n, "origin_rcd");
                        f.od_destination_rcd = XmlHelper.XpathValueNullToEmpty(n, "destination_rcd");

                        f.origin_rcd = f.od_origin_rcd;
                        f.currency_rcd = XmlHelper.XpathValueNullToEmpty(n, "currency_rcd");

                        strDate = XmlHelper.XpathValueNullToEmpty(n, "departure_date");
                        if (string.IsNullOrEmpty(strDate) == false)
                        {
                            f.departure_date = new DateTime(Convert.ToInt16(strDate.Substring(0, 4)), Convert.ToInt16(strDate.Substring(4, 2)), Convert.ToInt16(strDate.Substring(6, 2)));
                        }

                        strDate = XmlHelper.XpathValueNullToEmpty(n, "arrival_date");
                        if (string.IsNullOrEmpty(strDate) == false)
                        {
                            f.arrival_date = new DateTime(Convert.ToInt16(strDate.Substring(0, 4)), Convert.ToInt16(strDate.Substring(4, 2)), Convert.ToInt16(strDate.Substring(6, 2)));
                        }

                        f.departure_dayOfWeek = XmlHelper.XpathValueNullToByte(n, "departure_day");
                        f.arrival_dayOfWeek = XmlHelper.XpathValueNullToByte(n, "arrival_day");

                        f.planned_departure_time = XmlHelper.XpathValueNullToInt16(n, "planned_departure_time");
                        f.planned_arrival_time = XmlHelper.XpathValueNullToInt16(n, "planned_arrival_time");

                        if (XmlHelper.XpathValueNullToEmpty(n, "transit_flight_id").Length == 0)
                        {
                            f.destination_rcd = f.od_destination_rcd;
                        }
                        else
                        {
                            f.destination_rcd = XmlHelper.XpathValueNullToEmpty(n, "transit_airport_rcd");
                            f.flight_connection_id = Guid.NewGuid();
                        }
                        flights.Add(f);

                        //Add Transit information
                        if (f.flight_connection_id.Equals(Guid.Empty) == false)
                        {
                            tF = new Flight();

                            tF.flight_connection_id = f.flight_connection_id;
                            tF.flight_id = XmlHelper.XpathValueNullToGUID(n, "transit_flight_id");
                            tF.fare_id = XmlHelper.XpathValueNullToGUID(n, "transit_fare_id");
                            tF.airline_rcd = XmlHelper.XpathValueNullToEmpty(n, "transit_airline_rcd");
                            tF.flight_number = XmlHelper.XpathValueNullToEmpty(n, "transit_flight_number");

                            tF.origin_rcd = f.destination_rcd;
                            tF.destination_rcd = f.od_destination_rcd;
                            tF.od_origin_rcd = f.od_origin_rcd;
                            tF.od_destination_rcd = f.od_destination_rcd;
                            tF.booking_class_rcd = f.booking_class_rcd;

                            strDate = XmlHelper.XpathValueNullToEmpty(n, "transit_departure_date");
                            if (string.IsNullOrEmpty(strDate) == false)
                            {
                                tF.departure_date = new DateTime(Convert.ToInt16(strDate.Substring(0, 4)), Convert.ToInt16(strDate.Substring(4, 2)), Convert.ToInt16(strDate.Substring(6, 2)));
                            }

                            strDate = XmlHelper.XpathValueNullToEmpty(n, "transit_arrival_date");
                            if (string.IsNullOrEmpty(strDate) == false)
                            {
                                tF.arrival_date = new DateTime(Convert.ToInt16(strDate.Substring(0, 4)), Convert.ToInt16(strDate.Substring(4, 2)), Convert.ToInt16(strDate.Substring(6, 2)));
                            }

                            tF.departure_dayOfWeek = XmlHelper.XpathValueNullToByte(n, "transit_departure_day");
                            tF.arrival_dayOfWeek = XmlHelper.XpathValueNullToByte(n, "transit_arrival_day");

                            tF.planned_departure_time = XmlHelper.XpathValueNullToInt16(n, "transit_planned_departure_time");
                            tF.planned_arrival_time = XmlHelper.XpathValueNullToInt16(n, "transit_planned_arrival_time");

                            flights.Add(tF);
                            tF = null;
                        }
                        f = null;
                    }
                }
            }
        }
        #endregion

        #region Registration
        // Create user client profile
        [WebMethod(EnableSession = true)]
        public string CreateClientProfile(string title,
                                            string gender,
                                            string first_name,
                                            string last_name,
                                            string dateOfBirth,
                                            string nationality,
                                            string documenttype,
                                            string passportnumber,
                                            string issueCountry,
                                            string passportIssuePlace,
                                            string passportBirthPlace,
                                            string passportIssueDate,
                                            string passportExpiryDate,
                                            string client_password,
                                            string phoneMobile,
                                            string phoneHome,
                                            string phoneBusiness,
                                            string contactEmail,
                                            string language,
                                            string addressline1,
                                            string addressline2,
                                            string zipCode,
                                            string city,
                                            string country,
                                            string passenger_type_rcd,
                                            string url,
                                            string mobileEmail,
                                            string state)
        {

            try
            {
                ServiceClient SobjCient = new ServiceClient();
                if (B2CSession.AgentService != null)
                {
                    SobjCient.objService = B2CSession.AgentService;
                }

                if (SobjCient.CheckUniqueMailAddress(contactEmail, string.Empty) == true)
                {
                    Client objClient = new Client();

                    objClient.address_line1 = addressline1;
                    objClient.address_line2 = addressline2;
                    objClient.city = city;

                    if (client_password == "xxxxxxxxx")
                        objClient.client_password = "password";
                    else
                        objClient.client_password = client_password;

                    if (contactEmail.Length > 0)
                    {
                        objClient.contact_email = contactEmail;
                    }
                    if (string.IsNullOrEmpty(mobileEmail) == false)
                    {
                        objClient.mobile_email = mobileEmail;
                    }
                    if (country.Length > 0)
                    {
                        objClient.country_rcd = country;
                    }
                    if (documenttype.Length > 0)
                    {
                        objClient.document_type_rcd = documenttype;
                    }
                    if (first_name.Length > 0)
                    {
                        objClient.firstname = first_name.ToUpper();
                    }
                    if (title.Length > 0)
                    {
                        objClient.title_rcd = title.Split('|')[0];
                        objClient.gender_type_rcd = title.Split('|')[1];
                    }
                    if (language.Length > 0)
                    {
                        objClient.language_rcd = language;
                    }
                    if (last_name.Length > 0)
                    {
                        objClient.lastname = last_name.ToUpper();
                    }
                    if (nationality.Length > 0)
                    {
                        objClient.nationality_rcd = nationality;
                    }
                    if (passportBirthPlace.Length > 0)
                    {
                        objClient.passport_birth_place = passportBirthPlace;
                    }
                    if (passportIssuePlace.Length > 0)
                    {
                        objClient.passport_issue_place = passportIssuePlace;
                    }
                    if (passportExpiryDate.Length > 0)
                    {
                        objClient.passport_expiry_date = Convert.ToDateTime(passportExpiryDate);
                    }
                    if (passportIssueDate.Length > 0)
                    {
                        objClient.passport_issue_date = Convert.ToDateTime(passportIssueDate);
                    }
                    if (dateOfBirth.Length > 0)
                    {
                        objClient.date_of_birth = Convert.ToDateTime(dateOfBirth);
                    }
                    if (passportnumber.Length > 0)
                    {
                        objClient.passport_number = passportnumber;
                    }
                    if (phoneBusiness.Length > 0)
                    {
                        objClient.phone_business = phoneBusiness;
                    }
                    if (zipCode.Length > 0)
                    {
                        objClient.zip_code = zipCode;
                    }
                    if (phoneHome.Length > 0)
                    {
                        objClient.phone_home = phoneHome;
                    }
                    if (phoneMobile.Length > 0)
                    {
                        objClient.phone_mobile = phoneMobile;
                    }
                    if (string.IsNullOrEmpty(state) == false)
                    {
                        objClient.state = state;
                    }
                    if (string.IsNullOrEmpty(issueCountry) == false)
                    {
                        objClient.passport_issue_country_rcd = issueCountry;
                    }

                    objClient.client_type_rcd = "IND";
                    objClient.member_since_date = DateTime.Now;
                    if (passenger_type_rcd.Length > 0)
                    {
                        objClient.passenger_type_rcd = passenger_type_rcd;
                    }
                    if (language.Length > 0)
                    {
                        objClient.language_rcd = language;
                    }

                    Passengers objPassenger = new Passengers();
                    B2CVariable objUv = B2CSession.Variable;
                    if (objUv != null)
                    {
                        objClient.create_by = objUv.UserId;
                        objPassenger.Add(FillPassenger(objUv.UserId.ToString(), objClient, "ADD"));
                    }
                    //Finding the Member Level Record.
                    ServiceClient obj = new ServiceClient();
                    if (B2CSession.AgentService != null)
                    {
                        obj.objService = B2CSession.AgentService;

                        if (obj.objService != null)
                        {
                            DataSet ds = SobjCient.MemberLevelRead("", "", "A", true);
                            DataTable dt = ds.Tables[0];
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    DataView dv = dt.DefaultView;
                                    dv.Sort = "sort_sequence desc";

                                    DataRow row = dv.ToTable().Rows[0];
                                    if (row != null)
                                    {
                                        objClient.member_level_rcd = row["member_level_rcd"].ToString();
                                    }

                                    dv = null;
                                }
                            }
                        }
                    }

                    Library objLi = new Library();
                    string feesXml = string.Empty;
                    string xmlClient = XmlHelper.Serialize(objClient, false);
                    string xmlPassenger = XmlHelper.Serialize(objPassenger, false);
                    string xmlBookingRemark = string.Empty;

                    bool success = false;
                    string strClientNumber = "";

                    success = SobjCient.ClientSave(xmlClient, xmlPassenger, xmlBookingRemark);
                    if (success == true)
                    {
                        //Call to get Client number.
                        ClientLogon(objClient.contact_email, objClient.client_password);
                        //objClient
                        Client sClient = null;
                        if (B2CSession.Client != null) sClient = B2CSession.Client;

                        if (sClient != null)
                        {
                            if (url != "")
                            {
                                SendMaintainMail(objClient.contact_email,
                                                title.Split('|')[0],
                                                objClient.firstname,
                                                objClient.lastname,
                                                sClient.client_number.ToString(),
                                                "",
                                                "create",
                                                url.Substring(0, url.LastIndexOf('/')),
                                                objClient.client_password,
                                                objUv.UserId);
                            }
                            else
                            {
                                SendMaintainMail(objClient.contact_email,
                                                title.Split('|')[0],
                                                objClient.firstname,
                                                objClient.lastname,
                                                sClient.client_number.ToString(),
                                                "",
                                                "create",
                                                url,
                                                objClient.client_password,
                                                objUv.UserId);
                            }
                            strClientNumber = sClient.client_number.ToString();
                        }
                    }
                    else
                    {
                        //Show Save failed
                        strClientNumber = "01";
                    }
                    B2CSession.Client = null;

                    objClient = null;
                    return strClientNumber;
                }
                else
                {
                    //Show Error Email duplicate.
                    return "02";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Title : " + title +
                                         "<br/>Gender : " + gender +
                                         "<br/>Firstname : " + first_name +
                                         "<br/>Lastname : " + last_name +
                                         "<br/>Date of Birth : " + dateOfBirth +
                                         "<br/>Nationality : " + nationality +
                                         "<br/>Document Type : " + documenttype +
                                         "<br/>Passport Number : " + passportnumber +
                                         "<br/>Issue Place : " + passportIssuePlace +
                                         "<br/>Birth Place : " + passportBirthPlace +
                                         "<br/>Issue Date : " + passportIssueDate +
                                         "<br/>Expiry Date : " + passportExpiryDate +
                                         "<br/>Mobile : " + phoneMobile +
                                         "<br/>Home : " + phoneHome +
                                         "<br/>Business : " + phoneBusiness +
                                         "<br/>Email : " + contactEmail +
                                         "<br/>Language : " + language +
                                         "<br/>Address 1 : " + addressline1 +
                                         "<br/>Address 2 : " + addressline2 +
                                         "<br/>Zip : " + zipCode +
                                         "<br/>City : " + city +
                                         "<br/>Country : " + country +
                                         "<br/>Passenger Type : " + passenger_type_rcd +
                                         "<br/>Url : " + url);
                throw ex;
            }

        }

        [WebMethod(EnableSession = true)]
        public string LoadRegistration()
        {
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library objLi = new Library();
                string result;

                //Clear temp availability
                if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                {
                    //Clear block inventory
                    ServiceClient objSvc = new ServiceClient();
                    objSvc.objService = B2CSession.AgentService;
                    objSvc.ReleaseFlightInventorySession(bookingHeader.booking_id.ToString(), string.Empty, string.Empty, string.Empty, false, true, true);
                    objSvc.objService = null;
                    objSvc = null;
                    //Clear booking in session
                    objLi.ClearBooking(ref bookingHeader,
                                        ref itinerary,
                                        ref passengers,
                                        ref quotes,
                                        ref fees,
                                        ref mappings,
                                        ref services,
                                        ref remarks,
                                        ref payments,
                                        ref taxes);
                }
                result = objLi.GenerateControlString("UserControls/Registration.ascx", string.Empty);

                return result;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string LoadRegistration_Edit()
        {
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library objLi = new Library();
                string result;

                if (B2CSession.Client != null)
                {
                    //Clear temp availability
                    if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                    {
                        //Clear block inventory
                        ServiceClient objSvc = new ServiceClient();
                        objSvc.objService = B2CSession.AgentService;
                        objSvc.ReleaseFlightInventorySession(bookingHeader.booking_id.ToString(), string.Empty, string.Empty, string.Empty, false, true, true);
                        objSvc.objService = null;
                        objSvc = null;
                        //Clear booking in session
                        objLi.ClearBooking(ref bookingHeader,
                                            ref itinerary,
                                            ref passengers,
                                            ref quotes,
                                            ref fees,
                                            ref mappings,
                                            ref services,
                                            ref remarks,
                                            ref payments,
                                            ref taxes);

                    }

                    result = objLi.GenerateControlString("UserControls/Registration_Edit.ascx", string.Empty);
                }
                else
                    result = "{002}";

                return result;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public bool ClientProfileSave(string title,
                                        string gender,
                                        string first_name,
                                        string last_name,
                                        string client_password,
                                        string phoneMobile,
                                        string phoneHome,
                                        string phoneBusiness,
                                        string contactEmail,
                                        string language,
                                        string addressline1,
                                        string addressline2,
                                        string zipCode, string city,
                                        string country,
                                        string nationality,
                                        string documenttype,
                                        string PassengerRole,
                                        string PassengerType,
                                        string passportIssuePlace,
                                        string passportBirthPlace,
                                        string passportIssueDate,
                                        string passportExpiryDate,
                                        string dateOfBirth,
                                        string passportnumber,
                                        string passportIssueCountry,
                                        string strPassenger,
                                        string url,
                                        string mobileEmail,
                                        string state)
        {
            string statusPWS = "";
            try
            {
                Client sClient = B2CSession.Client;

                if (sClient != null)
                {
                    sClient.address_line1 = addressline1;
                    sClient.address_line2 = addressline2;
                    sClient.city = city;
                    if (client_password == "sssssssss")
                    {
                        sClient.client_password = sClient.client_password;
                    }
                    else
                    {
                        sClient.client_password = client_password;
                        statusPWS = "change";
                    }
                    if (contactEmail.Length > 0)
                    {
                        sClient.contact_email = contactEmail;
                    }
                    if (string.IsNullOrEmpty(mobileEmail) == false)
                    {
                        sClient.mobile_email = mobileEmail;
                    }
                    if (country.Length > 0)
                    {
                        sClient.country_rcd = country;
                    }
                    if (dateOfBirth.Length > 0)
                    {
                        sClient.date_of_birth = Convert.ToDateTime(dateOfBirth);
                    }
                    if (passportIssueDate.Length > 0)
                    {
                        sClient.passport_issue_date = Convert.ToDateTime(passportIssueDate);
                    }
                    if (passportExpiryDate.Length > 0)
                    {
                        sClient.passport_expiry_date = Convert.ToDateTime(passportExpiryDate);
                    }
                    if (documenttype.Length > 0)
                    {
                        sClient.document_type_rcd = documenttype;
                    }
                    if (first_name.Length > 0)
                    {
                        sClient.firstname = first_name.ToUpper();
                    }
                    if (title.Length > 0)
                    {
                        sClient.title_rcd = title.Split('|')[0];
                        sClient.gender_type_rcd = title.Split('|')[1];
                    }
                    if (language.Length > 0)
                    {
                        sClient.language_rcd = language;
                    }
                    if (last_name.Length > 0)
                    {
                        sClient.lastname = last_name.ToUpper();
                    }
                    if (nationality.Length > 0)
                    {
                        sClient.nationality_rcd = nationality;
                    }
                    if (passportIssuePlace.Length > 0)
                    {
                        sClient.passport_birth_place = passportIssuePlace;
                    }
                    if (passportBirthPlace.Length > 0)
                    {
                        sClient.passport_issue_place = passportBirthPlace;
                    }
                    if (passportnumber.Length > 0)
                    {
                        sClient.passport_number = passportnumber;
                    }
                    if (passportIssueCountry.Length > 0)
                    {
                        sClient.passport_issue_country_rcd = passportIssueCountry;
                    }
                    if (phoneBusiness.Length > 0)
                    {
                        sClient.phone_business = phoneBusiness;
                    }
                    if (zipCode.Length > 0)
                    {
                        sClient.zip_code = zipCode;
                    }
                    if (phoneHome.Length > 0)
                    {
                        sClient.phone_home = phoneHome;
                    }
                    if (phoneMobile.Length > 0)
                    {
                        sClient.phone_mobile = phoneMobile;
                    }
                    if (string.IsNullOrEmpty(state) == false)
                    {
                        sClient.state = state;
                    }
                    sClient.client_profile_id = sClient.client_profile_id;
                    sClient.client_number = sClient.client_number;
                    if (PassengerType.Length > 0)
                    {
                        sClient.passenger_type_rcd = PassengerType;
                    }

                    sClient.client_type_rcd = sClient.client_type_rcd;
                    Passengers objPassenger = new Passengers();
                    B2CVariable objUv = B2CSession.Variable;

                    sClient.update_by = objUv.UserId;
                    sClient.member_level_rcd = sClient.member_level_rcd;
                    sClient.member_since_date = sClient.member_since_date;
                    sClient.update_date_time = DateTime.Now;
                    Library objLi = new Library();

                    string xmlClient = XmlHelper.Serialize(sClient, false);
                    string xmlPassenger = string.Empty;
                    ServiceClient SobjCient = new ServiceClient();
                    string feesXml = string.Empty;

                    SobjCient.objService = B2CSession.AgentService;
                    if (strPassenger.Length > 0)
                    {
                        xmlPassenger = strPassenger.Replace("***create_by***", objUv.UserId.ToString()).Replace("***update_by***", objUv.UserId.ToString()).Replace("***client_profile_id***", sClient.client_profile_id.ToString());
                    }

                    string xmlBookingRemark = string.Empty;
                    //string xmlFOP = SerializeXML("");
                    bool success = false;
                    // Savve client profile
                    string result = string.Empty;
                    success = SobjCient.ClientSave(xmlClient, xmlPassenger, xmlBookingRemark);

                    if (success == true)
                    {
                        ClientLogon(sClient.client_number.ToString(), sClient.client_password);
                        if (statusPWS == "change")
                        {
                            if (url != "")
                                SendMaintainMail(sClient.contact_email,
                                                title.Split('|')[0],
                                                sClient.firstname,
                                                sClient.lastname,
                                                sClient.client_number.ToString(),
                                                "",
                                                "change",
                                                url.Substring(0, url.LastIndexOf('/')),
                                                sClient.client_password,
                                                objUv.UserId);
                            else
                                SendMaintainMail(sClient.contact_email,
                                                title.Split('|')[0],
                                                sClient.firstname,
                                                sClient.lastname,
                                                sClient.client_number.ToString(),
                                                "",
                                                "change",
                                                url,
                                                sClient.client_password,
                                                objUv.UserId);
                        }
                    }
                    return success;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Title : " + title +
                                         "<br/>Gender : " + gender +
                                         "<br/>Firstname : " + first_name +
                                         "<br/>Lastname : " + last_name +
                                         "<br/>Date of Birth : " + dateOfBirth +
                                         "<br/>Nationality : " + nationality +
                                         "<br/>Document Type : " + documenttype +
                                         "<br/>Passport Number : " + passportnumber +
                                         "<br/>Issue Place : " + passportIssuePlace +
                                         "<br/>Birth Place : " + passportBirthPlace +
                                         "<br/>Issue Date : " + passportIssueDate +
                                         "<br/>Expiry Date : " + passportExpiryDate +
                                         "<br/>Mobile : " + phoneMobile +
                                         "<br/>Home : " + phoneHome +
                                         "<br/>Business : " + phoneBusiness +
                                         "<br/>Email : " + contactEmail +
                                         "<br/>Language : " + language +
                                         "<br/>Address 1 : " + addressline1 +
                                         "<br/>Address 2 : " + addressline2 +
                                         "<br/>Zip : " + zipCode +
                                         "<br/>City : " + city +
                                         "<br/>Country : " + country +
                                         "<br/>Passenger Type : " + PassengerType +
                                         "<br/>Passenger : " + strPassenger +
                                         "<br/>Url : " + url);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public B2CVariable GetSessionVariable()
        {
            return B2CSession.Variable != null ? B2CSession.Variable : new B2CVariable();
        }

        [WebMethod(EnableSession = true)]
        public void ClearVariable()
        {
            if (B2CSession.Variable != null)
            {
                B2CVariable v = B2CSession.Variable;
                v.OneWay = false;
                v.OriginName = "";
                v.OriginRcd = "";
                v.DestinationName = "";
                v.DestinationRcd = "";
                v.DepartureDate = "";
                v.ReturnDate = "";
                v.Adult = 0;
                v.Child = 0;
                v.Infant = 0;
                v.Other = 0;
                v.OtherPassengerType = "";
                v.SearchCurrencyRcd = "";
            }
        }

        private Passengers GetPassengerSave(string booking_id, string client_profile_id, string client_number)
        {
            try
            {
                string xmlPassenger = "";
                ServiceClient obj = new ServiceClient();
                obj.objService = B2CSession.AgentService;
                DataSet ds = obj.GetClientPassenger(booking_id, client_profile_id, client_number);

                xmlPassenger = new Helper().ConvertDataTableToXML(ds.Tables["Passenger"]);
                xmlPassenger = xmlPassenger.Replace("<NewDataSet>", "<ArrayOfPassenger>");
                xmlPassenger = xmlPassenger.Replace("</NewDataSet>", "</ArrayOfPassenger>");
                Passengers p = (Passengers)XmlHelper.Deserialize(xmlPassenger, typeof(Passengers));
                return p;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Booking Id<br/>" + booking_id + "<br/>Client Profile Id<br/>" + client_profile_id + "<br/>Client Number<br/>" + client_number);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public bool PassengerAdd(string title, string first_name, string last_name, string dateOfBirth,
                                 string documenttype, string passportnumber, string passportIssuePlace,
                                 string passportBirthPlace, string passportIssueDate, string passportExpiryDate,
                                 string nationality_rcd, string PassengerRole, string PassengerType, string IssueCountry)
        {

            try
            {

                Client sClient = B2CSession.Client;
                Passengers objPassenger = new Passengers();
                B2CVariable objUv = B2CSession.Variable;

                objPassenger.Add(FillPassengerProfile(objUv.UserId.ToString(), first_name, last_name, title, "ADD", sClient.client_number, documenttype, dateOfBirth, passportExpiryDate, passportIssueDate, nationality_rcd, sClient.client_profile_id, sClient.client_number, PassengerRole, PassengerType, IssueCountry, passportnumber, passportBirthPlace, passportIssuePlace));

                Library objLi = new Library();
                sClient.update_date_time = DateTime.Now;

                string xmlClient = XmlHelper.Serialize(sClient, false);
                string xmlPassenger = XmlHelper.Serialize(objPassenger, false);
                string xmlBookingRemark = string.Empty;
                //string xmlFOP = SerializeXML("");
                bool success = false;
                // Savve client profile
                string result = string.Empty;
                xmlClient = "";
                // Clients objClients = new Clients();
                ServiceClient SobjCient = new ServiceClient();
                string feesXml = string.Empty;
                SobjCient.objService = B2CSession.AgentService;
                success = SobjCient.AddClientPassengerList(xmlClient, xmlPassenger, xmlBookingRemark);
                return success;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Title : " + title +
                                        "<br/>Firstname : " + first_name +
                                        "<br/>Lastname : " + last_name +
                                        "<br/>Date Of Birth : " + dateOfBirth +
                                        "<br/>Document Type : " + documenttype +
                                        "<br/>Passport Number : " + passportnumber +
                                        "<br/>Issue Palce : " + passportIssuePlace +
                                        "<br/>Birth Place : " + passportBirthPlace +
                                        "<br/>Issue Date : " + passportIssueDate +
                                        "<br/>Expiry Date : " + passportExpiryDate +
                                        "<br/>Nationality : " + nationality_rcd +
                                        "<br/>Role : " + PassengerRole +
                                        "<br/>Passenger Type : " + PassengerType +
                                        "<br/>IssueCountry : " + IssueCountry);
                throw ex;
            }
        }

        private void SendMaintainMail(string mailto, string title, string firstname,
                                      string lastname, string username, string company,
                                      string action, string url, string password, Guid userId)
        {
            Helper objHelper = new Helper();

            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                Library objLi = new Library();

                string mailFrom = "";
                string mailSubject = "";
                string strHMTLBody = "";

                mailFrom = B2CSetting.EmailFrom;

                switch (action)
                {
                    case "create": mailSubject = Classes.Language.Value(B2CSetting.FFPCreateEmailSubject, B2CSetting.FFPCreateEmailSubject, _stdLanguage); break;
                    case "reset":
                        mailSubject = Classes.Language.Value(B2CSetting.FFPResetEmailSubject, B2CSetting.FFPResetEmailSubject, _stdLanguage);
                        break;
                    case "change":
                        mailSubject = Classes.Language.Value(B2CSetting.FFPChangeEmailSubject, B2CSetting.FFPChangeEmailSubject, _stdLanguage);
                        break;
                }

                string a = HttpContext.Current.Request.Url.AbsoluteUri.ToString();
                string myXmlMail = string.Format("<MailDetail><Title>{0}</Title><FirstName>{1}</FirstName><LastName>{2}</LastName><UserName>{3}" +
                                                "</UserName><Company>{4}</Company><Action>{5}</Action><Password>{6}</Password><HeaderURL>" + url + "</HeaderURL>" +
                                                "<FooterURL>" + url + "</FooterURL></MailDetail>",
                                                HTMLEncodeSpecialChars(title), HTMLEncodeSpecialChars(firstname), HTMLEncodeSpecialChars(lastname), HTMLEncodeSpecialChars(username),
                                                HTMLEncodeSpecialChars(company), action, HTMLEncodeSpecialChars(password));

                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objHelper.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                objTransform = objHelper.GetXSLMail(Classes.Language.CurrentCode().ToLower(), "email_maintain");
                strHMTLBody = objLi.RenderHtml(objTransform, objArgument, myXmlMail);

                objHelper.SendMail(mailFrom,
                                    mailto,
                                    mailSubject,
                                    strHMTLBody,
                                    string.Empty,
                                    userId.ToString(),
                                    false,
                                    false);
            }
            catch (Exception ex)
            {
                objHelper.SendErrorEmail(ex, "Mail To : " + mailto +
                                            "<br/>Title : " + title +
                                            "<br/>Firstname : " + firstname +
                                            "<br/>Lastname : " + lastname +
                                            "<br/>Username : " + username +
                                            "<br/>Company : " + company +
                                            "<br/>Action : " + action +
                                            "<br/>Url : " + url);
                throw ex;
            }
        }
        #endregion

        #region Menu
        [WebMethod(EnableSession = true)]
        public string LoadMenu()
        {
            try
            {
                string strResult = string.Empty;

                Library objLi = new Library();
                strResult = objLi.GenerateControlString("UserControls/menu.ascx", string.Empty);

                return strResult;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        #endregion

        #region News Register

        private class RegisterDB
        {
            private OleDbConnection connection;
            public RegisterDB(string connString)
            {
                try
                {
                    this.connection = new OleDbConnection(connString);
                }
                catch (Exception ex)
                {
                    Classes.Helper objHp = new Classes.Helper();
                    objHp.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }

            public DataSet GetDataset(string SqlStatement)
            {
                DataSet dataset = new DataSet();
                try
                {
                    if (this.connection.State != ConnectionState.Open)
                    {
                        this.connection.Close();
                        this.connection.Open();
                    }
                    OleDbDataAdapter adapter = new OleDbDataAdapter(SqlStatement, this.connection);
                    adapter.Fill(dataset);
                    return dataset;
                }
                catch (Exception ex)
                {
                    Classes.Helper objHp = new Classes.Helper();
                    objHp.SendErrorEmail(ex, "SQL Statement<br/>" + SqlStatement);
                    throw ex;
                }
            }

            public int ExecuteSQL(string SqlStatement)
            {
                int result;
                OleDbCommand command = new OleDbCommand();
                try
                {
                    if (SqlStatement == "" || SqlStatement == null) return -1;

                    command.Connection = this.connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = SqlStatement;

                    if (this.connection.State != ConnectionState.Open)
                    {
                        this.connection.Close();
                        this.connection.Open();
                    }
                    result = command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Classes.Helper objHp = new Classes.Helper();
                    objHp.SendErrorEmail(ex, "SQL Statement<br/>" + SqlStatement);
                    throw ex;
                }
                finally
                {
                    command.Dispose();
                }
                return result;
            }
            public void Close()
            {
                try
                {
                    this.connection.Close();
                    this.connection.Dispose();
                }
                catch (Exception ex)
                {
                    Classes.Helper objHp = new Classes.Helper();
                    objHp.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetNewsRegisterDetail()
        {
            try
            {
                Library li = new Library();
                return li.GenerateControlString("UserControls/NewsRegisterDetail.ascx", string.Empty);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string FillNewsRegisterData(string xml)
        {
            try
            {
                XPathDocument xmlDoc = new XPathDocument(new StringReader(xml));
                XPathNavigator nv;

                string strSqlConnect = "PROVIDER=Microsoft.Jet.OLEDB.4.0;DATA SOURCE=" + Server.MapPath("~") + B2CSetting.NewsRegisterDB;

                Library li = new Library();

                nv = xmlDoc.CreateNavigator();
                string strSql = string.Empty;

                //Fill contact information
                foreach (XPathNavigator n in nv.Select("register/contact"))
                {
                    strSql = "INSERT INTO [Register_profile] (" +
                                   "[Register_name]" +
                                   ",[Register_email]" +
                                   ",[Register_postcode]" +
                                   ",[Country_name]" +
                                   ",[Departure_name]" +
                                   ",[Destination_name]" +
                                   ",[Travel_for]" +
                                   ",[Have_childern]" +
                                   ",[IP_address]" +
                                   ",[Browser]" +
                                   ")VALUES('" +
                                   li.getXPathNodevalue(n, "name", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "email", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "postcode", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "country", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "departure", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "destination", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "travelfor", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "havechildren", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "ipaddress", Library.xmlReturnType.value) + "','" +
                                   li.getXPathNodevalue(n, "browser", Library.xmlReturnType.value) + "')";
                }

                RegisterDB registerDB = new RegisterDB(strSqlConnect);
                registerDB.ExecuteSQL(strSql);
                registerDB.Close();

                return "complete";
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Xml Data<br/>" + xml.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveUnsubscribe(string email)
        {
            try
            {
                string strSqlConnect = "PROVIDER=Microsoft.Jet.OLEDB.4.0;DATA SOURCE=" + Server.MapPath("~") + B2CSetting.NewsRegisterDB;
                string strSqlInsert = " update Register_profile set Email_flag='1', Update_mail_date = Now where Register_email ='" +
                                        email.Trim() + "' ";

                RegisterDB registerDB = new RegisterDB(strSqlConnect);
                registerDB.ExecuteSQL(strSqlInsert);
                registerDB.Close();

                return "complete";
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Email : " + email);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetNewsAdmin()
        {
            try
            {
                Library li = new Library();
                return li.GenerateControlString("UserControls/NewsAdmin.ascx", string.Empty);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string NewsAdminLogin(string xml)
        {
            try
            {
                XPathDocument xmlDoc = new XPathDocument(new StringReader(xml));
                XPathNavigator nv;

                string strSqlConnect = "PROVIDER=Microsoft.Jet.OLEDB.4.0;DATA SOURCE=" + Server.MapPath("~") + B2CSetting.NewsRegisterDB;

                Library li = new Library();

                B2CSession.Login = false;
                nv = xmlDoc.CreateNavigator();
                string strSql = string.Empty;
                string password = "";
                //Fill contact information
                foreach (XPathNavigator n in nv.Select("newsLogin"))
                {
                    strSql = "select * from Register_user where Username ='" +
                                   li.getXPathNodevalue(n, "UserId", Library.xmlReturnType.value) + "'";
                    password = li.getXPathNodevalue(n, "Password", Library.xmlReturnType.value);
                }
                RegisterDB registerDB = new RegisterDB(strSqlConnect);
                DataSet ds = registerDB.GetDataset(strSql);
                registerDB.Close();

                if (ds.Tables[0].Rows.Count < 1)
                    return "incorrect User Id !!!";
                if (Convert.ToString(ds.Tables[0].Rows[0]["Password"]).Trim() != password)
                    return "incorrect password !!!";
                B2CSession.Login = true;
                return "complete";
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Xml Data<br/>" + xml.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SearchNewsRegister(string strCondition, string pageNo)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                string strSqlConnect = "PROVIDER=Microsoft.Jet.OLEDB.4.0;DATA SOURCE=" + Server.MapPath("~") + B2CSetting.NewsRegisterDB;
                RegisterDB registerDB = new RegisterDB(strSqlConnect);

                string strSql = "";
                DataSet ds = new DataSet();

                strSql = "select ConfigValue from Register_config where ConfigName = 'SenderEmailFrom'";
                ds = registerDB.GetDataset(strSql);
                B2CSession.SenderEmailFrom = Convert.ToString(ds.Tables[0].Rows[0]["ConfigValue"]);

                strSql = "select ConfigValue from Register_config where ConfigName = 'SenderEmailTo'";
                ds = registerDB.GetDataset(strSql);
                B2CSession.SenderEmailTo = Convert.ToString(ds.Tables[0].Rows[0]["ConfigValue"]);

                strSql = "select ConfigValue from Register_config where ConfigName = 'NumberOfBccGroup'";
                ds = registerDB.GetDataset(strSql);
                B2CSession.NumberOfBccGroup = Convert.ToInt16(ds.Tables[0].Rows[0]["ConfigValue"]);

                Int16 pageNumber = 0;
                try { pageNumber = Convert.ToInt16(pageNo); }
                catch { pageNumber = 1; }

                strSql = " select count(*) as recCount from Register_profile " + strCondition;
                DataSet dsCountRegister = registerDB.GetDataset(strSql);
                const Int16 RecPerPage = 500;
                Int16 recCount = Convert.ToInt16(dsCountRegister.Tables[0].Rows[0][0]);

                Int16 pageTotal = (Int16)(recCount / RecPerPage);
                if (pageTotal < (((double)recCount) / RecPerPage)) pageTotal += 1;

                strSql = " select top " + RecPerPage + " *, " + (RecPerPage * (pageNumber - 1)) + " as pageStart " +
                         " from Register_profile " + strCondition;
                if (pageNumber > 1)
                    strSql += " and register_id> ( " +
                                " select max(register_id) from( " +
                                    " select top " + RecPerPage * (pageNumber - 1) + "  register_id from Register_profile " +
                                    strCondition + " order by register_id)) ";
                strSql += " ORDER BY register_id ";

                DataSet dsSearchRegister = registerDB.GetDataset(strSql);
                B2CSession.SearchRegister = dsSearchRegister.Tables[0];
                registerDB.Close();

                XPathDocument xmlDoc = new XPathDocument(new StringReader(dsSearchRegister.GetXml()));
                Library li = new Library();

                string result = li.ExtractItinerary(xmlDoc, "NewDataSet");

                string linkPage = "";
                for (int i = 1; i <= pageTotal; i++)
                {
                    linkPage += "<result_Page>" +
                                "<link_Page>" + (i) + "</link_Page>" +
                                "<pageNumber>" + pageNumber + "</pageNumber>" +
                                "</result_Page>";
                }
                if (recCount > 0)
                    result = result.Replace("</NewDataSet>", "") +
                            linkPage +
                            "<Summary>" +
                                "<record_Count>" + recCount.ToString("#,#0") + "</record_Count>" +
                            "</Summary>" +
                            "</NewDataSet>";

                StringBuilder stbHtml = new StringBuilder();

                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                objTransform = objXsl.GetXSLDocument("NewsRegister");
                stbHtml.Append(li.RenderHtml(objTransform, objArgument, result));

                result = stbHtml.ToString() +
                        "<input type='hidden' id='SenderEmailFrom' value='" + B2CSession.SenderEmailFrom + "' />" +
                        "<input type='hidden' id='SenderEmailTo' value='" + B2CSession.SenderEmailTo + "' />";
                return result;
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Condition<br/>" + strCondition + "<br/>Page Number<br/>" + pageNo);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string DeleteNewsRegister(string register_id)
        {
            try
            {
                string strSqlConnect = "PROVIDER=Microsoft.Jet.OLEDB.4.0;DATA SOURCE=" + Server.MapPath("~") + B2CSetting.NewsRegisterDB;

                if (register_id.Trim() != "")
                {
                    string strSql = "delete from [Register_profile] where Register_id in (" + register_id + ")";

                    RegisterDB registerDB = new RegisterDB(strSqlConnect);
                    registerDB.ExecuteSQL(strSql);
                    registerDB.Close();
                }
                return "complete";
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Register Id : " + register_id);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string NewsRegisterSendMail(string xml)
        {
            try
            {
                XPathDocument xmlDoc = new XPathDocument(new StringReader(xml));
                XPathNavigator nv;

                Library li = new Library();

                nv = xmlDoc.CreateNavigator();

                //Fill contact information
                foreach (XPathNavigator n in nv.Select("SendMail"))
                {

                    string strMailForm = li.getXPathNodevalue(n, "MailFrom", Library.xmlReturnType.value);
                    string strMailTo = li.getXPathNodevalue(n, "MailTo", Library.xmlReturnType.value);
                    string strMailSubject = li.getXPathNodevalue(n, "MailSubject", Library.xmlReturnType.value);
                    string strMailMessage = li.getXPathNodevalue(n, "MailMessage", Library.xmlReturnType.value);
                    string strMailBcc = li.getXPathNodevalue(n, "MailBcc", Library.xmlReturnType.value);
                    string strMailAddBcc = li.getXPathNodevalue(n, "MailAddBcc", Library.xmlReturnType.value);

                    strMailMessage = strMailMessage.Replace("\r\n", "<br>");
                    StreamReader sr = new StreamReader(Server.MapPath("~") + "/HTML/registerMailForm.html");
                    string mailForm = sr.ReadToEnd();
                    string mailBody = mailForm.Replace("<%MESSAGE%>", strMailMessage);
                    sr.Dispose();
                    sr = null;

                    string strBCC = "";
                    DataTable dtMail = B2CSession.SearchRegister;
                    string[] txtBCC = strMailBcc.Split(',');
                    for (int i = 0; i < txtBCC.Length; i++)
                    {
                        if (txtBCC[i].Trim() != "")
                        {
                            foreach (DataRow dr in dtMail.Rows)
                            {
                                if ((dr["Email_flag"].ToString() != "1") && (dr["Register_id"].ToString() == txtBCC[i].Trim()))
                                {
                                    strBCC += dr["Register_email"].ToString() + ";";
                                }
                            }
                        }
                    }

                    strBCC += strMailAddBcc;

                    SendMailSmtp(strMailForm, strMailTo, strBCC, strMailSubject, mailBody);
                }

                return "complete";
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Xml Data<br/>" + xml.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        private bool SendMailSmtp(string mailForm, string mailTo, string mailBcc, string mailSubject, string mailBody)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To = mailTo;
                if (mailBcc != "")
                    mail.Bcc = mailBcc;
                mail.From = mailForm;
                mail.BodyEncoding = Encoding.UTF8;
                mail.Subject = mailSubject;
                mail.BodyFormat = MailFormat.Html;
                mail.Body = mailBody;

                SmtpMail.SmtpServer = B2CSetting.SmtpServer;
                SmtpMail.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        #endregion

        #region GroupBooking

        [WebMethod(EnableSession = true)]
        public string LoadGroupBooking()
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library objLi = new Library();
                string result;

                //Clear temp availability
                if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                {
                    //Clear block inventory
                    ServiceClient objSvc = new ServiceClient();
                    objSvc.objService = B2CSession.AgentService;
                    objSvc.ReleaseFlightInventorySession(bookingHeader.booking_id.ToString(), string.Empty, string.Empty, string.Empty, false, true, true);
                    objSvc.objService = null;
                    objSvc = null;
                    //Clear booking in session
                    objLi.ClearBooking(ref bookingHeader,
                                        ref itinerary,
                                        ref passengers,
                                        ref quotes,
                                        ref fees,
                                        ref mappings,
                                        ref services,
                                        ref remarks,
                                        ref payments,
                                        ref taxes);
                }
                objUv.CurrentStep = 1;

                result = objLi.GenerateControlString("UserControls/GroupBooking.ascx", string.Empty);

                return result;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SendMailGroupBooking(string cmbAdult, string cmbChild, string cmbInfant,
                                        string ddlTitle, string txtFirstName, string txtLastName,
                                        string txtContactMobile, string txtContactPhoneNumber,
                                        string txtEmailAddress, string txtAddress1, string txtAddress2,
                                        string txtTownCity, string txtContactCounty, string txtPostcode, string ddlCountry,
                                        string txtOrigin, string txtDestination,
                                        string ctlDepatureDate, string ctlReturnDate,
                                        string cmbPreferedDepartureTime, string cmbPreferedReturnDepartureTime, string txtAdditionalInformation)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                Library objLi = new Library();

                string mailFrom = "";
                string mailSubject = "";
                string strHMTLBody = "";
                string mailTo = "";

                mailFrom = txtEmailAddress;
                mailTo = B2CSetting.GroupEmailTo;
                mailSubject = "Group Booking";

                string myXmlMail = string.Format("<GroupBooking> " +
                                        "<cmbAdult>{0}</cmbAdult> " +
                                        "<cmbChild>{1}</cmbChild> " +
                                        "<cmbInfant>{2}</cmbInfant> " +
                                        "<ddlTitle>{3}</ddlTitle> " +
                                        "<txtFirstName>{4}</txtFirstName> " +
                                        "<txtLastName>{5}</txtLastName> " +
                                        "<txtContactMobile>{6}</txtContactMobile> " +
                                        "<txtContactPhoneNumber>{7}</txtContactPhoneNumber> " +
                                        "<txtEmailAddress>{8}</txtEmailAddress> " +
                                        "<txtAddress1>{9}</txtAddress1> " +
                                        "<txtAddress2>{10}</txtAddress2> " +
                                        "<txtTownCity>{11}</txtTownCity> " +
                                        "<txtContactCounty>{12}</txtContactCounty> " +
                                        "<txtPostcode>{13}</txtPostcode> " +
                                        "<ddlCountry>{14}</ddlCountry> " +
                                        "<txtOrigin>{15}</txtOrigin> " +
                                        "<txtDestination>{16}</txtDestination> " +
                                        "<ctlDepatureDate>{17}</ctlDepatureDate> " +
                                        "<ctlReturnDate>{18}</ctlReturnDate> " +
                                        "<cmbPreferedDepartureTime>{19}</cmbPreferedDepartureTime> " +
                                        "<cmbPreferedReturnDepartureTime>{20}</cmbPreferedReturnDepartureTime> " +
                                        "<txtAdditionalInformation>{21}</txtAdditionalInformation> " +
                                        "</GroupBooking>",
                                        HTMLEncodeSpecialChars(cmbAdult), HTMLEncodeSpecialChars(cmbChild), HTMLEncodeSpecialChars(cmbInfant),
                                        HTMLEncodeSpecialChars(ddlTitle), HTMLEncodeSpecialChars(txtFirstName), HTMLEncodeSpecialChars(txtLastName),
                                        HTMLEncodeSpecialChars(txtContactMobile), HTMLEncodeSpecialChars(txtContactPhoneNumber),
                                        HTMLEncodeSpecialChars(txtEmailAddress),
                                        HTMLEncodeSpecialChars(txtAddress1), HTMLEncodeSpecialChars(txtAddress2),
                                        HTMLEncodeSpecialChars(txtTownCity), HTMLEncodeSpecialChars(txtContactCounty),
                                        HTMLEncodeSpecialChars(txtPostcode), HTMLEncodeSpecialChars(ddlCountry),
                                        HTMLEncodeSpecialChars(txtOrigin), HTMLEncodeSpecialChars(txtDestination),
                                        HTMLEncodeSpecialChars(ctlDepatureDate), HTMLEncodeSpecialChars(ctlReturnDate),
                                        HTMLEncodeSpecialChars(cmbPreferedDepartureTime),
                                        HTMLEncodeSpecialChars(cmbPreferedReturnDepartureTime),
                                        HTMLEncodeSpecialChars(txtAdditionalInformation));

                myXmlMail = myXmlMail.Replace("&", "&amp;");

                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                objTransform = objXsl.GetXSLDocument("GroupBookingEmail");
                strHMTLBody = objLi.RenderHtml(objTransform, objArgument, myXmlMail);

                if (SendMailSmtp(mailFrom, mailTo, "", mailSubject, strHMTLBody))
                    return "1";
                return null;
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Adult : " + cmbAdult +
                                        "<br/>Child : " + cmbChild +
                                        "<br/>infant : " + cmbInfant +
                                        "<br/>Title : " + ddlTitle +
                                        "<br/>Firstname : " + txtFirstName +
                                        "<br/>Lastname : " + txtLastName +
                                        "<br/>Mobile : " + txtContactMobile +
                                        "<br/>Phone : " + txtContactPhoneNumber +
                                        "<br/>Email : " + txtEmailAddress +
                                        "<br/>Address 1 : " + txtAddress1 +
                                        "<br/>Address 2 : " + txtAddress2 +
                                        "<br/>City : " + txtTownCity +
                                        "<br/>Contact Country : " + txtContactCounty +
                                        "<br/>Post Code : " + txtPostcode +
                                        "<br/>Country : " + ddlCountry +
                                        "<br/>Origin : " + txtOrigin +
                                        "<br/>Destination : " + txtDestination +
                                        "<br/>Departure Date : " + ctlDepatureDate +
                                        "<br/>Return Date : " + ctlReturnDate +
                                        "<br/>Departure Time : " + cmbPreferedDepartureTime +
                                        "<br/>Return Time : " + cmbPreferedReturnDepartureTime +
                                        "<br/>Information : " + txtAdditionalInformation);
                throw ex;
            }
        }

        #endregion

        #region CharterFlight

        [WebMethod(EnableSession = true)]
        public string LoadCharterFlight()
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library objLi = new Library();
                string result;

                //Clear temp availability
                if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                {
                    //Clear block inventory
                    ServiceClient objSvc = new ServiceClient();
                    objSvc.objService = B2CSession.AgentService;
                    objSvc.ReleaseFlightInventorySession(bookingHeader.booking_id.ToString(), string.Empty, string.Empty, string.Empty, false, true, true);
                    objSvc.objService = null;
                    objSvc = null;
                    //Clear booking in session
                    objLi.ClearBooking(ref bookingHeader,
                                        ref itinerary,
                                        ref passengers,
                                        ref quotes,
                                        ref fees,
                                        ref mappings,
                                        ref services,
                                        ref remarks,
                                        ref payments,
                                        ref taxes);
                }
                objUv.CurrentStep = 1;
                result = objLi.GenerateControlString("UserControls/CharterFlight.ascx", string.Empty);

                return result;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SendMailCharterFlight(string ddlLeadPassengerTitle, string tbLeadPassengerFirstName,
                                        string tbLeadPassengerLastName, string tbLeadPassengerAddress1, string tbLeadPassengerAddress2,
                                        string tbLeadPassengerPostalTown, string ddlLeadPassengerCountry, string tbLeadPassengerPostcode,
                                        string tbLeadPassengerContactPhoneNumber, string tbLeadPassengerEmailAddress, string tboOrigin,
                                        string tboDestination, string ctlDepatureDate,
                                        string ctlReturnDate, string txtAdditionalInformation, string tboCompanyName,
                                        string cmbTypeOfRequest, string cmbTypeOfTrip, string cmbPreferedDepartureTime,
                                        string cmbPreferedReturnDepartureTime, string tbLeadPassengerContactMobile, string tbLeadPassengerContactCounty,
                                        string tboTotalPassenger)
        {
            Helper objXsl = new Helper();
            System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
            try
            {
                Library objLi = new Library();

                string mailFrom = "";
                string mailSubject = "";
                string strHMTLBody = "";
                string mailTo = "";

                mailFrom = tbLeadPassengerEmailAddress;
                mailTo = B2CSetting.CharterEmailTo;
                mailSubject = "Charter Flight";

                string myXmlMail = string.Format("<CharterFlights><CharterFlight>  " +
                                      " <ddlLeadPassengerTitle>{0}</ddlLeadPassengerTitle> " +
                                      "<tbLeadPassengerFirstName>{1}</tbLeadPassengerFirstName> " +
                                      "<tbLeadPassengerLastName>{2}</tbLeadPassengerLastName> " +
                                      "<tbLeadPassengerAddress1>{3}</tbLeadPassengerAddress1> " +
                                      "<tbLeadPassengerAddress2>{4}</tbLeadPassengerAddress2> " +
                                      "<tbLeadPassengerPostalTown>{5}</tbLeadPassengerPostalTown> " +
                                      "<ddlLeadPassengerCountry>{6}</ddlLeadPassengerCountry> " +
                                      "<tbLeadPassengerPostcode>{7}</tbLeadPassengerPostcode> " +
                                      "<tbLeadPassengerContactPhoneNumber>{8}</tbLeadPassengerContactPhoneNumber> " +
                                      "<tbLeadPassengerEmailAddress>{9}</tbLeadPassengerEmailAddress> " +
                                      "<tboOrigin>{10}</tboOrigin> " +
                                      "<tboDestination>{11}</tboDestination> " +
                                      "<ctlDepatureDate>{12}</ctlDepatureDate> " +
                                      "<ctlReturnDate>{13}</ctlReturnDate> " +
                                      "<txtAdditionalInformation>{14}</txtAdditionalInformation> " +
                                      "<tboCompanyName>{15}</tboCompanyName> " +
                                      "<cmbTypeOfRequest>{16}</cmbTypeOfRequest> " +
                                      "<cmbTypeOfTrip>{17}</cmbTypeOfTrip> " +
                                      "<cmbPreferedDepartureTime>{18}</cmbPreferedDepartureTime> " +
                                      "<cmbPreferedReturnDepartureTime>{19}</cmbPreferedReturnDepartureTime> " +
                                      "<tbLeadPassengerContactMobile>{20}</tbLeadPassengerContactMobile> " +
                                      "<tbLeadPassengerContactCounty>{21}</tbLeadPassengerContactCounty> " +
                                      "<tboTotalPassenger>{22}</tboTotalPassenger> " +
                                      " </CharterFlight></CharterFlights> ",
                                                HTMLEncodeSpecialChars(ddlLeadPassengerTitle), HTMLEncodeSpecialChars(tbLeadPassengerFirstName), HTMLEncodeSpecialChars(tbLeadPassengerLastName), HTMLEncodeSpecialChars(tbLeadPassengerAddress1), HTMLEncodeSpecialChars(tbLeadPassengerAddress2), HTMLEncodeSpecialChars(tbLeadPassengerPostalTown), HTMLEncodeSpecialChars(ddlLeadPassengerCountry), HTMLEncodeSpecialChars(tbLeadPassengerPostcode),
                                                HTMLEncodeSpecialChars(tbLeadPassengerContactPhoneNumber), HTMLEncodeSpecialChars(tbLeadPassengerEmailAddress), HTMLEncodeSpecialChars(tboOrigin), HTMLEncodeSpecialChars(tboDestination), HTMLEncodeSpecialChars(ctlDepatureDate), HTMLEncodeSpecialChars(ctlReturnDate), HTMLEncodeSpecialChars(txtAdditionalInformation), HTMLEncodeSpecialChars(tboCompanyName),
                                                HTMLEncodeSpecialChars(cmbTypeOfRequest), HTMLEncodeSpecialChars(cmbTypeOfTrip), HTMLEncodeSpecialChars(cmbPreferedDepartureTime), HTMLEncodeSpecialChars(cmbPreferedReturnDepartureTime), HTMLEncodeSpecialChars(tbLeadPassengerContactMobile), HTMLEncodeSpecialChars(tbLeadPassengerContactCounty), HTMLEncodeSpecialChars(tboTotalPassenger));
                myXmlMail = myXmlMail.Replace("&", "&amp;");

                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                objTransform = objXsl.GetXSLDocument("CharterFlightEmail");
                strHMTLBody = objLi.RenderHtml(objTransform, objArgument, myXmlMail);

                if (SendMailSmtp(mailFrom, mailTo, "", mailSubject, strHMTLBody))
                    return "1";
                return null;
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Title : " + ddlLeadPassengerTitle +
                                        "<br/>Firstname : " + tbLeadPassengerFirstName +
                                        "<br/>Firstname : " + tbLeadPassengerLastName +
                                        "<br/>Firstname : " + tbLeadPassengerAddress1 +
                                        "<br/>Firstname : " + tbLeadPassengerAddress2 +
                                        "<br/>Firstname : " + tbLeadPassengerPostalTown +
                                        "<br/>Firstname : " + ddlLeadPassengerCountry +
                                        "<br/>Firstname : " + tbLeadPassengerPostcode +
                                        "<br/>Firstname : " + tbLeadPassengerContactPhoneNumber +
                                        "<br/>Firstname : " + tbLeadPassengerEmailAddress +
                                        "<br/>Firstname : " + tboOrigin +
                                        "<br/>Firstname : " + tboDestination +
                                        "<br/>Firstname : " + ctlDepatureDate +
                                        "<br/>Firstname : " + ctlReturnDate +
                                        "<br/>Firstname : " + txtAdditionalInformation +
                                        "<br/>Firstname : " + tboCompanyName +
                                        "<br/>Firstname : " + cmbTypeOfRequest +
                                        "<br/>Firstname : " + cmbTypeOfTrip +
                                        "<br/>Firstname : " + cmbPreferedDepartureTime +
                                        "<br/>Firstname : " + cmbPreferedReturnDepartureTime +
                                        "<br/>Firstname : " + tbLeadPassengerContactMobile +
                                        "<br/>Firstname : " + tbLeadPassengerContactCounty +
                                        "<br/>Firstname : " + tboTotalPassenger);
                throw ex;
            }
        }

        #endregion

        #region BonVoyageBreak

        [WebMethod(EnableSession = true)]
        public string LoadBonVoyageBreak()
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library objLi = new Library();
                string result;

                //Clear temp availability
                if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                {
                    //Clear block inventory
                    ServiceClient objSvc = new ServiceClient();
                    objSvc.objService = B2CSession.AgentService;
                    objSvc.ReleaseFlightInventorySession(bookingHeader.booking_id.ToString(), string.Empty, string.Empty, string.Empty, false, true, true);
                    objSvc.objService = null;
                    objSvc = null;
                    //Clear booking in session
                    objLi.ClearBooking(ref bookingHeader,
                                        ref itinerary,
                                        ref passengers,
                                        ref quotes,
                                        ref fees,
                                        ref mappings,
                                        ref services,
                                        ref remarks,
                                        ref payments,
                                        ref taxes);
                }
                objUv.CurrentStep = 1;
                result = objLi.GenerateControlString("UserControls/BonVoyageBreak.ascx", string.Empty);

                return result;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SendMailBonVoyageBreak(string ddlTitle, string txtLeadName,
                                        string txtTelephone, string txtEmailAddress,
                                        string ddlTravelingFrom, string ddlDestination,
                                        string ddlHotel1, string ddlHotel2,
                                        string ddlRoomType, string ddlOptions,
                                        string txtAdultNumber, string txtChildrenNumber,
                                        string txtChildrenAge, string txtNightNumber,
                                        string ctlArrivalDate, string cmbPreferedArrivalTime,
                                        string ctlDepatureDate, string cmbPreferedDepartureTime,
                                        string ddlCarHireGroup, string ddlRentalPeriod,
                                        string ddlPickUp, string ddlDropOff,
                                        string txtAdditionalInformation)
        {
            Helper objXsl = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                Library objLi = new Library();

                string mailFrom = "";
                string mailSubject = "";
                string strHMTLBody = "";
                string mailTo = "";

                mailFrom = txtEmailAddress;
                mailTo = B2CSetting.BonVoyageEmailTo;
                mailSubject = "BonVoyage Break";

                string myXmlMail = string.Format("<BonVoyageBreak> " +
                                        "<ddlTitle>{0}</ddlTitle> " +
                                        "<txtLeadName>{1}</txtLeadName> " +
                                        "<txtTelephone>{2}</txtTelephone> " +
                                        "<txtEmailAddress>{3}</txtEmailAddress> " +
                                        "<ddlTravelingFrom>{4}</ddlTravelingFrom> " +
                                        "<ddlDestination>{5}</ddlDestination> " +
                                        "<ddlHotel1>{6}</ddlHotel1> " +
                                        "<ddlHotel2>{7}</ddlHotel2> " +
                                        "<ddlRoomType>{8}</ddlRoomType> " +
                                        "<ddlOptions>{9}</ddlOptions> " +
                                        "<txtAdultNumber>{10}</txtAdultNumber> " +
                                        "<txtChildrenNumber>{11}</txtChildrenNumber> " +
                                        "<txtChildrenAge>{12}</txtChildrenAge> " +
                                        "<txtNightNumber>{13}</txtNightNumber> " +
                                        "<ctlArrivalDate>{14}</ctlArrivalDate> " +
                                        "<cmbPreferedArrivalTime>{15}</cmbPreferedArrivalTime> " +
                                        "<ctlDepatureDate>{16}</ctlDepatureDate> " +
                                        "<cmbPreferedDepartureTime>{17}</cmbPreferedDepartureTime> " +
                                        "<ddlCarHireGroup>{18}</ddlCarHireGroup> " +
                                        "<ddlRentalPeriod>{19}</ddlRentalPeriod> " +
                                        "<ddlPickUp>{20}</ddlPickUp> " +
                                        "<ddlDropOff>{21}</ddlDropOff> " +
                                        "<txtAdditionalInformation>{22}</txtAdditionalInformation> " +
                                        "</BonVoyageBreak>",
                                        HTMLEncodeSpecialChars(ddlTitle), HTMLEncodeSpecialChars(txtLeadName),
                                        HTMLEncodeSpecialChars(txtTelephone), HTMLEncodeSpecialChars(txtEmailAddress),
                                        HTMLEncodeSpecialChars(ddlTravelingFrom), HTMLEncodeSpecialChars(ddlDestination),
                                        HTMLEncodeSpecialChars(ddlHotel1), HTMLEncodeSpecialChars(ddlHotel2),
                                        HTMLEncodeSpecialChars(ddlRoomType), HTMLEncodeSpecialChars(ddlOptions),
                                        HTMLEncodeSpecialChars(txtAdultNumber), HTMLEncodeSpecialChars(txtChildrenNumber),
                                        HTMLEncodeSpecialChars(txtChildrenAge), HTMLEncodeSpecialChars(txtNightNumber),
                                        HTMLEncodeSpecialChars(ctlArrivalDate), HTMLEncodeSpecialChars(cmbPreferedArrivalTime),
                                        HTMLEncodeSpecialChars(ctlDepatureDate), HTMLEncodeSpecialChars(cmbPreferedDepartureTime),
                                        HTMLEncodeSpecialChars(ddlCarHireGroup), HTMLEncodeSpecialChars(ddlRentalPeriod),
                                        HTMLEncodeSpecialChars(ddlPickUp), HTMLEncodeSpecialChars(ddlDropOff),
                                        HTMLEncodeSpecialChars(txtAdditionalInformation));
                myXmlMail = myXmlMail.Replace("&", "&amp;");

                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                objTransform = objXsl.GetXSLDocument("BonVoyageBreakEmail");
                strHMTLBody = objLi.RenderHtml(objTransform, objArgument, myXmlMail);

                if (SendMailSmtp(mailFrom, mailTo, "", mailSubject, strHMTLBody))
                    return "1";
                return null;
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Title : " + ddlTitle +
                                        "<br/>Leadname : " + txtLeadName +
                                        "<br/>Telephone : " + txtTelephone +
                                        "<br/>Email : " + txtEmailAddress +
                                        "<br/>travelling From : " + ddlTravelingFrom +
                                        "<br/>Destination : " + ddlDestination +
                                        "<br/>Hotel 1 : " + ddlHotel1 +
                                        "<br/>Hotel 2 : " + ddlHotel2 +
                                        "<br/>Room Type : " + ddlRoomType +
                                        "<br/>Option : " + ddlOptions +
                                        "<br/>Adult : " + txtAdultNumber +
                                        "<br/>Child : " + txtChildrenNumber +
                                        "<br/>Child age : " + txtChildrenAge +
                                        "<br/>Night Number : " + txtNightNumber +
                                        "<br/>Arrival Date : " + ctlArrivalDate +
                                        "<br/>Arrival Time : " + cmbPreferedArrivalTime +
                                        "<br/>Departure Date : " + ctlDepatureDate +
                                        "<br/>Departure Time : " + cmbPreferedDepartureTime +
                                        "<br/>Car Hire : " + ddlCarHireGroup +
                                        "<br/>Rental Period : " + ddlRentalPeriod +
                                        "<br/>Pick Up : " + ddlPickUp +
                                        "<br/>Drop Off : " + ddlDropOff +
                                        "<br/>Information : " + txtAdditionalInformation);
                throw ex;
            }
        }

        #endregion

        #region Insurance
        [WebMethod(EnableSession = true)]
        public string ShowInsurance()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/Insurance.ascx", string.Empty);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string RequestPriceInsurance()
        {
            try
            {
                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("InsuranceMondialSetting"); ;
                if (Setting != null)
                {
                    string requestURL = Setting["InsuranceRequestPriceURL"].ToString();
                    string responseFromServer = RequestInsuranceService(requestURL, GenerateRequestData()).Replace("xmlns=\"http://www.mondial-assistance.com/ecommerce/schema/\"", "");
                    B2CSession.responsePrice = responseFromServer;
                    return responseFromServer;
                }
                else
                {
                    return "{400}";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                return "{004}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string ACEQuoteRequest(DateTime dtDateTime)
        {
            B2CVariable objUv = B2CSession.Variable;
            try
            {
                Library objLi = new Library();
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Passengers passengers = B2CSession.Passengers;
                Itinerary itinerary = B2CSession.Itinerary;
                Fees objFees = B2CSession.Fees;
                Agents objAgents = B2CSession.Agents;

                if (objUv == null ||
                   bookingHeader == null ||
                   (passengers == null || passengers.Count == 0) &&
                   (itinerary == null || itinerary.Count == 0) &&
                   (objAgents == null || objAgents.Count == 0) &&
                   (string.IsNullOrEmpty(objUv.DepartureDate) || objUv.DepartureDate.Length != 8) ||
                   objUv.OneWay == false && (string.IsNullOrEmpty(objUv.ReturnDate) || objUv.ReturnDate.Length != 8))
                {
                    return "{\"ErrorCode\":\"004\", \"Message\":\"Session Timeout object not found\", \"CurrencyCode\":\"\", \"FlightType\":\"\", \"LanguageCode\":\"\"}";
                }
                else
                {
                    int iAgencyTimeZone = 0;
                    int iSystemTimeZone = 0;

                    Insurances objInsurances = null;

                    Routes routes = new Routes();

                    DateTime dtDept = new DateTime(Convert.ToInt16(objUv.DepartureDate.Substring(0, 4)),
                                                   Convert.ToInt16(objUv.DepartureDate.Substring(4, 2)),
                                                   Convert.ToInt16(objUv.DepartureDate.Substring(6, 2)));

                    DateTime dtReturn = DateTime.MinValue;
                    if (string.IsNullOrEmpty(objUv.ReturnDate) == false && objUv.ReturnDate != "0")
                    {
                        dtReturn = new DateTime(Convert.ToInt16(objUv.ReturnDate.Substring(0, 4)),
                                                Convert.ToInt16(objUv.ReturnDate.Substring(4, 2)),
                                                Convert.ToInt16(objUv.ReturnDate.Substring(6, 2)));

                    }

                    //Get agency timezone.
                    iAgencyTimeZone = objAgents[0].agency_timezone;
                    iSystemTimeZone = objAgents[0].system_setting_timezone;

                    if (itinerary[0].od_origin_rcd.Equals("HKG"))
                    {
                        objInsurances = new Insurances(Insurances.InsuranceType.ACENonJapan);
                    }
                    else
                    {
                        objInsurances = new Insurances(Insurances.InsuranceType.ACEJapan);
                    }

                    if (objInsurances.RequestQuote(bookingHeader,
                                                    passengers,
                                                    itinerary,
                                                    objFees,
                                                    dtDateTime,
                                                    dtDept,
                                                    dtReturn,
                                                    routes,
                                                    objUv.OriginRcd,
                                                    objUv.DestinationRcd,
                                                    Classes.Language.CurrentCode(),
                                                    iAgencyTimeZone,
                                                    iSystemTimeZone) == true)
                    {
                        if (objInsurances != null && objInsurances.Count > 0 && objInsurances[0].error_code == "000")
                        {
                            Fee objInsFee = B2CSession.InsuaranceFee;
                            if (objInsFee == null)
                            {
                                objInsurances.objService = B2CSession.AgentService;
                                objInsFee = objInsurances.ReadFee(false);
                                B2CSession.InsuaranceFee = objInsFee;
                            }

                            //Fill Premium to fee.
                            if (objInsurances.FillPremiumToFee(objInsFee) == true)
                            {
                                return "{\"ErrorCode\":\"000\", \"Message\":\"" + objInsurances[0].Fee.fee_amount + "\", \"CurrencyCode\":\"" + bookingHeader.currency_rcd + "\", \"FlightType\":\"" + objInsurances[0].flight_Type + "\", \"Origin\":\"" + itinerary[0].od_origin_rcd + "\",\"LanguageCode\":\"" + Classes.Language.CurrentCode() + "\", \"FeeFound\":" + objInsurances[0].fee_found.ToString().ToLower() + "}";
                            }
                            else
                            {
                                return "{\"ErrorCode\":\"404\", \"Message\":\"Request Quote Failed.\", \"CurrencyCode\":\"\", \"FlightType\":\"\", \"LanguageCode\":\"" + Classes.Language.CurrentCode() + "\", \"FeeFound\":\"" + objInsurances[0].fee_found.ToString().ToLower() + "\"}";
                            }
                        }
                        else
                        {
                            return "{\"ErrorCode\":\"" + objInsurances[0].error_code + "\", \"Message\":\"" + objInsurances[0].error_message + "\", \"CurrencyCode\":\"" + bookingHeader.currency_rcd + "\", \"FlightType\":\"" + objInsurances[0].flight_Type + "\", \"LanguageCode\":\"" + Classes.Language.CurrentCode() + "\", \"FeeFound\":true}";
                        }
                    }
                    else
                    {
                        return "{\"ErrorCode\":\"404\", \"Message\":\"Request Quote Failed.\", \"CurrencyCode\":\"\", \"FlightType\":\"\", \"LanguageCode\":\"" + Classes.Language.CurrentCode() + "\", \"FeeFound\":true}";
                    }
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                if (objUv == null)
                {
                    objHp.SendErrorEmail(ex, "User Variable is null" +
                                             "<br/>Client Date : " + dtDateTime);
                }
                else if (string.IsNullOrEmpty(objUv.DepartureDate))
                {
                    objHp.SendErrorEmail(ex, "DepartureDate is Empty" +
                                             "<br/>Client Date : " + dtDateTime);
                }
                else if (string.IsNullOrEmpty(objUv.ReturnDate))
                {
                    objHp.SendErrorEmail(ex, "ReturnDate is Empty" +
                                             "<br/>Client Date : " + dtDateTime);
                }
                else
                {
                    objHp.SendErrorEmail(ex, "Session Dept Date  : " + objUv.DepartureDate +
                                             "<br/>Session Return Date : " + objUv.ReturnDate +
                                             "<br/>Client Date : " + dtDateTime);
                }


                return "{\"ErrorCode\":\"004\", \"Message\":\"" + ex.Message + "\", \"CurrencyCode\":\"\", \"FlightType\":\"\", \"LanguageCode\":\"" + Classes.Language.CurrentCode() + "\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public bool AcePolicyRequest(BookingHeader bookingHeader,
                                        Itinerary itinerary,
                                        Passengers passengers,
                                        Fees fees,
                                        Payments payments)
        {
            Fee objInsFee = B2CSession.InsuaranceFee;
            B2CVariable objUv = B2CSession.Variable;
            bool IsSuccess = false;

            if (objInsFee != null)
            {
                //generate booking REF before....
                string RecordLocator = "";
                int bookingNumber = 0;
                payments.objService = B2CSession.AgentService;
                payments.objService.GetRecordLocator(ref RecordLocator, ref bookingNumber);
                bookingHeader.record_locator = RecordLocator;
                bookingHeader.booking_number = bookingNumber;
                Agents objAgents = B2CSession.Agents;

                int iAgencyTimeZone = 0;
                int iSystemTimeZone = 0;

                Insurances objInsurances = null;

                if (itinerary[0].od_origin_rcd.Equals("HKG"))
                {
                    objInsurances = new Insurances(Insurances.InsuranceType.ACENonJapan);
                }
                else
                {
                    objInsurances = new Insurances(Insurances.InsuranceType.ACEJapan);
                }

                // pra change routes
                Routes routes = new Routes();
                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("InsuranceACESetting");

                if (string.IsNullOrEmpty(Setting["DomesticCountry"]) == false)
                {
                    routes = CacheHelper.CacheOrigin();
                }
                else
                {
                    routes = CacheHelper.CacheDestination();
                }

                DateTime dtDept = DateTime.MinValue;
                if (string.IsNullOrEmpty(objUv.DepartureDate) == false)
                {
                    dtDept = new DateTime(Convert.ToInt16(objUv.DepartureDate.Substring(0, 4)),
                                          Convert.ToInt16(objUv.DepartureDate.Substring(4, 2)),
                                          Convert.ToInt16(objUv.DepartureDate.Substring(6, 2)));

                }
                DateTime dtReturn = DateTime.MinValue;
                if (string.IsNullOrEmpty(objUv.ReturnDate) == false)
                {
                    dtReturn = new DateTime(Convert.ToInt16(objUv.ReturnDate.Substring(0, 4)),
                                            Convert.ToInt16(objUv.ReturnDate.Substring(4, 2)),
                                            Convert.ToInt16(objUv.ReturnDate.Substring(6, 2)));
                }

                DateTime dtClient = new DateTime();
                string strPurposeOfTravel = "";
                if (string.IsNullOrEmpty(strPurposeOfTravel))
                {
                    //Default value is 2
                    strPurposeOfTravel = "2";
                }
                if (dtClient.Equals(DateTime.MinValue))
                {
                    dtClient = DateTime.Now;
                }

                //Get agency timezone.
                if (objAgents != null && objAgents.Count > 0)
                {
                    iAgencyTimeZone = objAgents[0].agency_timezone;
                    iSystemTimeZone = objAgents[0].system_setting_timezone;
                }
                else
                {
                    Agent objAgent = CacheHelper.CacheAgency(objUv.Agency_Code);
                    iAgencyTimeZone = objAgent.agency_timezone;
                    iSystemTimeZone = objAgent.system_setting_timezone;
                }

                if (objInsurances.RequestPolicy(bookingHeader,
                                                passengers,
                                                itinerary,
                                                dtClient,
                                                dtDept,
                                                dtReturn,
                                                fees,
                                                objUv.OriginRcd,
                                                objUv.DestinationRcd,
                                                Classes.Language.CurrentCode(),
                                                strPurposeOfTravel,
                                                objInsFee.fee_amount_incl, iAgencyTimeZone, iSystemTimeZone) == true)
                {
                    //Perform insurance opertion
                }

                // add policynumber to fee
                if (objInsurances.Count > 0 && objInsurances[0].policy_number != null)
                {
                    // fill policy_number to fee
                    objInsurances.FillPolicyNumberToFee(objInsFee, objInsurances[0].policy_number);

                    IsSuccess = true;
                }
                else
                {
                    if (fees != null && fees.Count > 0)
                    {

                        for (int i = 0; i < fees.Count; i++)
                        {
                            if (fees[i].fee_rcd.Equals("INSU"))
                            {
                                fees.RemoveAt(i);
                            }

                        }
                    }

                    IsSuccess = false;
                }

            }

            return IsSuccess;
        }

        // display ACE passengers
        [WebMethod(EnableSession = true)]
        public string DisplayQuoteACEPassenger()
        {
            string strObj = String.Empty;

            if (B2CSession.ACEQuotePassengers != null)
            {
                ACEQuotePassengers ACEQuotePassengers = new ACEQuotePassengers();
                ACEQuotePassengers = B2CSession.ACEQuotePassengers;

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                strObj = serializer.Serialize(ACEQuotePassengers);
            }

            return strObj;
        }

        [WebMethod(EnableSession = true)]
        public string SubscriptionInsurance(string xmlPayment, string productVariantID, string recordLocator, string requestURL, Passengers objPass)
        {
            try
            {
                string responseFromServer = RequestInsuranceService(requestURL, GenerateSubscription(xmlPayment, productVariantID, recordLocator, objPass));
                return responseFromServer;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string AddInsuranceFee()
        {
            //Add Fee.
            return GetSessionQuoteSummary();
        }
        [WebMethod(EnableSession = true)]
        public string RemoveInsuranceFee()
        {
            //Remove Fee
            return GetSessionQuoteSummary();
        }
        [WebMethod(EnableSession = true)]
        public string CalculateInsuranceFee(bool bGenerateControl, bool bAdd)
        {
            Fees fees = B2CSession.Fees;
            Fee objInsFee = B2CSession.InsuaranceFee;
            if (objInsFee != null)
            {
                fees.ClearInsuranceFee(objInsFee);
                if (bAdd == true)
                {
                    fees.Add(objInsFee);
                }
                if (bGenerateControl == true)
                {
                    return GetSessionQuoteSummary();
                }
            }
            return string.Empty;
        }
        private string RequestInsuranceService(string url, string xmlData)
        {
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                string postData = xmlData;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ContentType = "text/xml";
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        private string GenerateRequestData()
        {
            B2CVariable objUv = B2CSession.Variable;
            Fees fees = B2CSession.Fees;
            Quotes quotes = B2CSession.Quotes;
            Payments payments = B2CSession.Payments;
            Library objLi = new Library();

            string issueDate = DateTime.Now.ToString("dd/MM/yyyy");
            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("InsuranceMondialSetting");
            string salesOrigin = Setting["salesOrigin"].ToString();
            string startDate = objUv.DepartureDate.Substring(6, 2) + "/" + objUv.DepartureDate.Substring(4, 2) + "/" + objUv.DepartureDate.Substring(0, 4);
            string endDate = "";
            if (objUv.ReturnDate != "" && objUv.ReturnDate != "0")
                endDate = objUv.ReturnDate.Substring(6, 2) + "/" + objUv.ReturnDate.Substring(4, 2) + "/" + objUv.ReturnDate.Substring(0, 4);
            else
                endDate = startDate;
            string travelType = "RoundTrip"; // (objBooking.OneWay) ? "OneWayTrip" : "RoundTrip";
            string originLocation = "AU";
            string destinationLocation = "AU";

            // HardCode
            if (objUv.OriginRcd == "HKT")
                originLocation = "TH";
            if (objUv.OriginRcd == "DPS")
                originLocation = "ID";
            if (objUv.DestinationRcd == "HKT")
                destinationLocation = "TH";
            if (objUv.DestinationRcd == "DPS")
                destinationLocation = "ID";

            string totalTravelPrice = objLi.CalOutStandingBalance(quotes, fees, payments).ToString();
            string adult = objUv.Adult.ToString();
            string child = objUv.Child.ToString();
            string infant = objUv.Infant.ToString();

            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<?xml version='1.0' encoding='UTF-8'?>");
            xmlData.Append("    <requestPricing ");
            xmlData.Append("        xmlns='http://www.mondial-assistance.com/ecommerce/schema/'>");
            xmlData.Append("        <adminValues>");
            xmlData.Append("            <securityKey>c5d69b6a6279ad83c92340fa62fcf56c</securityKey>");
            xmlData.Append("            <partnerName>STG</partnerName>");
            xmlData.Append("            <country>AU</country>");
            xmlData.Append("            <issueDate>" + issueDate + "</issueDate>");
            xmlData.Append("            <salesOrigin type='Integrated'>" + salesOrigin + "</salesOrigin>");
            xmlData.Append("            <language>en</language>");
            xmlData.Append("        </adminValues>");
            xmlData.Append("        <travelDescription>");
            xmlData.Append("            <startDate>" + startDate + "</startDate>");
            xmlData.Append("            <endDate>" + endDate + "</endDate>");
            xmlData.Append("            <travelType>" + travelType + "</travelType>");
            xmlData.Append("            <originLocation>" + originLocation + "</originLocation>");
            xmlData.Append("            <destinationLocation>" + destinationLocation + "</destinationLocation>");
            xmlData.Append("            <totalTravelPrice currency='AUD'>" + totalTravelPrice + "</totalTravelPrice>");
            xmlData.Append("         </travelDescription>");
            xmlData.Append("         <travellers>");
            xmlData.Append("            <item class='adult'>");
            xmlData.Append("                <number>" + adult + "</number>");
            xmlData.Append("            </item>");
            xmlData.Append("            <item class='child'>");
            xmlData.Append("                <number>" + child + "</number>");
            xmlData.Append("            </item>");
            xmlData.Append("            <item class='infant'>");
            xmlData.Append("                <number>" + infant + "</number>");
            xmlData.Append("            </item>");
            xmlData.Append("        </travellers>");
            xmlData.Append("    </requestPricing>");

            return xmlData.ToString();
        }

        private string GenerateSubscription(string xmlPayment, string productVariantID, string recordLocator, Passengers objPassenger)
        {
            XPathDocument xmlDocPayment = new XPathDocument(new StringReader(xmlPayment));
            XPathNavigator nvPayment = xmlDocPayment.CreateNavigator();
            XPathDocument xmlDocInsurance = new XPathDocument(new StringReader(B2CSession.responsePrice));
            XPathNavigator nvInsurance = xmlDocInsurance.CreateNavigator();
            string totalPaidPremium = "0";

            B2CVariable objUv = B2CSession.Variable;
            BookingHeader bookingHeader = B2CSession.BookingHeader;
            Fees fees = B2CSession.Fees;
            Quotes quotes = B2CSession.Quotes;
            Payments payments = B2CSession.Payments;
            Library objLi = new Library();

            string issueDate = DateTime.Now.ToString("dd/MM/yyyy");
            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("InsuranceMondialSetting");
            string salesOrigin = Setting["salesOrigin"].ToString();
            string startDate = objUv.DepartureDate.Substring(6, 2) + "/" + objUv.DepartureDate.Substring(4, 2) + "/" + objUv.DepartureDate.Substring(0, 4);
            string endDate = "";
            if (objUv.ReturnDate != "" && objUv.ReturnDate != "0")
                endDate = objUv.ReturnDate.Substring(6, 2) + "/" + objUv.ReturnDate.Substring(4, 2) + "/" + objUv.ReturnDate.Substring(0, 4);
            else
                endDate = startDate;
            string travelType = "RoundTrip";// (objBooking.OneWay) ? "OneWayTrip" : "RoundTrip";
            string originLocation = "AU";
            string destinationLocation = "AU";
            string totalTravelPrice = "0";
            string adult = objUv.Adult.ToString();
            string child = objUv.Child.ToString();
            string infant = objUv.Infant.ToString();
            int count = 1;

            // HardCode
            if (objUv.OriginRcd == "HKT")
                originLocation = "TH";
            if (objUv.OriginRcd == "DPS")
                originLocation = "ID";
            if (objUv.DestinationRcd == "HKT")
                destinationLocation = "TH";
            if (objUv.DestinationRcd == "DPS")
                destinationLocation = "ID";

            // Find Total Insuarance Price
            foreach (XPathNavigator nf in nvInsurance.Select("responsePricing/productsAvailable"))
            {
                if (nf.SelectSingleNode("productVariant/@id").InnerXml == productVariantID)
                {
                    totalPaidPremium = nf.SelectSingleNode("premiumProduct").InnerXml;
                    break;
                }
            }

            // Find TotalCost
            totalTravelPrice = objLi.CalOutStandingBalance(quotes, fees, payments).ToString();

            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<?xml version='1.0' encoding='UTF-8'?>");
            xmlData.Append("    <subscription xmlns='http://www.mondial-assistance.com/ecommerce/schema/'>");
            xmlData.Append("        <requestValues>");
            xmlData.Append("            <securityKey>c5d69b6a6279ad83c92340fa62fcf56c</securityKey>");
            xmlData.Append("            <partnerName>STG</partnerName>");
            //xmlData.Append("            <partnerIdentifier>" + recordLocator + "</partnerIdentifier>");
            xmlData.Append("            <partnerIdentifier>" + XmlHelper.XpathValueNullToEmpty(nvPayment, "payment/record_locator") + "</partnerIdentifier>");
            xmlData.Append("            <country>AU</country>");
            xmlData.Append("            <issueDate>" + issueDate + "</issueDate>");
            xmlData.Append("            <salesOrigin type='Integrated'>" + salesOrigin + "</salesOrigin>");
            xmlData.Append("            <language>en</language>");
            xmlData.Append("        </requestValues>");
            xmlData.Append("        <travelDescription>");
            xmlData.Append("            <startDate>" + startDate + "</startDate>");
            xmlData.Append("            <endDate>" + endDate + "</endDate>");
            xmlData.Append("            <travelType>" + travelType + "</travelType>");
            xmlData.Append("            <originLocation>" + originLocation + "</originLocation>");
            xmlData.Append("            <destinationLocation>" + destinationLocation + "</destinationLocation>");
            xmlData.Append("            <totalTravelPrice currency='AUD'>" + totalTravelPrice.ToString() + "</totalTravelPrice>");
            xmlData.Append("         </travelDescription>");
            xmlData.Append("         <productVariant id='" + productVariantID + "' />");
            xmlData.Append("         <contractHolder personId='" + count + "'>");
            xmlData.Append("            <email>" + bookingHeader.contact_email + "</email>");
            xmlData.Append("            <address>" + bookingHeader.address_line1 + "</address>");
            if (bookingHeader.address_line2 != "")
                xmlData.Append("            <address>" + bookingHeader.address_line2 + "</address>");
            xmlData.Append("            <zipcode>" + bookingHeader.zip_code + "</zipcode>");
            xmlData.Append("            <town>" + bookingHeader.city + "</town>");
            xmlData.Append("            <country>" + bookingHeader.country_rcd + "</country>");
            xmlData.Append("            <phoneNumber>" + bookingHeader.phone_home + "</phoneNumber>");
            xmlData.Append("            <phoneNumber>" + bookingHeader.phone_mobile + "</phoneNumber>");
            xmlData.Append("         </contractHolder>");
            // Fill Passengger
            foreach (Passenger objPass in objPassenger)
            {
                xmlData.Append("         <insuredPerson id='" + count + "'>");
                xmlData.Append("            <title>" + objPass.title_rcd + "</title>");
                xmlData.Append("            <surname>" + objPass.lastname + "</surname>");
                xmlData.Append("            <firstname>" + objPass.firstname + "</firstname>");
                if (objPass.date_of_birth.ToString("dd/MM/yyyy") != "01/01/0001")
                    xmlData.Append("            <dob>" + objPass.date_of_birth.ToString("dd/MM/yyyy") + "</dob>");
                else
                    xmlData.Append("            <dob>01/01/1980</dob>");
                xmlData.Append("         </insuredPerson>");
                count++;
            }
            xmlData.Append("         <totalPaidPremium currency='AUD'>" + totalPaidPremium + "</totalPaidPremium>");
            xmlData.Append("         <payment>");
            xmlData.Append("            <paymentHolder>");
            xmlData.Append("                <lastName>" + XmlHelper.XpathValueNullToEmpty(nvPayment, "payment/NameOnCard") + "</lastName>");
            xmlData.Append("                <country>" + XmlHelper.XpathValueNullToEmpty(nvPayment, "payment/Country") + "</country>");
            xmlData.Append("            </paymentHolder>");
            xmlData.Append("            <cardInformation>");
            xmlData.Append("                <cardNumber>" + XmlHelper.XpathValueNullToEmpty(nvPayment, "payment/CreditCardNumber") + "</cardNumber>");
            xmlData.Append("                <cv2Number>" + XmlHelper.XpathValueNullToEmpty(nvPayment, "payment/CVV") + "</cv2Number>");
            xmlData.Append("                <validityDate>01/" + XmlHelper.XpathValueNullToEmpty(nvPayment, "payment/ExpiryMonth") + "/" + XmlHelper.XpathValueNullToEmpty(nvPayment, "payment/ExpiryYear") + "</validityDate>");
            xmlData.Append("            </cardInformation>");
            xmlData.Append("         </payment>");
            xmlData.Append("         <confirmationEmail>" + bookingHeader.contact_email + "</confirmationEmail>");
            xmlData.Append("         <confirmationMessaging>true</confirmationMessaging>");
            xmlData.Append("         <paymentMessaging>true</paymentMessaging>");
            xmlData.Append("    </subscription>");

            return xmlData.ToString();
        }

        private void FillSSRInsuarance(string InsuarnaceSSRRCD)
        {
            Itinerary itinerary = B2CSession.Itinerary;
            Passengers passengers = B2CSession.Passengers;
            Library objLi = new Library();
            StringBuilder strXML = new StringBuilder();

            Services objSSRs = CacheHelper.CacheSpecialServiceRef();
            Service objSSRInsarance = null;
            foreach (Service s in objSSRs)
            {
                if (s.special_service_rcd == InsuarnaceSSRRCD)
                {
                    objSSRInsarance = s;
                    break;
                }
            }
            strXML.Append("<booking>");
            foreach (FlightSegment objSegment in itinerary)
            {
                foreach (Passenger objPass in passengers)
                {
                    strXML.Append("<service>");
                    strXML.Append("     <passenger_id>" + objPass.passenger_id + "</passenger_id>");
                    strXML.Append("     <booking_segment_id>" + objSegment.booking_segment_id + "</booking_segment_id>");
                    strXML.Append("     <origin_rcd>" + objSegment.origin_rcd + "</origin_rcd>");
                    strXML.Append("     <destination_rcd>" + objSegment.destination_rcd + "</destination_rcd>");
                    strXML.Append("     <fee_id></fee_id>");
                    strXML.Append("     <special_service_rcd>" + objSSRInsarance.special_service_rcd + "</special_service_rcd>");
                    strXML.Append("     <service_text>" + objSSRInsarance.display_name + "</service_text>");
                    strXML.Append("     <number_of_units>1</number_of_units>");
                    strXML.Append("     <service_on_request_flag>" + objSSRInsarance.service_on_request_flag.ToString() + "</service_on_request_flag>");
                    strXML.Append("</service>");
                }
            }
            strXML.Append("</booking>");

            FillSpecialService(strXML.ToString(), false);

        }

        private void InsuaranceSave(string productVariantID, string xmlPayment, string strPass)
        {
            //Load Config
            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("InsuranceMondialSetting");
            string requestURL = Setting["InsuranceSubscriptionURL"].ToString();
            string InsuarnaceSSRRCD = Setting["InsuarnaceSSRRCD"].ToString();
            string InsuarnaceRemarkRCD = Setting["InsuarnaceRemarkRCD"].ToString();

            Classes.Helper objHp = new Classes.Helper();
            //Booking objBooking = (Booking)Session["Booking"];
            //BookingHeader bookingHeader = (BookingHeader)Session["BookingHeader"];
            //Remarks remarks = (Remarks)Session["Remarks"];

            Passengers objPass = (Passengers)XmlHelper.Deserialize(strPass, typeof(Passengers));
            // Get Locator
            //objBooking.objService = (TikAeroXMLwebservice)Session["AgentService"];
            string recordLocator = "";
            //int bookingNumber = 0;
            //objBooking.GetRecordLocator(ref recordLocator, ref bookingNumber);

            // Subcription
            string xml = SubscriptionInsurance(xmlPayment, productVariantID, recordLocator, requestURL, objPass);
            //bookingHeader.record_locator = recordLocator;
            //bookingHeader.booking_number = bookingNumber;
            xml = xml.Replace("xmlns=\"http://www.mondial-assistance.com/ecommerce/schema/\"", "");

            // Add Remark
            XPathDocument xmlSubcription = new XPathDocument(new StringReader(xml));
            XPathNavigator nvSubcription = xmlSubcription.CreateNavigator();
            //Remark objRemark = new Remark();
            B2CSession.InsuaranceNumber = "";
            if (nvSubcription.Select("subscriptionACK").Count > 0 && nvSubcription.SelectSingleNode("subscriptionACK/contractNumber") != null)
            {
                //objRemark.remark_type_rcd = InsuarnaceRemarkRCD;
                //objRemark.remark_text = nvSubcription.SelectSingleNode("subscriptionACK/contractNumber").InnerXml;
                //objRemark.booking_id = bookingHeader.booking_id;
                //objRemark.booking_remark_id = Guid.NewGuid();
                //objRemark.client_profile_id = bookingHeader.client_profile_id;
                //objRemark.agency_code = bookingHeader.agency_code;
                B2CSession.InsuaranceNumber = nvSubcription.SelectSingleNode("subscriptionACK/contractNumber").InnerXml;

                // Fill SSR
                //FillSSRInsuarance(InsuarnaceSSRRCD);
            }
            //remarks.Add(objRemark);
        }
        #endregion

        #region ExternalLoadPage
        [WebMethod(EnableSession = true)]
        public string ExternalLoadPage(string page)
        {
            try
            {
                Library objLi = new Library();
                string result = "";
                string pagePath = "";

                switch (page)
                {
                    case "groupbooking":
                        pagePath = "UserControls/GroupBooking.ascx";
                        break;
                    case "charterflight":
                        pagePath = "UserControls/CharterFlight.ascx";
                        break;
                    case "registerdetail":
                        pagePath = "UserControls/NewsRegisterDetail.ascx";
                        break;
                }

                result = objLi.GenerateControlString(pagePath, string.Empty);

                return result;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, page);
                throw ex;
            }
        }

        #endregion

        #region Credit Card
        [WebMethod(EnableSession = true)]
        public string EqipayPaymentRequest(string xmlPayment)
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Payments objPayment = new Payments();
                Library objLi = new Library();

                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("EquipayPaymentSetting");
                StringBuilder stbHtml = new StringBuilder();
                B2CSession.Https = true;
                bookingHeader.approval_flag = 1;

                if (PointValidatePass(B2CSession.Client, mappings) == true)
                {
                    objPayment.objService = B2CSession.AgentService;
                    //Set update iniformation
                    SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                    Fees objPaymentFees = null;

                    if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                         ref itinerary,
                                                         ref passengers,
                                                         ref quotes,
                                                         ref fees,
                                                         ref objPaymentFees,
                                                         ref mappings,
                                                         ref services,
                                                         ref remarks,
                                                         ref payments,
                                                         ref taxes,
                                                         xmlPayment,
                                                         objUv.FormOfPaymentFee,
                                                         objUv.UserId,
                                                         objUv.booking_payment_id,
                                                         objUv.ip_address) == true)
                    {
                        if (objPayment.Count > 0)
                        {
                            //Save booking as booknow paylater
                            if (bookingHeader.record_locator.Length == 0)
                            {
                                objUv.PaymentReference = string.Format("{0:yyMMddhhmmss}", DateTime.Now);

                                B2CSession.BookingHeader = bookingHeader;
                                B2CSession.Itinerary = itinerary;
                                B2CSession.Passengers = passengers;
                                B2CSession.Quotes = quotes;
                                B2CSession.Fees = fees;
                                B2CSession.Mappings = mappings;
                                B2CSession.Services = services;
                                B2CSession.Remarks = remarks;
                                B2CSession.Payments = payments;
                                B2CSession.Taxes = taxes;
                            }

                            //Save CCpayment object to Session
                            B2CSession.CcPayment = objPayment;
                            B2CSession.PaymentFees = objPaymentFees;
                            objUv.PaymentGateway = "EQP";

                            //Submit information to gateway.
                            stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["PaymentUrl"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='merchantId' value='" + Setting["MerchantId"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='successUrl' value='" + Setting["SuccessUrl"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='failUrl' value='" + Setting["FailUrl"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='cancelUrl' value='" + Setting["CancelUrl"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='payType' value='N'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='lang' value='E'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='amount' value='" + objPayment[0].payment_amount + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='orderRef' value='" + objUv.PaymentReference + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='currCode' value='" + Setting["currency_number"] + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='pMethod' value='" + objPayment[0].form_of_payment_subtype_rcd + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='epMonth' value='" + objPayment[0].expiry_month + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='epYear' value='" + objPayment[0].expiry_year + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='cardNo' value='" + objPayment[0].document_number + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='securityCode' value='" + objPayment[0].cvv_code + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='cardHolder' value='" + objPayment[0].name_on_card + "' >" + Environment.NewLine);
                            stbHtml.Append("</form>" + Environment.NewLine);


                        }
                    }
                }

                return stbHtml.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Payment Xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public string JccPaymentRequest(string xmlPayment)
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Payments objPayment = new Payments();

                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("JccPaymentSetting");
                StringBuilder stbHtml = new StringBuilder();
                B2CSession.Https = true;
                bookingHeader.approval_flag = 1;

                if (PointValidatePass(B2CSession.Client, mappings) == true)
                {
                    objPayment.objService = B2CSession.AgentService;
                    //Set update iniformation
                    SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                    Fees objPaymentFees = null;
                    if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                         ref itinerary,
                                                         ref passengers,
                                                         ref quotes,
                                                         ref fees,
                                                         ref objPaymentFees,
                                                         ref mappings,
                                                         ref services,
                                                         ref remarks,
                                                         ref payments,
                                                         ref taxes,
                                                         xmlPayment,
                                                         objUv.FormOfPaymentFee,
                                                         objUv.UserId,
                                                         objUv.booking_payment_id,
                                                         objUv.ip_address) == true)
                    {
                        if (objPayment.Count > 0)
                        {
                            //Save CCpayment object to Session
                            B2CSession.CcPayment = objPayment;
                            B2CSession.PaymentFees = objPaymentFees;
                            objUv.PaymentGateway = "JCC";
                            //Submit information to gateway.
                            string strPaymentAmount = String.Format("{0:000000000000}", (objPayment[0].payment_amount * 100));
                            //Create SHA string to encrypt.
                            string strHash = Setting["Password"] +
                                             Setting["MerchantId"] +
                                             Setting["Acquirer"] +
                                             objUv.PaymentReference +
                                             strPaymentAmount +
                                             Setting["currency_number"];
                            //SHA1 Encoding.
                            System.Security.Cryptography.SHA1 objEncrypt = System.Security.Cryptography.SHA1.Create();
                            System.Text.ASCIIEncoding encoder = new System.Text.ASCIIEncoding();
                            byte[] combined = encoder.GetBytes(strHash);
                            strHash = Convert.ToBase64String(objEncrypt.ComputeHash(combined));

                            objUv.PaymentReference = string.Format("{0:yyMMddhhmmss}", DateTime.Now);
                            stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["PaymentUrl"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='Version' value='1.0.0'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='MerID' value='" + Setting["MerchantId"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='AcqID' value='" + Setting["Acquirer"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='MerRespURL' value='" + Setting["ResponseUrl"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='PurchaseAmt' value='" + strPaymentAmount + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='PurchaseCurrency' value='" + Setting["currency_number"] + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='PurchaseCurrencyExponent' value='2'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='OrderID' value='" + objUv.PaymentReference + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='CaptureFlag' value='" + Setting["CaptureFlag"] + "'>" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='CardNo' value='" + objPayment[0].document_number + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='CardExpDate' value='" + string.Format("{0:00}", objPayment[0].expiry_month) + objPayment[0].expiry_year.ToString().Substring(2) + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='CardCVV2' value='" + objPayment[0].cvv_code + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='Signature' value='" + strHash + "' >" + Environment.NewLine);
                            stbHtml.Append("<input type='hidden' name='SignatureMethod' value='SHA1' >" + Environment.NewLine);
                            stbHtml.Append("</form>" + Environment.NewLine);
                        }
                    }
                }

                return stbHtml.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Payment Xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string VisaPaymentRequest(string xmlPayment)
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Payments objPayment = new Payments();

                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("VisaPaymentSetting");
                StringBuilder stbHtml = new StringBuilder();
                B2CSession.Https = true;
                bookingHeader.approval_flag = 1;

                if (PointValidatePass(B2CSession.Client, mappings) == true)
                {
                    objPayment.objService = B2CSession.AgentService;
                    //Set update iniformation
                    SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                    Fees objPaymentFees = null;

                    //// Find Insuarance ID And Save
                    //XPathDocument xmlDocPayment = new XPathDocument(new StringReader(xmlPayment));
                    //XPathNavigator nvPayment = xmlDocPayment.CreateNavigator();
                    //string productVariantID = "";
                    //if (nvPayment.Select("payment").Count > 0 && nvPayment.SelectSingleNode("payment/InsuranceID") != null
                    //    && nvPayment.SelectSingleNode("payment/InsuranceID").InnerXml != "")
                    //{
                    //    productVariantID = nvPayment.SelectSingleNode("payment/InsuranceID").InnerXml;
                    //    //Save Insuarance
                    //    InsuaranceSvae(productVariantID, xmlPayment);
                    //}


                    if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                         ref itinerary,
                                                         ref passengers,
                                                         ref quotes,
                                                         ref fees,
                                                         ref objPaymentFees,
                                                         ref mappings,
                                                         ref services,
                                                         ref remarks,
                                                         ref payments,
                                                         ref taxes,
                                                         xmlPayment,
                                                         objUv.FormOfPaymentFee,
                                                         objUv.UserId,
                                                         objUv.booking_payment_id,
                                                         objUv.ip_address) == true)
                    {
                        if (objPayment.Count > 0)
                        {
                            if (bookingHeader.record_locator.Length == 0)
                            {
                                objUv.PaymentReference = string.Format("{0:yyMMddHHmmssFFF}", DateTime.Now);

                                B2CSession.BookingHeader = bookingHeader;
                                B2CSession.Itinerary = itinerary;
                                B2CSession.Passengers = passengers;
                                B2CSession.Quotes = quotes;
                                B2CSession.Fees = fees;
                                B2CSession.Mappings = mappings;
                                B2CSession.Services = services;
                                B2CSession.Remarks = remarks;
                                B2CSession.Payments = payments;
                                B2CSession.Taxes = taxes;
                            }
                            objPayment[0].name_on_card = passengers[0].firstname + " " + passengers[0].lastname;

                            //Save CCpayment object to Session
                            B2CSession.CcPayment = objPayment;
                            B2CSession.PaymentFees = objPaymentFees;
                            objUv.PaymentGateway = "VISA";

                            B2CSession.PaymentApprovalID = Guid.NewGuid().ToString();

                            Boolean result = objPayment.objService.InsertPaymentApproval(B2CSession.PaymentApprovalID,
                                                                    objUv.PaymentGateway, "",
                                                                    passengers[0].firstname + " " + passengers[0].lastname,
                                                                    0, 0, 0, 0, 0, "",
                                                                    objPayment[0].booking_payment_id.ToString(),
                                                                    objPayment[0].currency_rcd,
                                                                    Convert.ToDouble(objPayment[0].payment_amount),
                                                                    bookingHeader.ip_address);
                            if (result)
                            {
                                //Submit information to gateway.
                                stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["PaymentUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='merid' value='" + Setting["MerId"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='acqBIN' value='" + Setting["acqBIN"] + "'>" + Environment.NewLine);

                                stbHtml.Append("<input type='hidden' name='ReturnURLApprove' value='" + Setting["SuccessUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='ReturnURLDecline' value='" + Setting["DeclineUrl"] + "'>" + Environment.NewLine);

                                stbHtml.Append("<input type='hidden' name='OrderID' value='" + objUv.PaymentReference + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='PurchaseAmount' value='" + (objPayment[0].payment_amount.ToString("#0.00")).Replace(".", "") + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='VisualAmount' value='" + objPayment[0].payment_amount.ToString("#0.00") + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='Currency' value='" + Setting["currency_number"] + "' >" + Environment.NewLine);
                                stbHtml.Append("</form>" + Environment.NewLine);

                                B2CSession.PaymentRequestStreamText = stbHtml.ToString();
                            }
                            else
                                objUv.PaymentGateway = "";
                        }
                    }
                }

                return stbHtml.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Payment Xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string ETranzactPaymentRequest(string xmlPayment)
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Payments objPayment = new Payments();

                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("ETranzactPaymentSetting");
                StringBuilder stbHtml = new StringBuilder();
                B2CSession.Https = true;
                bookingHeader.approval_flag = 1;

                if (PointValidatePass(B2CSession.Client, mappings) == true)
                {
                    objPayment.objService = B2CSession.AgentService;
                    //Set update iniformation
                    SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                    Fees objPaymentFees = null;

                    if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                         ref itinerary,
                                                         ref passengers,
                                                         ref quotes,
                                                         ref fees,
                                                         ref objPaymentFees,
                                                         ref mappings,
                                                         ref services,
                                                         ref remarks,
                                                         ref payments,
                                                         ref taxes,
                                                         xmlPayment,
                                                         objUv.FormOfPaymentFee,
                                                         objUv.UserId,
                                                         objUv.booking_payment_id,
                                                         objUv.ip_address) == true)
                    {
                        if (objPayment.Count > 0)
                        {
                            if (bookingHeader.record_locator.Length == 0)
                            {
                                objUv.PaymentReference = string.Format("{0:yyMMddHHmmssFFF}", DateTime.Now);

                                B2CSession.BookingHeader = bookingHeader;
                                B2CSession.Itinerary = itinerary;
                                B2CSession.Passengers = passengers;
                                B2CSession.Quotes = quotes;
                                B2CSession.Fees = fees;
                                B2CSession.Mappings = mappings;
                                B2CSession.Services = services;
                                B2CSession.Remarks = remarks;
                                B2CSession.Payments = payments;
                                B2CSession.Taxes = taxes;
                            }

                            //Save CCpayment object to Session
                            B2CSession.CcPayment = objPayment;
                            B2CSession.PaymentFees = objPaymentFees;
                            objUv.PaymentGateway = "ETZ";

                            string strEncryptText = objPayment[0].payment_amount.ToString("#0.00") +
                                             Setting["TerminalId"] +
                                             objUv.PaymentReference +
                                             Setting["RedirectUrl"] +
                                             Setting["Secretkey"];
                            B2CSession.ETZEncrypt = strEncryptText;

                            MD5 objEncrypt = MD5.Create();
                            UTF8Encoding encoder = new UTF8Encoding();
                            byte[] data = objEncrypt.ComputeHash(encoder.GetBytes(strEncryptText));
                            StringBuilder sBuilder = new StringBuilder();
                            foreach (byte byt in data)
                            {
                                sBuilder.Append(byt.ToString("x2"));
                            }
                            string strEncryptResult = sBuilder.ToString().ToUpper();

                            B2CSession.PaymentApprovalID = Guid.NewGuid().ToString();

                            Boolean result = objPayment.objService.InsertPaymentApproval(B2CSession.PaymentApprovalID,
                                                                    objUv.PaymentGateway, "",
                                                                    passengers[0].firstname + " " + passengers[0].lastname,
                                                                    0, 0, 0, 0, 0, "",
                                                                    objPayment[0].booking_payment_id.ToString(),
                                                                    objPayment[0].currency_rcd,
                                                                    Convert.ToDouble(objPayment[0].payment_amount),
                                                                    bookingHeader.ip_address);
                            string trans_desc = itinerary[0].airline_rcd + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date.ToString("dd MMM yyyy") + " " + itinerary[0].departure_time.ToString("00:00");
                            if (itinerary.Count > 1)
                                trans_desc += " :: " +
                                                itinerary[1].airline_rcd + " " + itinerary[1].flight_number + " " + itinerary[1].departure_date.ToString("dd MMM yyyy") + " " + itinerary[1].departure_time.ToString("00:00");
                            if (result)
                            {
                                //Submit information to gateway.                        
                                stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["ActionURL"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='TERMINAL_ID' value='" + Setting["TerminalId"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='TRANSACTION_ID' value='" + objUv.PaymentReference + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='AMOUNT' value='" + objPayment[0].payment_amount.ToString("#0.00") + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='DESCRIPTION' value='" + trans_desc + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='CHECKSUM' value='" + strEncryptResult + "' >" + Environment.NewLine);

                                stbHtml.Append("<input type='hidden' name='RESPONSE_URL' value='" + Setting["RedirectUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='LOGO_URL' value='" + Setting["LogoUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("</form>" + Environment.NewLine);

                                B2CSession.PaymentRequestStreamText = stbHtml.ToString();
                            }
                            else
                                objUv.PaymentGateway = "";
                        }
                    }
                }

                return stbHtml.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Payment Xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string InterswitchPaymentRequest(string xmlPayment)
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Payments objPayment = new Payments();

                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("InterswitchPaymentSetting");
                StringBuilder stbHtml = new StringBuilder();
                B2CSession.Https = true;
                bookingHeader.approval_flag = 1;

                if (PointValidatePass(B2CSession.Client, mappings) == true)
                {
                    objPayment.objService = B2CSession.AgentService;
                    //Set update iniformation
                    SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                    Fees objPaymentFees = null;

                    if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                         ref itinerary,
                                                         ref passengers,
                                                         ref quotes,
                                                         ref fees,
                                                         ref objPaymentFees,
                                                         ref mappings,
                                                         ref services,
                                                         ref remarks,
                                                         ref payments,
                                                         ref taxes,
                                                         xmlPayment,
                                                         objUv.FormOfPaymentFee,
                                                         objUv.UserId,
                                                         objUv.booking_payment_id,
                                                         objUv.ip_address) == true)
                    {
                        if (objPayment.Count > 0)
                        {
                            if (bookingHeader.record_locator.Length == 0)
                            {
                                objUv.PaymentReference = string.Format("{0:yyMMddHHmmssFFF}", DateTime.Now);

                                B2CSession.BookingHeader = bookingHeader;
                                B2CSession.Itinerary = itinerary;
                                B2CSession.Passengers = passengers;
                                B2CSession.Quotes = quotes;
                                B2CSession.Fees = fees;
                                B2CSession.Mappings = mappings;
                                B2CSession.Services = services;
                                B2CSession.Remarks = remarks;
                                B2CSession.Payments = payments;
                                B2CSession.Taxes = taxes;
                            }
                            objPayment[0].name_on_card = passengers[0].firstname + " " + passengers[0].lastname;

                            //Save CCpayment object to Session
                            B2CSession.CcPayment = objPayment;
                            B2CSession.PaymentFees = objPaymentFees;
                            objUv.PaymentGateway = "ITS";

                            MD5 objEncrypt = MD5.Create();
                            UTF8Encoding encoder = new UTF8Encoding();
                            byte[] data = objEncrypt.ComputeHash(encoder.GetBytes(string.Format("{0:yyMMddHHmmssFFF}", DateTime.Now) + Setting["currency_number"]));
                            StringBuilder sBuilder = new StringBuilder();
                            foreach (byte byt in data)
                            {
                                sBuilder.Append(byt.ToString("x2"));
                            }
                            B2CSession.ITSEncrypt = sBuilder.ToString().ToUpper();

                            B2CSession.PaymentApprovalID = Guid.NewGuid().ToString();

                            Boolean result = objPayment.objService.InsertPaymentApproval(B2CSession.PaymentApprovalID,
                                                                    objUv.PaymentGateway, "",
                                                                    passengers[0].firstname + " " + passengers[0].lastname,
                                                                    0, 0, 0, 0, 0, "",
                                                                    objPayment[0].booking_payment_id.ToString(),
                                                                    objPayment[0].currency_rcd,
                                                                    Convert.ToDouble(objPayment[0].payment_amount),
                                                                    bookingHeader.ip_address);
                            string trans_desc = itinerary[0].airline_rcd + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date.ToString("dd MMM yyyy") + " " + itinerary[0].departure_time.ToString("00:00");
                            if (itinerary.Count > 1)
                                trans_desc += " :: " +
                                                itinerary[1].airline_rcd + " " + itinerary[1].flight_number + " " + itinerary[1].departure_date.ToString("dd MMM yyyy") + " " + itinerary[1].departure_time.ToString("00:00");
                            if (result)
                            {

                                //Submit information to gateway.
                                stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["PaymentUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='prod_id' value='" + Setting["Product_ID"] + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='amount' value='" + (objPayment[0].payment_amount.ToString("#0.00")).Replace(".", "") + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='currency' value='" + Setting["currency_number"] + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='payment_params' value='0' />" + Environment.NewLine);

                                stbHtml.Append("<input type='hidden' name='site_redirect_url' value='" + Setting["RedirectUrl"] + B2CSession.ITSEncrypt + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='site_name' value='" + Setting["HomeSiteUrl"] + "' />" + Environment.NewLine);

                                stbHtml.Append("<input type='hidden' name='cust_id' value='" + objUv.UserId + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='cust_id_desc' value='User Id' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='cust_name' value='" + bookingHeader.firstname + " " + bookingHeader.lastname + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='cust_name_desc' value='Custormer Name' />" + Environment.NewLine);

                                stbHtml.Append("<input type='hidden' name='txn_ref' value='" + objUv.PaymentReference + "' />" + Environment.NewLine);

                                stbHtml.Append("<input type='hidden' name='pay_item_id' value='" + Setting["Pay_Item_ID"] + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='pay_item_name' value='" + trans_desc + "' />" + Environment.NewLine);

                                stbHtml.Append("<input type='hidden' name='local_date_time' value='" + string.Format("{0:dd-MM-yy HH:mm:ss}", DateTime.Now) + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='xml_data'/>" + Environment.NewLine);
                                stbHtml.Append("</form>" + Environment.NewLine);

                                B2CSession.PaymentRequestStreamText = stbHtml.ToString();
                            }
                            else
                                objUv.PaymentGateway = "";
                        }
                    }
                }

                return stbHtml.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Payment Xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string Ogone3DPaymentRequest(string xmlPayment)
        {
            Helper objHelper = new Helper();
            try
            {
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Library objLi = new Library();
                Agents objAgents = B2CSession.Agents;

                #region Validate SSRInventoryService
                if (B2CSetting.SSRInventoryService)
                {
                    string ssrResult = string.Empty;
                    string resultValidate = string.Empty;
                    resultValidate = ValidateSpecialService(string.Empty, services);
                    if (!string.IsNullOrEmpty(resultValidate))
                    {
                        if (resultValidate == "{003}")
                        { ssrResult = "{003}"; }
                        else if (resultValidate == "{003_1}")
                        { ssrResult = "{003_1}"; }
                        else if (resultValidate == "{003_2}")
                        { ssrResult = "{003_2}"; }

                        #region Remove SSR
                        if (ssrResult.Length > 0)
                        {
                            string ssrGroup = B2CSetting.SsrGroup;
                            string[] arr = !string.IsNullOrEmpty(ssrGroup) ? ssrGroup.Split(',') : new string[] { };
                            for (int i = services.Count - 1; i > -1; i--)
                            {
                                if (arr.Contains<string>(services[i].special_service_rcd))
                                {
                                    services.Remove(services[i]);
                                }
                            }

                            for (int j = fees.Count - 1; j > -1; j--)
                            {
                                if (arr.Contains<string>(fees[j].fee_rcd))
                                {
                                    fees.Remove(fees[j]);
                                }
                            }
                            //Fare summary
                            ServiceResponse response = base.FillSpecialService(true,
                                                                            objAgents[0].agency_code,
                                                                            bookingHeader,
                                                                            itinerary,
                                                                            passengers,
                                                                            quotes,
                                                                            mappings,
                                                                            taxes,
                                                                            fees,
                                                                            services,
                                                                            remarks);

                            if (response.Success == true)
                            {
                                return "SSRValidate{}" + ssrResult + "{}" + response.HTML + "{}";
                            }
                            else
                            {
                                return "{" + response.Code + "}";
                            }
                        }
                        #endregion
                        return ssrResult;
                    }
                }
                #endregion

                //fill exchange information when exchange_fee_amount not empty
                if (string.IsNullOrEmpty(B2CSetting.DefaultCurrency))
                {
                    xmlPayment = xmlPayment.Replace("</exchange_currency>", B2CSetting.DefaultCurrency + "</exchange_currency>");
                }

                //Travel Reason
                XPathDocument xmlDocument;
                XPathNavigator nvv;
                string businessFlag = string.Empty;
                string no_vat_flag = string.Empty;

                xmlDocument = new XPathDocument(new StringReader(xmlPayment));
                nvv = xmlDocument.CreateNavigator();

                foreach (XPathNavigator n in nvv.Select("payment"))
                {
                    businessFlag = objLi.getXPathNodevalue(n, "travelReason", Library.xmlReturnType.value);
                    no_vat_flag = objLi.getXPathNodevalue(n, "no_vat_flag", Library.xmlReturnType.value);
                }
                if (!string.IsNullOrEmpty(businessFlag))
                {
                    bookingHeader.business_flag = Convert.ToByte(businessFlag);
                }
                if (!string.IsNullOrEmpty(no_vat_flag))
                {
                    bookingHeader.no_vat_flag = Convert.ToByte(no_vat_flag);
                }

                Payments objPayment = new Payments();

                Fees objPaymentFees = null;

                string strResult = string.Empty;
                //Call Credit Card Payment.
                if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                     ref itinerary,
                                                     ref passengers,
                                                     ref quotes,
                                                     ref fees,
                                                     ref objPaymentFees,
                                                     ref mappings,
                                                     ref services,
                                                     ref remarks,
                                                     ref payments,
                                                     ref taxes,
                                                     xmlPayment,
                                                     objUv.FormOfPaymentFee,
                                                     objUv.UserId,
                                                     objUv.booking_payment_id,
                                                     objUv.ip_address) == true)
                {
                    if (objPayment.Count > 0)
                    {
                        //Save booking as booknow paylater
                        string strPaymentResult;
                        string strRequestSource = string.Empty;
                        if (!string.IsNullOrEmpty(B2CSetting.Ogone3DRequestSource))
                            strRequestSource = B2CSetting.Ogone3DRequestSource;

                        ServiceClient objService = new ServiceClient();
                        objService.objService = B2CSession.AgentService;

                        bookingHeader.approval_flag = 1;
                        SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                        strPaymentResult = objService.SaveBookingCreditCard(bookingHeader.booking_id.ToString(),
                                                                            bookingHeader,
                                                                            itinerary,
                                                                            passengers,
                                                                            remarks,
                                                                            objPayment,
                                                                            mappings,
                                                                            services,
                                                                            taxes,
                                                                            fees,
                                                                            objPaymentFees,
                                                                            string.Empty,
                                                                            string.Empty,
                                                                            string.Empty,
                                                                            string.Empty,
                                                                            strRequestSource,
                                                                            true,
                                                                            false,
                                                                            false,
                                                                            Classes.Language.CurrentCode().ToUpper());
                        if (strPaymentResult.Length > 0)
                        {
                            XPathDocument xmlDoc = new XPathDocument(new StringReader(strPaymentResult));
                            XPathNavigator nv = xmlDoc.CreateNavigator();

                            if (nv.Select("Booking/BookingHeader").Count > 0)
                            {
                                //Success Payment.
                                //Get Xml String and return out to generate control
                                objLi.FillBooking(strPaymentResult,
                                                  ref bookingHeader,
                                                  ref passengers,
                                                  ref itinerary,
                                                  ref mappings,
                                                  ref payments,
                                                  ref remarks,
                                                  ref taxes,
                                                  ref quotes,
                                                  ref fees,
                                                  ref services);
                                objUv.FormOfPaymentFee = string.Empty;

                                //Send passenger itinerary by email.
                                objHelper.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                                //Generate booking summary control.
                                strResult = objLi.GenerateControlString("UserControls/BookingSummary.ascx", strPaymentResult);
                                B2CSession.RemoveBookingSession();
                            }
                            else if (nv.Select("NewDataSet/Payments").Count > 0)
                            {
                                foreach (XPathNavigator n in nv.Select("NewDataSet/Payments"))
                                {
                                    //Check Response Data.
                                    if (XmlHelper.XpathValueNullToEmpty(n, "ResponseCode") == "3D")
                                    {
                                        strResult = XmlHelper.XpathValueNullToEmpty(n, "ResponseCode") + "{}" +
                                                    XmlHelper.XpathValueNullToEmpty(n, "ResponseHtml");

                                        if (bookingHeader.record_locator.Length == 0)
                                        {
                                            objUv.PaymentReference = "TRUE";
                                            if (n.SelectSingleNode("record_locator") != null)
                                            {
                                                bookingHeader.record_locator = XmlHelper.XpathValueNullToEmpty(n, "record_locator");
                                            }

                                            B2CSession.BookingHeader = bookingHeader;
                                            B2CSession.Itinerary = itinerary;
                                            B2CSession.Passengers = passengers;
                                            B2CSession.Quotes = quotes;
                                            B2CSession.Fees = fees;
                                            B2CSession.Mappings = mappings;
                                            B2CSession.Services = services;
                                            B2CSession.Remarks = remarks;
                                            B2CSession.Payments = payments;
                                            B2CSession.Taxes = taxes;
                                        }

                                        //Save CCpayment object to Session
                                        B2CSession.Https = true;
                                        B2CSession.CcPayment = objPayment;
                                        B2CSession.PaymentFees = objPaymentFees;
                                        objUv.PaymentGateway = "OGN";
                                    }
                                    else
                                    {
                                        if (n.SelectSingleNode("ResponseCode") != null)
                                        {
                                            strResult = XmlHelper.XpathValueNullToEmpty(n, "ResponseCode") + "{}" +
                                                        XmlHelper.XpathValueNullToEmpty(n, "ResponseText");
                                        }
                                        else
                                        {
                                            strResult = XmlHelper.XpathValueNullToEmpty(n, "ErrorCode") + "{}" +
                                                        XmlHelper.XpathValueNullToEmpty(n, "ErrorMessage");
                                        }
                                    }
                                }
                            }
                            else if (nv.Select("ErrorResponse/Error").Count > 0)
                            {
                                strResult = XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/ErrorCode") + "{}" +
                                            XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/Message");
                            }
                        }


                    }
                }

                return strResult;
            }
            catch (Exception ex)
            {
                objHelper.SendErrorEmail(ex, "Payment Xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        public static string CallWebServiceWithCredential(string strURL, string strPostdata, int iTimeoutSec, string userId, string password)
        {
            string strResult = "";
            WebRequest wrGETURL = null;
            Stream objStream = null;

            if (!string.IsNullOrEmpty(strPostdata))
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(strURL);
                request.Method = WebRequestMethods.Http.Post;
                request.ContentLength = strPostdata.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                request.KeepAlive = false;
                request.Credentials = new NetworkCredential(userId, password);

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(strPostdata);
                writer.Close();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                strResult = reader.ReadToEnd();
                response.Close();
            }
            else
            {
                wrGETURL = WebRequest.Create(strURL);

                WebProxy myProxy = new WebProxy("myproxy", 80);
                myProxy.BypassProxyOnLocal = true;
                wrGETURL.Timeout = iTimeoutSec * 1000;
                wrGETURL.Proxy = WebProxy.GetDefaultProxy();
                wrGETURL.Proxy.Credentials = new NetworkCredential(userId, password);
                wrGETURL.Credentials = new NetworkCredential(userId, password);

                objStream = wrGETURL.GetResponse().GetResponseStream();

                StreamReader objReader = new StreamReader(objStream);
                string sLine = "";
                while (sLine != null)
                {
                    sLine = objReader.ReadLine();
                    if (sLine != null)
                        strResult += sLine;
                }
                strResult = HttpUtility.UrlDecode(strResult);
            }

            return strResult;

        }

        [WebMethod(EnableSession = true)]
        public string TatraPaymentRequest(string xmlPayment)
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Payments objPayment = new Payments();

                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("TatraPaymentSetting");
                StringBuilder stbHtml = new StringBuilder();
                B2CSession.Https = true;
                bookingHeader.approval_flag = 1;

                if (PointValidatePass(B2CSession.Client, mappings) == true)
                {
                    objPayment.objService = B2CSession.AgentService;

                    string RecordLocator = "";
                    int bookingNumber = 0;

                    objPayment.objService.GetRecordLocator(ref RecordLocator, ref bookingNumber);
                    bookingHeader.record_locator = RecordLocator;
                    bookingHeader.booking_number = bookingNumber;

                    //Set update iniformation

                    SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                    Fees objPaymentFees = null;

                    if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                         ref itinerary,
                                                         ref passengers,
                                                         ref quotes,
                                                         ref fees,
                                                         ref objPaymentFees,
                                                         ref mappings,
                                                         ref services,
                                                         ref remarks,
                                                         ref payments,
                                                         ref taxes,
                                                         xmlPayment,
                                                         objUv.FormOfPaymentFee,
                                                         objUv.UserId,
                                                         objUv.booking_payment_id,
                                                         objUv.ip_address) == true)
                    {
                        if (objPayment.Count > 0)
                        {
                            if (bookingHeader.record_locator.Length == 0)
                            {
                                objUv.PaymentReference = string.Format("{0:yyMMddHHmmssFFF}", DateTime.Now);

                                B2CSession.BookingHeader = bookingHeader;
                                B2CSession.Itinerary = itinerary;
                                B2CSession.Passengers = passengers;
                                B2CSession.Quotes = quotes;
                                B2CSession.Fees = fees;
                                B2CSession.Mappings = mappings;
                                B2CSession.Services = services;
                                B2CSession.Remarks = remarks;
                                B2CSession.Payments = payments;
                                B2CSession.Taxes = taxes;
                            }

                            //Save CCpayment object to Session
                            B2CSession.CcPayment = objPayment;
                            B2CSession.PaymentFees = objPaymentFees;
                            objUv.PaymentGateway = "Tatra";
                            string strEncryptText = "";

                            Library objLi = new Library();
                            XPathDocument xmlDoc = new XPathDocument(new StringReader(xmlPayment));
                            XPathNavigator nv = xmlDoc.CreateNavigator();
                            string PT = "";

                            if (nv.Select("payment").Count > 0)
                            {
                                foreach (XPathNavigator n in nv.Select("payment"))
                                {
                                    //Check Response Data.
                                    if (XmlHelper.XpathValueNullToEmpty(n, "form_of_payment_subtype_rcd").ToUpper() == "TATRA")
                                    {
                                        PT = "CardPay";

                                        //MID + AMT + CURR + VS + CS + RURL + IPC + NAME;
                                        strEncryptText = Setting["MerchantId"] + objPayment[0].payment_amount.ToString("#0.00") +
                                             Setting["currency_number"] + bookingNumber +
                                             Setting["ReturnUrl"] + bookingHeader.ip_address.ToString() + bookingHeader.firstname + " " + bookingHeader.lastname;
                                    }
                                    else
                                    {
                                        PT = "TatraPay";
                                        //MID + AMT + CURR + VS + CS + RURL;
                                        strEncryptText = Setting["MerchantId"] + objPayment[0].payment_amount.ToString("#0.00") +
                                            Setting["currency_number"] + bookingNumber +
                                            Setting["ReturnUrl"];
                                    }

                                }
                            }


                            B2CSession.TatraEncrypt = strEncryptText;


                            string strHash = strEncryptText;
                            //SHA1 Encoding.
                            System.Security.Cryptography.SHA1 objEncrypt = System.Security.Cryptography.SHA1.Create();
                            System.Text.ASCIIEncoding encoder = new System.Text.ASCIIEncoding();
                            byte[] combined = encoder.GetBytes(strHash);
                            byte[] hashBytes = objEncrypt.ComputeHash(combined);

                            byte[] hashByteS = new byte[8];

                            for (int i = 0; i < 8; i++)
                            {
                                hashByteS[i] = hashBytes[i];

                            }

                            string sKey = Setting["sKey"].Replace("*", "<");

                            byte[] keyBytes = encoder.GetBytes(sKey);

                            System.Security.Cryptography.DESCryptoServiceProvider DES = new System.Security.Cryptography.DESCryptoServiceProvider();

                            string encrypted = "";
                            try
                            {
                                DES.Key = keyBytes;
                                DES.Mode = System.Security.Cryptography.CipherMode.ECB;
                                System.Security.Cryptography.ICryptoTransform DESEncrypter = DES.CreateEncryptor();

                                byte[] signatureBytes = DESEncrypter.TransformFinalBlock(hashByteS, 0, hashByteS.Length);
                                for (int i = 0; i < sKey.Length; i++)
                                {
                                    encrypted = encrypted + signatureBytes[i].ToString("X2");
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                            B2CSession.PaymentApprovalID = Guid.NewGuid().ToString();

                            Boolean result = objPayment.objService.InsertPaymentApproval(B2CSession.PaymentApprovalID,
                                                                    objUv.PaymentGateway, "",
                                                                    bookingHeader.firstname + " " + bookingHeader.lastname,
                                                                    0, 0, 0, 0, 0, "",
                                                                    objPayment[0].booking_payment_id.ToString(),
                                                                    objPayment[0].currency_rcd,
                                                                    Convert.ToDouble(objPayment[0].payment_amount),
                                                                    bookingHeader.ip_address);
                            //Submit information to gateway.
                            if (PT == "CardPay")
                            {
                                stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["PaymentUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='PT' value='" + PT + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='MID' value='" + Setting["MerchantId"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='AMT' value='" + objPayment[0].payment_amount.ToString("#0.00") + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='CURR' value='" + Setting["currency_number"] + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='VS' value='" + bookingNumber.ToString() + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='RURL' value='" + Setting["ReturnUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='IPC' value='" + bookingHeader.ip_address.ToString() + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='NAME' value='" + bookingHeader.firstname + " " + bookingHeader.lastname + "' />" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='SIGN' value='" + encrypted + "' >" + Environment.NewLine);

                                stbHtml.Append("</form>" + Environment.NewLine);
                            }
                            else
                            {
                                stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["PaymentUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='PT' value='" + PT + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='MID' value='" + Setting["MerchantId"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='AMT' value='" + objPayment[0].payment_amount.ToString("#0.00") + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='CURR' value='" + Setting["currency_number"] + "' >" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='VS' value='" + bookingNumber.ToString() + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='RURL' value='" + Setting["ReturnUrl"] + "'>" + Environment.NewLine);
                                stbHtml.Append("<input type='hidden' name='SIGN' value='" + encrypted + "' >" + Environment.NewLine);
                                stbHtml.Append("</form>" + Environment.NewLine);
                            }
                        }
                    }
                }
                B2CSession.PaymentRequestStreamText = stbHtml.ToString();
                return stbHtml.ToString();
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "Payment Xml<br/>" + xmlPayment.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SofortPaymentRequest(string xmlPayment)
        {

            B2CVariable objUv = B2CSession.Variable;
            string result = string.Empty;
            objUv.PaymentReference = GeneratePaymentReference();
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.Sofort, xmlPayment);

            return result;
        }

        [WebMethod(EnableSession = true)]
        public string ESewaPaymentRequest(string xmlPayment)
        {
            B2CVariable objUv = B2CSession.Variable;
            string result = string.Empty;
            objUv.PaymentReference = GeneratePaymentReference();
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.ESewa, xmlPayment);

            return result;
        }

        [WebMethod(EnableSession = true)]
        public string AirPlusLookupPaymentRequest()
        {
            Library objLi = new Library();
            string strResult = string.Empty;
            string strLookupURL = string.Empty;
            int iTimeoutSec = 15;
            try
            {
                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("AirPlusPaymentSetting");
                if (Setting != null)
                {
                    strLookupURL = string.Format(Setting["LookupRequestUrl"], Setting["CompanyAccount"]);
                    iTimeoutSec = Convert.ToInt16(Setting["TimeOutSec"]);
                    strResult = CallWebServiceWithCredential(strLookupURL, "", iTimeoutSec, Setting["UserID"], Setting["Password"]);
                }

                return objLi.GenerateControlString("UserControls/AirPlus.ascx", strResult);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "AirPlusLookupPaymentRequest Error : " + strLookupURL + " ");
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string AirPlusUpdatePaymentRequest(string parameter)
        {
            Library objLi = new Library();
            string strXMLResult = string.Empty;
            string strUpdateURL = string.Empty;
            string strResult = string.Empty;
            string bookingId = string.Empty;
            int iTimeoutSec = 15;

            try
            {
                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("AirPlusPaymentSetting");
                if (Setting != null)
                {
                    string[] parameters = parameter.Split('|');
                    if (parameters != null && parameters.Length > 1)
                    {
                        bookingId = parameters[1];
                        strUpdateURL = string.Format(Setting["UpdateRequestUrl"], Setting["CompanyAccount"], bookingId, Setting["SupplierCode"]);
                        strUpdateURL = strUpdateURL + parameters[0];
                        iTimeoutSec = Convert.ToInt16(Setting["TimeOutSec"]);
                        strXMLResult = CallWebServiceWithCredential(strUpdateURL.Split('?')[0], strUpdateURL.Split('?')[1], iTimeoutSec, Setting["UserID"], Setting["Password"]);
                    }
                }

                if (strXMLResult.Length > 0)
                {
                    XPathDocument xmlDoc = new XPathDocument(new StringReader(strXMLResult));
                    XPathNavigator nv = xmlDoc.CreateNavigator();

                    foreach (XPathNavigator n in nv.Select("dbi-reply"))
                    {
                        if (XmlHelper.XpathValueNullToEmpty(n, "error") != "0")
                        {
                            strResult = XmlHelper.XpathValueNullToEmpty(n, "error-desc");
                        }
                    }
                }

                return strResult;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "AirPlusLookupPaymentRequest Error : " + strUpdateURL + " ");
                throw ex;
            }
        }

        #endregion

        #region Cross App Cookies
        [WebMethod(EnableSession = true)]
        public string LoadB2BLogon()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/B2BLogon.ascx", "StandardUser");
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadB2BLogonDialog()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/B2BLogon.ascx", "DialogUser");
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        //---
        [WebMethod(EnableSession = true)]
        public string LoadB2BAdminLogon()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/B2BLogon.ascx", "StandardAdmin");
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadB2BAdminLogonDialog()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/B2BLogon.ascx", "DialogAdmin");
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        //---

        [WebMethod(EnableSession = true)]
        public string LoadB2ELogon()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/B2ELogon.ascx", "StandardUser");
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadB2ELogonDialog()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/B2ELogon.ascx", "DialogUser");
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        // --
        [WebMethod(EnableSession = true)]
        public string LoadB2EAdminLogon()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/B2ELogon.ascx", "StandardAdmin");
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadB2EAdminLogonDialog()
        {
            try
            {
                Library objLi = new Library();
                return objLi.GenerateControlString("UserControls/B2ELogon.ascx", "DialogAdmin");
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string AgencyAdminLogon(string strAgencyCode, string strUserName, string strPassword)
        {
            // Check Existing User
            ServiceClient objSvc = new ServiceClient();
            Helper objHelper = new Helper();
            string strResult = string.Empty;
            HttpCookie aCookieLogon = null;
            int iAddedCookieTime = 0;
            string strCookieName = "B2BInfo";
            string strLang = "en-us";
            bool bPass = false;

            BookingHeader bookingHeader = B2CSession.BookingHeader;
            Itinerary itinerary = B2CSession.Itinerary;
            Passengers passengers = B2CSession.Passengers;
            Quotes quotes = B2CSession.Quotes;
            Fees fees = B2CSession.Fees;
            Mappings mappings = B2CSession.Mappings;
            Services services = B2CSession.Services;
            Remarks remarks = B2CSession.Remarks;
            Payments payments = B2CSession.Payments;
            Taxes taxes = B2CSession.Taxes;
            Library objLi = null;

            try
            {
                if (!string.IsNullOrEmpty(B2CSetting.B2BCookieName))
                    strCookieName = B2CSetting.B2BCookieName;
                if (B2CSession.LanCode != null)
                    strLang = B2CSession.LanCode;

                // Check Agency Admin Logon
                objSvc.objService = B2CSession.AgentService;
                DataSet dsAgency = objSvc.AgencyRead(strAgencyCode.Trim(), false);
                if (dsAgency != null)
                {
                    if (dsAgency.Tables[0].Rows.Count > 0)
                    {
                        string strComparedAgencyLogon = dsAgency.Tables[0].Rows[0]["agency_logon"].ToString();
                        string strComparedAgencyPassword = dsAgency.Tables[0].Rows[0]["agency_password"].ToString();
                        if (string.Compare(strComparedAgencyLogon, strUserName, true) == 0 && string.Compare(strComparedAgencyPassword, strPassword, true) == 0)
                        {
                            bPass = true;
                        }
                    }
                }

                // if pass
                if (bPass)
                {
                    objLi = new Library();
                    //Clear temp availability
                    if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                    {
                        //Clear block inventory
                        objSvc.ReleaseFlightInventorySession(bookingHeader.booking_id.ToString(), string.Empty, string.Empty, string.Empty, false, true, true);
                        //Clear booking in session
                        objLi.ClearBooking(ref bookingHeader,
                                            ref itinerary,
                                            ref passengers,
                                            ref quotes,
                                            ref fees,
                                            ref mappings,
                                            ref services,
                                            ref remarks,
                                            ref payments,
                                            ref taxes);
                    }
                    //Remove Clear session check
                    Session.Remove("bHttps");
                    strResult = B2CSetting.B2BAdminLocation;
                    iAddedCookieTime = B2CSetting.LogonCookieLifeTime;
                    aCookieLogon = objHelper.CreateCookie(strCookieName, strUserName.Trim() + "|" + strAgencyCode.Trim() + "|" + strPassword.Trim() + "|" + strLang + "|0", iAddedCookieTime, CookiePeriod.Minute);
                }
                else
                {
                    iAddedCookieTime = -1;
                    aCookieLogon = objHelper.CreateCookie(strCookieName, strUserName.Trim() + "|" + strAgencyCode.Trim() + "|" + strPassword.Trim() + "|" + strLang + "|0", iAddedCookieTime, CookiePeriod.Day);
                    strResult = "Error,0";
                }
                objSvc.objService = null;
                objSvc = null;
                System.Web.HttpContext.Current.Response.Cookies.Add(aCookieLogon);
            }
            catch (Exception ex)
            {
                strResult = "Error," + ex.Message;
                objHelper.SendErrorEmail(ex, "Agency Code : " + strAgencyCode + "<br/>Username : " + strUserName);
            }
            return strResult;
        }

        #endregion

        //start wellnet
        private string GenerateTransactionNumber()
        {
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }
            return string.Format("{0:x}", i - DateTime.Now.Ticks).Remove(11, 2);
        }
        //end wellnet

        #region QPay Payment
        [WebMethod(EnableSession = true)]
        public string QPayPaymentInit()
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;

                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("QPayPaymentSetting");

                string orderIdent = GeneratePaymentReference();
                string returnUrl = Setting["returnUrl"] + orderIdent;
                string strEncryptText = Setting["customerId"] +
                                        Setting["shopId"] +
                                        orderIdent +
                                        returnUrl +
                                        bookingHeader.language_rcd.ToLower() +
                                        Setting["Secret"];

                string strEncryptResult = "";

                System.Security.Cryptography.SHA512 sha512 = System.Security.Cryptography.SHA512.Create();
                byte[] hash2 = sha512.ComputeHash(Encoding.ASCII.GetBytes(strEncryptText));
                StringBuilder digest = new StringBuilder();
                foreach (byte n in hash2)
                    digest.Append(n.ToString("x2"));
                strEncryptResult = digest.ToString();


                string strParam = "customerId=" + Setting["customerId"] + "&" +
                                    "orderIdent=" + orderIdent + "&" +
                                    "returnUrl=" + returnUrl + "&" +
                                    "language=" + bookingHeader.language_rcd.ToLower() + "&" +
                                    "requestFingerprint=" + strEncryptResult;
                if (Setting["shopId"] != null && Setting["shopId"] != "")
                    strParam += "&shopId=" + Setting["shopId"];
                if (Setting["javascriptScriptVersion"] != null && Setting["javascriptScriptVersion"] != "")
                    strParam += "&javascriptScriptVersion=" + Setting["javascriptScriptVersion"];

                B2CSession.PaymentRequestStreamText = strParam;

                string strRequestResult = "";

                strRequestResult = HttpRequestPost(Setting["InitURL"], strParam, Setting["UserAgent"]);

                return strRequestResult + "&orderIdent=" + orderIdent;

            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, ex.Message);
                throw ex;
            }
        }

        public static string HttpRequestPost(string strUrl, string strParam, string strUserAgent)
        {
            string strResult = "";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strUrl);
            string proxy = null;
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(strParam);

            request.UserAgent = strUserAgent;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = buffer.Length;
            request.Proxy = new WebProxy(proxy, true); // ignore for local addresses
            request.KeepAlive = false;

            // add form data to request stream
            using (System.IO.Stream reqst = request.GetRequestStream())
            {
                reqst.Write(buffer, 0, buffer.Length);
            }

            using (HttpWebResponse res = (HttpWebResponse)request.GetResponse())
            {
                // display HTTP response
                using (System.IO.Stream resst = res.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(resst))
                    {
                        strResult = sr.ReadToEnd();
                    }
                }
            }

            return HttpUtility.UrlDecode(strResult);
        }

        [WebMethod(EnableSession = true)]
        public string QPayPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.QPay, xmlPayment);

            return result;
        }

        #endregion

        #region PayPal Payment
        [WebMethod(EnableSession = true)]
        public string PayPalPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            B2CVariable objUv = B2CSession.Variable;
            objUv.GatewayRepeat = objUv.GatewayRepeat + 1;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.PayPal, xmlPayment);

            return result;
        }

        [WebMethod(EnableSession = true)]
        public string GetPayPalFee()
        {
            string result = string.Empty;
            BookingHeader header = B2CSession.BookingHeader;
            Itinerary itinerary = B2CSession.Itinerary;
            Passengers passengers = B2CSession.Passengers;
            Quotes quotes = B2CSession.Quotes;
            Mappings mappings = B2CSession.Mappings;
            Taxes taxes = B2CSession.Taxes;
            Payments payments = B2CSession.Payments;
            Fees fees = new Fees();
            if (B2CSession.Fees != null)
            {
                for (int i = 0; i < B2CSession.Fees.Count; i++)
                    fees.Add(B2CSession.Fees[i]);
            }

            fees.objService = B2CSession.AgentService;
            fees.GetPayPalSubTypeFee(header, itinerary, passengers, quotes, fees, payments);

            StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
            Helper objXsl = new Helper();
            Library objLi = new Library();
            System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);
            System.Xml.Xsl.XslTransform objTransform = objXsl.GetXSLDocument("FareSummary");

            using (StringWriter stw = new StringWriter())
            {
                using (XmlWriter xtw = XmlWriter.Create(stw))
                {
                    xtw.WriteStartElement("Booking");
                    {
                        objLi.BuiltBookingXml(xtw, null, itinerary, passengers, quotes, fees, mappings, null, null, null, taxes);
                    }
                    xtw.WriteEndElement();
                }
                objTransform = objXsl.GetXSLDocument("FareSummary");
                result = objLi.RenderHtml(objTransform, objArgument, stw.ToString());
            }

            return result;
        }

        [WebMethod(EnableSession = true)]
        public string GetFareSummary()
        {
            string result = string.Empty;
            BookingHeader header = B2CSession.BookingHeader;
            Itinerary itinerary = B2CSession.Itinerary;
            Passengers passengers = B2CSession.Passengers;
            Quotes quotes = B2CSession.Quotes;
            Mappings mappings = B2CSession.Mappings;
            Taxes taxes = B2CSession.Taxes;
            Fees fees = B2CSession.Fees;

            StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
            Helper objXsl = new Helper();
            Library objLi = new Library();
            System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);
            System.Xml.Xsl.XslTransform objTransform = objXsl.GetXSLDocument("FareSummary");

            using (StringWriter stw = new StringWriter())
            {
                using (XmlWriter xtw = XmlWriter.Create(stw))
                {
                    xtw.WriteStartElement("Booking");
                    {
                        objLi.BuiltBookingXml(xtw, null, itinerary, passengers, quotes, fees, mappings, null, null, null, taxes);
                    }
                    xtw.WriteEndElement();
                }

                objTransform = objXsl.GetXSLDocument("FareSummary");
                result = objLi.RenderHtml(objTransform, objArgument, stw.ToString());
            }

            return result;
        }

        #endregion

        #region Zenith Payment
        [WebMethod(EnableSession = true)]
        public string ZenithPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.Zenith, xmlPayment);

            return result;
        }

        #endregion

        #region Ozinbo Payment
        [WebMethod(EnableSession = true)]
        public string OzinboPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.Ozinbo, xmlPayment);

            return result;
        }

        #endregion

        #region BBL Payment
        [WebMethod(EnableSession = true)]
        public string BBLPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.BBL, xmlPayment);

            return result;
        }
        #endregion

        #region Piraeus Payment
        [WebMethod(EnableSession = true)]
        public string PiraeusPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.Piraeus, xmlPayment);

            return result;
        }

        #endregion

        #region Doku Payment
        [WebMethod(EnableSession = true)]
        public string DokuPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.Doku, xmlPayment);

            return result;
        }
        #endregion

        #region PostFinance Payment
        [WebMethod(EnableSession = true)]
        public string PostFinancePaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.PostFinance, xmlPayment);

            return result;
        }
        #endregion

        #region Yahoo Payment
        [WebMethod(EnableSession = true)]
        public string YahooPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            Library objLi = new Library();
            BookingHeader bookingHeader = B2CSession.BookingHeader;
            Itinerary itinerary = B2CSession.Itinerary;
            Passengers passengers = B2CSession.Passengers;
            Mappings mappings = B2CSession.Mappings;
            if (objLi.ValidSave(bookingHeader, itinerary, passengers, mappings) == true)
                result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.Yahoo, xmlPayment);

            return result;
        }
        #endregion

        #region Ogone3DPaymentRequest
        [WebMethod(EnableSession = true)]
        public string Ogone3D2015PaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.Ogone3D, xmlPayment);

            return result;
        }
        #endregion

        #region Satim Payment
        [WebMethod(EnableSession = true)]
        public string SatimPaymentRequest(string xmlPayment)
        {
            string result = string.Empty;
            result = GeneratePaymentRequest(PaymentGatewayFactory.GateWayType.Satim, xmlPayment);

            return result;
        }

        #endregion

        private string GeneratePaymentRequest(PaymentGatewayFactory.GateWayType egateway, string xmlPayment)
        {
            B2CVariable objUv = B2CSession.Variable;
            BookingHeader bookingHeader = B2CSession.BookingHeader;
            Itinerary itinerary = B2CSession.Itinerary;
            Passengers passengers = B2CSession.Passengers;
            Quotes quotes = B2CSession.Quotes;
            Fees fees = B2CSession.Fees;
            Mappings mappings = B2CSession.Mappings;
            Services services = B2CSession.Services;
            Remarks remarks = B2CSession.Remarks;
            Payments payments = B2CSession.Payments;
            Taxes taxes = B2CSession.Taxes;

            PaymentGatewayFactory.GateWayType gateway = egateway;
            tikSystem.Web.Contact.PaymentGateway objGateway = PaymentGatewayFactory.GetGatePaymentGateway(gateway);
            bookingHeader.approval_flag = 1;

            Library objLi = new Library();
            Payments objPayment = new Payments();
            string result = string.Empty;
            DateTime temp_timelimit_date_time = DateTime.MinValue;
            try
            {
                objPayment.objService = B2CSession.AgentService;
                //Set update information
                SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);
                Fees objPaymentFees = null;
                if (objPayment.FillCreditCardOffline(ref bookingHeader,
                                                         ref itinerary,
                                                         ref passengers,
                                                         ref quotes,
                                                         ref fees,
                                                         ref objPaymentFees,
                                                         ref mappings,
                                                         ref services,
                                                         ref remarks,
                                                         ref payments,
                                                         ref taxes,
                                                         xmlPayment,
                                                         objUv.FormOfPaymentFee,
                                                         objUv.UserId,
                                                         objUv.booking_payment_id,
                                                         objUv.ip_address) == true)
                {
                    if (objPayment.Count > 0)
                    {
                        if (bookingHeader.record_locator.Length == 0)
                        {
                            B2CSession.BookingHeader = bookingHeader;
                            B2CSession.Itinerary = itinerary;
                            B2CSession.Passengers = passengers;
                            B2CSession.Quotes = quotes;
                            B2CSession.Fees = fees;
                            B2CSession.Mappings = mappings;
                            B2CSession.Services = services;
                            B2CSession.Remarks = remarks;
                            B2CSession.Payments = payments;
                            B2CSession.Taxes = taxes;

                            // Edit TKTL Timelimit
                            temp_timelimit_date_time = CalculateTKTLTimelimit(remarks);

                            //Save change information
                            Booking objBooking = new Booking();
                            objBooking.objService = B2CSession.AgentService;
                            result = objBooking.SaveBooking(false,
                                                            ref bookingHeader,
                                                            ref itinerary,
                                                            ref passengers,
                                                            ref quotes,
                                                            ref fees,
                                                            ref mappings,
                                                            ref services,
                                                            ref remarks,
                                                            ref payments,
                                                            ref taxes,
                                                            Classes.Language.CurrentCode().ToUpper());
                            if (result.Length > 0)
                            {
                                //Check Error Response
                                XPathDocument xmlDoc = new XPathDocument(new StringReader(result));
                                XPathNavigator nv = xmlDoc.CreateNavigator();
                                if (nv.Select("ErrorResponse/Error").Count == 0)
                                {
                                    // Get Booking session in webservice prevent user click back button in browsers.
                                    ServiceClient obj = new ServiceClient();
                                    obj.objService = B2CSession.AgentService;
                                    obj.GetBooking(bookingHeader.booking_id);

                                    //Fill new booking xml into booking object.
                                    objLi.FillBooking(result,
                                                        ref bookingHeader,
                                                        ref passengers,
                                                        ref itinerary,
                                                        ref mappings,
                                                        ref payments,
                                                        ref remarks,
                                                        ref taxes,
                                                        ref quotes,
                                                        ref fees,
                                                        ref services);

                                    if (bookingHeader.booking_date_time == DateTime.MinValue)
                                    {
                                        bookingHeader.booking_date_time = DateTime.Now;
                                    }
                                    B2CSession.BookingHeader = bookingHeader;

                                    foreach (Remark r in remarks)
                                    {
                                        if (r.remark_type_rcd == "TKTL")
                                            r.temp_timelimit_date_time = temp_timelimit_date_time;
                                    }
                                }
                            }
                        }
                    }

                    if (bookingHeader.record_locator.Length == 0)
                        return "Error,002";//Resources.ErrorMessage.SaveBooking;
                    else
                    {
                        objUv.PaymentReference = GeneratePaymentReference();
                        objUv.PaymentGateway = egateway.ToString();
                        string BookingPaymentID = "";
                        string PaymentApprovalID = Guid.NewGuid().ToString();
                        string trans_desc = string.Empty;
                        XPathDocument xmlDoc;
                        XPathNavigator nv;
                        string CardNo = string.Empty;

                        xmlDoc = new XPathDocument(new StringReader(xmlPayment));
                        nv = xmlDoc.CreateNavigator();

                        foreach (XPathNavigator n in nv.Select("payment"))
                        {
                            CardNo = objLi.getXPathNodevalue(n, "CreditCardNumber", Library.xmlReturnType.value);
                        }

                        if (objPayment[0].booking_payment_id == null || objPayment[0].booking_payment_id.ToString() == string.Empty)
                            BookingPaymentID = Guid.NewGuid().ToString();
                        else
                            BookingPaymentID = objPayment[0].booking_payment_id.ToString();

                        int issue_number = 0;
                        if (objPayment[0].issue_number.Trim() != "")
                            issue_number = Convert.ToInt16(objPayment[0].issue_number);
                        bool success = objPayment.objService.InsertPaymentApproval(PaymentApprovalID,
                                                                objPayment[0].form_of_payment_subtype_rcd, CardNo, objPayment[0].name_on_card,
                                                                objPayment[0].expiry_month, objPayment[0].expiry_year,
                                                                objPayment[0].issue_month, objPayment[0].issue_year,
                                                                issue_number, string.Empty,
                                                                BookingPaymentID,
                                                                bookingHeader.currency_rcd,
                                                                Convert.ToDouble(objPayment[0].payment_amount),
                                                                objUv.ip_address);
                        if (success)
                        {
                            objGateway.BookingHeader = bookingHeader;
                            objGateway.Mappings = mappings;
                            objGateway.Passengers = passengers;
                            objGateway.Itinerary = itinerary;
                            objGateway.Quotes = quotes;
                            objGateway.Fees = fees;
                            objGateway.PaymentFees = objPaymentFees;
                            objGateway.PaymentRequestReference = objUv.PaymentReference;
                            objGateway.PaymentXml = xmlPayment;
                            objGateway.redirect_cc_fee_amount = objPayment[0].payment_amount + GetPaymentGatewayFee(xmlPayment);
                            objGateway.PayRef = Session.SessionID;
                            objGateway.AgencyCode = objUv.Agency_Code;
                            objGateway.Language = Classes.Language.CurrentFullCode();
                            objGateway.CurrencyCode = bookingHeader.currency_rcd;

                            Currencies currencies = CacheHelper.CacheCurrencies();
                            for (int i = 0; i < currencies.Count; i++)
                            {
                                if (currencies[i].currency_rcd.ToUpper() == bookingHeader.currency_rcd.ToUpper())
                                {
                                    objGateway.CurrencyNumber = currencies[i].currency_number;
                                    i = currencies.Count;
                                }
                            }

                            if (objGateway.CCPayment == null)
                                objGateway.CCPayment = new Payments();
                            else
                                objGateway.CCPayment.Clear();
                            objGateway.CCPayment.Add(objGateway.CCPayment.CreateCreditCardPaymentInput(xmlPayment));

                            //Session.Timeout = 30;                            
                            result = objGateway.PaymentRequest();
                            string ResponseCode = string.Empty;
                            string ResponseMsg = string.Empty;
                            string NCSTATUS = string.Empty;
                            string NCERROR = string.Empty;
                            string NCERRORPLUS = string.Empty;
                            if (!string.IsNullOrEmpty(result) && egateway == PaymentGatewayFactory.GateWayType.Ogone3D)
                            {
                                try
                                {
                                    Ogone3DPaymentResponse response = JsonConvert.DeserializeObject<Ogone3DPaymentResponse>(result);
                                    if (response != null)
                                    {
                                        if (!string.IsNullOrEmpty(response.code))
                                        {
                                            //ResponseCode = response.code;
                                            ResponseMsg = response.msg;
                                            if (response.error != null)
                                            {
                                                NCSTATUS = response.error["NCSTATUS"];
                                                NCERROR = response.error["NCERROR"];
                                                NCERRORPLUS = response.error["NCERRORPLUS"];
                                                ResponseCode = "DECLINED";
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogsManagement.SaveErrorLogs(ex, string.Format("OGONE error response: {0}", result));
                                }
                            }

                            if (egateway == PaymentGatewayFactory.GateWayType.Satim)
                            {
                                objUv.PaymentReference = objGateway.CvvResponse;
                            }

                            trans_desc = bookingHeader.booking_id.ToString() + "," +
                                            "B2C" + "," + objPayment[0].form_of_payment_rcd + "," +
                                            itinerary[0].airline_rcd + "," + itinerary[0].flight_number + "," +
                                            itinerary[0].departure_date.ToString("dd MMM yyyy") + "," + itinerary[0].departure_time.ToString("00:00");
                            if (itinerary.Count > 1)
                                trans_desc += " :: " +
                                                itinerary[1].airline_rcd + "," + itinerary[1].flight_number + "," +
                                                itinerary[1].departure_date.ToString("dd MMM yyyy") + "," + itinerary[1].departure_time.ToString("00:00");

                            success = objPayment.objService.UpdatePaymentApproval(string.Empty,
                                                            objUv.PaymentReference,
                                                            objGateway.PayRef,
                                                            trans_desc,
                                                            string.Empty,
                                                            objGateway.AvsResponse,
                                                            string.Empty,
                                                            string.Empty,
                                                            NCERROR,
                                                            string.Empty,
                                                            string.Empty,
                                                            ResponseCode,
                                                            NCSTATUS,
                                                            string.Empty,
                                                            PaymentApprovalID,
                                                            objGateway.PaymentRequestStreamText,
                                                            objGateway.ResponseStream,
                                                            string.Empty,
                                                            string.Empty);

                            switch (egateway)
                            {
                                case PaymentGatewayFactory.GateWayType.Ogone3D:

                                    //var resultX = new System.Web.Script.Serialization.JavaScriptSerializer().de
                                    //var objGatewayOgone = objGateway as Ogone3DPayment;
                                    //if (objGatewayOgone.Response.code == "200")
                                    //{
                                    //    objGatewayOgone.PaymentDone();
                                    //}
                                    //else
                                    //{
                                    //    objGatewayOgone.PaymentFail();
                                    //}
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            objUv.PaymentGateway = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                result = "Error," + ex.Message.ToString();
            }
            finally
            {
                objGateway = null;
                objLi = null;
                objPayment = null;
            }

            return result;
        }

        private DateTime CalculateTKTLTimelimit(Remarks objRemarks)
        {
            DateTime temp_timelimit_date_time = DateTime.MinValue;
            if (B2CSetting.TimelimitMinute > 0)
            {
                double timelimitMinute = B2CSetting.TimelimitMinute;
                foreach (Remark obj in objRemarks)
                {
                    if (obj.remark_type_rcd == "TKTL")
                    {
                        obj.temp_timelimit_date_time = obj.timelimit_date_time;
                        obj.timelimit_date_time = DateTime.Now.AddMinutes(timelimitMinute);
                        temp_timelimit_date_time = obj.temp_timelimit_date_time;
                    }
                }
            }
            return temp_timelimit_date_time;
        }

        private string GeneratePaymentReference()
        {
            String strReturn = String.Empty;
            if (B2CSetting.PaymentRefDigit == 10)
                strReturn = string.Format("{0:d10}", (DateTime.Now.Ticks / 10) % 1000000000);
            else
                strReturn = string.Format("{0:yyMMddHHmmssFFF}", DateTime.Now);
            return strReturn;
        }

        private decimal GetPaymentGatewayFee(string xml)
        {
            decimal dFee = 0;
            try
            {
                if (!string.IsNullOrEmpty(xml))
                {
                    using (StringReader srd = new StringReader(xml))
                    {
                        XPathDocument xmlDoc = new XPathDocument(srd);
                        XPathNavigator nv = xmlDoc.CreateNavigator();
                        foreach (XPathNavigator n in nv.Select("payment"))
                        {
                            dFee = XmlHelper.XpathValueNullToDecimal(n, "fee_amount_incl_gateway");
                        }
                    }
                }
                return dFee;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "," + xml);
            }
        }
    }
}
