using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Web.Script.Services;
using System.Text;
using tikSystem.Web.Library;
using tikAeroB2C.Classes;
using tikSystem.Web.Library.agentservice;
namespace tikAeroB2C.WebService
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class WebUIRender : System.Web.Services.WebService
    {

        // for jquery ui
        [WebMethod(EnableSession = true)]
        public string GetDLLCalendar(string ID, string LimitYear, string StartMonth, string IsBeginCurrentMont, string flightType, string monthFormat, bool IsRenderCalendaIcon, bool IsVisibleCalendarIcon)
        {
            if (IsBeginCurrentMont == "") IsBeginCurrentMont = "1";
            UserControls.Calendar Cal = new UserControls.Calendar();
            Cal.RenderCtrlType = UserControls.Calendar.uxControlType.DropdownList;
            Cal.id = ID;
            Cal.LimitYear = LimitYear;
            Cal.StartMonth = StartMonth;
            Cal.IsBeginCurrentMont = IsBeginCurrentMont;
            Cal.flightType = flightType;
            Cal.monthFormat = monthFormat;
            string strx = Cal.GetHTML(IsRenderCalendaIcon, IsVisibleCalendarIcon);
            return strx;
        }

        [WebMethod(EnableSession = true)]
        public string GetCalendar(string ID, string flightType, string isonlyonemonth, string date, string month, string year, string mxMY, string mnMY, string Lang, string from, string to, int calType)
        {

            UserControls.Calendar Cal = new UserControls.Calendar();
            Cal.RenderCtrlType = UserControls.Calendar.uxControlType.Calendar;

            Cal.id = ID;
            tikAeroB2C.Classes.UserCalandar.activeCalendar = ID;
            if (month != "" && date != "")
            {
                setSelectedDate(year + Convert.ToInt16(month).ToString("00") + Convert.ToInt16(date).ToString("00"), ID);
            }

            Cal.Currentculture = Lang;

            // string date,string month,string year
            Cal.day = ((date == "") ? 0 : Convert.ToInt32(date));
            Cal.month = ((month == "") ? 0 : Convert.ToInt32(month));
            Cal.year = ((year == "") ? 0 : Convert.ToInt32(year));
            Cal.strFrom = from;
            Cal.strTo = to;

            // to change cell color when have flight
            if (calType.Equals(2))
            {
                if (flightType == "outward")
                    Cal.dsDateFlightInMonth = GetFlightDailyCount(Cal.day, Cal.year, Cal.month, from, to); //���¡ web service ���¡ web service �������
                else if (flightType == "return")
                    Cal.dsDateFlightInMonth = GetFlightDailyCount(Cal.day, Cal.year, Cal.month, to, from);

                Cal.flightType = flightType;
                Cal.showDateFlight = true;
                //Cal.dsDateFlightInMonth = GetFlightDailyCount(Cal.day, Cal.year, Cal.month, from, to);
            }
            else
                Cal.showDateFlight = false;

           

            if (isonlyonemonth == "1") Cal.isonlyonemonth = true;
            if (mxMY != "") Cal.mxMY = mxMY;
            if (mnMY != "") Cal.mnMY = mnMY;

            string strx = Cal.GetHTML(calType);
            return strx;

        }
        [WebMethod(EnableSession = true)]
        public string GetDLLCalendar(string ID, string LimitYear, string StartMonth, string IsBeginCurrentMont, string flightType, string monthFormat) 
        {
            if (IsBeginCurrentMont=="")IsBeginCurrentMont = "1";
            UserControls.Calendar Cal = new UserControls.Calendar();
            Cal.RenderCtrlType = UserControls.Calendar.uxControlType.DropdownList;
            Cal.id = ID;
            Cal.LimitYear = LimitYear;
            Cal.StartMonth = StartMonth;
            Cal.IsBeginCurrentMont = IsBeginCurrentMont;
            Cal.flightType = flightType;
            Cal.monthFormat = monthFormat;
            string strx = Cal.GetHTML();
            return strx;
        }

        [WebMethod(EnableSession = true)]
        private DataSet GetFlightDailyCount(int date, int year, int month, string from, string to)
        {
            DateTime bDate;
            DateTime eDate;
            DateTime selectDate = DateTime.Now;
            //DateTime currDate = new DateTime(year, month, 1);
            DataSet ret = new DataSet();

            //if (date == 0)//next,prev month
            //{
            //    selectDate = new DateTime(year, month, 1);
            //}

            //if (DateTime.Compare(selectDate, currDate) <= 0)
            //{

                if (selectDate.Month == month)//current month
                {
                    if(date == 0)
                        bDate = new DateTime(year, month, DateTime.Now.Day);
                    else
                        bDate = new DateTime(year, month, date);
                }
                else//next month
                {
                    bDate = new DateTime(year, month, 1);
                }
                //-------------------------------------
                int end = DateTime.DaysInMonth(year, month);
                eDate = new DateTime(year, month, end);
                //-------------------------------------
                ServiceClient obj = new ServiceClient();
                WebService.B2cService service = new tikAeroB2C.WebService.B2cService();
                obj.objService = B2CSession.AgentService;

                //TikAeroWebB2E.BaseClass.UserDeatil objDetail = (TikAeroWebB2E.BaseClass.UserDeatil)Session["UserDetail"];
                ret = obj.GetFlightDailyCount(bDate, eDate, from, to);
            //}

            return ret;
        }
        //---------------------------------------------------------------------------------
        // set format date as year month day
        [WebMethod(EnableSession = true)]
        public void setSelectedDate(string year_month_date, string id)
        {
            if (id == "1")
            {
                tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate1 = year_month_date;

                if (tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate2 != "")
                {
                    string s1 = tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate1;
                    string s2 = tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate2;

                    DateTime d1 = new DateTime(Convert.ToInt32(s1.Substring(0, 4)), Convert.ToInt32(s1.Substring(4, 2)), Convert.ToInt32(s1.Substring(6, 2)));
                    DateTime d2 = new DateTime(Convert.ToInt32(s2.Substring(0, 4)), Convert.ToInt32(s2.Substring(4, 2)), Convert.ToInt32(s2.Substring(6, 2)));

                    if (tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate2 == "" ||
                        d2 > d1)
                    {
                        tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate2 = year_month_date;
                    }
                }

            }
            else
            {
                tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate2 = year_month_date;
            }

        }

    }
}
