using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace tikAeroB2C
{
    public partial class TicketPopUp : System.Web.UI.Page
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        #region Private Methods
        private string BookingID
        {
            get
            {
                object o = Request.QueryString["bid"];
                return o != null ? Convert.ToString(o) : string.Empty;
            }
        }

        private string PassengerID
        {
            get
            {
                object o = Request.QueryString["pid"];
                return o != null ? Convert.ToString(o) : string.Empty;
            }
        }

        private string BookingSegmentID
        {
            get
            {
                object o = Request.QueryString["sid"];
                return o != null ? Convert.ToString(o) : string.Empty;
            }
        }
        #endregion

        #region Page Load Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                //Load Language Dictionary
                _stdLanguage = Classes.Language.GetLanguageDictionary();
                if (B2CSession.Client != null)
                {
                    if (BookingID != "" && PassengerID != "" && BookingSegmentID != "")
                    {
                        Client objClient = B2CSession.Client;
                        ServiceClient obj = new ServiceClient();
                        obj.objService = B2CSession.AgentService;
                        string serverPath = Server.MapPath("~") + @"\xsl\";
                        Library ObjLi = new Library();
                        string bookID = BookingID;
                        string passengerID = PassengerID;
                        string segmentID = BookingSegmentID;
                        string ticketNumber = "";
                        string xmlTaxs = "";
                        bool bReadonly = false;
                        bool bReturnTax = true;

                        DataSet ds = obj.TicketRead(ref bookID, ref passengerID, ref segmentID, ref ticketNumber, ref xmlTaxs, ref bReadonly, ref bReturnTax);

                        obj = null;

                        Helper objXsl = new Helper();
                        System.Xml.Xsl.XslTransform objTransform = null;
                        System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                        string xml = "<object>" + ReformatXML(ds.GetXml()) + CalculateTaxVat(xmlTaxs) + "</object>";

                        objTransform = objXsl.GetXSLDocument("PopupTicket");
                        string outPut = ObjLi.RenderHtml(objTransform, objArgument, xml);
                        Response.Write(outPut);

                    }
                    else
                    {
                        string msg = "Cann't view this page.";
                        Response.Write("<script language='javascript'>alert('" + msg + "');window.close();</script>");
                    }
                }
            }
        }
        #endregion

        #region CalculateTaxVat
        public string CalculateTaxVat(string xml)
        {
            xml = xml.Replace("</Taxes>", "<sales_vat>0</sales_vat></Taxes>");
            XmlDocument XmlDoc = new XmlDocument();

            XmlDoc.LoadXml(xml);

            foreach (XmlNode n in XmlDoc.SelectNodes("NewDataSet/Taxes"))
            {
                double total = Convert.ToDouble((n["sales_amount_incl"] != null) ? n["sales_amount_incl"].InnerXml : "0", new System.Globalization.CultureInfo("en-US")) -
                    Convert.ToDouble((n["sales_amount"] != null) ? n["sales_amount"].InnerXml : "0", new System.Globalization.CultureInfo("en-US"));
                n["sales_vat"].InnerXml = total.ToString(new System.Globalization.CultureInfo("en-US")); //total.ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //n["tax_amount"].InnerXml = Convert.ToDouble(n["tax_amount"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));
                //n["tax_amount_incl"].InnerXml = Convert.ToDouble(n["tax_amount_incl"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));
                //n["sales_amount"].InnerXml = Convert.ToDouble(n["sales_amount"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));
                //n["sales_amount_incl"].InnerXml = Convert.ToDouble(n["sales_amount_incl"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));
                //n["vat_percentage"].InnerXml = Convert.ToDouble(n["vat_percentage"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));
            }

            return XmlDoc.InnerXml;
        }
        #endregion

        #region ReformatXML
        private string ReformatXML(string xml)
        {
            XmlDocument XmlDoc = new XmlDocument();

            XmlDoc.LoadXml(xml);

            foreach (XmlNode n in XmlDoc.SelectNodes("NewDataSet/Ticket"))
            {
                if (n["ticket_number"] != null && n["ticket_number"].InnerXml != "")
                    n["ticket_number"].InnerXml = n["ticket_number"].InnerXml.Substring(0, 3) + "-" + n["ticket_number"].InnerXml.Substring(3, 4) + "-" +
                        n["ticket_number"].InnerXml.Substring(7, 3) + "-" + n["ticket_number"].InnerXml.Substring(10, 3);

                if (n["departure_date"] != null && n["departure_date"].InnerXml != "")
                    n["departure_date"].InnerXml = Convert.ToDateTime(n["departure_date"].InnerXml).ToString("dd-MMM-yy");

                if (n["fare_date_time"] != null && n["fare_date_time"].InnerXml != "")
                    n["fare_date_time"].InnerXml = Convert.ToDateTime(n["fare_date_time"].InnerXml).ToString("dd-MMM-yy");

                if (n["ticketing_date_time"] != null && n["ticketing_date_time"].InnerXml != "")
                    n["ticketing_date_time"].InnerXml = Convert.ToDateTime(n["ticketing_date_time"].InnerXml).ToString("dd-MMM-yy");

                if (n["not_valid_before_date"] != null && n["not_valid_before_date"].InnerXml != "")
                    n["not_valid_before_date"].InnerXml = Convert.ToDateTime(n["not_valid_before_date"].InnerXml).ToString("dd-MMM-yy");

                if (n["not_valid_after_date"] != null && n["not_valid_after_date"].InnerXml != "")
                    n["not_valid_after_date"].InnerXml = Convert.ToDateTime(n["not_valid_after_date"].InnerXml).ToString("dd-MMM-yy");

                //if (n["baggage_weight"] != null && n["baggage_weight"].InnerXml != "")
                //    n["baggage_weight"].InnerXml = Convert.ToDouble(n["baggage_weight"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["payment_amount"] != null && n["payment_amount"].InnerXml != "")
                //    n["payment_amount"].InnerXml = Convert.ToDouble(n["payment_amount"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["net_total"] != null && n["net_total"].InnerXml != "")
                //    n["net_total"].InnerXml = Convert.ToDouble(n["net_total"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["tax_amount"] != null && n["tax_amount"].InnerXml != "")
                //    n["tax_amount"].InnerXml = Convert.ToDouble(n["tax_amount"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["fare_vat"] != null && n["fare_vat"].InnerXml != "")
                //    n["fare_vat"].InnerXml = Convert.ToDouble(n["fare_vat"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["fare_amount"] != null && n["fare_amount"].InnerXml != "")
                //    n["fare_amount"].InnerXml = Convert.ToDouble(n["fare_amount"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["fare_amount_incl"] != null && n["fare_amount_incl"].InnerXml != "")
                //    n["fare_amount_incl"].InnerXml = Convert.ToDouble(n["fare_amount_incl"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["acct_fare_amount"] != null && n["acct_fare_amount"].InnerXml != "")
                //    n["acct_fare_amount"].InnerXml = Convert.ToDouble(n["acct_fare_amount"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["acct_fare_amount_incl"] != null && n["acct_fare_amount_incl"].InnerXml != "")
                //    n["acct_fare_amount_incl"].InnerXml = Convert.ToDouble(n["acct_fare_amount_incl"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));

                //if (n["acct_tax_amount_incl"] != null && n["acct_tax_amount_incl"].InnerXml != "")
                //    n["acct_tax_amount_incl"].InnerXml = Convert.ToDouble(n["acct_tax_amount_incl"].InnerXml).ToString("###,##0.00", new System.Globalization.CultureInfo("en-US"));
            }

            return XmlDoc.InnerXml;
        }
        #endregion
    }
}
