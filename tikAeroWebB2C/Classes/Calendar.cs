using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace tikAeroB2C.Classes
{
    public static class Calendar
    {
        // for jquery ui
        public static string getDLLCalendar(string ID, string flightType, string monthFormat, bool IsRenderCalendaIcon, bool IsVisibleCalendarIcon)
        {
            WebService.WebUIRender web = new tikAeroB2C.WebService.WebUIRender();
            string ret = web.GetDLLCalendar(ID, "", "", "", flightType, monthFormat, IsRenderCalendaIcon, IsVisibleCalendarIcon);
            return ret;
        }
        public static string getDLLCalendar(string ID,string flightType, string monthFormat)
        {
            WebService.WebUIRender web = new tikAeroB2C.WebService.WebUIRender();
            string ret = web.GetDLLCalendar(ID, "", "", "", flightType, monthFormat);
            return ret;
              

        }
        public static string getDLLCalendar(string ID, string flightType)
        {
            WebService.WebUIRender web = new tikAeroB2C.WebService.WebUIRender();
            string ret = web.GetDLLCalendar(ID, "", "", "", flightType, "MMMM");
            return ret;
        }
    }

    public static class UserCalandar
    {
        public static string selectedYearMonthDate1 = "";
        public static string selectedYearMonthDate2 = "";
        public static string activeCalendar = "1";
    }   

}
