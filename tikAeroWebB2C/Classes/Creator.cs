﻿using System;
using System.Collections.Generic;
using System.Web;

using System.Data;
using tikSystem.Web.Library;

namespace tikAeroB2C
{
    public class Creator
    {
        public static DataSet GetDataSetOrigin()
        {
            try
            {
                DataSet ds = (DataSet)HttpRuntime.Cache["origin-" + tikAeroB2C.Classes.Language.CurrentCode().ToUpper()];
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    ServiceClient objClient = new ServiceClient();
                    //Booking objBooking = new Booking();
                    //ds = objClient.GetSessionlessOrigins(tikAeroB2C.Classes.Language.CurrentCode().ToUpper(), true, false, false, false, false, objBooking.GenerateSessionlessToken());
                    ds = objClient.GetSessionlessOrigins(tikAeroB2C.Classes.Language.CurrentCode().ToUpper(), true, false, false, false, false, SecurityHelper.GenerateSessionlessToken());
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("origin-" + tikAeroB2C.Classes.Language.CurrentCode().ToUpper(), ds, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    }

                    return ds;
                }
            }
            catch (Exception ex)
            {
                //Classes.Helper objHp = new Classes.Helper();
                //objHp.SendErrorEmail(ex, string.Empty);
                //throw ex;
                throw new Exception("Creator::GetDataSetOrigin()");
            }
        }
        public static DataSet GetDataSetDestination()
        {
            try
            {
                DataSet ds = (DataSet)HttpRuntime.Cache["destination-" + tikAeroB2C.Classes.Language.CurrentCode().ToUpper()];
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    ServiceClient objClient = new ServiceClient();
                    ds = objClient.GetSessionlessDestination(tikAeroB2C.Classes.Language.CurrentCode().ToUpper(), true, false, false, false, false, SecurityHelper.GenerateSessionlessToken());
                   
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("destination-" + tikAeroB2C.Classes.Language.CurrentCode().ToUpper(), ds, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    }
                    return ds;
                }
            }
            catch (Exception ex)
            {
                //Classes.Helper objHp = new Classes.Helper();
                //objHp.SendErrorEmail(ex, string.Empty);
                throw new Exception("Creator::GetDataSetDestination()");
            }
        }
        //
        //
        //
        public static DataSet GetDataSetBinRangeSearch()
        {
            try
            {
                DataSet ds = (DataSet)HttpRuntime.Cache["BinRangeSearch"];
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    Payments objPayments = new Payments();
                    objPayments.objService = (tikSystem.Web.Library.agentservice.TikAeroXMLwebservice)Util.GetSession(Cnum.CnumSession.AgentService);
                    ds = objPayments.GetBinRangeSearch(string.Empty, "A", SecurityHelper.GenerateSessionlessToken());

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("BinRangeSearch", ds, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    }
                    return ds;
                }
            }
            catch (Exception ex)
            {
                //Classes.Helper objHp = new Classes.Helper();
                //objHp.SendErrorEmail(ex, string.Empty);
                throw new Exception("Creator::GetBinRangeSearch()");
            }
        }
        //
        //
        //
        public static System.Web.UI.HtmlControls.HtmlGenericControl GetJavaScriptBlockControl()
        {
            System.Web.UI.HtmlControls.HtmlGenericControl js = new System.Web.UI.HtmlControls.HtmlGenericControl("script");
            js.Attributes["language"] = "javascript";
            js.Attributes["type"] = "text/javascript";

            return js;
        }
        public static System.Web.UI.HtmlControls.HtmlGenericControl GetJavaScriptIncludeControl()
        {
            System.Web.UI.HtmlControls.HtmlGenericControl js = new System.Web.UI.HtmlControls.HtmlGenericControl("script");
            js.Attributes["language"] = "javascript";
            //js.Attributes["type"] = "text/javascript";

            return js;
        }
    }
}