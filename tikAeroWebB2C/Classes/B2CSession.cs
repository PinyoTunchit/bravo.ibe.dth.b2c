﻿using System;
using System.Collections.Generic;
using System.Web;
//
using tikSystem.Web.Library;

namespace tikAeroB2C
{
    public class B2CSession
    {
        static bool boolDefaultValue = false;
        static int intDefaultValue = 0;
        static short Int16DefaultValue = 0;
        static string stringDefaultValue = null;

        #region Property
        public static tikSystem.Web.Library.agentservice.TikAeroXMLwebservice AgentService
        {
            get { return Util.GetSession(Cnum.CnumSession.AgentService) as tikSystem.Web.Library.agentservice.TikAeroXMLwebservice; }
            set { Util.SetSession(Cnum.CnumSession.AgentService, value); }
        }
        public static string LanCode
        {
            //Session["LanCode"]
            get { return Util.GetSessionToString(Cnum.CnumSession.LanCode) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.LanCode, value); } 
        }
        public static B2CVariable Variable
        {
            //Session["Booking"]
            get { return Util.GetSession(Cnum.CnumSession.ObjB2CVariable) as B2CVariable; }
            set { Util.SetSession(Cnum.CnumSession.ObjB2CVariable, value); }
        }
        public static Payments CcPayment
        {
            //Session["CcPayment"]        
            get { return Util.GetSession(Cnum.CnumSession.CcPayment) as Payments; }
            set { Util.SetSession(Cnum.CnumSession.CcPayment, value); }
        }
        public static Fees PaymentFees
        {
            //(Fees)Session["PaymentFees"]; 
            get { return Util.GetSession(Cnum.CnumSession.PaymentFees) as Fees; }
            set { Util.SetSession(Cnum.CnumSession.PaymentFees, value); }
        }
        public static string PaymentApprovalID
        {
            //Session["PaymentApprovalID"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.PaymentApprovalID) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.PaymentApprovalID, value); }
        }
        public static string PaymentRequestStreamText
        {
            //Session["PaymentRequestStreamText"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.PaymentRequestStreamText) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.PaymentRequestStreamText, value); }
        }
        public static string ETZEncrypt
        {
            //Session["ETZEncrypt"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.ETZEncrypt) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.ETZEncrypt, value); }
        }
        public static string ITSEncrypt
        {
            //Session["ITSEncrypt"].ToString().ToUpper()
            //get { return (Util.GetSession(Cnum.CnumSession.ITSEncrypt)).ToUpper(); }
            get
            {
                string ret = Util.GetSessionToString(Cnum.CnumSession.ITSEncrypt);
                return ret == null ? stringDefaultValue : ret.ToUpper();
            }
            set { Util.SetSession(Cnum.CnumSession.ITSEncrypt, value); }
            //set { Util.SetSession(Cnum.CnumSession.ITSEncrypt, ((string)value).ToUpper()); }
        }
        public static BookingHeader BookingHeader
        {
            //(BookingHeader)Session["BookingHeader"]
            get { return Util.GetSession(Cnum.CnumSession.BookingHeader) as BookingHeader; }
            set { Util.SetSession(Cnum.CnumSession.BookingHeader, value); }            
        }
        public static Itinerary Itinerary
        {
            //(Itinerary)Session["Itinerary"]
            get { return Util.GetSession(Cnum.CnumSession.Itinerary) as Itinerary; }
            set { Util.SetSession(Cnum.CnumSession.Itinerary, value); }
        }
        public static Passenger Passenger
        {
            //(Passenger)Session["Passenger"]
            get { return Util.GetSession(Cnum.CnumSession.Passenger) as Passenger; }
            set { Util.SetSession(Cnum.CnumSession.Passenger, value); }
        }
        public static Passengers Passengers
        {
            //(Passengers)Session["Passengers"]
            get { return Util.GetSession(Cnum.CnumSession.Passengers) as Passengers; }
            set { Util.SetSession(Cnum.CnumSession.Passengers, value); }
        }
        public static Quotes Quotes
        {
            //(Quotes)Session["Quotes"]
            get { return Util.GetSession(Cnum.CnumSession.Quotes) as Quotes; }
            set { Util.SetSession(Cnum.CnumSession.Quotes, value); }
        }
        public static Fees Fees
        {
            //(Fees)Session["Fees"]
            get { return Util.GetSession(Cnum.CnumSession.Fees) as Fees; }
            set { Util.SetSession(Cnum.CnumSession.Fees, value); }
        }
        public static Mappings Mappings
        {
            //(Mappings)Session["Mappings"]
            get { return Util.GetSession(Cnum.CnumSession.Mappings) as Mappings; }
            set { Util.SetSession(Cnum.CnumSession.Mappings, value); }
        }
        public static Services Services
        {
            //(Services)Session["Services"]
            get { return Util.GetSession(Cnum.CnumSession.Services) as Services; }
            set { Util.SetSession(Cnum.CnumSession.Services, value); }
        }
        public static Remarks Remarks
        {
            //(Remarks)Session["Remarks"]
            get { return Util.GetSession(Cnum.CnumSession.Remarks) as Remarks; }
            set { Util.SetSession(Cnum.CnumSession.Remarks, value); }
        }
        public static Payments Payments
        {
            //(Payments)Session["Payments"];
            get { return Util.GetSession(Cnum.CnumSession.Payments) as Payments; }
            set { Util.SetSession(Cnum.CnumSession.Payments, value); }
        }
        public static Taxes Taxes
        {
            //(Taxes)Session["Taxes"]
            get { return Util.GetSession(Cnum.CnumSession.Taxes) as Taxes; }
            set { Util.SetSession(Cnum.CnumSession.Taxes, value); }
        }
        public static string responsePrice
        {
            //Session["responsePrice"]
            get { return Util.GetSessionToString(Cnum.CnumSession.ResponsePrice) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.ResponsePrice, value); }
        }
        public static Fee InsuaranceFee
        {
            //Session["responsePrice"] *****
            get { return Util.GetSession(Cnum.CnumSession.InsuaranceFee) as Fee; }
            set { Util.SetSession(Cnum.CnumSession.InsuaranceFee, value); }
        }
        public static Fees BaggageFeeDepart
        {
            //(Fees)Session["BaggageFeeDepart"]
            get { return Util.GetSession(Cnum.CnumSession.BaggageFeeDepart) as Fees; }
            set { Util.SetSession(Cnum.CnumSession.BaggageFeeDepart, value); }
        }
        public static Fees BaggageFeeReturn
        {
            //(Fees)Session["BaggageFeeReturn"]
            get { return Util.GetSession(Cnum.CnumSession.BaggageFeeReturn) as Fees; }
            set { Util.SetSession(Cnum.CnumSession.BaggageFeeReturn, value); }
        }
        public static Fees BaggageFeeTemp
        {
            //(Fees)Session["BaggageFeeTemp"]
            get { return Util.GetSession(Cnum.CnumSession.BaggageFeeTemp) as Fees; }
            set { Util.SetSession(Cnum.CnumSession.BaggageFeeTemp, value); }
        }
        public static bool Https
        {
            //Session["bHttps"] = true
            get { return Util.GetSessionToBoolean(Cnum.CnumSession.Https).GetValueOrDefault(boolDefaultValue); }
            set { Util.SetSession(Cnum.CnumSession.Https, value); }
        }
        public static string CurrentClientDate
        {
            //Session["strReturnDate"]
            get { return Util.GetSessionToString(Cnum.CnumSession.CurrentClientDate) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.CurrentClientDate, value); }
        }
        public static Client Client
        {
            //(Client)Session["Client"]
            get { return Util.GetSession(Cnum.CnumSession.Client) as Client; }
            set { Util.SetSession(Cnum.CnumSession.Client, value); }
        }
        public static string CaptchaImageText
        {
            //Session["CaptchaImageText"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.CaptchaImageText) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.CaptchaImageText, value); }
        }
        public static Passengers ACEPassenger
        {
            //(Passengers)Session["ACEPassenger"] 
            get { return Util.GetSession(Cnum.CnumSession.ACEPassenger) as Passengers; }
            set { Util.SetSession(Cnum.CnumSession.ACEPassenger, value); }
        }
        public static ACEQuotePassengers ACEQuotePassengers
        {
            get { return Util.GetSession(Cnum.CnumSession.ACEQuotePassengers) as ACEQuotePassengers; }
            set { Util.SetSession(Cnum.CnumSession.ACEQuotePassengers, value); }
        }
        public static string BookingSummary
        {
            get { return Util.GetSession(Cnum.CnumSession.BookingSummary) as string; }
            set { Util.SetSession(Cnum.CnumSession.BookingSummary, value); }
        }
        public static bool Login
        {
            //Session["Login"] = true
            get { return Util.GetSessionToBoolean(Cnum.CnumSession.Login).GetValueOrDefault(boolDefaultValue); }
            set { Util.SetSession(Cnum.CnumSession.Login, value); }
        }
        public static string SenderEmailFrom
        {
            //Session["SenderEmailFrom"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.SenderEmailFrom) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.SenderEmailFrom, value); }
        }
        public static string SenderEmailTo
        {
            //Session["SenderEmailTo"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.SenderEmailTo) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.SenderEmailTo, value); }
        }
        public static Int16 NumberOfBccGroup
        {
            //Session["NumberOfBccGroup"].ToString()
            get { return Util.GetSessionToInt16(Cnum.CnumSession.NumberOfBccGroup).GetValueOrDefault(Int16DefaultValue); }
            set { Util.SetSession(Cnum.CnumSession.NumberOfBccGroup, value); }
        }
        public static System.Data.DataTable SearchRegister
        {
            //(DataTable)Session["SearchRegister"]
            get { return Util.GetSession(Cnum.CnumSession.SearchRegister) as System.Data.DataTable; }
            set { Util.SetSession(Cnum.CnumSession.SearchRegister, value); }
        }
        public static string InsuaranceNumber
        {
            //Session["InsuaranceNumber"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.InsuaranceNumber) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.InsuaranceNumber, value); }
        }
        public static string TatraEncrypt
        {
            //Session["TatraEncrypt"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.TatraEncrypt) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.TatraEncrypt, value); }
        }
        public static Vouchers Vouchers
        {
            //(Vouchers)Session["Vouchers"]
            get { return Util.GetSession(Cnum.CnumSession.Vouchers) as Vouchers; }
            set { Util.SetSession(Cnum.CnumSession.Vouchers, value); }
        }
        public static string xmlHistory
        {
            //Session["xmlHistory"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.xmlHistory) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.xmlHistory, value); }
        }
        public static string xmlLife
        {
            //Session["xmlLife"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.xmlLife) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.xmlLife, value); }
        }
        public static string xmlFFP
        {
            //Session["xmlFFP"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.xmlFFP) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.xmlFFP, value); }
        }
        public static string CurrentLanguage
        {
            //Session["CurrentLanguage"].ToString()
            get { return Util.GetSessionToString(Cnum.CnumSession.CurrentLanguage) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.CurrentLanguage, value); }
        }
        public static Agents Agents
        {
            get { return Util.GetSession(Cnum.CnumSession.Agents) as Agents; }
            set { Util.SetSession(Cnum.CnumSession.Agents, value); }
        }
        public static bool SessionCreate
        {
            get { return Util.GetSessionToBoolean(Cnum.CnumSession.SessionCreate).GetValueOrDefault(boolDefaultValue); }
            set { Util.SetSession(Cnum.CnumSession.SessionCreate, value); }
        }
        public static string SeatMapParameter
        {
            get { return Util.GetSessionToString(Cnum.CnumSession.SeatMapParameter) ?? stringDefaultValue; }
            set { Util.SetSession(Cnum.CnumSession.SeatMapParameter, value); }
        }
        #endregion Property

        #region Method
        public static void Remove(string key)
        {
            Util.RemoveSession(key);
        }
        public static void RemoveBookingSession()
        {
            Remove("Itinerary");
            Remove("Passengers");
            Remove("Quotes");
            Remove("Fees");
            Remove("Mappings");
            Remove("Services");
            Remove("Remarks");
            Remove("Payments");
            Remove("Taxes");
            Remove("BaggageFeeDepart");
            Remove("BaggageFeeReturn");
            Remove("BaggageFeeTemp");
            Remove("ACEQuotePassengers");
            Remove("InsuaranceFee");
            Variable.booking_payment_id = Guid.Empty;
        }
        #endregion
    }
    //
    //
    //
    public class EXCB2CSession : B2CSession
    {
        
    }
}