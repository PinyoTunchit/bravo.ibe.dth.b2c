﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Xml.XPath;
using System.IO;

namespace tikAeroB2C
{
    public partial class Util
    {
        //Page.Request.QueryString
        //
        //
        //
        public static string GetCookieValue(Page page, string key)
        {
            //HttpCookie myCookie = new HttpCookie("logAgency");
            //myCookie = Request.Cookies["logAgency"];
            var cookie = page.Request.Cookies[key];

            return cookie == null ? "" : cookie.Value;
        }
        //
        //
        //
        public static string GetQueryString(Page page, string key)
        {
            return page.Request.QueryString[key];
        }
        public static string[] GetQueryStringLike(Page page, string key)
        {
            List<string> list = new List<string>();
            string[] keys = page.Request.QueryString.AllKeys;
            foreach (string item in keys)
            {
                if (item.Contains(key))
                {
                    list.Add(page.Request.QueryString[item]);
                }
            }
            return list.ToArray();

        }
        public static string GetAppSetting(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key] == null ? "" : System.Configuration.ConfigurationManager.AppSettings[key];
        }
        public static object GetSession(Page page, string key)
        {
            try
            {
                return page.Session[key] == null ? Cnum.CnumDefault.Null : page.Session[key];
            }
            catch { return null; }
        }
        public static object GetSession(string key)
        {
            try
            {
                return HttpContext.Current.Session[key] == null ? Cnum.CnumDefault.Null : HttpContext.Current.Session[key];
            }
            catch { return null; }
        }
        public static object GetSessionPage(Page page, string key)
        {
            return HttpContext.Current.Session[page.GetType().Name + key] == null ? Cnum.CnumDefault.Null : HttpContext.Current.Session[page.GetType().Name + key];
        }
        public static string GetSessionToString(Page page, string key)
        {
            var ret = GetSession(page, key);
            return ret == null ? Cnum.CnumDefault.NullString : ret.ToString();
        }
        public static string GetSessionToString(string key)
        {
            return GetSession(key) as string;
        }
        public static bool? GetSessionToBoolean(string key)
        {
            return GetSession(key) as bool?;
        }
        public static int? GetSessionToInt(string key)
        {
            return GetSession(key) as int?;
        }
        public static Int16? GetSessionToInt16(string key)
        {
            return GetSession(key) as Int16?;
        }
        public static byte? GetSessionToByte(string key)
        {
            return GetSession(key) as byte?;
        }
        public static void SetSession(Page page, string key, object value)
        {
            page.Session[key] = value;
        }
        public static void SetSession(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }
        public static void SetSessionPage(Page page, string key, object value)
        {
            page.Session[page.GetType().Name + key] = value;

        }
        public static void RemoveSession(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }

        public static string GetFirstNotNullOrEmpty(string data1, string data2, string strdefault)
        {
            if (string.IsNullOrEmpty(data1))
                return data1;
            if (string.IsNullOrEmpty(data2))
                return data2;
            return strdefault;

        }
        public static string GetFirstNotNullOrEmpty(string data1, string data2, string data3, string strdefault)
        {
            return data1 ?? data2 ?? data3 ?? strdefault;
        }

        public static bool IsValidAgencyCode(string agencycode)
        {

            if (agencycode == null)
                return false;

            return agencycode == "" ? false : true;
        }
        public static int GetIndexBodyTag(System.Web.UI.Page page)
        {

            return page.Controls.IndexOf(GetBodyTag(page));

        }
        public static System.Web.UI.HtmlControls.HtmlGenericControl GetBodyTag(System.Web.UI.Page page)
        {
            foreach (var item in page.Controls)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl body = item as System.Web.UI.HtmlControls.HtmlGenericControl;
                if (body == null)
                    break;
                if (body.TagName == "body")
                    return body;
            }
            return null;
        }

        public static bool IsNullSession(string key)
        {
            if (GetSession(key) == null)
            {
                return true;
            }
            return false;
        }

        public static void AddScriptHeaderBlock(System.Web.UI.Page page, string key, string script)
        {

            System.Web.UI.HtmlControls.HtmlGenericControl js = Creator.GetJavaScriptBlockControl();
            js.InnerHtml = script;
            page.Header.Controls.Add(js);
        }
        public static void AddScriptHeaderInclude(System.Web.UI.Page page, string key, string url)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl js = Creator.GetJavaScriptIncludeControl();
            js.Attributes["src"] = url;
            page.Header.Controls.Add(js);
        }
        public static void AddScriptBodyTopBlock(System.Web.UI.Page page, string key, string script)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl body = GetBodyTag(page);
            if (body == null)
                throw new Exception(@"plese add attribute runat=""server"" on body tag");

            System.Web.UI.HtmlControls.HtmlGenericControl js = Creator.GetJavaScriptBlockControl();
            js.InnerHtml = script;
            body.Controls.AddAt(0, js);
        }
        public static void AddScriptBodyTopInclude(System.Web.UI.Page page, string key, string url)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl body = GetBodyTag(page);
            if (body == null)
                throw new Exception(@"plese add attribute runat=""server"" on body tag");

            System.Web.UI.HtmlControls.HtmlGenericControl js = Creator.GetJavaScriptIncludeControl();
            js.Attributes["src"] = url;
            body.Controls.AddAt(0, js);
        }
        public static void AddScriptBodyBottomBlock(System.Web.UI.Page page, string key, string script)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl body = GetBodyTag(page);
            if (body == null)
                throw new Exception(@"plese add attribute runat=""server"" on body tag");

            System.Web.UI.HtmlControls.HtmlGenericControl js = Creator.GetJavaScriptBlockControl();
            js.InnerHtml = script;
            body.Controls.Add(js);
        }
        public static void AddScriptBodyBottomInclude(System.Web.UI.Page page, string key, string url)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl body = GetBodyTag(page);
            if (body == null)
                throw new Exception(@"plese add attribute runat=""server"" on body tag");

            System.Web.UI.HtmlControls.HtmlGenericControl js = Creator.GetJavaScriptIncludeControl();
            js.Attributes["src"] = url;
            body.Controls.Add(js);
        }

        public static List<string> GetFareAllID(string flightXML)
        {
            List<string> fareFlight = new List<string>();
            XPathDocument xmlDoc = new XPathDocument(new StringReader(flightXML));
            XPathNavigator nv = xmlDoc.CreateNavigator();
            foreach (XPathNavigator n in nv.Select("//flight/fare/group"))
            {
                string fare_id = n.SelectSingleNode("fare_id").Value;
                if (!string.IsNullOrEmpty(fare_id))
                {
                    string flight_id = n.SelectSingleNode("flight_id").Value;
                    string transit_flight_id = n.SelectSingleNode("transit_flight_id") != null ? n.SelectSingleNode("transit_flight_id").Value : string.Empty;
                    string currency_rcd = nv.SelectSingleNode("//FlightGroup/currency_rcd").Value;
                    fareFlight.Add(flight_id + ":" + fare_id + ":" + currency_rcd);
                    if(!string.IsNullOrEmpty(transit_flight_id))
                        fareFlight.Add(transit_flight_id + ":" + fare_id + ":" + currency_rcd);
                }
            }

            return fareFlight;
        }

    }
}