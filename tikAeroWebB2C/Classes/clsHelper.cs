using Bravo.Logging.Client;
using Bravo.Logging.Client.API;
using Bravo.Logging.Client.Messaging;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Xsl;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;

namespace tikAeroB2C.Classes
{
    public class Helper
    {
        public void SendMail(Guid bookingId, string mailTo, XslTransform objTransform, string strDocumentType, string strUserId, bool bRemoveQueue, bool bWithPdf, string strLanguage, StringDictionary std)
        {
            try
            {
                ServiceClient objClient = new ServiceClient();

                string tempResult = string.Empty;

                objClient.objService = B2CSession.AgentService;
                tempResult = objClient.GetItinerary(bookingId.ToString(), strLanguage, string.Empty, string.Empty);
                if (tempResult.Length > 0)
                {
                    Library ObjLi = new Library();

                    tempResult = ObjLi.RenderHtml(objTransform, null, tempResult);
                    ObjLi = null;

                    objClient.QueueMail(B2CSetting.EmailFrom,
                                        B2CSetting.EmailFrom,
                                        mailTo,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        Classes.Language.Value("Booking_Step_5_65", B2CSetting.EmailSubject, std),
                                        tempResult,
                                        strDocumentType,
                                        (bWithPdf == true) ? tempResult : string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        true,
                                        bWithPdf,
                                        false,
                                        strUserId,
                                        bookingId.ToString(),
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty);
                }
                objClient.objService = null;
                objClient = null;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        public void SendMail(BookingHeader bookingHeader,
                            string mailTo,
                            string mailSubject,
                            string strDocumentType,
                            string strUserId,
                            bool bRemoveQueue,
                            bool bWithPdf,
                            string strLanguage)
        {
            try
            {
                ServiceClient objClient = new ServiceClient();

                string tempResult = string.Empty;
                string tempResultAttach = string.Empty;
                string strXml = string.Empty;

                XslTransform objTransform = null;
                XslTransform objTransformAttach = null;

                objClient.objService = B2CSession.AgentService;
                strXml = objClient.GetItinerary(bookingHeader.booking_id.ToString(), strLanguage, string.Empty, string.Empty);

                if (string.IsNullOrEmpty(strXml) == false)
                {
                    Library ObjLi = new Library();

                    if (!System.IO.File.Exists(HttpContext.Current.Server.MapPath("~") + @"\Xsl\mail\" + strLanguage.ToLower() + "_email_body.xsl"))
                        objTransform = GetXSLMail(strLanguage.ToLower(), "email_b2c");
                    else
                        objTransform = GetXSLMail(strLanguage.ToLower(), "email_body");

                    objTransformAttach = GetXSLMail(strLanguage.ToLower(), "email_b2c");
                    tempResult = ObjLi.RenderHtml(objTransform, null, strXml);
                    tempResultAttach = ObjLi.RenderHtml(objTransformAttach, null, strXml);

                    if (mailTo.Length > 0)
                    {
                        objClient.QueueMail(B2CSetting.EmailFrom,
                                            B2CSetting.EmailFrom,
                                            mailTo,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            mailSubject,
                                            tempResult,
                                            strDocumentType,
                                            (bWithPdf == true) ? tempResultAttach : string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            true,
                                            bWithPdf,
                                            false,
                                            strUserId,
                                            bookingHeader.booking_id.ToString(),
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty);
                    }
                    else
                    {

                        objClient.QueueMail(B2CSetting.EmailFrom,
                                            B2CSetting.EmailFrom,
                                            bookingHeader.contact_email,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            mailSubject,
                                            tempResult,
                                            strDocumentType,
                                            (bWithPdf == true) ? tempResultAttach : string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            true,
                                            bWithPdf,
                                            false,
                                            strUserId,
                                            bookingHeader.booking_id.ToString(),
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty);

                        if (bookingHeader.mobile_email.Length > 0)
                        {
                            objTransform = GetXSLMail(bookingHeader.language_rcd.ToLower(), "email_b2m");
                            tempResult = ObjLi.RenderHtml(objTransform, null, strXml);
                            //For sending itinerary to mobile.
                            if (string.IsNullOrEmpty(tempResult) == false)
                            {
                                objClient.QueueMail(B2CSetting.EmailFrom,
                                                    B2CSetting.EmailFrom,
                                                    bookingHeader.mobile_email,
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty,
                                                    mailSubject,
                                                    tempResult,
                                                    strDocumentType,
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty,
                                                    false,
                                                    false,
                                                    false,
                                                    strUserId,
                                                    bookingHeader.booking_id.ToString(),
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty);
                            }
                        }
                    }
                }
                objClient.objService = null;
                objClient = null;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public void SendMail(string mailFrom, string mailTo, string mailSubject, string mailBody, string strDocumentType, string strUserId, bool bRemoveQueue, bool bWithPdf)
        {
            try
            {
                ServiceClient objClient = new ServiceClient();

                objClient.objService = B2CSession.AgentService;

                if (mailBody.Length > 0 && mailFrom.Length > 0 && mailTo.Length > 0 && mailSubject.Length > 0)
                {
                    objClient.QueueMail(mailFrom,
                                        mailFrom,
                                        mailTo,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        mailSubject,
                                        mailBody,
                                        strDocumentType,
                                        (bWithPdf == true) ? mailBody : string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        true,
                                        bWithPdf,
                                        false,
                                        strUserId,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty);
                }
                objClient.objService = null;
                objClient = null;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        public bool SendMailSmtp(string mailForm, string mailTo, string mailBcc, string mailSubject, string mailBody, bool isHtml)
        {
            bool bResult = false;
            if (!string.IsNullOrEmpty(B2CSetting.SmtpServer))
            {
                MailMessage message = null;
                try
                {
                    // Command line argument must the the SMTP host.
                    SmtpClient client = new SmtpClient(B2CSetting.SmtpServer);
                    MailAddress from = new MailAddress(mailForm,
                                                       mailForm,
                                                       System.Text.Encoding.UTF8);

                    MailAddress to = new MailAddress(mailTo);
                    message = new MailMessage(from, to);

                    if (mailBcc.Length > 0)
                    { message.Bcc.Add(mailBcc); }

                    message.IsBodyHtml = isHtml;
                    message.Body = mailBody;

                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.Subject = mailSubject;
                    message.SubjectEncoding = System.Text.Encoding.UTF8;

                    client.Send(message);
                    bResult = true;
                }
                catch
                {
                    bResult = false;
                }
                finally
                {
                    if (message != null)
                    {
                        // Clean up.
                        message.Dispose();
                    }
                }
            }
            else
            { bResult = false; }

            return bResult;
        }
        public string ConvertDataTableToXML(DataTable dtBuildSQL)
        {
            try
            {
                DataSet dsBuildSQL = new DataSet();
                if (dtBuildSQL != null)
                {
                    dsBuildSQL.Merge(dtBuildSQL, true, MissingSchemaAction.AddWithKey);
                    return dsBuildSQL.GetXml();
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        public object ConvertDataTableObject(DataTable dtBuildSQL, Type t, bool isList)
        {
            try
            {
                DataSet dsBuildSQL = new DataSet();
                object obj = new object();

                if (dtBuildSQL != null)
                {
                    dsBuildSQL.Merge(dtBuildSQL, true, MissingSchemaAction.AddWithKey);
                    if (!isList)
                        obj = XmlHelper.Deserialize(dsBuildSQL.GetXml().Replace("<NewDataSet>", "").Replace("</NewDataSet>", ""), t);
                    else
                        obj = XmlHelper.Deserialize(dsBuildSQL.GetXml().Replace("<NewDataSet>", "<ArrayOf" + t.Name.Substring(0, t.Name.Length - 1) + ">")
                            .Replace("</NewDataSet>", "</ArrayOf" + t.Name.Substring(0, t.Name.Length - 1) + ">"), t);
                }
                else
                {
                    if (!isList)
                        obj = XmlHelper.Deserialize("<" + t.Name + "></" + t.Name + ">", t);
                    else
                        obj = XmlHelper.Deserialize("<ArrayOf" + t.Name.Substring(0, t.Name.Length - 1) + "></ArrayOf" + t.Name.Substring(0, t.Name.Length - 1) + ">", t);
                }

                return obj;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public XslTransform GetXSLDocument(string documentName)
        {
            try
            {
                XslTransform objTransform = (XslTransform)HttpRuntime.Cache[documentName];
                if (objTransform != null)
                {
                    return objTransform;
                }
                else
                {
                    string serverPath = HttpContext.Current.Server.MapPath("~") + @"\xsl\" + documentName + ".xsl";
                    objTransform = new XslTransform();
                    objTransform.Load(serverPath);
                    HttpRuntime.Cache.Insert(documentName, objTransform, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    return objTransform;
                }

            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, documentName);
                throw ex;
            }
        }
        public XsltArgumentList AddLanguageXsltArgumentList(string argumentName, StringDictionary dicLanguage)
        {
            try
            {
                LangaugeHelper lhp = new LangaugeHelper(dicLanguage);
                XsltArgumentList objArgument = new XsltArgumentList();
                if (lhp != null)
                {
                    objArgument.AddExtensionObject(argumentName, lhp);
                    return objArgument;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, "");
                throw ex;
            }
        }
        public XslTransform GetXSLMail(string strLanguage, string documentName)
        {
            try
            {
                XslTransform objTransform = (XslTransform)HttpRuntime.Cache[strLanguage + "_" + documentName];
                if (objTransform != null)
                {
                    return objTransform;
                }
                else
                {
                    string serverPath = HttpContext.Current.Server.MapPath("~") + @"\xsl\mail\" + strLanguage + "_" + documentName + ".xsl";
                    if (File.Exists(serverPath) == false)
                    {
                        serverPath = HttpContext.Current.Server.MapPath("~") + @"\xsl\mail\en_" + documentName + ".xsl";
                    }

                    if (File.Exists(serverPath))
                    {
                        objTransform = new XslTransform();
                        objTransform.Load(serverPath);
                        HttpRuntime.Cache.Insert(strLanguage + "_" + documentName, objTransform, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    }
                    return objTransform;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strLanguage + "_" + documentName);
                throw ex;
            }
        }
        public void FillClientToHeader(BookingHeader bookingHeader, Client client)
        {
            try
            {
                if (client != null && client.client_number > 0 && bookingHeader.client_number == 0)
                {
                    bookingHeader.client_number = client.client_number;
                    bookingHeader.client_profile_id = client.client_profile_id;
                    bookingHeader.title_rcd = client.title_rcd;
                    bookingHeader.firstname = client.firstname;
                    bookingHeader.lastname = client.lastname;
                    bookingHeader.phone_mobile = client.phone_mobile;
                    bookingHeader.phone_home = client.phone_home;
                    bookingHeader.phone_business = client.phone_business;
                    bookingHeader.contact_email = client.contact_email;
                    bookingHeader.mobile_email = client.mobile_email;
                    bookingHeader.address_line1 = client.address_line1;
                    bookingHeader.address_line2 = client.address_line2;
                    bookingHeader.zip_code = client.zip_code;
                    bookingHeader.city = client.city;
                    bookingHeader.state = client.state;
                    bookingHeader.country_rcd = client.country_rcd;
                    bookingHeader.language_rcd = client.language_rcd;
                    bookingHeader.contact_name = client.firstname + " " + client.lastname;

                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, client.client_number +
                                         "," +
                                         client.client_profile_id.ToString() +
                                         "," +
                                         client.title_rcd +
                                         "," +
                                         client.firstname +
                                         "," +
                                         client.lastname +
                                         "," +
                                         client.phone_mobile +
                                         "," +
                                         client.phone_home +
                                         "," +
                                         client.phone_business +
                                         "," +
                                         client.contact_email +
                                         "," +
                                         client.address_line1 +
                                         "," +
                                         client.address_line2 +
                                         "," +
                                         client.zip_code +
                                         "," +
                                         client.city +
                                         "," +
                                         client.state +
                                         "," +
                                         client.country_rcd +
                                         "," +
                                         client.language_rcd +
                                         ",");
                throw ex;
            }
        }

        // Start Login Cross App Cookie
        public HttpCookie CreateCookie(string strCookieName, string strValue, int iAddedCookieTime, CookiePeriod period)
        {
            try
            {
                // Create Cookie
                HttpCookie aCookie = new HttpCookie(strCookieName);

                // Encode String info and set to cookie
                string strBeforeEncodeData = B2CSetting.PrefixEncrypt + strValue + B2CSetting.AffixEncrypt;

                aCookie.Values["info"] = EncodeDataBase64(strBeforeEncodeData);

                // Set Cookie Time
                switch (period)
                {
                    case CookiePeriod.Day:
                        aCookie.Expires = DateTime.Now.AddDays(iAddedCookieTime);
                        break;
                    case CookiePeriod.Month:
                        aCookie.Expires = DateTime.Now.AddMonths(iAddedCookieTime);
                        break;
                    case CookiePeriod.Year:
                        aCookie.Expires = DateTime.Now.AddYears(iAddedCookieTime);
                        break;
                    case CookiePeriod.Hour:
                        aCookie.Expires = DateTime.Now.AddHours(iAddedCookieTime);
                        break;
                    case CookiePeriod.Minute:
                        aCookie.Expires = DateTime.Now.AddMinutes(iAddedCookieTime);
                        break;
                    case CookiePeriod.Second:
                        aCookie.Expires = DateTime.Now.AddSeconds(iAddedCookieTime);
                        break;
                    case CookiePeriod.Millisecond:
                        aCookie.Expires = DateTime.Now.AddMilliseconds(iAddedCookieTime);
                        break;
                    default:
                        aCookie.Expires = DateTime.Now.AddDays(iAddedCookieTime);
                        break;
                }
                return aCookie;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        public string EncodeDataBase64(string strValue)
        {
            try
            {
                byte[] byteData = Encoding.ASCII.GetBytes(strValue.Trim().ToCharArray());
                return Convert.ToBase64String(byteData);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strValue);
                throw ex;
            }
        }
        public string DecodeDataBase64(string strValue)
        {
            try
            {
                byte[] byteData = System.Convert.FromBase64String(strValue);
                return System.Text.Encoding.ASCII.GetString(byteData);
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, strValue);
                throw ex;
            }
        }
        //Login Cross App Cookie
        public string[] GetLanguage2Calendar(System.Collections.Specialized.StringDictionary sdLanguage)
        {
            string[] returnValue = new string[2];

            try
            {
                if (sdLanguage != null)
                {

                    System.Globalization.CultureInfo GlobalDt = new System.Globalization.CultureInfo(Language.CurrentFullCode());
                    System.Globalization.DateTimeFormatInfo dt = GlobalDt.DateTimeFormat;

                    string DayString = String.Join(",", dt.AbbreviatedDayNames);
                    string MonthString = String.Join(",", dt.MonthNames);

                    returnValue[0] = DayString;
                    returnValue[1] = MonthString;

                    return returnValue;

                }

                return null;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }


        public string GetLanguagePageScript(System.Collections.Specialized.StringDictionary sdLanguage)
        {
            try
            {
                if (sdLanguage != null)
                {
                    StringBuilder stb = new StringBuilder();
                    string strKeyValue = string.Empty;
                    foreach (DictionaryEntry et in sdLanguage)
                    {
                        if (et.Key.ToString().Contains("alert_message") == true)
                        {
                            strKeyValue = et.Key.ToString().Replace("alert_message", "Alert_Message");
                        }
                        else if (et.Key.ToString().Contains("date_display") == true)
                        {
                            strKeyValue = et.Key.ToString().Replace("date_display", "Date_Display");
                        }
                        else if (et.Key.ToString().Contains("default_value") == true)
                        {
                            strKeyValue = et.Key.ToString();
                        }

                        if (strKeyValue.Length > 0)
                        {
                            stb.Append("\"" + strKeyValue + "\" : \"" + et.Value.ToString().Replace("'", "\\u0027").Replace("\r\n", "").Replace("\n", "<br/>").Replace("\\n", "<br/>") + "\",");
                            strKeyValue = string.Empty;
                        }
                    }

                    System.Globalization.CultureInfo GlobalDt = new System.Globalization.CultureInfo(Language.CurrentFullCode());
                    System.Globalization.DateTimeFormatInfo dt = GlobalDt.DateTimeFormat;
                    //Day Of Week Abbreviated
                    stb.Append("\"Date_Sun\" : \"" + dt.AbbreviatedDayNames[0] + "\",");
                    stb.Append("\"Date_Mon\" : \"" + dt.AbbreviatedDayNames[1] + "\",");
                    stb.Append("\"Date_Tue\" : \"" + dt.AbbreviatedDayNames[2] + "\",");
                    stb.Append("\"Date_Wed\" : \"" + dt.AbbreviatedDayNames[3] + "\",");
                    stb.Append("\"Date_Thu\" : \"" + dt.AbbreviatedDayNames[4] + "\",");
                    stb.Append("\"Date_Fri\" : \"" + dt.AbbreviatedDayNames[5] + "\",");
                    stb.Append("\"Date_Sat\" : \"" + dt.AbbreviatedDayNames[6] + "\",");

                    //Month Abbreviated
                    stb.Append("\"Date_Jan\" : \"" + dt.AbbreviatedMonthNames[0] + "\",");
                    stb.Append("\"Date_Fed\" : \"" + dt.AbbreviatedMonthNames[1] + "\",");
                    stb.Append("\"Date_Mar\" : \"" + dt.AbbreviatedMonthNames[2] + "\",");
                    stb.Append("\"Date_Apr\" : \"" + dt.AbbreviatedMonthNames[3] + "\",");
                    stb.Append("\"Date_May\" : \"" + dt.AbbreviatedMonthNames[4] + "\",");
                    stb.Append("\"Date_Jun\" : \"" + dt.AbbreviatedMonthNames[5] + "\",");
                    stb.Append("\"Date_Jul\" : \"" + dt.AbbreviatedMonthNames[6] + "\",");
                    stb.Append("\"Date_Aug\" : \"" + dt.AbbreviatedMonthNames[7] + "\",");
                    stb.Append("\"Date_Sep\" : \"" + dt.AbbreviatedMonthNames[8] + "\",");
                    stb.Append("\"Date_Oct\" : \"" + dt.AbbreviatedMonthNames[9] + "\",");
                    stb.Append("\"Date_Nov\" : \"" + dt.AbbreviatedMonthNames[10] + "\",");
                    stb.Append("\"Date_Dec\" : \"" + dt.AbbreviatedMonthNames[11] + "\",");

                    //Day Of Week Full
                    stb.Append("\"Date_Sun_Full\" : \"" + dt.DayNames[0] + "\",");
                    stb.Append("\"Date_Mon_Full\" : \"" + dt.DayNames[1] + "\",");
                    stb.Append("\"Date_Tue_Full\" : \"" + dt.DayNames[2] + "\",");
                    stb.Append("\"Date_Wed_Full\" : \"" + dt.DayNames[3] + "\",");
                    stb.Append("\"Date_Thu_Full\" : \"" + dt.DayNames[4] + "\",");
                    stb.Append("\"Date_Fri_Full\" : \"" + dt.DayNames[5] + "\",");
                    stb.Append("\"Date_Sat_Full\" : \"" + dt.DayNames[6] + "\",");


                    //ShortestDayNames
                    stb.Append("\"Date_Sun_short\" : \"" + dt.ShortestDayNames[0] + "\",");
                    stb.Append("\"Date_Mon_short\" : \"" + dt.ShortestDayNames[1] + "\",");
                    stb.Append("\"Date_Tue_short\" : \"" + dt.ShortestDayNames[2] + "\",");
                    stb.Append("\"Date_Wed_short\" : \"" + dt.ShortestDayNames[3] + "\",");
                    stb.Append("\"Date_Thu_short\" : \"" + dt.ShortestDayNames[4] + "\",");
                    stb.Append("\"Date_Fri_short\" : \"" + dt.ShortestDayNames[5] + "\",");
                    stb.Append("\"Date_Sat_short\" : \"" + dt.ShortestDayNames[6] + "\",");

                    //Month Full
                    stb.Append("\"Date_Jan_Full\" : \"" + dt.MonthNames[0] + "\",");
                    stb.Append("\"Date_Fed_Full\" : \"" + dt.MonthNames[1] + "\",");
                    stb.Append("\"Date_Mar_Full\" : \"" + dt.MonthNames[2] + "\",");
                    stb.Append("\"Date_Apr_Full\" : \"" + dt.MonthNames[3] + "\",");
                    stb.Append("\"Date_May_Full\" : \"" + dt.MonthNames[4] + "\",");
                    stb.Append("\"Date_Jun_Full\" : \"" + dt.MonthNames[5] + "\",");
                    stb.Append("\"Date_Jul_Full\" : \"" + dt.MonthNames[6] + "\",");
                    stb.Append("\"Date_Aug_Full\" : \"" + dt.MonthNames[7] + "\",");
                    stb.Append("\"Date_Sep_Full\" : \"" + dt.MonthNames[8] + "\",");
                    stb.Append("\"Date_Oct_Full\" : \"" + dt.MonthNames[9] + "\",");
                    stb.Append("\"Date_Nov_Full\" : \"" + dt.MonthNames[10] + "\",");
                    stb.Append("\"Date_Dec_Full\" : \"" + dt.MonthNames[11] + "\",");

                    stb.Append("\"LangCode\" : \"" + Classes.Language.CurrentCode() + "\"");

                    return stb.ToString();
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public void SendErrorEmail(Exception ex, string strInput)
        {
            ErrorLog(ex);
            ErrorEmail(strInput, ex.Message, ex.StackTrace, ex.TargetSite.DeclaringType.FullName, ex.TargetSite.Name);
        }
        public void SendErrorEmail(string strInput, string strMessage, string strTrace, string strLocation, string strFunctionName)
        {
            var strErr = strInput + " | " + strMessage + " | " + strTrace + " | " + strLocation + " | " + strFunctionName;
            var ex = new Exception(strErr);
            ErrorLog(ex);
            ErrorEmail(strInput, strMessage, strTrace, strLocation, strFunctionName);
        }
        public bool SessionTimeout()
        {
            try
            {
                if (HttpContext.Current.Session != null)
                {
                    if (HttpContext.Current.Session.IsNewSession)
                    {
                        string strCookiesHeader = HttpContext.Current.Request.Headers["Cookie"];
                        if (strCookiesHeader != null)
                        {
                            if (strCookiesHeader.ToUpper().IndexOf("ASP.NET_SESSIONID") >= 0)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public void ApplicationInitialize(string strAgencyCode, Guid gUserId)
        {
            try
            {
                //Initialize webservice
                Agents objAgents = B2CSession.Agents;
                if (B2CSetting.Service == 1)
                {
                    if (B2CSession.AgentService == null ||
                        objAgents == null ||
                        objAgents.Count == 0 ||
                        (objAgents != null && objAgents.Count > 0 && objAgents[0].agency_code.Equals(strAgencyCode) == false))
                    {
                        //Initialize Agentservice
                        if (objAgents == null)
                        {
                            objAgents = new Agents();
                        }
                        ServiceClient objService = new ServiceClient();
                        if (string.IsNullOrEmpty(strAgencyCode))
                        {
                            objAgents.Clear();
                            objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgents);
                        }
                        else
                        {
                            objAgents.Clear();
                            objService.initializeWebService(strAgencyCode, ref objAgents);
                            if (objAgents == null || objAgents.Count == 0)
                            {
                                objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgents);
                            }
                        }

                        if (objAgents.Count > 0 && objAgents[0].default_user_account_id != Guid.Empty)
                        {
                            gUserId = objAgents[0].default_user_account_id;
                        }
                        else
                        {
                            gUserId = B2CSetting.UserId;
                        }

                        //Add Agency Information to session.
                        B2CSession.Agents = objAgents;
                        B2CSession.AgentService = (TikAeroXMLwebservice)objService.objService;
                        objService = null;
                    }
                }
                else
                {
                    //Get B2c agency information
                    if (objAgents == null ||
                        objAgents.Count == 0 ||
                        (objAgents != null && objAgents.Count > 0 && objAgents[0].agency_code.Equals(strAgencyCode) == false))
                    {
                        if (objAgents == null)
                        {
                            ServiceClient objService = new ServiceClient();
                            objAgents = objService.GetAgencySessionProfile(B2CSetting.DefaultAgencyCode, string.Empty);
                            objService = null;
                        }
                        if (objAgents != null && objAgents.Count > 0 && objAgents[0].default_user_account_id != Guid.Empty)
                        {
                            gUserId = objAgents[0].default_user_account_id;
                        }
                        else
                        {
                            gUserId = B2CSetting.UserId;
                        }
                        //Add Agency Information to session.
                        B2CSession.Agents = objAgents;
                    }
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public void LanguageInitialize()
        {
            try
            {
                //Load Multi-Language
                if (B2CSession.CurrentLanguage == null ||
                    B2CSession.LanCode == null ||
                    B2CSession.CurrentLanguage != Language.CurrentFullCode())
                {
                    B2CSession.CurrentLanguage = Language.CurrentFullCode();
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail(ex, string.Empty);
                //throw ex;
            }
        }
        private void ErrorEmail(string strInput, string strMessage, string strTrace, string strLocation, string strFunctionName)
        {
            try
            {
                if (SessionTimeout() == false)
                {
                    if (!string.IsNullOrEmpty(B2CSetting.ErrorTo))
                    {
                        StringBuilder stbHtml = new StringBuilder();

                        stbHtml.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
                        stbHtml.Append("<body>");
                        stbHtml.Append("<table width='400' border='0' cellpadding='4' cellspacing='2' bgcolor='#B4CBD6' style='margin:0px auto; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#304974;'>");
                        stbHtml.Append("<tr>");
                        stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Date</td>");
                        stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>" + string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now) + "</td>");
                        stbHtml.Append("</tr>");
                        stbHtml.Append("<tr>");
                        stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>IP Address</td>");
                        stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>" + DataHelper.GetClientIpAddress() + "</td>");
                        stbHtml.Append("</tr>");
                        stbHtml.Append("<tr>");
                        stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Location</td>");
                        stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>" + strLocation + "</td>");
                        stbHtml.Append("</tr>");
                        stbHtml.Append("<tr>");
                        stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Function</td>");
                        stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>" + strFunctionName + "</td>");
                        stbHtml.Append("</tr>");
                        stbHtml.Append("<tr>");
                        stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Input</td>");
                        stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>");
                        stbHtml.Append(strInput);
                        stbHtml.Append("</td>");
                        stbHtml.Append("</tr>");
                        stbHtml.Append("<tr>");
                        stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Message</td>");
                        stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>");
                        stbHtml.Append(strMessage);
                        stbHtml.Append("</td>");
                        stbHtml.Append("</tr>");
                        stbHtml.Append("<tr>");
                        stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Trace</td>");
                        stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>");
                        stbHtml.Append(strTrace);
                        stbHtml.Append("</td>");
                        stbHtml.Append("</tr>");
                        stbHtml.Append("</table>");
                        stbHtml.Append("</body>");
                        stbHtml.Append("</html>");

                        //Send Error Email.
                        SendMailSmtp(System.Environment.MachineName + "@bravo.aero",
                                     B2CSetting.ErrorTo,
                                     string.Empty,
                                     "[" + strFunctionName + "] - " + strLocation,
                                     stbHtml.ToString(),
                                     true);

                    }
                }
            }
            catch { }
        }
        public void SendItineraryByMail(BookingHeader bookingHeader, string strEmail, Guid userId, StringDictionary std)
        {


            try
            {
                bool bWithPdf = true;
                string mailSubject = string.Empty;
                if (ConfigurationManager.AppSettings[bookingHeader.language_rcd.ToLower() + "_EmailSubject"] != null &&
                    string.IsNullOrEmpty(ConfigurationManager.AppSettings[bookingHeader.language_rcd.ToLower() + "_EmailSubject"]) == false)
                {
                    mailSubject = ConfigurationManager.AppSettings[bookingHeader.language_rcd.ToLower() + "_EmailSubject"];
                }
                else
                    mailSubject = Classes.Language.Value(B2CSetting.EmailSubject, B2CSetting.EmailSubject, std);

                bWithPdf = B2CSetting.AttactEmail;

                if (bookingHeader != null)
                {
                    tikSystem.Web.Library.MailManagement objMail = new tikSystem.Web.Library.MailManagement();
                    objMail.objService = B2CSession.AgentService; //Webservice proxy
                    objMail.BookingID = bookingHeader.booking_id.ToString();
                    objMail.MailTo = strEmail;   //it will take from booking header if not specify.
                    objMail.MailForm = B2CSetting.EmailFrom;  // web.config
                    objMail.MailSubject = mailSubject.Replace("{PNR}", bookingHeader.record_locator);  //WEMS
                    objMail.UserAccountID = userId.ToString();
                    objMail.IBEEngine = "B2C";  // web.config Key THREAD_WEMS
                    objMail.LanguageRCD = bookingHeader.language_rcd;  // Language that use get booking
                    objMail.XslEmailPath = HttpContext.Current.Server.MapPath("~") + @"\Xsl\mail\";
                    objMail.IsUsePDF = bWithPdf;
                    objMail.SendItineraryMail(); //  return true,false
                }

            }
            catch (Exception ex)
            {
                Helper objHelper = new Helper();
                string strErrorParam;
                if (bookingHeader != null)
                {
                    strErrorParam = "Email: " + strEmail + "<br/>" +
                                    "Header Email: " + ((string.IsNullOrEmpty(bookingHeader.contact_email)) ? string.Empty : bookingHeader.contact_email) + "<br/>" +
                                    "Booking ID: " + bookingHeader.booking_id.ToString() + "<br/>" +
                                    "User ID: " + userId.ToString() + "<br/>";
                }
                else
                {
                    strErrorParam = "Email: " + strEmail + "<br/>" +
                                    "User ID: " + userId.ToString() + "<br/>";
                }
                objHelper.SendErrorEmail(ex, strErrorParam);
                throw ex;
            }

        }
        public void FindPieceAllowance(ref XmlWriter xtw, Mappings mp, Guid gBookingSegmentId)
        {
            xtw.WriteStartElement("PieceAllowance");
            {
                for (int i = 0; i < mp.Count; i++)
                {
                    if (mp[i].booking_segment_id.Equals(gBookingSegmentId) & mp[i].passenger_type_rcd.Equals("ADULT") & mp[i].piece_allowance > 0)
                    {
                        xtw.WriteStartElement("Value");
                        {
                            xtw.WriteValue(mp[i].piece_allowance);
                        }
                        xtw.WriteEndElement();
                        break;
                    }
                }
            }
            xtw.WriteEndElement();
        }
        public void BuiltBaggage(XmlWriter xtw, string flightID, tikSystem.Web.Library.Passengers passengers)
        {
            Classes.Helper objHp = new Classes.Helper();
            StringBuilder str = new StringBuilder();
            Library li = new Library();

            string tempXML = string.Empty;
            XmlReaderSettings setting = new XmlReaderSettings();
            setting.IgnoreWhitespace = true;

            B2CVariable objUv = B2CSession.Variable;
            Itinerary itinerary = B2CSession.Itinerary;
            Mappings mappings = B2CSession.Mappings;
            Fees fees = B2CSession.Fees;

            tikSystem.Web.Library.Passenger objAdult;
            tikSystem.Web.Library.Passenger objChild;

            if (B2CSetting.UseBaggageCalculation == "1")
            {
                xtw.WriteStartElement("BaggageFees");
                {
                    //Departure Baggage Fees.
                    Guid gSegmentId = Guid.Empty;
                    Guid gPassengerId = Guid.Empty;
                    for (int i = 0; i < itinerary.Count; i++)
                    {
                        if (itinerary[i].flight_id.Equals(new Guid(flightID)))
                        {
                            gSegmentId = itinerary[i].booking_segment_id;
                            break;
                        }
                    }

                    objAdult = passengers.GetAdultPassenger()[0];
                    // Fill Passenger
                    if (objAdult != null)
                        gPassengerId = objAdult.passenger_id;

                    //Get Piece allowance
                    xtw.WriteStartElement("PieceAllowance");
                    {
                        xtw.WriteStartElement("Value");
                        {
                            xtw.WriteValue(mappings.FindPieceAllowance(gSegmentId, gPassengerId));
                        }
                        xtw.WriteEndElement();
                        xtw.WriteStartElement("passenger_type_rcd");
                        {
                            xtw.WriteValue(objAdult.passenger_type_rcd);
                        }
                        xtw.WriteEndElement();
                    }
                    xtw.WriteEndElement();
                    //Get Piece allowance for Child
                    if (passengers.GetChildPassenger().Count > 0)
                    {
                        objChild = passengers.GetChildPassenger()[0];
                        xtw.WriteStartElement("PieceAllowance");
                        {
                            xtw.WriteStartElement("Value");
                            {
                                xtw.WriteValue(mappings.FindPieceAllowance(gSegmentId, objChild.passenger_id));
                            }
                            xtw.WriteEndElement();
                            xtw.WriteStartElement("passenger_type_rcd");
                            {
                                xtw.WriteValue(objChild.passenger_type_rcd);
                            }
                            xtw.WriteEndElement();
                        }
                        xtw.WriteEndElement();
                    }

                    //objHp.FindPieceAllowance(ref xtw, mappings, gSegmentId);

                    if (gSegmentId.Equals(Guid.Empty) == false)
                    {
                        Fees objOutWardFee = new Fees();

                        objOutWardFee.objService = B2CSession.AgentService;
                        objOutWardFee.GetBaggageFeeOptions(mappings,
                                                            gSegmentId,
                                                            gPassengerId,
                                                            objUv.Agency_Code,
                                                            Classes.Language.CurrentCode().ToUpper(),
                                                            0,
                                                            null,
                                                            false,
                                                            false);
                        objOutWardFee.objService = null;

                        if (objOutWardFee != null && objOutWardFee.Count > 0)
                        {
                            //Find Selected fee.
                            int j;
                            for (j = 0; j < fees.Count; j++)
                            {
                                if (fees[j].booking_segment_id.Equals(gSegmentId) &
                                    fees[j].passenger_id.Equals(gPassengerId) &
                                    (fees[j].fee_category_rcd != null && fees[j].fee_category_rcd.Equals("BAGSALES")))
                                {
                                    break;
                                }
                            }

                            for (int i = 0; i < objOutWardFee.Count; i++)
                            {
                                xtw.WriteStartElement("Fee");
                                {
                                    xtw.WriteStartElement("fee_id");
                                    {
                                        xtw.WriteValue(objOutWardFee[i].fee_id.ToString());
                                    }
                                    xtw.WriteEndElement();
                                    xtw.WriteStartElement("booking_segment_id");
                                    {
                                        xtw.WriteValue(objOutWardFee[i].booking_segment_id.ToString());
                                    }
                                    xtw.WriteEndElement();
                                    xtw.WriteStartElement("fee_rcd");
                                    {
                                        xtw.WriteValue(objOutWardFee[i].fee_rcd);
                                    }
                                    xtw.WriteEndElement();
                                    xtw.WriteStartElement("currency_rcd");
                                    {
                                        xtw.WriteValue(objOutWardFee[i].currency_rcd);
                                    }
                                    xtw.WriteEndElement();
                                    xtw.WriteStartElement("display_name");
                                    {
                                        xtw.WriteValue(objOutWardFee[i].display_name);
                                    }
                                    xtw.WriteEndElement();
                                    xtw.WriteStartElement("number_of_units");
                                    {
                                        xtw.WriteValue(objOutWardFee[i].number_of_units.ToString());
                                    }
                                    xtw.WriteEndElement();
                                    xtw.WriteStartElement("total_amount");
                                    {
                                        xtw.WriteValue(objOutWardFee[i].total_amount.ToString());
                                    }
                                    xtw.WriteEndElement();
                                    xtw.WriteStartElement("total_amount_incl");
                                    {
                                        xtw.WriteValue(objOutWardFee[i].total_amount_incl.ToString());
                                    }
                                    xtw.WriteEndElement();
                                }
                                xtw.WriteEndElement();
                            }
                        }
                    }

                    foreach (Fee obj in fees)
                    {
                        if (obj.fee_category_rcd != null && obj.fee_category_rcd.Equals("BAGSALES"))
                        {
                            xtw.WriteStartElement("Selected");
                            {
                                xtw.WriteStartElement("fee_id");
                                {
                                    xtw.WriteValue(obj.fee_id.ToString());
                                }
                                xtw.WriteEndElement();
                                xtw.WriteStartElement("passenger_id");
                                {
                                    xtw.WriteValue(obj.passenger_id.ToString());
                                }
                                xtw.WriteEndElement();
                                xtw.WriteStartElement("booking_segment_id");
                                {
                                    xtw.WriteValue(obj.booking_segment_id.ToString());
                                }
                                xtw.WriteEndElement();
                                xtw.WriteStartElement("fee_rcd");
                                {
                                    xtw.WriteValue(obj.fee_rcd.ToString());
                                }
                                xtw.WriteEndElement();
                            }
                            xtw.WriteEndElement();
                        }
                    }

                }
                xtw.WriteEndElement();
            }
        }
        public void FillBaggageFeeToTemp()
        {
            Fees fees = B2CSession.Fees;
            Fees feesTMP = new Fees();
            if (fees != null && fees.Count > 0)
            {
                for (int i = 0; i < fees.Count; i++)
                {
                    if (fees[i].fee_category_rcd != null && fees[i].fee_category_rcd.Equals("BAGSALES"))
                    {
                        feesTMP.Add(fees[i]);
                    }
                }
                //Add new fee object to null fee session.
                B2CSession.BaggageFeeTemp = feesTMP;
            }
        }
        public void FillTempToBaggageFee()
        {
            Fees fees = B2CSession.Fees;
            Fees feesTMP = B2CSession.BaggageFeeTemp;

            if (feesTMP != null)
            {
                //Remove All Baggage Fee
                if (fees != null && fees.Count > 0)
                {
                    for (int i = 0; i < fees.Count; i++)
                    {
                        if (fees[i].fee_category_rcd != null && fees[i].fee_category_rcd.Equals("BAGSALES"))
                        {
                            fees.RemoveAt(i);
                            i--;
                        }
                    }
                }

                if (feesTMP != null && feesTMP.Count > 0)
                {
                    for (int i = 0; i < feesTMP.Count; i++)
                    {
                        if (feesTMP[i].fee_category_rcd != null && feesTMP[i].fee_category_rcd.Equals("BAGSALES"))
                            fees.Add(feesTMP[i]);
                    }
                }

                B2CSession.Fees = fees;
            }
        }
        public void ErrorLog(Exception ex)
        {
            var webAPI = new LoggingWebAPI();
            var loggingService = new WebAPILoggingClient(webAPI);

            string strAppName = B2CSetting.LoggingServiceAppName;
            string strEnvironment = B2CSetting.LoggingServiceEnvironment;
            string strEndpoint = B2CSetting.LoggingServiceEndPoint;

            if (!string.IsNullOrEmpty(strAppName) && !string.IsNullOrEmpty(strEnvironment) && !string.IsNullOrEmpty(strEndpoint))
            {
                var endPoint = new Uri(strEndpoint);
                var logErrorRequest = new LogErrorRequest(strAppName, strEnvironment, endPoint, ex);
                loggingService.LogError(logErrorRequest);
            }
        }

    }

    // Start Login Cross App Cookie
    public enum CookiePeriod
    {
        Day,
        Month,
        Year,
        Hour,
        Minute,
        Second,
        Millisecond
    }
    // End Login Cross App Cookie

    public static class Language
    {
        public static string Value(string key, string defaultValue, StringDictionary sd)
        {
            try
            {
                string result;

                if (key.Length == 0 || sd == null)
                { result = defaultValue; }
                else
                {
                    if (sd[key] != null)
                    { result = HttpUtility.UrlDecode(HttpUtility.HtmlDecode(sd[key])); }
                    else
                    { result = defaultValue; }
                }

                return result;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, key + "," + defaultValue);
                throw ex;
            }
        }
        public static string CurrentCode()
        {
            try
            {
                string[] strLangCode;
                if (string.IsNullOrEmpty(B2CSession.LanCode) == false)
                {
                    strLangCode = B2CSession.LanCode.Split('-');
                    if (strLangCode.Length == 0)
                    {
                        strLangCode = B2CSetting.DefaultLanguage.Split('-');
                    }
                }
                else
                {
                    strLangCode = B2CSetting.DefaultLanguage.Split('-');
                }

                if (strLangCode.Length != 0)
                {
                    if (strLangCode[0].ToUpper().Equals("ZH"))
                    {
                        return strLangCode[1];
                    }
                    else
                    {
                        return strLangCode[0];
                    }
                }
                else
                {
                    return "EN";
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static string CurrentFullCode()
        {
            try
            {
                if (string.IsNullOrEmpty(B2CSession.LanCode) == false)
                {
                    string strLangCode = B2CSession.LanCode;
                    if (strLangCode.Length == 0)
                    {
                        strLangCode = B2CSetting.DefaultLanguage;
                    }
                    return strLangCode;
                }
                else
                {
                    B2CSession.LanCode = B2CSetting.DefaultLanguage;
                    return B2CSession.LanCode;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }

        public static StringDictionary GetLanguageDictionary()
        {
            try
            {
                string strLanguageCode = CurrentFullCode();
                StringDictionary sd = (StringDictionary)HttpContext.Current.Cache["Language-" + strLanguageCode];
                if (sd == null || sd.Count == 0)
                {
                    Languages objLanguage = new Languages();
                    if (objLanguage.LoadLanguage("B2C", strLanguageCode) == true)
                    {
                        sd = objLanguage.GetDictionary();
                        if (sd != null && sd.Count > 0)
                        {
                            HttpRuntime.Cache.Insert("Language-" + strLanguageCode,
                                                sd,
                                                null,
                                                DateTime.Now.AddMinutes(20),
                                                System.Web.Caching.Cache.NoSlidingExpiration,
                                                System.Web.Caching.CacheItemPriority.Normal,
                                                null);
                        }
                    }
                    else
                    {
                        //load en-us as default
                        B2CSession.LanCode = string.Empty;
                        strLanguageCode = CurrentFullCode();
                        sd = (StringDictionary)HttpContext.Current.Cache["Language-" + strLanguageCode];
                        if (sd == null || sd.Count == 0)
                        {
                            if (objLanguage.LoadLanguage("B2C", strLanguageCode))
                            {
                                sd = objLanguage.GetDictionary();
                                HttpRuntime.Cache.Insert("Language-" + strLanguageCode,
                                                        sd,
                                                        null,
                                                        DateTime.Now.AddMinutes(20),
                                                        System.Web.Caching.Cache.NoSlidingExpiration,
                                                        System.Web.Caching.CacheItemPriority.Normal,
                                                        null);
                            }
                        }
                    }

                    objLanguage.Dispose();
                }
                return sd;
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
    }
    public class LangaugeHelper
    {
        private StringDictionary _sd;
        public LangaugeHelper(StringDictionary sd)
        {
            _sd = sd;
        }
        public string get(string key, string defaultValue)
        {
            return Classes.Language.Value(key, defaultValue, _sd);
        }
        public string CurrentCode()
        {
            return Classes.Language.CurrentCode();
        }
        public static string GetAbbrivateDay(int iDay)
        {
            System.Globalization.CultureInfo GlobalDt = new System.Globalization.CultureInfo(Language.CurrentFullCode());
            System.Globalization.DateTimeFormatInfo dt = GlobalDt.DateTimeFormat;

            return dt.AbbreviatedDayNames[iDay].ToString();

        }
        public static string GetAbbrivateMonth(int iMonth)
        {
            System.Globalization.CultureInfo GlobalDt = new System.Globalization.CultureInfo(Language.CurrentFullCode());
            System.Globalization.DateTimeFormatInfo dt = GlobalDt.DateTimeFormat;

            return dt.AbbreviatedMonthNames[iMonth].ToString();
        }
        public static string GetFullDay(int iDay)
        {
            System.Globalization.CultureInfo GlobalDt = new System.Globalization.CultureInfo(Language.CurrentFullCode());
            System.Globalization.DateTimeFormatInfo dt = GlobalDt.DateTimeFormat;

            return dt.DayNames[iDay].ToString();
        }
        public static string GetFullMonth(int iMonth)
        {
            System.Globalization.CultureInfo GlobalDt = new System.Globalization.CultureInfo(Language.CurrentFullCode());
            System.Globalization.DateTimeFormatInfo dt = GlobalDt.DateTimeFormat;

            return dt.MonthNames[iMonth].ToString();
        }
    }
    public static class CacheHelper
    {
        public static Routes CacheOrigin()
        {
            try
            {
                Routes route = (Routes)HttpRuntime.Cache["origin-" + Classes.Language.CurrentCode().ToUpper()];
                if (route != null && route.Count > 0)
                {
                    return route;
                }
                else
                {
                    ServiceClient srvClient = new ServiceClient();
                    route = srvClient.GetOrigins(Classes.Language.CurrentCode().ToUpper(), true, false, false, false, false);
                    srvClient = null;

                    if (route != null && route.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("origin-" + Classes.Language.CurrentCode().ToUpper(),
                                                route,
                                                null,
                                                DateTime.Now.AddMinutes(20),
                                                System.Web.Caching.Cache.NoSlidingExpiration,
                                                System.Web.Caching.CacheItemPriority.Normal,
                                                null);
                    }

                    return route;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static Routes CacheDestination()
        {
            try
            {
                Routes routes = (Routes)HttpRuntime.Cache["destination-" + Classes.Language.CurrentCode().ToUpper()];
                if (routes != null && routes.Count > 0)
                {
                    return routes;
                }
                else
                {
                    ServiceClient srvClient = new ServiceClient();
                    routes = srvClient.GetDestination(Classes.Language.CurrentCode().ToUpper(), true, false, false, false, false);
                    srvClient = null;

                    if (routes != null && routes.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("destination-" + Classes.Language.CurrentCode().ToUpper(),
                                                routes,
                                                null,
                                                DateTime.Now.AddMinutes(20),
                                                System.Web.Caching.Cache.NoSlidingExpiration,
                                                System.Web.Caching.CacheItemPriority.Normal,
                                                null);
                    }
                    return routes;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static Documents CacheDocumentType()
        {
            try
            {
                Documents documents = (Documents)HttpRuntime.Cache["DocType-" + Classes.Language.CurrentCode().ToUpper()];
                if (documents != null && documents.Count > 0)
                {
                    return documents;
                }
                else
                {
                    ServiceClient srvClient = new ServiceClient();
                    srvClient.objService = B2CSession.AgentService;
                    documents = srvClient.GetDocumentType(Classes.Language.CurrentCode().ToUpper());
                    srvClient.objService = null;

                    if (documents != null && documents.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("DocType-" + Classes.Language.CurrentCode().ToUpper(), documents, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    }
                    return documents;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static Titles CachePassengerTitle()
        {
            try
            {
                Titles titles = (Titles)HttpRuntime.Cache["PaxTitle-" + Classes.Language.CurrentCode().ToUpper()];
                if (titles != null && titles.Count > 0)
                {
                    return titles;
                }
                else
                {
                    ServiceClient srvClient = new ServiceClient();
                    srvClient.objService = B2CSession.AgentService;

                    titles = srvClient.GetPassengerTitles(Classes.Language.CurrentCode().ToUpper());

                    srvClient.objService = null;
                    HttpRuntime.Cache.Insert("PaxTitle-" + Classes.Language.CurrentCode().ToUpper(), titles, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    return titles;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static Countries CacheCountry()
        {
            try
            {
                Countries countries = (Countries)HttpRuntime.Cache["Country-" + Classes.Language.CurrentCode().ToUpper()];
                if (countries != null && countries.Count > 0)
                {
                    return countries;
                }
                else
                {
                    ServiceClient srvClient = new ServiceClient();
                    countries = srvClient.GetCountry(Classes.Language.CurrentCode().ToUpper());
                    srvClient = null;

                    HttpRuntime.Cache.Insert("Country-" + Classes.Language.CurrentCode().ToUpper(), countries, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    return countries;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static Languages CacheLanguage()
        {
            try
            {
                Languages objLanguages = (Languages)HttpRuntime.Cache["Languages-" + Classes.Language.CurrentCode().ToUpper()];
                if (objLanguages != null && objLanguages.Count > 0)
                {
                    return objLanguages;
                }
                else
                {
                    ServiceClient srvClient = new ServiceClient();
                    srvClient.objService = B2CSession.AgentService;

                    objLanguages = srvClient.GetLanguages(Classes.Language.CurrentCode().ToUpper());

                    srvClient.objService = null;

                    HttpRuntime.Cache.Insert("Languages-" + Classes.Language.CurrentCode().ToUpper(), objLanguages, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    return objLanguages;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static Services CacheSpecialServiceRef()
        {
            try
            {
                Services objServices = (Services)HttpRuntime.Cache["SpecialServiceRef-" + Classes.Language.CurrentCode().ToUpper()];
                if (objServices != null && objServices.Count > 0)
                {
                    return objServices;
                }
                else
                {
                    ServiceClient srvClient = new ServiceClient();
                    srvClient.objService = B2CSession.AgentService;

                    objServices = srvClient.GetSpecialServices(Classes.Language.CurrentCode().ToUpper());

                    srvClient.objService = null;

                    HttpRuntime.Cache.Insert("SpecialServiceRef-" + Classes.Language.CurrentCode().ToUpper(), objServices, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    return objServices;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static Currencies CacheCurrencies()
        {
            try
            {
                Currencies currencies = (Currencies)HttpRuntime.Cache["Currencies-" + Classes.Language.CurrentCode().ToUpper()];
                if (currencies != null && currencies.Count > 0)
                {
                    return currencies;
                }
                else
                {
                    ServiceClient srvClient = new ServiceClient();
                    currencies = srvClient.GetCurrencies(Classes.Language.CurrentCode().ToUpper());
                    srvClient = null;

                    if (currencies != null && currencies.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("Currencies-" + Classes.Language.CurrentCode().ToUpper(), currencies, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    }
                    return currencies;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        public static Agent CacheAgency(string strAgencyCode)
        {
            try
            {
                Agent objAgent = (Agent)HttpRuntime.Cache["DefaultAgency-" + strAgencyCode];
                if (objAgent != null)
                {
                    return objAgent;
                }
                else
                {
                    Agents objAgents = new Agents();
                    tikSystem.Web.Library.WebService objService = new tikSystem.Web.Library.WebService();
                    objService.InitialWebServices(string.Empty, string.Empty, string.Empty, strAgencyCode, ref objAgents);
                    if (objAgents != null && objAgents.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("DefaultAgency-" + strAgencyCode, objAgents[0], null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                        return objAgents[0];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
    }

    public static class HttpHelper
    {
        public static string LoadSecurePage(Client client, int currentStep)
        {
            string result = string.Empty;   //Off HttpsAtStep
            int HttpsAtStep = B2CSetting.HttpsAtStep;
            if (HttpsAtStep == 0)
                return result;  //Force redirect to HTTP

            //On HttpsAtStep
            if ((B2CSetting.UseSecureHttps || HttpsAtStep != 0 && currentStep >= HttpsAtStep || currentStep == 6 ||
                (client != null && client.client_number > 0)) && currentStep != 12)
            {
                //Redirect to HTTPS if current state is HTTP;
                if (LoadBalanceScheme.ToLower().Equals(Uri.UriSchemeHttp))
                {
                    return Uri.UriSchemeHttps;
                }
            }
            else
            {
                //Redirect to HTTP if current state is HTTPS;
                if (LoadBalanceScheme.ToLower().Equals(Uri.UriSchemeHttps))
                {
                    return Uri.UriSchemeHttp;
                }
            }

            return result;
        }

        public static bool IsIpAddress(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            Regex regex = new Regex(@"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return regex.IsMatch(value);
        }

        public static bool IsHostName(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            Regex regex = new Regex(@"^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return regex.IsMatch(value);
        }

        public static bool IsLocalhost(string value)
        {
            bool output = false;
            if (string.IsNullOrEmpty(value) || value.ToLower().Equals("localhost"))
                return true;

            IPAddress[] ipaddresses = Dns.GetHostAddresses(value);
            for (int i = 0; i < ipaddresses.Length; i++)
            {
                if (ipaddresses[i].ToString().Equals("::1") || ipaddresses[i].ToString().Equals("127.0.0.1"))
                {
                    output = true;
                    break;
                }
            }

            return output;
        }

        public static bool isSecureConnection
        {
            get
            {
                bool hasForwordedProto = String.Equals(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_PROTO"], Uri.UriSchemeHttps, StringComparison.OrdinalIgnoreCase);
                bool hasForwaredFor = !string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);
                return hasForwaredFor ? hasForwaredFor : hasForwordedProto;
            }
        }

        public static string LoadBalanceScheme
        {
            get
            {
                if (isSecureConnection)
                    return Uri.UriSchemeHttps;
                else
                    return HttpContext.Current.Request.Url.Scheme;
            }
        }
    }
}
