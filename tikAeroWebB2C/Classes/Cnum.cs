﻿using System;
using System.Collections.Generic;
using System.Web;

namespace tikAeroB2C.Cnum
{
    public static class CnumQueryString
    {
        static string _langculture = "langculture";
        static string _ao = "ao";

        public static string LanguageCulture { get { return _langculture; } }
        public static string ao { get { return _ao; } }
    }

    public static class CnumAppSetting
    {
        static string _True = "true";
        static string _False = "false";
        static string _OutputCalendarControl        = "OutputCalendarControl";
        static string _CalendarToControls           = "CalendarToControls";
        static string _DateFormat                   = "DateFormat";
        static string _DefaultAgencyCode            = "DefaultAgencyCode";
        static string _UseForceWWW                  = "UseForceWWW";
        

        public static string OutputCalendarControl { get { return _OutputCalendarControl; } }
        public static string CalendarToControls { get { return _CalendarToControls; } }
        public static string DateFormat { get { return _DateFormat; } }
        public static string DefaultAgencyCode { get { return _DefaultAgencyCode; } }
        public static string UseForceWWW { get { return _UseForceWWW; } }
        public static string True { get { return _True; } }
        public static string False { get { return _False; } }
        
    }

    public static class CnumDefault
    {
        public static object Null 
        { 
            get { return null; } 
        }
        public static string NullString
        {
            get { return ""; }
        }
        //public static object Null
        //{
        //    get { return null; }
        //}

        public static int ErrorCodeInt
        {
            get { return -1; }
        }

        public static string ErrorCodeString
        {
            get { return ""; }
        }
    }

    public static class CnumSession
    {
        static string _ObjB2CVariable           = "B2CVariable";
        static string _CurrentLanguage          = "CurrentLanguage";
        static string _LanCode                  = "LanCode";
        static string _Language                 = "Language";
        static string _AgentService             = "AgentService";
        static string _CcPayment                = "CcPayment";
        static string _PaymentFees              = "PaymentFees";
        static string _PaymentApprovalID        = "PaymentApprovalID";
        static string _PaymentRequestStreamText = "PaymentRequestStreamText";
        static string _ETZEncrypt               = "ETZEncrypt";
        static string _ITSEncrypt               = "ITSEncrypt";
        static string _BookingHeader            = "BookingHeader";
        static string _Itinerary                = "Itinerary";
        static string _Passenger                = "Passenger";
        static string _Passengers               = "Passengers";
        static string _Quotes                   = "Quotes";
        static string _Fees                     = "Fees";
        static string _Mappings                 = "Mappings";
        static string _Services                 = "Services";
        static string _Remarks                  = "Remarks";
        static string _Payments                 = "Payments";
        static string _Taxes                    = "Taxes";
        static string _ResponsePrice            = "responsePrice";
        static string _InsuaranceFee            = "InsuaranceFee";
        static string _BaggageFeeDepart         = "BaggageFeeDepart";
        static string _BaggageFeeReturn         = "BaggageFeeReturn";
        static string _BaggageFeeTemp           = "BaggageFeeTemp";
        static string _Https                    = "bHttps";
        static string _CurrentClientDate        = "strCurrentClientDate";
        static string _Client                   = "Client";
        static string _CaptchaImageText         = "CaptchaImageText";
        static string _ACEPassenger             = "ACEPassenger";
        static string _ACEQuotePassengers       = "ACEQuotePassengers";
        static string _Login                    = "Login";
        static string _SenderEmailFrom          = "SenderEmailFrom";
        static string _SenderEmailTo            = "SenderEmailTo";
        static string _NumberOfBccGroup         = "NumberOfBccGroup";
        static string _SearchRegister           = "SearchRegister";
        static string _InsuaranceNumber         = "InsuaranceNumber";
        static string _TatraEncrypt             = "TatraEncrypt";
        static string _Vouchers                 = "Vouchers";
        static string _xmlHistory               = "xmlHistory";
        static string _xmlLife                  = "xmlLife";
        static string _xmlFFP                   = "xmlFFP";
        static string _Agents                   = "Agents";
        static string _SessionCreate            = "SessionCreate";
        static string _SeatMapParameter         = "SeatMapParameter";
        static string _BookingSummary = "BookingSummary";
        //static string _Language = "Language";
        
        public static string ObjB2CVariable { get { return _ObjB2CVariable; } }
        public static string CurrentLanguage { get { return _CurrentLanguage; } }
        public static string LanCode { get { return _LanCode; } }
        public static string Language { get { return _Language; } }
        public static string AgentService { get { return _AgentService; } }
        public static string CcPayment { get { return _CcPayment; } }
        public static string PaymentFees { get { return _PaymentFees; } }
        public static string PaymentApprovalID { get { return _PaymentApprovalID; } }
        public static string PaymentRequestStreamText { get { return _PaymentRequestStreamText; } }
        public static string ETZEncrypt { get { return _ETZEncrypt; } }
        public static string ITSEncrypt { get { return _ITSEncrypt; } }
        public static string BookingHeader { get { return _BookingHeader; } }
        public static string Itinerary { get { return _Itinerary; } }
        public static string Passenger { get { return _Passenger; } }
        public static string Passengers { get { return _Passengers; } }
        public static string Quotes { get { return _Quotes; } }
        public static string Fees { get { return _Fees; } }
        public static string Mappings { get { return _Mappings; } }
        public static string Services { get { return _Services; } }
        public static string Remarks { get { return _Remarks; } }
        public static string Payments { get { return _Payments; } }
        public static string Taxes { get { return _Taxes; } }
        public static string ResponsePrice { get { return _ResponsePrice; } }
        public static string InsuaranceFee { get { return _InsuaranceFee; } }
        public static string BaggageFeeDepart { get { return _BaggageFeeDepart; } }
        public static string BaggageFeeReturn { get { return _BaggageFeeReturn; } }
        public static string BaggageFeeTemp { get { return _BaggageFeeTemp; } }
        public static string Https { get { return _Https; } }
        public static string CurrentClientDate { get { return _CurrentClientDate; } }
        public static string Client { get { return _Client; } }
        public static string CaptchaImageText { get { return _CaptchaImageText; } }
        public static string ACEPassenger { get { return _ACEPassenger; } }
        public static string ACEQuotePassengers { get { return _ACEQuotePassengers; } }
        public static string Login { get { return _Login; } }
        public static string SenderEmailFrom { get { return _SenderEmailFrom; } }
        public static string SenderEmailTo { get { return _SenderEmailTo; } }
        public static string NumberOfBccGroup { get { return _NumberOfBccGroup; } }
        public static string SearchRegister { get { return _SearchRegister; } }
        public static string InsuaranceNumber { get { return _InsuaranceNumber; } }
        public static string TatraEncrypt { get { return _TatraEncrypt; } }
        public static string Vouchers { get { return _Vouchers; } }
        public static string xmlHistory { get { return _xmlHistory; } }
        public static string xmlLife { get { return _xmlLife; } }
        public static string xmlFFP { get { return _xmlFFP; } }
        public static string Agents { get { return _Agents; } }
        public static string SessionCreate { get { return _SessionCreate; } }
        public static string SeatMapParameter { get { return _SeatMapParameter; } }
        public static string BookingSummary { get { return _BookingSummary; } }

        //public static string Language { get { return _Language; } } 
    }

    public static class CnumCookie
    {
        static string _logAgency = "logAgency";

        public static string LogAgency { get { return _logAgency; } }
    }
   
    public static class CnumJSName
    {
        static string _LanguageScript = "languageScript";

        public static string LanguageScript { get { return _LanguageScript; } }
    }

    //public static class CnumSessionPage
    //{
    //    static string _XXX = "XXX";

    //    public static string XXX { get { return _XXX; } }
    //}
    
    
    //Code Template
    //public static class CnumXXX
    //{
    //    static string _XXX = "XXX";
        
    //    public static string XXX { get { return _XXX; } }
    //}
}