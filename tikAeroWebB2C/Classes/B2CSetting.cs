﻿using System;
using System.Configuration;
using System.Reflection;

namespace tikAeroB2C
{
    public class B2CSetting
    {
        public static string DefaultAgencyCode
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DefaultAgencyCode"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }

        public static string DefaultAgencyLogon
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DefaultAgencyLogon"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }

        public static string DefaultAgencyPassword
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DefaultAgencyPassword"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string DefaultLanguage
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DefaultLanguage"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static int Service
        {
            get
            {
                int output = 1;
                string result = ConfigurationManager.AppSettings["Service"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                int.TryParse(result, out output);
                return output;
            }
        }
        public static int UseSessionless
        {
            get
            {
                int output = 0;
                string result = ConfigurationManager.AppSettings["UseSessionless"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                int.TryParse(result, out output);
                return output;
            }
        }
        public static string AffixEncrypt
        {
            get
            {
                string result = ConfigurationManager.AppSettings["AffixEncrypt"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string PrefixEncrypt
        {
            get
            {
                string result = ConfigurationManager.AppSettings["PrefixEncrypt"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static Guid UserId
        {
            get
            {
                string result = ConfigurationManager.AppSettings["UserId"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? new Guid(result) : Guid.Empty;
            }
        }
        public static bool UseSecureHttps
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["UseSecureHttps"];
                bool.TryParse(result, out output);
                return output;
            }
        }

        public static int HttpsAtStep
        {
            get
            {
                int output = 0;
                string result = ConfigurationManager.AppSettings["HttpsAtStep"];
                int.TryParse(result, out output);
                return output;
            }
        }
        public static string DefaultTitle
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DefaultTitle"];
                //if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string DefaultDestination
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DefaultDestination"];
                //if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static bool DifferentiateSeatByFee
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["DifferentiateSeatByFee"];
                //if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                bool.TryParse(result, out output);
                return output;
            }
        }

        public static string UseDialogLogon
        {
            get
            {
                string result = ConfigurationManager.AppSettings["UseDialogLogon"];
                //if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string THREAD_WEMS
        {
            get
            {
                string result = ConfigurationManager.AppSettings["THREAD_WEMS"];
                return !string.IsNullOrEmpty(result) ? result : "B2C";
            }
        }
        public static string SearchFareType
        {
            get
            {
                string result = ConfigurationManager.AppSettings["SearchFareType"];
                return !string.IsNullOrEmpty(result) ? result : "LF";
            }
        }

        public static Int16 LowFareFinderRange
        {
            get
            {
                Int16 output = 7;
                string result = ConfigurationManager.AppSettings["LowFareFinderRange"];
                Int16.TryParse(result, out output);
                return output;
            }
        }

        public static bool LowFareCalendarMode
        {
            get
            {
                bool output = true;
                string result = ConfigurationManager.AppSettings["LowFareCalendarMode"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static double TimelimitMinute
        {
            get
            {
                double output = 0;
                string result = ConfigurationManager.AppSettings["TimelimitMinute"];
                double.TryParse(result, out output);
                return output;
            }
        }
        public static bool SkipStep3
        {
            get
            {
                bool output = true;
                string result = ConfigurationManager.AppSettings["SkipStep3"];
                bool.TryParse(result, out output);
                return output;
            }
        }

        public static string SeatToStep
        {
            get
            {
                string result = ConfigurationManager.AppSettings["SeatToStep"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string NewsRegisterDB
        {
            get
            {
                string result = ConfigurationManager.AppSettings["NewsRegisterDB"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }

        public static Int16 RowsPerPageBooking
        {
            get
            {
                Int16 output = 10;
                string result = ConfigurationManager.AppSettings["RowsPerPageBooking"];
                Int16.TryParse(result, out output);
                return output;
            }
        }
        public static string UseB2BLogonDialog
        {
            get
            {
                string result = ConfigurationManager.AppSettings["UseB2BLogonDialog"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string UseB2ELogonDialog
        {
            get
            {
                string result = ConfigurationManager.AppSettings["UseB2ELogonDialog"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string B2BLocation
        {
            get
            {
                string result = ConfigurationManager.AppSettings["B2BLocation"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string B2ELocation
        {
            get
            {
                string result = ConfigurationManager.AppSettings["B2ELocation"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string B2BAdminLocation
        {
            get
            {
                string result = ConfigurationManager.AppSettings["B2BAdminLocation"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string B2EAdminLocation
        {
            get
            {
                string result = ConfigurationManager.AppSettings["B2EAdminLocation"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string B2BCookieName
        {
            get
            {
                string result = ConfigurationManager.AppSettings["B2BCookieName"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string B2ECookieName
        {
            get
            {
                string result = ConfigurationManager.AppSettings["B2ECookieName"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static Int16 LogonCookieLifeTime
        {
            get
            {
                Int16 output = 5;
                string result = ConfigurationManager.AppSettings["LogonCookieLifeTime"];
                Int16.TryParse(result, out output);
                return output;
            }
        }
        public static string FFPCreateEmailSubject
        {
            get
            {
                string result = ConfigurationManager.AppSettings["FFPCreateEmailSubject"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string FFPResetEmailSubject
        {
            get
            {
                string result = ConfigurationManager.AppSettings["FFPResetEmailSubject"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string FFPChangeEmailSubject
        {
            get
            {
                string result = ConfigurationManager.AppSettings["FFPChangeEmailSubject"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string DefaultCurrency
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DefaultCurrency"];
                if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string CountryOrder
        {
            get
            {
                string result = ConfigurationManager.AppSettings["CountryOrder"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string CountryPath
        {
            get
            {
                string result = ConfigurationManager.AppSettings["CountryPath"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static Int16 MaximumCalMonth
        {
            get
            {
                Int16 output = 0;
                string result = ConfigurationManager.AppSettings["MaximumCalMonth"];
                Int16.TryParse(result, out output);
                return output;
            }
        }
        public static string UseBaggageCalculation
        {
            get
            {
                string result = ConfigurationManager.AppSettings["UseBaggageCalculation"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string OutputCalendarControl
        {
            get
            {
                string result = ConfigurationManager.AppSettings["OutputCalendarControl"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string CalendarToControls
        {
            get
            {
                string result = ConfigurationManager.AppSettings["CalendarToControls"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string DateFormat
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DateFormat"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static Int16 PaymentRefDigit
        {
            get
            {
                Int16 output = 0;
                string result = ConfigurationManager.AppSettings["PaymentRefDigit"];
                Int16.TryParse(result, out output);
                return output;
            }
        }
        public static string DateTimeMark
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DateTimeMark"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static bool SkipOutBoundDayRange
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["SkipOutBoundDayRange"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static bool SkipFareLogic
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["SkipFareLogic"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static string SmtpServer
        {
            get
            {
                string result = ConfigurationManager.AppSettings["SmtpServer"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string EmailSubject
        {
            get
            {
                string result = ConfigurationManager.AppSettings["EmailSubject"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string EmailFrom
        {
            get
            {
                string result = ConfigurationManager.AppSettings["EmailFrom"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string ErrorTo
        {
            get
            {
                string result = ConfigurationManager.AppSettings["ErrorTo"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string GroupEmailTo
        {
            get
            {
                string result = ConfigurationManager.AppSettings["GroupEmailTo"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string CharterEmailTo
        {
            get
            {
                string result = ConfigurationManager.AppSettings["CharterEmailTo"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string BonVoyageEmailTo
        {
            get
            {
                string result = ConfigurationManager.AppSettings["BonVoyageEmailTo"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string Ogone3DRequestSource
        {
            get
            {
                string result = ConfigurationManager.AppSettings["Ogone3DRequestSource"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static bool SetSeatMandatory
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["SetSeatMandatory"];
                if (!string.IsNullOrEmpty(result)) bool.TryParse(result, out output);
                return output;
            }
        }
        public static short NumberOfColumn
        {
            get
            {
                short output = 0;
                string result = ConfigurationManager.AppSettings["NumberOfColumn"];
                short.TryParse(result, out output);
                return output;
            }
        }
        public static string CobUrl
        {
            get
            {
                string result = ConfigurationManager.AppSettings["CobUrl"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static bool UseForceWWW
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["UseForceWWW"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static string SsrGroup
        {
            get
            {
                string result = ConfigurationManager.AppSettings["SsrGroup"];
                //if (result == null) throw new ArgumentNullException(MethodBase.GetCurrentMethod().Name);
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string SSRCodeMultipleSelect
        {
            get
            {
                string result = ConfigurationManager.AppSettings["SSRCodeMultipleSelect"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string SSRCodeSingleSelect
        {
            get
            {
                string result = ConfigurationManager.AppSettings["SSRCodeSingleSelect"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string BusTransfer
        {
            get
            {
                string result = ConfigurationManager.AppSettings["BusTransfer"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static bool UsePaging
        {
            get
            {
                bool output = true;
                string result = ConfigurationManager.AppSettings["UsePaging"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static string DefaultCardType
        {
            get
            {
                string result = ConfigurationManager.AppSettings["DefaultCardType"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static Int16 SearchDayRange
        {
            get
            {
                Int16 output = -1;
                string result = ConfigurationManager.AppSettings["SearchDayRange"];
                Int16.TryParse(result, out output);
                return output;
            }
        }
        public static string CalendarFirstDayOfWeek
        {
            get
            {
                string result = ConfigurationManager.AppSettings["CalendarFirstDayOfWeek"];
                return !string.IsNullOrEmpty(result) ? result : "SUNDAY";
            }
        }
        public static bool AttactEmail
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["AttactEmail"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static bool SSRInventoryService
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["SSRInventoryService"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static int TabDayRange
        {
            get
            {
                int output = 0;
                string result = ConfigurationManager.AppSettings["TabDayRange"];
                int.TryParse(result, out output);
                return output;
            }
        }
        public static string OffLowFareFinder
        {
            get
            {
                string result = ConfigurationManager.AppSettings["OffLowFareFinder"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string AllowPaypal
        {
            get
            {
                string result = ConfigurationManager.AppSettings["AllowPaypal"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string PaypalRepeat
        {
            get
            {
                string result = ConfigurationManager.AppSettings["PaypalRepeat"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string GoogleTagManagerID
        {
            get
            {
                string result = ConfigurationManager.AppSettings["GoogleTagManagerID"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string GoogleAnalyticID
        {
            get
            {
                string result = ConfigurationManager.AppSettings["GoogleAnalyticID"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static int MaximumAddMember
        {
            get
            {
                int output = 1;
                string result = ConfigurationManager.AppSettings["MaximumAddMember"];
                int.TryParse(result, out output);
                return output;
            }
        }
        public static string EnablePaxMiddlename
        {
            get
            {
                string result = ConfigurationManager.AppSettings["EnablePaxMiddlename"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static bool ShowFFPPoint
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["ShowFFPPoint"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static string ShowClientNumber
        {
            get
            {
                string result = ConfigurationManager.AppSettings["ShowClientNumber"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string AllowMultipleVoucher
        {
            get
            {
                string result = ConfigurationManager.AppSettings["AllowMultipleVoucher"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static string AllowMultipleFOP
        {
            get
            {
                string result = ConfigurationManager.AppSettings["AllowMultipleFOP"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }

        public static string CreateACEPassengerSession
        {
            get
            {
                string result = ConfigurationManager.AppSettings["CreateACEPassengerSession"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
        public static bool ClearSessionItinerary
        {
            get
            {
                bool output = false;
                string result = ConfigurationManager.AppSettings["ClearSessionItinerary"];
                bool.TryParse(result, out output);
                return output;
            }
        }
        public static string LoggingServiceAppName
        {
            get
            {
                string result = ConfigurationManager.AppSettings["LoggingService.AppName"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }

        public static string LoggingServiceEnvironment
        {
            get
            {
                string result = ConfigurationManager.AppSettings["LoggingService.Environment"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }

        public static string LoggingServiceEndPoint
        {
            get
            {
                string result = ConfigurationManager.AppSettings["LoggingService.EndPoint"];
                return !string.IsNullOrEmpty(result) ? result : "";
            }
        }
    }
}