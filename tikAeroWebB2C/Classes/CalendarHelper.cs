﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using System.Configuration;


namespace tikAeroB2C.Classes
{
    public static class CalendarHelper
    {
        public static string Currentculture = "";
        public static string monthFormat = "MMM";

        public static string GetCalOutputControl(string id, bool IsRenderCalendarIcon, bool IsVisibleCalendarIcon)
        {

            if (B2CSession.LanCode != null)
            {
                CultureInfo ci = new CultureInfo(B2CSession.LanCode);
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;
            }
            else
            {
                if (Currentculture != "")
                {
                    CultureInfo ci = new CultureInfo(Currentculture);
                    Thread.CurrentThread.CurrentCulture = ci;
                    Thread.CurrentThread.CurrentUICulture = ci;
                }
            }

            int iMaximumCalMonth = 12;

            DropDownList ddlDate = new DropDownList();
            DropDownList ddlMonthYear = new DropDownList();
            TextBox txtDate = new TextBox();

            txtDate.ID = "txt_" + id;
            txtDate.Attributes.Add("style", "width:51%");
            


            HtmlImage img = new HtmlImage();
            img.Src = "App_Themes/Default/Images/icon_calendar.gif";
            img.Style.Add("cursor", "hand");
            img.Border = 0;
            img.ID = "img_" + id;
 


            // show or not show calendar icon 
            if (!IsVisibleCalendarIcon)
                img.Style.Add("display", "none");

            ddlDate.ID = "ddlDate_" + id;
            ddlMonthYear.ID = "ddlMY_" + id;
            ddlDate.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
            ddlMonthYear.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
            GenDate(ddlDate);

            if (B2CSetting.MaximumCalMonth > 1)
                iMaximumCalMonth = B2CSetting.MaximumCalMonth;

            GenLimitYear(ddlMonthYear, iMaximumCalMonth, monthFormat);

            string Tmp = "";
            string OutputCalendarControl = "ddl";

            if(!string.IsNullOrEmpty(B2CSetting.OutputCalendarControl))
                 OutputCalendarControl = B2CSetting.OutputCalendarControl;

            if (OutputCalendarControl.Equals("text"))
            {
                Tmp = GetTempPlate("");

            }
            else
            {
                Tmp = GetTempPlate();

            }


            ddlDate.CssClass = "Day";
            ddlMonthYear.CssClass = "MonthYear";

            Tmp = Tmp.Replace("<!--DDL_Day-->", RenderControl(ddlDate));
            Tmp = Tmp.Replace("<!--DDL_Month_Year-->", RenderControl(ddlMonthYear));
            Tmp = Tmp.Replace("<!--TextBox-->", RenderControl(txtDate));

            if (IsRenderCalendarIcon)
                Tmp = Tmp.Replace("<!--IMG_Ctrl-->", RenderControl(img));



            return Tmp;


        }

        public static string GetTempPlate()
        {
            string ret = "";
            ret += "<!--DDL_Day-->&nbsp;<!--DDL_Month_Year-->&nbsp;<!--IMG_Ctrl-->";
            return ret;

        }

        public static string GetTempPlate(string txt)
        {
            string ret = "";
            ret += "<!--TextBox-->&nbsp;&nbsp;<!--IMG_Ctrl-->";
            return ret;

        }

        public static string RenderControl(Control ctrl)
        {
            StringBuilder html = new StringBuilder();
            StringWriter sw = new StringWriter(html);
            HtmlTextWriter hm = new HtmlTextWriter(sw);

            ctrl.RenderControl(hm);
            return html.ToString();
        }

        public static void GenLimitYear(DropDownList MyddlMonthList, int monthlimit, string monthFormat)
        {

            DateTime month = DateTime.Now;
            DateTime dtCurrentDate;
            int iMonthCount = 0;
            for (int m = 0; m < monthlimit; m++)
            {
                dtCurrentDate = month.AddMonths(iMonthCount);
                ListItem list = new ListItem();
                list.Text = dtCurrentDate.ToString(monthFormat) + " " + dtCurrentDate.Year.ToString();
                list.Value = dtCurrentDate.Year.ToString() + string.Format("{0:00}", dtCurrentDate.Month);
                MyddlMonthList.Items.Add(list);

                iMonthCount++;
            }
        }

        public static void GenDate(DropDownList ddl)
        {

          //  int d = DateTime.Now.Day;

            for (int i = 1; i <= 31; i++)
            {
                try
                {
                  //  if (i >= d)
                    {
                        DateTime NewDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, i);
                        ddl.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0')));
                    }
                }
                catch
                {
                   
                }

            }


            if (ddl.Items.FindByValue(DateTime.Today.Day.ToString().PadLeft(2, '0')) != null) ddl.Items.FindByValue(DateTime.Today.Day.ToString().PadLeft(2, '0')).Selected = true;

        }

    }

}
