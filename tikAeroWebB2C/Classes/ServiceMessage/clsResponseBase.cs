﻿using System;
using System.Collections.Generic;
using System.Web;

namespace tikAeroB2C.Classes.ServiceMessage
{
    public enum Message
    {
        SUCCESS,
        INFORMATION,
        WARNING,
        CRITICAL,
        ERROR
    };

    public abstract class ResponseBase
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public string HTML { get; set; }
        public short ToInt16 { get; set; }
    }
}