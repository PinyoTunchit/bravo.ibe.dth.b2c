﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using tikAeroB2C.Classes;
using tikAeroB2C.Classes.ServiceMessage;
using tikSystem.Web.Library;
using System.Xml.XPath;
using System.Xml.Linq;

namespace tikAeroB2C.Base
{
    public abstract partial class ServiceBase : System.Web.Services.WebService
    {
        protected ServiceResponse BasePaymentBookNowPayLater(bool ticketCreate,
                                                    string bookingDate,
                                                    string formOfPayment,
                                                    string formOfPaymentSubType)
        {
            DateTime payLimit = DateTime.MinValue;
            DateTime departureDate = DateTime.MinValue;
            string departureTime = string.Empty;
            string result = string.Empty;
            string formOfPaymentFeeXml = string.Empty;
            int hourAdd = 0;
            int minuteAdd = 0;
            decimal outStandingBalance = 0;

            try
            {
                ServiceResponse response = new ServiceResponse();
                Booking objBooking = new Booking();
                Library objLi = new Library();
                Classes.Helper objHp = new Classes.Helper();
                
                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;
                Agents agents = B2CSession.Agents;
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                //calculate pay limit
                departureDate = itinerary[0].departure_date;
                departureTime = itinerary[0].departure_time.ToString();
                hourAdd = int.Parse(departureTime.PadLeft(4, '0').Substring(0, 2));
                minuteAdd = int.Parse(departureTime.PadLeft(4, '0').Substring(2, 2));
                //Recalculate departure date with time.
                departureDate = new DateTime(departureDate.Year, departureDate.Month, departureDate.Day, hourAdd, minuteAdd, 0);

                payLimit = objBooking.GetBookingTimeLimit(remarks,
                                                          agents[0].agency_timezone,
                                                          agents[0].system_setting_timezone,
                                                          departureDate);

                if (payLimit.Equals(DateTime.MinValue))
                {
                    //External Payment Timelimit not found.
                    response.Success = false;
                    response.Code = "706";
                    response.Message = "External payment time limit not found";
                }
                else if (payLimit >= departureDate)
                {
                    //Payment time limit exceed departure date.
                    response.Success = false;
                    response.Code = "707";
                    response.Message = "Payment time limit exceed departure date";

                }
                else
                {
                    //add fees to booking object
                    if (fees != null)
                    {
                        if (fees.objService == null)
                        {
                            fees.objService = B2CSession.AgentService;
                        }
                        //fees.GetFormOfPaymentSubTypeFee(formOfPayment, formOfPaymentSubType, bookingHeader.currency_rcd, bookingHeader.agency_code, DateTime.MinValue);
                        fees.GetPaymentFee(formOfPayment, formOfPaymentSubType, bookingHeader, passengers, itinerary, quotes, fees, payments, bookingHeader.currency_rcd, objUv.UserId, formOfPaymentFeeXml, outStandingBalance);
                    }
                    //save booking
                    if (objLi.ValidSave(bookingHeader, itinerary, passengers, mappings) == true)
                    {
                        if (objUv == null || objHp.SessionTimeout() == true)
                        {
                            response.Success = false;
                            response.Code = "002";
                            response.Message = "B2CVariable is null or session expired";
                        }
                        else
                        {
                            //Clear Insurance Fee....
                            //CalculateInsuranceFee(false, false);
                            Fee objInsFee = B2CSession.InsuaranceFee;
                            fees.ClearInsuranceFee(objInsFee);

                            //tikAERO-2010-0935 start
                            bookingHeader.approval_flag = 1;
                            //end

                            if (objUv != null)
                            {
                                if (ticketCreate == true)
                                {
                                    foreach (Mapping mp in mappings)
                                    {
                                        if (mp.ticket_number == string.Empty && mp.passenger_status_rcd == "OK" && objLi.CalOutStandingBalance(quotes, fees, payments) == 0)
                                        {
                                            mp.create_by = objUv.UserId;
                                            mp.create_date_time = DateTime.Now;
                                            mp.update_by = objUv.UserId;
                                            mp.update_date_time = DateTime.Now;
                                        }
                                    }
                                }

                                //Set update information
                                objLi.SetCreateUpdateInformation(objUv.UserId, bookingHeader, itinerary, passengers, fees, mappings, services, remarks, payments, taxes);

                                //Save change information
                                objBooking = new Booking();
                                objBooking.objService = B2CSession.AgentService;
                                result = objBooking.SaveBooking(ticketCreate,
                                                                ref bookingHeader,
                                                                ref itinerary,
                                                                ref passengers,
                                                                ref quotes,
                                                                ref fees,
                                                                ref mappings,
                                                                ref services,
                                                                ref remarks,
                                                                ref payments,
                                                                ref taxes,
                                                                Classes.Language.CurrentCode().ToUpper());
                                objBooking.objService = null;
                                if (result.Length > 0)
                                {
                                    XElement elements = XElement.Parse(result);
                                    if (elements != null)
                                    {
                                        foreach (XElement x in elements.XPathSelectElements("Fee[fee_rcd='PAYPAL']"))
                                            x.RemoveAll();

                                        result = elements.ToString();
                                    }

                                    //Check Error Response
                                    XPathDocument xmlDoc = new XPathDocument(new StringReader(result));
                                    XPathNavigator nv = xmlDoc.CreateNavigator();
                                    if (nv.Select("ErrorResponse/Error").Count == 0)
                                    {
                                        //Fill new booking xml into booking object.
                                        objLi.FillBooking(result,
                                                            ref bookingHeader,
                                                            ref passengers,
                                                            ref itinerary,
                                                            ref mappings,
                                                            ref payments,
                                                            ref remarks,
                                                            ref taxes,
                                                            ref quotes,
                                                            ref fees,
                                                            ref services);

                                        B2CSession.BookingHeader = bookingHeader;

                                        //Send passenger itinerary by email.
                                        objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                                        if (B2CSession.Client != null)
                                        {
                                            objUv.CurrentStep = 7;
                                        }
                                        else
                                        {
                                            objUv.CurrentStep = 1;
                                        }

                                        response.Success = true;
                                        response.Code = "000";
                                        response.Message = "SUCCESS";
                                        response.HTML = objLi.GenerateControlString("UserControls/BookingSummary.ascx", result);

                                        objUv.FormOfPaymentFee = string.Empty;
                                        B2CSession.RemoveBookingSession();
                                    }
                                    else
                                    {
                                        response.Success = false;
                                        response.Code = XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/ErrorCode");
                                        response.Message = XmlHelper.XpathValueNullToEmpty(nv, "ErrorResponse/Error/Message");
                                    }
                                }
                                else
                                {
                                    response.Success = false;
                                    response.Code = "003";
                                    response.Message = "SaveBooking Failed";
                                }

                            }
                            else
                            {
                                response.Success = false;
                                response.Code = "004";
                                response.Message = "B2CVariable is null or session expired";
                            }
                        }
                    }
                    else
                    {
                        //Object validation failed.
                        response.Success = false;
                        response.Code = "005";
                        response.Message = "Object validation failed";
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}