﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using tikSystem.Web.Library;
using tikAeroB2C.Classes;
using tikAeroB2C.Classes.ServiceMessage;
using System.Xml.Linq;

namespace tikAeroB2C.Base
{
    public abstract partial class ServiceBase : System.Web.Services.WebService
    {
        protected ServiceResponse AddFlight(Flights flights,
                                            string ipAddress,
                                            short adult,
                                            short child,
                                            short infant,
                                            short other,
                                            string otherType,
                                            string currencyRcd)
        {
            Helper objHelper = new Helper();
            try
            {
                ServiceResponse objResponse = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;
                Library li = new Library();

                string result = string.Empty;

                if (flights != null)
                {
                    //Get Client Ip Address
                    if (string.IsNullOrEmpty(ipAddress))
                    {
                        ipAddress = DataHelper.GetClientIpAddress();
                    }

                    if (flights != null && flights.Count > 0)
                    {
                        //   if it been transited from LF Departure date will defaut at 1 of month.

                        objUv.DepartureDate = flights[0].departure_date.Year.ToString() + (flights[0].departure_date.Month < 10 ? "0" + flights[0].departure_date.Month.ToString() : flights[0].departure_date.Month.ToString()) + (flights[0].departure_date.Day < 10 ? "0" + flights[0].departure_date.Day.ToString() : flights[0].departure_date.Day.ToString());
                        //objUv.ReturnDate = objUv.DepartureDate;
                        if (objUv != null && string.IsNullOrEmpty(objUv.Agency_Code) == false)
                        {
                            if (objUv.OneWay == false & flights.Count == 1)
                            {
                                objResponse.Success = false;
                                objResponse.Code = "207";
                                objResponse.Message = "Please select return flight.";
                            }
                            else
                            {
                                if (!objUv.OneWay)
                                {
                                    if (flights.Count > 1)
                                        objUv.ReturnDate = flights[1].departure_date.Year.ToString() + (flights[1].departure_date.Month < 10 ? "0" + flights[1].departure_date.Month.ToString() : flights[1].departure_date.Month.ToString()) + (flights[1].departure_date.Day < 10 ? "0" + flights[1].departure_date.Day.ToString() : flights[1].departure_date.Day.ToString());
                                    else
                                    {
                                        objResponse.Success = false;
                                        objResponse.Code = "202";
                                        objResponse.Message = "Booking Return Flight cann't InsertFlight.";
                                        return objResponse;
                                    }
                                }

                                Agents objAgents = B2CSession.Agents;
                                string bookingID = string.Empty;

                                //Assign parameter from client script.
                                if (string.IsNullOrEmpty(currencyRcd) == false)
                                {
                                    objUv.SearchCurrencyRcd = currencyRcd;
                                    objUv.Adult = adult;
                                    objUv.Child = child;
                                    objUv.Infant = infant;
                                    objUv.Other = other;
                                    objUv.OtherPassengerType = otherType;
                                }

                                if (B2CSession.AgentService == null || objAgents == null || objUv.Agency_Code != objAgents[0].agency_code)
                                {
                                    objHelper.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                                    objAgents = B2CSession.Agents;
                                    B2CSession.Variable = objUv;
                                }

                                //Check if initialize web service success.
                                if (B2CSession.AgentService == null ||
                                    objAgents == null ||
                                    objAgents.Count == 0)
                                {
                                    objResponse.Success = false;
                                    objResponse.Code = "204";
                                    objResponse.Message = "Select flight failed please try again.";
                                }
                                else
                                {
                                    //Remove old session Data
                                    B2CSession.BookingHeader = null;
                                    B2CSession.RemoveBookingSession();

                                    Booking objBooking = new Booking();
                                    objBooking.objService = B2CSession.AgentService;

                                    if (objBooking.Booked(flights,
                                                          objAgents[0].agency_code,
                                                          objUv.SearchCurrencyRcd,
                                                          bookingID,
                                                          objUv.Adult,
                                                          objUv.Child,
                                                          objUv.Infant,
                                                          objUv.Other,
                                                          objUv.OtherPassengerType,
                                                          objUv.UserId.ToString(),
                                                          ipAddress,
                                                          Classes.Language.CurrentCode().ToUpper(),
                                                          false) == true)
                                    {

                                        if (string.IsNullOrEmpty(objBooking.ErrorCode) == true)
                                        {
                                            if ((objBooking.Passengers != null && objBooking.Passengers.Count > 0) &&
                                                (objBooking.Itinerary != null && objBooking.Itinerary.Count > 0))
                                            {
                                                string strUsFlight = objBooking.Itinerary.FindUSSegment();
                                                if (strUsFlight.Length == 0)
                                                {
                                                    if (objBooking.Mappings != null && objBooking.Mappings.Count > 0)
                                                    {
                                                        Routes rOrigin = CacheHelper.CacheOrigin();
                                                        Routes rDesc = CacheHelper.CacheDestination();

                                                        objBooking.Itinerary.FillAirportName(rOrigin, rDesc);
                                                        objBooking.Itinerary.FillIsInternationalFlight(rOrigin, rDesc, objUv.OriginRcd, objUv.DestinationRcd, objAgents[0].country_rcd);

                                                        objUv.ip_address = ipAddress;

                                                        B2CSession.BookingHeader = objBooking.BookingHeader;
                                                        B2CSession.Itinerary = objBooking.Itinerary;
                                                        B2CSession.Passengers = objBooking.Passengers;
                                                        B2CSession.Quotes = objBooking.Quotes;
                                                        B2CSession.Fees = objBooking.Fees;
                                                        B2CSession.Mappings = objBooking.Mappings;
                                                        B2CSession.Services = objBooking.Services;
                                                        B2CSession.Remarks = objBooking.Remarks;
                                                        B2CSession.Payments = objBooking.Payments;
                                                        B2CSession.Taxes = objBooking.Taxes;

                                                        if (B2CSetting.SkipStep3 == false)
                                                        {
                                                            objUv.CurrentStep = 3;
                                                            objResponse.Success = true;
                                                            objResponse.Code = "000";
                                                            objResponse.Message = "SUCCESS";
                                                            objResponse.HTML = li.GenerateControlString("UserControls/FlightSummary.ascx", string.Empty);
                                                        }
                                                        else
                                                        {
                                                            //Generate interface.
                                                            objUv.CurrentStep = 4;
                                                            if (HttpHelper.LoadSecurePage(B2CSession.Client, objUv.CurrentStep) == "https")
                                                            {
                                                                B2CSession.Https = true;
                                                                objResponse.Success = true;
                                                                objResponse.Code = "https";
                                                                objResponse.Message = "SUCCESS";
                                                            }
                                                            else
                                                            {
                                                                objResponse.Success = true;
                                                                objResponse.Code = "000";
                                                                objResponse.Message = "SUCCESS";
                                                                objResponse.HTML = li.GenerateControlString("UserControls/PassengerDetail.ascx", string.Empty);
                                                            }

                                                        }
                                                    }
                                                    else
                                                    {
                                                        objResponse.Success = false;
                                                        objResponse.Code = "201";
                                                        objResponse.Message = "Booking object is null.(Session time out).";
                                                    }
                                                }
                                                else
                                                {
                                                    objResponse.Success = false;
                                                    objResponse.Code = "101";
                                                    objResponse.Message = "Selected Flight is full";
                                                    objResponse.HTML = strUsFlight;

                                                    objBooking.Clear();
                                                }
                                            }
                                            else
                                            {
                                                objResponse.Success = false;
                                                objResponse.Code = "201";
                                                objResponse.Message = "Booking object is null.(Session time out).";
                                            }
                                        }
                                        else
                                        {
                                            objResponse.Success = false;
                                            objResponse.Code = objBooking.ErrorCode;
                                            objResponse.Message = objBooking.ErrorMessage;
                                        }
                                    }
                                    else
                                    {
                                        objResponse.Success = false;
                                        objResponse.Code = objBooking.ErrorCode;
                                        objResponse.Message = objBooking.ErrorMessage;
                                    }
                                }
                            }
                        }
                        else
                        {
                            objResponse.Success = false;
                            objResponse.Code = "201";
                            objResponse.Message = "Booking object is null.(Session time out).";
                        }
                    }
                    else
                    {
                        objResponse.Success = false;
                        objResponse.Code = "206";
                        objResponse.Message = "No Flight Request Parameter found.";
                    }
                }
                else
                {
                    objResponse.Success = false;
                    objResponse.Code = "205";
                    objResponse.Message = "No Request information found.";
                }

                return objResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse GetFullQuoteSummary(Flights flights)
        {
            try
            {
                Helper objXsl = new Helper();
                ServiceResponse response = new ServiceResponse();
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                if (flights == null || flights.Count == 0)
                {
                    //Flight xml not found.
                    response.Success = false;
                    response.Code = "100";
                    response.Message = "INVALID PARAMETER";
                }
                else
                {
                    //Add passenger type information
                    Library objLi = new Library();
                    Agents objAgents = B2CSession.Agents;
                    B2CVariable objUv = B2CSession.Variable;
                    Passengers objPassengers = new Passengers();
                    Passenger p = null;

                    //Add Adule
                    for (int i = 0; i < objUv.Adult; i++)
                    {
                        p = new Passenger();
                        p.passenger_id = Guid.NewGuid();
                        p.passenger_type_rcd = "ADULT";
                        objPassengers.Add(p);
                    }
                    //Add Child
                    for (int i = 0; i < objUv.Child; i++)
                    {
                        p = new Passenger();
                        p.passenger_id = Guid.NewGuid();
                        p.passenger_type_rcd = "CHD";
                        objPassengers.Add(p);
                    }
                    //Add Infant
                    for (int i = 0; i < objUv.Infant; i++)
                    {
                        p = new Passenger();
                        p.passenger_id = Guid.NewGuid();
                        p.passenger_type_rcd = "INF";
                        objPassengers.Add(p);
                    }

                    string strResult = string.Empty;
                    flights.objService = B2CSession.AgentService;
                    strResult = flights.GetQuoteSummary(objPassengers,
                                                        objAgents[0].agency_code,
                                                        Classes.Language.CurrentCode().ToUpper(),
                                                        SecurityHelper.GenerateSessionlessToken(),
                                                        objUv.SearchCurrencyRcd,
                                                        false);

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";

                    if (strResult.Length > 0)
                    {
                        //Render HTML.
                        System.Xml.Xsl.XslTransform objTransform = null;
                        System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);
                        objTransform = objXsl.GetXSLDocument("FareSummary");

                        response.HTML = objLi.RenderHtml(objTransform, objArgument, strResult);
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse GetQuoteSummary(Flights flights, string strFlightType)
        {
            try
            {
                Helper objXsl = new Helper();
                StringBuilder stb = new StringBuilder();
                ServiceResponse response = new ServiceResponse();
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                if (flights == null)
                {
                    response.Success = false;
                    response.Code = "100";
                    response.Message = "Request not found";
                }
                else
                {
                    if (string.IsNullOrEmpty(strFlightType))
                    {
                        response.Success = false;
                        response.Code = "101";
                        response.Message = "Flight Type Not Found";
                    }
                    else
                    {
                        string strResult = string.Empty;
                        string strDate = string.Empty;
                        Library objLi = new Library();
                        B2CVariable objUv = B2CSession.Variable;

                        if (objUv != null)
                        {
                            //Get Mapping information
                            using (StringWriter stw = new StringWriter(stb))
                            {
                                using (XmlWriter xtw = XmlWriter.Create(stw))
                                {
                                    xtw.WriteStartElement("Booking");
                                    {
                                        xtw.WriteStartElement("setting");
                                        {
                                            xtw.WriteStartElement("flight_type");
                                            {
                                                xtw.WriteValue(strFlightType);
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("OneWay");
                                            {
                                                xtw.WriteValue(Convert.ToByte(objUv.OneWay));
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("adult");
                                            {
                                                xtw.WriteValue(objUv.Adult);
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("child");
                                            {
                                                xtw.WriteValue(objUv.Child);
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("infant");
                                            {
                                                xtw.WriteValue(objUv.Infant);
                                            }
                                            xtw.WriteEndElement();

                                            //Add Flight information
                                            if (flights.Count > 0)
                                            {
                                                //Add flight information into setting node.
                                                Routes routes = CacheHelper.CacheOrigin();
                                                xtw.WriteStartElement("origin_name");
                                                {
                                                    xtw.WriteValue(objLi.FindOriginName(routes, flights[0].origin_rcd));
                                                }
                                                xtw.WriteEndElement();
                                                if (flights.Count > 1)
                                                {
                                                    xtw.WriteStartElement("transit_airport_name");
                                                    {
                                                        xtw.WriteValue(objLi.FindOriginName(routes, flights[1].origin_rcd));
                                                    }
                                                    xtw.WriteEndElement();
                                                }
                                                routes = null;

                                                routes = CacheHelper.CacheDestination();
                                                xtw.WriteStartElement("destination_name");
                                                {
                                                    if (flights.Count > 1)
                                                    {
                                                        xtw.WriteValue(objLi.FindDestinationName(routes, flights[1].destination_rcd));
                                                    }
                                                    else
                                                    {
                                                        xtw.WriteValue(objLi.FindDestinationName(routes, flights[0].destination_rcd));
                                                    }

                                                }
                                                xtw.WriteEndElement();
                                                routes = null;

                                                //Copy All input information.
                                                xtw.WriteStartElement("flight_id");
                                                {
                                                    xtw.WriteValue(flights[0].flight_id.ToString());
                                                }
                                                xtw.WriteEndElement();
                                                xtw.WriteStartElement("airline_rcd");
                                                {
                                                    xtw.WriteValue(flights[0].airline_rcd);
                                                }
                                                xtw.WriteEndElement();
                                                xtw.WriteStartElement("flight_number");
                                                {
                                                    xtw.WriteValue(flights[0].flight_number);
                                                }
                                                xtw.WriteEndElement();
                                                xtw.WriteStartElement("origin_rcd");
                                                {
                                                    xtw.WriteValue(flights[0].origin_rcd);
                                                }
                                                xtw.WriteEndElement();
                                                xtw.WriteStartElement("destination_rcd");
                                                {
                                                    xtw.WriteValue(flights[0].destination_rcd);
                                                }
                                                xtw.WriteEndElement();
                                                xtw.WriteStartElement("fare_id");
                                                {
                                                    xtw.WriteValue(flights[0].fare_id.ToString());
                                                }
                                                xtw.WriteEndElement();

                                                xtw.WriteStartElement("departure_date");
                                                {
                                                    xtw.WriteValue(string.Format("{0:yyyyMMdd}", flights[0].departure_date));
                                                }
                                                xtw.WriteEndElement();
                                                xtw.WriteStartElement("arrival_date");
                                                {
                                                    xtw.WriteValue(string.Format("{0:yyyyMMdd}", flights[0].arrival_date));
                                                }
                                                xtw.WriteEndElement();

                                                xtw.WriteStartElement("planned_departure_time");
                                                {
                                                    xtw.WriteValue(flights[0].planned_departure_time);
                                                }
                                                xtw.WriteEndElement();
                                                xtw.WriteStartElement("planned_arrival_time");
                                                {
                                                    xtw.WriteValue(flights[0].planned_arrival_time);
                                                }
                                                xtw.WriteEndElement();

                                                xtw.WriteStartElement("departure_day");
                                                {
                                                    xtw.WriteValue(flights[0].departure_dayOfWeek);
                                                }
                                                xtw.WriteEndElement();

                                                xtw.WriteStartElement("arrival_day");
                                                {
                                                    xtw.WriteValue(flights[0].arrival_dayOfWeek);
                                                }
                                                xtw.WriteEndElement();

                                                xtw.WriteStartElement("booking_class_rcd");
                                                {
                                                    xtw.WriteValue(flights[0].booking_class_rcd);
                                                }
                                                xtw.WriteEndElement();
                                                xtw.WriteStartElement("currency_rcd");
                                                {
                                                    xtw.WriteValue(flights[0].currency_rcd);
                                                }
                                                xtw.WriteEndElement();

                                                //Transit information
                                                if (flights.Count > 1)
                                                {
                                                    xtw.WriteStartElement("transit_airline_rcd");
                                                    {
                                                        xtw.WriteValue(flights[1].airline_rcd);
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("transit_flight_number");
                                                    {
                                                        xtw.WriteValue(flights[1].flight_number);
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("transit_flight_id");
                                                    {
                                                        xtw.WriteValue(flights[1].flight_id.ToString());
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("transit_departure_date");
                                                    {
                                                        xtw.WriteValue(string.Format("{0:yyyyMMdd}", flights[1].departure_date));
                                                    }
                                                    xtw.WriteEndElement();

                                                    xtw.WriteStartElement("transit_planned_departure_time");
                                                    {
                                                        xtw.WriteValue(flights[1].planned_departure_time);
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("transit_planned_arrival_time");
                                                    {
                                                        xtw.WriteValue(flights[1].planned_arrival_time);
                                                    }
                                                    xtw.WriteEndElement();

                                                    xtw.WriteStartElement("transit_arrival_date");
                                                    {
                                                        xtw.WriteValue(string.Format("{0:yyyyMMdd}", flights[1].arrival_date));
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("transit_departure_day");
                                                    {
                                                        xtw.WriteValue(flights[1].departure_dayOfWeek);
                                                    }
                                                    xtw.WriteEndElement();

                                                    xtw.WriteStartElement("transit_arrival_day");
                                                    {
                                                        xtw.WriteValue(flights[1].arrival_dayOfWeek);
                                                    }
                                                    xtw.WriteEndElement();


                                                    xtw.WriteStartElement("transit_airport_rcd");
                                                    {
                                                        xtw.WriteValue(flights[1].origin_rcd);
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("transit_fare_id");
                                                    {
                                                        xtw.WriteValue(flights[1].fare_id.ToString());
                                                    }
                                                    xtw.WriteEndElement();
                                                }
                                            }
                                        }
                                        xtw.WriteEndElement();


                                        //Add passenger type information
                                        Passengers objPassengers = new Passengers();
                                        Passenger p = null;
                                        //Add Adule
                                        for (int i = 0; i < objUv.Adult; i++)
                                        {
                                            p = new Passenger();
                                            p.passenger_id = Guid.NewGuid();
                                            p.passenger_type_rcd = "ADULT";
                                            objPassengers.Add(p);
                                        }
                                        //Add Child
                                        for (int i = 0; i < objUv.Child; i++)
                                        {
                                            p = new Passenger();
                                            p.passenger_id = Guid.NewGuid();
                                            p.passenger_type_rcd = "CHD";
                                            objPassengers.Add(p);
                                        }
                                        //Add Infant
                                        for (int i = 0; i < objUv.Infant; i++)
                                        {
                                            p = new Passenger();
                                            p.passenger_id = Guid.NewGuid();
                                            p.passenger_type_rcd = "INF";
                                            objPassengers.Add(p);
                                        }
                                        strResult = flights.FlightSummary(objPassengers,
                                                                        objUv.Agency_Code,
                                                                        Classes.Language.CurrentCode().ToUpper(),
                                                                        objUv.SearchCurrencyRcd,
                                                                        false);

                                        if (strResult.Length > 0)
                                        {
                                            XmlReaderSettings xmlSetting = new XmlReaderSettings();
                                            xmlSetting.IgnoreWhitespace = true;

                                            using (XmlReader reader = XmlReader.Create(new StringReader(strResult), xmlSetting))
                                            {
                                                while (!reader.EOF)
                                                {
                                                    if (reader.NodeType == XmlNodeType.Element && (reader.Name.Equals("Mapping") | reader.Name.Equals("Tax")))
                                                    {
                                                        xtw.WriteNode(reader, false);
                                                    }
                                                    else
                                                    {
                                                        reader.Read();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    xtw.WriteEndElement(); // End Booking Element
                                }
                            } //End xml gerenate element.

                            //Render HTML.
                            System.Xml.Xsl.XslTransform objTransform = null;
                            System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                            objTransform = objXsl.GetXSLDocument("QuoteSummary");

                            response.Success = true;
                            response.Code = "000";
                            response.Message = "SUCCESS";
                            response.HTML = objLi.RenderHtml(objTransform, objArgument, stb.ToString());
                        }
                        else
                        {
                            response.Success = false;
                            response.Code = "104";
                            response.Message = "Booking Session Not found.";
                        }

                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetSessionQuoteSummary()
        {

            ServiceResponse response = new ServiceResponse();
            Classes.Helper objHp = new Classes.Helper();
            Itinerary itinerary = B2CSession.Itinerary;
            Passengers passengers = B2CSession.Passengers;
            Quotes quotes = B2CSession.Quotes;
            Fees fees = B2CSession.Fees;
            Mappings mappings = B2CSession.Mappings;
            Services services = B2CSession.Services;
            Taxes taxes = B2CSession.Taxes;
            B2CVariable objUv = B2CSession.Variable;
            Library objLi = new Library();
            System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
            //  Show fare summary
            if (itinerary != null)
            {
                using (StringWriter stw = new StringWriter())
                {
                    using (XmlWriter xtw = XmlWriter.Create(stw))
                    {
                        xtw.WriteStartElement("Booking");
                        {
                            xtw.WriteStartElement("setting");
                            {
                                xtw.WriteStartElement("booking_step");
                                {
                                    xtw.WriteValue(objUv.CurrentStep);
                                }
                                xtw.WriteEndElement();
                            }
                            xtw.WriteEndElement();

                            objLi.BuiltBookingXml(xtw, null, itinerary, passengers, quotes, fees, mappings, null, null, null, taxes);
                        }
                        xtw.WriteEndElement();
                    }
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    objTransform = objHp.GetXSLDocument("FareSummary");

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.RenderHtml(objTransform, objArgument, stw.ToString());
                }
            }
            else
            {
                //Session is timeout.
                response.Success = false;
                response.Code = "002";
                response.Message = "SESSION TIMEOUT";
            }
            return response;
        }
        protected ServiceResponse BaseShowFlightSummary()
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                B2CVariable objUv = B2CSession.Variable;
                objUv.CurrentStep = 3;
                string strHttp = HttpHelper.LoadSecurePage(B2CSession.Client, objUv.CurrentStep);

                response.Success = true;
                if (strHttp == "https")
                {
                    response.Code = "https";
                    response.Message = "Redirect to https";
                }
                else if (strHttp == "http")
                {
                    response.Code = "http";
                    response.Message = "Redirect to http";
                }
                else
                {
                    Library objLi = new Library();
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.GenerateControlString("UserControls/FlightSummary.ascx", string.Empty); ;
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetPassengerDetail()
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                ServiceResponse response = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;
                if (objUv == null || objHp.SessionTimeout() == true)
                {
                    response.Success = false;
                    response.Code = "002";
                    response.Message = "SESSION TIMEOUT";
                }
                else
                {
                    objUv.CurrentStep = 4;
                    string strHttp = HttpHelper.LoadSecurePage(B2CSession.Client, objUv.CurrentStep);
                    response.Success = true;
                    if (strHttp == "https")
                    {
                        response.Code = "https";
                        response.Message = "Redirect to HTTPS";
                    }
                    else if (strHttp == "http")
                    {
                        response.Code = "http";
                        response.Message = "Redirect to HTTP";
                    }
                    else
                    {
                        Library li = new Library();
                        response.Code = "000";
                        response.Message = "SUCCESS";
                        response.HTML = li.GenerateControlString("UserControls/PassengerDetail.ascx", string.Empty); ;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        protected ServiceResponse GetPaymentPage()
        {
            try
            {
                ServiceResponse respose = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;

                //Generate interface.
                objUv.CurrentStep = 5;
                if (HttpHelper.LoadSecurePage(B2CSession.Client, objUv.CurrentStep) == "https")
                {
                    B2CSession.Https = true;
                    respose.Success = true;
                    respose.Code = "https";
                    respose.Message = "Redirect to https.";

                }
                else
                {
                    Library li = new Library();

                    respose.Success = true;
                    respose.Code = "000";
                    respose.Message = "SUCCESS";
                    respose.HTML = li.GenerateControlString("UserControls/Payment.ascx", string.Empty);
                }

                return respose;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse FillValidatePassengerData(Clients profilePassengers)
        {
            try
            {
                ServiceResponse respose = null;
                Passengers pasengers = B2CSession.Passengers;
                StringBuilder stbErr = new StringBuilder();

                if (profilePassengers.Count > 0)
                {
                    //Fill passenger profile.
                    for (int j = 0; j < pasengers.Count; j++)
                    {
                        for (int i = 0; i < profilePassengers.Count; i++)
                        {
                            if (profilePassengers[i].client_number.Equals(pasengers[j].client_number) &
                                profilePassengers[i].title_rcd.ToUpper().Equals(pasengers[j].title_rcd.ToUpper()) &
                                profilePassengers[i].firstname.ToUpper().Equals(pasengers[j].firstname.ToUpper()) &
                                profilePassengers[i].lastname.ToUpper().Equals(pasengers[j].lastname.ToUpper()))
                            {
                                if (profilePassengers[i].client_profile_id.Equals(Guid.Empty) == false)
                                {
                                    pasengers[j].client_profile_id = profilePassengers[i].client_profile_id;
                                    pasengers[j].passenger_profile_id = profilePassengers[i].passenger_profile_id;

                                    //------------------------------------------------------------------------------------//
                                    pasengers[j].firstname = profilePassengers[i].firstname;
                                    pasengers[j].lastname = profilePassengers[i].lastname;
                                    pasengers[j].title_rcd = profilePassengers[i].title_rcd;
                                    pasengers[j].gender_type_rcd = profilePassengers[i].gender_type_rcd;
                                    pasengers[j].document_type_rcd = profilePassengers[i].document_type_rcd;
                                    pasengers[j].nationality_rcd = profilePassengers[i].nationality_rcd;


                                    pasengers[j].passport_number = profilePassengers[i].passport_number;
                                    pasengers[j].passport_issue_place = profilePassengers[i].passport_issue_place;
                                    pasengers[j].vip_flag = profilePassengers[i].vip_flag;
                                    pasengers[j].passport_issue_date = profilePassengers[i].passport_issue_date;
                                    pasengers[j].passport_expiry_date = profilePassengers[i].passport_expiry_date;
                                    pasengers[j].date_of_birth = profilePassengers[i].date_of_birth;
                                    pasengers[j].passenger_weight = profilePassengers[i].passenger_weight;
                                    pasengers[j].passport_birth_place = profilePassengers[i].passport_birth_place;
                                    pasengers[j].member_level_rcd = profilePassengers[i].member_level_rcd;
                                }
                                else
                                {
                                    stbErr.Append(j + "|");
                                }

                                break;
                            }
                        }
                    }
                }

                if (stbErr.Length > 0)
                {
                    respose = new ServiceResponse();
                    respose.Success = false;
                    respose.Code = "100";
                    respose.Message = "FAILED CLIENT PROFILE VALIDATION";
                    respose.HTML = "{100}|" + stbErr.ToString();
                }
                else
                {
                    respose = GetPaymentPage();
                }

                return respose;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseShowSpecialService(string strPassengerId)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                Library objLi = new Library();
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;

                if (bookingHeader != null)
                {
                    XElement xDoc;

                    string language_rcd = Classes.Language.CurrentCode().ToUpper();

                    XElement booking = new XElement("Booking",
                        new XElement("setting",
                        new XElement("language_rcd", language_rcd)));

                    if (itinerary != null && itinerary.Count > 0)
                    {
                        xDoc = XElement.Parse(XmlHelper.Serialize(itinerary, false));
                        if (xDoc != null)
                        {
                            foreach (XElement x in xDoc.XPathSelectElements("FlightSegment"))
                                booking.Add(x);
                        }
                    }

                    if (strPassengerId.Trim() != "")
                    {

                        if (mappings != null && mappings.Count > 0)
                        {
                            xDoc = XElement.Parse(XmlHelper.Serialize(mappings, false));
                            if (xDoc != null)
                            {
                                foreach (XElement x in xDoc.XPathSelectElements("Mapping[passenger_id = '" + strPassengerId + "']"))
                                    booking.Add(x);
                            }
                        }

                        if (services != null && services.Count > 0)
                        {
                            xDoc = XElement.Parse(XmlHelper.Serialize(services, false));
                            if (xDoc != null)
                            {
                                foreach (XElement x in xDoc.XPathSelectElements("Service[passenger_id = '" + strPassengerId + "']"))
                                    booking.Add(x);
                            }
                        }
                    }
                    else
                    {
                        if (mappings != null && mappings.Count > 0)
                        {
                            xDoc = XElement.Parse(XmlHelper.Serialize(mappings, false));
                            if (xDoc != null)
                            {
                                foreach (XElement x in xDoc.XPathSelectElements("Mapping[passenger_type_rcd != 'INF']"))
                                    booking.Add(x);
                            }
                        }

                        if (services != null && services.Count > 0)
                        {
                            xDoc = XElement.Parse(XmlHelper.Serialize(services, false));
                            if (xDoc != null)
                            {
                                foreach (XElement x in xDoc.XPathSelectElements("Service"))
                                    booking.Add(x);
                            }
                        }
                    }

                    //Get SSR Information           
                    string strSsrGroup = ConfigurationHelper.ToString("SsrGroup");
                    if (string.IsNullOrEmpty(strSsrGroup) == false)
                    {
                        Fees objFees = new Fees();
                        Services objServices = CacheHelper.CacheSpecialServiceRef();
                        Agents objAgents = B2CSession.Agents;
                        Routes routesDes = CacheHelper.CacheDestination();

                        objFees.objService = B2CSession.AgentService;
                        booking.Add(XElement.Parse(objFees.SegmentFee(objAgents[0],
                                routesDes,
                                bookingHeader,
                                mappings,
                                strSsrGroup.Split(','),
                                objServices,
                                0,
                                0,
                                Classes.Language.CurrentCode().ToUpper(),
                                true,
                                false)));

                        objFees.objService = null;
                    }

                    Classes.Helper objHp = new Classes.Helper();
                    //Render Special service xsl.
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);


                    objTransform = objHp.GetXSLDocument("SpecialService");

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.RenderHtml(objTransform, objArgument, booking.ToString());
                }
                else
                {
                    response.Success = false;
                    response.Code = "201";
                    response.Message = "Booking object is null.";
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseShowAdditionalFee()
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                Library objLi = new Library();
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers pax = B2CSession.Passengers;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Fees fees = B2CSession.Fees;
                fees.objService = B2CSession.AgentService;

                if (bookingHeader != null)
                {
                    XElement xDoc;

                    string language_rcd = Classes.Language.CurrentCode().ToUpper();

                    XElement booking = new XElement("Booking",
                        new XElement("setting",
                        new XElement("language_rcd", language_rcd),
                        new XElement("agency_code", bookingHeader.agency_code)));

                    if (itinerary != null && itinerary.Count > 0)
                    {
                        xDoc = XElement.Parse(XmlHelper.Serialize(itinerary, false));
                        if (xDoc != null)
                        {
                            foreach (XElement x in xDoc.XPathSelectElements("FlightSegment"))
                                booking.Add(x);
                        }

                        if (fees != null)
                        {
                            List<Fee> fee_list = fees.GetFeesByCode("KLIMA");
                            fees.RemoveEmptyFees();
                            for (int i = 0; i < itinerary.Count; i++)
                            {
                                FlightSegment segment = itinerary[i];
                                List<Mapping> mapping = mappings.GetPassengerBySegmentId(segment.booking_segment_id);
                                string fareCode = mapping != null && mapping.Count > 0 ? mapping[0].fare_code : "";
                                Fee fee = fee_list.Find(f => f.booking_segment_id == segment.booking_segment_id);
                                if (fee == null)
                                    fees.GetFeeDefinition("KLIMA", bookingHeader.currency_rcd, bookingHeader.agency_code, segment.booking_class_rcd, fareCode, segment.origin_rcd, segment.destination_rcd, "", DateTime.MinValue, Classes.Language.CurrentCode(), false);
                            }
                        }

                        if (fees != null && fees.Count > 0)
                        {
                            xDoc = XElement.Parse(XmlHelper.Serialize(fees, false));
                            if (xDoc != null)
                            {
                                foreach (XElement x in xDoc.XPathSelectElements("Fee"))
                                    booking.Add(x);
                            }
                            fees.RemoveFeesByCode("KLIMA");
                        }
                    }

                    if (pax != null && pax.Count > 0)
                    {
                        xDoc = XElement.Parse(XmlHelper.Serialize(pax, false));
                        if (xDoc != null)
                        {
                            foreach (XElement x in xDoc.XPathSelectElements("Passenger"))
                                booking.Add(x);
                        }
                    }

                    if (mappings != null && mappings.Count > 0)
                    {
                        xDoc = XElement.Parse(XmlHelper.Serialize(mappings, false));
                        if (xDoc != null)
                        {
                            foreach (XElement x in xDoc.XPathSelectElements("Mapping[passenger_type_rcd != 'INF']"))
                                booking.Add(x);
                        }
                    }

                    if (services != null && services.Count > 0)
                    {
                        xDoc = XElement.Parse(XmlHelper.Serialize(services, false));
                        if (xDoc != null)
                        {
                            foreach (XElement x in xDoc.XPathSelectElements("Service"))
                                booking.Add(x);
                        }
                    }

                    Classes.Helper objHp = new Classes.Helper();
                    //Render Special service xsl.
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);


                    objTransform = objHp.GetXSLDocument("AdditionalFee");

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.RenderHtml(objTransform, objArgument, booking.ToString());
                }
                else
                {
                    response.Success = false;
                    response.Code = "201";
                    response.Message = "Booking object is null.";
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Booking Service Helper
        protected void UpdateSeatInfoFromTemp(Mappings mappings)
        {
            foreach (Mapping mp in mappings)
            {
                if (mp.SeatConfirm == false)
                {
                    mp.seat_row = mp.temp_seat_row;
                    mp.seat_column = mp.temp_seat_column;
                    mp.seat_number = mp.temp_seat_number;
                    mp.seat_fee_rcd = mp.temp_seat_fee_rcd;
                }
            }

        }
        #endregion
    }
}
