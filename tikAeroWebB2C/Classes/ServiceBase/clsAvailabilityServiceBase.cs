﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Configuration;
using tikAeroB2C.Classes;
using tikAeroB2C.Classes.ServiceMessage;
using tikSystem.Web.Library;

namespace tikAeroB2C.Base
{
    public abstract partial class ServiceBase : System.Web.Services.WebService
    {
        #region Availability Service
        protected ServiceResponse GetAvailability(string originRcd,
                                                    string destinationRcd,
                                                    string currencyRcd,
                                                    string ipAddress,
                                                    string departureDate,
                                                    string arrivalDate,
                                                    string flightOnly,
                                                    string searchType,
                                                    string oneWayFlight,
                                                    short numberOfAdult,
                                                    short numberOfChild,
                                                    short numberOfInfant,
                                                    short numberOfOther,
                                                    string otherPassengerType,
                                                    string boardingClass,
                                                    string promoCode)
        {
            try
            {
                Library objLi = new Library();
                ServiceResponse objResponse = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;
                if (string.IsNullOrEmpty(ipAddress))
                {
                    ipAddress = DataHelper.GetClientIpAddress();
                }

                if (objUv == null)
                {
                    objUv = new B2CVariable();
                    B2CSession.Variable = objUv;
                }

                //Fill Airport name.
                Routes routes = CacheHelper.CacheOrigin();
                objUv.OriginName = objLi.FindOriginName(routes, originRcd);

                routes = CacheHelper.CacheDestination();
                objUv.DestinationName = objLi.FindDestinationName(routes, destinationRcd);

                if (string.IsNullOrEmpty(originRcd) == true ||
                    string.IsNullOrEmpty(destinationRcd) == true ||
                    string.IsNullOrEmpty(departureDate) == true ||
                    string.IsNullOrEmpty(arrivalDate) == true ||
                    string.IsNullOrEmpty(flightOnly) == true ||
                    string.IsNullOrEmpty(searchType) == true ||
                    string.IsNullOrEmpty(objUv.OriginName) == true ||
                    string.IsNullOrEmpty(objUv.DestinationName) == true ||
                    DataHelper.DateValid(departureDate) == false ||
                    Convert.ToInt32(arrivalDate) < Convert.ToInt32(string.Format("{0:yyyyMMdd}", DateTime.Now)) ||
                    (oneWayFlight.ToLower().Equals("false") && DataHelper.DateValid(arrivalDate) == false) ||
                    (arrivalDate.Length > 0 && (Convert.ToInt32(departureDate) > Convert.ToInt32(arrivalDate)) && oneWayFlight.ToLower().Equals("false")) ||
                    routes == null)
                {
                    //Invalid Required Parameter.
                    objResponse.Success = false;
                    objResponse.Code = "003";
                    objResponse.Message = "Invalid Required Parameter";
                }
                else
                {
                    if (string.IsNullOrEmpty(currencyRcd) == false)
                    { objUv.SearchCurrencyRcd = currencyRcd; }
                    else
                    {
                        Agent objAgent = CacheHelper.CacheAgency(objUv.Agency_Code);
                        if (objAgent != null)
                        {
                            if (objAgent.use_origin_currency_flag == 1)
                            {
                                objUv.SearchCurrencyRcd = routes.GetCurrencyCode(originRcd, destinationRcd);
                            }
                            else
                            {
                                objUv.SearchCurrencyRcd = objAgent.currency_rcd;
                            }
                        }
                        else
                        {
                            objUv.SearchCurrencyRcd = routes.GetCurrencyCode(originRcd, destinationRcd);
                        }

                    }

                    //If currency still not found then take from origin.
                    if (objUv.SearchCurrencyRcd == string.Empty)
                    {
                        objUv.SearchCurrencyRcd = CacheHelper.CacheOrigin().GetCurrencyCode(originRcd);
                    }

                    objUv.DayRange = routes.GetDayRange(originRcd, destinationRcd);

                    objUv.Adult = numberOfAdult;
                    objUv.Child = numberOfChild;
                    objUv.Infant = numberOfInfant;
                    objUv.BoardingClass = boardingClass;
                    objUv.PromoCode = promoCode;
                    objUv.SearchType = searchType;
                    objUv.OriginRcd = originRcd;
                    objUv.DestinationRcd = destinationRcd;
                    objUv.DepartureDate = departureDate;
                    objUv.OneWay = Convert.ToBoolean(oneWayFlight);
                    objUv.Other = numberOfOther;
                    objUv.OtherPassengerType = otherPassengerType;
                    objUv.ip_address = ipAddress;

                    //Get Search Day range
                    objUv.DayRange = B2CSetting.SearchDayRange;
                    objUv.ReturnDate = objUv.OneWay == false ? arrivalDate : string.Empty;
                    objUv.SkipFareLogic = B2CSetting.SkipFareLogic;

                    //Load availability usercontrol with the search result.
                    objResponse.Success = true;
                    objResponse.Message = "SUCCESS";
                    objResponse.HTML = objLi.GenerateControlString("UserControls/Availability.ascx", string.Empty);
                }
                return objResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse GetLowFareFinderBeforeAvailability(string originRcd,
                                                                        string destinationRcd,
                                                                        string currencyRcd,
                                                                        string ipAddress,
                                                                        string departureDate,
                                                                        string arrivalDate,
                                                                        string flightOnly,
                                                                        string searchType,
                                                                        string oneWayFlight,
                                                                        short numberOfAdult,
                                                                        short numberOfChild,
                                                                        short numberOfInfant,
                                                                        short numberOfOther,
                                                                        string otherPassengerType,
                                                                        string boardingClass,
                                                                        string promoCode)
        {
            B2CVariable objUv = B2CSession.Variable;
            ServiceResponse objResponse;
            if (objUv != null)
            {
                Availabilities availability = new Availabilities(SecurityHelper.GenerateSessionlessToken());
                if (availability.LowFareFinderAllow(objUv.Agency_Code) == true)
                {
                    #region normal low fare finder
                    Library objLi = new Library();
                    objResponse = new ServiceResponse();
                    try
                    {
                        if (string.IsNullOrEmpty(ipAddress))
                        {
                            ipAddress = DataHelper.GetClientIpAddress();
                        }

                        if (objUv == null)
                        {
                            objUv = new B2CVariable();
                            B2CSession.Variable = objUv;
                        }

                        //Fill destination information.
                        Routes routes = CacheHelper.CacheDestination();
                        objUv.DestinationName = objLi.FindDestinationName(routes, destinationRcd);

                        if (string.IsNullOrEmpty(currencyRcd) == false)
                        { objUv.SearchCurrencyRcd = currencyRcd; }
                        else
                        {
                            objUv.SearchCurrencyRcd = routes.GetCurrencyCode(originRcd, destinationRcd);
                        }
                        routes = null;

                        //Fill Origin information.
                        routes = CacheHelper.CacheOrigin();
                        objUv.OriginName = objLi.FindOriginName(routes, originRcd);

                        //If currency still not found then take from origin.
                        if (string.IsNullOrEmpty(objUv.SearchCurrencyRcd))
                        {
                            objUv.SearchCurrencyRcd = routes.GetCurrencyCode(originRcd);
                        }

                        if (string.IsNullOrEmpty(originRcd) == true ||
                            string.IsNullOrEmpty(destinationRcd) == true ||
                            string.IsNullOrEmpty(departureDate) == true ||
                            string.IsNullOrEmpty(oneWayFlight) == true ||
                            string.IsNullOrEmpty(searchType) == true ||
                            string.IsNullOrEmpty(objUv.OriginName) == true ||
                            string.IsNullOrEmpty(objUv.DestinationName) == true ||
                            DataHelper.DateValid(departureDate) == false ||
                            Convert.ToInt32(departureDate) < Convert.ToInt32(string.Format("{0:yyyyMMdd}", DateTime.Now)) ||
                            (oneWayFlight.ToLower().Equals("false") && DataHelper.DateValid(arrivalDate) == false) ||
                            (arrivalDate.Length > 0 && (Convert.ToInt32(departureDate) > Convert.ToInt32(arrivalDate)) && oneWayFlight.ToLower().Equals("false")) && oneWayFlight.ToLower().Equals("false"))
                        {
                            objResponse.Success = false;
                            objResponse.Code = "102";
                            objResponse.Message = "Invalid Parameter";
                        }
                        else
                        {
                            objUv.Adult = numberOfAdult;
                            objUv.Child = numberOfChild;
                            objUv.Infant = numberOfInfant;
                            objUv.Other = numberOfOther;
                            objUv.OtherPassengerType = otherPassengerType;
                            objUv.BoardingClass = boardingClass;
                            objUv.PromoCode = promoCode;
                            objUv.SearchType = searchType;
                            objUv.OneWay = Convert.ToBoolean(oneWayFlight);
                            objUv.OriginRcd = originRcd;
                            objUv.DestinationRcd = destinationRcd;
                            objUv.DepartureDate = departureDate;
                            objUv.ip_address = ipAddress;

                            if (objUv.OneWay == false)
                            {
                                objUv.ReturnDate = arrivalDate;
                            }

                            objUv.SkipFareLogic = true;
                            objUv.CurrentStep = 12;

                            objResponse.Success = true;
                            objResponse.Code = "000";
                            objResponse.HTML = objLi.GenerateControlString("UserControls/LowFareFinder.ascx", string.Empty);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    return objResponse;
                    #endregion
                }
                else
                {
                    return GetAvailability(originRcd,
                                            destinationRcd,
                                            currencyRcd,
                                            ipAddress,
                                            departureDate,
                                            arrivalDate,
                                            flightOnly,
                                            searchType,
                                            oneWayFlight,
                                            numberOfAdult,
                                            numberOfChild,
                                            numberOfInfant,
                                            numberOfOther,
                                            otherPassengerType,
                                            boardingClass,
                                            promoCode);
                }
            }
            else
            {
                objResponse = new ServiceResponse();
                objResponse.Success = false;
                objResponse.Code = "002";
                objResponse.Message = "Session is timeout.";

                return objResponse;
            }
        }
        protected ServiceResponse BaseAvailabilityGetSession()
        {

            try
            {
                ServiceResponse objResponse = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;
                if (objUv != null)
                {
                    //Get Search Day range
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SearchDayRange"]))
                    {
                        objUv.DayRange = B2CSetting.SearchDayRange;
                    }
                    else
                    {
                        Routes routes = CacheHelper.CacheDestination();
                        objUv.DayRange = routes.GetDayRange(objUv.OriginRcd, objUv.DestinationRcd);
                    }
                    objUv.CurrentStep = 2;
                    string strHttp = HttpHelper.LoadSecurePage(B2CSession.Client, objUv.CurrentStep);
                    //Redirect to https.
                    objResponse.Success = true;
                    objResponse.Code = strHttp;

                    if (strHttp == "http")
                    {
                        objResponse.Message = "SUCCESS WITH REDIRECT";
                    }
                    else
                    {
                        Library objLi = new Library();
                        objResponse.Message = "SUCCESS";
                        objResponse.HTML = objLi.GenerateControlString("UserControls/Availability.ascx", string.Empty);
                    }
                }
                else
                {
                    objResponse.Success = false;
                    objResponse.Code = "002";
                    objResponse.Message = "Session is timeout";
                }
                return objResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseSearchSingleFlight(string flightDate, bool OutWard)
        {
            try
            {
                ServiceResponse objResponse = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;
                Library objLi = new Library();

                if (objUv == null)
                {
                    //Session is timeout.
                    objResponse.Success = false;
                    objResponse.Code = "002";
                    objResponse.Message = "Session is timeout";
                }
                else if (flightDate.Trim().Length == 0 ||
                    string.IsNullOrEmpty(objUv.OriginName) == true ||
                    string.IsNullOrEmpty(objUv.DestinationName) == true ||
                    Convert.ToInt32(flightDate) < Convert.ToInt32(string.Format("{0:yyyyMMdd}", DateTime.Now)))
                {
                    objResponse.Success = false;
                    objResponse.Code = "003";
                    objResponse.Message = "Invalid parameter";
                }
                else
                {
                    if (OutWard == true)
                    {
                        objUv.DepartureDate = flightDate;
                        if (objUv.OneWay == false)
                        {
                            if (Convert.ToInt32(flightDate) > Convert.ToInt32(objUv.ReturnDate))
                            {
                                objUv.ReturnDate = flightDate;
                            }
                        }
                    }
                    else
                    {
                        objUv.ReturnDate = flightDate;
                    }

                    objResponse.Success = true;
                    objResponse.Code = "000";
                    objResponse.HTML = objLi.GenerateControlString("UserControls/Availability.ascx", string.Empty);
                }

                return objResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseSearchSingleFlightSameDay(string flightDate)
        {
            try
            {
                ServiceResponse objResponse = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;
                if (objUv != null)
                {
                    if (string.IsNullOrEmpty(flightDate) || flightDate.Trim().Length == 0)
                    {
                        objResponse.Success = false;
                        objResponse.Code = "003";
                        objResponse.Message = "INVALID PARAMETER";
                    }
                    else
                    {
                        Library objLi = new Library();

                        objUv.DepartureDate = flightDate;
                        objUv.ReturnDate = flightDate;

                        objResponse.Success = true;
                        objResponse.Code = "000";
                        objResponse.HTML = objLi.GenerateControlString("UserControls/Availability.ascx", string.Empty);
                    }
                }
                else
                {
                    objResponse.Success = false;
                    objResponse.Code = "002";
                    objResponse.Message = "Session is timeout.";
                }

                return objResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseGetLowFareFinder(string strFromAirport, string strToAirport, string strFlightDate, string strReturnDate)
        {
            try
            {
                ServiceResponse objResponse = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;
                if (string.IsNullOrEmpty(strFromAirport) == true ||
                    string.IsNullOrEmpty(strToAirport) == true ||
                    string.IsNullOrEmpty(strFlightDate) == true ||
                    objUv == null ||
                    string.IsNullOrEmpty(objUv.OriginName) == true ||
                    string.IsNullOrEmpty(objUv.DestinationName) == true ||
                    Convert.ToInt32(strFlightDate) < Convert.ToInt32(string.Format("{0:yyyyMMdd}", DateTime.Now)) ||
                    (strReturnDate.Length > 0 && (Convert.ToInt32(strFlightDate) > Convert.ToInt32(strReturnDate)) && !objUv.OneWay))
                {
                    objResponse.Success = false;
                    objResponse.Code = "102";
                    objResponse.Message = "INVALID PARAMETER";
                }
                else
                {
                    Routes routes = CacheHelper.CacheOrigin();
                    string strResult = string.Empty;

                    if (strFromAirport == "")
                        strFromAirport = objUv.OriginRcd;
                    if (strToAirport == "")
                        strToAirport = objUv.DestinationRcd;

                    objUv.SkipFareLogic = true;
                    objUv.CurrentStep = 12;
                    if (B2CSetting.LowFareCalendarMode && B2CSetting.LowFareFinderRange == -1)
                    {
                        DateTime dtValue = DateTime.MinValue;
                        objUv.DayRange = -1;
                        if (string.IsNullOrEmpty(strFlightDate) == false)
                        {
                            //Take only yearMonth
                            dtValue = DataHelper.ParseDate(strFlightDate);
                            objUv.DepartureDate = string.Format("{0:yyyyMM}", dtValue) + "01";
                        }
                        if (string.IsNullOrEmpty(strReturnDate) == false)
                        {
                            //Take only yearMonth
                            dtValue = DataHelper.ParseDate(strReturnDate);
                            objUv.ReturnDate = string.Format("{0:yyyyMM}", dtValue) + "01";
                        }
                    }

                    Library objLi = new Library();
                    objResponse.Success = true;
                    objResponse.Code = "000";
                    objResponse.Message = "SUCCESS";
                    objResponse.HTML = objLi.GenerateControlString("UserControls/LowFareFinder.ascx", string.Empty); ;
                }
                return objResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseGetAvailabilityFromLowFareFinder(string routeInformation,
                                                                       string dateOutward,
                                                                       string dateReturn,
                                                                       bool bRouteDayRange,
                                                                       string outwardFare,
                                                                       string returnFare,
                                                                       string strOutFlight,
                                                                       string strRetFlight)
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                ServiceResponse response = new ServiceResponse();

                if (objUv == null ||
                    string.IsNullOrEmpty(objUv.OriginRcd) ||
                    string.IsNullOrEmpty(objUv.OriginName) ||
                    string.IsNullOrEmpty(objUv.DestinationRcd))
                {
                    response.Success = false;
                    response.Code = "201";
                    response.Message = "Booking object is null.(Session time out).";
                }
                else if (string.IsNullOrEmpty(routeInformation) == true ||
                        (string.IsNullOrEmpty(dateOutward) == true || dateOutward.Length != 8) ||
                        objUv.OneWay == false && (string.IsNullOrEmpty(dateReturn) || dateReturn.Length != 8))
                {
                    response.Success = false;
                    response.Code = "102";
                    response.Message = "Low fare finder input not found.";
                }
                else
                {
                    string result = string.Empty;
                    StringBuilder parameter = new StringBuilder();
                    Taxes taxes = B2CSession.Taxes;

                    Library objLi = new Library();

                    if (routeInformation.Length == 0 & objUv != null)
                    { parameter.Append(objUv.OriginRcd + "|" + objUv.OriginName + "|" + objUv.DestinationRcd + "|0"); }
                    else
                    { parameter.Append(routeInformation); }

                    parameter.Append("|");
                    parameter.Append(dateOutward);
                    parameter.Append("|");
                    parameter.Append(dateReturn);
                    parameter.Append("|");
                    if (dateReturn.Length > 0)
                    { parameter.Append("false"); }
                    else
                    { parameter.Append("true"); }
                    parameter.Append("|");
                    parameter.Append("0"); //Apply Farelogic
                    parameter.Append("|");
                    parameter.Append(outwardFare);
                    parameter.Append("|");
                    parameter.Append(returnFare);
                    parameter.Append("|");
                    parameter.Append(strOutFlight);
                    parameter.Append("|");
                    parameter.Append(strRetFlight);

                    if (bRouteDayRange == false)
                    {
                        //Get Search Day range
                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SearchDayRange"]))
                        {
                            objUv.DayRange = B2CSetting.SearchDayRange;
                        }
                        else
                        {
                            Routes routes = CacheHelper.CacheDestination();
                            objUv.DayRange = routes.GetDayRange(objUv.OriginRcd, objUv.DestinationRcd);
                        }
                    }

                    if (dateOutward != "0")
                    {
                        objUv.DepartureDate = dateOutward;
                    }
                    if (dateReturn != "0")
                    {
                        objUv.ReturnDate = dateReturn;
                    }

                    objUv.SkipFareLogic = true;
                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.GenerateControlString("UserControls/Availability.ascx", parameter.ToString()) + "{}";
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseGetFlightTime(string originRcd, string destinationRcd, string strDate)
        {
            try
            {
                string result = string.Empty;

                Helper objXsl = new Helper();
                B2CVariable objUv = B2CSession.Variable;
                Library objLi = new Library();
                ServiceResponse response = new ServiceResponse();

                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                if (objUv == null || objXsl.SessionTimeout() == true)
                {
                    response.Success = false;
                    response.Code = "002";
                    response.Message = "SESSION TIMEOUT";
                }
                else
                {
                    //Get Flight availability. Retrive only flight information
                    Flights objFlights = new Flights();
                    string strResult = objFlights.GetFlightTimeLowestFare(objUv.Agency_Code,
                                                                          objUv.BoardingClass,
                                                                          objUv.DepartureDate,
                                                                          objUv.OriginRcd,
                                                                          objUv.DestinationRcd,
                                                                          objUv.SearchCurrencyRcd,
                                                                          objUv.PromoCode,
                                                                          objUv.SearchType,
                                                                          objUv.DayRange,
                                                                          objUv.Adult,
                                                                          objUv.Child,
                                                                          objUv.Infant,
                                                                          objUv.Other,
                                                                          1,
                                                                          objUv.OtherPassengerType,
                                                                          SecurityHelper.GenerateSessionlessToken());

                    //Extract Availability xml
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    objTransform = objXsl.GetXSLDocument("LowFareFinderFlightTime");

                    //Get Time html.
                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.RenderHtml(objTransform, objArgument, strResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetLowFareFinderMonth(string strFromAirport,
                                                        string strToAirport,
                                                        string departMonth,
                                                        string returnMonth,
                                                        string iOneWay,
                                                        short iAdult,
                                                        short iChild,
                                                        short iInfant,
                                                        string BoardingClass,
                                                        string CurrencyCode,
                                                        string strPromoCode,
                                                        string SearchType,
                                                        short iOther,
                                                        string otherType,
                                                        string strIpAddress,
                                                        string strCurrentDate)
        {

            Library objLi = new Library();
            int iReturnMonth = 0;
            int iReturnYear = 0;
            try
            {
                ServiceResponse response;
                B2CVariable objUv = B2CSession.Variable;
                if (objUv != null)
                {
                    Availabilities availability = new Availabilities(SecurityHelper.GenerateSessionlessToken());
                    if (availability.LowFareFinderAllow(objUv.Agency_Code) == true)
                    {
                        #region Low Fare Finder
                        response = new ServiceResponse();
                        string ipAddress = string.Empty;

                        if (string.IsNullOrEmpty(strIpAddress))
                        {
                            ipAddress = DataHelper.GetClientIpAddress();
                        }
                        else
                        {
                            ipAddress = strIpAddress;
                        }


                        if (objUv == null)
                        {
                            objUv = new B2CVariable();
                            B2CSession.Variable = objUv;
                        }

                        //Fill Origin Airport Info.
                        Routes routes = CacheHelper.CacheDestination();
                        objUv.OriginName = objLi.FindOriginName(routes, strFromAirport);
                        if (CurrencyCode.Length > 0)
                        { objUv.SearchCurrencyRcd = CurrencyCode; }
                        else
                        {
                            objUv.SearchCurrencyRcd = routes.GetCurrencyCode(strFromAirport, strToAirport);
                        }
                        routes = null;
                        //Fill Destination Airport Info.
                        routes = CacheHelper.CacheDestination();
                        objUv.DestinationName = objLi.FindDestinationName(routes, strToAirport);

                        //Set to "-1" to tell the function to get the one month calendar.
                        objUv.DayRange = -1;
                        //Change Month Format.
                        if (string.IsNullOrEmpty(departMonth) == false && departMonth.Length == 8)
                        {
                            departMonth = departMonth.Substring(0, 6);
                            returnMonth = returnMonth.Substring(0, 6);
                        }

                        if (string.IsNullOrEmpty(strFromAirport) == true ||
                            string.IsNullOrEmpty(strToAirport) == true ||
                            string.IsNullOrEmpty(departMonth) ||
                            string.IsNullOrEmpty(iOneWay) == true ||
                            string.IsNullOrEmpty(SearchType) == true ||
                            string.IsNullOrEmpty(objUv.OriginName) == true ||
                            string.IsNullOrEmpty(objUv.DestinationName) == true ||
                            Convert.ToInt32(departMonth) < Convert.ToInt32(string.Format("{0:yyyyMM}", DateTime.Now)) ||
                            (iOneWay.ToLower().Equals("false") && string.IsNullOrEmpty(returnMonth) == true) ||
                            (string.IsNullOrEmpty(returnMonth) == false && (Convert.ToInt32(departMonth) > Convert.ToInt32(returnMonth)) && iOneWay.ToLower().Equals("false")) && iOneWay.ToLower().Equals("false"))
                        {
                            response.Success = false;
                            response.Code = "102";
                            response.Message = "Low Fare finder input not found.";
                        }
                        else
                        {
                            objUv.Adult = iAdult;
                            objUv.Child = iChild;
                            objUv.Infant = iInfant;
                            objUv.Other = iOther;
                            objUv.OtherPassengerType = otherType;
                            objUv.BoardingClass = BoardingClass;
                            objUv.PromoCode = strPromoCode;
                            objUv.SearchType = SearchType.ToUpper();
                            objUv.OneWay = Convert.ToBoolean(iOneWay);
                            objUv.OriginRcd = strFromAirport;
                            objUv.DestinationRcd = strToAirport;
                            objUv.DepartureDate = departMonth + "01";
                            objUv.ip_address = ipAddress;
                            B2CSession.CurrentClientDate = strCurrentDate;

                            if (objUv.OneWay == false && string.IsNullOrEmpty(returnMonth) == false && returnMonth.Length == 6)
                            {
                                iReturnMonth = Convert.ToInt16(returnMonth.Substring(0, 4));
                                iReturnYear = Convert.ToInt16(returnMonth.Substring(4, 2));
                                objUv.ReturnDate = returnMonth + "01";
                            }

                            objUv.SkipFareLogic = true;
                            objUv.CurrentStep = 12;

                            response.Success = true;
                            response.Code = "000";
                            response.Message = "SUCCESS";
                            response.HTML = objLi.GenerateControlString("UserControls/LowFareFinder.ascx", string.Empty);
                        }

                        return response;
                        #endregion
                    }
                    else
                    {
                        return GetAvailability(strFromAirport,
                                               strToAirport,
                                               CurrencyCode,
                                               strIpAddress,
                                               departMonth,
                                               returnMonth,
                                               "0",
                                               SearchType,
                                               iOneWay,
                                               iAdult,
                                               iChild,
                                               iInfant,
                                               iOther,
                                               otherType,
                                               BoardingClass,
                                               strPromoCode);
                    }
                }
                else
                {
                    response = new ServiceResponse();
                    response.Success = false;
                    response.Code = "002";
                    response.Message = "Session is timeout.";

                    return response;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseSearchLowFareSingleMonth(string strYearMonth, bool OutWard)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                ServiceResponse response = new ServiceResponse();

                B2CVariable objUv = B2CSession.Variable;
                Library objLi = new Library();

                if (objUv == null)
                {
                    response.Success = false;
                    response.Code = "002";
                    response.Message = "Session is timeout.";
                }
                else if (strYearMonth.Trim().Length == 0 ||
                        string.IsNullOrEmpty(objUv.OriginName) == true ||
                        string.IsNullOrEmpty(objUv.DestinationName) == true ||
                        Convert.ToInt32(strYearMonth) < Convert.ToInt32(string.Format("{0:yyyyMM}", DateTime.Now)))
                {
                    response.Success = false;
                    response.Code = "003";
                    response.Message = "INVALID PARAMETER.";
                }
                else
                {
                    if (OutWard == true)
                    {
                        objUv.DepartureDate = strYearMonth + "01";
                        if (objUv.OneWay == false)
                        {
                            if (Convert.ToInt32(objUv.DepartureDate) > Convert.ToInt32(objUv.ReturnDate))
                            {
                                objUv.ReturnDate = strYearMonth + "01";
                            }
                        }
                    }
                    else
                    {
                        objUv.ReturnDate = strYearMonth + "01";
                    }

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.GenerateControlString("UserControls/LowFareFinder.ascx", string.Empty); ;
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseShowAvailabilityPopup()
        {
            try
            {
                ServiceResponse respose = new ServiceResponse();
                Library objLi = new Library();

                respose.Success = true;
                respose.Code = "000";
                respose.Message = "SUCCESS";
                respose.HTML = objLi.GenerateControlString("UserControls/SearchAvailabilityPopup.ascx", string.Empty);

                return respose;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}