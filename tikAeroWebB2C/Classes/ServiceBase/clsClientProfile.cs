﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using tikSystem.Web.Library;
using tikAeroB2C.Classes;
using tikAeroB2C.Classes.ServiceMessage;

namespace tikAeroB2C.Base
{
    public abstract partial class ServiceBase : System.Web.Services.WebService
    {
        protected ServiceResponse BaseGetClient(string clientNumber, string strLastName, bool bShowAll)
        {
            string strResult = string.Empty;
            try
            {
                ServiceResponse response = new ServiceResponse();

                Clients objClient = new Clients();
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                if (bookingHeader != null)
                {
                    objClient.objService = B2CSession.AgentService;
                    System.Data.DataSet ds = objClient.GetClientPassenger(bookingHeader.booking_id.ToString(), string.Empty, clientNumber);


                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        if (strLastName.Length == 0)
                        {
                            strResult = ds.GetXml();
                        }
                        else
                        {
                            StringBuilder stb = new StringBuilder();
                            XPathDocument xmlDoc = new XPathDocument(new StringReader(ds.GetXml()));
                            XPathNavigator nv = xmlDoc.CreateNavigator();

                            if (bShowAll == false)
                            {
                                stb.Append("<TikAero>");
                                foreach (XPathNavigator n in nv.Select("TikAero/Passenger[lastname = '" + strLastName.ToUpper() + "']"))
                                {
                                    stb.Append(n.OuterXml);
                                }
                                stb.Append("</TikAero>");

                                strResult = stb.ToString();
                            }
                            else
                            {
                                if (nv.Select("TikAero/Passenger[lastname = '" + strLastName.ToUpper() + "']").Count > 0)
                                {
                                    strResult = ds.GetXml();
                                }
                            }
                        }
                    }
                    ds.Dispose();

                    Library objLi = new Library();
                    response.Success = true;
                    response.Code = "000";
                    response.Message = "MESSAGE";
                    response.HTML = objLi.GenerateControlString("UserControls/ClientProfile.ascx", strResult);
                }
                else
                {
                    response.Success = false;
                    response.Code = "002";
                    response.Message = "Session Timeout.";
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetClientProfile(string clientNumber)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                System.Web.Script.Serialization.JavaScriptSerializer objJSON = new System.Web.Script.Serialization.JavaScriptSerializer();
                Clients objClient = new Clients();

                //Read Client information from web service
                objClient.objService = B2CSession.AgentService;
                objClient.Read(string.Empty, clientNumber, string.Empty, false);

                //Serialize to JSON for used in javascript
                if (objClient.Count > 0)
                {
                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objJSON.Serialize(objClient);
                }
                else
                {
                    response.Success = false;
                    response.Code = "01";
                    response.Message = "Code 01 is client not found.";
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse ValidateAllClient(Clients clientPassenger)
        {
            try
            {
                ServiceResponse respose = new ServiceResponse();
                Passengers pasengers = B2CSession.Passengers;
                StringBuilder stbErr = new StringBuilder();
                bool bFoundProfile = false;
                if (clientPassenger.Count > 0)
                {
                    //Fill passenger profile.
                    for (int j = 0; j < pasengers.Count; j++)
                    {
                        bFoundProfile = false;
                        for (int i = 0; i < clientPassenger.Count; i++)
                        {
                            if (clientPassenger[i].client_number.Equals(pasengers[j].client_number) &
                                clientPassenger[i].firstname.ToUpper().Equals(pasengers[j].firstname.ToUpper()) &
                                clientPassenger[i].lastname.ToUpper().Equals(pasengers[j].lastname.ToUpper()))
                            {
                                bFoundProfile = true;
                                break;
                            }

                            //Set passenger number that are not found in the profile.
                            if (bFoundProfile == false)
                            {
                                stbErr.Append(j + "|");
                            }
                        }
                    }
                }

                if (stbErr.Length > 0)
                {
                    respose.Success = false;
                    respose.Code = "100";
                    respose.Message = "FAILED CLIENT PROFILE VALIDATION";
                    respose.HTML = stbErr.ToString();
                }
                else
                {
                    respose.Success = true;
                    respose.Code = "000";
                    respose.Message = "SUCCESS";
                }

                return respose;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}