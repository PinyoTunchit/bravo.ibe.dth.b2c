﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using tikAeroB2C.Classes;
using tikAeroB2C.Classes.ServiceMessage;
using tikSystem.Web.Library;
using System.Configuration;
using System.Linq;

namespace tikAeroB2C.Base
{
    public abstract partial class ServiceBase : System.Web.Services.WebService
    {
        protected ServiceResponse FillSpecialService(bool renderControl,
                                                    string agencyCode,
                                                    BookingHeader bookingHeader,
                                                    Itinerary itinerary,
                                                    Passengers passengers,
                                                    Quotes quotes,
                                                    Mappings mappings,
                                                    Taxes taxes,
                                                    Fees fees,
                                                    Services services,
                                                    Remarks remarks)
        {

            try
            {
                ServiceResponse response = new ServiceResponse();

                if (services.Count > 0)
                {
                    if (fees == null)
                    {
                        fees = new Fees();
                    }

                    //Calclate fee
                    if (fees.objService == null)
                    {
                        fees.objService = B2CSession.AgentService;
                    }

                    fees.CalculateSpecialServiceFee(agencyCode,
                                                    bookingHeader.currency_rcd,
                                                    bookingHeader,
                                                    services,
                                                    remarks,
                                                    mappings,
                                                    Classes.Language.CurrentCode().ToUpper(),
                                                    false);

                    if (B2CSession.Services == null)
                    { B2CSession.Services = services; }

                    if (B2CSession.Fees == null)
                    { B2CSession.Fees = fees; }
                }

                if (renderControl == true)
                {
                    //Show fare summary
                    Library objLi = new Library();
                    Classes.Helper objHp = new Classes.Helper();
                    StringBuilder stb = new StringBuilder();

                    System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);


                    objTransform = objHp.GetXSLDocument("FareSummary");

                    using (StringWriter stw = new StringWriter(stb))
                    {
                        using (XmlWriter xtw = XmlWriter.Create(stw))
                        {
                            xtw.WriteStartElement("Booking");
                            {
                                objLi.BuiltBookingXml(xtw, null, itinerary, passengers, quotes, fees, mappings, null, null, null, taxes);
                            }
                            xtw.WriteEndElement();
                        }
                    }

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.RenderHtml(objTransform, objArgument, stb.ToString());
                }
                else
                {
                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = string.Empty;
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseRemoveSpecialService(bool removeAll)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                string ssrGroup = B2CSetting.SsrGroup;
                string[] arr = !string.IsNullOrEmpty(ssrGroup) ? ssrGroup.Split(',') : new string[] { };
                BookingHeader header = B2CSession.BookingHeader;
                Passengers passengers = B2CSession.Passengers;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Taxes taxes = B2CSession.Taxes;
                Remarks remarks = B2CSession.Remarks;
                Itinerary itinerary = B2CSession.Itinerary;
                if (services != null)
                {
                    if (removeAll)
                    {
                        services = null;
                    }
                    else
                    {
                        for (int i = services.Count - 1; i > -1; i--)
                        {
                            if (arr.Contains<string>(services[i].special_service_rcd))
                            {
                                services.Remove(services[i]);
                            }
                        }

                        for (int j = fees.Count - 1; j > -1; j--)
                        {
                            if (arr.Contains<string>(fees[j].fee_rcd))
                            {
                                fees.Remove(fees[j]);
                            }
                        }

                        B2CSession.Services = services;
                        B2CSession.Fees = fees;
                    }

                    fees = fees ?? new Fees();

                    //Calclate fee
                    if (fees.objService == null)
                    {
                        fees.objService = B2CSession.AgentService;
                    }

                    fees.CalculateSpecialServiceFee(header.agency_code,
                                                    header.currency_rcd,
                                                    header,
                                                    services,
                                                    remarks,
                                                    mappings,
                                                    Classes.Language.CurrentCode().ToUpper(),
                                                    false);

                    //Show fare summary
                    Library objLi = new Library();
                    Classes.Helper objHp = new Classes.Helper();
                    StringBuilder stb = new StringBuilder();

                    System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);


                    objTransform = objHp.GetXSLDocument("FareSummary");

                    using (StringWriter stw = new StringWriter(stb))
                    {
                        using (XmlWriter xtw = XmlWriter.Create(stw))
                        {
                            xtw.WriteStartElement("Booking");
                            {
                                objLi.BuiltBookingXml(xtw, null, itinerary, passengers, quotes, fees, mappings, null, null, null, taxes);
                            }
                            xtw.WriteEndElement();
                        }
                    }

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = objLi.RenderHtml(objTransform, objArgument, stb.ToString());
                }
                else
                {
                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = string.Empty;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
                return response;
            }
        }
        protected ServiceResponse BaseRemoveSpecialService(Service service)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                Services services = B2CSession.Services;
                if (services != null)
                {
                    services.Remove(service);
                    response.Success = true;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
                return response;
            }
        }

        //protected ServiceResponse BaseCalculateBaggageFee(decimal numberOfUnit, 
        //                                                string strBookingSegmentId, 
        //                                                string strPassengerId, 
        //                                                bool bReturn, 
        //                                                bool bGenerateControl)
        //{
        //    try
        //    {
        //        ServiceResponse respose = null;

        //        B2CVariable objUv = B2CSession.Variable;

        //        Fees objBagFee;

        //        if (objUv != null)
        //        {
        //            if (bReturn == false)
        //            {
        //                objBagFee = B2CSession.BaggageFeeDepart;
        //            }
        //            else
        //            {
        //                objBagFee = B2CSession.BaggageFeeReturn;
        //            }
        //            respose = FillBaggageFee(objBagFee, strBookingSegmentId, strPassengerId, numberOfUnit, bGenerateControl);
        //        }
        //        else
        //        {
        //            respose = new ServiceResponse();
        //            respose.Success = true;
        //            respose.Code = "002";
        //            respose.Message = "Session is timeout.";
        //        }

        //        return respose;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        protected ServiceResponse BaseCalculateBaggageFee(decimal numberOfUnit,
                                                        string strBookingSegmentId,
                                                        string strPassengerId,
                                                        bool bGenerateControl)
        {
            try
            {
                ServiceResponse respose = null;

                B2CVariable objUv = B2CSession.Variable;
                Mappings mappings = B2CSession.Mappings;
                Fees objBagFee;
                Guid gSegmentId = Guid.Empty;
                Guid gPassengerId = Guid.Empty;

                if (objUv != null)
                {
                    objBagFee = new Fees();
                    if (!string.IsNullOrEmpty(strPassengerId))
                        gPassengerId = new Guid(strPassengerId);
                    if (!string.IsNullOrEmpty(strBookingSegmentId))
                        gSegmentId = new Guid(strBookingSegmentId);

                    objBagFee.objService = B2CSession.AgentService;
                    objBagFee.GetBaggageFeeOptions(mappings,
                                                        gSegmentId,
                                                        gPassengerId,
                                                        objUv.Agency_Code,
                                                        Classes.Language.CurrentCode().ToUpper(),
                                                        0,
                                                        null,
                                                        false,
                                                        false);
                    objBagFee.objService = null;
                    respose = FillBaggageFee(objBagFee, strBookingSegmentId, strPassengerId, numberOfUnit, bGenerateControl);
                }
                else
                {
                    respose = new ServiceResponse();
                    respose.Success = true;
                    respose.Code = "002";
                    respose.Message = "Session is timeout.";
                }

                return respose;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseClearBaggageFee(string strSegmentId,
                                                      string strPassengerId,
                                                      bool bGenerateControl)
        {
            ServiceResponse respose = new ServiceResponse();
            Fees fees = B2CSession.Fees;
            ClearBaggageFee(ref fees, new Guid(strSegmentId), strPassengerId);

            if (bGenerateControl == true)
            {
                return BaseGetSessionQuoteSummary();
            }
            else
            {
                respose = new ServiceResponse();
                respose.Success = true;
                respose.Code = "000";
                respose.Message = "SUCCESS";
                respose.HTML = string.Empty;
            }

            return respose;
        }

        protected ServiceResponse GetBaggageFeeOptions(Guid passengerId)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                B2CVariable objUv = B2CSession.Variable;
                Mappings mappings = B2CSession.Mappings;
                Itinerary itinerary = B2CSession.Itinerary;
                Fees fees = B2CSession.Fees;
                Fees objOutWardFee = B2CSession.BaggageFeeDepart;
                Fees objReturnFee = B2CSession.BaggageFeeReturn;

                if (objUv == null || mappings == null || itinerary == null || fees == null)
                {
                    response.Success = false;
                    response.Code = "201";
                    response.Message = "Booking object is null.(Session time out)";

                    return response;
                }
                else
                {
                    FlightSegment firstOdSegment;
                    StringBuilder stb = new StringBuilder();
                    int iFeeCount = 0;
                    stb.Append("[{");

                    #region Departure Flight
                    //Departure Baggage Fees.
                    firstOdSegment = itinerary.GetOdFirstSegment(objUv.OriginRcd, objUv.DestinationRcd);
                    if (firstOdSegment != null && firstOdSegment.booking_segment_id.Equals(Guid.Empty) == false)
                    {
                        stb.Append("\"BaggageOutAllowance\":" + mappings.FindPieceAllowance(firstOdSegment.booking_segment_id, passengerId) + ",");
                        stb.Append("\"BaggageOut\":");
                        {
                            stb.Append("[");
                            if (objOutWardFee == null)
                            {
                                objOutWardFee = new Fees();
                                B2CSession.BaggageFeeDepart = objOutWardFee;
                            }
                            else
                            {
                                objOutWardFee = B2CSession.BaggageFeeDepart;
                                if (objOutWardFee != null)
                                {
                                    objOutWardFee.Clear();
                                }
                            }
                            //Get Baggage Fee
                            objOutWardFee.objService = B2CSession.AgentService;
                            objOutWardFee.GetBaggageFeeOptions(mappings,
                                                               firstOdSegment.booking_segment_id,
                                                               passengerId,
                                                               objUv.Agency_Code,
                                                               Classes.Language.CurrentCode().ToUpper(),
                                                               0,
                                                               null,
                                                               false,
                                                               false);
                            objOutWardFee.objService = null;

                            //Find Selected fee.
                            int j;
                            for (j = 0; j < fees.Count; j++)
                            {
                                if (fees[j].booking_segment_id.Equals(firstOdSegment.booking_segment_id) &
                                    fees[j].passenger_id.Equals(passengerId) &
                                    (fees[j].fee_category_rcd != null && fees[j].fee_category_rcd.Equals("BAGSALES")))
                                {
                                    break;
                                }
                            }

                            //Date Start
                            if (firstOdSegment.booking_segment_id.Equals(Guid.Empty) == false)
                            {
                                iFeeCount = objOutWardFee.Count;
                                for (int i = 0; i < iFeeCount; i++)
                                {
                                    stb.Append("{");
                                    stb.Append("\"fee_id\":\"" + objOutWardFee[i].fee_id.ToString() + "\",");
                                    stb.Append("\"fee_rcd\":\"" + objOutWardFee[i].fee_rcd + "\",");
                                    stb.Append("\"currency_rcd\":\"" + objOutWardFee[i].currency_rcd + "\",");
                                    stb.Append("\"display_name\":\"" + objOutWardFee[i].display_name + "\",");
                                    stb.Append("\"number_of_units\":\"" + objOutWardFee[i].number_of_units + "\",");
                                    stb.Append("\"total_amount\":\"" + objOutWardFee[i].total_amount + "\",");
                                    stb.Append("\"total_amount_incl\":\"" + objOutWardFee[i].total_amount_incl + "\",");

                                    if (fees.Count > 0 &&
                                        fees.Count != j &&
                                        objOutWardFee[i].passenger_id.Equals(fees[j].passenger_id) &&
                                        objOutWardFee[i].booking_segment_id.Equals(fees[j].booking_segment_id) &&
                                        objOutWardFee[i].fee_category_rcd.Equals(fees[j].fee_category_rcd) &&
                                        objOutWardFee[i].number_of_units.Equals(fees[j].number_of_units))
                                    {
                                        stb.Append("\"Selected\":true");
                                    }
                                    else
                                    {
                                        stb.Append("\"Selected\":false");
                                    }

                                    if ((i < (iFeeCount - 1)) == true)
                                    { stb.Append("},"); }
                                    else
                                    { stb.Append("}"); }
                                }
                            }
                            //Data End
                            stb.Append("],");
                        }
                    }
                    firstOdSegment = null;

                    #endregion

                    #region Return Flight
                    //Return Baggage Fees.
                    firstOdSegment = itinerary.GetOdFirstSegment(objUv.DestinationRcd, objUv.OriginRcd);
                    if (firstOdSegment != null && firstOdSegment.booking_segment_id.Equals(Guid.Empty) == false)
                    {
                        stb.Append("\"BaggageReturnAllowance\":" + mappings.FindPieceAllowance(firstOdSegment.booking_segment_id, passengerId) + ",");
                        stb.Append("\"BaggageReturn\":");
                        {
                            stb.Append("[");
                            if (objReturnFee == null)
                            {
                                objReturnFee = new Fees();
                                B2CSession.BaggageFeeReturn = objReturnFee;
                            }
                            else
                            {
                                objReturnFee = B2CSession.BaggageFeeReturn;
                                objReturnFee.Clear();
                            }
                            objReturnFee.objService = B2CSession.AgentService;
                            objReturnFee.GetBaggageFeeOptions(mappings,
                                                              firstOdSegment.booking_segment_id,
                                                              passengerId,
                                                              objUv.Agency_Code,
                                                              Classes.Language.CurrentCode().ToUpper(),
                                                              0,
                                                              null,
                                                              false,
                                                              false);
                            objReturnFee.objService = null;
                            //Find Selected fee.
                            int j;
                            for (j = 0; j < fees.Count; j++)
                            {
                                if (fees[j].booking_segment_id.Equals(firstOdSegment.booking_segment_id) &
                                    fees[j].passenger_id.Equals(passengerId) &
                                    (fees[j].fee_category_rcd != null && fees[j].fee_category_rcd.Equals("BAGSALES")))
                                {
                                    break;
                                }
                            }
                            if (firstOdSegment.booking_segment_id.Equals(Guid.Empty) == false)
                            {
                                iFeeCount = objReturnFee.Count;
                                for (int i = 0; i < iFeeCount; i++)
                                {
                                    stb.Append("{");
                                    stb.Append("\"fee_id\":\"" + objReturnFee[i].fee_id.ToString() + "\",");
                                    stb.Append("\"fee_rcd\":\"" + objReturnFee[i].fee_rcd + "\",");
                                    stb.Append("\"currency_rcd\":\"" + objReturnFee[i].currency_rcd + "\",");
                                    stb.Append("\"display_name\":\"" + objReturnFee[i].display_name + "\",");
                                    stb.Append("\"number_of_units\":\"" + objReturnFee[i].number_of_units + "\",");
                                    stb.Append("\"total_amount\":\"" + objReturnFee[i].total_amount + "\",");
                                    stb.Append("\"total_amount_incl\":\"" + objReturnFee[i].total_amount_incl + "\",");

                                    if (fees.Count > 0 &&
                                        fees.Count != j &&
                                        objReturnFee[i].passenger_id.Equals(fees[j].passenger_id) &&
                                        objReturnFee[i].booking_segment_id.Equals(fees[j].booking_segment_id) &&
                                        objReturnFee[i].fee_category_rcd.Equals(fees[j].fee_category_rcd) &&
                                        objReturnFee[i].number_of_units.Equals(fees[j].number_of_units))
                                    {
                                        stb.Append("\"Selected\":true");
                                    }
                                    else
                                    {
                                        stb.Append("\"Selected\":false");
                                    }

                                    if ((i < (iFeeCount - 1)) == true)
                                    { stb.Append("},"); }
                                    else
                                    { stb.Append("}"); }

                                }
                            }
                            stb.Append("]");
                        }
                    }
                    firstOdSegment = null;
                    #endregion

                    stb.Append("}]");


                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = stb.ToString();

                    return response;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected ServiceResponse BaseGetPaymentFee(string formOfPayment,
                                                    string formOfPaymentSubType)
        {
            string formOfPaymentFeeXml = string.Empty;
            decimal outStandingBalance = 0;
            try
            {
                ServiceResponse response = new ServiceResponse();
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Passengers passengers = B2CSession.Passengers;
                Itinerary itinerary = B2CSession.Itinerary;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Payments payments = B2CSession.Payments;
                B2CVariable objUv = B2CSession.Variable;

                //Calclate payment fee
                Fees paymentFee = new Fees();
                paymentFee.objService = B2CSession.AgentService;

                if (paymentFee.GetPaymentFee(formOfPayment,
                                             formOfPaymentSubType,
                                             bookingHeader,
                                             passengers,
                                             itinerary,
                                             quotes,
                                             fees,
                                             payments,
                                             bookingHeader.currency_rcd,
                                             objUv.UserId,
                                             formOfPaymentFeeXml,
                                             outStandingBalance))
                {
                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    //response.HTML = paymentFee.GetXml();

                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    response.HTML = serializer.Serialize(paymentFee);
                }
                else
                {
                    response.Success = false;
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Helper
        private void ClearBaggageFee(ref Fees fees, Guid gBookingSegmentId, string PassengerId)
        {
            if (fees != null && fees.Count > 0)
            {
                for (int i = 0; i < fees.Count; i++)
                {
                    if (fees[i].passenger_id.Equals(new Guid(PassengerId)) &
                        fees[i].booking_segment_id.Equals(gBookingSegmentId) &
                        (fees[i].fee_category_rcd != null && fees[i].fee_category_rcd.Equals("BAGSALES")))
                    {
                        fees.RemoveAt(i);
                        break;
                    }
                }
            }

        }
        private ServiceResponse FillBaggageFee(Fees objBagFee, string strBookingSegmentId, string strPassengerId, decimal numberOfUnit, bool bGenerateControl)
        {
            ServiceResponse respose = null;

            B2CVariable objUv = B2CSession.Variable;
            Fees fees = B2CSession.Fees;
            Itinerary itinerary = B2CSession.Itinerary;
            string comboFlight_booking_segment_id = itinerary.GetBookingSegmentIdComboFlight(strBookingSegmentId);

            if (objBagFee != null)
            {
                //Create instance if fee object is null.
                if (fees == null)
                {
                    fees = new Fees();
                }

                //Clear baggage fee before adding the new one.
                ClearBaggageFee(ref fees, new Guid(strBookingSegmentId), strPassengerId);
                Fee fe;
                for (int f = 0; f < objBagFee.Count; f++)
                {
                    if (objBagFee[f].number_of_units == numberOfUnit)
                    {
                        fe = new Fee();

                        fe.booking_fee_id = Guid.NewGuid();
                        fe.passenger_id = new Guid(strPassengerId);
                        fe.booking_segment_id = new Guid(strBookingSegmentId);
                        fe.fee_id = objBagFee[f].fee_id;
                        fe.fee_rcd = objBagFee[f].fee_rcd;
                        fe.fee_category_rcd = objBagFee[f].fee_category_rcd;
                        fe.currency_rcd = objBagFee[f].currency_rcd;
                        fe.display_name = objBagFee[f].display_name;
                        fe.number_of_units = objBagFee[f].number_of_units;
                        fe.fee_amount = objBagFee[f].total_amount;
                        fe.fee_amount_incl = objBagFee[f].total_amount_incl;
                        fe.total_amount = 0;
                        fe.total_amount_incl = 0;
                        fe.vat_percentage = objBagFee[f].vat_percentage;
                        fe.agency_code = objUv.Agency_Code;

                        fees.Add(fe);
                        break;
                    }
                }

                // for combo flight
                if (!String.IsNullOrEmpty(comboFlight_booking_segment_id))
                {
                    for (int f = 0; f < objBagFee.Count; f++)
                    {
                        if (objBagFee[f].number_of_units == numberOfUnit)
                        {
                            fe = new Fee();

                            fe.booking_fee_id = Guid.NewGuid();
                            fe.passenger_id = new Guid(strPassengerId);
                            fe.booking_segment_id = new Guid(comboFlight_booking_segment_id);
                            fe.fee_id = objBagFee[f].fee_id;
                            fe.fee_rcd = objBagFee[f].fee_rcd;
                            fe.fee_category_rcd = objBagFee[f].fee_category_rcd;
                            fe.currency_rcd = objBagFee[f].currency_rcd;
                            fe.display_name = objBagFee[f].display_name;
                            fe.number_of_units = objBagFee[f].number_of_units;
                            fe.fee_amount = 0;
                            fe.fee_amount_incl = 0;
                            fe.total_amount = 0;
                            fe.total_amount_incl = 0;
                            fe.vat_percentage = objBagFee[f].vat_percentage;
                            fe.agency_code = objUv.Agency_Code;

                            fees.Add(fe);
                            break;

                        }
                    }
                }

                //Add new fee object to null fee session.
                if (B2CSession.Fees == null)
                { B2CSession.Fees = fees; }

                if (bGenerateControl == true)
                {
                    respose = BaseGetSessionQuoteSummary();
                }
                else
                {
                    respose = new ServiceResponse();
                    respose.Success = true;
                    respose.Code = "000";
                    respose.Message = "SUCCESS";
                }
            }
            return respose;
        }

        #endregion
    }
}