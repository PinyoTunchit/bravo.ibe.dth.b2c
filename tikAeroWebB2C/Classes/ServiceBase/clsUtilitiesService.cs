﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using tikSystem.Web.Library;
using tikAeroB2C.Classes;
using tikAeroB2C.Classes.ServiceMessage;
using System.Linq;

namespace tikAeroB2C.Base
{
    public abstract partial class ServiceBase : System.Web.Services.WebService
    {
        protected ServiceResponse BaseGetDocumentType(string language)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                ServiceClient objClientService = new ServiceClient();

                //Read Client information from web service
                objClientService.objService = B2CSession.AgentService;
                Documents documents = objClientService.GetDocumentType(language);
                objClientService = null;

                if (documents != null)
                {
                    //Construct Document Type string
                    StringBuilder stbResult = new StringBuilder();

                    for (int i = 0; i < documents.Count; i++)
                    {
                        stbResult.Append(documents[i].document_type_rcd);
                        stbResult.Append("|");
                        stbResult.Append(documents[i].display_name);
                        stbResult.Append("{}");
                    }

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = stbResult.ToString();
                }
                else
                {
                    response.Success = false;
                    response.Code = "006";
                    response.Message = "Read failed.";
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetPassengerTitles(string language)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                ServiceClient objClientService = new ServiceClient();


                //Read Client information from web service
                objClientService.objService = B2CSession.AgentService;
                Titles titles = objClientService.GetPassengerTitles(language);

                if (titles != null)
                {
                    //Construct Document Type string
                    StringBuilder stbResult = new StringBuilder();

                    for (int i = 0; i < titles.Count; i++)
                    {
                        stbResult.Append(titles[0].title_rcd);
                        stbResult.Append("|");
                        stbResult.Append(titles[0].display_name);
                        stbResult.Append("|");
                        stbResult.Append(titles[0].gender_type_rcd);
                        stbResult.Append("{}");
                    }

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = stbResult.ToString();
                }
                else
                {
                    response.Success = false;
                    response.Code = "006";
                    response.Message = "Read failed.";
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetCountry(string language)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                ServiceClient objClientService = new ServiceClient();

                //Read Client information from web service
                objClientService.objService = B2CSession.AgentService;
                Countries countries = objClientService.GetCountry(language);

                if (countries != null)
                {
                    //Construct Document Type string
                    StringBuilder stbResult = new StringBuilder();

                    for (int i = 0; i < countries.Count; i++)
                    {
                        stbResult.Append(countries[i].country_rcd);
                        stbResult.Append("|");
                        stbResult.Append(countries[i].display_name);
                        stbResult.Append("{}");
                    }

                    response.Success = true;
                    response.Code = "000";
                    response.Message = "SUCCESS";
                    response.HTML = stbResult.ToString();
                }
                else
                {
                    response.Success = false;
                    response.Code = "006";
                    response.Message = "Read failed.";
                }



                return response;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        protected ServiceResponse BaseLoadCob(bool parameter, string bookingId)
        {
            try
            {
                ServiceResponse response = LoadChangeOfBooking(parameter, bookingId, false);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetCOBUrl(bool parameter, string bookingId)
        {
            try
            {
                string strLanguage = string.Empty;
                string strUrl = B2CSetting.CobUrl;

                Classes.Helper objHp = new Classes.Helper();
                B2CVariable objUv = B2CSession.Variable;

                if (objHp.SessionTimeout() == true || objUv == null)
                {
                    BaseClearSession();
                    objUv = new B2CVariable();
                    B2CSession.Variable = objUv;
                }

                Uri cUri = HttpContext.Current.Request.Url;
                Uri uri;
                if (Uri.TryCreate(strUrl, UriKind.Relative, out uri))
                {
                    uri = new Uri(string.Format("{0}://{1}{2}", cUri.Scheme, cUri.Host, strUrl.StartsWith("/") ? strUrl : "/" + strUrl));
                    strUrl = string.Format("{0}", uri.AbsolutePath);
                }
                else if (Uri.TryCreate(strUrl, UriKind.Absolute, out uri))
                {

                }
                else
                {
                    strUrl = string.Format("{0}", "/COB/default.aspx");
                }

                //Set language.
                if (B2CSession.LanCode == null)
                { strLanguage = "en-us"; }
                else
                { strLanguage = B2CSession.LanCode; }

                objUv.CurrentStep = 6;
                Library objLi = new Library();
                if (parameter == false || bookingId.Length == 0)
                {
                    //Load Login Page
                    strUrl += "?id=||" +
                            objUv.UserId.ToString() + "|" +
                            objUv.Agency_Code + "|" +
                            strLanguage + "|" +
                            "0";
                }
                else
                {
                    //Load Booking Summary
                    Client objClient = B2CSession.Client;
                    if (objClient != null || bookingId.Length > 0)
                    {
                        strUrl += "?id=" +
                                bookingId + "|" +
                                ((objClient != null) ? objClient.client_profile_id.ToString() : string.Empty) + "|" +
                                objUv.UserId.ToString() + "|" +
                                objUv.Agency_Code + "|" +
                                strLanguage + "|" +
                                "0";
                    }
                    else
                    {
                        strUrl += "?id=||" +
                                objUv.UserId.ToString() + "|" +
                                objUv.Agency_Code + "|" +
                                strLanguage + "|" +
                                "0";
                    }
                }

                ServiceResponse response = new ServiceResponse();

                response.Success = true;
                response.Code = "000";
                response.Message = "SUCCESS";
                response.HTML = strUrl;

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseLoadManageBooking(string url)
        {
            try
            {
                ServiceResponse response = LoadChangeOfBooking(true, url, true);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseLoadCMS(string url)
        {
            try
            {
                Library objLi = new Library();
                ServiceResponse response = new ServiceResponse();

                response.Success = true;
                response.Code = "000";
                response.Message = "MESSAGE";
                response.HTML = objLi.GenerateControlString("UserControls/Cob.ascx", url);

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BasesSetLanguage(string language)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                response.Success = true;
                response.Code = "000";
                response.Message = "MESSAGE";
                response.HTML = language;
                B2CSession.LanCode = language;

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseLoadSearchAvailability()
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                Library li = new Library();

                response.Success = true;
                response.Code = "000";
                response.Message = "SUCCESS";
                response.HTML = li.GenerateControlString("UserControls/SearchAvailability.ascx", string.Empty);

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseLoadHTML(string fileName)
        {
            try
            {
                Library objLi = new Library();
                ServiceResponse response = new ServiceResponse();
                string strLanguage = string.Empty;

                if (B2CSession.LanCode == null)
                { strLanguage = B2CSetting.DefaultLanguage.Split('-')[0]; }
                else
                { strLanguage = B2CSession.LanCode.Split('-')[0]; }

                response.Success = true;
                response.Code = "000";
                response.Message = "SUCCESS";
                response.HTML = objLi.GenerateControlString("UserControls/HtmlContainer.ascx", "html/" + strLanguage + "/" + fileName); ;

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseShowEmailSender()
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                Library objLi = new Library();

                response.Success = true;
                response.Code = "000";
                response.Message = "SUCCESS";
                response.HTML = objLi.GenerateControlString("UserControls/EmailSender.ascx", "");

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetCurrentStep()
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                B2CVariable objUv = B2CSession.Variable;
                response.Success = true;
                response.Code = "000";
                response.Message = "SUCCESS";

                if (objUv != null)
                {
                    response.ToInt16 = objUv.CurrentStep;
                }
                else
                {
                    response.ToInt16 = 1;
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseGetCurrentState()
        {
            try
            {
                B2CVariable objUv = B2CSession.Variable;
                string strBookingId = string.Empty;
                string currentStep = "1";
                bool sessionCreate = B2CSession.SessionCreate;
                bool bClientLogon = false;
                string gateway = string.Empty;
                BookingHeader bookingHeader = B2CSession.BookingHeader;

                //Get Booking Header Id.
                if (bookingHeader != null && bookingHeader.booking_id.Equals(Guid.Empty) == false)
                {
                    strBookingId = bookingHeader.booking_id.ToString();
                }
                //Read current booking step.
                if (objUv != null)
                {
                    currentStep = objUv.CurrentStep.ToString();
                    gateway = objUv.PaymentGateway;
                }

                if (sessionCreate == false)
                {
                    B2CSession.SessionCreate = true;
                }

                if (B2CSession.Client != null)
                {
                    bClientLogon = true;
                }

                ServiceResponse response = new ServiceResponse();

                response.Success = true;
                response.Code = "000";
                response.Message = "SUCCESS";
                response.HTML = "{\"SessionId\":\"" + Session.SessionID + "\", \"SessionCreate\":" + sessionCreate.ToString().ToLower() + ", \"CurrentStep\":" + currentStep + ", \"BookingId\":\"" + strBookingId
                    + "\" , \"LanguageCode\":\"" + Classes.Language.CurrentCode() + "\", \"Gateway\":\"" + gateway + "\", \"ClientProfile\":" + bClientLogon.ToString().ToLower() + "}";

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected ServiceResponse BaseLoadHome()
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                B2CVariable objUv = B2CSession.Variable;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library objLi = new Library();

                //Clear temp availability
                if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                {
                    //Clear block inventory
                    ServiceClient objSvc = new ServiceClient();
                    objSvc.ReleaseSessionlessFlightInventorySession(bookingHeader.booking_id.ToString(),
                                                                    string.Empty,
                                                                    string.Empty,
                                                                    string.Empty,
                                                                    false,
                                                                    true,
                                                                    true,
                                                                    SecurityHelper.GenerateSessionlessToken());

                    //Clear booking in session
                    objLi.ClearBooking(ref bookingHeader,
                                        ref itinerary,
                                        ref passengers,
                                        ref quotes,
                                        ref fees,
                                        ref mappings,
                                        ref services,
                                        ref remarks,
                                        ref payments,
                                        ref taxes);
                }

                response.Success = true;
                response.Code = "000";
                response.Message = "SUCCESS";

                //Remove Clear session check
                Session.Remove("bHttps");
                if (objUv == null)
                {
                    response.HTML = objLi.GenerateControlString("UserControls/home.ascx", string.Empty);
                }
                else if (B2CSession.Client != null)
                {
                    objUv.CurrentStep = 7;
                    response.HTML = objLi.GenerateControlString("UserControls/MyBooking.ascx", "");
                }
                else
                {
                    objUv.CurrentStep = 1;
                    response.HTML = objLi.GenerateControlString("UserControls/home.ascx", string.Empty);
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void BaseClearPassengerData()
        {
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Itinerary itinerary = B2CSession.Itinerary;
                Passengers passengers = B2CSession.Passengers;
                Quotes quotes = B2CSession.Quotes;
                Fees fees = B2CSession.Fees;
                Mappings mappings = B2CSession.Mappings;
                Services services = B2CSession.Services;
                Remarks remarks = B2CSession.Remarks;
                Payments payments = B2CSession.Payments;
                Taxes taxes = B2CSession.Taxes;

                Library li = new Library();

                //Clear contact information
                bookingHeader.title_rcd = string.Empty;
                bookingHeader.firstname = string.Empty;
                bookingHeader.middlename = string.Empty;
                bookingHeader.lastname = string.Empty;
                bookingHeader.phone_mobile = string.Empty;
                bookingHeader.phone_home = string.Empty;
                bookingHeader.phone_business = string.Empty;
                bookingHeader.contact_email = string.Empty;
                bookingHeader.address_line1 = string.Empty;
                bookingHeader.address_line2 = string.Empty;
                bookingHeader.zip_code = string.Empty;
                bookingHeader.city = string.Empty;
                bookingHeader.country_rcd = string.Empty;
                bookingHeader.language_rcd = string.Empty;

                //Clear passenger infomation
                foreach (Passenger p in passengers)
                {
                    p.firstname = string.Empty;
                    p.middlename = string.Empty;
                    p.lastname = string.Empty;
                    p.title_rcd = string.Empty;
                    p.document_type_rcd = string.Empty;
                    p.nationality_rcd = string.Empty;
                    p.passport_number = string.Empty;
                    p.passport_issue_place = string.Empty;
                    p.passport_issue_date = DateTime.MinValue;
                    p.passport_expiry_date = DateTime.MinValue;
                    p.date_of_birth = DateTime.MinValue;
                    p.member_number = string.Empty;
                }

                //Clear mapping information
                foreach (Mapping mp in mappings)
                {
                    mp.firstname = string.Empty;
                    mp.middlename = string.Empty;
                    mp.lastname = string.Empty;
                    mp.title_rcd = string.Empty;
                    mp.date_of_birth = DateTime.MinValue;
                    mp.seat_row = 0;
                    mp.seat_column = string.Empty;
                    mp.seat_number = string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void BaseClearSession()
        {
            try
            {
                if (B2CSession.Https == false)
                {
                    //Release Seat
                    B2CVariable objUv = B2CSession.Variable;
                    BookingHeader bookingHeader = B2CSession.BookingHeader;
                    Itinerary itinerary = B2CSession.Itinerary;
                    Passengers passengers = B2CSession.Passengers;
                    Quotes quotes = B2CSession.Quotes;
                    Fees fees = B2CSession.Fees;
                    Mappings mappings = B2CSession.Mappings;
                    Services services = B2CSession.Services;
                    Remarks remarks = B2CSession.Remarks;
                    Payments payments = B2CSession.Payments;
                    Taxes taxes = B2CSession.Taxes;

                    tikSystem.Web.Library.agentservice.TikAeroXMLwebservice objService = B2CSession.AgentService;

                    bool bResult = false;

                    if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
                    {
                        if (objService != null)
                        {
                            ServiceClient obj = new ServiceClient();
                            bResult = obj.ReleaseSessionlessFlightInventorySession(string.Empty, string.Empty, string.Empty, bookingHeader.booking_id.ToString(), false, true, true, SecurityHelper.GenerateSessionlessToken());
                        }
                    }

                    //Clear Value.
                    if (objUv != null)
                    {
                        //reset to first step
                        objUv.CurrentStep = 1;
                        B2CSession.Variable = objUv;
                    }

                    //Booking Section.
                    B2CSession.BookingHeader = null;
                    B2CSession.Itinerary = null;
                    B2CSession.Passengers = null;
                    B2CSession.Quotes = null;
                    B2CSession.Fees = null;
                    B2CSession.Mappings = null;
                    B2CSession.Services = null;
                    B2CSession.Remarks = null;
                    B2CSession.Payments = null;
                    B2CSession.Taxes = null;
                    B2CSession.CaptchaImageText = null;
                    B2CSession.responsePrice = null;
                    B2CSession.InsuaranceFee = null;
                    B2CSession.BaggageFeeDepart = null;
                    B2CSession.BaggageFeeReturn = null;
                    B2CSession.BookingSummary = null;

                    //Remove Clear session check
                    Util.RemoveSession(Cnum.CnumSession.Https);
                }
                else
                {
                    //Remove Clear session check
                    Util.RemoveSession(Cnum.CnumSession.Https);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void BaseSendItineraryEmail()
        {
            try
            {
                Helper objHp = new Helper();
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                B2CVariable objUv = B2CSession.Variable;
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();

                objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void BaseSendItineraryEmailOptional(string email)
        {
            try
            {
                Helper objHp = new Helper();
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                System.Collections.Specialized.StringDictionary _stdLanguage = Classes.Language.GetLanguageDictionary();
                objHp.SendItineraryByMail(bookingHeader, email, B2CSession.Variable.UserId, _stdLanguage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Helper
        private ServiceResponse LoadChangeOfBooking(bool parameter, string value, bool singleSignon)
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                ServiceResponse response = new ServiceResponse();

                string strLanguage = string.Empty;
                string strUrl = B2CSetting.CobUrl;
                Uri cUri = HttpContext.Current.Request.Url;
                Uri uri;

                if (Uri.TryCreate(strUrl, UriKind.Relative, out uri))
                {
                    uri = new Uri(string.Format("{0}://{1}{2}", cUri.Scheme, cUri.Host, strUrl.StartsWith("/") ? strUrl : "/" + strUrl));
                    strUrl = string.Format("{0}", uri.AbsolutePath);
                }
                else if (Uri.TryCreate(strUrl, UriKind.Absolute, out uri))
                {

                }
                else
                {
                    strUrl = string.Format("{0}", "/COB/default.aspx");
                }

                B2CVariable objUv = B2CSession.Variable;

                if (objHp.SessionTimeout() == true || objUv == null)
                {
                    BaseClearSession();
                    objUv = new B2CVariable();
                    B2CSession.Variable = objUv;
                }

                //Change the COB link to match the host Url.
                if (B2CSession.LanCode == null)
                { strLanguage = "en-us"; }
                else
                { strLanguage = B2CSession.LanCode; }

                objUv.CurrentStep = 6;
                Library objLi = new Library();

                response.Success = true;
                if (parameter == false || string.IsNullOrEmpty(value))
                {
                    //Load Login Page
                    string strHttp = HttpHelper.LoadSecurePage(B2CSession.Client, objUv.CurrentStep);
                    if (strHttp == "https")
                    {
                        B2CSession.Https = true;
                        response.Code = "https";
                        response.Message = "Redirect to https";
                    }
                    else
                    {
                        response.Code = "000";
                        response.Message = "SUCCESS";
                        response.HTML = objLi.GenerateControlString("UserControls/Cob.ascx", strUrl + "?id=||" +
                                                                                            objUv.UserId.ToString() + "|" +
                                                                                            objUv.Agency_Code + "|" +
                                                                                            strLanguage + "|" +
                                                                                            "0");
                    }
                }
                else
                {
                    //Load Booking Summary
                    Client objClient = B2CSession.Client;

                    response.Code = "000";
                    response.Message = "SUCCESS";


                    if (singleSignon == false)
                    {
                        if ((objClient != null || B2CSession.BookingHeader != null) && string.IsNullOrEmpty(value) == false)
                        {
                            response.HTML = objLi.GenerateControlString("UserControls/Cob.ascx", strUrl + "?id=" +
                                                                                                 value + "|" +
                                                                                                 ((objClient != null) ? objClient.client_profile_id.ToString() : string.Empty) + "|" +
                                                                                                 objUv.UserId.ToString() + "|" +
                                                                                                 objUv.Agency_Code + "|" +
                                                                                                 strLanguage + "|" +
                                                                                                 "0");
                        }
                        else
                        {
                            objUv.CurrentStep = 1;
                            response.Success = false;
                            response.Code = "002";
                        }
                    }
                    else
                    {
                        response.HTML = objLi.GenerateControlString("UserControls/Cob.ascx", strUrl + "?sn=" + value);
                    }

                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}