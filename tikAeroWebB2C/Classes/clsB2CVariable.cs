﻿using System;
using System.Collections.Generic;
using System.Web;
using tikSystem.Web.Library;

namespace tikAeroB2C
{
    public class B2CVariable : MAVariable
    {
        #region Detail Section
        string _promoCode;
        public string PromoCode
        {
            get { return _promoCode; }
            set { _promoCode = value; }
        }
        Int16 _DayRange;
        public Int16 DayRange
        {
            get { return _DayRange; }
            set { _DayRange = value; }
        }
        string _searchType;
        public string SearchType
        {
            get { return _searchType; }
            set { _searchType = value; }
        }
        string _DepartureDate = string.Empty;
        public string DepartureDate
        {
            get { return _DepartureDate; }
            set { _DepartureDate = value; }
        }
        string _ReturnDate = string.Empty;
        public string ReturnDate
        {
            get { return _ReturnDate; }
            set { _ReturnDate = value; }
        }
        bool _SkipFareLogic = false;
        public bool SkipFareLogic
        {
            get { return _SkipFareLogic; }
            set { _SkipFareLogic = value; }
        }
        bool _OneWay = false;
        public bool OneWay
        {
            get { return _OneWay; }
            set { _OneWay = value; }
        }
        bool _GroupBooking = false;
        public bool GroupBooking
        {
            get { return _GroupBooking; }
            set { _GroupBooking = value; }
        }
        string _PaymentReference;
        public string PaymentReference
        {
            get { return _PaymentReference; }
            set { _PaymentReference = value; }
        }
        string _PaymentGateway = string.Empty;
        public string PaymentGateway
        {
            get { return _PaymentGateway; }
            set { _PaymentGateway = value; }
        }
        int _GatewayRepeat = 0;
        public int GatewayRepeat
        {
            get { return _GatewayRepeat; }
            set { _GatewayRepeat = value; }
        }
        string _ip_address = string.Empty;
        public string ip_address
        {
            get { return _ip_address; }
            set { _ip_address = value; }
        }
        string _agency_code = string.Empty;
        public string Agency_Code
        {
            get { return _agency_code; }
            set { _agency_code = value; }
        }
        #endregion
    }
}