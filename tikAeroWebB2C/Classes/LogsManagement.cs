﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;

namespace tikAeroB2C.Classes
{
    public class LogsManagement
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Properties.Settings settings = Properties.Settings.Default;

        #region EnableErrorLog
        public static bool EnableErrorLog
        {
            get
            {
                return settings.EnableErrorLog;
            }
        }
        #endregion

        #region EnableNotifyLog
        public static bool EnableNotifyLog
        {
            get
            {
                return settings.EnableNotifyLog;
            }
        }
        #endregion

        #region EnableEmailErrorLog
        public static bool EnableEmailErrorLog
        {
            get
            {
                return settings.EnableEmailErrorLog;
            }
        }
        #endregion

        #region EmailErrorLog
        public static string EmailErrorLog
        {
            get
            {
                return settings.EmailErrorLog;
            }
        }
        #endregion

        #region SaveErrorLogs
        public static void SaveErrorLogs(string message, string mailXML)
        {
            //Validate on/off Write Log
            if (EnableErrorLog)
            {
                log.Error(message);
                if (EnableEmailErrorLog)
                {
                    tikSystem.Web.Library.MailManagement SendMail = new tikSystem.Web.Library.MailManagement();
                    string airLine = B2CSetting.DefaultAgencyLogon;
                    string product = "B2C";

                    //string strHMTLBody = Xsltmanagement.TranfromXLST(mailXML, HttpContext.Current.Server.MapPath("~") + @"\xsl\Email\Error_Email.xsl",
                    //    Xsltmanagement.ReadXsltMode.FileName, false);
                    string strHMTLBody = mailXML;
                    SendMail.MailForm = "Error@web.info";
                    SendMail.MailTo = EmailErrorLog;
                    SendMail.MailSubject = airLine + " " + product;
                    SendMail.SendMailSmtp(strHMTLBody, B2CSetting.SmtpServer);
                }
            }
        }
        public static void SaveErrorLogs(Exception ex, string input)
        {
            string functionName = (new StackTrace(true)).GetFrame(1).GetMethod().Name;
            string path = (new StackTrace(true)).GetFrame(1).ToString();
            SaveErrorLogs("[Function] " + functionName + " [Location] "
                + ex.TargetSite.DeclaringType.FullName + " [Message] " + ex.Message + path + " [Input] " + input
                , BuildXMLError(ex, "", functionName, input, path));
        }
        #endregion

        #region SaveNotifyLogs
        public static void SaveNotifyLogs(string message)
        {
            //Validate on/off Write Log
            if (EnableNotifyLog)
                log.Info(message);
        }
        #endregion

        private static string BuildXMLError(Exception ex, string page, string functionName, string input, string path)
        {
            StringBuilder xmlError = new StringBuilder();
            xmlError.Append("<Error>");
            xmlError.Append("<Location>");
            xmlError.Append(ex.TargetSite.DeclaringType.FullName);
            xmlError.Append("</Location>");
            xmlError.Append("<Function>");
            xmlError.Append(functionName);
            xmlError.Append("</Function>");
            xmlError.Append("<Massage>");
            xmlError.Append(ex.Message + path);
            xmlError.Append("</Massage>");
            xmlError.Append("<Input>");
            xmlError.Append(input.Replace("<", "&lt;").Replace(">", "&gt;"));
            xmlError.Append("</Input>");
            xmlError.Append("<Trace>");
            xmlError.Append(ex.StackTrace.ToString());
            xmlError.Append("</Trace>");
            xmlError.Append("<Page>");
            xmlError.Append(page);
            xmlError.Append("</Page>");
            xmlError.Append("</Error>");
            return xmlError.ToString();
        }
    }
}