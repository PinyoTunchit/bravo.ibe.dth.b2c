using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;
using System.Net.Mail;
using System.IO;

namespace tikAeroB2C
{
    public partial class ASearchRegisterLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Library objLi = new Library();
            if (B2CSession.Login == null)
            {
                dvSubContainer.InnerHtml = objLi.GenerateControlString("UserControls/NewsAdmin.ascx", string.Empty);
            }
            else if (IsPostBack == true)
            {
                SendMail();
            }
            else 
            {
                dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/NewsAdmin.ascx", string.Empty);
            }
            objLi = null;
        }

        protected void SendMail()
        {
            bool valid = true;
            if (B2CSession.SenderEmailFrom == null) valid = false;
            if (txtMailSubject.Value.Trim() == "") valid = false;
            if (txtMailTo.Value.Trim() == "") valid = false;

            if(!valid)
            {
                dvContainer.InnerHtml = dvResultMailError.InnerHtml;
                return;
            }

            string strBCC = "";

            DataTable dtMail = B2CSession.SearchRegister;
            string[] txtBCC = txtMailBCC.Value.Split(',');
            for (int i = 0; i < txtBCC.Length; i++)
            {
                if (txtBCC[i].Trim() != "")
                {
                    foreach (DataRow dr in dtMail.Rows)
                    {
                        if ((dr["Email_flag"].ToString() != "1") && (dr["Register_id"].ToString() == txtBCC[i].Trim()))
                        {
                            strBCC += dr["Register_email"].ToString() + ";";
                        }
                    }
                }
            } 

            strBCC += txtMailAddBcc.Value;

            StreamReader sr = new StreamReader(Server.MapPath("~") + "/HTML/registerMailForm.html");
            string mailForm = sr.ReadToEnd();
            string mailBody = mailForm.Replace("<%MESSAGE%>", txtMailMessage.Value.Replace("\r\n","<br>"));
            sr.Dispose();
            sr = null;

            if (txtMailAttachFile.PostedFile.FileName.Trim() != "")
            {
                if (!SendMail(txtMailTo.Value, B2CSession.SenderEmailFrom, strBCC, txtMailSubject.Value, mailBody, txtMailAttachFile))
                    return;
            }
            txtMailTo.Value = "";
            txtMailFrom.Value = "";
            B2CSession.SenderEmailFrom = null;
            dvContainer.InnerHtml = dvResultMailComplete.InnerHtml;
        }

        private bool SendMail(string mailTo, string mailFrom, string mailBcc, string mailSubject, string mailBody, System.Web.UI.HtmlControls.HtmlInputFile inputFile)
        {
            if (mailTo.Trim() == "") return false;
            if (mailFrom == null || mailFrom.Trim() == "") return false;

            MailAddress SendFrom = new MailAddress(mailFrom.Trim());
            MailAddress SendTo = new MailAddress(mailTo.Trim());


            MailMessage MyMessage = new MailMessage(mailFrom.Trim(), mailTo.Trim(), mailSubject, mailBody);
            MyMessage.IsBodyHtml = true;            

            string[] addBCC = mailBcc.Split(';');

            for (int i = 0; i < addBCC.Length; i++)
            {
                if (addBCC[i].Trim() != "")
                {
                    MailAddress SendBCC = new MailAddress(addBCC[i].Trim());
                    MyMessage.Bcc.Add(SendBCC);
                }
            }

            string fileAttach = inputFile.PostedFile.FileName;
            if (fileAttach.Trim() != "")
            {
                MyMessage.Attachments.Clear();
                string fPath = Server.MapPath("~") + "/Attachfile/";

                if (!Directory.Exists(fPath))
                {
                    Directory.CreateDirectory(fPath);
                }

                FileInfo fInfo = new FileInfo(fileAttach);
                fileAttach = fPath + fInfo.Name;
                inputFile.PostedFile.SaveAs(fileAttach);
                File.SetAttributes(fileAttach, FileAttributes.Normal);

                Attachment attachment = new Attachment(fileAttach);
                attachment.Name = fInfo.Name;
                MyMessage.Attachments.Add(attachment);
            }
            
            SmtpClient emailClient = new SmtpClient(B2CSetting.SmtpServer);
            emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            emailClient.UseDefaultCredentials = true;
            emailClient.Send(MyMessage);
            
            emailClient = null;            
            MyMessage.Dispose();
            MyMessage = null;

            return true;
        }
    }
}
