using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C
{
    public partial class SearchAvailability : System.Web.UI.Page
    {
        protected string ResultType = String.Empty;
        protected string CalendarToControls = String.Empty;
        protected string DateFormat = String.Empty;
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request["langculture"] != null)
            {
                B2CSession.LanCode = Page.Request["langculture"];
            }
            if (IsPostBack == false)
            {
                if (!string.IsNullOrEmpty(B2CSetting.OutputCalendarControl))
                    ResultType = B2CSetting.OutputCalendarControl;
                if (!string.IsNullOrEmpty(B2CSetting.CalendarToControls))
                    CalendarToControls = B2CSetting.CalendarToControls;
                if (!string.IsNullOrEmpty(B2CSetting.DateFormat))
                    DateFormat = B2CSetting.DateFormat;


                B2CVariable objUv = B2CSession.Variable;
                Helper objHelper = new Helper();
                if (B2CSession.Variable== null)
                {
                    tikAeroB2C.WebService.B2cService objService = new tikAeroB2C.WebService.B2cService();
                    objService.ClearSession();

                    objUv = new B2CVariable();

                    //Check agency cookies.
                    string strAgencyCode = string.Empty;

                    objUv.CurrentStep = 1;

                    if (string.IsNullOrEmpty(strAgencyCode))
                    {
                        objUv.Agency_Code = B2CSetting.DefaultAgencyCode;
                    }
                    else
                    {
                        objUv.Agency_Code = strAgencyCode;
                    }
                    B2CSession.Variable = objUv;
                }
                
                //Language
                objHelper.LanguageInitialize();
                //Load Language Dictionary
                _stdLanguage = Classes.Language.GetLanguageDictionary();

                //Write Language to page
                string noFlight = Classes.Language.Value("No Flight", "No Flight", _stdLanguage);
                string operateFlight = Classes.Language.Value("Operate Flight", "Operate Flight", _stdLanguage);
                string strJson = "<script type=\"text/javascript\">" +
                                        "SetLanguage(\'{" + objHelper.GetLanguagePageScript(Classes.Language.GetLanguageDictionary()) +
                                        ",\"NoFlight\" : \"" + noFlight + "\",\"OperateFlight\" : \"" + operateFlight + "\"" + "}');" +
                                     "</script>";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "languageScript", strJson);
                getOriginDestination();
            }
        }
        private void getOriginDestination()
        {
            
            Routes routes = CacheHelper.CacheDestination();

            //Load Destination Airport to Javascript Array
            StringBuilder sbRoute = new StringBuilder();
            if (routes != null && routes.Count > 0)
            {
                for (int i = 0; i < routes.Count; i++)
                {
                    sbRoute.Append("\"" + routes[i].destination_rcd + "|" +
                                         routes[i].display_name + "|" +
                                         routes[i].origin_rcd + "|" +
                                         routes[i].day_range + "|" +
                                         routes[i].currency_rcd + "\",");
                }
            }
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "var scDestination", "arrDestination = new Array(" + sbRoute.ToString().TrimEnd(',') + ");", true);
        }
    }
}
