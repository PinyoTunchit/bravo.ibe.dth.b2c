<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script language="javascript" src="Scripts/forCMS.js"></script>
<!--BeginTitle--><title>List All Page</title><!--EndTitle-->
<!--BeginKeywords--><META NAME="KEYWORDS" CONTENT="Airline"><!--EndKeywords-->
<!--BeginDescription--><META NAME="DESCRIPTION" CONTENT="DESCRIPTION"><!--EndDescription-->
<link href="stylesheets/cms_stylesheet.css"  rel="stylesheet" type="text/css">
</head>
<body>	
<!--#include file="EN/header.html" -->
<table width="0" cellspacing="0" cellpadding="0" id="wrapperHomeInside" class="Wrapper">
  	<tr>
    <td width="610" valign="top">
        <table style="margin-left:30px"> 
            <tr>
                <td class="Heading1">List all popup pages</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        <%
                    lang = ""  
                    lang = Request.QueryString("lang")
             
                    if (lang = "") then 
                      Response.Redirect("../CMSLogin.aspx") 
                    end if 
                    
                    if (lang = "EN") then
                        Response.Write("<script type='text/javascript' language='JavaScript1.2' src='HTML/includedefault/pop_core.js'></script>")
                        Response.Write("<script type='text/javascript' language='JavaScript1.2' src='HTML/includedefault/pop_code.js'></script>")
                        Response.Write("<script type='text/javascript' language='JavaScript1.2' src='HTML/includedefault/pop_events.js'></script>")
                    end if

                    if (lang = "FR") then
                        Response.Write("<script type='text/javascript' language='JavaScript1.2' src='HTML/includedefaultFR/pop_core.js'></script>")
                        Response.Write("<script type='text/javascript' language='JavaScript1.2' src='HTML/includedefaultFR/pop_code.js'></script>")
                        Response.Write("<script type='text/javascript' language='JavaScript1.2' src='HTML/includedefaultFR/pop_events.js'></script>")
                    end if
                
                    
				    strDirectoryPath = server.mappath(Request.ServerVariables("PATH_INFO"))
					strDirectoryPath = Replace(strDirectoryPath, "\listAllPage.asp", "\" & lang & "\Popup\")
     				strUrlPath="http://arisara/newCMS/HTML/" & lang & "\Popup\"
    				
    			    Set objFileScripting = CreateObject("Scripting.FileSystemObject")
    				Set objFolder = objFileScripting.GetFolder(strDirectoryPath)
    				Set filecollection = objFolder.Files
    				
    				For Each filename In filecollection
     					Filename= right(Filename,len(Filename)-InStrRev(Filename, "\"))
     					FilenameandLang = Filename & "?lang=" & lang
     					if not (InStr(Filename,"banner.html") > 0) and not (InStr(Filename,"popuphtmlEditor.aspx") > 0) then
     					    Response.Write "<tr>"
    					    Response.Write "<td>"
    					    Response.Write "<A HREF=""" & strUrlPath & FilenameandLang & """ target='_blank'>" & filename & "</A>"
    					    Response.Write "</td></tr>" 
     					end if 
    				Next
			%>
			
			</table>
	</td>
      </tr>
</table><br />
</div>
<!--#include file="EN/footer.html" -->

</body>
</html>




