<%@ Page Language="C#" AutoEventWireup="true" Theme="Default" CodeBehind="SearchAvailability.aspx.cs" Inherits="tikAeroB2C.SearchAvailability" %>
<%@ Register TagPrefix="AvailabilitySearch" TagName="AvailabilitySearch" src="~/UserControls/SearchAvailability.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <link rel="stylesheet" href="App_Themes/Default/style/ui.daterangepicker.css" type="text/css" />
	<link rel="stylesheet" href="App_Themes/Default/style/redmond/jquery-ui-1.7.1.custom.css" type="text/css" title="ui-theme" />   
    <script type="text/javascript" src="Scripts/jQuery/jquery-1.6.1.min.js"></script>   
    <script type="text/javascript" src="Scripts/jQuery/jquery.maskedinput-1.2.2.min.js"></script>   
    <script type="text/javascript" src="Scripts/jQuery/jquery.base64.min.js"></script>
    <script type="text/javascript" src="Scripts/jQuery/jquery.collapse.js"></script>

    <script type="text/javascript" src="Scripts/Main.js"></script>
    <script type="text/javascript" src="Scripts/uxCalendar.js"></script>
    <script type="text/javascript" src="Scripts/Availability.js"></script>
    
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
    window.onbeforeunload = function (oEvent) {
        oEvent = (oEvent) ? oEvent : event;
        if(!oEvent.clientY){
            CloseSession();
        }
        else {
            CloseSession();
        }
    }
   </script>
   <style type="text/css">            
        .calendar_icon            
        {            
            background-image: url('App_Themes/Default/Images/icon_calendar.gif');   
            background-repeat: no-repeat;         
            cursor:pointer;   
            width: 23px;    
            height: 40px; 
            border:0;                             
       } 
	</style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smService" runat="server" ScriptMode="Release" ScriptPath="~/Scripts">
            <Services>
                 <asp:ServiceReference Path="WebService/B2cService.asmx" />
                 <asp:ServiceReference Path="WebService/WebUIRender.asmx" />                
            </Services>
        </asp:ScriptManager>
          <div id="dvAvailabilitySearch" class="SearchBox">
            <AvailabilitySearch:AvailabilitySearch runat="server" ID="AvailabilitySearch1" />

            <div class="BTN-Search">
                <a href="javascript:SearchAvailabilityParameter();" title="<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_38", "Low Fare", _stdLanguage)%>" class="defaultbutton">
                    <span>
                        <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_16", "Book Now", _stdLanguage)%>
                    </span>
                </a>
                 <!--Start Main Contend-->
                  <div id="dvContainer" runat="server"></div>
                  <!--End Main Contend-->
                 
    		</div><!-- End BTN-Search -->
		<div class="clearboth"></div>
        <div id="dvLogon" style="display:none;"><input type="text" id="temp" /></div>
        </div><!-- End ID dvAvailabilitySearch -->

         <!--START script for SearchAvailability calendar Only--> 
    <script language="javascript" type="text/javascript">

        var ResultType = "<%= ResultType %>";
        var CalendarToControls = "<%= CalendarToControls %>";
        var DateFormat = "<%= DateFormat %>";

        $(function () {
            $('#calendar').daterangepicker().click(function () {
                $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
            });

            $('#calendar2').daterangepicker().click(function () {
                $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
            });

        });

    </script>
    <script type="text/javascript" src="Scripts/JQuery/jquery-ui-1.7.1.custom.min.js"></script>
    <script type="text/javascript" src="Scripts/JQuery/daterangepicker.jQuery.js"></script>
     <!--END script for SearchAvailability calendar Only-->
    </form>
</body>
</html>
