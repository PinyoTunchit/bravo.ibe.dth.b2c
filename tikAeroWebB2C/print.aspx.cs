using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C
{
    public partial class print : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                printItinerary();
            }
        }
        private void printItinerary()
        {
            Helper objHelper = new Helper();
            string bookingResult = string.Empty;
            string strLanguageCode = Classes.Language.CurrentCode();
            try
            {
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                if (bookingHeader == null || objHelper.SessionTimeout() == true)
                {
                    string serverPath = @"html/error.html";
                    if (File.Exists(Server.MapPath("~") + serverPath) == false)
                    {
                        Server.Transfer(serverPath);
                    }
                }
                else
                {
                    if (B2CSession.BookingHeader != null)
                    {
                        ServiceClient objClient = new ServiceClient();
                        Library objLibrary = new Library();
                        Helper objXsl = new Helper();
                        System.Xml.Xsl.XslTransform objTransform = null;

                        string reportType = Page.Request["type"];
                        string reportName = string.Empty;
                        string stript = "<script language='javascript'>window.print();</script>";

                        switch (reportType)
                        {
                            case "IL":
                                reportName = "email_light_b2c";
                                break;
                            case "IF":
                                reportName = "email_b2c";
                                break;
                        }

                        objClient.objService = B2CSession.AgentService;
                        bookingResult = objClient.GetItinerary(bookingHeader.booking_id.ToString(),
                                                               strLanguageCode, 
                                                               string.Empty, 
                                                               string.Empty);


                        if (string.IsNullOrEmpty(bookingResult) == false)
                        {
                            string serverPath = Server.MapPath("~") + @"\xsl\mail\";

                            if (File.Exists(serverPath + strLanguageCode.ToLower() + "_" + reportName + ".xsl") == false)
                            {
                                //No language file found used en as default file
                                objTransform = objXsl.GetXSLMail("en", reportName);
                                bookingResult = objLibrary.RenderHtml(objTransform, null, bookingResult);
                            }
                            else
                            {
                                objTransform = objXsl.GetXSLMail(strLanguageCode.ToLower(), reportName);
                                bookingResult = objLibrary.RenderHtml(objTransform, null, bookingResult);
                            }

                            objClient.objService = null;

                            //Show Print Result
                            Response.Write(bookingResult + stript);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                objHelper.SendErrorEmail(ex, bookingResult);
            }
        }
    }
}
