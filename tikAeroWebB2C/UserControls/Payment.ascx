<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Payment.ascx.cs" Inherits="tikAeroB2C.UserControls.Payment" %>
<!--button-->
	<div class="StepContent">
		<ul>

			<li>
				<li class="StepNumber">1</li>
				<li class="StepName"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Step 2", _stdLanguage)%></li>
			</li>
			
			<li>
				<li class="StepNumber">2</li>
				<li class="StepName"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Step 3", _stdLanguage)%></li>
			</li>
			
			<li>
				<li class="StepNumberActive">3</li>
				<li class="StepName"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Step 4", _stdLanguage)%></li>
			</li>
			
			<li>
				<li class="StepNumber">4</li>
				<li class="StepName"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_1", "Step 5", _stdLanguage)%></li>
			</li>
		</ul>
		<div class="clear-all"></div>
    </div>

	<div class="Payment" id="dvPayment">
		<div class="Topic HeaderTopic">
			<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_58", "Please select your method of payment:", _stdLanguage)%>
		</div>
		
		<div class="clear-all"></div>
		
		<div class="PaymentMethod">
			<div id="dvPaymentTab" runat="server"></div>
		</div>
		
		<div class="clear-all"></div>
		
		<div class="PaymentMethodDetail">
			<!-- Credit Card details -->
			
			<div class="TabCreditCard" id="CreditCard">
				<div class="boxborder">
					<div class="displaycard">
						<img src="App_Themes/Default/Images/creditcard.png" alt="We accept Mastercard, Visa, Visa Electron, American Express, Switch, Solo, Delta and Discover" title="We accept Mastercard, Visa, Visa Electron, American Express, Switch, Solo, Delta and Discover">
					</div>
				
					<div class="PassengerDetails">
						
							<div class="Topic PaymentTopic">
                                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_3", "Credit Card Detail", _stdLanguage)%>
                            </div>
			
							<div class="ContactDetail">
								<label for="ctl00_optCardType">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_7", "Credit Card Type", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <select id="optCardType" onchange="activateCreditCardControl();" runat="server">
                                    <option value="" class="watermarkOn"></option>
                                  </select>
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="txtCardNumber">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_10", "Credit Card Number", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <input id="txtCardNumber" type="text" />
						          <div class="mandatory">*</div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="txtNameOnCard">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_6", "Name On Card", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <input id ="txtNameOnCard" type="text" onkeypress="EmptyErrorMessage();"/>
						          <div class="mandatory">*</div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail" id="dvIssueNumber">
								<label for="txtIssueNumber">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_8", "Issue Number", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
									<input id="txtIssueNumber" type="text" />
									<div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail" id="dvIssuDate">
								<label for="optIssueMonth">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_9", "Issue Date(if present)", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
									<select id="optIssueMonth" class="CardExpiryDate">
                                        <option value="" class="watermarkOn"></option>
										<option selected="selected" value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									
									<select id="optIssueYear" class="CardExpiryDate">
                                        <option value="" class="watermarkOn"></option>
										<option selected="selected" value="2008">2008</option>
										<option value="2009">2009</option>
										<option value="2010">2010</option>
										<option value="2011">2011</option>
										<option value="2012">2012</option>
										<option value="2013">2013</option>
										<option value="2014">2014</option>
										<option value="2015">2015</option>
										<option value="2016">2016</option>
										<option value="2017">2017</option>
										<option value="2018">2018</option>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
									</select>
	
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							
							<div class="ContactDetail" id="dvExpiryDate">
								<label for="optExpiredMonth">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_11", "Expiry Date", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
									<select id="optExpiredMonth" class="CardExpiryDate" onchange="EmptyErrorMessage();">
                                        <option value="" class="watermarkOn"></option>
										<option selected="selected" value="01">1</option>
										<option value="02">2</option>
										<option value="03">3</option>
										<option value="04">4</option>
										<option value="05">5</option>
										<option value="06">6</option>
										<option value="07">7</option>
										<option value="08">8</option>
										<option value="09">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									
									<select id="optExpiredYear" class="CardExpiryDate" onchange="EmptyErrorMessage();">
                                        <option value="" class="watermarkOn"></option>
			                        </select>
	
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail Left" id="dvCvv">
								<label for="txtCvv">
						            CVV:
								</label>
			
								<div class="InformationInput">
									<input id="txtCvv" type="text" class="InputboxCVV" maxlength="4" onkeypress="EmptyErrorMessage();"/>
									<div class="mandatory">
										<div class="DisplayCVV"></div>
									</div>
								</div>

							</div>

							<div class="clear-all"></div>
							
							<div class="SpecialInformation">
							
								<a href="javascript:CreateWnd('HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/cdcSecurity.html', 500, 588, false);">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_25", "Information on credit card security", _stdLanguage)%>
								</a>
							
							</div>
						
						<div class="clear-all"></div>
					</div>
		
					<!-- Address Detail Section -->
					<div class="PassengerDetails" id="dvAddress">
						<div class="Topic PaymentTopic">Address Detail</div>
						
						<div class="ContactDetail">
							<label for="chkMyname">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_5", "Use contact address", _stdLanguage)%>
							</label>
				        
					        <div class="checkbox">
					          <input id="chkMyname" type="checkbox" onclick="LoadMyName();"/>
					          <div class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="txtAddress1">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_16", "Address Line 1", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtAddress1" type="text" onkeypress="EmptyErrorMessage();"/>
					          <div class="mandatory">*</div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="txtAddress2">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_17", "Address Line 2 (Optional)", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtAddress2" type="text"/>
					          <div class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="txtCity">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_20", "Province/County", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtCity" type="text" onkeypress="EmptyErrorMessage();"/>
					          <div class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="txtCounty">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_19", "Town/City", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtCounty" type="text"/>
					          <div class="mandatory">*</div>
					        </div>
							<div class="clear-all"></div>
						</div>
		
						<div class="ContactDetail">
							<label for="txtPostCode">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_22", "Postal Code", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtPostCode" type="text" onkeypress="EmptyErrorMessage();"/>
					          <div class="mandatory">*</div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="ctl00_optCountry">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_23", "Country", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
								<select id="optCountry" runat="server" onchange="EmptyErrorMessage();">
									<option value="" class="watermarkOn"></option>
								</select>
								<div class="mandatory">*</div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
					</div>
					
					<div id="dvCCConfirm" class="optional">
						<div class="Remember FirstLine">
							<input id="chkPayCreditCard" type="checkbox" />
							<label for="chkPayCreditCard">
								I have read, understood and accept all
							</label>
							<a href="HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/Agreement.html" target="_blank">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_34", "Terms and Conditions.", _stdLanguage)%>
							</a>
						</div>
						<div class="clear-all"></div>
					</div>
					
					<div class="clear-all"></div>
				</div>
				<div id="dvCCButton" class="BTN-Search">
						<div class="ButtonAlignLeft">
							<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
		                        <span>
	                                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
		                        </span>
							</a>
						</div>
						

	                    <div class="ButtonAlignRight"> 
							<a href="javascript:paymentCreditCard();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>" class="defaultbutton">
		                        <span>                    
	                                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>
		                        </span>
							</a>
	                    </div>

	            </div>
				<div class="clear-all"></div>
			</div>
			
			<div class="clear-all"></div>
	
		  <span class="asterisk">
		    <span id="spnError"></span>
		  </span>
			<div class="clear-all"></div>
			
			<!-- Voucher -->
			<div class="TabVoucher" id="Voucher">
				<div class="boxborder">
					<div class="ContactDetail fontB FirstLine">
						<label for="txtVoucherNumber">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_27", "Voucher Number", _stdLanguage)%>
						</label>
			        
				        <div class="InformationInput">
				          <input id="txtVoucherNumber" type="text" maxlength="10"/>
				          <div class="mandatory"></div>
				        </div>
						<div class="clear-all"></div>
					</div>

					<div class="ContactDetail fontB">
						<label for="txtVoucherPassword">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_28", "Password", _stdLanguage)%>
						</label>
			        
				        <div class="InformationInput">
				          <input id="txtVoucherPassword" type="password" />
				          <div class="mandatory"></div>
				        </div>
		
					</div>
	
					<div class="LastLine">
						<a href="javascript:validateVoucher(document.getElementById('txtVoucherNumber').value,document.getElementById('txtVoucherPassword').value);" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_29", "Validate Voucher", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_29", "Validate Voucher", _stdLanguage)%>
							</span>
						</a>
						<div class="clear-all"></div>
					</div>
					<div class="clear-all"></div>
					
					<span class="asterisk">
						<span id="spnVoucherError"></span>
					</span>
	
					<div id="dvVoucherDetail"></div>
					
					<div class="clear-all"></div>
					
				    <div id="dvMultipleConfirm">
	                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_85", "Your Voucher amount is not enough. Please click process to pay addition payment with credit card.", _stdLanguage)%>
                	</div>
					
					<div class="clear-all"></div>
				
					<div class="optional">
						<div class="Remember FirstLine">
							<input id="chkPayVoucher" type="checkbox" />
							<label for="chkPayVoucher">
								I have read, understood and accept all
							</label>
							<a href="HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/Agreement.html" target="_blank">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_34", "Terms and Conditions.", _stdLanguage)%>
							</a>
							
						</div>
						<div class="clear-all"></div>
					</div>
				</div>
				
				<div class="BTN-Search">
                    <div class="ButtonAlignLeft">
						<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
	                        <span>
	                            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
	                        </span>
						</a>
                    </div>

                    <div id="dvPayVoucher" class="ButtonAlignRight">
						<a href="javascript:PaymentMultipleForm();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>" class="defaultbutton">
	                        <span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>
	                        </span>
						</a>
                    </div>
                </div>
			</div>

			<div class="clear-all"></div>		
		  	
			<!-- Book now -->
			<div class="TabCrAccount" id="BookNow">
				<div class="boxborder">
					<div class="Topic PaymentTopic FirstLine">Note for ATM Payment:</div>
					<ul>
						<li>* <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_67", "Payment code is for ticket payment via ATM.", _stdLanguage)%></li>
						<li>* <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_68", "Payment must be completed within 3 hours after booking created.", _stdLanguage)%></li>
						<li>* <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_69", "Your booking will be cancelled automatically if you not make any payment within the determined period.", _stdLanguage)%></li>
					</ul>
					
					<div class="clear-all"></div>
				
					<div class="optional">
						<div class="Remember FirstLine">
							<input id="chkPayPostPaid" type="checkbox" />
							<label for="chkPayPostPaid">
								I have read, understood and accept all
							</label>
							<a href="HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/Agreement.html" target="_blank">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_34", "Terms and Conditions.", _stdLanguage)%>
							</a>
							
						</div>
						<div class="clear-all"></div>
					</div>
					<div class="clear-all"></div>
				</div>
				<div class="BTN-Search">
					<div class="ButtonAlignLeft">
						<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
							</span>
						</a>
					</div>
				
					<div class="ButtonAlignRight">
						<a href="javascript:SaveBookedNowPayLater();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>
							</span>
						</a>
					</div>
				</div>
			</div>
		
			<div class="clear-all"></div>

            <!-- Pay at Bank -->
			<div class="TabCrAccount" id="PayAtBank">
				<div id="graygradient">
					<div class="PassengerDetails paymentinnercontent">
						<div class="PaymentContentDetail">
						<div class="paymentcontent">
                            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_143", "You've almost finished booking your flights! We recommend that you double check your information provided is accurate and the total amount due before you select the mode of payment that best suits you.", _stdLanguage)%>
						</div>
						
						<div class="paymentcontent">
                            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_146", "If you chose to pay via Book Now Pay Later option, you have to contact Gambia bird to pay the outstanding balance.", _stdLanguage)%>
						</div>
                        <div class="paymentcontent">
                            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_145", "You will not be able to travel until the outstanding balance is piad.", _stdLanguage)%>
						</div>
                        <div style="text-align: left; color: #ff0000; font-size: 12px; font-weight: bold; padding-left: 30px;">
                            <div><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_xx", "The booking has to be paid before ", _stdLanguage)%></div>
                            <div id="dvDisplayTimeLimit" runat="server"></div>
                        </div>
					</div>
						<div class="clear-all"></div>
					</div>

					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>

                    <div class="clear-all"></div>
				</div>
				
                <div class="clear-all"></div>
				
				<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BTN-Search">
					<div class="ButtonAlignLeft">
						<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
							</span>
						</a>
					</div>
					
					<div class="PurchaseConfirm">
							<div class="ConfirmCheckbox">
							
								<input id="chkPayAtBank" type="checkbox" />
								<a href="html/Agreement.html" target="_blank">
								
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_81", "I have read, understood and accept all Terms and Conditions associated with this Reservation.", _stdLanguage)%>
								
                                </a>
							</div>
							<div class="clear-all"></div>
					</div>

					<div class="ButtonAlignRight">
						<a href="javascript:SavePayAtBank();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>
							</span>
						</a>
					</div>
					
					
					
				</div>
			</div>
            <div class="clear-all"></div>
		
			<!-- External Payment -->
			<div class="TabExternal" id="External">
				<div class="boxborder">
				
					<div class="ContactDetail FirstLine">
						<label for="txtReferenceCode">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_59", "User ID KlikBCA", _stdLanguage)%>
						</label>
			        
				        <div class="InformationInput">
				          <input id="txtReferenceCode" type="text" onkeypress="EmptyErrorMessage();" />
				          <div class="mandatory">*</div>
				        </div>
						<div class="clear-all"></div>
					</div>
					
					<div class="ContactDetail">
						<label for="txtConfirmReferenceCode">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_60", "Confirm ID KlikBCA", _stdLanguage)%>
						</label>
			        
				        <div class="InformationInput">
				          <input id="txtConfirmReferenceCode" type="text" onkeypress="EmptyErrorMessage();" />
				          <div class="mandatory">*</div>
				        </div>
						<div class="clear-all"></div>
					</div>
					
					<div class="ContactDetail">
						<label for="txtSecurityCode">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_61", "Security Code", _stdLanguage)%>
						</label>
			        
				        <div class="InformationInput">
				          <input id="txtSecurityCode" type="text" onkeypress="EmptyErrorMessage();" />
				          <div class="mandatory">*</div>
				        </div>
						<div class="clear-all"></div>
					</div>
					
                    <!-- Captcha Image -->
					<div class="SpecialInformation">
			          <img id="imgSecurity" alt="Please enter the security code as shown in the image." title="Please enter the security code as shown in the image." src="captimage.aspx?r=<%= System.Guid.NewGuid().ToString("N")  %>" vspace="5" />
					</div>
					
					<div class="ButtonAlignLeft">
						<a href="javascript:refreshCaptcha();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_62", "Refresh", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_62", "Refresh", _stdLanguage)%>
							</span>
						</a>
					</div>
					
					
					<div class="clear-all"></div>
					
					<div class="optional">
						<div class="Remember FirstLine">
							<input id="chkPayExternal" type="checkbox" />
							<label for="chkPayExternal">
								I have read, understood and accept all
							</label>
							<a href="HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/Agreement.html" target="_blank">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_34", "Terms and Conditions.", _stdLanguage)%>
							</a>
							
						</div>
						<div class="clear-all"></div>
					</div>
					<div class="clear-all"></div>
				</div>
				
				<div class="clear-all"></div>
			
				<div class="BTN-Search">
					<div class="ButtonAlignLeft">
						<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
							</span>
						</a>
					</div>
				
					<div class="ButtonAlignRight">
						<a href="javascript:paymentExternal();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>
							</span>
						</a>
					</div>
				</div>
				
				<div class="clear-all"></div>
				<span class="asterisk">
					<span id="spnExternalError"></span>
				</span>
			</div>

            <!-- ******************************** -->
            <div class="clear-all"></div>
            <div class="TabCreditCard" id="ESewa" style="display:none;">
                <div class="TextCreditCard">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_655", "eSewa Debit Card", _stdLanguage)%>
                </div>
                <div class="alert">Debit Card</div>
                <div class="clear-all"></div>
                <div class="Confirm">
                    &#160;&#160;<input id="chkESewa" type="checkbox" />
                    <a href="javascript:CreateWnd('HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/Agreement.html', 640, 800, false);">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_34", "I have read, understood and accept all Terms and Conditions associated with this Reservation.", _stdLanguage)%>
                    </a>
                </div>
                <div class="BTN-Search">
                    <div class="ButtonAlignLeft">
						<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
	                        <span>
	                            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
	                        </span>
						</a>
                    </div>

                    <div id="Div1" class="ButtonAlignRight">
						<a href="javascript:paymentESewa();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>" class="defaultbutton">
	                        <span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>
	                        </span>
						</a>
                    </div>
                </div>
            </div> 
            <!-- ******************************** -->   
				
		</div>
		<div class="clear-all"></div>
	</div>


    <!-- Booking Summary -->
    <!--<div class="TabCostSummary">
	    <div id="dvFareSummary" runat="server"></div>
    </div>-->
  
    <div class="TabCostSummary" id="dvFfpRedeemSummary" style="display:none;">
        <div class="TabCostSummaryHeader"></div>
        <div class="TextCostSummary">
         <div class="TextCostSummaryLeft" id="dvRedeemClientId">00000000</div>
        <div class="TextCostSummaryRight"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_70", "Points", _stdLanguage)%></div>
        </div>
	    <div class="clear-all"></div>
	    <div class="TabCostPointsLeft"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_71", "Current Points", _stdLanguage)%></div>
	    <div class="TabCostPointsRight" id="dvRedeemCurrentPoint">30</div>
	    <div class="TabCostPointsLeft"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_72", "Redeem Points", _stdLanguage)%></div>
	    <div class="TabCostPointsRight" id="dvRedeemPoint">24</div>
	    <div class="TabCostPointsLeft"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_73", "Balance Points", _stdLanguage)%></div>
	    <div class="TabCostPointsRight" id="dvRedeemBalance">6</div>
	    <div class="TabCostRemark "><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_74", "*Points will be deducted when tickets are issued.", _stdLanguage)%></div>
         <div class="TabCostSummaryFooter"></div>
    </div>
   
    <div class="TabCostSummary" id="dvInsurance" style="display:none;" >
        <input type="hidden" id="hdProductID" />
        <div class="TabCostSummaryHeader">Insurance</div>
	    <div class="clear-all"></div>
	    <div class="TabCostPointsLeft" id="dvInsuranceLabel">Quote</div>
	    <div class="TabCostPointsRight" id="dvInsuranceCost"></div>
        <div class="TabCostSummaryFooter"></div>
    </div>
    <div class="TabCostSummary" id="dvAccualSummary"></div>

      <!-- For Insurance-->
     <!-- <div class="Step4PanelLeft" >
        <input type="radio" name="rdInsrance" value="Y" checked id="rdYes" onclick="UseInsuarance()"/><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_64", "Yes", _stdLanguage)%> <br />
        <input type="radio" name="rdInsrance" value="N" id="rdNo" onclick="NoInsuarance()" /><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_65", "No", _stdLanguage)%> <br/>
        <div class="BTN-Search">
            <div class="ButtonAlignRight" id="btnConfirm"> 
			    <a href="" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_63", "Confirm", _stdLanguage)%>" onclick="ShowInsurance();return false;" class="defaultbutton">
		            <span>                    
	                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_63", "Confirm", _stdLanguage)%>
		            </span>
			    </a>
	        </div>
        </div>
     </div> -->

    <!-- For Contact Detail On PaymentStep-->
    <!--<div class="Step4PanelLeft" >
        <input id="txtClientProfileId" type="hidden" runat="server"/>
        <input id="txtClientNumber" type="hidden" runat="server"/>
        <ul class="Step4Top">
            <li class="header">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_3", "Main Contact Details (details in English only)", _stdLanguage)%>
            </li>
            <li class="RememberTop">
                <input id="chkLeadPassenger" type="checkbox" onclick="GetLeadPassenger();"/>                
			    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_66", "Use Lead Passenger Details", _stdLanguage)%>
            </li>
            <li class="PassengerType" style="display:none;"><input id="rdoPrivate" name="optContactType" type="radio" value="" checked="checked" onclick="ShowContactTypeInfo();"/><%=tikAeroB2C.Classes.Language.Value("", "Private", _stdLanguage)%></li>
            <li class="PassengerType" style="display:none;"><input id="rdoBusiness" name="optContactType" type="radio" value="" onclick="ShowContactTypeInfo();" /><%=tikAeroB2C.Classes.Language.Value("", "Business", _stdLanguage)%></li>
            <li id="liBusinessTypeMsg" class="TypeError" style="display:none;">
                <%=tikAeroB2C.Classes.Language.Value("", "Should you wish to use the Reverse Charge method for VAT settlement, please register for the InterSky Corporate Portal.", _stdLanguage)%> 
            </li>
            <li class="Remember">
                <input id="" type="checkbox" style="display:none;" />
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_65", "Passenger address is also billing address?", _stdLanguage)%>
            </li>
            <li id="liCompanyName" style="display:none;">
                <span class="title">
                	    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_66", "Company Name", _stdLanguage)%>
                </span>
                <input id="txtCompanyName" type="text" runat="server" />
                <span id="spErrCompany" class="RequestStar">*</span>
            </li>
            <li id="liVat" style="display:none;">
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_67", "VAT", _stdLanguage)%>
                </span>
                <input id="txtVat" type="text" runat="server"/>
                <span id="spErrVat" class="RequestStar">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_4", "Title", _stdLanguage)%>
                </span>
                <select id="stContactTitle" runat="server"></select>
                <span class="RequestStar">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_5", "First Name", _stdLanguage)%>
                </span>
                <input id="txtContactFirstname" type="text" runat="server"/>
                <span id="spErrFirstname" class="RequestStar">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_6", "Surname", _stdLanguage)%>
                </span>
                <input id="txtContactLastname" type="text" runat="server" />
                <span id="spErrLastname" class="RequestStar">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_7", "Mobile No", _stdLanguage)%>
                </span>
                <input id="txtContactMobile" type="text" runat="server" />
                <span id="spErrMobile" class="RequestStar"></span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_8", "Home No", _stdLanguage)%>
                </span>
                <input id="txtContactHome" type="text" runat="server" />
                <span id="spErrHome" class="RequestStar">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_9", "Business No", _stdLanguage)%>
                </span>
                <input id="txtContactBusiness" type="text" runat="server" />
                <span id="spnErrBusiness" class="RequestStar"></span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_10", "Email", _stdLanguage)%>
                </span>
                <input id="txtContactEmail" type="text" runat="server" />
                <span id="spErrEmail" class="RequestStar">*</span>
            </li>
            <div class="ContactDetail">
                <label for="ctl00_txtMobileEmail">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_73", "Mobile Email", _stdLanguage)%>
                </label>
                <input id="txtMobileEmail" type="text" runat="server" maxlength="60" />
                <div id="spMobileEmail" class="mandatory"></div>
                <div class="clear-all"></div>
            </div>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_11", "Address 1", _stdLanguage)%>
                </span>
                <input id="txtContactAddress1" type="text" runat="server" />
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_12", "Address 2", _stdLanguage)%>
                </span>
                <input id="txtContactAddress2" type="text" runat="server" />
                <span id="spErrAddress1" class="RequestStar">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_15", "Zip Code", _stdLanguage)%>
                </span>
                <input id="txtContactZip" type="text" runat="server" />
                <span id="spErrZip" class="RequestStar">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_13", "Town/City", _stdLanguage)%>
                </span>
                <input id="txtContactCity" type="text" runat="server" />
                <span id="spErrCity" class="RequestStar">*</span>
            </li>
            <li style="display:none;">
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_14", "State", _stdLanguage)%>
                </span>
                <input id="txtContactState" type="text" runat="server" />
                <span id="spErrState" class="none">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_16", "Country", _stdLanguage)%>
                </span>
                <select id="stContactCountry" runat="server">
                </select>
                <span class="RequestStar">*</span>
            </li>
            <li>
                <span class="title">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_58", "Language", _stdLanguage)%>
                </span>
                <select id="stContactLanguage" name="select2" runat="server"></select>
                <span class="RequestStar">*</span>
            </li>
            <div class="space5"></div>
            <li class="Remember">
                <input id="chkRemember" name="Input" type="checkbox" value="" /> 
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_17", "Remember these details for next visit.", _stdLanguage)%>
                <a href="javascript:CreateWnd('HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/en_CookieDetail.html', 500, 260, false);">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_61", "more", _stdLanguage)%>
                </a>
            </li>
            <li class="Remember">
                <input id="chkNewsLetter" name="Input" type="checkbox" /> 
			    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_68", "Yes, add my mailaddress for newsletter", _stdLanguage)%>
            </li>
            <div class="space10"></div> 
            <div class="clearboth"></div>   
        </ul>
    </div>-->

    <!-- EFT Control-->
    <div class="TabELV" id="dvEft" style="display:none;">
        <div class="TextELV">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_75", "ELV", _stdLanguage)%>
        </div>
    
        <div class="LeftDesc">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_76", "Bank Name", _stdLanguage)%> 
        </div>
        <div class="RightDesc">
            <input id="txtElvBankName" class="Inputbox" type="text" />
            <span class="RequestStar">*</span>
        </div>
	    <div class="clearboth"></div>
    
        <div class="LeftDesc">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_77", "Bank Code", _stdLanguage)%>
        </div>
        <div class="RightDesc">
            <input id="txtElvBankCode" class="Inputbox" type="text" />
            <span class="RequestStar">*</span>
        </div>
	    <div class="clearboth"></div>
    
        <div class="LeftDesc"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_78", "Account Number", _stdLanguage)%></div>
        <div class="RightDesc">
            <input id="txtElvAccountNumber" class="Inputbox" type="text" />
            <span class="RequestStar">*</span>
        </div>
	    <div class="clearboth"></div>
    
        <div class="LeftDesc"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_79", "Account Holder", _stdLanguage)%></div>
        <div class="RightDesc">
            <input id="txtElvAccountHolder" class="Inputbox" type="text" />
            <span class="RequestStar">*</span>
        </div>
	    <div class="clearboth"></div>
    
        <div class="ELVerror"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_80", "Please input Firstname and Lastname with space in between.", _stdLanguage)%></div>
	    <div class="clearboth"></div>
    
        <div class="LeftDesc">Country</div>
        <div class="RightDesc">
            <select id="optEftCountry" class="Selectbox" runat="server">
                <option value="" class="watermarkOn"></option>
            </select>
            <span class="asterisk">*</span>
        </div>
	    <div class="space15"></div>
    
        <div class="clearboth"></div>
        <div class="Confirm">
     	    <input id="chkElvCondition" type="checkbox" />
     	    <a href="http://www.intersky.biz/bausteine.net/file/showfile.aspx?downdaid=8893&domid=1047&fd=2" target="_blank">
        	    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_81", "I have read, understood and accept all Terms and Conditions associated with this Reservation.", _stdLanguage)%>
     	    </a>
            <div class="clearboth"></div>
            <span class="asterisk">
    	        <span id="spnEftError"></span>
  	        </span>
        </div>
        <div id="dvElvButton" class="BTNPayment" name="dvELV">
          <!--button-->
          <div class="ButtonAlignRight">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent" onclick="paymentEft();">
                 <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
          </div>
          <!-- end button-->
          <!--button-->
          <div class="ButtonAlignLeft">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent" onclick="GetPassengerDetail();">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Back", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
          </div>
          <!-- end button-->
          <!--button-->
          <div class="ButtonAlignMiddleVoucher">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent" onclick="loadHome();">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_49", "Cancel", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
          </div>
          <!-- end button-->
          <div class="clearboth"></div>
        </div>
    </div>
