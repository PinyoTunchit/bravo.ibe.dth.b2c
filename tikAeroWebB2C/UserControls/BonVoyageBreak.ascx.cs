using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Xml.XPath;
using System.Text;
using System.IO;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;


namespace tikAeroB2C.UserControls
{
    public partial class BonVoyageBreak : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                FillContactInformation();
            }
        }

        private void FillContactInformation()
        {
            try
            {
                ListItem objList;
                Library objLi = new Library();

                string titleRcd = string.Empty;

                Titles objTitles = CacheHelper.CachePassengerTitle();

                for (int i = 0; i < objTitles.Count; i++)
                {
                    objList = new ListItem();
                    titleRcd = objTitles[i].title_rcd;
                    objList.Value = titleRcd + "|" + objTitles[i].gender_type_rcd;
                    objList.Text = objTitles[i].display_name;

                    ddlTitle.Items.Add(objList);
                    objList = null;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
    }
}