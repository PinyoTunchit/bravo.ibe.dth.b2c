using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.XPath;
using System.Text;
using System.IO;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class NewsRegisterDetail : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                FillRegisterInformation();
            }
        }

        private void FillRegisterInformation()
        {
            Helper objHelper = new Helper();
            try
            {
                ListItem objList;

                Library objLi = new Library();

                if (B2CSession.AgentService == null)
                {
                    B2CVariable objUv = B2CSession.Variable;
                    if (objUv == null)
                    {
                        objUv = new B2CVariable();
                    }
                    objHelper.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;
                }

                if (txtIPaddress != null) txtIPaddress.Value = HttpContext.Current.Request.UserHostAddress;
                if (txtBrowser != null) txtBrowser.Value = Request.ServerVariables.Get("HTTP_USER_AGENT");
                
                if (ddlCountryALL != null)
                {
                    Countries objCountries = CacheHelper.CacheCountry();
                    for (int i = 0; i < objCountries.Count; i++)
                    {
                        objList = new ListItem();
                        objList.Value = objCountries[i].country_rcd;
                        objList.Text = objCountries[i].display_name;

                        ddlCountryALL.Items.Add(objList);
                    }
                }
                string serverPath = Server.MapPath("~") + B2CSetting.CountryPath;
                DataSet ds = new DataSet();
                ds.ReadXml(Server.MapPath("~") + B2CSetting.CountryPath);

                ddlCountry.DataTextField = "name";
                ddlCountry.DataSource = ds.Tables[0];
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, (new ListItem("-- Choose country --", "-1")));
            }
            catch (Exception ex)
            {
                objHelper.SendErrorEmail(ex, string.Empty);
                throw ex;
            }

        }        
    }
}