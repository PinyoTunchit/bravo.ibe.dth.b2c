using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace tikAeroB2C.UserControls
{
    public partial class MyBooking : System.Web.UI.UserControl
    {
        public string _parameter;
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        #region Page Load Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper objXsl = new Helper();
            if (IsPostBack == false)
            {
                try
                {
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                    if (B2CSession.Client != null)
                    {
                        Library objLi = new Library();
                        Client objClient = B2CSession.Client;
                        Booking objBooking = new Booking();
                        objBooking.objService = B2CSession.AgentService;

                        if (objBooking.ReadBookingHistory(objClient.client_profile_id.ToString()) == true)
                        {
                            //Create Paging.
                            WebService.B2cService service = new tikAeroB2C.WebService.B2cService();
                            int rowPerPage = B2CSetting.RowsPerPageBooking;
                            bool usePaging = B2CSetting.UsePaging;

                            string xmlLife = "";
                            string xmlHistory = "";

                            B2CSession.xmlHistory = objBooking.FlownXml();
                            B2CSession.xmlLife = objBooking.ActiveXml();

                            if (usePaging)
                            {
                                xmlHistory = service.GridPagingWithXML(B2CSession.xmlHistory, rowPerPage, 0, true);
                                xmlLife = service.GridPagingWithXML(B2CSession.xmlLife, rowPerPage, 0, true);
                            }
                            else
                            {
                                if (rowPerPage == 0)
                                {
                                    xmlHistory = objBooking.FlownXml();
                                    xmlLife = objBooking.ActiveXml();
                                }
                                else
                                {
                                    xmlHistory = service.GridPagingWithXML(B2CSession.xmlHistory, rowPerPage, 0, false);
                                    xmlLife = service.GridPagingWithXML(B2CSession.xmlLife, rowPerPage, 0, false);
                                }

                            }
                            System.Xml.Xsl.XslTransform objTransform = null;
                            System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                            objTransform = objXsl.GetXSLDocument("BookingList");
                            dvLifeBooking.InnerHtml = objLi.RenderHtml(objTransform, objArgument, xmlLife);

                            objTransform = objXsl.GetXSLDocument("BookingHistory");
                            dvHistory.InnerHtml = objLi.RenderHtml(objTransform, objArgument, xmlHistory);
                        }
                    }
                }
                catch (Exception ex)
                {
                    objXsl.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }
        }

        #endregion
    }   
}