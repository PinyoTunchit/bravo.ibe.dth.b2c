<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyBooking.ascx.cs" Inherits="tikAeroB2C.UserControls.MyBooking" %>
<div class="FFPWrapper">
    <div class="BoxGray">
      <div class="BoxGrayTop"></div>
      <div class="BoxGrayContent">
  	    <div class="BoxGrayContentHeader">
  	        <%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_1", "My Personal Booking Overview", _stdLanguage)%>
  	    </div>
  	    <div class="clearboth"></div>
        <%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_2", "The information contained...", _stdLanguage)%>
	  </div>
      <div class="BoxGrayEnd"></div>
    </div>
    <div class="clearboth"></div>
        
    <div class="BoxGray">
      <div class="BoxGrayTop"></div>
      <div class="BoxGrayContent">
  	    <div class="BoxGrayContentHeader"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_3", "My Booking Search", _stdLanguage)%></div>
            <div class="TextGray"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_14", "My Booking Reference Number", _stdLanguage)%></div>
            <input id="txtBookingCode" type="text" class="BookingRef" onkeypress="return SubmitEnterSearch(this,event)"/> 
   	        <div class="ButtonAlignMiddleUserInfo">
                <div class="buttonCornerLeft"></div>
          	    <div class="buttonContent"><a href="Javascript:MyBookingSearch();"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_4", "Search", _stdLanguage)%></a></div>
                <div class="buttonCornerRight"></div>
            </div>
		    <div class="BookingCode"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_5", "Booking Code (Max 6 characters)", _stdLanguage)%></div>                 
        </div>
      <div class="BoxGrayEnd"></div>
    </div>
    <div class="clearboth"></div>
    <div id="dvResultBox" style="display:none;">
        <div class="BoxGray">
            <div class="BoxGrayTop"></div>
                <div class="BoxGrayContent">
                    <div class="BoxGrayContentHeader"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_6", "Search Result", _stdLanguage)%></div>
                    <div id="dvSearch"></div>               
                </div> 
            <div class="BoxGrayEnd"></div>
        </div> 
    </div> 
    <div class="clearboth"></div>
       
    <div id="dvBookingBox">    
        <div class="BoxGray" >
            <div class="BoxGrayTop"></div>
            <div class="BoxGrayContent">
                <div class="BoxGrayContentHeader"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_7", "Live Bookings", _stdLanguage)%></div>
                <div id="dvLifePage"/> 
                <div id="dvLifeBooking" runat="server" />                   
            </div>
            <div class="BoxGrayEnd"></div>
        </div>
        <div class="clearboth"></div>
        	
        <div class="BoxGray" >
	        <div class="BoxGrayTop"></div>
                <div class="BoxGrayContent">
                    <div class="BoxGrayContentHeader"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_8", "Booking History", _stdLanguage)%></div>
                    <div id="dvPaging"/> 
                    <div id="dvHistory" runat="server" />                  
                </div>
            <div class="BoxGrayEnd"></div>
        </div>
    </div>
</div>