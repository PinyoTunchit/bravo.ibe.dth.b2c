using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Threading;
using System.Globalization;
using System.Collections.Specialized;
using tikAeroB2C;

namespace tikAeroB2C.UserControls
{
    public partial class Calendar : System.Web.UI.UserControl
    {
        #region -* Declaretion *-
        private System.Web.UI.WebControls.Calendar Tempcal = new System.Web.UI.WebControls.Calendar();


        //------------------------------------------

        public enum uxControlType
        {
            DropdownList = 0,
            Calendar = 1,
            HtmlImage = 2
        }
        public string LimitYear = "";
        public string StartMonth = "";
        public string IsBeginCurrentMont = "1";
        public string RenderType = "0";
        public DataSet dsDateFlightInMonth;

        DateTime tDate;

        public uxControlType RenderCtrlType = uxControlType.DropdownList;
        public bool showDateFlight = false;
        public int day = 0;
        public int month = 0;
        public int year = 0;
        public int isdefaultdate = 0;
        public int isShowLastDate = 0;
        public bool isonlyonemonth = true;
        public bool isnextcurrentmonth = false;
        public string calendartype = "none";
        public string FirstDayOfWeek = "MONDAY";
        public string strFrom = "";
        public string strTo = "";
        public string flightType = "";
        public string monthFormat = "";
        public string id = "";
        public string mxMY = "";
        public string mnMY = "";

        public string Currentculture = "";
        private DateTime prevDate = new DateTime(1900, 1, 1);
        bool set = false;
        int calType = 1;

        #endregion

        //---------------------------------------------------------------------------------

        public Calendar()
        {
            this.Tempcal.DayRender += new System.Web.UI.WebControls.DayRenderEventHandler(this.Tempcal_DayRender);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        private void InitializeComponent()
        {
            this.Tempcal.DayRender += new System.Web.UI.WebControls.DayRenderEventHandler(this.Tempcal_DayRender);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        // for jquery ui
        public string GetHTML(bool IsRenderCalendarIcon, bool IsVisibleCalendarIcon)
        {
            //RenderCtrlType.
            //culture
            if (tikAeroB2C.B2CSession.LanCode != null)
            {
                CultureInfo ci = new CultureInfo(tikAeroB2C.B2CSession.LanCode);
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;
            }
            else
            {
                if (Currentculture != "")
                {
                    CultureInfo ci = new CultureInfo(Currentculture);
                    Thread.CurrentThread.CurrentCulture = ci;
                    Thread.CurrentThread.CurrentUICulture = ci;
                }
            }

            if (RenderCtrlType == uxControlType.Calendar)
            {


                if (day == 0) day = DateTime.Now.Date.Date.Day;
                if (month == 0) month = DateTime.Now.Date.Month;
                if (year == 0) year = DateTime.Now.Date.Year;

                try
                {
                    tDate = new DateTime(year, month, day);
                }
                catch
                {
                    tDate = new DateTime(year, month, 1);
                }

                if (isnextcurrentmonth)
                {
                    tDate = tDate.AddMonths(1).Date;
                    month = tDate.Month;
                    year = tDate.Year;
                }

                //**Calendar set**//			
                try
                {
                    if (!string.IsNullOrEmpty(B2CSetting.CalendarFirstDayOfWeek))
                    {
                        FirstDayOfWeek = B2CSetting.CalendarFirstDayOfWeek.ToUpper();
                    }
                    System.Web.UI.WebControls.FirstDayOfWeek tmp = (System.Web.UI.WebControls.FirstDayOfWeek)Enum.Parse(typeof(System.Web.UI.WebControls.FirstDayOfWeek), FirstDayOfWeek, true);
                    Tempcal.FirstDayOfWeek = tmp;
                }
                catch
                {
                    Tempcal.FirstDayOfWeek = System.Web.UI.WebControls.FirstDayOfWeek.Monday;
                }

                Tempcal.TodaysDate = tDate;
                Tempcal.SelectedDate = tDate;

                Tempcal.ID = "ctr_" + id;

                //* Set Calendar Style*//

                Tempcal.SelectorStyle.CssClass = "calendar_SelectorStyle";
                Tempcal.TitleStyle.CssClass = "calendar_TitleStyle";


                Tempcal.TitleStyle.BackColor = System.Drawing.Color.White;

                Tempcal.SelectedDayStyle.CssClass = "calendar_dateselect";
                Tempcal.CssClass = "calendar_body";
                Tempcal.DayStyle.CssClass = "calendar_DayStyle";
                Tempcal.DayHeaderStyle.CssClass = "calendar_DayHeader";



                StringBuilder sb = new StringBuilder();
                StringWriter tw = new StringWriter(sb);
                HtmlTextWriter hw = new HtmlTextWriter(tw);

                string sdDate = year.ToString() + month.ToString().PadLeft(2, '0') + tDate.Date.Day.ToString().PadLeft(2, '0');

                int PN = Convert.ToInt32(tDate.AddMonths(1).Year.ToString() + tDate.AddMonths(1).Month.ToString().PadLeft(2, '0'));
                int PP = Convert.ToInt32(tDate.AddMonths(-1).Year.ToString() + tDate.AddMonths(-1).Month.ToString().PadLeft(2, '0'));

                // mxMY,mnMY

                if (mnMY == "") mnMY = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0');
                if (mxMY == "") mxMY = "999999";

                if ((PN <= Convert.ToInt32(mxMY)))
                {
                    Tempcal.NextMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(1).Month)) + "','" + tDate.AddMonths(1).Year.ToString() + "','" + Tempcal.ID + "','" + flightType + "');>" + Tempcal.NextMonthText + "</a>";
                }
                else
                {
                    Tempcal.NextMonthText = "";
                }

                if (PP >= Convert.ToInt32(mnMY))
                {
                    Tempcal.PrevMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(-1).Month)) + "','" + tDate.AddMonths(-1).Year.ToString() + "','" + Tempcal.ID + "','" + flightType + "');>" + Tempcal.PrevMonthText + "</a>";
                }
                else
                {
                    Tempcal.PrevMonthText = "";
                }

                Tempcal.RenderControl(hw);
                string strCal = sb.ToString() + "<input type='hidden' id='hdd_" + Tempcal.ID + "' name='hdd_" + Tempcal.ID + "' value='" + sdDate + "'  />";
                if (this.calendartype.ToString() == "datefrom")
                {
                    strCal += "<input type='hidden' id='hdd_defCell' name='hdd_defCell' value='ctr_" + this.calendartype.ToString() + "_" + sdDate + "'  />";
                }

                strCal.Replace("\r", "");
                strCal.Replace("\n", "");
                strCal.Replace("\t", "");

                return strCal;

            }
            else
            {

                //Generate DDL,Calendar

                switch (IsBeginCurrentMont)
                {
                    case "0":
                        break;
                    case "1":
                        break;
                }
                int iMaximumCalMonth = 12;

                DropDownList ddl = new DropDownList();
                DropDownList ddlDate = new DropDownList();
                DropDownList ddlMonthYear = new DropDownList();

                HtmlImage img = new HtmlImage();
                img.Src = "App_Themes/Default/Images/icon_calendar.gif";
                img.Style.Add("cursor", "hand");
                img.Border = 0;
                img.ID = id;

                if (IsRenderCalendarIcon)
                    img.Attributes.Add("onclick", "javascript:showCalendar(this,'" + flightType + "')");

                // show or not show calendar icon 
                if (!IsVisibleCalendarIcon)
                    img.Style.Add("display", "none");

                ddlDate.ID = "ddlDate_" + id;
                ddlMonthYear.ID = "ddlMY_" + id;
                ddlDate.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
                ddlMonthYear.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
                GenDate(ddlDate);

                if (B2CSetting.MaximumCalMonth > 1)
                {
                    iMaximumCalMonth = B2CSetting.MaximumCalMonth;
                }
                GenLimitYear(ddlMonthYear, iMaximumCalMonth, monthFormat);
                string Tmp = GetTempPlate();
                ddlDate.CssClass = "Day";
                ddlMonthYear.CssClass = "MonthYear";

                Tmp = Tmp.Replace("<!--DDL_Day-->", RenderControls(ddlDate, uxControlType.DropdownList));
                Tmp = Tmp.Replace("<!--DDL_Month_Year-->", RenderControls(ddlMonthYear, uxControlType.DropdownList));

                if (IsRenderCalendarIcon)
                    Tmp = Tmp.Replace("<!--IMG_Ctrl-->", RenderControls(img, uxControlType.HtmlImage));

                //"<select class='Day'></select> <select class='MonthYear'></select><img src='Images/icon_calendar.gif' />";
                return Tmp;

            }
        }

        public string GetHTML()
        {
            //RenderCtrlType.
            //culture
            if (tikAeroB2C.B2CSession.LanCode != null)
            {
                CultureInfo ci = new CultureInfo(tikAeroB2C.B2CSession.LanCode);
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;
            }
            else
            {
                if (Currentculture != "")
                {
                    CultureInfo ci = new CultureInfo(Currentculture);
                    Thread.CurrentThread.CurrentCulture = ci;
                    Thread.CurrentThread.CurrentUICulture = ci;
                }
            }

            if (RenderCtrlType == uxControlType.Calendar)
            {


                if (day == 0) day = DateTime.Now.Date.Date.Day;
                if (month == 0) month = DateTime.Now.Date.Month;
                if (year == 0) year = DateTime.Now.Date.Year;

                try
                {
                    tDate = new DateTime(year, month, day);
                }
                catch
                {
                    tDate = new DateTime(year, month, 1);
                }

                if (isnextcurrentmonth)
                {
                    tDate = tDate.AddMonths(1).Date;
                    month = tDate.Month;
                    year = tDate.Year;
                }

                //**Calendar set**//			
                try
                {
                    if (!string.IsNullOrEmpty(B2CSetting.CalendarFirstDayOfWeek))
                    {
                        FirstDayOfWeek = B2CSetting.CalendarFirstDayOfWeek.ToUpper();
                    }
                    FirstDayOfWeek tmp = (FirstDayOfWeek)Enum.Parse(typeof(FirstDayOfWeek), FirstDayOfWeek, true);
                    Tempcal.FirstDayOfWeek = tmp;
                }
                catch
                {
                    Tempcal.FirstDayOfWeek = System.Web.UI.WebControls.FirstDayOfWeek.Monday;
                }

                Tempcal.TodaysDate = tDate;
                Tempcal.SelectedDate = tDate;

                Tempcal.ID = "ctr_" + id;

                //* Set Calendar Style*//

                Tempcal.SelectorStyle.CssClass = "calendar_SelectorStyle";
                Tempcal.TitleStyle.CssClass = "calendar_TitleStyle";


                Tempcal.TitleStyle.BackColor = System.Drawing.Color.White;

                Tempcal.SelectedDayStyle.CssClass = "calendar_dateselect";
                Tempcal.CssClass = "calendar_body";
                Tempcal.DayStyle.CssClass = "calendar_DayStyle";
                Tempcal.DayHeaderStyle.CssClass = "calendar_DayHeader";



                StringBuilder sb = new StringBuilder();
                StringWriter tw = new StringWriter(sb);
                HtmlTextWriter hw = new HtmlTextWriter(tw);

                string sdDate = year.ToString() + month.ToString().PadLeft(2, '0') + tDate.Date.Day.ToString().PadLeft(2, '0');

                int PN = Convert.ToInt32(tDate.AddMonths(1).Year.ToString() + tDate.AddMonths(1).Month.ToString().PadLeft(2, '0'));
                int PP = Convert.ToInt32(tDate.AddMonths(-1).Year.ToString() + tDate.AddMonths(-1).Month.ToString().PadLeft(2, '0'));

                // mxMY,mnMY

                if (mnMY == "") mnMY = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0');
                if (mxMY == "") mxMY = "999999";

                if ((PN <= Convert.ToInt32(mxMY)))
                {
                    Tempcal.NextMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(1).Month)) + "','" + tDate.AddMonths(1).Year.ToString() + "','" + Tempcal.ID + "','" + flightType + "');>" + Tempcal.NextMonthText + "</a>";
                }
                else
                {
                    Tempcal.NextMonthText = "";
                }

                if (PP >= Convert.ToInt32(mnMY))
                {
                    Tempcal.PrevMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(-1).Month)) + "','" + tDate.AddMonths(-1).Year.ToString() + "','" + Tempcal.ID + "','" + flightType + "');>" + Tempcal.PrevMonthText + "</a>";
                }
                else
                {
                    Tempcal.PrevMonthText = "";
                }

                Tempcal.RenderControl(hw);
                string strCal = sb.ToString() + "<input type='hidden' id='hdd_" + Tempcal.ID + "' name='hdd_" + Tempcal.ID + "' value='" + sdDate + "'  />";
                if (this.calendartype.ToString() == "datefrom")
                {
                    strCal += "<input type='hidden' id='hdd_defCell' name='hdd_defCell' value='ctr_" + this.calendartype.ToString() + "_" + sdDate + "'  />";
                }

                strCal.Replace("\r", "");
                strCal.Replace("\n", "");
                strCal.Replace("\t", "");

                return strCal;

            }
            else
            {

                //Generate DDL,Calendar

                switch (IsBeginCurrentMont)
                {
                    case "0":
                        break;
                    case "1":
                        break;
                }
                int iMaximumCalMonth = 12;

                DropDownList ddl = new DropDownList();
                DropDownList ddlDate = new DropDownList();
                DropDownList ddlMonthYear = new DropDownList();

                HtmlImage img = new HtmlImage();
                img.Src = "App_Themes/Default/Images/icon_calendar.gif";
                img.Style.Add("cursor", "hand");
                img.Border = 0;
                img.ID = id;
                img.Attributes.Add("onclick", "javascript:showCalendar(this,'" + flightType + "')");
                ddlDate.ID = "ddlDate_" + id;
                ddlMonthYear.ID = "ddlMY_" + id;
                ddlDate.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
                ddlMonthYear.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
                GenDate(ddlDate);

                if (B2CSetting.MaximumCalMonth > 1)
                    iMaximumCalMonth = B2CSetting.MaximumCalMonth;
                GenLimitYear(ddlMonthYear, iMaximumCalMonth, monthFormat);
                string Tmp = GetTempPlate();
                ddlDate.CssClass = "Day";
                ddlMonthYear.CssClass = "MonthYear";

                Tmp = Tmp.Replace("<!--DDL_Day-->", RenderControls(ddlDate, uxControlType.DropdownList));
                Tmp = Tmp.Replace("<!--DDL_Month_Year-->", RenderControls(ddlMonthYear, uxControlType.DropdownList));
                Tmp = Tmp.Replace("<!--IMG_Ctrl-->", RenderControls(img, uxControlType.HtmlImage));
                //"<select class='Day'></select> <select class='MonthYear'></select><img src='Images/icon_calendar.gif' />";
                return Tmp;

            }
        }
        public string GetHTML(int cType)
        {
            this.calType = cType;
            if (tikAeroB2C.B2CSession.LanCode != null)
            {
                CultureInfo ci = new CultureInfo(tikAeroB2C.B2CSession.LanCode);
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;
            }
            else
            {
                if (Currentculture != "")
                {
                    CultureInfo ci = new CultureInfo(Currentculture);
                    Thread.CurrentThread.CurrentCulture = ci;
                    Thread.CurrentThread.CurrentUICulture = ci;
                }
            }

            if (day == 0) day = DateTime.Now.Date.Date.Day;
            if (month == 0) month = DateTime.Now.Date.Month;
            if (year == 0) year = DateTime.Now.Date.Year;

            try
            {
                tDate = new DateTime(year, month, day);
            }
            catch
            {
                tDate = new DateTime(year, month, 1);
            }

            if (isnextcurrentmonth)
            {
                tDate = tDate.AddMonths(1).Date;
                month = tDate.Month;
                year = tDate.Year;
            }

            //**Calendar set**//			
            try
            {
                if (!string.IsNullOrEmpty(B2CSetting.CalendarFirstDayOfWeek))
                {
                    FirstDayOfWeek = B2CSetting.CalendarFirstDayOfWeek.ToUpper();
                }
                System.Web.UI.WebControls.FirstDayOfWeek tmp = (System.Web.UI.WebControls.FirstDayOfWeek)Enum.Parse(typeof(System.Web.UI.WebControls.FirstDayOfWeek), FirstDayOfWeek, true);
                Tempcal.FirstDayOfWeek = tmp;
            }
            catch
            {
                Tempcal.FirstDayOfWeek = System.Web.UI.WebControls.FirstDayOfWeek.Monday;
            }

            Tempcal.TodaysDate = tDate;
            Tempcal.SelectedDate = tDate;

            Tempcal.ID = "ctr_" + id;

            //* Set Calendar Style*//

            Tempcal.SelectorStyle.CssClass = "calendar_SelectorStyle";
            Tempcal.TitleStyle.CssClass = "calendar_TitleStyle";


            Tempcal.TitleStyle.BackColor = System.Drawing.Color.White;

            Tempcal.SelectedDayStyle.CssClass = "calendar_dateselect";
            Tempcal.CssClass = "calendar_body";
            Tempcal.DayStyle.CssClass = "calendar_DayStyle";
            Tempcal.DayHeaderStyle.CssClass = "calendar_DayHeader";



            StringBuilder sb = new StringBuilder();
            StringWriter tw = new StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(tw);

            string sdDate = year.ToString() + month.ToString().PadLeft(2, '0') + tDate.Date.Day.ToString().PadLeft(2, '0');

            int PN = Convert.ToInt32(tDate.AddMonths(1).Year.ToString() + tDate.AddMonths(1).Month.ToString().PadLeft(2, '0'));
            int PP = Convert.ToInt32(tDate.AddMonths(-1).Year.ToString() + tDate.AddMonths(-1).Month.ToString().PadLeft(2, '0'));

            // mxMY,mnMY

            if (mnMY == "") mnMY = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0');
            if (mxMY == "") mxMY = "999999";

            if ((PN <= Convert.ToInt32(mxMY)))
            {
                Tempcal.NextMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(1).Month)) + "','" + tDate.AddMonths(1).Year.ToString() + "','" + Tempcal.ID + "','" + flightType + "');>" + Tempcal.NextMonthText + "</a>";
            }
            else
            {
                Tempcal.NextMonthText = "";
            }

            if (PP >= Convert.ToInt32(mnMY))
            {
                Tempcal.PrevMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(-1).Month)) + "','" + tDate.AddMonths(-1).Year.ToString() + "','" + Tempcal.ID + "','" + flightType + "');>" + Tempcal.PrevMonthText + "</a>";
            }
            else
            {
                Tempcal.PrevMonthText = "";
            }

            Tempcal.RenderControl(hw);
            string strCal = sb.ToString() + "<input type='hidden' id='hdd_" + Tempcal.ID + "' name='hdd_" + Tempcal.ID + "' value='" + sdDate + "'  />";
            if (this.calendartype.ToString() == "datefrom")
            {
                strCal += "<input type='hidden' id='hdd_defCell' name='hdd_defCell' value='ctr_" + this.calendartype.ToString() + "_" + sdDate + "'  />";
            }

            strCal.Replace("\r", "");
            strCal.Replace("\n", "");
            strCal.Replace("\t", "");
            strCal = strCal.Replace(">Senin<", ">Sen<");
            strCal = strCal.Replace(">Selasa<", ">Sel<");
            strCal = strCal.Replace(">Rabu<", ">Rab<");
            strCal = strCal.Replace(">Kamis<", ">Kam<");
            strCal = strCal.Replace(">Jumat<", ">Jum<");
            strCal = strCal.Replace(">Sabtu<", ">Sab<");
            strCal = strCal.Replace(">Minggu<", ">Min<");

            return strCal;

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //RenderCtrlType.

            RenderType = RenderCtrlType.ToString();
            if (RenderType != "0")
            {


                if (day == 0) day = DateTime.Now.Date.Date.Day;
                if (month == 0) month = DateTime.Now.Date.Month;
                if (year == 0) year = DateTime.Now.Date.Year;

                try
                {
                    tDate = new DateTime(year, month, day);
                }
                catch
                {
                    tDate = new DateTime(year, month, 1);
                }

                if (isnextcurrentmonth)
                {
                    tDate = tDate.AddMonths(1).Date;
                    month = tDate.Month;
                    year = tDate.Year;
                }

                //**Calendar set**//			
                try
                {
                    if (!string.IsNullOrEmpty(B2CSetting.CalendarFirstDayOfWeek))
                    {
                        FirstDayOfWeek = B2CSetting.CalendarFirstDayOfWeek.ToUpper();
                    }
                    FirstDayOfWeek tmp = (FirstDayOfWeek)Enum.Parse(typeof(FirstDayOfWeek), FirstDayOfWeek, true);
                    Tempcal.FirstDayOfWeek = tmp;
                }
                catch
                {
                    Tempcal.FirstDayOfWeek = System.Web.UI.WebControls.FirstDayOfWeek.Monday;
                }

                Tempcal.TodaysDate = tDate;
                Tempcal.SelectedDate = tDate;

                Tempcal.ID = "ctr_" + id;

                //* Set Calendar Style*//

                Tempcal.SelectorStyle.CssClass = "calendar_SelectorStyle";
                Tempcal.TitleStyle.CssClass = "calendar_TitleStyle";


                Tempcal.TitleStyle.BackColor = System.Drawing.Color.White;

                Tempcal.SelectedDayStyle.CssClass = "calendar_dateselect";
                Tempcal.CssClass = "calendar_body";
                Tempcal.DayStyle.CssClass = "calendar_DayStyle";
                Tempcal.DayHeaderStyle.CssClass = "calendar_DayHeader";



                StringBuilder sb = new StringBuilder();
                StringWriter tw = new StringWriter(sb);
                HtmlTextWriter hw = new HtmlTextWriter(tw);

                string sdDate = year.ToString() + month.ToString().PadLeft(2, '0') + tDate.Date.Day.ToString().PadLeft(2, '0');

                int PN = Convert.ToInt32(tDate.AddMonths(1).Year.ToString() + tDate.AddMonths(1).Month.ToString().PadLeft(2, '0'));
                int PP = Convert.ToInt32(tDate.AddMonths(-1).Year.ToString() + tDate.AddMonths(-1).Month.ToString().PadLeft(2, '0'));

                // mxMY,mnMY

                if (mnMY == "") mnMY = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0');
                if (mxMY == "") mxMY = "999999";

                if ((PN <= Convert.ToInt32(mxMY)))
                {
                    Tempcal.NextMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(1).Month)) + "','" + tDate.AddMonths(1).Year.ToString() + "','" + Tempcal.ID + "','" + flightType + "');>" + Tempcal.NextMonthText + "</a>";
                }
                else
                {
                    Tempcal.NextMonthText = "";
                }

                if (PP >= Convert.ToInt32(mnMY))
                {
                    Tempcal.PrevMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(-1).Month)) + "','" + tDate.AddMonths(-1).Year.ToString() + "','" + Tempcal.ID + "','" + flightType + "');>" + Tempcal.PrevMonthText + "</a>";
                }
                else
                {
                    Tempcal.PrevMonthText = "";
                }

                Tempcal.RenderControl(hw);
                string strCal = sb.ToString() + "<input type='hidden' id='hdd_" + Tempcal.ID + "' name='hdd_" + Tempcal.ID + "' value='" + sdDate + "'  />";
                if (this.calendartype.ToString() == "datefrom")
                {
                    strCal += "<input type='hidden' id='hdd_defCell' name='hdd_defCell' value='ctr_" + this.calendartype.ToString() + "_" + sdDate + "'  />";
                }

                strCal.Replace("\r", "");
                strCal.Replace("\n", "");
                strCal.Replace("\t", "");
                Response.Clear();
                Response.Write(strCal);

            }
            else
            {

                //Generate DDL,Calendar
                if (this.Page.Request["my"] != null) LimitYear = Convert.ToString("" + this.Page.Request["my"]);
                if (this.Page.Request["sm"] != null) StartMonth = Convert.ToString("" + this.Page.Request["sm"]);
                if (this.Page.Request["bc"] != null) IsBeginCurrentMont = Convert.ToString("" + this.Page.Request["bc"]);


                switch (IsBeginCurrentMont)
                {
                    case "0":
                        break;
                    case "1":
                        break;
                }

                int iMaximumCalMonth = 12;
                DropDownList ddl = new DropDownList();
                DropDownList ddlDate = new DropDownList();
                DropDownList ddlMonthYear = new DropDownList();

                HtmlImage img = new HtmlImage();
                img.Src = "App_Themes/Default/Images/icon_calendar.gif";
                img.Style.Add("cursor", "hand");
                img.Border = 0;
                img.ID = id;
                img.Attributes.Add("onclick", "javascript:showCalendar(this'" + flightType + "')");
                ddlDate.ID = "ddlDate_" + id;
                ddlMonthYear.ID = "ddlMY_" + id;
                ddlDate.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
                ddlMonthYear.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
                GenDate(ddlDate);

                if (B2CSetting.MaximumCalMonth > 1)
                    iMaximumCalMonth = B2CSetting.MaximumCalMonth;

                GenLimitYear(ddlMonthYear, iMaximumCalMonth, monthFormat);
                string Tmp = GetTempPlate();
                ddlDate.CssClass = "Day";
                ddlMonthYear.CssClass = "MonthYear";

                Tmp = Tmp.Replace("<!--DDL_Day-->", RenderControls(ddlDate, uxControlType.DropdownList));
                Tmp = Tmp.Replace("<!--DDL_Month_Year-->", RenderControls(ddlMonthYear, uxControlType.DropdownList));
                Tmp = Tmp.Replace("<!--IMG_Ctrl-->", RenderControls(img, uxControlType.HtmlImage));
                //"<select class='Day'></select> <select class='MonthYear'></select><img src='Images/icon_calendar.gif' />";
                Response.Clear();
                Response.Write(Tmp);

            }

        }

        public string RenderControls(object obj, uxControlType control)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter hm = new HtmlTextWriter(sw);

            switch (control)
            {
                case uxControlType.DropdownList:
                    DropDownList ddl = (DropDownList)obj;
                    ddl.RenderControl(hm);
                    break;
                case uxControlType.Calendar:

                    System.Web.UI.WebControls.Calendar cal = (System.Web.UI.WebControls.Calendar)obj;
                    cal.RenderControl(hm);
                    break;
                case uxControlType.HtmlImage:
                    HtmlImage img = (HtmlImage)obj;
                    img.RenderControl(hm);
                    break;
            }
            return sb.ToString();
        }
        public string GetTempPlate()
        {
            string ret = "";
            ret += "<!--DDL_Day-->&nbsp;<!--DDL_Month_Year-->&nbsp;<!--IMG_Ctrl-->";
            return ret;

        }
        public void GenLimitYear(DropDownList MyddlMonthList, int monthlimit, string monthFormat)
        {

            DateTime month = DateTime.Now;
            DateTime dtCurrentDate;
            int iMonthCount = 0;
            for (int m = 0; m < monthlimit; m++)
            {
                dtCurrentDate = month.AddMonths(iMonthCount);
                ListItem list = new ListItem();
                list.Text = dtCurrentDate.ToString(monthFormat) + " " + dtCurrentDate.Year.ToString();
                list.Value = dtCurrentDate.Year.ToString() + string.Format("{0:00}", dtCurrentDate.Month);
                MyddlMonthList.Items.Add(list);

                iMonthCount++;
            }
        }
        public void GenDate(DropDownList ddl)
        {

            for (int i = 1; i <= 31; i++)
            {
                try
                {
                    DateTime NewDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, i);
                    ddl.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0')));
                }
                catch
                {

                }

            }

            if (ddl.Items.FindByValue(DateTime.Today.Day.ToString().PadLeft(2, '0')) != null) ddl.Items.FindByValue(DateTime.Today.Day.ToString().PadLeft(2, '0')).Selected = true;

        }
        private void Tempcal_DayRender(object sender, System.Web.UI.WebControls.DayRenderEventArgs e)
        {
            {
                if (month == 0) { this.month = DateTime.Now.Month; }
                if (year == 0) { this.year = DateTime.Now.Year; }
                e.Cell.Text = e.Day.DayNumberText;

                int UsYear = e.Day.Date.Year;
                if (UsYear >= 2500) UsYear = UsYear - 543;
                string Fdate = UsYear.ToString() + e.Day.Date.Month.ToString().PadLeft(2, '0') + e.Day.Date.Day.ToString().PadLeft(2, '0');

                e.Cell.ID = "ctr_" + this.calendartype.ToString() + "_" + Fdate;
                if (this.isonlyonemonth)
                {
                    if (this.month != e.Day.Date.Month)
                    {
                        e.Cell.CssClass = "Calendar_notinmonth";
                        e.Cell.Text = "";
                    }
                    else
                    {
                        if (DateTime.Now.ToString("yyyyMMdd") == e.Day.Date.ToString("yyyyMMdd"))
                        {
                            e.Cell.CssClass = "calendar_dateselect";
                        }
                        string LinkDay = "<a href=javascript:showschedule2('" + id + "','" + Fdate + "','" + e.Cell.ClientID.ToString() + "')>" + e.Cell.Text + "</a>";
                        if (isShowLastDate == 0)
                        {
                            if (DateTime.Now.Date > e.Day.Date)
                            {
                                LinkDay = e.Cell.Text;
                                e.Cell.CssClass = "Calendar_notinmonth";
                            }
                        }

                        e.Cell.Text = LinkDay;
                    }
                }
                else
                {
                    if (isdefaultdate == 1)
                    {
                        e.Cell.CssClass = "calendar_dateselect";
                    }
                    e.Cell.Text = "<a href=javascript:showschedule2('" + id + "','" + Fdate + "','" + e.Cell.ClientID.ToString() + "')>" + e.Cell.Text + "</a>";
                }

                if (!this.calType.Equals(1))
                {
                    string selectedYearMonthDate = "";
                    #region - to change cell color when have flight -

                    int tmpDate = 0;
                    int tmpMonth = 0;

                    #region - init selected Date -
                    if (tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate1.Equals(string.Empty))
                    {
                        tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate1 = DateTime.Now.ToString("yyyyMMdd");
                    }
                    if (tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate2.Equals(string.Empty))
                    {
                        tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate2 = DateTime.Now.ToString("yyyyMMdd");
                    }

                    if (tikAeroB2C.Classes.UserCalandar.activeCalendar == "1")
                    {
                        selectedYearMonthDate = tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate1;
                    }
                    else
                    {
                        selectedYearMonthDate = tikAeroB2C.Classes.UserCalandar.selectedYearMonthDate2;
                    }
                    #endregion

                    DateTime currentDate = DateTime.Now;

                    foreach (DataRow dr in this.dsDateFlightInMonth.Tables[0].Rows)
                    {
                        tmpDate = Convert.ToDateTime(dr[0]).Day;
                        tmpMonth = Convert.ToDateTime(dr[0]).Month;

                        currentDate = e.Day.Date;
                        if (currentDate != prevDate)
                        {
                            prevDate = currentDate;
                            set = false;
                        }


                        if (set == true)
                        {
                            break;
                        }


                        if (e.Day.Date.Month == tmpMonth && e.Day.Date.Day == tmpDate)
                        {
                            if (Convert.ToInt16(dr[1]) != 0) // have flight
                            {
                                if (e.Day.Date.ToShortDateString() == DateTime.Now.ToShortDateString() &&
                                    e.Day.Date.ToString("yyyyMMdd") == selectedYearMonthDate)//current date with select
                                {
                                    e.Cell.BackColor = System.Drawing.Color.Empty;
                                    e.Cell.CssClass = "current_date_with_flight";
                                    set = true;
                                }
                                else if (e.Day.Date.ToShortDateString() == DateTime.Now.ToShortDateString() &&
                                    e.Day.Date.ToString("yyyyMMdd") != selectedYearMonthDate)//current date with select
                                {
                                    e.Cell.BackColor = System.Drawing.Color.Empty;
                                    e.Cell.CssClass = "current_date_with_flight_no_selected";
                                    set = true;
                                }
                                else if (e.Day.Date.ToShortDateString() != DateTime.Now.ToShortDateString() &&
                               e.Day.Date.ToString("yyyyMMdd") == selectedYearMonthDate)//next date with select
                                {
                                    e.Cell.BackColor = System.Drawing.Color.Empty;
                                    e.Cell.CssClass = "next_date_with_select";
                                    set = true;
                                }
                                else//next date with flight
                                {
                                    e.Cell.BackColor = System.Drawing.Color.Empty;
                                    e.Cell.CssClass = "operate_flight";
                                    set = true;
                                }
                            }
                            else if (e.Day.Date.Day > DateTime.Now.Day) // no flight
                            {
                                e.Cell.BackColor = System.Drawing.Color.Empty;
                                e.Cell.CssClass = "no_flight";
                                set = true;
                                //}
                            }
                        }
                        else if (e.Day.Date.Month == tmpMonth && e.Day.Date >= DateTime.Now)
                        {
                            if (e.Day.Date.ToShortDateString() == DateTime.Now.ToShortDateString())//current date with no select and no selected
                            {
                                e.Cell.BackColor = System.Drawing.Color.Empty;
                                e.Cell.CssClass = "current_date_with_no_flight";
                                set = true;
                            }
                            else if (e.Day.Date.Day <= tmpDate)
                            {
                                e.Cell.BackColor = System.Drawing.Color.Empty;
                                e.Cell.CssClass = "no_flight";
                                set = true;
                            }
                        }
                    }
                    #endregion

                }

            }
        }
    }
}