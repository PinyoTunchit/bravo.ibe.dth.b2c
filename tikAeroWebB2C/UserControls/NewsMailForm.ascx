<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsMailForm.ascx.cs" Inherits="tikAeroB2C.UserControls.NewsMailForm" %>

  <div id="dvInputMail">
    <table cellSpacing="2" width ="830" border="0" class="RegisterB2C">
		<TR>
			<TD class="groupForm" style="width: 100px; height: 30px"><%=tikAeroB2C.Classes.Language.Value("NewsMail_1", "From :", _stdLanguage)%></TD>
			<TD class="groupForm">&nbsp;</TD>
			<TD class="groupForm" style="width: 714px;">
			<input id="txtMailFrom" runat="server" style="width: 201px">
			<span id="spErrMailFrom" class="RequestStar">*</span></td>
		</TR>
		<tr>
			<td class="groupForm" style="width: 100px; height: 30px"><%=tikAeroB2C.Classes.Language.Value("NewsMail_2", "To :", _stdLanguage)%></td>
			<td class="groupForm">&nbsp;</td>
			<td class="groupForm" style="width: 714px;">
			<input id="txtMailTo" runat="server" style="width: 600px">
			<span id="spErrMailTo" class="RequestStar">*</span></td>
		</tr>
		<TR>
			<TD class="groupForm" style="width: 100px; height: 30px"><%=tikAeroB2C.Classes.Language.Value("NewsMail_3", "Bcc :", _stdLanguage)%></TD>
			<TD class="groupForm" style="height: 30px"></TD>
			<TD class="groupForm" style="height: 30px; width: 714px;">
			<span id="lblBcc" runat="server"></span></TD>
		</TR>
		<TR>
			<TD class="groupForm" style="width: 100px"><%=tikAeroB2C.Classes.Language.Value("NewsMail_4", "Additional Bcc :", _stdLanguage)%></TD>
			<TD class="groupForm"></TD>
			<TD class="groupForm" style="width: 714px">
            <textarea id="txtMailAddBcc" style="width: 600px; height: 47px"></textarea>
            <span id="spErrMailBCC" class="RequestStar">*</span></TD>
		</TR>
		<TR>
			<TD class="groupForm" style="width: 100px; height: 30px"><%=tikAeroB2C.Classes.Language.Value("NewsMail_5", "Subject :", _stdLanguage)%></TD>
			<TD class="groupForm" style="height: 30px"></TD>
			<TD class="groupForm" style="height: 30px; width: 714px;">
			<input id="txtMailSubject" runat="server" MaxLength="60" style="width: 600px">
			<span id="spErrMailSubject" class="RequestStar">*</span></TD>
		</TR>
		<TR>
			<TD class="groupForm" style="width: 100px; height: 30px"><%=tikAeroB2C.Classes.Language.Value("NewsMail_6", "Attachments :", _stdLanguage)%></TD>
			<TD class="groupForm" style="height: 30px"></TD>
			<TD class="groupForm" style="height: 30px; width: 714px;">
			<INPUT class="txtBoxList" id="txtMailAttachFile" style="WIDTH: 400px; HEIGHT: 19px" type="file" size="47"	name="txtImage" runat="server"></TD>
		</TR>
		<TR>
			<TD class="groupForm" style="width: 100px"><%=tikAeroB2C.Classes.Language.Value("NewsMail_7", "Message :", _stdLanguage)%></TD>
			<TD class="groupForm"></TD>
			<TD class="groupForm" style="width: 714px">
			<textarea id="txtMailMessage" runat="server" style="width: 600px; height: 111px"></textarea>
			<span id="spErrMailMessage" class="RequestStar"></span></TD>
		</TR>
		<tr>
			<td align="center" colSpan="3">
				<div class="RegisterCancelButton" onclick="NewsRegisterSendMail()">
                    <div class="buttonCornerLeft"></div>
                    <div class="buttonContent"><%=tikAeroB2C.Classes.Language.Value("NewsMail_8", "Send", _stdLanguage)%></div>
                    <div class="buttonCornerRight"></div>
                </div>
                <div class="RegisterSubmitButton" onclick="CloseMailForm()">
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent"><%=tikAeroB2C.Classes.Language.Value("NewsMail_11", "Back", _stdLanguage)%></div>
					<div class="buttonCornerRight"></div>
			    </div> 
			</td>
		</tr>
	</table>  
	</div>
	
    <div id="dvResultMail" style="display:none">
	  <table width ="830" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="RegisterB2C">
	        <tr><td style="height: 39px">&nbsp;</td></tr>
			<tr>
			<td class="groupForm" style="height: 23px" align="center"><div style="FONT-SIZE:20px;COLOR:#000066"><STRONG><%=tikAeroB2C.Classes.Language.Value("", "Send Mail Complete", _stdLanguage)%></STRONG></div>
			</td>
			</tr>
			<tr>
				<td  class="groupForm" style="height: 33px">&nbsp;</td>
			</tr>
			<TR>
			    <td align="center">
				<div class="RegisterCancelButton" onclick="CloseMailForm()">
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent"><%=tikAeroB2C.Classes.Language.Value("", "Back", _stdLanguage)%></div>
					<div class="buttonCornerRight"></div>
			    </div> 
			    </td>
			</TR>
	  </table>
	</div>	
	<span id="submitXML" class="RequestStar"></span>
