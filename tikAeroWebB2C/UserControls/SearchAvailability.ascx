<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchAvailability.ascx.cs" Inherits="tikAeroB2C.UserControls.SearchAvailability" %>
<iframe  style="height:0px; z-index:666;position:absolute; display: none;"  id='fdCal1'  src='' class='frmcls'></iframe>
<iframe  style="height:0px; z-index:555;position:absolute; display: none;"  id='fdCal2'  src='' class='frmcls'></iframe>
<div class="FlightSelectionBox">
    <div class="boxheader">Online Booking</div>
    <ul>
        <li class="type">
			<input id="optReturn" name="optSearch" type="radio"   checked="checked"  value="" onclick="ShowHideCalendar('optReturn', 'calRen2', 'lblReturnDate');"/> 
        <label for="optReturn">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_1", "Return", _stdLanguage)%>
			</label> 
        </li>
		
        <li class="type">
			<input id="optOneWay" name="optSearch"   type="radio" value=""  onclick="ShowHideCalendar('optReturn', 'calRen2', 'lblReturnDate');"/> 
            <label for="optOneWay">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_2", "One Way", _stdLanguage)%>
			</label>
        </li>
    </ul>
    <div class="clearboth"></div>
    <ul>
        <li class="FromTo">
			<label for="AvailabilitySearch1_optOrigin">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_3", "From", _stdLanguage)%>
			</label>
			<select id="optOrigin" runat="server" onchange="javascript:getDestination();" />
        </li>

        <li class="FromTo">
			<label for="AvailabilitySearch1_optDestination">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_4", "To", _stdLanguage)%>
			</label>
			<select id="optDestination" runat="server"/>
        </li>

		<li class="FromTo">
			<label for="AvailabilitySearch1_optCurrency">	
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_377", "Currency", _stdLanguage)%>
			</label>
			<select id="optCurrency" runat="server"  ></select> 
        </li>

		<li class="FromTo">
			<label for="optBoardingClass">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_", "Class", _stdLanguage)%>
			</label>
			<select id="optBoardingClass" class="class">
				<option value="Y">Economy Class</option>
			</select>
        </li>

		<div class="clear-all"></div>
        
        <li class="FromTo">
			<label for="ddlDate_1">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_5", "Deparature Date", _stdLanguage)%>
			</label>
        </li>

        <li id="CalenderBase"  class="TabDate">
           <%-- <%=tikAeroB2C.Classes.Calendar.getDLLCalendar("1", "outward", "MMM", _stdLanguage)%>--%>

            <%= tikAeroB2C.Classes.CalendarHelper.GetCalOutputControl("1", false, false)%>                  
            <input type="button" title=""  class="calendar_icon" id="calendar"  style="display:inline" onclick="TriggerCalendar1();" />  
            <div id="divCa1"></div>
        </li> 

        <li id='Cal1' class="CAL"></li>

		<div class="clear-all"></div>
        
        <li id="lblReturnDate" class="FromTo">
			<label for="ddlDate_2">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_6", "Return Date", _stdLanguage)%>
			</label>
        </li>

        <li id="calRen2" class="TabDate">
            <%--<%=tikAeroB2C.Classes.Calendar.getDLLCalendar("2", "return", "MMM", _stdLanguage)%>--%>

            <%= tikAeroB2C.Classes.CalendarHelper.GetCalOutputControl("2", false, false)%>                  
            <input type="button" title=""  class="calendar_icon" id="calendar2"  style="display:inline" onclick="TriggerCalendar2();" />  
            <div id="divCa2"></div>
        </li>
        <li id='Cal2'   style='POSITION: absolute;Z-INDEX: 1001;' class="CAL"></li>
        
        <li class="passenger">
			<label for="optAdult">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_11", "12+Years", _stdLanguage)%>
			</label>
			<select id="optAdult" class="pax">
				<option value="1" selected="selected">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
			</select>
			<div class="passengerage">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_", "12+ Years", _stdLanguage)%>
			</div>
        </li>
		<li class="passenger">
			<label for="optChild">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_12", "2-11 Years", _stdLanguage)%>
			</label>
			<select id="optChild" class="pax">
				<option value="0">0 </option>
				<option value="1">1 </option>
				<option value="2">2 </option>
				<option value="3">3 </option>
				<option value="4">4 </option>
				<option value="5">5 </option>
				<option value="6">6 </option>
				<option value="7">7 </option>
				<option value="8">8 </option>
				<option value="9">9 </option>
			</select>
			<div class="passengerage">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_", "2-11 Years", _stdLanguage)%>
			</div>
		</li>
        <li class="passenger">
			<label for="optInfant">
            	<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_13", "1-23 Months", _stdLanguage)%>
			</label>
            <select id="optInfant" class="pax">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
            </select>
			<div class="passengerage">
				<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_", "1-23 Months", _stdLanguage)%>
			</div>
        </li>

        <li class="PromoCode">
        	<label for="txtPromoCode">
            	Promotion Code
           </label>
            <input id="txtPromoCode" type="text" />
        </li>

        <li class="passenger">
			<label for="optInfant">
            	<%=tikAeroB2C.Classes.Language.Value("Registration_47", "Country", _stdLanguage)%>
			</label>
            <select id="optNationality" class="Selectbox" runat="server">
            <option value="" class="watermarkOn"></option>
            </select>
        </li>
    </ul>
    <div class="clear-all"></div>
    
	<div class="BTN-Search">
		<div class="lowfare">
			<a href="javascript:SearchAvailability('FARE,LOWFARE', '', '');" title="<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_38", "Low Fare", _stdLanguage)%>" class="defaultbutton">
				<span>
					<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_38", "Low Fare", _stdLanguage)%>
				</span>
			</a>
		</div>
		
		<div class="booknow">
			<a href="javascript:SearchAvailability('FARE', '', '');" title="<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_16", "Book Now", _stdLanguage)%>" class="defaultbutton">
				<span>
					<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_16", "Book Now", _stdLanguage)%>
				</span>
			</a>
		</div>
		<div class="clear-all"></div>
        <div id="dvlowFareFinderOption" runat="server">
        </div>
	</div>
</div>
