﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LowFareFinder.ascx.cs" Inherits="tikAeroB2C.UserControls.LowFareFinder" %>
<div class="LowFareCalendar">
  <!--Outward Flight-->
  <div id="dvOutbound" runat="server">
    <!--<div class="Per2-Outbound"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_25", "Outbound flights", _stdLanguage)%></div>
<div class="Per2-Detail"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_26", "Please select your outbound flight by clicking on the chosen date", _stdLanguage)%>.</div>-->
    <div id="dvOutwardResult" runat="server"/>
  </div>
  <div class="clearboth"></div>
  <!--Return Flight-->
  <div id="dvReturn" runat="server">
    <!--<div class="Per2-Return"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_27", "Return flights", _stdLanguage)%></div>
<div class="Per2-Detail"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_28", "Please select your return flight by clicking on the chosen date", _stdLanguage)%>.</div>-->
    <div id="dvReturnResult" runat="server"/>
  </div>
  <div class="clearboth"></div>
  <div id="dvAvaiNext" class="ButtonAlignRight" onclick="GetAvailabilityFromLowFareFinder();">
    <div class="BTN-Left"></div>
    <div class="BTN-Middle">
      <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_9", "Next", _stdLanguage)%>
    </div>
    <div class="BTN-Right"></div>
  </div>
</div>
