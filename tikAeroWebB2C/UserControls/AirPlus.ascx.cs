﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class AirPlus : System.Web.UI.UserControl
    {
        public string _parameter = string.Empty;
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                RenderAirPlus();
            }
        }

        private void RenderAirPlus()
        {
            //Load Langauge to String Dictionary
            _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
            Library ObjLi = new Library();
            Helper objXsl = new Helper();
            
            try
            {
                System.Xml.Xsl.XslTransform objTransform = objXsl.GetXSLDocument("AirPlus");
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);
                Response.Write(ObjLi.RenderHtml(objTransform, objArgument, _parameter));
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "RenderAirPlus");
                throw ex;
            }

        }
    }
}