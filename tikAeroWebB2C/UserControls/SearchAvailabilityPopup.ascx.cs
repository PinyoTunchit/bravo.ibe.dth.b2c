﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tikAeroB2C.UserControls
{
    public partial class SearchAvailabilityPopup : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
            }
        }
    }
}