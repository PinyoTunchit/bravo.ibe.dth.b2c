using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class B2ELogon : System.Web.UI.UserControl
    {
        public string _parameter;
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper objXsl = new Helper();
            if (IsPostBack == false)
            {
                try
                {
                    Library ObjLi = new Library();
                    System.Xml.Xsl.XslTransform objTransform = null;

                    //Load Langauge to String Dictionary
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                    System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    switch (_parameter)
                    {
                        case "StandardUser":
                            objTransform = objXsl.GetXSLDocument("B2ELogon");
                            dvLogon.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, "<NewDataSet></NewDataSet>");
                            break;
                        case "DialogUser":
                            objTransform = objXsl.GetXSLDocument("B2ELogonDialog");
                            dvLogon.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, "<NewDataSet></NewDataSet>");
                            break;
                        case "StandardAdmin":
                            objTransform = objXsl.GetXSLDocument("B2EAdminLogon");
                            dvLogon.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, "<NewDataSet></NewDataSet>");
                            break;
                        case "DialogAdmin":
                            objTransform = objXsl.GetXSLDocument("B2EAdminLogonDialog");
                            dvLogon.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, "<NewDataSet></NewDataSet>");
                            break;
                        default:
                            objTransform = objXsl.GetXSLDocument("B2ELogon");
                            dvLogon.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, "<NewDataSet></NewDataSet>");
                            break;
                    }
                }
                catch (Exception ex)
                {
                    objXsl.SendErrorEmail(ex, "Parameter<br/>" + _parameter);
                    throw ex;
                }
            }
        }
    }
}