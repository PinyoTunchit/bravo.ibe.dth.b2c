using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.XPath;
using System.Xml;
using System.IO;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;

namespace tikAeroB2C.UserControls
{
    public partial class Availability : System.Web.UI.UserControl
    {
        public string _parameter;
        public string ClientOnHold = "0";
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Helper objXsl = new Helper();
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                try
                {
                    Library li = new Library();
                    B2CVariable objUv = B2CSession.Variable;
                    Availabilities objFlights = new Availabilities(SecurityHelper.GenerateSessionlessToken());
                    string flightXML = string.Empty;
                    string strFareType;
                    string serverPath;
                    decimal dclOutSelectedFare = 0;
                    Guid gOutSelectedFlightId = Guid.Empty;
                    Guid gOutSelectedFareId = Guid.Empty;
                    decimal dclReturnSelectedFare = 0;
                    Guid gReturnSelectedFlightId = Guid.Empty;
                    Guid gReturnSelectedFareId = Guid.Empty;
                    bool skipOutBoundDayRange = B2CSetting.SkipOutBoundDayRange;

                    if (objUv == null)
                    {
                        objUv = new B2CVariable();
                        //Clear Session.
                        tikAeroB2C.WebService.B2cService objService = new tikAeroB2C.WebService.B2cService();
                        objService.ClearSession();
                    }

                    //Check agency cookies.
                    HttpCookie myCookie = new HttpCookie("logAgency");
                    myCookie = Request.Cookies["logAgency"];

                    if (myCookie != null && string.IsNullOrEmpty(myCookie.Value) == false)
                    {
                        objUv.Agency_Code = objXsl.DecodeDataBase64(Server.UrlDecode(myCookie.Value));
                    }
                    else if (string.IsNullOrEmpty(objUv.Agency_Code))
                    {
                        objUv.Agency_Code = B2CSetting.DefaultAgencyCode;
                    }

                    //Clear block inventory
                    BookingHeader bookingHeader = B2CSession.BookingHeader;
                    if (bookingHeader != null && bookingHeader.booking_id.Equals(Guid.Empty) == false)
                    {
                        Itinerary itinerary = B2CSession.Itinerary;
                        Passengers passengers = B2CSession.Passengers;
                        Quotes quotes = B2CSession.Quotes;
                        Fees fees = B2CSession.Fees;
                        Mappings mappings = B2CSession.Mappings;
                        Services services = B2CSession.Services;
                        Remarks remarks = B2CSession.Remarks;
                        Payments payments = B2CSession.Payments;
                        Taxes taxes = B2CSession.Taxes;

                        ServiceClient objSvc = new ServiceClient();
                        objSvc.ReleaseSessionlessFlightInventorySession(string.Empty, string.Empty, string.Empty, bookingHeader.booking_id.ToString(), false, true, true, SecurityHelper.GenerateSessionlessToken());

                        //Clear Gateway value
                        objUv.PaymentGateway = string.Empty;

                        //Clear booking in session
                        li.ClearBooking(ref bookingHeader,
                                        ref itinerary,
                                        ref passengers,
                                        ref quotes,
                                        ref fees,
                                        ref mappings,
                                        ref services,
                                        ref remarks,
                                        ref payments,
                                        ref taxes);

                        tikAeroB2C.WebService.B2cService objService = new tikAeroB2C.WebService.B2cService();
                        objService.ClearSession();
                    }

                    objUv.CurrentStep = 2;
                    objFlights.objService = B2CSession.AgentService;

                    strFareType = B2CSetting.SearchFareType;

                    if (objUv != null)
                    {
                        if (_parameter != null && _parameter.Length > 0)
                        {
                            string[] arrParam = _parameter.Split('|');

                            if (arrParam[8] != null)
                            {
                                dclOutSelectedFare = Convert.ToDecimal(0 + arrParam[8]);
                            }

                            if (arrParam[9] != null)
                            {
                                dclReturnSelectedFare = Convert.ToDecimal(0 + arrParam[9]);
                            }

                            if (arrParam[10] != ":" && arrParam[10].Length > 0)
                            {
                                string strOutFlight = arrParam[10].ToString();
                                string strFlightId = strOutFlight.Split(':')[0];
                                string strFareId = strOutFlight.Split(':')[1];
                                if (string.IsNullOrEmpty(strFlightId) == false)
                                {
                                    gOutSelectedFlightId = new Guid(strFlightId);
                                }
                                if (string.IsNullOrEmpty(strFareId) == false)
                                {
                                    gOutSelectedFareId = new Guid(strFareId);
                                }
                            }

                            if (arrParam[11] != ":" && arrParam[11].Length > 0)
                            {

                                string strRetFlight = arrParam[11].ToString();
                                string strFlightId = strRetFlight.Split(':')[0];
                                string strFareId = strRetFlight.Split(':')[1];

                                if (string.IsNullOrEmpty(strFlightId) == false)
                                {
                                    gReturnSelectedFlightId = new Guid(strFlightId);
                                }
                                if (string.IsNullOrEmpty(strFareId) == false)
                                {
                                    gReturnSelectedFareId = new Guid(strFareId);
                                }
                            }
                        }

                        if (strFareType == "LF")
                        {
                            serverPath = "Availability";
                            flightXML = objFlights.GetLowestFareGroupAvailability(objUv.OriginRcd,
                                                                                objUv.DestinationRcd,
                                                                                objUv.DepartureDate,
                                                                                objUv.ReturnDate,
                                                                                objUv.OriginName,
                                                                                objUv.DestinationName,
                                                                                objUv.OneWay,
                                                                                objUv.DayRange,
                                                                                objUv.Adult,
                                                                                objUv.Child,
                                                                                objUv.Infant,
                                                                                objUv.Other,
                                                                                objUv.OtherPassengerType,
                                                                                objUv.BoardingClass,
                                                                                objUv.Agency_Code,
                                                                                objUv.SearchCurrencyRcd,
                                                                                objUv.GroupBooking,
                                                                                objUv.PromoCode,
                                                                                true,
                                                                                ((objUv.OneWay == true) ? false : true),
                                                                                objUv.ip_address,
                                                                                tikAeroB2C.Classes.Language.CurrentCode().ToUpper(),
                                                                                objUv.SkipFareLogic,
                                                                                B2CSetting.NumberOfColumn,
                                                                                objUv.SearchType,
                                                                                dclOutSelectedFare,
                                                                                gOutSelectedFlightId,
                                                                                gOutSelectedFareId,
                                                                                dclReturnSelectedFare,
                                                                                gReturnSelectedFlightId,
                                                                                gReturnSelectedFareId,
                                                                                false,
                                                                                false,
                                                                                skipOutBoundDayRange,
                                                                                B2CSetting.TabDayRange);
                        }
                        else
                        {
                            serverPath = "AvailabilityAllFares";
                            flightXML = objFlights.GetAllFareAvailability(objUv.OriginRcd,
                                                                        objUv.DestinationRcd,
                                                                        objUv.DepartureDate,
                                                                        objUv.ReturnDate,
                                                                        objUv.OriginName,
                                                                        objUv.DestinationName,
                                                                        objUv.OneWay,
                                                                        objUv.DayRange,
                                                                        objUv.Adult,
                                                                        objUv.Child,
                                                                        objUv.Infant,
                                                                        objUv.Other,
                                                                        objUv.OtherPassengerType,
                                                                        objUv.BoardingClass,
                                                                        objUv.Agency_Code,
                                                                        objUv.SearchCurrencyRcd,
                                                                        objUv.GroupBooking,
                                                                        objUv.PromoCode,
                                                                        false,
                                                                        ((objUv.OneWay == true) ? false : true),
                                                                        objUv.ip_address,
                                                                        tikAeroB2C.Classes.Language.CurrentCode().ToUpper(),
                                                                        objUv.SkipFareLogic,
                                                                        B2CSetting.NumberOfColumn,
                                                                        objUv.SearchType,
                                                                        dclOutSelectedFare,
                                                                        gOutSelectedFlightId,
                                                                        gOutSelectedFareId,
                                                                        dclReturnSelectedFare,
                                                                        gReturnSelectedFlightId,
                                                                        gReturnSelectedFareId,
                                                                        false,
                                                                        false,
                                                                        false,
                                                                        skipOutBoundDayRange,
                                                                        B2CSetting.TabDayRange);
                        }

                        Session["FareAllID"] = null;

                        if (string.IsNullOrEmpty(flightXML) == false)
                        {
                            // Temp fare id from search flight
                            Session["FareAllID"] = Util.GetFareAllID(flightXML);

                            //Keep availability in session for used when select flight
                            //Selected which file to used. 
                            System.Xml.Xsl.XslTransform objTransform = null;
                            System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);
                            //Get Outboung Flight
                            objTransform = objXsl.GetXSLDocument(serverPath);

                            //Extract Xml 
                            using (StringReader srd = new StringReader(flightXML))
                            {
                                using (XmlReader reader = XmlReader.Create(srd))
                                {
                                    while (!reader.EOF)
                                    {
                                        if (reader.NodeType == XmlNodeType.Element)
                                        {
                                            if (reader.Name == "AvailabilityOutbound")
                                            {
                                                dvOutwardResult.InnerHtml = li.RenderHtml(objTransform, objArgument, reader.ReadInnerXml());
                                                if (objUv.OneWay == true)
                                                {
                                                    break;
                                                }
                                            }
                                            else if (reader.Name == "AvailabilityReturn")
                                            {
                                                if (objUv.OneWay == true)
                                                {
                                                    break;
                                                }
                                                dvReturnResult.InnerHtml = li.RenderHtml(objTransform, objArgument, reader.ReadInnerXml());
                                            }
                                            else
                                            {
                                                reader.Read();
                                            }

                                        }
                                        else
                                        {
                                            reader.Read();
                                        }
                                    }
                                }
                            }

                            //Check Client Setting
                            Client objClient = B2CSession.Client;
                            if (objClient != null && objClient.profile_on_hold_date_time != DateTime.MinValue)
                            {
                                ClientOnHold = "1";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (_parameter != null)
                    {
                        objXsl.SendErrorEmail(ex, "Avai Param<br/>" + _parameter.Replace("<", "&lt;").Replace(">", "&gt;"));
                    }
                    else
                    {
                        objXsl.SendErrorEmail(ex, string.Empty);
                    }
                    throw ex;
                }
            }
            _parameter = string.Empty;
        }
    }
}