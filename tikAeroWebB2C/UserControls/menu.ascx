<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="menu.ascx.cs" Inherits="tikAeroB2C.UserControls.menu" %>
<%@ Import Namespace="tikAeroB2C.Classes" %>
<ul id="nav">

	<% if (System.Configuration.ConfigurationManager.AppSettings["UseDialogLogon"].ToString() == "true")
		{
            if (tikAeroB2C.B2CSession.Client == null)
				{%>
					<li class="first">
						<a href="javascript:LoadDialog();">
							<%=tikAeroB2C.Classes.Language.Value("Menu_1", "FrequentFlyer Logon", _stdLanguage)%>
						</a>
					</li>
				<% }
			else
				{%>
					<li class="first">
						<a href="javascript:ClientLogOff();">
							<%=tikAeroB2C.Classes.Language.Value("Menu_2", "FrequentFlyer Logout", _stdLanguage)%>
						</a>
					</li>
				<% }
	}%>
	
	
	<li onclick="LoadCob(false, '');">
		<a href="#">
			<%=tikAeroB2C.Classes.Language.Value("Menu_3", "My Booking", _stdLanguage)%>
		</a>
	</li>
	
	<li>
		<a href="#">
			<%=tikAeroB2C.Classes.Language.Value("Menu_4", "Web Checkin", _stdLanguage)%>
		</a>
	</li>
  
  <!-- <li>
    <a href="javascript:LoadCMS('HTML/<%=Language.CurrentCode()%>/news.asp');">
        <%=tikAeroB2C.Classes.Language.Value("", "News &amp; Info", _stdLanguage)%>
    </a>
  </li> -->
  <!-- <li>
    <a href="javascript:LoadCMS('HTML/<%=Language.CurrentCode()%>/contactus.asp');">
        <%=tikAeroB2C.Classes.Language.Value("", "Contact Us ", _stdLanguage)%>
    </a>
  </li> -->
	<% if (System.Configuration.ConfigurationManager.AppSettings["UseB2ELogonDialog"].ToString().ToLower() == "true")
		{%>
			<li>
				<a href="javascript:LoadB2ELogonDialog();">
					<%=tikAeroB2C.Classes.Language.Value("Menu_5", "Corporate", _stdLanguage)%>
				</a>
			</li>
	<% }%>
  
	<% if (System.Configuration.ConfigurationManager.AppSettings["UseB2BLogonDialog"].ToString().ToLower() == "true")
		{%>
			<li>
				<a href="javascript:LoadB2BLogonDialog();">
					<%=tikAeroB2C.Classes.Language.Value("Menu_6", "Travel Agency", _stdLanguage)%>
				</a>
			</li>
	<% }%>
 
</ul>
