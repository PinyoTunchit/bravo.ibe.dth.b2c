<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientLogon.ascx.cs" Inherits="tikAeroB2C.UserControls.ClientLogon" %>
<!-- Start FFP Login -->
<div class="xouter">
	<div class="xcontainer">
		<div class="xinner LoadingBox">
		
			<div class="Header">
				<div class="mainlogo">
					<a href="http://www.flywithstyle.com/fws/b2c/default.aspx">
						<img src="App_Themes/Default/Images/mainlogo.png" alt="Style Airways" title="Style Airways">
					</a>
				</div>
            </div>
			
			<div class="FFPLogin">
				<div class="Topic">LOGIN</div>
				<div class="ContactDetail">
					<label for="txtClientID">Client ID / Email</label>
					<div class="InformationInput">
						<input id="txtClientID" type="text" onkeypress="return SubmitEnterUser(this,event,'false')" />
					</div>
					<div class="clear-all"></div>
				</div>
				
				<div class="ContactDetail">
					<label for="txtPassword">Password</label>
					<div class="InformationInput">
						<input id="txtPassword" type="password" onkeypress="return SubmitEnterUser(this,event,'false')" />
					</div>
					<div class="clear-all"></div>
				</div>

				<div class="L"><a href="javascript:LoadRegistration();">Register</a></div>
			
				<div class="clear-all"></div>
			
				<div class="forgetID" onclick="LoadForgetPassword();">Forget Password</div>
		
				<div class="BTN-FFP">
					<div class="buttonCornerLeft"></div>
					<div class="buttonContent" onclick="ClientLogon();">LOGIN</div>
					<div class="buttonCornerRight"></div>
				</div>
				<div class="clear-all"></div>
			</div>
		</div>
	</div>
</div>
<div class="clear-all"></div>
