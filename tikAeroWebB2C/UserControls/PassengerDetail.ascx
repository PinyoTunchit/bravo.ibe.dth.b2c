<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PassengerDetail.ascx.cs"
    Inherits="tikAeroB2C.UserControls.PassengerDetail" %>
<div class="StepContent">
    <ul>
        
        <li>
            <li class="StepNumber">1</li>
            <li class="StepName">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Step 2", _stdLanguage)%></li>
        </li>
        <li>
            <li class="StepNumberActive">2</li>
            <li class="StepName">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Step 3", _stdLanguage)%></li>
        </li>
        <li>
            <li class="StepNumber">3</li>
            <li class="StepName">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Step 4", _stdLanguage)%></li>
        </li>
        <li>
            <li class="StepNumber">4</li>
            <li class="StepName">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_1", "Step 5", _stdLanguage)%></li>
        </li>
    </ul>
    <div class="clear-all"></div>
</div>	
	
<div class="CoverDetailPopup">
    
    <div class="PassengerMainContact">
        <div class="MainContactDetails">
            <input id="txtClientProfileId" type="hidden" runat="server" />
            <input id="txtClientNumber" type="hidden" runat="server" />
           <%-- <input id="txtCompanyName" type="text" runat="server" style="display: none;" />
            <input id="txtVat" type="text" runat="server" style="display: none;" />--%>
            <div class="boxheader">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_3", "Main Contact Details (details in English only)", _stdLanguage)%>
            </div>
            <div class="clear-all"></div>
			
            <div class="ContactDetail FirstLine">
                <div class="ContactDetail">
                    <label for="ctl00_txtInvoiceReceiver">
                        <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_69", "Company Name", _stdLanguage)%>
                    </label>
                    <div class="InformationInput">
                        <input id="txtInvoiceReceiver" type="text" runat="server" maxlength="60" />
                    </div>
                    <div class="clear-all"></div>
                </div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtCIN">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_70", "CIN", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtCIN" type="text" runat="server" maxlength="60" />
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtEUVat">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_71", "EU VaT", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtEUVat" type="text" runat="server" maxlength="60" />
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtTIN">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_72", "TIN", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtTIN" type="text" runat="server" maxlength="60" />
                </div>
                <div class="clear-all"></div>
            </div>
             <div class="ContactDetail">
            <label for="ctl00_stContactTitle">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_4", "Title", _stdLanguage)%>
            </label>
            <div class="InformationInput">
                <select id="stContactTitle" class="Selectbox" runat="server">
                    <option value="" class="watermarkOn"></option>
                </select>
                <div class="mandatory"></div>
            </div>
            </div>
            <div class="clear-all"></div>
			
            <div class="ContactDetail">
                <label for="ctl00_txtContactFirstname">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_5", "First Name", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactFirstname" type="text" runat="server" maxlength="60" />
                    <div id="spErrFirstname" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactLastname">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_6", "Surname", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactLastname" type="text" runat="server" maxlength="60" />
                    <div id="spErrLastname" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactHome">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_8", "Home No", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactHome" type="text" runat="server" maxlength="60" />
                    <div id="spErrHome" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactMobile">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_7", "Mobile No", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactMobile" type="text" runat="server" maxlength="60" />
                    <div id="spErrMobile" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactBusiness">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_9", "Business No", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactBusiness" type="text" runat="server" maxlength="60" />
                    <div id="spnErrBusiness" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactEmail">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_10", "Email", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactEmail" type="text" runat="server" maxlength="60" />
                    <div id="spErrEmail" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
							<label for="ctl00_txtEmailConfirm">
				            	<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_93", "Email Confirmation", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtEmailConfirm" type="text" runat="server" maxlength="60" />
					          <div id="spErrEmailConfirm" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactEmail2">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_62", "Optional Email", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactEmail2" type="text" runat="server" maxlength="60" />
                    <div id="spOptionalEmail" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtMobileEmail">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_73", "Mobile Email", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtMobileEmail" type="text" runat="server" maxlength="60" />
                    <div id="spMobileEmail" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactAddress1">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_11", "Address 1", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactAddress1" type="text" runat="server" maxlength="60" />
                    <div id="spErrAddress1" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactAddress2">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_12", "Address 2", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactAddress2" type="text" runat="server" maxlength="60" />
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactState">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_14", "State", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactState" type="text" runat="server" maxlength="60" />
                    <div id="spErrState" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactCity">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_13", "Town/City", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactCity" type="text" runat="server" maxlength="60" />
                    <div id="spErrCity" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_txtContactZip">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_15", "Zip Code", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <input id="txtContactZip" type="text" runat="server" maxlength="60" />
                    <div id="spErrZip" class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_stContactCountry">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_16", "Country", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <select id="stContactCountry" class="Selectbox" runat="server">
                        <option value="" class="watermarkOn"></option>
                    </select>
                    <div class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
            <div class="ContactDetail">
                <label for="ctl00_stContactLanguage">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_58", "Language", _stdLanguage)%>
                </label>
                <div class="InformationInput">
                    <select id="stContactLanguage" class="Selectbox" name="select2" runat="server">
                        <option value="" class="watermarkOn"></option>
                    </select>
                    <div class="mandatory"></div>
                </div>
                <div class="clear-all"></div>
            </div>
        </div>
        <div class="optional">
            <div class="Remember FirstLine">
                <input id="chkRemember" name="Input" type="checkbox" value="" />
                <label for="chkRemember">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_17", "Remember these details for next visit.", _stdLanguage)%>
                </label>
                <a href="javascript:CreateWnd('HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/CookieDetail.html', 500, 371, false);">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_61", "more", _stdLanguage)%>
                </a>
            </div>
            <div class="clear-all"></div>
			
            <div class="Remember">
                <input id="chkCopy" type="checkbox" value="" onclick="CopyContactToPassenger();" />
                <label for="chkCopy">
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_18", "I'm a passenger on the flight.", _stdLanguage)%>
                </label>
            </div>
            <div class="Step4Remember">
    <input id="chkNewsLetter" name="Input" type="checkbox" value="" runat="server" />
	<label for="ctl00_chkNewsLetter" >
	<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_68", "Yes, add my mail address for newsletter", _stdLanguage)%>
	</label>
         
    </div>
            <div class="clear-all"></div>
        </div>
        <div class="clear-all"></div>
    </div>
    <!--<div id="dvFareSummary" runat="server">
    </div>-->
    <div class="clear-all"></div>
	
	<div id="PaxdetailInput" class="boxborder">
        <div class="boxheader">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_30", "Passenger", _stdLanguage)%>
        </div>
		
        <div class="clear-all"></div>
		
        <!--Passenger Input-->
        <div id="dvPassengerInput" runat="server"></div>
		
        <div id="dvPaxError" class="NBStep4"></div>
		
        <div class="clear-all"></div>
	</div>
	
    <div class="clear-all"></div>
	
    <div id="YourItinerary" class="boxborder">
        <div class="boxheader">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_45", "Your Itinerary", _stdLanguage)%>
        </div>
        <div class="clear-all"></div>
		
        <!--Flight information-->
        <div id="dvFlightInformation" runat="server">
        </div>
    </div>
    <div class="clear-all"></div>
	
    <div class="Remarks_Airline_Itinerary boxborder">
        <div class="boxheader">
            Remarks for Airline</div>
        <textarea id="txaAirlineRemark" runat="server"></textarea>
    </div>
    <div class="Remarks_Airline_Itinerary boxborder">
        <div class="boxheader">
            Remarks for Itinerary</div>
        <textarea id="txaItineraryRemark" runat="server"></textarea>
    </div>
    <div class="clear-all"></div>
	
    <div class="BTN-Search">
        <div class="ButtonAlignLeft">
			<a href="javascript:BackToAvailability();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_55", "Back", _stdLanguage)%>" class="defaultbutton">
                <span>
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_55", "Back", _stdLanguage)%>
                </span>
            </a>
        </div>
		
        <!-- <div class="ButtonAlignMiddleStep4">
			 <a href="javascript:ClearAllData('ctl00_');" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_56", "Cancel", _stdLanguage)%>" class="defaultbutton">
                <span>
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_56", "Cancel", _stdLanguage)%>
                </span>
            </a>   
        </div> -->
		
        <div class="ButtonAlignRight">
			<a href="javascript:SavePassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_57", "Next", _stdLanguage)%>" class="defaultbutton">
                <span>
                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_57", "Next", _stdLanguage)%>
                </span>
            </a>
        </div>
    </div>
</div>
