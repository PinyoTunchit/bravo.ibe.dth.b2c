using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.XPath;
using System.Text;
using System.IO;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;

namespace tikAeroB2C.UserControls
{
    public partial class ClientProfile : System.Web.UI.UserControl
    {
        public string _parameter;
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Helper objXsl = new Helper();
                try
                {
                    if (_parameter != null && _parameter.Length > 0)
                    {
                        Library ObjLi = new Library();

                        //Load Langauge to String Dictionary
                        _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                        System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                        System.Xml.Xsl.XslTransform objTransform = objXsl.GetXSLDocument("ClientList");

                        dvPassengerList.InnerHtml = ObjLi.RenderHtml(objTransform,
                                                                     objArgument,
                                                                     ReformatClientListXml(_parameter));
                    }
                }
                catch (Exception ex)
                {
                    objXsl.SendErrorEmail(ex, _parameter.Replace("<", "&lt;").Replace(">", "&gt;"));
                    throw ex;
                }
                
            }
        }

        #region Helper
        private string ReformatClientListXml(string strXml)
        {
            XPathDocument xmlDoc = new XPathDocument(new StringReader(strXml));
            XPathNavigator nv = xmlDoc.CreateNavigator();
            
            StringBuilder Stb = new StringBuilder();

            Stb.Append("<TikAero>");
            foreach (XPathNavigator n in nv.Select("TikAero/Passenger[passenger_role_rcd = 'MYSELF']"))
            {
                Stb.Append(n.OuterXml);
            }

            XPathExpression exp = nv.Compile("TikAero/Passenger[passenger_role_rcd != 'MYSELF']");
            exp.AddSort("passenger_type_rcd", XmlSortOrder.Ascending, XmlCaseOrder.None, string.Empty, XmlDataType.Text);
            exp.AddSort("lastname", XmlSortOrder.Ascending, XmlCaseOrder.None, string.Empty, XmlDataType.Text);
            exp.AddSort("firstname", XmlSortOrder.Ascending, XmlCaseOrder.None, string.Empty, XmlDataType.Text);
            foreach (XPathNavigator n in nv.Select(exp))
            {
                Stb.Append(n.OuterXml);
            }
            Stb.Append("</TikAero>");
            
            return Stb.ToString();
        }
        #endregion
    }
}