<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registration.ascx.cs" Inherits="tikAeroB2C.UserControls.Registration" %>
	<div class="Content">
		<div class="BoxGray">
			<div class="BoxGrayTop"></div>
			<div class="BoxGrayContent">
				<div class="BoxGrayContentHeader">
					<%=tikAeroB2C.Classes.Language.Value("Registration_25", "Registration", _stdLanguage)%>
				</div>
				<div class="clearboth"></div>
				<br /><br />
				<%=tikAeroB2C.Classes.Language.Value("Registration_26", "Thank you for your Interest in Aurigny�s Frequent Flyer.", _stdLanguage)%>
				</div>
			<div class="BoxGrayEnd"></div>
		</div>
		<div class="clearboth"></div>

		<div class="BoxGray">
			<div class="BoxGrayTop"></div>
			<div class="BoxGrayContent">
				<div class="BoxGrayContentHeader">
					<%=tikAeroB2C.Classes.Language.Value("Registration_2", "Passenger Profile", _stdLanguage)%>
				</div>
				<ul>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_27", "Title", _stdLanguage)%></div>
						<div class="Detail">
							<select id="ddlTitle" runat="server">
                                <option value="" class="watermarkOn"></option>
                            </select>
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_3", "Passenger Type", _stdLanguage)%>
						</div>
						<div class="Detail">
							<select id="ddlPassengerType" >
                                <option value="" class="watermarkOn"></option>
								<option value="ADULT">
									<%=tikAeroB2C.Classes.Language.Value("Registration_15", "Adult", _stdLanguage)%>
								</option>
								<option value="CHD">
									<%=tikAeroB2C.Classes.Language.Value("Registration_16", "Child", _stdLanguage)%>
								</option>
							</select>
						</div>
					</li>
					<div class="clearboth"></div>

					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_28", "Last Name", _stdLanguage)%><span id="spErrLastName" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtLastName" onkeypress="return CheckCharacter();" runat="server" />
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_29", "First Name", _stdLanguage)%><span id="spFirstname" class="RequestStar">*</span>
						</div>

						<div class="Detail">
							<input type="text" id="txtFirstName" onkeypress="return CheckCharacter();" runat="server" />
						</div>
					</li>
					<div class="clearboth"></div>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_30", "Date of Birth", _stdLanguage)%><span id="spDateOfBirth" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtDateofBirth" runat="server" />
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_6", "Nationality", _stdLanguage)%></div>
						<div class="Detail">
							<select id="optNationality" runat="server">
                                <option value="" class="watermarkOn"></option>
                            </select>
						</div>
					</li>
                    <div class="ContactDetail Passportdetail">
						<label class="toptitle"><%=tikAeroB2C.Classes.Language.Value("Registration_34", "Issue Country", _stdLanguage)%></label>
                        <div class="clear-all"></div>
                        <div class="InformationInput">
                        <select id="opIssueCountry" class="role" runat="server">
                            <option value="" class="watermarkOn"></option>
                        </select>
					<div class="clearboth"></div>
				</ul>
				<br />

				<div class="BoxGrayContentHeader">
					<%=tikAeroB2C.Classes.Language.Value("Registration_4", "Document Information", _stdLanguage)%>
				</div>
				<ul>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_32", "Document Type", _stdLanguage)%>
						</div>
						<div class="Detail">
							<select id="optDocumentType" runat="server">
                                <option value="" class="watermarkOn"></option>
                            </select>
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_33", "Document Number", _stdLanguage)%><span id="spDocNumber" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtDocumentNumber" runat="server" />
						</div>
					</li>
					<div class="clearboth"></div>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_34", "Place of Issue", _stdLanguage)%><span id="spPlaceOfIssue" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtPlaceOfIssue" runat="server" />
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_35", "Place of Birth", _stdLanguage)%><span id="spPlaceOfBirth" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtPlaceOfBirth" runat="server" />
						</div>
					</li>
					<div class="clearboth"></div>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_36", "Issue Date", _stdLanguage)%><span id="spIssueDate" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtIssueDate" runat="server" />
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_37", "Expiry Date", _stdLanguage)%><span id="spExpiryDate" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtExpiryDate" runat="server" />
						</div>
					</li>
					<div class="clearboth"></div>
				</ul>
				<br />
			</div>
			<div class="BoxGrayEnd"></div>
		</div>
		<div class="BoxGray">
			<div class="BoxGrayTop"></div>
			<div class="BoxGrayContent">
				<div class="BoxGrayContentHeader">
					<%=tikAeroB2C.Classes.Language.Value("Registration_5", "Contact Details", _stdLanguage)%>
				</div>
				<ul>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_38", "Mobile Phone", _stdLanguage)%><span id="spMobilePhone" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtMobilePhone" runat="server" />
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_39", "Home Phone", _stdLanguage)%>
						</div>
						<div class="Detail">
							<input type="text" ID="txtHomePhone" runat="server" />
						</div>
					</li>
					<div class="clearboth"></div>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_40", "Business Phone", _stdLanguage)%>
						</div>
						<div class="Detail">
							<input type="text" ID="txtBusinessPhone" runat="server" />
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_41", "Email", _stdLanguage)%><span id="spEmail" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtEmail" runat="server" />
						</div>
					</li>
                    <li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_54", "Mobile Email", _stdLanguage)%><span id="Span1" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtMobileEmail" runat="server" />
						</div>
					</li>
					<div class="clearboth"></div>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_42", "Language", _stdLanguage)%></div>
						<div class="Detail">
							<select id="optLanguage" runat="server">
                                <option value="" class="watermarkOn"></option>
                            </select>
						</div>
					</li>
					<div class="clearboth"></div>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_43", "Address 1", _stdLanguage)%><span id="spAddress1" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtAddress1" runat="server" />
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_44", "Address 2 (Optional)", _stdLanguage)%></div>
						<div class="Detail">
							<input type="text" ID="txtAddressline2" runat="server" />
						</div>
					</li>
					<div class="clearboth"></div>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_45", "Zip Code", _stdLanguage)%><span id="spZipCode" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtZipCode" runat="server" />
						</div>
					</li>
					<li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_46", "Township", _stdLanguage)%><span id="spTownship" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtcity" runat="server" />
						</div>
					</li>
					<div class="clearboth"></div>
                    <li class="Right">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_55", "State", _stdLanguage)%><span id="Span2" class="RequestStar">*</span>
						</div>
						<div class="Detail">
							<input type="text" ID="txtState" runat="server" maxlength="60"/>
						</div>
					</li>
					<div class="clearboth"></div>
					<li class="Left">
						<div class="Title">
							<%=tikAeroB2C.Classes.Language.Value("Registration_47", "Country", _stdLanguage)%></div>
						<div class="Detail">
							<select id="ddlCountry" runat ="server">
                                <option value="" class="watermarkOn"></option>
                            </select>
						</div>
					</li>
					<div class="clearboth"></div>
                    <li class="Left">
						    <div class="Title">
							    <%=tikAeroB2C.Classes.Language.Value("Registration_13", "Password", _stdLanguage)%><span id="spPassword" class="RequestStar">*</span>
						    </div>
						    <div class="Detail">
							    <input type="Password" ID="txtPassword" runat="server" TextMode="Password" />
						    </div>
					    </li>
					    <li class="Right">
						    <div class="Title">
							    <%=tikAeroB2C.Classes.Language.Value("Registration_48", "Confirm Password", _stdLanguage)%><span id="spConPassword" class="RequestStar">*</span>
						    </div>
						    <div class="Detail">
							    <input type="Password" ID="txtReconfirmPassword" runat="server" TextMode="Password" />
						    </div>
					    </li>
					    <div class="clearboth"></div>
				</ul>
				<br />

			</div>
			<div class="BoxGrayEnd"></div>

			<div class="AcceptTerms">
				<input type="checkbox" id="cboConfirm" value="cboConfirm" runat ="server" />
					<a href="#">
						<%=tikAeroB2C.Classes.Language.Value("Registration_6", "I have read, understood and accept all Terms & Conditions ", _stdLanguage)%>
					</a>
				</div>
			<div class="clearboth"></div>
			<div class="ButtonBig">
				<div class="ButtonBigLeft"></div>
				<div class="ButtonBigMid" onclick="CreateClientProfile('ctl00_');">
					<%=tikAeroB2C.Classes.Language.Value("Registration_49", "Register", _stdLanguage)%>
				</div>
				<div class="ButtonBigRight"></div>
			</div>
			<div class="ErrorList">
				<br/>
				<br/>
				<br/>
				<br/>
				<asp:Label ID="LabError" runat="server"></asp:Label>
			</div>

		</div>

	</div>