<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeatMap.ascx.cs" Inherits="tikAeroB2C.UserControls.SeatMap" %>
<div class="xouter">
	<div class="xcontainer">
		<div class="xinner SeatSelection">
			<div class="boxheader">
                <%=tikAeroB2C.Classes.Language.Value("", "Seat Selection", _stdLanguage)%>
            </div>
			
			<div class="clear-all"></div>
				
			<div class="SeatAssignment">
				<div class="header">
			
				</div>
			
				<div class="SelectSeat">
					<div class="HeadAirPlane"></div>
					<span id="dvSeatMap" runat="server"></span>
					<div class="TailAirPlane"></div>
				</div>
			</div>
			
			<div class="WrapperTBLSeat">
				 <div id="dvSeatItinerary" runat="server"></div>
			
				 <div id="dvSeatPassenger" runat="server"></div>
			
				 <div class="boxborder">
				 	<div class="boxheader">
                        <%=tikAeroB2C.Classes.Language.Value("", "Seat Map Legend", _stdLanguage)%>
                    </div>
					 <div class="TBLSeatInfo">
					 	<div class="seatmaplegend">
							<div>
								<div class="Icon">
									<img src="App_Themes/Default/Images/available_seat.png" alt="Available" title="Available">
									<img src="App_Themes/Default/Images/available_seat_invert.png" alt="Available" title="Available">
								</div>
								
								<div class="IconDesc">
									<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_12", "Available Seat", _stdLanguage)%>
								</div>
								<div class="clear-all"></div>
							</div>
		
							<div>
								<div class="Icon">
									<img src="App_Themes/Default/Images/reserved.png" alt="Reserved" title="Reserved">
									<img src="App_Themes/Default/Images/reserved_invert.png" alt="Reserved" title="Reserved">
								</div>
								
								<div class="IconDesc">
									<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_12_", "Reserved", _stdLanguage)%>
								</div>
								<div class="clear-all"></div>
							</div>
		
							<div>
								<div class="Icon">
									<img src="App_Themes/Default/Images/occupied_seat.png" alt="Occupied" title="Occupied">
									<img src="App_Themes/Default/Images/occupied_seat_invert.png" alt="Occupied" title="Occupied">
								</div>
								
								<div class="IconDesc">
									<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_12_", "Occupied", _stdLanguage)%>
								</div>
								<div class="clear-all"></div>
							</div>
							
						</div>
						
						<div class="seatmaplegend">
							<div>
								<div class="Icon">
									<img src="App_Themes/Default/Images/wing.gif" alt="Wing" title="Wing">
								</div>
								
								<div class="IconDesc">
									<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_14", "Wing", _stdLanguage)%>
								</div>
								<div class="clear-all"></div>
							</div>
		
							<div>
								<div class="Icon">
									<img src="App_Themes/Default/Images/lavatory.png" alt="Lavatory" title="Lavatory">
								</div>
								
								<div class="IconDesc">
									<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_13", "Lavatory", _stdLanguage)%>
								</div>
								<div class="clear-all"></div>
							</div>
		
							<div>
								<div class="Icon">
									<img src="App_Themes/Default/Images/galley.png" alt="Galley" title="Galley">
								</div>
								
								<div class="IconDesc">
									<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_12_", "Galley", _stdLanguage)%>
								</div>
								<div class="clear-all"></div>
							</div>
							
						</div>
						
						<div class="seatmaplegend">
						
							<div>
								<div class="Icon">
									<img src="App_Themes/Default/Images/exit_left.png" alt="Exit" title="Exit">
								</div>
								
								<div class="IconDesc">
									<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_15", "Exit", _stdLanguage)%>
								</div>
								<div class="clear-all"></div>
							</div>
							
						</div>
					
					   <div class="clear-all"></div>
		
					 </div>
					 
					 
				 </div>
				 
				<div>
					<div class="ButtonAlignLeft">
						<a href="javascript:CancelSelectSeat();" title="<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_17", "Back", _stdLanguage)%>">
							<div class="BTN-Left"></div>
							<div class="BTN-Middle">
								<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_17", "Back", _stdLanguage)%>
							</div>
							<div class="BTN-Right"></div>
						</a>
					</div>
			
					<div class="ButtonAlignRight">
						<a href="javascript:SaveSeatMap();" title="<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_18", "Confirm", _stdLanguage)%>">
							<div class="BTN-Left"></div>
							<div class="BTN-Middle">
								<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_18", "Confirm", _stdLanguage)%>
							</div>
							<div class="BTN-Right"></div>
						</a>
					</div>
				
					<div class="clear-all"></div>
				</div>
			</div>
			<div class="clear-all"></div>
		</div>
	</div>
</div>


