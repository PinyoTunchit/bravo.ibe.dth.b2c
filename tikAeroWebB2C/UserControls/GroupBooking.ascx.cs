using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Xml.XPath;
using System.Text;
using System.IO;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;


namespace tikAeroB2C.UserControls
{
    public partial class GroupBooking : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                FillContactInformation();
            }
        }

        private void FillContactInformation()
        {
            Helper objHelper = new Helper();
            try
            {
                XPathDocument xmlDoc;
                XPathNavigator nv;
                ListItem objList;

                Library objLi = new Library();

                B2CVariable objUv = B2CSession.Variable;
                
                if (B2CSession.AgentService == null)
                {
                    if (objUv == null)
                    {
                        objUv = new B2CVariable();
                    }
                    objHelper.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;
                }

                //Get Origin airport.
                Agents objAgent = B2CSession.Agents;
                Routes routes = CacheHelper.CacheOrigin();
                if (routes != null && routes.Count > 0)
                {
                    for (int i = 0; i < routes.Count; i++)
                    {
                        objList = new ListItem();
                        objList.Value = routes[i].origin_rcd + "|" + routes[i].currency_rcd;
                        objList.Text = routes[i].display_name;
                        if (objAgent[0].airport_rcd == routes[i].origin_rcd)
                        {
                            objList.Selected = true;
                        }
                        optOrigin.Items.Add(objList);
                        objList = null;
                    }
                }
                routes = null;
                
                //Load origin to htmlselect
                routes = CacheHelper.CacheDestination();
                string selectValue = optOrigin.Items[optOrigin.SelectedIndex].Value.Split('|')[0];
                for (int i = 0; i < routes.Count; i++)
                {
                    if (routes[i].origin_rcd.Equals(selectValue) == true && routes[i].b2c_flag == true)
                    {
                        objList = new ListItem();
                        objList.Value = routes[i].origin_rcd + "|" +
                                        optOrigin.Items[optOrigin.SelectedIndex].Text + "|" +
                                        routes[i].destination_rcd + "|" +
                                        routes[i].day_range;

                        objList.Text = routes[i].display_name;
                        optDestination.Items.Add(objList);
                        objList = null;
                    }
                }

                string titleRcd = string.Empty;
                Titles objTitles = CacheHelper.CachePassengerTitle();
                for (int i = 0; i < objTitles.Count; i++)
                {
                    objList = new ListItem();
                    titleRcd = objTitles[i].title_rcd;
                    objList.Value = titleRcd + "|" + objTitles[i].gender_type_rcd;
                    objList.Text = objTitles[i].display_name;

                    ddlTitle.Items.Add(objList);
                    objList = null;
                }
                nv = null;
                xmlDoc = null;

                ListItem objListChannelIslands = new ListItem();
                ListItem objListUnitedKingdom = new ListItem();

                Countries objCountries = CacheHelper.CacheCountry();
                
                for(int i = 0; i < objCountries.Count; i++)
                {
                    objList = new ListItem();
                    objList.Value = objCountries[i].country_rcd;
                    objList.Text = objCountries[i].display_name;

                    if (objList.Text == "Channel Islands")
                        objListChannelIslands = objList;
                    else if (objList.Text == "United Kingdom")
                        objListUnitedKingdom = objList;

                    ddlCountry.Items.Add(objList);
                    objList = null;
                }
                ddlCountry.Items.Insert(0, objListUnitedKingdom);
                ddlCountry.Items.Insert(0, objListChannelIslands);
            }
            catch (Exception ex)
            {
                objHelper.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
    }
}