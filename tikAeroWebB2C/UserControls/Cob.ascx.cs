using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace tikAeroB2C.UserControls
{
    public partial class Cob : System.Web.UI.UserControl
    {
        public string _parameter = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                try
                { 
                    Page.Response.Write("<iframe frameborder='0' scrolling='no' id='frmCOB' src='" + _parameter + "'  width='100%' height='2000px' runat='server'></iframe>"); 
                }
                catch (Exception ex)
                {
                    Classes.Helper objHp = new Classes.Helper();
                    objHp.SendErrorEmail(ex, _parameter);
                    throw ex;
                }
            }
        }
    }
}