using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class Insurance : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                //Load Langauge to String Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                Helper objXsl = new Helper();
                try
                {
                    tikAeroB2C.WebService.B2cService objService = new tikAeroB2C.WebService.B2cService();
                    Library ObjLi = new Library();

                    System.Xml.Xsl.XslTransform objTransform = objXsl.GetXSLDocument("Insurance");
                    System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    string xml = objService.RequestPriceInsurance();
                    dvInsurance.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, xml);

                    ObjLi = null;
                }
                catch (Exception ex)
                {
                    objXsl.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }
        }
    }
}