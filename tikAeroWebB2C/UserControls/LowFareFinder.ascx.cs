using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.XPath;
using System.IO;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using System.Text;
using System.Xml;

namespace tikAeroB2C.UserControls
{
    public partial class LowFareFinder : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Helper objXsl = new Helper();
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                string strXmlResult = string.Empty;
                try
                {
                    Library objLi = new Library();
                    B2CVariable objUv = B2CSession.Variable;
                    Routes routes;

                    Int16 iDayRange = 0;

                    if (objUv == null)
                    {
                        objUv = new B2CVariable();
                        //Clear Session.
                        tikAeroB2C.WebService.B2cService objService = new tikAeroB2C.WebService.B2cService();
                        objService.ClearSession();
                    }

                    //Check agency cookies.
                    HttpCookie myCookie = new HttpCookie("logAgency");
                    myCookie = Request.Cookies["logAgency"];

                    if (myCookie != null && string.IsNullOrEmpty(myCookie.Value) == false)
                    {
                        objUv.Agency_Code = objXsl.DecodeDataBase64(Server.UrlDecode(myCookie.Value));
                    }
                    else if (string.IsNullOrEmpty(objUv.Agency_Code))
                    {
                        objUv.Agency_Code = B2CSetting.DefaultAgencyCode;
                    }
                    B2CSession.Variable = objUv;


                    //Read Low Fare Finder.
                    Availabilities objFlights = new Availabilities(SecurityHelper.GenerateSessionlessToken());
                    objUv.SearchType = "FARE";
                    DayOfWeek dw = DayOfWeek.Sunday;
                    switch (B2CSetting.CalendarFirstDayOfWeek)
                    {
                        case "MONDAY": dw = DayOfWeek.Monday; break;
                        case "TUESDAY": dw = DayOfWeek.Tuesday; break;
                        case "WEDNESDAY": dw = DayOfWeek.Wednesday; break;
                        case "THURSDAY": dw = DayOfWeek.Thursday; break;
                        case "FRIDAY": dw = DayOfWeek.Friday; break;
                        case "SATURDAY": dw = DayOfWeek.Saturday; break;
                        default: dw = DayOfWeek.Sunday; break;
                    }

                    //Get Origin info.
                    routes = CacheHelper.CacheOrigin();
                    objUv.OriginName = objLi.FindOriginName(routes, objUv.OriginRcd);
                    routes = null;

                    //Get Destination Info.
                    routes = CacheHelper.CacheDestination();
                    objUv.DestinationName = objLi.FindDestinationName(routes, objUv.DestinationRcd);
                    Boolean bCalendarMode = B2CSetting.LowFareCalendarMode;

                    if (objUv.DayRange == -1)
                    {
                        iDayRange = -1;
                    }
                    else
                    {
                        if (B2CSetting.LowFareFinderRange == 0)
                        {
                            iDayRange = routes.GetDayRange(objUv.OriginRcd, objUv.DestinationRcd);
                        }
                        else
                        {
                            iDayRange = B2CSetting.LowFareFinderRange;
                        }
                        routes = null;
                    }


                    strXmlResult = objFlights.GetLowFareFinderAvailability(objUv.OriginRcd,
                                                                            objUv.DestinationRcd,
                                                                            objUv.DepartureDate,
                                                                            objUv.ReturnDate,
                                                                            objUv.OriginName,
                                                                            objUv.DestinationName,
                                                                            objUv.OneWay,
                                                                            iDayRange,
                                                                            objUv.Adult,
                                                                            objUv.Child,
                                                                            objUv.Infant,
                                                                            objUv.Other,
                                                                            objUv.OtherPassengerType,
                                                                            objUv.BoardingClass,
                                                                            objUv.Agency_Code,
                                                                            objUv.SearchCurrencyRcd,
                                                                            objUv.GroupBooking,
                                                                            objUv.PromoCode,
                                                                            false,
                                                                            ((objUv.OneWay == true) ? false : true),
                                                                            objUv.ip_address,
                                                                            tikAeroB2C.Classes.Language.CurrentCode().ToUpper(),
                                                                            objUv.SkipFareLogic,
                                                                            B2CSetting.NumberOfColumn,
                                                                            objUv.SearchType,
                                                                            dw,
                                                                            bCalendarMode,
                                                                            B2CSession.CurrentClientDate,
                                                                            false,
                                                                            false,
                                                                            false);


                    if (string.IsNullOrEmpty(strXmlResult) == false)
                    {
                        //Read xml to Xpath.
                        System.Xml.Xsl.XslTransform objTransform = null;
                        System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                        string serverPath = Server.MapPath("~") + @"\xsl\";
                        using (StringReader srd = new StringReader(strXmlResult))
                        {
                            using (XmlReader reader = XmlReader.Create(srd))
                            {
                                objTransform = objXsl.GetXSLDocument("LowFareFinder");
                                while (!reader.EOF)
                                {
                                    if (reader.NodeType == XmlNodeType.Element)
                                    {
                                        if (reader.Name == "flight_departure")
                                        {

                                            dvOutwardResult.InnerHtml = objLi.RenderHtml(objTransform,
                                                                                         objArgument,
                                                                                         "<flights>" + reader.ReadInnerXml() + "</flights>");
                                            if (objUv.OneWay == true)
                                            {
                                                break;
                                            }
                                        }
                                        else if (reader.Name == "flight_return")
                                        {
                                            if (objUv.OneWay == true)
                                            {
                                                break;
                                            }

                                            dvReturnResult.InnerHtml = objLi.RenderHtml(objTransform,
                                                                                        objArgument,
                                                                                        "<flights>" + reader.ReadInnerXml() + "</flights>");
                                        }
                                        else
                                        {
                                            reader.Read();
                                        }
                                    }
                                    else
                                    {
                                        reader.Read();
                                    }
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    objXsl.SendErrorEmail(ex, "LFF Xml<br/>" + strXmlResult.Replace("<", "&lt;").Replace(">", "&gt;"));
                    throw ex;
                }
            }
        }
    }
}