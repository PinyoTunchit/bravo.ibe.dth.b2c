using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Xml;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;

namespace tikAeroB2C.UserControls
{
    public partial class SeatMap : System.Web.UI.UserControl
    {
        #region Decalration
        public string _parameter;
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        #endregion
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                string xmlResult = LoadSeatInformation();

                Library objLi = new Library();
                Helper objXsl = new Helper();
                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);
                //Flight Segment Information
                objTransform = objXsl.GetXSLDocument("SeatItinerary");
                dvSeatItinerary.InnerHtml = objLi.RenderHtml(objTransform, objArgument, xmlResult);

                //Passenger Information
                objTransform = objXsl.GetXSLDocument("SeatPassenger");
                dvSeatPassenger.InnerHtml = objLi.RenderHtml(objTransform, objArgument, xmlResult);
                
            }
        }
        protected string LoadSeatInformation()
        {
            Helper objXsl = new Helper();
            try
            {
                
                Library objLi = new Library();

                //System.Xml.Xsl.XslTransform objTransform = null;
               
                string[] ParamSplit;
                if (string.IsNullOrEmpty(_parameter) == false)
                {
                    ParamSplit = _parameter.Split('|');
                    B2CSession.SeatMapParameter = _parameter;
                }
                else
                    ParamSplit = B2CSession.SeatMapParameter.Split('|');

                string flightID = ParamSplit[0];
                string originRcd = ParamSplit[1];
                string destinationRcd = ParamSplit[2];
                string boardingClass = ParamSplit[3];
                string bookingingClass = ParamSplit[4];

                if (string.IsNullOrEmpty(ParamSplit[0]))
                {
                    flightID = B2CSession.Itinerary[0].flight_id.ToString();
                    originRcd = B2CSession.Itinerary[0].origin_rcd;
                    destinationRcd = B2CSession.Itinerary[0].destination_rcd;
                    boardingClass = B2CSession.Itinerary[0].boarding_class_rcd;
                    bookingingClass = B2CSession.Itinerary[0].booking_class_rcd;
                }

                //System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);
                ////Flight Segment Information
                //objTransform = objXsl.GetXSLDocument("SeatItinerary");
                //dvSeatItinerary.InnerHtml = objLi.RenderHtml(objTransform, objArgument, xmlResult);

                ////Passenger Information
                //objTransform = objXsl.GetXSLDocument("SeatPassenger");
                //dvSeatPassenger.InnerHtml = objLi.RenderHtml(objTransform, objArgument, xmlResult);

                //Get Seat map from user control
                dvSeatMap.InnerHtml = objLi.GenerateControlString("UserControls/Map.ascx", flightID + "|" + originRcd + "|" + destinationRcd + "|" + boardingClass + "|" + bookingingClass);
                return GenerateInputXML(objLi, flightID);
                //return true;
            }
            catch (Exception ex)
            {
                objXsl.SendErrorEmail(ex, "Seat Map Parameter<br/>" + _parameter);
                throw ex;
            }
        }

        public virtual string GenerateInputXML(Library objLi, string flightId)
        {
            Itinerary itinerary = B2CSession.Itinerary;
            Mappings mappings = B2CSession.Mappings;
            StringBuilder stb = new StringBuilder();
            
            using (StringWriter stw = new StringWriter(stb))
            {
                using (XmlWriter xtw = XmlWriter.Create(stw))
                {
                    objLi.RetrivedSegmentMappingXml(xtw, itinerary, mappings, flightId, string.Empty);
                }
            }

            return stb.ToString();
        }

    }
}