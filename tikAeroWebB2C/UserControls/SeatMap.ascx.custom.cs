﻿using System;
using System.Collections.Generic;
using System.Web;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Xml;
using System.IO;

namespace tikAeroB2C.UserControls
{
    public class SeatMapCustom : SeatMap
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);
            Helper objXsl = new Helper();
            // Fill temp baggage
            objXsl.FillBaggageFeeToTemp();

            // Add Step
            B2CVariable objUv = B2CSession.Variable;
            objUv.CurrentStep = 11;
        }

        public override string GenerateInputXML(Library objLi, string flightId)
        {
            Itinerary itinerary = B2CSession.Itinerary;
            Mappings mappings = B2CSession.Mappings;
            Passengers passengers = B2CSession.Passengers;
            StringBuilder stb = new StringBuilder();
            Helper objXsl = new Helper();

            using (StringWriter stw = new StringWriter(stb))
            {
                using (XmlWriter xtw = XmlWriter.Create(stw))
                {
                    xtw.WriteStartElement("Booking");
                    {
                        xtw.WriteStartElement("Setting");
                        {
                            xtw.WriteStartElement("selected_flight_id");
                            {
                                xtw.WriteValue(flightId);
                            }
                            xtw.WriteEndElement();
                        }
                        xtw.WriteEndElement();
                        objLi.BuiltBookingXml(xtw, null, itinerary, null, null, null, mappings, null, null, null, null);
                        objXsl.BuiltBaggage(xtw, flightId, passengers);
                    }
                    xtw.WriteEndElement();
                }
            }

            return stb.ToString();
        }
    }
}