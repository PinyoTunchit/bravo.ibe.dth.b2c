using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class NewsAdmin : System.Web.UI.UserControl
    {
        protected string chkCountry = "";
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Classes.Helper objHp = new Classes.Helper();
                try
                {
                    //Load Language Dictionary
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                    Library objLi = new Library();
                    string serverPath = Server.MapPath("~") + @"\xsl\";

                    //Show NewsAdminLogin input information
                    serverPath = Server.MapPath("~") + B2CSetting.CountryPath;
                    DataSet ds = new DataSet();
                    ds.ReadXml(Server.MapPath("~") + B2CSetting.CountryPath);

                    if (ds.Tables != null)
                    {
                        chkCountry = "";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            chkCountry += "<input id=''chkCountry_" + i.ToString() + "'' " +
                                          "type=''checkbox'' name=''chkCountry'' value=''" + ds.Tables[0].Rows[i]["name"].ToString() + "''/>" +
                                          "<label for=''chkCountry_" + i.ToString() + "''>" + ds.Tables[0].Rows[i]["name"].ToString() +
                                          "</label>&nbsp;&nbsp;";
                        chkCountry += "<input id=''chkCountry_All'' type=''checkbox'' name=''chkCountry_All'' " +
                                      "onclick=''validCountryAll(this.checked);''/><label for=''chkCountry_All''>ALL</label> ";
                        chkCountry = chkCountry.Replace("''", "\"");

                    }
                }
                catch (Exception ex)
                {
                    objHp.SendErrorEmail(ex, chkCountry);
                    throw ex;
                }
            }
        }
    }
}