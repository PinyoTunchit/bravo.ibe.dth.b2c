<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Availability.ascx.cs" Inherits="tikAeroB2C.UserControls.Availability" %>
<div class="StepContent">
		<ul>
			<li>
				<li class="StepNumberActive">1</li>
				<li class="StepName"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Step 2", _stdLanguage)%></li>
			</li>
			
			<li>
				<li class="StepNumber">2</li>
				<li class="StepName"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Step 3", _stdLanguage)%></li>
			</li>
			
			<li>
				<li class="StepNumber">3</li>
				<li class="StepName"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Step 4", _stdLanguage)%></li>
			</li>
			
			<li>
				<li class="StepNumber">4</li>
				<li class="StepName"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_1", "Step 5", _stdLanguage)%></li>
			</li>
		</ul>
		<div class="clear-all"></div>
    </div>
	
	
<div class="CoverDetailPopup">
    <input id="dvClientOnHold" type="hidden" value="<%=ClientOnHold %>" />
    
	
    <div class="clear-all"></div>
	<div class="Availability">
	    <!--Outward Flight-->
	    <div id="dvOutwardResult" runat="server"/>
	    <div class="clear-all"></div>
	    <!--Return Flight-->
	    <div id="dvReturnResult" runat="server"/>
		
		<div class="no-col-three-button">
		<div class="BTN-Search">
		    <div class="ButtonAlignLeft">
		    		<a href="javascript:loadHome();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_8", "Back", _stdLanguage)%>" class="defaultbutton">
			        <span>
			            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_8", "Back", _stdLanguage)%>
			        </span>
		        </a>
		    </div>
			
		    <div id="dvAvaiNext" class="ButtonAlignRight">
		    		<a href="javascript:SelectFlight();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_9", "Next", _stdLanguage)%>" class="defaultbutton">
			        <span>
			            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_9", "Next", _stdLanguage)%>
			        </span>
			      </a>
		    </div>
		</div>
	</div>
    </div>
    
    <div class="clear-all"></div>
	
</div>