<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupBooking.ascx.cs" Inherits="tikAeroB2C.UserControls.GroupBooking" %>
<iframe  style="height:0px; z-index:666;position:absolute; display: none;"  id='fdCal3'  src='' class='frmcls'></iframe>
<iframe  style="height:0px; z-index:555;position:absolute; display: none;"  id='fdCal4'  src='' class='frmcls'></iframe>
  <div id="dvGroupBooking">
  <table width ="830" border="0" cellspacing="0" cellpadding="0" class="base">
  <tr>
    <td colspan="4" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_36", "Group Information", _stdLanguage)%></td>
  </tr>
  <tr>
    <td style="width: 60px;">&nbsp;</td>
	<td style="width: 170px;">&nbsp;</td>
	<td style="width: 300px;">&nbsp;</td>
	<td style="width: 300px;">&nbsp;</td>
  </tr>
  <tr>  	
    <td colspan="4" class="groupForm">
		<%=tikAeroB2C.Classes.Language.Value("GroupBooking_1", "Please provide information in the following form and we will get back to you shortly. Thank you for your enquiry.", _stdLanguage)%>
	</td>
  </tr>
  <tr>
    <td colspan="4" height="10"></td>  
  </tr>
  <tr>    
    <td class="groupForm" colspan="2"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_16", "How many people are travelling ?", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupFormClear" colspan="2">
        <select id="cmbAdult" tabindex="1" onchange="CheckAdultInfant();">
	<option selected="selected" value="1">1 Adult</option>
	<option value="2">2 Adults</option>
	<option value="3">3 Adults</option>
	<option value="4">4 Adults</option>
	<option value="5">5 Adults</option>
	<option value="6">6 Adults</option>
	<option value="7">7 Adults</option>
	<option value="8">8 Adults</option>
	<option value="9">9 Adults</option>
	<option value="10">10 Adults</option>
	<option value="11">11 Adults</option>
	<option value="12">12 Adults</option>
	<option value="13">13 Adults</option>
	<option value="14">14 Adults</option>
	<option value="15">15 Adults</option>
	<option value="16">16 Adults</option>
	<option value="17">17 Adults</option>
	<option value="18">18 Adults</option>
	<option value="19">19 Adults</option>
	<option value="20">20 Adults</option>
	<option value="21">21 Adults</option>
	<option value="22">22 Adults</option>
	<option value="23">23 Adults</option>
	<option value="24">24 Adults</option>
	<option value="25">25 Adults</option>
	<option value="26">26 Adults</option>
	<option value="27">27 Adults</option>
	<option value="28">28 Adults</option>
	<option value="29">29 Adults</option>
	<option value="30">30 Adults</option>
	<option value="31">31 Adults</option>
	<option value="32">32 Adults</option>
	<option value="33">33 Adults</option>
	<option value="34">34 Adults</option>
	<option value="35">35 Adults</option>
	<option value="36">36 Adults</option>
	<option value="37">37 Adults</option>
	<option value="38">38 Adults</option>
	<option value="39">39 Adults</option>
	<option value="40">40 Adults</option>
	<option value="41">41 Adults</option>
	<option value="42">42 Adults</option>
	<option value="43">43 Adults</option>
	<option value="44">44 Adults</option>
	<option value="45">45 Adults</option>
	<option value="46">46 Adults</option>
	<option value="47">47 Adults</option>
	<option value="48">48 Adults</option>
	<option value="49">49 Adults</option>
	<option value="50">50 Adults</option>
	<option value="51">51 Adults</option>
	<option value="52">52 Adults</option>
	<option value="53">53 Adults</option>
	<option value="54">54 Adults</option>
	<option value="55">55 Adults</option>
	<option value="56">56 Adults</option>
	<option value="57">57 Adults</option>
	<option value="58">58 Adults</option>
	<option value="59">59 Adults</option>
	<option value="60">60 Adults</option>
	<option value="61">61 Adults</option>
	<option value="62">62 Adults</option>
	<option value="63">63 Adults</option>
	<option value="64">64 Adults</option>
	<option value="65">65 Adults</option>
	<option value="66">66 Adults</option>
	<option value="67">67 Adults</option>
	<option value="68">68 Adults</option>
	<option value="69">69 Adults</option>
	<option value="70">70 Adults</option>
	<option value="71">71 Adults</option>
	<option value="72">72 Adults</option>
	<option value="73">73 Adults</option>
	<option value="74">74 Adults</option>
	<option value="75">75 Adults</option>
	<option value="76">76 Adults</option>
	<option value="77">77 Adults</option>
	<option value="78">78 Adults</option>
	<option value="79">79 Adults</option>
	<option value="80">80 Adults</option>
	<option value="81">81 Adults</option>
	<option value="82">82 Adults</option>
	<option value="83">83 Adults</option>
	<option value="84">84 Adults</option>
	<option value="85">85 Adults</option>
	<option value="86">86 Adults</option>
	<option value="87">87 Adults</option>
	<option value="88">88 Adults</option>
	<option value="89">89 Adults</option>
	<option value="90">90 Adults</option>
	<option value="91">91 Adults</option>
	<option value="92">92 Adults</option>
	<option value="93">93 Adults</option>
	<option value="94">94 Adults</option>
	<option value="95">95 Adults</option>
	<option value="96">96 Adults</option>
	<option value="97">97 Adults</option>
	<option value="98">98 Adults</option>
	<option value="99">99 Adults</option>
	<option value="100">100 Adults</option>
 
</select>&nbsp;<select id="cmbChild" tabindex="2">
	<option selected="selected" value="0">Children</option>
	<option value="1">1 Child</option>
	<option value="2">2 Children</option>
	<option value="3">3 Children</option>
	<option value="4">4 Children</option>
	<option value="5">5 Children</option>
	<option value="6">6 Children</option>
	<option value="7">7 Children</option>
	<option value="8">8 Children</option>
	<option value="9">9 Children</option>
	<option value="10">10 Children</option>
	<option value="11">11 Children</option>
	<option value="12">12 Children</option>
	<option value="13">13 Children</option>
	<option value="14">14 Children</option>
	<option value="15">15 Children</option>
	<option value="16">16 Children</option>
	<option value="17">17 Children</option>
	<option value="18">18 Children</option>
	<option value="19">19 Children</option>
	<option value="20">20 Children</option>
	<option value="21">21 Children</option>
	<option value="22">22 Children</option>
	<option value="23">23 Children</option>
	<option value="24">24 Children</option>
	<option value="25">25 Children</option>
	<option value="26">26 Children</option>
	<option value="27">27 Children</option>
	<option value="28">28 Children</option>
	<option value="29">29 Children</option>
	<option value="30">30 Children</option>
	<option value="31">31 Children</option>
	<option value="32">32 Children</option>
	<option value="33">33 Children</option>
	<option value="34">34 Children</option>
	<option value="35">35 Children</option>
	<option value="36">36 Children</option>
	<option value="37">37 Children</option>
	<option value="38">38 Children</option>
	<option value="39">39 Children</option>
	<option value="40">40 Children</option>
	<option value="41">41 Children</option>
	<option value="42">42 Children</option>
	<option value="43">43 Children</option>
	<option value="44">44 Children</option>
	<option value="45">45 Children</option>
	<option value="46">46 Children</option>
	<option value="47">47 Children</option>
	<option value="48">48 Children</option>
	<option value="49">49 Children</option>
	<option value="50">50 Children</option>
	<option value="51">51 Children</option>
	<option value="52">52 Children</option>
	<option value="53">53 Children</option>
	<option value="54">54 Children</option>
	<option value="55">55 Children</option>
	<option value="56">56 Children</option>
	<option value="57">57 Children</option>
	<option value="58">58 Children</option>
	<option value="59">59 Children</option>
	<option value="60">60 Children</option>
	<option value="61">61 Children</option>
	<option value="62">62 Children</option>
	<option value="63">63 Children</option>
	<option value="64">64 Children</option>
	<option value="65">65 Children</option>
	<option value="66">66 Children</option>
	<option value="67">67 Children</option>
	<option value="68">68 Children</option>
	<option value="69">69 Children</option>
	<option value="70">70 Children</option>
	<option value="71">71 Children</option>
	<option value="72">72 Children</option>
	<option value="73">73 Children</option>
	<option value="74">74 Children</option>
	<option value="75">75 Children</option>
	<option value="76">76 Children</option>
	<option value="77">77 Children</option>
	<option value="78">78 Children</option>
	<option value="79">79 Children</option>
	<option value="80">80 Children</option>
	<option value="81">81 Children</option>
	<option value="82">82 Children</option>
	<option value="83">83 Children</option>
	<option value="84">84 Children</option>
	<option value="85">85 Children</option>
	<option value="86">86 Children</option>
	<option value="87">87 Children</option>
	<option value="88">88 Children</option>
	<option value="89">89 Children</option>
	<option value="90">90 Children</option>
	<option value="91">91 Children</option>
	<option value="92">92 Children</option>
	<option value="93">93 Children</option>
	<option value="94">94 Children</option>
	<option value="95">95 Children</option>
	<option value="96">96 Children</option>
	<option value="97">97 Children</option>
	<option value="98">98 Children</option>
	<option value="99">99 Children</option>
	<option value="100">100 Children</option>
 
</select>&nbsp;<select id="cmbInfant" tabindex="3" onchange="CheckAdultInfant()">
	<option selected="selected" value="0">Infants</option>
	<option value="1">1 Infant</option>
	<option value="2">2 Infants</option>
	<option value="3">3 Infants</option>
	<option value="4">4 Infants</option>
	<option value="5">5 Infants</option>
	<option value="6">6 Infants</option>
	<option value="7">7 Infants</option>
	<option value="8">8 Infants</option>
	<option value="9">9 Infants</option>
	<option value="10">10 Infants</option>
	<option value="11">11 Infants</option>
	<option value="12">12 Infants</option>
	<option value="13">13 Infants</option>
	<option value="14">14 Infants</option>
	<option value="15">15 Infants</option>
	<option value="16">16 Infants</option>
	<option value="17">17 Infants</option>
	<option value="18">18 Infants</option>
	<option value="19">19 Infants</option>
	<option value="20">20 Infants</option>
	<option value="21">21 Infants</option>
	<option value="22">22 Infants</option>
	<option value="23">23 Infants</option>
	<option value="24">24 Infants</option>
	<option value="25">25 Infants</option>
	<option value="26">26 Infants</option>
	<option value="27">27 Infants</option>
	<option value="28">28 Infants</option>
	<option value="29">29 Infants</option>
	<option value="30">30 Infants</option>
	<option value="31">31 Infants</option>
	<option value="32">32 Infants</option>
	<option value="33">33 Infants</option>
	<option value="34">34 Infants</option>
	<option value="35">35 Infants</option>
	<option value="36">36 Infants</option>
	<option value="37">37 Infants</option>
	<option value="38">38 Infants</option>
	<option value="39">39 Infants</option>
	<option value="40">40 Infants</option>
	<option value="41">41 Infants</option>
	<option value="42">42 Infants</option>
	<option value="43">43 Infants</option>
	<option value="44">44 Infants</option>
	<option value="45">45 Infants</option>
	<option value="46">46 Infants</option>
	<option value="47">47 Infants</option>
	<option value="48">48 Infants</option>
	<option value="49">49 Infants</option>
	<option value="50">50 Infants</option>
	<option value="51">51 Infants</option>
	<option value="52">52 Infants</option>
	<option value="53">53 Infants</option>
	<option value="54">54 Infants</option>
	<option value="55">55 Infants</option>
	<option value="56">56 Infants</option>
	<option value="57">57 Infants</option>
	<option value="58">58 Infants</option>
	<option value="59">59 Infants</option>
	<option value="60">60 Infants</option>
	<option value="61">61 Infants</option>
	<option value="62">62 Infants</option>
	<option value="63">63 Infants</option>
	<option value="64">64 Infants</option>
	<option value="65">65 Infants</option>
	<option value="66">66 Infants</option>
	<option value="67">67 Infants</option>
	<option value="68">68 Infants</option>
	<option value="69">69 Infants</option>
	<option value="70">70 Infants</option>
	<option value="71">71 Infants</option>
	<option value="72">72 Infants</option>
	<option value="73">73 Infants</option>
	<option value="74">74 Infants</option>
	<option value="75">75 Infants</option>
	<option value="76">76 Infants</option>
	<option value="77">77 Infants</option>
	<option value="78">78 Infants</option>
	<option value="79">79 Infants</option>
	<option value="80">80 Infants</option>
	<option value="81">81 Infants</option>
	<option value="82">82 Infants</option>
	<option value="83">83 Infants</option>
	<option value="84">84 Infants</option>
	<option value="85">85 Infants</option>
	<option value="86">86 Infants</option>
	<option value="87">87 Infants</option>
	<option value="88">88 Infants</option>
	<option value="89">89 Infants</option>
	<option value="90">90 Infants</option>
	<option value="91">91 Infants</option>
	<option value="92">92 Infants</option>
	<option value="93">93 Infants</option>
	<option value="94">94 Infants</option>
	<option value="95">95 Infants</option>
	<option value="96">96 Infants</option>
	<option value="97">97 Infants</option>
	<option value="98">98 Infants</option>
	<option value="99">99 Infants</option>
	<option value="100">100 Infants</option>
 
    </select>
    <span class="groupFormResult">
			&nbsp;&nbsp;
			&nbsp;&nbsp;
		</span>
    	<div style="padding-top:5px;">&nbsp;Minimum 10 People for a group(Not Including Infant)</div>

	</td>
  </tr>
  <tr>
    <td colspan="4" height="10"></td>
  </tr>
  <tr>
    <td colspan="4" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_1_1", "Organizer's Details", _stdLanguage)%></td>
  </tr>
  <tr>
    <td rowspan="15"></td>
    <td colspan="3" height="10"></td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_2", "Title", _stdLanguage)%>:</td>
    <td class="groupForm" colspan="2">
<select id="ddlTitle" runat="server"> </select>  
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_3", "First Name", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" colspan="2">
		<input type="text"  id="txtFirstName" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_4", "Surname", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" colspan="2">
		<input type="text"  id="txtLastName" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_6", "Mobile No", _stdLanguage)%>.:</td>
    <td class="groupForm" colspan="2">
		<input type="text"  id="txtContactMobile" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_7", "Home/Business No.", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" colspan="2">
		<input type="text"  id="txtContactPhoneNumber" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_8", "Email", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" colspan="2">
		<input type="text" id="txtEmailAddress" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_9", "Confirm Email", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" colspan="2">
		<input type="text"  id="txtEmailAddressReEnter" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_10", "Address 1", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" colspan="2">
		<input type="text"  id="txtAddress1" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_11", "Address 2", _stdLanguage)%>:</td>
    <td class="groupForm" colspan="2">
		<input type="text"  id="txtAddress2" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_12", "Town/City", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" colspan="2">
		<input type="text" id="txtTownCity" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_13", "Country", _stdLanguage)%>:</td>
    <td class="groupForm" colspan="2">
		<input type="text" id="txtContactCounty" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_14", "Postal Code", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" colspan="2">
		<input type="text" id="txtPostcode" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm" style="height: 22px;"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_15", "Country", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" style="height: 22px;" colspan="2">
<select id="ddlCountry" runat="server"> </select>  
	</td>
  </tr>
  <tr>
    <td colspan="3" height="10"></td>
  </tr>
  <tr>
    <td colspan="4" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_26", "Flight Details", _stdLanguage)%></td>
  </tr>
  <tr>
    <td colspan="4" height="10"></td>
  </tr>
  <tr>
        <td style="height: 19px;" colspan="2"></td>
	    <td class="groupForm" style="height: 19px;"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_27", "From", _stdLanguage)%>:</td>
	    <td class="groupForm" style="height: 19px;"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_28", "To", _stdLanguage)%>:</td>   
    </tr>
	<tr>
	    <td colspan="2"></td>
		<td class="groupForm">
			<select id="optOrigin" runat="server" onchange="javascript:getDestination();" />            
		</td>
		<td class="groupForm">
			<select id="optDestination" runat="server" />
		</td>
	</tr>
	<tr>
	    <td colspan="2"></td>
		<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_29", "Date of Departure", _stdLanguage)%>:</td>
		<td id="ra" class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_30", "Date of Return", _stdLanguage)%>:</td>
	</tr>
	<tr>
	    <td colspan="2"></td>
		<td class="groupFormClear">			    
				  <%=tikAeroB2C.Classes.Calendar.getDLLCalendar("3", "outward", _stdLanguage)%>
				  <div id="Cal3" style="z-index: 900; position: absolute;"/>
		</td>
		<td class="groupFormClear">
				 <%=tikAeroB2C.Classes.Calendar.getDLLCalendar("4", "return", _stdLanguage)%>
				 <div id="Cal4" style="z-index: 1000; position: absolute;"/>
		</td>
	</tr>
	<tr>
	    <td colspan="2"></td>
		<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_31", "Prefered Departure Time", _stdLanguage)%>:</td>
		<td id="rc" class="groupForm"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_37", "Prefered Return Departure Time", _stdLanguage)%>:</td>
    </tr>
	<tr>
	    <td colspan="2"></td>
		<td class="groupFormClear">
            <select id="cmbPreferedDepartureTime" > 
                <option value="Anytime"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_32", "Anytime", _stdLanguage)%></option>
                <option value="AM"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_33", "AM", _stdLanguage)%></option>
                <option value="PM"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_34", "PM", _stdLanguage)%></option>
            </select>
		</td>
		<td id="rd" class="groupFormClear">
            <select id="cmbPreferedReturnDepartureTime" > 
                <option value="Anytime"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_32", "Anytime", _stdLanguage)%></option>
                <option value="AM"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_33", "AM", _stdLanguage)%></option>
                <option value="PM"><%=tikAeroB2C.Classes.Language.Value("GroupBooking_34", "PM", _stdLanguage)%></option>
            </select>
		</td>
	</tr>
	<tr>
	    <td colspan="2"></td>
		<td colspan="2" class="groupFormClear">
				<%=tikAeroB2C.Classes.Language.Value("GroupBooking_35", "Additional Information", _stdLanguage)%>:<br />
				<textarea id="txtAdditionalInformation" name="dddd" cols="54" rows="4" runat="server"></textarea>
		</td>
    </tr>
    <tr>
	<td colspan="4" class="groupFormClear">
	            <div class="RegisterCancelButton" onclick="SendMailGroupBooking('ctl00_');">
                    <div class="buttonCornerLeft"></div>
                    <div class="buttonContent">
                    <%=tikAeroB2C.Classes.Language.Value("GroupBooking_38", "Submit", _stdLanguage)%>
                    </div>
                    <div class="buttonCornerRight"></div>
                </div>	
	</td>
  </tr>
  <tr>
	<td colspan="2">&nbsp;</td>
	<td class="groupForm" colspan="2"> <div class="ErrorList">
    <asp:Label ID="LabError" runat="server"></asp:Label></div></td>
  </tr>
</table>
<table style="top:0px; left:770px; position:absolute; z-index:99;" width="225" border="0" cellspacing="0" cellpadding="0">
	<tr>
	 <td style="padding:10px 0px 5px 0px">
		 <%--<iframe src="./HTML/Popup/banner.html" name="BannerIFrame" id="BannerIFrame" width="225" height="768" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" align="left"></iframe>--%>
	 </td>
	</tr>
</table>
  </div> 