﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Text;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using System.Configuration;

namespace tikAeroB2C.UserControls
{
    public partial class PaymentCreditCard : System.Web.UI.UserControl
    {
        public string _parameter;
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Helper objHelper = new Helper();
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                try
                {
                    Library objLi = new Library();
                    B2CVariable objUv = B2CSession.Variable;
                    
                    BookingHeader bookingHeader = B2CSession.BookingHeader;
                    Passengers passengers = B2CSession.Passengers;
                    Quotes quotes = B2CSession.Quotes;
                    Fees fees = B2CSession.Fees;
                    Payments payments = B2CSession.Payments;
                    Itinerary itinerary = B2CSession.Itinerary;
                    Mappings mappings = B2CSession.Mappings;
                    Taxes taxes = B2CSession.Taxes;
                    Vouchers vouchers = B2CSession.Vouchers;
                    Agents objAgents = B2CSession.Agents;

                    if (objUv == null ||
                        bookingHeader == null ||
                        passengers == null ||
                        passengers.Count == 0 ||
                        itinerary == null ||
                        itinerary.Count == 0 ||
                        mappings == null ||
                        mappings.Count == 0 ||
                        objHelper.SessionTimeout() == true)
                    {
                        B2CSession.Variable = null;
                    }
                    else
                    {
                        decimal dclTotalVcAmount = 0;
                        decimal dclVaAmount = 0;
                        decimal dclTotalPayment = 0;

                        System.Xml.Xsl.XslTransform objTransform = null;
                        System.Xml.Xsl.XsltArgumentList objArgument = objHelper.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                        if (string.IsNullOrEmpty(_parameter) == false)
                        {
                            //Load Summary.
                            using (StringWriter stw = new StringWriter())
                            {
                                using (XmlWriter xtw = XmlWriter.Create(stw))
                                {
                                    xtw.WriteStartElement("multiple_form");
                                    {
                                        using (StringReader rd = new StringReader(_parameter))
                                        {
                                            XPathDocument xmlDoc = new XPathDocument(rd);
                                            XPathNavigator nv = xmlDoc.CreateNavigator();

                                            foreach (XPathNavigator n in nv.Select("vouchers/voucher"))
                                            {
                                                xtw.WriteStartElement("voucher");
                                                {
                                                    dclVaAmount = objLi.ReadVoucherAmount(vouchers, XmlHelper.XpathValueNullToGUID(n, "voucher_id"));
                                                    dclTotalVcAmount = dclTotalVcAmount + dclVaAmount;
                                                    xtw.WriteStartElement("voucher_id");
                                                    {
                                                        xtw.WriteValue(XmlHelper.XpathValueNullToEmpty(n, "voucher_id"));
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("voucher_number");
                                                    {
                                                        xtw.WriteValue(XmlHelper.XpathValueNullToEmpty(n, "voucher_number"));
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("voucher_amount");
                                                    {
                                                        xtw.WriteValue(string.Format("{0:0.00}", dclVaAmount));
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("currency_rcd");
                                                    {
                                                        xtw.WriteValue(bookingHeader.currency_rcd);
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("form_of_payment_rcd");
                                                    {
                                                        xtw.WriteValue(XmlHelper.XpathValueNullToEmpty(n, "form_of_payment_rcd"));
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("form_of_payment_subtype_rcd");
                                                    {
                                                        xtw.WriteValue(XmlHelper.XpathValueNullToEmpty(n, "form_of_payment_subtype_rcd"));
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("display_name");
                                                    {
                                                        xtw.WriteValue(XmlHelper.XpathValueNullToEmpty(n, "display_name"));
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("fee_amount_incl");
                                                    {
                                                        xtw.WriteValue(XmlHelper.XpathValueNullToDecimal(n, "fee_amount_incl"));
                                                    }
                                                    xtw.WriteEndElement();
                                                    xtw.WriteStartElement("fee_amount");
                                                    {
                                                        xtw.WriteValue(XmlHelper.XpathValueNullToDecimal(n, "fee_amount"));
                                                    }
                                                    xtw.WriteEndElement();
                                                }
                                                xtw.WriteEndElement();

                                            }
                                        }

                                        xtw.WriteStartElement("total_amount");
                                        {
                                            dclTotalPayment = objLi.CalOutStandingBalance(quotes, fees, payments);
                                            xtw.WriteValue(dclTotalPayment);
                                        }
                                        xtw.WriteEndElement();
                                    }
                                    xtw.WriteEndElement();
                                }

                                objTransform = objHelper.GetXSLDocument("MultipleFormSummary");
                                dvFormCCSummary.InnerHtml = objLi.RenderHtml(objTransform,
                                                                             objArgument,
                                                                             stw.ToString());
                            }
                        }

                        //Load Initial setting.
                        GetAgencyInfo(dclTotalPayment - dclTotalVcAmount, objUv, bookingHeader, passengers, quotes, fees, payments, itinerary, mappings, taxes);

                        //Load Country
                        ListItem objList;
                        if (bookingHeader.country_rcd.Length == 0)
                        {
                            //Set default country
                            bookingHeader.country_rcd = objAgents[0].country_rcd;
                        }

                        Countries objCountries = CacheHelper.CacheCountry();
                        //Get From config first
                        string[] strCountryOrder = null;
                        if (!string.IsNullOrEmpty(B2CSetting.CountryOrder))
                        {
                            strCountryOrder = B2CSetting.CountryOrder.ToUpper().Split(',');
                            for (int i = 0; i < strCountryOrder.Length; i++)
                            {
                                objList = new ListItem();
                                objList.Value = strCountryOrder[i].ToString();
                                for (int j = 0; j < objCountries.Count; j++)
                                {
                                    if (objCountries[j].country_rcd.Equals(strCountryOrder[i]))
                                    {
                                        objList.Text = objCountries[j].display_name;
                                        break;
                                    }
                                }
                                
                                if (objList.Value == bookingHeader.country_rcd)
                                { objList.Selected = true; }

                                optMultiCountry.Items.Add(objList);
                            }
                        }
                        //Get from DB
                        for (int i = 0; i < objCountries.Count; i++)
                        {
                            if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                            {
                                objList = new ListItem();
                                objList.Value = objCountries[i].country_rcd;
                                objList.Text = objCountries[i].display_name;

                                if (objList.Value == bookingHeader.country_rcd)
                                { objList.Selected = true; }

                                optMultiCountry.Items.Add(objList);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    objHelper.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }
        }
        private bool GetAgencyInfo(decimal dclTotalBalance,
                                   B2CVariable objUv,
                                   BookingHeader bookingHeader,
                                   Passengers passengers,
                                   Quotes quotes,
                                   Fees fees,
                                   Payments payments,
                                   Itinerary itinerary,
                                   Mappings mappings,
                                   Taxes taxes)
        {
            string tempXml;

            Library objLi = new Library();
            Helper objXsl = new Helper();
            System.Xml.Xsl.XslTransform objTransform = null;

            //Load Credit Card Type
            StringBuilder stb = new StringBuilder();
            ServiceClient objService = new ServiceClient();
            XPathNavigator nv;
            ListItem objList;

            string formOfPayment = string.Empty;
            string formOfPaymentSubtype = string.Empty;
            string strFeeLevel = string.Empty;

            short sOdFlag = 0;
            short sMinimumFeeFlag = 0;

            decimal dFeePercentage = 0;
            decimal dFeeAmount = 0;
            decimal dFeeAmountIncl = 0;
            decimal dFee = 0;
            decimal dFeeIncl = 0;

            int iFactor = 0;

            objService.objService = B2CSession.AgentService;
            tempXml = objService.GetFormOfPaymentSubTypes("CC", string.Empty);

            using (StringWriter stw = new StringWriter())
            {
                XmlWriterSettings xmlSetting = new XmlWriterSettings();
                xmlSetting.OmitXmlDeclaration = true;
                xmlSetting.Indent = false;
                using (XmlWriter xtw = XmlWriter.Create(stw, xmlSetting))
                {
                    xtw.WriteStartElement("FormOfPaymentSubtypeFees");
                    {
                        GetFormOfPaymentSubTypeFee(xtw, objService, bookingHeader, bookingHeader.currency_rcd);
                    }
                    xtw.WriteEndElement();
                }
                
                objUv.FormOfPaymentFee = stw.ToString();
            }

            using (StringReader srd = new StringReader("<Payment>" + tempXml + objUv.FormOfPaymentFee + "</Payment>"))
            {
                XPathDocument xmlDoc = new XPathDocument(srd);
                nv = xmlDoc.CreateNavigator();

                foreach (XPathNavigator n in nv.Select("Payment/FormOfPayments/GetFormOfPayments[internet_payment_flag = 1]"))
                {
                    objList = new ListItem();

                    formOfPayment = n.SelectSingleNode("form_of_payment_rcd").InnerXml;
                    formOfPaymentSubtype = n.SelectSingleNode("form_of_payment_subtype_rcd").InnerXml;
                    stb.Append(formOfPaymentSubtype + "{}");
                    stb.Append(formOfPayment + "{}");
                    stb.Append(n.SelectSingleNode("cvv_required_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_cvv_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_expiry_date_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("expiry_date_required_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("address_required_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_address_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_issue_date_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_issue_number_flag").InnerXml + "{}");

                    dFeeAmountIncl = 0;
                    dFeeAmount = 0;

                    strFeeLevel = string.Empty;
                    sOdFlag = 0;
                    sMinimumFeeFlag = 0;
                    dFeePercentage = 0;
                    dFee = 0;
                    dFeeIncl = 0;
                    string strDisplayName = string.Empty;

                    //Find fee with route. If not find go to find fee without route.
                    foreach (XPathNavigator nf in nv.Select("Payment/FormOfPaymentSubtypeFees/Fee[fee_rcd = '" + formOfPaymentSubtype + "'][currency_rcd = '" + bookingHeader.currency_rcd + "']"))
                    {
                        payments.FindFee(nf,
                                       ref strFeeLevel,
                                       ref sOdFlag,
                                       ref sMinimumFeeFlag,
                                       ref dFeePercentage,
                                       ref dFee,
                                       ref dFeeIncl,
                                       ref strDisplayName);

                        dFeeAmount = dFee;
                        dFeeAmountIncl = dFeeIncl;

                        break;
                    }

                    iFactor = payments.calCalculateCreditCardFee(ref dFeeAmount,
                                                                 ref dFeeAmountIncl,
                                                                 itinerary,
                                                                 dFeePercentage,
                                                                 strFeeLevel,
                                                                 sOdFlag,
                                                                 sMinimumFeeFlag,
                                                                 dclTotalBalance,
                                                                 objUv.Adult,
                                                                 objUv.Child,
                                                                 objUv.Infant);

                    stb.Append(dFeeAmountIncl + "{}");
                    stb.Append(dFeeAmount + "{}");
                    stb.Append(n.SelectSingleNode("validate_document_number_flag").InnerXml + "{}");
                    stb.Append(string.Format("{0:0.00}", dFeePercentage) + "{}");
                    stb.Append(string.Format("{0:0}", sMinimumFeeFlag) + "{}");
                    stb.Append(string.Format("{0:0}", iFactor) + "{}");
                    stb.Append(string.Format("{0:0.00}", dFee) + "{}");
                    stb.Append(string.Format("{0:0.00}", dFeeIncl));

                    objList.Value = stb.ToString();
                    objList.Text = n.SelectSingleNode("display_name").InnerXml;

                    if (!string.IsNullOrEmpty(B2CSetting.DefaultCardType))
                    {
                        if (formOfPaymentSubtype.Equals(B2CSetting.DefaultCardType))
                        {
                            objList.Selected = true;
                        }
                    }
                    optMultiCardType.Items.Add(objList);
                    objList = null;
                    //Clear 
                    stb.Remove(0, stb.Length);
                }
            }
            return true;
        }
        #region Helper
        private void GetFormOfPaymentSubTypeFee(XmlWriter xtw, ServiceClient objService,  BookingHeader bookingHeader, string currencyRcd)
        {
            //Get CC payment Fee
            XmlReaderSettings setting = new XmlReaderSettings();
            setting.IgnoreWhitespace = true;
            using (XmlReader reader = XmlReader.Create(new StringReader(objService.GetFormOfPaymentSubtypeFees("CC", string.Empty, currencyRcd, bookingHeader.agency_code, DateTime.MinValue).GetXml()), setting))
            {
                while (!reader.EOF)
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("Fee"))
                    {
                        xtw.WriteNode(reader, false);
                    }
                    else
                    {
                        reader.Read();
                    }
                }
            }

            //Get EFT Payment Fee
            using (XmlReader reader = XmlReader.Create(new StringReader(objService.GetFormOfPaymentSubtypeFees("EFT", string.Empty, currencyRcd, bookingHeader.agency_code, DateTime.MinValue).GetXml()), setting))
            {
                while (!reader.EOF)
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("Fee"))
                    {
                        xtw.WriteNode(reader, false);
                    }
                    else
                    {
                        reader.Read();
                    }
                }
            }
        }
        #endregion
    }
}