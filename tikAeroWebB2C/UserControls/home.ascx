<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="home.ascx.cs" Inherits="tikAeroB2C.UserControls.home" %>
<%@ Assembly Name="TikAero.Web.CMS2.0" %>
<%@ Import Namespace="TikAero.Web.CMS2._0" %>
<!--<div class="WrapperPanelLeft">
    <div class="HomeLinks">
        <div class="HomeLinksList">
            <a href="#">
                <%=tikAeroB2C.Classes.Language.Value("Menu_1", "Travel Agency", _stdLanguage)%>
            </a>
        </div>
        <div class="HomeLinksList">
            <a href="#">
                <%=tikAeroB2C.Classes.Language.Value("Menu_2", "Corporate", _stdLanguage)%>
            </a>
        </div>
        <div class="clearboth"></div>
        <div class="HomeLinksList" onclick="LoadCob(false, '');">
			<a href="#">
           		<%=tikAeroB2C.Classes.Language.Value("Menu_3", "My Booking", _stdLanguage)%>
		   </a>
        </div>
        <div class="HomeLinksList">
            <a href="#">
                <%=tikAeroB2C.Classes.Language.Value("Menu_4", "Web Checkin", _stdLanguage)%>
            </a>
        </div>
    </div>
</div>-->

<div class="WrapperPanelRight">
   <!-- <%		   
        string strLanguage = string.Empty;
        if (Session["LanCode"] == null)
        {
            strLanguage = ConfigurationManager.AppSettings["DefaultLanguage"].Split('-')[0];
        }
        else
        {
            strLanguage = Session["LanCode"].ToString().Split('-')[0];
        }
        string parameter = "html/" + strLanguage + "/HomeFlightSearch.asp?lang=" + strLanguage;
        Page.Response.Write("<iframe frameborder=\"0\" scrolling=\"no\" id=\"frmHtmlContainer\" src=\"" + parameter + "\" width=\"100%\" onload=\"resizeFrame(document.getElementById('frmHtmlContainer'));window.parent.scroll(0,0);\"></iframe>");
        %> -->
		
		<div class="col-two">
			<div class="mainbanner">
				<img src="App_Themes/Default/Images/mainbanner.jpg" alt="Fly with Style by Style Airways." title="Fly with Style by Style Airways." />
			</div>
			
			<div class="subbanner first">
				<div class="content">
					<img src="App_Themes/Default/Images/content1.jpg" alt="Airport services" title="Airport services" />
					<div class="texthead"><a href="#">Airport services</a></div>
					<div class="textdetail">We are dedicated to the vision that a medium sized, private and independently owned company can provide.</div>
				</div>
			</div>
			
			<div class="subbanner">
				<div class="content">
					<img src="App_Themes/Default/Images/content2.jpg" alt="Hotels worldwide" title="Hotels worldwide" />
					<div class="texthead"><a href="#">Hotels worldwide</a></div>
					<div class="textdetail">Choose from over 250,000 hotels worldwide. Click here for special offers!</div>
				</div>
			</div>
			
			<div class="subbanner">
				<div class="content">
					<img src="App_Themes/Default/Images/content3.jpg" alt="Train tickets" title="Train tickets" />
					<div class="texthead"><a href="#">Hotels worldwide</a></div>
					<div class="textdetail">No stress, no hassle � travel in Germany with Deutsche Bahn for just 24,99 &euro;!</div>
				</div>
			</div>
			
		</div>
		
		<div class="col-three">
			<div class="banner">
				<img src="App_Themes/Default/Images/banner1.jpg" alt="Ready to go! Check out our timetable." title="Ready to go! Check out our timetable." />
			</div>
			
			<div class="banner">
				<img src="App_Themes/Default/Images/banner2.jpg" alt="Shopping Guide." title="Shopping Guide." />
			</div>
			
			<div class="banner">
				<img src="App_Themes/Default/Images/banner3.jpg" alt="Call and book." title="Call and book." />
			</div>
		</div>
</div>