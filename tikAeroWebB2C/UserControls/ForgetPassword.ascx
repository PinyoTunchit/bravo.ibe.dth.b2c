<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.ascx.cs" Inherits="tikAeroB2C.UserControls.ForgetPassword" %>
<div class="BoxGrayRight">
  <div class="BoxGrayTop"></div>
  
  <div class="BoxGrayContent">
  	<div class="BoxGrayContentHeader"><%=tikAeroB2C.Classes.Language.Value("Registration_56", "User Access to Your Airline", _stdLanguage)%></div>
  	<div class="clearboth"></div>
    
	<%=tikAeroB2C.Classes.Language.Value("Registration_57", "Please enter your user-ID and e-mail address as stored in your personal profile. Your new password will be automatically generated and sent to this e-mail address.", _stdLanguage)%>
	<div class="ForgetHeader"><%=tikAeroB2C.Classes.Language.Value("Registration_58", "Forget Password", _stdLanguage)%></div>
	<div class="clearboth"></div>
    
	<div class="ForgetLeft "><%=tikAeroB2C.Classes.Language.Value("Registration_59", "User-ID (4-15 characters)", _stdLanguage)%></div>
    
	<div class="ForgetInput">
        <div class="ForgetInputLeft"></div>
            <input type="text" id="txtUserID" />
        <div class="ForgetInputRight"></div>
	</div>
	<div class="clearboth"></div>
    		
	<div class="ForgetRight">
		<div class="ButtonBigLeft"></div>
		<div class="ButtonBigMid" onclick="ForgetPassword();"> <%=tikAeroB2C.Classes.Language.Value("Registration_60", "Request", _stdLanguage)%></div>
		<div class="ButtonBigRight"></div>
	</div>
    
  <div class="clearboth"></div>
  </div>
  <div class="BoxGrayEnd"></div>

<div class="clearboth"></div>
</div>