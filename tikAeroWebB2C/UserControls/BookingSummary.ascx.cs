using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;
using System.Collections.Specialized;
using System.Text;

namespace tikAeroB2C.UserControls
{
    public partial class BookingSummary : System.Web.UI.UserControl
    {
        public string _parameter;
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Helper objXsl = new Helper();
                try
                {
                    B2CVariable objUv = B2CSession.Variable;
                    Library ObjLi = new Library();

                    //Load Langauge to String Dictionary
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                    System.Xml.Xsl.XslTransform objTransform = null;
                    System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    string serverPath = Server.MapPath("~") + @"\xsl\";
                    string ExtendXML = "";
                    //if (objUv == null) objUv = new B2CVariable() { CurrentStep = 10 };


                    if (objUv.CurrentStep == 13)
                    {
                        if (B2CSession.Client != null && B2CSession.Client.profile_on_hold_by.ToString() == "00000000-0000-0000-0000-000000000000"
                            && B2CSession.Client.status_code == "A")
                            ExtendXML = "<ClientOnHold>F</ClientOnHold>";
                        else
                            ExtendXML = "<ClientOnHold>T</ClientOnHold>";

                        ExtendXML = ExtendXML + "<application_agency>" + B2CSetting.DefaultAgencyCode + "</application_agency>";

                        objTransform = objXsl.GetXSLDocument("BookingDetail");
                        if (string.IsNullOrEmpty(_parameter))
                        {
                            BookingHeader bookingHeader = B2CSession.BookingHeader;
                            if (bookingHeader != null)
                            {
                                ServiceClient obj = new ServiceClient();
                                obj.objService = B2CSession.AgentService;
                                _parameter = obj.GetItinerary(bookingHeader.booking_id.ToString(), tikAeroB2C.Classes.Language.CurrentCode().ToUpper(), string.Empty, string.Empty);
                                obj = null;
                            }
                            else
                            {
                                _parameter = string.Empty;
                            }
                        }

                        //Generate itinerary.
                        Page.Response.Write(ObjLi.RenderHtml(objTransform,
                                                             objArgument,
                                                             _parameter.Replace("</Booking>", ExtendXML + "</Booking>")));
                    }
                    else
                    {
                        if (objUv.CurrentStep == 10)
                        {
                            if (B2CSession.BookingHeader != null)
                            {
                                ServiceClient obj = new ServiceClient();
                                obj.objService = B2CSession.AgentService;
                                _parameter = obj.ViewBookingChange(B2CSession.BookingHeader.booking_id.ToString(), tikAeroB2C.Classes.Language.CurrentCode().ToUpper(), string.Empty);
                                obj = null;
                            }
                            else
                            {
                                _parameter = string.Empty;
                            }
                        }

                        if (_parameter == null && B2CSession.BookingSummary != null)
                        {
                            _parameter = B2CSession.BookingSummary as string;
                        }

                        NameValueCollection _Setting = (NameValueCollection)ConfigurationManager.GetSection("InsuranceACESetting");

                        // build ACE xml to Itin
                        Fee objInsFee = new Fee();
                        if (B2CSession.InsuaranceFee != null)
                        {
                            objInsFee = B2CSession.InsuaranceFee;
                        }

                        if (B2CSession.ACEQuotePassengers != null)
                        {
                            ACEQuotePassengers ACEQuotePassengers = new ACEQuotePassengers();
                            ACEQuotePassengers = B2CSession.ACEQuotePassengers;
                            StringBuilder insuranceItin = new StringBuilder();
                            DateTime IssueDt = DateTime.Now;
                            string PolicyNumber = "";
                            PolicyNumber = objInsFee.comment;

                            insuranceItin.Append(@"<PolicyNumber>" + PolicyNumber + "</PolicyNumber>");
                            insuranceItin.Append(@"<CurrentPremium>" + objInsFee.fee_amount + "</CurrentPremium>");
                            insuranceItin.Append(@"<currency_rcd>" + objInsFee.currency_rcd + "</currency_rcd>");
                            insuranceItin.Append(@"<IssueDt>" + IssueDt.ToString("yyyy-MM-dd HH:mm:ss") + "</IssueDt>");
                            insuranceItin.Append(@"<EffectiveDt>" + ACEQuotePassengers[0].EffectiveDt.ToString("yyyy-MM-dd HH:mm:ss") + "</EffectiveDt>");
                            insuranceItin.Append(@"<ExpirationDt>" + ACEQuotePassengers[0].ExpirationDt.ToString("yyyy-MM-dd HH:mm:ss") + "</ExpirationDt>");

                            foreach (ACEQuotePassenger ACEQuotePassenger in ACEQuotePassengers)
                            {
                                insuranceItin.Append(@"<InsurancePassenger>");
                                insuranceItin.Append(@"<lastname>" + ACEQuotePassenger.lastname + "</lastname>");
                                insuranceItin.Append(@"<middlename>" + ACEQuotePassenger.middlename + "</middlename>");
                                insuranceItin.Append(@"<firstname>" + ACEQuotePassenger.firstname + "</firstname>");
                                insuranceItin.Append(@"<title_rcd>" + ACEQuotePassenger.title_rcd + "</title_rcd>");
                                insuranceItin.Append(@"<date_of_birth>" + ACEQuotePassenger.date_of_birth.Date.ToString("yyyy-MM-dd HH:mm:ss") + "</date_of_birth>");
                                insuranceItin.Append(@"</InsurancePassenger>");
                            }

                            _parameter = _parameter.Replace("</Booking>", "<Insurance>" + insuranceItin.ToString() + "</Insurance></Booking>");

                        }
                        else
                        {
                            if(string.IsNullOrEmpty(_parameter)) _parameter = "<Booking><Step>" + objUv.CurrentStep + "</Step><Parameter>" + _parameter + "</Parameter></Booking>";
                            _parameter = _parameter.Replace("</Booking>", "<Insurance></Insurance></Booking>");
                        }


                        tikAeroB2C.B2CSession.BookingSummary = _parameter;

                        // ClearSessionItinerary                       
                        if (tikSystem.Web.Library.ConfigurationHelper.ToBoolean("ClearSessionItinerary"))
                        {
                            if (objUv.CurrentStep == 10 && B2CSession.Client != null && B2CSession.Client.profile_on_hold_by.ToString() == "00000000-0000-0000-0000-000000000000"
                            && B2CSession.Client.status_code == "A")
                            {
                                objUv.CurrentStep = 10;
                            }
                            else
                            {
                                objUv.CurrentStep = 1;
                            }
                        }
                        else
                        {
                            objUv.CurrentStep = 10;
                        }

                        objTransform = objXsl.GetXSLDocument("BookingSummary");
                        Page.Response.Write(ObjLi.RenderHtml(objTransform, objArgument, _parameter));
                    }

                    objUv = null;
                    ObjLi = null;
                }
                catch (Exception ex)
                {
                    objXsl.SendErrorEmail(ex, "Booking Xml<br/>" + _parameter.Replace("<", "&lt;").Replace(">", "&gt;"));
                    throw ex;
                }
            }
        }
    }
}