using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using tikSystem.Web.Library;

namespace tikAeroB2C.UserControls
{
    public partial class home : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Remove old session Data
            try 
            {
                if (IsPostBack == false)
                {
                    //Load Language Dictionary
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                    B2CSession.BookingHeader = null;
                    B2CSession.Itinerary = null;
                    B2CSession.Passengers = null;
                    B2CSession.Quotes = null;
                    B2CSession.Fees = null;
                    B2CSession.Mappings = null;
                    B2CSession.Services = null;
                    B2CSession.Remarks = null;
                    B2CSession.Payments = null;
                    B2CSession.Taxes = null;
                }
            }
            catch (Exception ex)
            {
                Classes.Helper objHp = new Classes.Helper();
                objHp.SendErrorEmail(ex, string.Empty);
                //throw ex;
            }
            
        }
    }
}