<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsAdmin.ascx.cs" Inherits="tikAeroB2C.UserControls.NewsAdmin" %>
    
<iframe  style="height:0px; z-index:666;position:absolute; display: none;"  id='fdCal3'  src='' class='frmcls'></iframe>
<iframe  style="height:0px; z-index:555;position:absolute; display: none;"  id='fdCal4'  src='' class='frmcls'></iframe>    
  <div id="dvLoginResult">
    <table width ="830" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" height="622" class="RegisterB2C">
	    <tr>
			<td class="headerWithSub" colspan="2" align="left"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_1", "Search by:", _stdLanguage)%></td> 
		</tr>
		<tr><td colspan="2" style="height: 10px"></td></tr>
		<tr>
			<td colspan="2" class="groupForm">
				<%=tikAeroB2C.Classes.Language.Value("NewsRegister_2", "Sign up now to receive the latest news on low fares, travel offers and promotions.<br/><br/>It only takes a minute.", _stdLanguage)%>
			</td>
		</tr>
		<tr><td colspan="2" style="height: 10px"></td></tr>
		<TR>
			<td colspan="2" class="groupForm"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_3", "Country:", _stdLanguage)%></STRONG></TD>
		</TR>				
		<tr>
			<td colspan="2" class="groupForm2">
			    <table id="tbListCountry" class="groupForm2" border="0" align ="left">
	            <tr>
	                <td><%=chkCountry%></td>
	            </tr>
	            </table>
	        </td>
		</tr>
		<tr><td colspan="2" style="height: 20px">&nbsp;</td></tr>
		<TR>
			<td colspan="2" class="groupForm"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_4", "Departure:", _stdLanguage)%></STRONG></TD>
		</TR>
		<tr>
			<td colspan="2" class="groupForm2" style="height: 52px;">
        	<table id="tbListDeparture" class="groupForm2" border="0" align ="left">
            	<tr>
            		<td><input id="chkListDeparture_0" type="checkbox" name="chkListDeparture" value="Alderney"/>
            		<label for="chkListDeparture_0">Alderney</label></td>
            		<td><input id="chkListDeparture_1" type="checkbox" name="chkListDeparture" value="Bristol"/>
            		<label for="chkListDeparture_1">Bristol</label></td>
            		<td><input id="chkListDeparture_2" type="checkbox" name="chkListDeparture" value="Dinard"/>
            		<label for="chkListDeparture_2">Dinard</label></td>
            		<td><input id="chkListDeparture_3" type="checkbox" name="chkListDeparture" value="Gatwick"/>
            		<label for="chkListDeparture_3">Gatwick</label></td>
            		<td><input id="chkListDeparture_4" type="checkbox" name="chkListDeparture" value="Guernsey"/>
            		<label for="chkListDeparture_4">Guernsey</label></td>		
            	</tr>
            	<tr>
            		<td><input id="chkListDeparture_5" type="checkbox" name="chkListDeparture" value="Jersey"/>
            		<label for="chkListDeparture_5">Jersey</label></td>
            		<td><input id="chkListDeparture_6" type="checkbox" name="chkListDeparture" value="Manchester"/>
            		<label for="chkListDeparture_6">Manchester</label></td>
            		<td><input id="chkListDeparture_7" type="checkbox" name="chkListDeparture" value="Southampton"/>
		            <label for="chkListDeparture_7">Southampton</label></td>
		            <td><input id="chkListDeparture_8" type="checkbox" name="chkListDeparture" value="Stansted"/>
		            <label for="chkListDeparture_8">Stansted</label></td>
		            <td><input id="chkListDeparture_9" type="checkbox" name="chkListDeparture" value="East Midlands"/>
		            <label for="chkListDeparture_9">East Midlands(for Nottingham, Leicester & Derby)</label></td>		
	            </tr>
	            <tr>
	            <td colspan=5><input id="chkListDeparture_ALL" type="checkbox" name="chkListDeparture_ALL" onclick="validDepartureAll(this.checked);"/>
		            <label for="chkListDeparture_ALL">ALL</label></td>
	            </tr>	
	
            </table>
            </td>
		</tr>
		<tr><td colspan="2" class="groupForm" style="height: 20px">&nbsp;</td></tr>
		<TR>
			<td colspan="2" class="groupForm"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_5", "Destinations:", _stdLanguage)%></STRONG></TD>
		</TR>
		<tr>
		    <td colspan="2" class="groupForm2">
	        <table id="tbListDestination" class="groupForm2" border="0" align ="left">
	            <tr>
		            <td><input id="chkListDestination_0" type="checkbox" name="chkListDestination" value="Alderney"/>
		            <label for="chkListDestination_0">Alderney</label></td>
		            <td><input id="chkListDestination_1" type="checkbox" name="chkListDestination" value="Bristol"/>
		            <label for="chkListDestination_1">Bristol</label></td>
		            <td><input id="chkListDestination_2" type="checkbox" name="chkListDestination" value="Dinard"/>
		            <label for="chkListDestination_2">Dinard</label></td>
		            <td><input id="chkListDestination_3" type="checkbox" name="chkListDestination" value="Gatwick"/>
		            <label for="chkListDestination_3">Gatwick</label></td>
		            <td><input id="chkListDestination_4" type="checkbox" name="chkListDestination" value="Guernsey"/>
		            <label for="chkListDestination_4">Guernsey</label></td>
	            </tr>
	            <tr>
		            <td><input id="chkListDestination_5" type="checkbox" name="chkListDestination" value="Jersey"/>
		            <label for="chkListDestination_5">Jersey</label></td>
		            <td><input id="chkListDestination_6" type="checkbox" name="chkListDestination" value="Manchester"/>
		            <label for="chkListDestination_6">Manchester</label></td>
		            <td><input id="chkListDestination_7" type="checkbox" name="chkListDestination" value="Southampton"/>
		            <label for="chkListDestination_7">Southampton</label></td>
		            <td><input id="chkListDestination_8" type="checkbox" name="chkListDestination" value="Stansted"/>
		            <label for="chkListDestination_8">Stansted</label></td>
		            <td><input id="chkListDestination_9" type="checkbox" name="chkListDestination" value="East Midlands"/>
		            <label for="chkListDestination_9">East Midlands(for Nottingham, Leicester & Derby)</label></td>
	            </tr>
	            <tr>
	            <td colspan=5><input id="chkListDestination_All" type="checkbox" name="chkListDestination_All" onclick="validDestinationsAll(this.checked);"/>
		            <label for="chkListDestination_All"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_21", "ALL", _stdLanguage)%></label></td>
	            </tr>	
            </table>
            </td>
		</tr>
		<tr><td colspan="2" class="groupForm" style="height: 20px">
		    <span id="spErrDestination" class="RequestStar"></span>&nbsp;</td></tr>
		<tr>
			<td colspan="2" class="groupForm"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_6", "Do you travel for:", _stdLanguage)%></STRONG></td>
		</tr>
		<tr>
			<td colspan="2" class="groupForm2" style="height: 30px">
			<table id="tbTravelfor" class="groupForm2" border="0" align ="left">
	            <tr>
		        <td><input id="rdbTravel_0" type="radio" name="rdbTravelfor" value="business" />
		            <label for="rdbTravel_0"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_7", "business", _stdLanguage)%></label></td>
		        <td><input id="rdbTravel_1" type="radio" name="rdbTravelfor" value="pleasure" />
		            <label for="rdbTravel_1"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_8", "pleasure", _stdLanguage)%></label></td>
		        <td><input id="rdbTravel_2" type="radio" name="rdbTravelfor" value="both" checked="checked" />
		        <label for="rdbTravel_2"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_9", "both", _stdLanguage)%></label></td>
	            </tr>
            </table>
            </td>
		</tr>
		<tr><td colspan="2" style="height: 20px">&nbsp;</td></tr>
		<tr>
			<td colspan="2" class="groupForm"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_10", "Do you have children:", _stdLanguage)%></STRONG></td>
		</tr>
		<tr>
			<td colspan="2" class="groupForm2" style="height: 30px">
			<table id="tbHaschild" class="groupForm2" border="0" align ="left">
			    <tr>
			    <td><input id="rdbHaschild_0" type="radio" name="rdbHaschild" value="y" />
			        <label for="rdbHaschild_0"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_11", "yes", _stdLanguage)%></label></td>
			    <td><input id="rdbHaschild_1" type="radio" name="rdbHaschild" value="n" checked="checked" />
			        <label for="rdbHaschild_1"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_12", "no", _stdLanguage)%></label></td>
	            </tr>
            </table>
            </td>
		</tr>
		<tr>
		    <td class="groupForm" style="height: 29px;"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_13", "Email:", _stdLanguage)%></STRONG></td>
		    <td class="groupForm">
		    <input id="txtEmail" runat="server" MaxLength="50"/></td>					
		</tr>			
		<tr>
		    <td class="groupForm"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_14", "Status:", _stdLanguage)%></STRONG></td>
		    <td class="groupForm">
		    <select name="ddlStatus" id="ddlStatus">
			        <option value="-1">-- <%=tikAeroB2C.Classes.Language.Value("NewsRegister_21", "ALL", _stdLanguage)%> --</option>
	                <option value="0"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_15", "Active", _stdLanguage)%></option>
	                <option value="1"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_16", "Unsubscribe", _stdLanguage)%></option>
                </select>
            </td>		
		</tr>	 
		<tr><td colspan="2" style="height: 10px"></td></tr>
		<tr>
		    <td colspan="2" class="groupForm2" style="text-align:left;">
		    <input type="checkbox" id="chkDateRegis" onclick="ShowDateRegis();"/><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_17", "Select registration date range:", _stdLanguage)%></STRONG>
		    </td>
		</tr>	   
		<tr>
			<td style="width: 120px;"></td>
			<td style="width: 700px;">
			    <div id="dvDate" style="display:none;">
			    <table><tr>
			    <td class="groupFormClear" style="width: 300px; vertical-align:top;"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_18", "Date from :", _stdLanguage)%></STRONG>
				  <%=tikAeroB2C.Classes.Calendar.getDLLCalendar("3", "return", _stdLanguage)%>
				  <div id="Cal3" style="z-index: 900; position: absolute;"/>
			    </td>
			    <td class="groupFormClear" style="width: 350px; vertical-align:top;"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_19", "Date to :", _stdLanguage)%></STRONG>
				 <%=tikAeroB2C.Classes.Calendar.getDLLCalendar("4", "return", _stdLanguage)%>
				 <div id="Cal4" style="z-index: 900; position: absolute;"/>
			    </td>
			    </tr></table>
			    </div>
			</td>
		  </tr>
		  <tr><td colspan="2" style="height: 10px"></td></tr>
		<TR>
			<td colspan="2" class="groupFormClear">&nbsp;&nbsp;&nbsp; 
            <div class="RegisterCancelButton" onclick="SearchNewsRegister('1');">
                <div class="buttonCornerLeft"></div>
                <div class="buttonContent">
                <%=tikAeroB2C.Classes.Language.Value("NewsRegister_20", "Search", _stdLanguage)%>
                </div>
                <div class="buttonCornerRight"></div>
            </div>                  
		    </TD>
		</TR>
		
		<tr>
			<td colspan="2" class="groupForm">
			<div class="clearboth"></div>
			
        <!--Search NewsRegister-->
            <div id="dvSearchResult" runat="server"/>
            <div class="clearboth"></div>					
			</td>
		</tr>
    </table>
  </div>
  
  <div id="dvSendmail" style="display:none">
  </div>
