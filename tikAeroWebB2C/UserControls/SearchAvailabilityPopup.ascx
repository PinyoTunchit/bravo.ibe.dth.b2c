﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchAvailabilityPopup.ascx.cs" Inherits="tikAeroB2C.UserControls.SearchAvailabilityPopup" %>
<div class='PopupFB'>
    <div class='FBtop'>
        <span>Wow!! Facebook with Style Airway</span>
        <div class='FBclose' onclick='ClosePassengerList();'>[X]</div>
        <div class='FBclear'></div>
    </div>
    <div class='FBmid'>
        <div class='FBdetail'>
            "The route you have selected is operated by Style Airway. You will be automatically transferred to the Style Airway website. The booking exclusively takes place on the web page of Style Airway.
            <br />
            <br />
            "Please select the number of adults, children and infants and click on “next”:
        </div>
        <div class='FBlogo'></div>
        <div class='FBclear'></div>
        <div class='FBselect'>
                <ul>
            <li class="typeleft">
			    <input id="optReturnPopup" name="optSearchPopup" type="radio" checked="checked" value="" onclick="ShowHideCalendar('optReturnPopup', 'calRenPopup2', 'lblReturnDatePopup');"/> 
                <label for="optReturnPopup">
				    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_1", "Return", _stdLanguage)%>
			    </label> 
            </li>
		
            <li class="typeright">
			    <input id="optOneWayPopup" name="optSearchPopup" type="radio" value="" onclick="ShowHideCalendar('optReturnPopup', 'calRenPopup2', 'lblReturnDatePopup');"/> 
                <label for="optOneWayPopup">
				    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_2", "One Way", _stdLanguage)%>
			    </label>
            </li>
        </ul>
        <div class="clearboth"></div>
        <ul>
            <li class="FromTo">
			    <label for="ddlDate_3">
				    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_5", "Deparature Date", _stdLanguage)%>
			    </label>
            </li>

            <li id="CalenderBase" class="TabDate">
                <%= tikAeroB2C.Classes.CalendarHelper.GetCalOutputControl("3", false, false)%>                  
                <input type="button" title="" class="calendar_icon" id="calendar3" style="display:inline" disabled="disabled" onclick="TriggerCalendar3();" />  
            <div id="divCa3"></div>
            </li> 
            <li id='Cal3' class="CAL"></li>
            <div class="clearboth"></div>
            <li id="lblReturnDatePopup" class="FromTo">
			    <label for="ddlDate_2">
				    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_6", "Return Date", _stdLanguage)%>
			    </label>
            </li>

            <li id="calRenPopup2" class="TabDate">
                <%= tikAeroB2C.Classes.CalendarHelper.GetCalOutputControl("4", false, false)%>                  
                <input type="button" title="" class="calendar_icon" id="calendar4" style="display:inline" disabled="disabled" onclick="TriggerCalendar4();" />  
                <div id="divCa4"></div>
            </li>
            <li id='Cal4' style='POSITION: absolute;Z-INDEX: 1001;' class="CAL"></li>
        </ul>
            <div class='FBleft1'>adults:</div>
            <div class='FBright'>
                <select id='sePopAdult' disabled="disabled">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                </select>
            </div>
            <div class='FBclear'></div>
            <div class='FBleft2'>children:</div><div class='FBinfo'></div>
            <div class='FBright'>
                <select id='sePopChild' disabled="disabled">
                    <option>0</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                </select>
            </div>
            <div class='FBclear'></div>
            <div class='FBleft2'>infants:</div><div class='FBinfo'></div>
            <div class='FBright'>
                <select id='sePopInfant' disabled="disabled">
                    <option>0</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                </select>
            </div>
            <div class='FBclear'></div>
        </div>
        <ul class='BTNfb' onclick='PopupGetAvai();'>
        <a title="Next" class="defaultbutton">
            <span>Next</span>
        </a>
        </ul>
        <div class='FBclear'></div>
    </div>
    <div class='FBclear'></div>
</div>