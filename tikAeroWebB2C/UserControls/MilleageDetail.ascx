<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MilleageDetail.ascx.cs" Inherits="tikAeroB2C.UserControls.MilleageDetail" %>
<div class="BoxGray">
  <div class="BoxGrayTop"></div>
    <div id="dvFFPDetail" runat="server"/>
  <div class="BoxGrayEnd"></div>
</div>
<div class="clearboth"></div>
	<div class="BoxGray">
	<div class="BoxGrayTop"></div>
	<div class="BoxGrayContent">
	  	<div class="BoxGrayContentHeader">
	  	    <%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_24", "My Mileage Transactions", _stdLanguage)%>
	  	</div>
	  	<div id="dvPaging"/> 
		<div id="dvFFPList" runat="server"/>
    </div>
    <div class="BoxGrayEnd"></div>	
</div>
<div class="clearboth"></div>
