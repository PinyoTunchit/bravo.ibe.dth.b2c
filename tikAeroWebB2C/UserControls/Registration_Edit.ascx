<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registration_Edit.ascx.cs" Inherits="tikAeroB2C.UserControls.Registration_Edit" %>
<div class="Content">
<div class="BoxGray">
  <div class="BoxGrayTop"></div>
  <div class="BoxGrayContent">
  	<div class="BoxGrayContentHeader"><%=tikAeroB2C.Classes.Language.Value("Registration_7", "User Profile", _stdLanguage)%></div>
  	<div class="clearboth"></div>
	<p><%=tikAeroB2C.Classes.Language.Value("Registration_8", "You may update your address", _stdLanguage)%>
	</p></div>
  <div class="BoxGrayEnd"></div>
</div>
	<div class="clearboth"></div>
    
<div class="BoxGray">
  <div class="BoxGrayTop"></div>
  <div class="BoxGrayContent">
<ul> 
<li class="Left"></li> 
       <li class="Right">
			<div id="bttSave" class="GrayButon">
				<div class="GrayButtonLeft"></div>
				<div class="GrayButtonContent" onclick ="ClientProfileSave('ctl00_',0);"><%=tikAeroB2C.Classes.Language.Value("Registration_50", "Save", _stdLanguage)%></div>
			<div class="GrayButtonShowAll"></div>
				</div>
				<div id="bttEdit" class="GrayButon">
				<div class="GrayButtonLeft"></div>
				<div class="GrayButtonContent" onclick ="enableField('ctl00_');"><%=tikAeroB2C.Classes.Language.Value("Registration_51", "Edit", _stdLanguage)%></div>
			<div class="GrayButtonShowAll"></div>
				</div>            	
       </li>
   </ul>    
  	<div class="BoxGrayContentHeader"><%=tikAeroB2C.Classes.Language.Value("Registration_5", "Contact Details", _stdLanguage)%> </div>
    	<ul>
            <div id="ClientProfileInfo">
                <li class="Left">
           	      <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_27", "Title", _stdLanguage)%> </div>
            	    <div class="Detail">
                	    <select id="ddlTitle" runat="server"> 
                            <option value="" class="watermarkOn"></option>
                        </select> 
                    </div>
          	    </li>
            	    <div class="clearboth"></div>
        	
                <li class="Left">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_28", "Last Name", _stdLanguage)%><span id="spLastName" class="RequestStar">*</span></div>
            	    <div class="Detail"><input type="text" ID="txtLastName" onkeypress="return CheckCharacter();" runat="server" /></div>
                </li>
                <li class="Right">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_29", "First Name", _stdLanguage)%><span id="spFirstName" class="RequestStar">*</span></div>
                    <div class="Detail"><input type="text" id="txtFirstName" onkeypress="return CheckCharacter();" runat="server" /></div>
                </li>
            	    <div class="clearboth"></div>
				    <li class="Left">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_38", "Mobile Phone", _stdLanguage)%><span id="spMobilePhone" class="RequestStar">*</span></div>
                    <div class="Detail"><input type="text" ID="txtMobilePhone" runat="server" /></div>
                </li>
                <li class="Right">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_39", "Home Phone", _stdLanguage)%> </div>
            	    <div class="Detail"><input type="text" ID="txtHomePhone" runat="server" /></div>
                </li>
            	    <div class="clearboth"></div>
				    <li class="Left">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_40", "Business Phone", _stdLanguage)%> </div>
                    <div class="Detail"><input type="text" ID="txtBusinessPhone" runat="server" /></div>
                </li>
                <li class="Right">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_41", "Email", _stdLanguage)%><span id="spEmail" class="RequestStar">*</span></div>
            	    <div class="Detail"><input type="text" ID="txtEmail" runat="server" /></div>
                </li>
                <li class="Right">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_54", "Mobile Email", _stdLanguage)%><span id="Span1" class="RequestStar">*</span></div>
            	    <div class="Detail"><input type="text" ID="txtMobileEmail" runat="server" /></div>
                </li>
            	    <div class="clearboth"></div>
        	    <li class="Left">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_42", "Language", _stdLanguage)%> </div>
            	    <div class="Detail">
             	        <select id="optLanguage" runat="server"> 
                            <option value="" class="watermarkOn"></option>
                        </select>
            	    </div>
        	    </li>
                <li class="Right">
            	    <div class="Title"></div>
            	    <div class="Detail">	
             	    </div>
                </li>
                <div class="clearboth"></div>
			    <li class="Left">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_43", "Address 1", _stdLanguage)%><span id="spAddress1" class="RequestStar">*</span></div>
            	    <div class="Detail">
            	      <input type="text" ID="txtAddress1" runat="server" />
            	    </div>
			    </li>
                <li class="Right">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_44", "Address 2 (Optional)", _stdLanguage)%> </div>
            	    <div class="Detail">
            	      <input type="text" ID="txtAddressline2" runat="server" />	
             	    </div>
                </li>
                <div class="clearboth"></div>
			    <li class="Left">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_45", "Zip Code", _stdLanguage)%><span id="spZipCode" class="RequestStar">*</span></div>
            	    <div class="Detail">
            	       <input type="text" ID="txtZipCode" runat="server" />
            	    </div>
			    </li>
                <li class="Right">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_46", "Township", _stdLanguage)%><span id="spTownship" class="RequestStar">*</span></div>
            	    <div class="Detail">
            	      <input type="text" ID="txtcity" runat="server" />
             	    </div>
                </li>
                <div class="clearboth"></div>
                <li class="Right">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_55", "State", _stdLanguage)%><span id="Span2" class="RequestStar">*</span></div>
            	    <div class="Detail">
            	      <input type="text" ID="txtState" runat="server" maxlength="60"/>
             	    </div>
                </li>
                <div class="clearboth"></div>
			    <li class="Left">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_47", "Country", _stdLanguage)%> </div>
            	    <div class="Detail">
            	     <select id="ddlCountry" runat ="server" >
                        <option value="" class="watermarkOn"></option>
                     </select>
            	    </div>
			    </li>
                <li class="Right">
            	    <div class="Title"></div>
            	    <div class="Detail"></div>
                </li>
            </div>
            <div class="clearboth"></div>

            <div id="dvRegPassword">
            <input id="hdCurrentPassword" type="hidden" runat="server" />
                <li class="Left">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_13", "Password", _stdLanguage)%><span id="spPassword" class="RequestStar">*</span></div>
            	    <div class="Detail">
            	     <input type="Password" ID="txtPassword" runat="server" TextMode="Password" />
            	    </div>
			    </li>
                <li class="Right">
            	    <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_48", "Confirm Password", _stdLanguage)%><span id="spConfirmPassword" class="RequestStar">*</span></div>
            	    <div class="Detail">
            	    <input type="Password" ID="txtReconfirmPassword" runat="server"  TextMode="Password"  />
             	    </div>
                </li>
                <div class="clearboth"></div>
            </div>
       </ul>
    
  </div>
  <div class="BoxGrayEnd"></div>
</div>
<div class="clearboth"></div>
<div id="dvPassengerProfileWrapper">
    <div class="ErrorList">  <asp:Label ID="LabError" runat="server"></asp:Label></div>
    	<div class="Right" >
      
      <div class="GrayAddButon" id="bttPassengerAdd">
        <div id="bttPassengerAdds"  class="GrayButtonLeft"></div>
          <div class="GrayButtonContent"  onclick="ControlBtt();"   >
            <%=tikAeroB2C.Classes.Language.Value("Registration_10", "Add New Passenger", _stdLanguage)%>
          </div>
        <div class="GrayButtonShowAll"></div>
      </div>
    </div>
	<div class="clearboth"></div>
<div class="BoxGray" id="dvShowpassenger">
  <div class="BoxGrayTop"></div>
  	<div id="dvPassengerAdd" runat="server" class="BoxGrayContent"></div>   
  <div class="BoxGrayEnd"></div>
  </div>
<div class="clearboth"></div>
<div id="dvNewPassenger" class="BoxGray" style="display:none;">
  <div class="BoxGrayTop"></div>
  <div   class="BoxGrayContent">
  	<div class="BoxGrayContentHeader">
        <%=tikAeroB2C.Classes.Language.Value("Registration_52", "Passenger", _stdLanguage)%>
        <span id="spnPaxNo"></span>
    </div>
    	<ul>
          	<li class="Left">
           	  <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_27", "Title", _stdLanguage)%> </div>
            	<div class="Detail">
                	<select id="ddlPsgTitle" runat="server">
                        <option value="" class="watermarkOn"></option>
                    </select> 
                </div>
          	</li>
          	<div class="clearboth"></div>
          	<li class="Left">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_9", "Passenger Role", _stdLanguage)%> </div>
             <div class="Detail">
                   	<select id="ddlPassengerRole" runat ="server">
                        <option value="" class="watermarkOn"></option>
                    </select> 
                </div>
          	</li>
          	<li class="Right">
           	  <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_3", "Passenger Type", _stdLanguage)%> </div>
            	<div class="Detail">
            	    <select id="ddlPassengerType" runat ="server"> 
                        <option value="" class="watermarkOn"></option>
                        <option value="ADULT">Adult</option>
                        <option value="CHD">Child</option>
                        <option value="INF">Infant</option>
                        <option value="STAFF">Staff</option>
                    </select>
            	</div>
          	</li>
            	<div class="clearboth"></div>
        	<li class="Left">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_28", "Last Name", _stdLanguage)%><span id="spPsgLastName" class="RequestStar">*</span></div>
                <div class="Detail"><input type="text" ID="txtPsgLastName" onkeypress="return CheckCharacter();" runat="server" /></div>
            </li>
            <li class="Right">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_29", "First Name", _stdLanguage)%><span id="spPsgFirstName" class="RequestStar">*</span></div>
            	<div class="Detail"><input type="text" ID="txtPsgFirstName" onkeypress="return CheckCharacter();" runat="server" /></div>
            </li>
            	<div class="clearboth"></div>
            <li class="Left">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_30", "Date of Birth", _stdLanguage)%><span id="spPsgDateofBirth" class="RequestStar">*</span></div>
                <div class="Detail"><input type="text" ID="TxtPsgDateofBirth" runat="server" /></div>
          	</li>
          	<li class="Right">
           	  <div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_31", "Nationality", _stdLanguage)%></div>
            	<div class="Detail">
            	<select id="optNationality" runat="server">
                    <option value="" class="watermarkOn"></option>
                </select>
            	</div>
          	</li>
            	<div class="clearboth"></div>
                <div class="ContactDetail">
				<label for="ctl00_opIssueCountry" class="topname">
					<%=tikAeroB2C.Classes.Language.Value("Registration_34", "Issue Country", _stdLanguage)%>
				</label>
													
				<div class="clear-all"></div>
								
				<div class="InformationInput">
					<select id="Select1" runat="server" class="role">
				        <option value="" class="watermarkOn"></option>
				    </select>
				</div>
			</div>
        	<li class="Left">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_32", "Document Type", _stdLanguage)%> </div>
                <div class="Detail">   
                    <select id="optDocumentType" runat="server"> 
                        <option value="" class="watermarkOn"></option>
                    </select>  
                 </div>
            </li>
            <li class="Right">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_33", "Document Number", _stdLanguage)%><span id="spPsgDocumentNumber" class="RequestStar">*</span></div>
            	<div class="Detail"><input type="text" ID="txtPsgDocumentNumber" runat="server" /></div>
            </li>
            	<div class="clearboth"></div>
             <li class="Left">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_34", "Place of Issue", _stdLanguage)%><span id="spPsgPlaceOfIssue" class="RequestStar">*</span></div>
                <div class="Detail"><input type="text" ID="txtPsgPlaceOfIssue" runat="server" /></div>
            </li>
            <li class="Right">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_35", "Place of Birth", _stdLanguage)%><span id="spPsgPlaceOfBirth" class="RequestStar">*</span></div>
            	<div class="Detail"><input type="text" ID="txtPsgPlaceOfBirth" runat="server" /></div>
            </li>
            	<div class="clearboth"></div>
			<li class="Left">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_36", "Issue Date", _stdLanguage)%><span id="spPsgIssueDate" class="RequestStar">*</span></div>
                <div class="Detail"><input type="text" ID="txtPsgIssueDate" runat="server" /></div>
            </li>
            <li class="Right">
            	<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_37", "Expiry Date", _stdLanguage)%><span id="spPsgExpiryDate" class="RequestStar">*</span></div>
            	<div class="Detail"><input type="text" ID="txtPsgExpiryDate" runat="server" /></div>
            </li>
            	<div class="clearboth"></div>
				<li class="Right">
				<div class="GrayButon">
				<div class="GrayButtonLeft"></div>
				<div class="GrayButtonContent" onclick="NewPassenger('ctl00_');"><%=tikAeroB2C.Classes.Language.Value("Registration_50", "Save", _stdLanguage)%></div>
			<div class="GrayButtonAdd"></div>
				</div>
            	</li>
        <li class="Left">
			<div class="GrayButon">
				<div class="GrayButtonLeft"></div>
				<div class="GrayButtonContent" onclick ="LoadRegistration_Edit();"><%=tikAeroB2C.Classes.Language.Value("Registration_53", "Cancel", _stdLanguage)%></div>
			<div class="GrayButtonShowAll"></div>
				</div>           	
            </li>
       </ul>
        <select id="opIssueCountry" runat="server" style="display:none">
            <option value="" class="watermarkOn"></option>
        </select>
       <br />
       
  </div>
  <div class="BoxGrayEnd"></div>
<div class="clearboth"></div>
    <div class="ErrorList">  <asp:Label ID="ErrorNewPassenger" runat="server"></asp:Label></div>
</div>
</div>



