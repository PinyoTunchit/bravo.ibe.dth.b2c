<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsRegisterDetail.ascx.cs" Inherits="tikAeroB2C.UserControls.NewsRegisterDetail" %>
   <div id="dvInput">
   <table width ="830" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="RegisterB2C">
				<tr>					
		            <td class="headerWithSub" style="width: 100px;"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_22", "Register", _stdLanguage)%></td>
		            <td class="headerWithSub" style="text-align:right; width: 730px;">
                        <div style="color: #FF0000;" onclick="ShowUnsubscribe()">
                        <div class="buttonUnsubscribe">&nbsp;&nbsp;<%=tikAeroB2C.Classes.Language.Value("NewsRegister_23", "How to Unsubscribe", _stdLanguage)%>&nbsp;&nbsp;</div></div>
                    </td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" class="groupForm">
						<%=tikAeroB2C.Classes.Language.Value("NewsRegister_24", "Sign up now to receive the latest news on low fares, travel offers and promotions.", _stdLanguage)%>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="groupForm">
						<%=tikAeroB2C.Classes.Language.Value("NewsRegister_25", "It only takes a minute.", _stdLanguage)%>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td class="groupForm" ><%=tikAeroB2C.Classes.Language.Value("NewsRegister_26", "name:", _stdLanguage)%></td>
					<td class="groupForm" ><input id="txtName" runat="server" MaxLength="85"/>
					<span id="spErrName" class="RequestStar">*</span></td>
				</tr>
				<tr>
					<td class="groupForm" ><%=tikAeroB2C.Classes.Language.Value("NewsRegister_13", "email:", _stdLanguage)%></td>
					<td class="groupForm" ><input id="txtEmail" runat="server" MaxLength="50"/>
					<span id="spErrEmail" class="RequestStar">*</span></td>
				</tr>
				<tr>
					<td class="groupForm" ><%=tikAeroB2C.Classes.Language.Value("NewsRegister_27", "post code:", _stdLanguage)%></td>
					<td class="groupForm" ><input id="txtPostcode" runat="server" MaxLength="10"/>
					<span id="spErrPostcode" class="RequestStar">*</span></td>
				</tr>
				<tr>
					<td class="groupForm" ><%=tikAeroB2C.Classes.Language.Value("NewsRegister_3", "country:", _stdLanguage)%></td>
					<td class="groupForm" >
					<select id="ddlCountry" runat="server"></select>
					<select id="ddlCountryALL" runat="server" visible="false"></select>
					<span id="spErrCountry" class="RequestStar">*</span></td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<TR>
					<TD class="groupForm" colspan="2"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_28", "Tick your preferred departure point(s):", _stdLanguage)%></STRONG></TD>
				</TR>
				<tr>
					<td class="groupForm2" colspan="2" style="height: 52px;">
	<table id="tbListDeparture" class="groupForm2" border="0" align ="left">
	<tr>
		<td><input id="chkListDeparture_0" type="checkbox" name="chkListDeparture" value="Alderney"/>
		<label for="chkListDeparture_0">Alderney</label></td>
		<td><input id="chkListDeparture_1" type="checkbox" name="chkListDeparture" value="Bristol"/>
		<label for="chkListDeparture_1">Bristol</label></td>
		<td><input id="chkListDeparture_2" type="checkbox" name="chkListDeparture" value="Dinard"/>
		<label for="chkListDeparture_2">Dinard</label></td>
		<td><input id="chkListDeparture_3" type="checkbox" name="chkListDeparture" value="Gatwick"/>
		<label for="chkListDeparture_3">Gatwick</label></td>
		<td><input id="chkListDeparture_4" type="checkbox" name="chkListDeparture" value="Guernsey"/>
		<label for="chkListDeparture_4">Guernsey</label></td>		
	</tr>
	<tr>
		<td><input id="chkListDeparture_5" type="checkbox" name="chkListDeparture" value="Jersey"/>
		<label for="chkListDeparture_5">Jersey</label></td>
		<td><input id="chkListDeparture_6" type="checkbox" name="chkListDeparture" value="Manchester"/>
		<label for="chkListDeparture_6">Manchester</label></td>
		<td><input id="chkListDeparture_7" type="checkbox" name="chkListDeparture" value="Southampton"/>
		<label for="chkListDeparture_7">Southampton</label></td>
		<td><input id="chkListDeparture_8" type="checkbox" name="chkListDeparture" value="Stansted"/>
		<label for="chkListDeparture_8">Stansted</label></td>
		<td><input id="chkListDeparture_9" type="checkbox" name="chkListDeparture" value="East Midlands"/>
		<label for="chkListDeparture_9">East Midlands(for Nottingham, Leicester & Derby)</label></td>		
	</tr>
	
    </table>
            </td>
				</tr>
				<tr><td class="groupForm" colspan="2" style="height: 29px">
				    <span id="spErrDeparture" class="RequestStar"></span>&nbsp;</td></tr>
				<TR>
					<TD class="groupForm" colspan="2"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_29", "Tick the destinations you are interested in receiving news and offers about: ", _stdLanguage)%></STRONG></TD>
				</TR>
				<tr>
				<td class="groupForm2" colspan="2" >
	<table id="tbListDestination" class="groupForm2" border="0" align ="left">
	<tr>
		<td><input id="chkListDestination_0" type="checkbox" name="chkListDestination" value="Alderney"/>
		<label for="chkListDestination_0">Alderney</label></td>
		<td><input id="chkListDestination_1" type="checkbox" name="chkListDestination" value="Bristol"/>
		<label for="chkListDestination_1">Bristol</label></td>
		<td><input id="chkListDestination_2" type="checkbox" name="chkListDestination" value="Dinard"/>
		<label for="chkListDestination_2">Dinard</label></td>
		<td><input id="chkListDestination_3" type="checkbox" name="chkListDestination" value="Gatwick"/>
		<label for="chkListDestination_3">Gatwick</label></td>
		<td><input id="chkListDestination_4" type="checkbox" name="chkListDestination" value="Guernsey"/>
		<label for="chkListDestination_4">Guernsey</label></td>
	</tr>
	<tr>
		<td><input id="chkListDestination_5" type="checkbox" name="chkListDestination" value="Jersey"/>
		<label for="chkListDestination_5">Jersey</label></td>
		<td><input id="chkListDestination_6" type="checkbox" name="chkListDestination" value="Manchester"/>
		<label for="chkListDestination_6">Manchester</label></td>
		<td><input id="chkListDestination_7" type="checkbox" name="chkListDestination" value="Southampton"/>
		<label for="chkListDestination_7">Southampton</label></td>
		<td><input id="chkListDestination_8" type="checkbox" name="chkListDestination" value="Stansted"/>
		<label for="chkListDestination_8">Stansted</label></td>
		<td><input id="chkListDestination_9" type="checkbox" name="chkListDestination" value="East Midlands"/>
		<label for="chkListDestination_9">East Midlands(for Nottingham, Leicester & Derby)</label></td>
	</tr>
	<tr>
	<td colspan=5><input class ="redborder" id="chkListDestination_All" type="checkbox" name="chkListDestination_All" onclick="validDestinationsAll(this.checked);"/>
		<label for="chkListDestination_All"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_21", "ALL", _stdLanguage)%></label></td>
	</tr>	
    </table>
                </td>
				</tr>
				<tr><td class="groupForm"colspan="2" style="height: 25px">
				    <span id="spErrDestination" class="RequestStar"></span>&nbsp;</td></tr>
				<tr>
					<td class="groupForm" colspan="2" style="width: 529px"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_6", "Do you travel for:", _stdLanguage)%></STRONG></td>
				</tr>
				<tr>
					<td class="groupForm2" colspan="2">
					<table id="tbTravelfor" class="groupForm2" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;">
	                    <tr>
		                <td><input id="rdbTravel_0" type="radio" name="rdbTravelfor" value="business" />
		                    <label for="rdbTravel_0"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_7", "business", _stdLanguage)%></label></td>
		                <td><input id="rdbTravel_1" type="radio" name="rdbTravelfor" value="pleasure" />
		                    <label for="rdbTravel_1"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_8", "pleasure", _stdLanguage)%></label></td>
		                <td><input id="rdbTravel_2" type="radio" name="rdbTravelfor" value="both" checked="checked" />
		                <label for="rdbTravel_2"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_9", "both", _stdLanguage)%></label></td>
	                    </tr>
                    </table>
                    </td>
				</tr>
				<tr><td colspan="2" style="height: 19px"></td></tr>
				<tr>
					<td class="groupForm" colspan="2" ><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_10", "Do you have children:", _stdLanguage)%></STRONG></td>
				</tr>
				<tr>
					<td class="groupForm2" colspan="2">
					<table id="tbHaschild" class="groupForm2" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;">
					    <tr>
					    <td><input id="rdbHaschild_0" type="radio" name="rdbHaschild" value="y" />
					        <label for="rdbHaschild_0"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_11", "yes", _stdLanguage)%></label></td>
					    <td><input id="rdbHaschild_1" type="radio" name="rdbHaschild" value="n" checked="checked" />
					        <label for="rdbHaschild_1"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_12", "no", _stdLanguage)%></label></td>
	                    </tr>
                    </table>
                    </td>
			    </tr>
				<TR>
					<TD class="groupFormClear" colspan="2">&nbsp;&nbsp;&nbsp; 
                    <div class="RegisterCancelButton" onclick="SaveNewsRegisterDetail();">
                        <div class="buttonCornerLeft"></div>
                        <div class="buttonContent">
                            <%=tikAeroB2C.Classes.Language.Value("NewsRegister_30", "Submit", _stdLanguage)%>
                        </div>
                        <div class="buttonCornerRight"></div>
                    </div> 
      
                    <div class="RegisterAdminButton" onclick="GetNewsAdmin()" style="display:none">
                        <div class="buttonCornerLeft"></div>
                        <div class="buttonContent"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_31", "ADMIN", _stdLanguage)%></div>
                        <div class="buttonCornerRight"></div>
                    </div>
				    </TD>
			    </TR>
			    <tr><td colspan="2" >&nbsp;</td></tr>
	</table>
	</div>
	
	<div id="dvResultComplete" style="display:none">
	  <table width ="830" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="RegisterB2C">
			<tr>
			<td colSpan="3" class="groupForm" style="height: 23px"><div style="FONT-SIZE:20px;COLOR:#000066"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_32", "Thank you for registering", _stdLanguage)%></STRONG></div>
			</td>
			</tr>
			<tr>
				<td colSpan="3" class="groupForm" style="height: 33px">&nbsp;</td>
			</tr>
			<TR>
				<TD colSpan="3" class="groupForm" style="height: 29px">
				    <%=tikAeroB2C.Classes.Language.Value("NewsRegister_33", "We will be in touch soon with news of lowest fare prices, great travel offers and special price promotions.", _stdLanguage)%>
			    </TD>
			</TR>
			<tr>
				<td colSpan="3" class="groupForm" style="height: 29px">
				    <%=tikAeroB2C.Classes.Language.Value("NewsRegister_34", "You can book flights online at", _stdLanguage)%> <a href="http://www.aurigny.com">www.aurigny.com</a> <%=tikAeroB2C.Classes.Language.Value("NewsRegister_35", "24 hours a day, 7 days a week.", _stdLanguage)%>&nbsp;
				</td>
			</tr>
			<TR>
				<TD class="groupForm" colSpan="3" style="height: 33px">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="groupFormClear" colSpan="3" align="center">
					<div class="RegisterCancelButton" onclick="loadHome();">
                    <div class="buttonCornerLeft"></div>
                    <div class="buttonContent">
                        <%=tikAeroB2C.Classes.Language.Value("NewsRegister_36", "Back to Homepage", _stdLanguage)%>
                    </div>
                    <div class="buttonCornerRight"></div>
                    </div>
				</TD>
			</TR>
	  </table>
	</div>
	
   <div id="dvUnsubscribe" style="display:none">
   <table width ="830" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="RegisterB2C">
				<tr>
				    <td colspan="2" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_16", "Unsubscribe", _stdLanguage)%></td>               
				</tr>
				<tr><td colspan="2" style="height: 19px"></td></tr>
				<tr>
					<td colspan="2" class="groupForm" style="height: 60px">
					    <%=tikAeroB2C.Classes.Language.Value("NewsRegister_37", "If you do not wish to receive our news on low fares, travel offers and promotions.<br/><br/>Please provide us your registered email. Thank you.", _stdLanguage)%>
					</td>
				</tr>
				<tr><td colspan="2" style="height: 25px">&nbsp;</td></tr>
				<tr>
					<td class="groupForm" style="width: 99px; height: 38px;"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_13", "email:", _stdLanguage)%></td>
					<td class="groupForm" style="height: 38px; width: 701px;">
					<input id="txtEmailUnsubscribe" runat="server" MaxLength="50"/>
					<span id="spErrEmailUnsubscribe" class="RequestStar">*</span></td>
				</tr>	
				<tr><td colspan="2" style="height: 29px">&nbsp;</td></tr>
				<TR>
					<TD class="groupFormClear" colspan="2">&nbsp;&nbsp;&nbsp; 
                    <div class="RegisterCancelButton" onclick="SaveUnsubscribe();">
                        <div class="buttonCornerLeft"></div>
                        <div class="buttonContent">
                            <%=tikAeroB2C.Classes.Language.Value("NewsRegister_30", "Submit", _stdLanguage)%>
                        </div>
                        <div class="buttonCornerRight"></div>
                    </div> 
				    </TD>
			    </TR>
			    <tr><td colspan="2" >&nbsp;</td></tr>			    
    </table>
    </div>
    <div id="dvResultUnsubscribe" style="display:none">
	  <table width ="830" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="RegisterB2C">
			<tr>
			<td colSpan="3" class="groupForm" style="height: 23px"><div style="FONT-SIZE:20px;COLOR:#000066"><STRONG><%=tikAeroB2C.Classes.Language.Value("NewsRegister_38", "Thank you", _stdLanguage)%></STRONG></div>
			</td>
			</tr>
			<tr>
				<td colSpan="3" class="groupForm" style="height: 33px">&nbsp;</td>
			</tr>
			<TR>
				<TD colSpan="3" class="groupForm" style="height: 29px"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_39", "Once you are interesting to receive news of lowest fare prices, great travel offers and special price promotions.", _stdLanguage)%></TD>
			</TR>
			<tr>
				<td colSpan="3" class="groupForm" style="height: 29px"><%=tikAeroB2C.Classes.Language.Value("NewsRegister_40", "You can register again at", _stdLanguage)%><a href="http://www.aurigny.com">
						www.aurigny.com</a> <%=tikAeroB2C.Classes.Language.Value("NewsRegister_35", "24 hours a day, 7 days a week.", _stdLanguage)%>&nbsp;</td>
			</tr>
			<TR>
				<TD class="groupForm" colSpan="3" style="height: 33px">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="groupFormClear" colSpan="3" align="center">
					<div class="RegisterCancelButton" onclick="loadHome();">
                    <div class="buttonCornerLeft"></div>
                    <div class="buttonContent">
                        <%=tikAeroB2C.Classes.Language.Value("NewsRegister_36", "Back to Homepage", _stdLanguage)%>
                    </div>
                    <div class="buttonCornerRight"></div>
                    </div>
				</TD>
			</TR>
	  </table>
	</div>
	<div id="dvPaxError" class="NBStep4"></div>
			<span id="submitXML" class="RequestStar"></span>
			<input id="txtIPaddress" runat="server" type="hidden">
			<input id="txtBrowser" runat="server" type="hidden">