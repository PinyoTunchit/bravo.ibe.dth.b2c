﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BonVoyageBreak.ascx.cs" Inherits="tikAeroB2C.UserControls.BonVoyageBreak" %>
<iframe  style="height:0px; z-index:666;position:absolute; display: none;"  id='fdCal3'  src='' class='frmcls'></iframe>
<iframe  style="height:0px; z-index:555;position:absolute; display: none;"  id='fdCal4'  src='' class='frmcls'></iframe>
  <div id="dvBonVoyageBreak">
  <table width ="830" border="0" cellspacing="0" cellpadding="0" class="base">
  <tr>
    <td colspan="2" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_36", "BON VOYAGE BREAK REQUEST", _stdLanguage)%></td>
  </tr>
  <tr>
	<td width="240">&nbsp;</td>
	<td width="590">&nbsp;</td>
  </tr>
  <tr>  	
    <td colspan="2" class="groupForm">
		<%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_1", "Please provide information in the following form and we will get back to you shortly. Thank you for your enquiry.", _stdLanguage)%>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_2", "Title", _stdLanguage)%>:</td>
    <td class="groupForm">
        <select id="ddlTitle" runat="server"> </select>  
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_3", "Lead Name", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="txtLeadName" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_6", "Telephone No", _stdLanguage)%>.:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="txtTelephone" runat="server"></>
	</td>
  </tr> 
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_8", "Email", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text" id="txtEmailAddress" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_9", "Confirm Email", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="txtEmailAddressReEnter" runat="server"></>
	</td>
  </tr>
  <tr>
    <td colspan="2" height="10"></td>
  </tr>
  <tr>
    <td colspan="2" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_1_1", "Quote & Availability / New Booking", _stdLanguage)%></td>
  </tr>
  <tr>
    <td colspan="2" height="10"></td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_10", "Travelling from", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<select id="ddlTravelingFrom">
	        <option value="Alderney">Alderney</option>
	        <option value="Jersey">Jersey</option>
	        <option value="Guernsey">Guernsey</option>
	        </select>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_11", "Destination", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<select id="ddlDestination" onchange="SyncSelectWithValueHotel(this,'ddlHotel1','ddlHotel2')">
	        <option selected="selected" value="Guernsey">Guernsey</option>
	        <option value="Alderney">Alderney</option>
	        <option value="Brittany">Brittany</option>
	        <option value="Jersey">Jersey</option> 
        </select>

	</td>
  </tr>
  <tr>
	<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_12", "Hotel: 1st Choice", _stdLanguage)%><font color="#FF0000">*</font></td>
	<td class="groupForm">
			<select id="ddlHotel1">
	            <option value="St Pierre Park Hotel">St Pierre Park Hotel</option>
	            <option value="The Old Government House Hotel &amp; Spa">The Old Government House Hotel &amp; Spa</option>
	            <option value="De Havelet Hotel">De Havelet Hotel</option>
	            <option value="Duke of Richmond">Duke of Richmond</option>
	            <option value="Hougue du Pommier Hotel">Hougue du Pommier Hotel</option>
	            <option value="La Villette Hotel">La Villette Hotel</option>
	            <option value="Les Rocquettes Hotel">Les Rocquettes Hotel</option>
	            <option value="Moore's Central Hotel">Moore's Central Hotel</option>
	            <option value="Peninsula Hotel">Peninsula Hotel</option>
	            <option value="Duke of Normandie Hotel">Duke of Normandie Hotel</option>
	            <option value="Le Chêne Hotel"> Le Ch&#234;ne Hotel</option> 
            </select>
			<span class="groupFormResult"></span>
		</td>
	</tr>
	<tr>
		<td class="groupForm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_13", "2nd Choice", _stdLanguage)%></td>
		<td class="groupForm">
			<select id="ddlHotel2">
	            <option value="St Pierre Park Hotel">St Pierre Park Hotel</option>
	            <option value="The Old Government House Hotel &amp; Spa">The Old Government House Hotel &amp; Spa</option>
	            <option value="De Havelet Hotel">De Havelet Hotel</option>
	            <option value="Duke of Richmond">Duke of Richmond</option>
	            <option value="Hougue du Pommier Hotel">Hougue du Pommier Hotel</option>
	            <option value="La Villette Hotel">La Villette Hotel</option>
	            <option value="Les Rocquettes Hotel">Les Rocquettes Hotel</option>
	            <option value="Moore's Central Hotel">Moore's Central Hotel</option>
	            <option value="Peninsula Hotel">Peninsula Hotel</option>
	            <option value="Duke of Normandie Hotel">Duke of Normandie Hotel</option>
	            <option value="Le Chêne Hotel">Le Ch&#234;ne Hotel</option> 
            </select>
			<span class="groupFormResult"></span>
		</td>
	</tr>
  <tr>
    <td class="groupForm" style="height: 22px"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_14", "Type of Room", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm" style="height: 22px">
		<select id="ddlRoomType">
	        <option value="Single">Single</option>
	        <option value="Double (Sole Occ)">Double (Sole Occ)</option>
	        <option value="Family Suite">Family Suite</option>
	        <option value="Standard Double">Standard Double</option>
	        <option value="Standard Twin">Standard Twin</option>
	        <option value="Superior/Deluxs">Superior/Deluxe</option>
        </select>
	</td>
  </tr>  
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_15", "Options", _stdLanguage)%>:</td>
    <td class="groupForm">
		<select id="ddlOptions" onchange="SetOptions2(this)">
	        <option value="B&amp;B">B&amp;B</option>
	        <option value="Half Board">Half Board</option> 
        </select>

	</td>
  </tr>  
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_16", "Number of Adults", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text" maxlength="2" id="txtAdultNumber" runat="server"/>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_17", "Number of Children under 12 Years", _stdLanguage)%>:</td>
    <td class="groupForm">
		<input type="text" id="txtChildrenNumber" runat="server"/>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_18", "Please provide ages", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text" id="txtChildrenAge" runat="server"/>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_19", "Number of Nights", _stdLanguage)%>:</td>
    <td class="groupForm">
        <input type="text" id="txtNightNumber" runat="server"/>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_20", "Date of Arrival", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupFormClear">
		<%=tikAeroB2C.Classes.Calendar.getDLLCalendar("3", "outward", _stdLanguage)%>
		<div id="Cal3" style="z-index: 900; position: absolute;"/>
	</td>
  </tr>
  
   <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_21", "Preferred Flight times", _stdLanguage)%>:</td>
    <td class="groupForm">
		<select id="cmbPreferedArrivalTime" > 
         <option value="Anytime"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_32", "Anytime", _stdLanguage)%></option>
         <option value="AM"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_33", "AM", _stdLanguage)%></option>
         <option value="PM"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_34", "PM", _stdLanguage)%></option>
         </select>
	</td>
  </tr>
    <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_22", "Date of Departure", _stdLanguage)%>:</td>
    <td class="groupFormClear">
		<%=tikAeroB2C.Classes.Calendar.getDLLCalendar("4", "return", _stdLanguage)%>
		<div id="Cal4" style="z-index: 900; position: absolute;"/>
	</td>
  </tr>
  
   <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_23", "Preferred Flight times", _stdLanguage)%>:</td>
    <td class="groupForm">
		<select id="cmbPreferedDepartureTime" > 
         <option value="Anytime"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_32", "Anytime", _stdLanguage)%></option>
         <option value="AM"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_33", "AM", _stdLanguage)%></option>
         <option value="PM"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_34", "PM", _stdLanguage)%></option>
         </select>
	</td>
  </tr>
  <tr>
    <td colspan="2" height="10"></td>
  </tr>
  <!--
  <tr>
    <td colspan="2" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_26", "Car Hire", _stdLanguage)%></td>
  </tr>
  <tr>
    <td colspan="2" height="10"></td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_27", "Car Hire Group", _stdLanguage)%>:</td>
	<td class="groupForm">
	    <select id="ddlCarHireGroup" onchange="ShowHideBreak(this)">
	        <option value="Not Required">Not Required</option>
	        <option value="Group A">Group A</option>
	        <option value="Group B">Group B</option>
	        <option value="Group C">Group C</option> 
        </select>
    </td>
  </tr>
  <tr id="ra" style="visibility:hidden;">
		<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_30", "Rental Period", _stdLanguage)%>:</td>
		<td class="groupForm">
			<select id="ddlRentalPeriod">
	            <option value="Whole Duration">Whole Duration</option>
	            <option value="Part Duration">Part Duration</option>
            </select>			
		</td>
	</tr>
	<tr id="rb" style="visibility:hidden;">
		<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_30", "Pick Up", _stdLanguage)%>:</td>
		<td class="groupForm">
			<select id="ddlPickUp">
	            <option value="Airport">Airport</option>
	            <option value="Hotel">Hotel</option>
            </select>
		</td>
	</tr>
	<tr id="rc" style="visibility:hidden;">
		<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_30", "Drop Off", _stdLanguage)%>:</td>
		<td class="groupForm">
			<select id="ddlDropOff">
	            <option value="Airport">Airport</option>
	            <option value="Hotel">Hotel</option> 
            </select>
		</td>
    </tr>
    
    
    -->
  <tr>
  	<td>&nbsp;</td>
    <td class="groupFormClear">
				<%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_35", "Additional Information", _stdLanguage)%>:<br />
				<textarea id="txtAdditionalInformation" cols="54" rows="4" runat="server"></textarea>
	</td>
  </tr>
  <tr>
	<td colspan="2" class="groupFormClear">
	            <div class="RegisterCancelButton" onclick="SendMailBonVoyageBreak('ctl00_');">
                    <div class="buttonCornerLeft"></div>
                    <div class="buttonContent">
                    <%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_38", "Submit", _stdLanguage)%>
                    </div>
                    <div class="buttonCornerRight"></div>
                </div> 	
	</td>
  </tr>
  <tr>
	<td>&nbsp;</td>
	<td class="groupForm"> <div class="ErrorList"> 
    <asp:Label ID="LabError" runat="server"></asp:Label></div></td>
  </tr>
</table>
</div> 
  <div id="dvBreakResult" style="display:none">
  <table width ="830" border="0" cellspacing="0" cellpadding="0" class="base">
  <tr>
    <td class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_36", "BON VOYAGE BREAK REQUEST", _stdLanguage)%></td>
  </tr>
  <tr>
	<td width="830">&nbsp;</td>
  </tr>
  <tr>  	
    <td class="groupForm">
		<%=tikAeroB2C.Classes.Language.Value("BonVoyageBreak_40", "Thank you for your enquiry. We will get back to you shortly.", _stdLanguage)%>
	</td>
  </tr>
  </table>
  </div>
  <div id="dvPaxError" class="NBStep4"></div><span id="PaxResult" class="RequestStar"></span>