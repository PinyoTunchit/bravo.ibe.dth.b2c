using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Xml.XPath;
using System.Text;
using System.IO;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class Registration : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Classes.Helper objHp = new Classes.Helper();
                try
                {
                    Library objLi = new Library();
                    B2CVariable objUv = B2CSession.Variable;

                    BookingHeader bookingHeader = B2CSession.BookingHeader;
                    Itinerary itinerary = B2CSession.Itinerary;
                    Passengers passengers = B2CSession.Passengers;
                    Quotes quotes = B2CSession.Quotes;
                    Fees fees = B2CSession.Fees;
                    Mappings mappings = B2CSession.Mappings;

                    //Load Language Dictionary
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                    if (B2CSession.AgentService == null)
                    {
                        if (objUv == null)
                        {
                            objUv = new B2CVariable();
                        }
                        objHp.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                        B2CSession.Variable = objUv;
                    }

                    string serverPath = Server.MapPath("~") + @"\xsl\";

                    objUv.CurrentStep = 1;
                    FillContactInformation();
                }
                catch (Exception ex)
                {
                    objHp.SendErrorEmail(ex, string.Empty);
                    //throw ex;
                }
            }
        }
        private void FillContactInformation()
        {
            ListItem objList;

            Library objLi = new Library();
            
            Agents objAgents = B2CSession.Agents;

            string titleRcd = string.Empty;

            Titles objTitles = CacheHelper.CachePassengerTitle();
            if (objTitles != null)
            {
                for (int i = 0; i < objTitles.Count; i++)
                {
                    objList = new ListItem();
                    titleRcd = objTitles[i].title_rcd;
                    objList.Value = titleRcd + "|" + objTitles[i].gender_type_rcd;
                    objList.Text = objTitles[i].display_name;
                    if (B2CSetting.DefaultTitle.ToUpper().Equals(titleRcd.ToUpper()))
                    {
                        objList.Selected = true;
                    }
                    ddlTitle.Items.Add(objList);
                    objList = null;
                }
            }
            
            
            B2CVariable objUv = B2CSession.Variable;
            //Set Country and nationality
            Countries objCountries = CacheHelper.CacheCountry();
            if (objCountries != null && objCountries.Count > 0)
            {
                //Get From config first
                string[] strCountryOrder = null;
                if (!string.IsNullOrEmpty(B2CSetting.CountryOrder))
                {
                    strCountryOrder = B2CSetting.CountryOrder.ToUpper().Split(',');
                    for (int i = 0; i < strCountryOrder.Length; i++)
                    {
                        objList = new ListItem();
                        objList.Value = strCountryOrder[i].ToString();
                        for (int j = 0; j < objCountries.Count; j++)
                        {
                            if (objCountries[j].Equals(strCountryOrder[i].ToString().Trim()))
                            {
                                objList.Text = objCountries[j].display_name;
                                break;
                            }
                        }

                        if (objList.Value == objAgents[0].country_rcd)
                        { objList.Selected = true; }

                        if (optNationality != null)
                            optNationality.Items.Add(objList);

                        if (ddlCountry != null)
                            ddlCountry.Items.Add(objList);

                    }
                }

                // National
                for (int i = 0; i < objCountries.Count; i++)
                {
                    ListItem objListNational;
                    if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                    {
                        if (objCountries[i].nationality_country_flag == 1)
                        {
                            objListNational = new ListItem();
                            objListNational.Value = objCountries[i].country_rcd;
                            objListNational.Text = objCountries[i].display_name;

                            if (objListNational.Value == objAgents[0].country_rcd)
                            {
                                objListNational.Selected = true;
                            }

                            if (optNationality != null)
                                optNationality.Items.Add(objListNational);

                            objListNational = null;
                        }
                    }
                }


                // Issuring country
                for (int i = 0; i < objCountries.Count; i++)
                {
                    ListItem objListIssue;
                    if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                    {
                        if (objCountries[i].issue_country_flag == 1)
                        {
                            objListIssue = new ListItem();
                            objListIssue.Value = objCountries[i].country_rcd;
                            objListIssue.Text = objCountries[i].display_name;

                            if (objListIssue.Value == objAgents[0].country_rcd)
                            {
                                objListIssue.Selected = true;
                            }

                            if (opIssueCountry != null)
                            {
                                opIssueCountry.Items.Add(objListIssue);
                            }

                            objListIssue = null;
                        }
                    }
                }


                // country

                for (int i = 0; i < objCountries.Count; i++)
                {
                    if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                    {
                        objList = new ListItem();
                        objList.Value = objCountries[i].country_rcd;
                        objList.Text = objCountries[i].display_name;

                        if (objList.Value == objAgents[0].country_rcd)
                        {
                            objList.Selected = true;
                        }
                        ddlCountry.Items.Add(objList);
                        objList = null;
                    }
                }
            }


            Languages objLanguage = CacheHelper.CacheLanguage();
            if (objLanguage != null && objLanguage.Count > 0)
            {
                for (int i = 0; i < objLanguage.Count; i++)
                {
                    objList = new ListItem();
                    objList.Value = objLanguage[i].language_rcd;
                    objList.Text = objLanguage[i].display_name;

                    if (objList.Value.ToUpper() == tikAeroB2C.Classes.Language.CurrentCode().ToUpper())
                    {
                        objList.Selected = true;
                    }

                    optLanguage.Items.Add(objList);
                    objList = null;
                }
            }

            Documents documents = CacheHelper.CacheDocumentType();
            if (documents != null && documents.Count > 0)
            {
                if (documents != null)
                {
                    for (int i = 0; i < documents.Count; i++)
                    {
                        objList = new ListItem();
                        objList.Value = documents[i].document_type_rcd;
                        objList.Text = documents[i].display_name;

                        optDocumentType.Items.Add(objList);

                        objList = null;
                    }
                }
            }

        }
    }
}