<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CharterFlight.ascx.cs" Inherits="tikAeroB2C.UserControls.CharterFlight" %>
<iframe  style="height:0px; z-index:666;position:absolute; display: none;"  id='fdCal3'  src='' class='frmcls'></iframe>
<iframe  style="height:0px; z-index:555;position:absolute; display: none;"  id='fdCal4'  src='' class='frmcls'></iframe>
  <div id="dvCharterFlight">
  <table width ="830" border="0" cellspacing="0" cellpadding="0" class="base">
  <tr>
    <td colspan="3" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_36", "Charter Flight", _stdLanguage)%></td>
  </tr>
  <tr>
    <td width="60">&nbsp;</td>
	<td width="170">&nbsp;</td>
	<td width="600">&nbsp;</td>
  </tr>
  <tr>  	
    <td colspan="3" class="groupForm">
		<%=tikAeroB2C.Classes.Language.Value("CharterFlight_1", "Please provide information in the following form and we will get back to you shortly. Thank you for your enquiry.", _stdLanguage)%>
	</td>
  </tr>
  <tr>
    <td rowspan="18"></td>
    <td colspan="2" height="10"></td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_2", "Title", _stdLanguage)%>:</td>
    <td class="groupForm">
<select id="ddlLeadPassengerTitle" runat="server"> </select>  
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_3", "First Name", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="tbLeadPassengerFirstName" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_4", "Surname", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="tbLeadPassengerLastName" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_5", "Company Name", _stdLanguage)%>:</td>
    <td class="groupForm">
		<input type="text" id="tboCompanyName" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_6", "Mobile No", _stdLanguage)%>.:</td>
    <td class="groupForm">
		<input type="text"  id="tbLeadPassengerContactMobile" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_7", "Home/Business No.", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="tbLeadPassengerContactPhoneNumber" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_8", "Email", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text" id="tbLeadPassengerEmailAddress" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_9", "Confirm Email", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="tbLeadPassengerEmailAddressReEnter" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_10", "Address 1", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="tbLeadPassengerAddress1" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_11", "Address 2", _stdLanguage)%>:</td>
    <td class="groupForm">
		<input type="text"  id="tbLeadPassengerAddress2" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_12", "Town/City", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"  id="tbLeadPassengerPostalTown" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_14", "Postal Code", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
		<input type="text"   id="tbLeadPassengerPostcode" runat="server"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_15", "Country", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
<select id="ddlLeadPassengerCountry" runat="server"> </select>  
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_16", "Type of Request", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
        <select id="cmbTypeOfRequest" > 
         <option value="Passenger"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_17", "Passenger", _stdLanguage)%></option>
         <option value="Cargo"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_18", "Cargo", _stdLanguage)%></option>
         <option value="Combined flight"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_19", "Combined flight", _stdLanguage)%></option>
         </select>
	</td>
  </tr>
  <tr>
	<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_20", "Total Passenger Numbers", _stdLanguage)%>:<font color="#FF0000">*</font><br/>
	<%=tikAeroB2C.Classes.Language.Value("CharterFlight_21", "Including Infants", _stdLanguage)%></td>
    <td class="groupForm">
		<input type="text"  id="tboTotalPassenger" runat="server" Width="200"></>
	</td>
  </tr>
  <tr>
    <td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_22", "Type of Trip", _stdLanguage)%>:<font color="#FF0000">*</font></td>
    <td class="groupForm">
        <select id="cmbTypeOfTrip" > 
         <option value="One Way"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_23", "One Way", _stdLanguage)%></option>
         <option value="Return"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_24", "Return", _stdLanguage)%></option>
         <option value="Multi-Sector"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_25", "Multi-Sector", _stdLanguage)%></option>
         </select>
	</td>
  </tr>
  <tr>
    <td colspan="3" height="10"></td>
  </tr>
  <tr>
    <td colspan="3" class="headerWithSub"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_26", "Flight Details", _stdLanguage)%></td>
  </tr>
  <tr>
    <td colspan="3" height="10"></td>
  </tr>
  <tr>
  	<td colspan="2">&nbsp;</td>
    <td>
		<table width="100%" border="0" cellspacing="2" cellpadding="2">
		  <tr>
			<td class="groupForm" width="35%"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_27", "From", _stdLanguage)%>:</td>
			<td class="groupForm" width="65%"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_28", "To", _stdLanguage)%>:</td>
		  </tr>
		  <tr>
			<td class="groupForm">
				<input type="text"  id="tboOrigin" runat="server"></>
			</td>
			<td class="groupForm">
				<input type="text"  id="tboDestination" runat="server"></>
			</td>
		  </tr>
		  <tr>
			<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_29", "Date of Departure", _stdLanguage)%>:</td>
			<td id="ra" class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_30", "Date of Return", _stdLanguage)%>:</td>
		  </tr>
		  <tr>
			<td class="groupFormClear">
				  <%=tikAeroB2C.Classes.Calendar.getDLLCalendar("3", "outward", _stdLanguage)%>
				  <div id="Cal3" style="z-index: 900; position: absolute;"/>
			</td>
			<td id="rb" class="groupFormClear">
				 <%=tikAeroB2C.Classes.Calendar.getDLLCalendar("4", "return", _stdLanguage)%>
				 <div id="Cal4" style="z-index: 900; position: absolute;"/>
			</td>
		  </tr>
		  <tr>
			<td class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_31", "Prefered Departure Time", _stdLanguage)%>:</td>
			<td id="rc" class="groupForm"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_37", "Prefered Return Departure Time", _stdLanguage)%>:</td>
		  </tr>
		  <tr>
			<td class="groupFormClear">
       <select id="cmbPreferedDepartureTime" > 
         <option value="Anytime"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_32", "Anytime", _stdLanguage)%></option>
         <option value="AM"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_33", "AM", _stdLanguage)%></option>
         <option value="PM"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_34", "PM", _stdLanguage)%></option>
         </select>
			</td>
			<td id="rd" class="groupFormClear">
     <select id="cmbPreferedReturnDepartureTime" > 
          <option value="Anytime"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_32", "Anytime", _stdLanguage)%></option>
         <option value="AM"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_33", "AM", _stdLanguage)%></option>
         <option value="PM"><%=tikAeroB2C.Classes.Language.Value("CharterFlight_34", "PM", _stdLanguage)%></option>
         </select>
			</td>
		  </tr>
		  <tr>
			<td colspan="2" class="groupFormClear">
				<%=tikAeroB2C.Classes.Language.Value("CharterFlight_35", "Additional Information", _stdLanguage)%>:<br />			
				<textarea id="txtAdditionalInformation" name="dddd" cols="54" rows="4" runat="server"></textarea>
			</td>
		  </tr>
		</table>
	</td>
  </tr>
  <tr>
	<td colspan="3" class="groupFormClear">
	            <div class="RegisterCancelButton" onclick="SendMailCharterFlight('ctl00_');">
                    <div class="buttonCornerLeft"></div>
                    <div class="buttonContent">
                    <%=tikAeroB2C.Classes.Language.Value("CharterFlight_38", "Submit", _stdLanguage)%>
                    </div>
                    <div class="buttonCornerRight"></div>
                </div> 	
	</td>
  </tr>
  <tr>
	<td colspan="2">&nbsp;</td>
	<td class="groupForm"> <div class="ErrorList">
    <asp:Label ID="LabError" runat="server"></asp:Label></div></td>
  </tr>
</table>
<table style="top:0px; left:770px; position:absolute; z-index:99;" width="225" border="0" cellspacing="0" cellpadding="0">
	<tr>
	 <td style="padding:10px 0px 5px 0px">
		 <%--<iframe src="./HTML/Popup/banner.html" name="BannerIFrame" id="BannerIFrame" width="225" height="768" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" align="left"></iframe>--%>
	 </td>
	</tr>
</table>
  </div> 