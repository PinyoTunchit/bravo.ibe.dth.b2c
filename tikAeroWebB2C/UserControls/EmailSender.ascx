<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailSender.ascx.cs" Inherits="tikAeroB2C.UserControls.EmailSender" %>
<div id="EmailSenderBox" class="ErrorBox">
	<div class="ErrorBoxHeader"></div>
	<span class="Email"><%=tikAeroB2C.Classes.Language.Value("Passenger_Itinerary_46", "Email:", _stdLanguage)%> <input type="text" id="txtEmail" /></span>
    <div class="ButtonEmail">
        <div class="" onclick="SendItineraryEmailInput();">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent">
                <%=tikAeroB2C.Classes.Language.Value("Passenger_Itinerary_44", "Send", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
        </div>
        <div class="" onclick="CloseEmailSender();">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent">
                <%=tikAeroB2C.Classes.Language.Value("Passenger_Itinerary_45", "Cencel", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
        </div>
	</div>
<div class="clearboth"></div>
</div>
