using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tikSystem.Web.Library;
using tikAeroB2C.Classes;
using System.Text;
using System.Net;
using System.Xml;
using System.Xml.XPath;
using System.IO;

namespace tikAeroB2C.UserControls
{
    public partial class SearchAvailability : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            B2CVariable objUv = B2CSession.Variable;
            BookingHeader bookingHeader = B2CSession.BookingHeader;
            Agent objAgent = null;
            Routes routes = null;

            try
            {
                if (objUv != null)
                {
                    objAgent = CacheHelper.CacheAgency(objUv.Agency_Code);
                    if (IsPostBack == false || B2CSession.LanCode != null)
                    {
                        ListItem objList;

                        //Load Langauge to String Dictionary
                        _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                        //Load origin to htmlselect
                        routes = CacheHelper.CacheOrigin();
                        for (int i = 0; i < routes.Count; i++)
                        {
                            if (routes[i].routes_b2c > 0)
                            {
                                objList = new ListItem();
                                objList.Value = routes[i].origin_rcd + "|" + routes[i].currency_rcd;
                                objList.Text = routes[i].display_name;
                                if (objAgent != null && objAgent.airport_rcd == routes[i].origin_rcd)
                                {
                                    objList.Selected = true;
                                }
                                optOrigin.Items.Add(objList);
                                objList = null;
                            }

                        }
                        routes = null;

                        //Load origin to htmlselect
                        routes = CacheHelper.CacheDestination();
                        if (routes != null && routes.Count > 0)
                        {
                            string selectValue = optOrigin.Items[optOrigin.SelectedIndex].Value.Split('|')[0];
                            for (int i = 0; i < routes.Count; i++)
                            {
                                if (routes[i].origin_rcd.Equals(selectValue) == true && routes[i].b2c_flag == true)
                                {
                                    objList = new ListItem();
                                    objList.Value = routes[i].origin_rcd + "|" +
                                                    optOrigin.Items[optOrigin.SelectedIndex].Text + "|" +
                                                    routes[i].destination_rcd + "|" +
                                                    routes[i].display_name + "|" +
                                                    routes[i].day_range.ToString() + "|" +
                                                    routes[i].currency_rcd;

                                    objList.Text = routes[i].display_name;
                                    if (B2CSetting.DefaultDestination == routes[i].destination_rcd)
                                    {
                                        objList.Selected = true;
                                    }
                                    optDestination.Items.Add(objList);
                                    objList = null;
                                }
                            }
                        }
                        //Load Currencies
                        if (optCurrency != null)
                        {
                            Currencies currencies = CacheHelper.CacheCurrencies();
                            if (currencies != null)
                            {
                                for (int i = 0; i < currencies.Count; i++)
                                {
                                    objList = new ListItem();
                                    objList.Value = currencies[i].currency_rcd;
                                    objList.Text = currencies[i].display_name;

                                    if (objAgent != null && objList.Value.ToUpper() == objAgent.currency_rcd)
                                    { objList.Selected = true; }

                                    optCurrency.Items.Add(objList);
                                    objList = null;
                                }
                            }
                        }

                        //Load Country
                        if (optNationality != null)
                        {
                            //if (bookingHeader == null)
                            //    bookingHeader = new BookingHeader();

                            //if (string.IsNullOrEmpty(bookingHeader.country_rcd))
                            //{
                            //    //Set default country
                            //    bookingHeader.country_rcd = objAgent.country_rcd;
                            //}

                            Countries objCountries = CacheHelper.CacheCountry();
                            //Get From config first
                            string[] strCountryOrder = null;
                            if (!string.IsNullOrEmpty(B2CSetting.CountryOrder))
                            {
                                strCountryOrder = B2CSetting.CountryOrder.ToUpper().Split(',');
                                for (int i = 0; i < strCountryOrder.Length; i++)
                                {
                                    if (objCountries[i].nationality_country_flag == 1)
                                    {
                                        objList = new ListItem();
                                        objList.Value = strCountryOrder[i].ToString();
                                        for (int j = 0; j < objCountries.Count; j++)
                                        {
                                            if (objCountries[j].country_rcd.Equals(strCountryOrder[i].ToString().Trim()))
                                            {
                                                objList.Text = objCountries[j].display_name;
                                            }
                                        }

                                        //if (objList.Value == bookingHeader.country_rcd)
                                        //{ objList.Selected = true; }
                                        optNationality.Items.Add(objList);
                                    }
                                }
                            }
                            //Get from DB
                            for (int i = 0; i < objCountries.Count; i++)
                            {
                                if (objCountries[i].nationality_country_flag == 1)
                                {
                                    if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                                    {
                                        objList = new ListItem();
                                        objList.Value = objCountries[i].country_rcd;
                                        objList.Text = objCountries[i].display_name;

                                        //if (objList.Value == bookingHeader.country_rcd)
                                        //{ objList.Selected = true; }
                                        optNationality.Items.Add(objList);
                                    }
                                }
                            }
                        }

                        if (dvlowFareFinderOption != null)
                        {
                            //Hide low fare finder button if it is not allowed.
                            Availabilities availability = new Availabilities(SecurityHelper.GenerateSessionlessToken());
                            if (availability.LowFareFinderAllow(objUv.Agency_Code) == true)
                            {
                                dvlowFareFinderOption.Visible = true;
                            }
                            else
                            {
                                dvlowFareFinderOption.Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper objHelper = new Helper();
                if (objUv != null)
                {
                    if (string.IsNullOrEmpty(objUv.Agency_Code))
                    {
                        objHelper.SendErrorEmail(ex, "SearchAvailability Session agency code is null.");
                    }
                    else if (objAgent == null)
                    {
                        objHelper.SendErrorEmail(ex, "SearchAvailability agency object is null. Agency Code = " + objUv.Agency_Code);
                    }
                    else if (objAgent.currency_rcd == null)
                    {
                        objHelper.SendErrorEmail(ex, "SearchAvailability one of the Agency currency is null.");
                    }
                    else if (objAgent.airport_rcd == null)
                    {
                        objHelper.SendErrorEmail(ex, "SearchAvailability one of Agency airport is null.");
                    }
                    else if (routes == null)
                    {
                        objHelper.SendErrorEmail(ex, "SearchAvailability one of the route object is null.");
                    }
                    else if (_stdLanguage == null)
                    {
                        objHelper.SendErrorEmail(ex, "SearchAvailability Language object is null.");
                    }
                    else
                    {
                        objHelper.SendErrorEmail(ex, "SearchAvailability : Error Availability At step = " + objUv.CurrentStep);
                    }
                }
                else
                {
                    objHelper.SendErrorEmail(ex, "SearchAvailability : Null at object booking");
                }
                
            }
        }
    }
}