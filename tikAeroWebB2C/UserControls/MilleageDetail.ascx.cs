using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class MilleageDetail : System.Web.UI.UserControl
    {
        public string _parameter;
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper objXsl = new Helper();
            if (IsPostBack == false)
            {
                try
                {
                    //Load Langauge to String Dictionary
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                    System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                    if (B2CSession.Client != null)
                    {
                        Client objClient = B2CSession.Client;
                        ServiceClient obj = new ServiceClient();
                        obj.objService = B2CSession.AgentService;
                        Library ObjLi = new Library();

                        System.Xml.Xsl.XslTransform objTransform = null;

                        string FFPXml = obj.GetTransaction("", "", "", "", "", objClient.client_profile_id.ToString(), "", "", "", DateTime.MinValue, DateTime.MinValue, DateTime.MinValue,
                          DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, 0, false, true, false, false, true);

                        obj = null;

                        // Paging
                        WebService.B2cService service = new tikAeroB2C.WebService.B2cService();
                        int rowPerPage = B2CSetting.RowsPerPageBooking;
                        bool usePaging = B2CSetting.UsePaging;

                        if (usePaging)
                        {
                            B2CSession.xmlFFP = FFPXml;
                            FFPXml = service.GridPagingWithXML(FFPXml, rowPerPage, 0, true);
                        }
                        // **********************//

                        objTransform = objXsl.GetXSLDocument("MilleageDetail");
                        dvFFPDetail.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, XmlHelper.Serialize(B2CSession.Client, false));

                        objTransform = objXsl.GetXSLDocument("FFPList");
                        dvFFPList.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, FFPXml);
                    }
                }
                catch (Exception ex)
                {
                    objXsl.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }
        }
    }
}