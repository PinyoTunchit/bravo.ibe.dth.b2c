using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;

namespace tikAeroB2C.UserControls
{
    public partial class Map : System.Web.UI.UserControl
    {
        #region Decalration
        public string _parameter;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                LoadSeatMap();
            }
        }

        private void LoadSeatMap()
        {
            Classes.Helper objHp = new Classes.Helper();
            try
            {
                Mappings mappings = B2CSession.Mappings;
                BookingHeader bookingHeader = B2CSession.BookingHeader;
                Passengers passengers = B2CSession.Passengers;
                string strLanguage = tikAeroB2C.Classes.Language.CurrentCode().ToUpper();

                HtmlTable table;
                HtmlTableCell td;
                HtmlTableRow tr;

                DataSet ds;

                StringBuilder stbHtml;
                StringWriter stwHtml;
                HtmlTextWriter htwHtml;

                string[] args = _parameter.Split('|');
                string flightId = args[0];
                string originRcd = args[1];
                string destinationRcd = args[2];
                string boardingClass = args[3];
                string bookingClass = args[4];
                string strXml = string.Empty;

                string classHeader = "header";
                string classExit = "exit";
                string classBook = "booked";
                string classAvailable = "available";
                string classSeatType = string.Empty;
                string classSelectedseat = "selectedseat";
                string classBgColor = "BgColor";
                //string classBgColorAvailable = "BgColor available";
                string classOccupiedseat = "occupiedseat";
                string classLava = "lava";
                string classPantry = "pantry";
                string reverse = string.Empty;
                string result = string.Empty;
                string strFeeRcd = string.Empty;

                string strSeatSelected = string.Empty;
                string strSeatRow = string.Empty;
                string strSeatColumn = string.Empty;
                int iLayoutRow = 0;

                int Rows = 0;
                int Cols = 0;
                int RowsNumber = 0;
                int PaxCount = 0;
                int iNumberOfInfant = 0;

                ServiceClient objClient = new ServiceClient();
                objClient.objService = B2CSession.AgentService;
                ds = objClient.GetSeatMap(originRcd, destinationRcd, flightId, boardingClass, bookingClass, strLanguage);
                objClient.objService = null;
                objClient = null;

                tikAeroB2C.WebService.B2cService objService = new tikAeroB2C.WebService.B2cService();

                if (mappings == null || bookingHeader == null || passengers == null || ds == null)
                {
                    objService.LoadHome();
                }
                else
                {
                    PaxCount = passengers.Count;

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            // Rows = Convert.ToInt16(ds.Tables[0].Rows[0]["number_of_rows"]) + 1;
                            // Cols = Convert.ToInt16(ds.Tables[0].Rows[0]["number_of_columns"]) + 1;

                            if (ds.Tables[0].Rows[0]["number_of_rows"] != DBNull.Value)
                            {
                                int value;
                                if (Int32.TryParse(ds.Tables[0].Rows[0]["number_of_rows"].ToString(), out value))
                                {
                                    Rows = value + 1;
                                }
                                else
                                {
                                    objService.LoadHome();
                                }
                            }
                            else
                            {
                                objService.LoadHome();
                            }

                            if (ds.Tables[0].Rows[0]["number_of_columns"] != DBNull.Value)
                            {
                                int value;
                                if (Int32.TryParse(ds.Tables[0].Rows[0]["number_of_columns"].ToString(), out value))
                                {
                                    Cols = value + 1;
                                }
                                else
                                {
                                    objService.LoadHome();
                                }
                            }
                            else
                            {
                                objService.LoadHome();
                            }


                            //Generate table according to number of rows and columns
                            table = new HtmlTable();
                            table.Attributes.Add("cellspacing", "0");
                            table.Attributes.Add("cellpadding", "0");
                            table.Attributes.Add("border", "0");
                            table.Attributes.Add("class", "TBLSeatAssign");

                            for (int i = 0; i < Rows; i++)
                            {
                                tr = new HtmlTableRow();
                                table.Rows.Add(tr);
                                for (int j = 0; j < Cols; j++)
                                {
                                    td = new HtmlTableCell();
                                    td.ID = "tbSeat_" + i + "_" + j;
                                    td.InnerHtml = "&nbsp;";
                                    RowsNumber = i - 1;
                                    if (j == 0 && RowsNumber > 0)
                                    {
                                        td.InnerText = Convert.ToString(RowsNumber);
                                    }
                                    tr.Cells.Add(td);
                                    tr.Attributes.Add("style", "display:none;");
                                    td.Dispose();
                                    td = null;
                                }
                                tr.Dispose();
                                tr = null;
                            }

                            //Assing Class to table cell
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                //Check Seat Row.
                                if (iLayoutRow != DataHelper.DBToInt16(dr, "layout_row"))
                                {
                                    iLayoutRow = DataHelper.DBToInt16(dr, "layout_row");
                                    strSeatRow = DataHelper.DBToString(dr, "seat_row");
                                    HtmlTableRow tRow = table.Rows[DataHelper.DBToInt16(dr, "layout_row")];
                                    if (string.IsNullOrEmpty(strSeatRow) == false)
                                    {
                                        iNumberOfInfant = ds.Tables[0].Select("seat_row = " + strSeatRow + " AND passenger_type_rcd = 'INF'").Length;
                                    }
                                    //Set Row number.
                                    tRow.Cells[0].InnerHtml = (string.IsNullOrEmpty(strSeatRow) || strSeatRow == "0") ? "&nbsp;" : strSeatRow;
                                    //Show Only Row that contain data.
                                    tRow.Attributes["style"] = "display:block;";
                                }

                                //Check Seat Column.
                                strSeatColumn = DataHelper.DBToString(dr, "seat_column");
                                HtmlTableCell tCell = table.Rows[0].Cells[DataHelper.DBToInt16(dr, "layout_column")];
                                if (strSeatColumn != string.Empty)
                                {
                                    //Set Column header.
                                    tCell.Attributes.Add("class", classHeader);
                                    tCell.InnerHtml = (string.IsNullOrEmpty(strSeatColumn)) ? "&nbsp;" : strSeatColumn;
                                    //Show Only Row that contain data.
                                    table.Rows[0].Attributes["style"] = "display:block;";
                                }

                                switch (dr["location_type_rcd"].ToString())
                                {
                                    case "EX":
                                        table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                             .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("class", classExit);
                                        break;
                                    case "ST":

                                        //Find Seat Type
                                        if (DataHelper.DBToByte(dr, "smoking_flag") == 1)
                                        {
                                            classSeatType = "Smoking";
                                        }
                                        else if (DataHelper.DBToByte(dr, "stretcher_flag") == 1)
                                        {
                                            classSeatType = "Stretcher";
                                        }
                                        else if (DataHelper.DBToByte(dr, "facility_flag") == 1)
                                        {
                                            classSeatType = "Facility";
                                        }
                                        else if (DataHelper.DBToByte(dr, "low_comfort_flag") == 1)
                                        {
                                            classSeatType = "Comfort";
                                        }
                                        else
                                        {
                                            classSeatType = string.Empty;
                                        }

                                        if (DataHelper.DBToByte(dr, "reverse_seat_flag") == 1)
                                        { reverse = "R"; }
                                        else
                                        { reverse = string.Empty; }

                                        //Set Fee rcd as part of the class name.
                                        if (B2CSetting.DifferentiateSeatByFee == true)
                                        {
                                            strFeeRcd = DataHelper.DBToString(dr, "fee_rcd");
                                        }
                                        if (DataHelper.DBToByte(dr, "blocked_flag") == 1)
                                        {
                                            table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                                 .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("class", classBook + reverse);
                                        }
                                        else if (dr["passenger_id"].ToString().Length == 0)
                                        {
                                            //Check Reverse Seat
                                            if (DataHelper.DBToByte(dr, "block_b2c_flag") == 1)
                                            {
                                                table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                                     .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("class", classBook + reverse);
                                            }
                                            else
                                            {
                                                if (strSeatSelected != dr["layout_row"] + "|" + dr["layout_column"])
                                                {
                                                    table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                                         .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("class", classAvailable + classSeatType + reverse + strFeeRcd);
                                                    table.Rows[Convert.ToInt16(dr["layout_row"])]
                                                         .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("onClick", "selectSeat('" + DataHelper.DBToString(dr, "layout_row") + "','" +
                                                                                                                                    DataHelper.DBToString(dr, "layout_column") + "','" +
                                                                                                                                    DataHelper.DBToString(dr, "seat_row") + "','" +
                                                                                                                                    DataHelper.DBToString(dr, "seat_column") + "','" +
                                                                                                                                    PaxCount + "','" +
                                                                                                                                    classAvailable + classSeatType + reverse + strFeeRcd + "', " +
                                                                                                                                    DataHelper.DBToByte(dr, "no_child_flag") + ", " +
                                                                                                                                    DataHelper.DBToByte(dr, "emergency_exit_flag") + ", '" +
                                                                                                                                    DataHelper.DBToString(dr, "fee_rcd") + "', '" +
                                                                                                                                    "{\"NumberOfInfant\":" + iNumberOfInfant + "}" + "')");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Find current passenger selected
                                            strSeatSelected = dr["layout_row"] + "|" + dr["layout_column"]; //Check for duplicate record.
                                            table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                                 .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("class", classOccupiedseat + reverse);
                                        }
                                        //Add ToolTip
                                        table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                                 .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("title", dr["tooltip_text"].ToString());
                                        break;
                                    case "WG":
                                        table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                             .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("class", classBgColor);
                                        break;
                                    case "GL":
                                        table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                             .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("class", classPantry);
                                        break;
                                    case "TL":
                                        table.Rows[DataHelper.DBToInt16(dr, "layout_row")]
                                             .Cells[DataHelper.DBToInt16(dr, "layout_column")].Attributes.Add("class", classLava);
                                        break;

                                }
                            }

                            //Assign Passenger To Seat

                            strXml = ds.GetXml().Replace(" xmlns=\"test2\"", string.Empty);

                            Library objLi = new Library();
                            XPathDocument xmlDoc = new XPathDocument(new StringReader(strXml));
                            XPathNavigator xN = xmlDoc.CreateNavigator();
                            int layoutRow = 0;
                            int layoutColumn = 0;
                            iNumberOfInfant = 0;
                            iLayoutRow = 0;

                            foreach (Mapping mp in mappings)
                            {
                                if ((mp.seat_number.Length > 0) && (mp.flight_id.ToString() == flightId))
                                {
                                    //Check Seat Row.
                                    if (iLayoutRow != mp.seat_row)
                                    {
                                        iLayoutRow = mp.seat_row;
                                        if (iLayoutRow != 0)
                                        {
                                            iNumberOfInfant = ds.Tables[0].Select("seat_row = " + iLayoutRow + " AND passenger_type_rcd = 'INF'").Length;
                                        }
                                    }

                                    foreach (XPathNavigator n in xN.Select("NewDataSet/SeatMap[seat_row = '" + mp.seat_row.ToString() + "'][seat_column  = '" + mp.seat_column.ToString() + "']"))
                                    {
                                        //Check Reverse Seat
                                        layoutRow = Convert.ToInt32(objLi.getXPathNodevalue(n, "layout_row", Library.xmlReturnType.value));
                                        layoutColumn = Convert.ToInt32(objLi.getXPathNodevalue(n, "layout_column", Library.xmlReturnType.value));
                                        layoutColumn = Convert.ToInt32(objLi.getXPathNodevalue(n, "layout_column", Library.xmlReturnType.value));

                                        if (XmlHelper.XpathValueNullToByte(n, "reverse_seat_flag") == 1)
                                        { reverse = "R"; }
                                        else
                                        { reverse = string.Empty; }

                                        //Set Fee rcd as part of the class name.

                                        if (B2CSetting.DifferentiateSeatByFee == true)
                                        {
                                            strFeeRcd = XmlHelper.XpathValueNullToEmpty(n, "fee_rcd");
                                        }

                                        table.Rows[layoutRow]
                                             .Cells[layoutColumn].Attributes["class"] = classSelectedseat + reverse;
                                        table.Rows[layoutRow]
                                             .Cells[layoutColumn].Attributes.Add("onClick", "selectSeat('" + layoutRow + "','" +
                                                                                                             layoutColumn + "','" +
                                                                                                             objLi.getXPathNodevalue(n, "seat_row", Library.xmlReturnType.value) + "','" +
                                                                                                             objLi.getXPathNodevalue(n, "seat_column", Library.xmlReturnType.value) + "','" +
                                                                                                             PaxCount + "','" +
                                                                                                             classAvailable + classSeatType + reverse + strFeeRcd + "', " +
                                                                                                             XmlHelper.XpathValueNullToByte(n, "no_child_flag") + ", " +
                                                                                                             XmlHelper.XpathValueNullToByte(n, "emergency_exit_flag") + ", '" +
                                                                                                             XmlHelper.XpathValueNullToEmpty(n, "fee_rcd") + "', '" +
                                                                                                             "{\"NumberOfInfant\":" + iNumberOfInfant + "}" + "')");
                                    }
                                }
                            }

                            xN = null;
                            xmlDoc = null;

                            //Render Table control to html
                            stbHtml = new StringBuilder();
                            stwHtml = new StringWriter(stbHtml);
                            htwHtml = new HtmlTextWriter(stwHtml);

                            table.RenderControl(htwHtml);
                            result = stbHtml.ToString();

                            stbHtml = null;

                            stwHtml.Dispose();
                            stwHtml = null;

                            htwHtml.Dispose();
                            htwHtml = null;

                            table.Dispose();
                            table = null;
                        }
                    }
                }

                dvMap.InnerHtml = result;

                if (ds != null)
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                objHp.SendErrorEmail(ex, "Seat Param<br/>" + _parameter.Replace("<", "&lt;").Replace(">", "&gt;"));
                throw ex;
            }
        }
    }
}