using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class FlightSummary : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Helper objHelper = new Helper();
                try
                {
                    Library ObjLi = new Library();
                    Itinerary itinerary = B2CSession.Itinerary;
                    Passengers passengers = B2CSession.Passengers;
                    Quotes quotes = B2CSession.Quotes;
                    Fees fees = B2CSession.Fees;
                    Mappings mappings = B2CSession.Mappings;

                    //Check is the origin and destination are still in the cache,
                    //if not reload again

                    //Load Langauge to String Dictionary
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                    System.Xml.Xsl.XsltArgumentList objArgument = objHelper.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);
                    System.Xml.Xsl.XslTransform objTransform = null;

                    //Render xml to html
                    StringBuilder stb = new StringBuilder();

                    //*************************************************************************************************************
                    //  Get Flight Information Html
                    using (StringWriter stw = new StringWriter(stb))
                    {
                        using (XmlWriter xtw = XmlWriter.Create(stw))
                        {
                            xtw.WriteStartElement("Booking");
                            {
                                ObjLi.BuiltBookingXml(xtw, null, itinerary, passengers, null, null, null, null, null, null, null);
                            }
                            xtw.WriteEndElement();
                        }
                    }

                    objTransform = objHelper.GetXSLDocument("FlightInformation");
                    dvFlightInfo.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, stb.ToString());
                    stb.Remove(0, stb.Length);
                    //*************************************************************************************************************
                    //  Get Quote Information Html
                    using (StringWriter stw = new StringWriter(stb))
                    {
                        using (XmlWriter xtw = XmlWriter.Create(stw))
                        {
                            xtw.WriteStartElement("Booking");
                            {
                                ObjLi.BuiltBookingXml(xtw, null, null, passengers, quotes, fees, null, null, null, null, null);
                            }
                            xtw.WriteEndElement();
                        }
                    }
                    objTransform = objHelper.GetXSLDocument("QuoteInformation");
                    dvQuoteInfo.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, stb.ToString());
                    stb.Remove(0, stb.Length);
                    //************************************************************************************************************
                    //  Get Rule Information Html.
                    using (StringWriter stw = new StringWriter(stb))
                    {
                        using (XmlWriter xtw = XmlWriter.Create(stw))
                        {
                            xtw.WriteStartElement("Booking");
                            {
                                ObjLi.BuiltBookingXml(xtw, null, itinerary, null, null, null, mappings, null, null, null, null);
                            }
                            xtw.WriteEndElement();
                        }
                    }
                    objTransform = objHelper.GetXSLDocument("RuleInformation");
                    dvRuleInfo.InnerHtml = ObjLi.RenderHtml(objTransform, objArgument, stb.ToString());
                    stb.Remove(0, stb.Length);
                    //*************************************************************************************************************
                }
                catch (Exception ex)
                {
                    objHelper.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }
        
        }
    }
}