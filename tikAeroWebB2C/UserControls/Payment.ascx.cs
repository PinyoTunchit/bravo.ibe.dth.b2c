using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Text;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using CaptchaDLL;

namespace tikAeroB2C.UserControls
{
    public partial class Payment : System.Web.UI.UserControl
    {
        public string userFirstName;
        public string userLastName;
        public string _parameter;
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Helper objHelper = new Helper();
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                try
                {
                    B2CVariable objUv = B2CSession.Variable;

                    BookingHeader bookingHeader = B2CSession.BookingHeader;
                    Passengers passengers = B2CSession.Passengers;
                    Vouchers vouchers = B2CSession.Vouchers;
                    Quotes quotes = B2CSession.Quotes;
                    Fees fees = B2CSession.Fees;
                    Payments payments = B2CSession.Payments;
                    Itinerary itinerary = B2CSession.Itinerary;
                    Mappings mappings = B2CSession.Mappings;
                    Taxes taxes = B2CSession.Taxes;
                    Agents objAgents = B2CSession.Agents;
                    Remarks remarks = B2CSession.Remarks;


                    if (objUv == null ||
                        bookingHeader == null ||
                        passengers == null ||
                        passengers.Count == 0 ||
                        itinerary == null ||
                        itinerary.Count == 0 ||
                        mappings == null ||
                        mappings.Count == 0 ||
                        objHelper.SessionTimeout() == true)
                    {
                        B2CSession.Variable = null;
                    }
                    else
                    {
                        if (stContactTitle != null)
                        {
                            Client objClient = B2CSession.Client;

                            if (objClient != null && objClient.client_number > 0)
                            {
                                //Fill Client information if client logon
                                objHelper.FillClientToHeader(bookingHeader, objClient);
                            }
                            //Fill Contact information
                            if (objAgents[0].language_rcd != null & objAgents[0].language_rcd != "")
                            {
                                FillContactInformation(bookingHeader, objAgents[0].country_rcd, objAgents[0].language_rcd);
                            }
                            else
                            {
                                FillContactInformation(bookingHeader, objAgents[0].country_rcd, B2CSetting.DefaultLanguage.Split('-')[0].ToUpper());
                            }
                        }

                        GetAgencyInfo(objUv,
                                      bookingHeader,
                                      passengers,
                                      vouchers,
                                      quotes,
                                      fees,
                                      payments,
                                      itinerary,
                                      mappings,
                                      taxes,
                                      objAgents,
                                      remarks);

                        //External Payment
                        //  creating the strting to show in CAPTCHA image
                        B2CSession.CaptchaImageText = CaptchaImage.GenerateRandomCode(CaptchaType.AlphaNumeric, 6);

                        //Load Country
                        ListItem objList;
                        if (bookingHeader.country_rcd.Length == 0)
                        {
                            //Set default country
                            bookingHeader.country_rcd = objAgents[0].country_rcd;
                        }
                        Countries objCountries = CacheHelper.CacheCountry();

                        //Get From config first
                        string[] strCountryOrder = null;
                        if (!string.IsNullOrEmpty(B2CSetting.CountryOrder))
                        {
                            strCountryOrder = B2CSetting.CountryOrder.ToUpper().Split(',');
                            for (int i = 0; i < strCountryOrder.Length; i++)
                            {
                                objList = new ListItem();
                                objList.Value = strCountryOrder[i].ToString();
                                for (int j = 0; j < objCountries.Count; j++)
                                {
                                    if (strCountryOrder[i].Equals(objCountries[j]))
                                    {
                                        objList.Text = objCountries[j].display_name;
                                        break;
                                    }
                                }

                                if (objList.Value == bookingHeader.country_rcd)
                                { objList.Selected = true; }

                                if (optCountry != null)
                                    optCountry.Items.Add(objList);
                                if (optEftCountry != null)
                                { optEftCountry.Items.Add(objList); }
                                if (stContactCountry != null)
                                { stContactCountry.Items.Add(objList); }

                            }
                        }
                        //Get from DB
                        for (int i = 0; i < objCountries.Count; i++)
                        {
                            if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                            {
                                objList = new ListItem();
                                objList.Value = objCountries[i].country_rcd;
                                objList.Text = objCountries[i].display_name;

                                if (objList.Value == bookingHeader.country_rcd)
                                { objList.Selected = true; }

                                if (optCountry != null)
                                    optCountry.Items.Add(objList);
                                if (optEftCountry != null)
                                { optEftCountry.Items.Add(objList); }
                                if (stContactCountry != null)
                                { stContactCountry.Items.Add(objList); }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    objHelper.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }
        }
        private bool GetAgencyInfo(B2CVariable objUv,
                                    BookingHeader bookingHeader,
                                    Passengers passengers,
                                    Vouchers vouchers,
                                    Quotes quotes,
                                    Fees fees,
                                    Payments payments,
                                    Itinerary itinerary,
                                    Mappings mappings,
                                    Taxes taxes,
                                    Agents objAgents,
                                    Remarks remarks)
        {
            ServiceClient objService = new ServiceClient();
            objService.objService = B2CSession.AgentService;
            Helper objXsl = new Helper();

            Library objLi = new Library();
            System.Xml.Xsl.XslTransform objTransform = null;
            System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

            if (vouchers != null)
            { vouchers.Clear(); }

            //get Payment Tab
            using (StringWriter stw = new StringWriter())
            {
                LoadPaymentTab(stw,
                                objService,
                                objLi,
                                objUv,
                                bookingHeader,
                                quotes,
                                fees,
                                payments,
                                itinerary,
                                objAgents,
                                remarks);

                objTransform = objXsl.GetXSLDocument("PaymentTab");
                dvPaymentTab.InnerHtml = objLi.RenderHtml(objTransform,
                                                          objArgument,
                                                          stw.ToString());
            }

            //Load Credit Card Type
            LoadCreditCardType(objService,
                                objLi,
                                objUv,
                                bookingHeader,
                                quotes,
                                fees,
                                payments,
                                itinerary);

            if (dvFareSummary != null)
            {
                //*************************************************************************************************
                //  Load booking summary
                using (StringWriter stw = new StringWriter())
                {
                    LoadFareSummary(stw,
                                    objService,
                                    objLi,
                                    bookingHeader,
                                    itinerary,
                                    passengers,
                                    quotes,
                                    fees,
                                    mappings,
                                    taxes);


                    objTransform = objXsl.GetXSLDocument("FareSummary");
                    dvFareSummary.InnerHtml = objLi.RenderHtml(objTransform, objArgument, stw.ToString());
                }
            }

            return true;
        }
        private void FillContactInformation(BookingHeader bookingHeader, string strDefaultCountry, string strDefaultLanguage)
        {
            ListItem objList;

            Library objLi = new Library();

            string titleRcd = string.Empty;
            Titles objTitles = CacheHelper.CachePassengerTitle();

            for (int i = 0; i < objTitles.Count; i++)
            {
                objList = new ListItem();
                titleRcd = objTitles[i].title_rcd;
                objList.Value = titleRcd + "|" + objTitles[i].gender_type_rcd;
                objList.Text = objTitles[i].display_name;

                if (bookingHeader.title_rcd.Length == 0)
                {
                    //Set default value from web.config.
                    if (B2CSetting.DefaultTitle.ToUpper().Equals(titleRcd.ToUpper()))
                    { objList.Selected = true; }
                }
                else
                {
                    //Selected value from DB.
                    if (bookingHeader.title_rcd.Equals(titleRcd))
                    { objList.Selected = true; }
                }

                if (stContactTitle != null)
                {
                    stContactTitle.Items.Add(objList);
                }
                objList = null;
            }

            if (txtClientProfileId != null)
            {
                txtClientProfileId.Value = bookingHeader.client_profile_id.ToString();
            }

            if (bookingHeader.client_number > 0 && txtClientNumber != null)
            {
                //Lock Control
                DisableControl();
                txtClientNumber.Value = bookingHeader.client_number.ToString();
            }
            if (txtContactFirstname != null)
            {
                txtContactFirstname.Value = bookingHeader.firstname;
            }
            if (txtContactMiddlename != null)
            {
                txtContactMiddlename.Value = bookingHeader.middlename;
            }
            if (txtContactLastname != null)
            {
                txtContactLastname.Value = bookingHeader.lastname;
            }
            if (txtContactMobile != null)
            {
                txtContactMobile.Value = bookingHeader.phone_mobile;
            }
            if (txtContactHome != null)
            {
                txtContactHome.Value = bookingHeader.phone_home;
            }
            if (txtContactBusiness != null)
            {
                txtContactBusiness.Value = bookingHeader.phone_business;
            }
            if (txtContactEmail != null)
            {
                txtContactEmail.Value = bookingHeader.contact_email;
            }
            if (txtMobileEmail != null)
            {
                txtMobileEmail.Value = bookingHeader.mobile_email;
            }
            if (txtContactAddress1 != null)
            {
                txtContactAddress1.Value = bookingHeader.address_line1;
            }
            if (txtContactAddress2 != null)
            {
                txtContactAddress2.Value = bookingHeader.address_line2;
            }
            if (txtContactZip != null)
            {
                txtContactZip.Value = bookingHeader.zip_code;
            }
            if (txtContactCity != null)
            {
                txtContactCity.Value = bookingHeader.city;
            }
            if (txtContactState != null)
            {
                txtContactState.Value = bookingHeader.state;
            }

            if (stContactCountry != null)
            {
                if (bookingHeader.country_rcd.Length == 0)
                {
                    //Set default country
                    bookingHeader.country_rcd = strDefaultCountry;
                }
                stContactCountry.Value = strDefaultCountry;
            }
            if (stContactLanguage != null)
            {
                if (bookingHeader.language_rcd.Length == 0)
                {
                    //Set default Language
                    bookingHeader.language_rcd = strDefaultLanguage;
                }

                Languages objLanguages = CacheHelper.CacheLanguage();
                for (int i = 0; i < objLanguages.Count; i++)
                {
                    objList = new ListItem();
                    objList.Value = objLanguages[i].language_rcd;
                    objList.Text = objLanguages[i].display_name;

                    if (objList.Value.ToUpper() == bookingHeader.language_rcd)
                    { objList.Selected = true; }

                    if (stContactLanguage != null)
                        stContactLanguage.Items.Add(objList);
                }
            }
            if (txtCompanyName != null)
            {
                txtCompanyName.Value = bookingHeader.invoice_receiver;
            }
            if (txtVat != null)
            {
                txtVat.Value = bookingHeader.tax_id;
            }

        }
        private void DisableControl()
        {
            if (stContactTitle != null)
            {
                stContactTitle.Disabled = true;
            }
            if (txtContactFirstname != null)
            {
                txtContactFirstname.Disabled = true;
            }
            if (txtContactLastname != null)
            {
                txtContactLastname.Disabled = true;
            }
        }
        private void GetFormOfPaymentSubTypeFee(XmlWriter xtw, ServiceClient objService, B2CVariable objUv, BookingHeader bookingHeader, string formOfPayment)
        {
            //Get CC payment Fee
            XmlReaderSettings setting = new XmlReaderSettings();
            setting.IgnoreWhitespace = true;
            using (XmlReader reader = XmlReader.Create(new StringReader(objService.GetFormOfPaymentSubtypeFees(formOfPayment, string.Empty, bookingHeader.currency_rcd, bookingHeader.agency_code, DateTime.MinValue).GetXml()), setting))
            {
                while (!reader.EOF)
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("Fee"))
                    {
                        xtw.WriteNode(reader, false);
                    }
                    else
                    {
                        reader.Read();
                    }
                }
            }

            //Get EFT Payment Fee
            using (XmlReader reader = XmlReader.Create(new StringReader(objService.GetFormOfPaymentSubtypeFees("EFT", string.Empty, bookingHeader.currency_rcd, bookingHeader.agency_code, DateTime.MinValue).GetXml()), setting))
            {
                while (!reader.EOF)
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("Fee"))
                    {
                        xtw.WriteNode(reader, false);
                    }
                    else
                    {
                        reader.Read();
                    }
                }
            }
        }
        private int GetTimeLimitFlag(string strFormOfPayment, DateTime dtBookingDate, int iLimitHour, Itinerary itinerary, Remarks remarks, int agencyTimeZone, int systemSettingTimeZone)
        {
            if (iLimitHour > 0)
            {
                if (itinerary == null || itinerary.Count == 0)
                {
                    return 0;
                }
                else
                {
                    Booking objBooking = new Booking();
                    DateTime fopPayLimit = dtBookingDate.AddHours(iLimitHour);
                    DateTime departureDate = itinerary[0].departure_date;
                    string departureTime = itinerary[0].departure_time.ToString();

                    //payLimit = departureDate;
                    int hourAdd = int.Parse(departureTime.PadLeft(4, '0').Substring(0, 2));
                    int minuteAdd = int.Parse(departureTime.PadLeft(4, '0').Substring(2, 2));
                    DateTime FlightDateTime = new DateTime(departureDate.Year, departureDate.Month, departureDate.Day, hourAdd, minuteAdd, 0);

                    DateTime tktlTimelimit = objBooking.GetBookingTimeLimit(remarks, agencyTimeZone, systemSettingTimeZone, FlightDateTime);

                    if (dvDisplayTimeLimit != null)
                    {
                        dvDisplayTimeLimit.InnerText = tktlTimelimit.ToString("dd/MM/yyyy HH:mm:ss");
                    }

                    if (tktlTimelimit.Equals(DateTime.MinValue))
                    {
                        //No timelimit set either in TKTL or form of payment fee.
                        return 2;
                    }
                    else
                    {
                        if (strFormOfPayment.Equals("ATM"))
                        {
                            if (Convert.ToInt64(string.Format("{0:yyyyMMddHHmm}", tktlTimelimit)) >= Convert.ToInt64(string.Format("{0:yyyyMMddHHmm}", FlightDateTime)))
                            {
                                return 1;
                            }
                        }

                        if (Convert.ToInt64(string.Format("{0:yyyyMMddHHmm}", fopPayLimit)) >= Convert.ToInt64(string.Format("{0:yyyyMMddHHmm}", FlightDateTime)))
                        {
                            return 1;
                        }
                    }
                }
            }
            return 0;
        }
        protected void LoadPaymentTab(StringWriter stw,
                                    ServiceClient objService,
                                    Library objLi,
                                    B2CVariable objUv,
                                    BookingHeader bookingHeader,
                                    Quotes quotes,
                                    Fees fees,
                                    Payments payments,
                                    Itinerary itinerary,
                                    Agents objAgents,
                                    Remarks remarks)
        {
            string tempXml = string.Empty;
            string strFOP = string.Empty;

            // get insurance flag from route
            Byte show_insurance_on_web_flag = 0;
            Routes rDestination = CacheHelper.CacheDestination();
            for (int i = 0; i <= rDestination.Count - 1; i++)
            {
                if (rDestination[i].origin_rcd == itinerary[0].od_origin_rcd & rDestination[i].destination_rcd == itinerary[0].od_destination_rcd)
                {
                    show_insurance_on_web_flag = Convert.ToByte(rDestination[i].show_insurance_on_web_flag);
                }
            }

            using (XmlWriter xtw = XmlWriter.Create(stw))
            {
                xtw.WriteStartElement("ArrayOfAgent");
                {
                    xtw.WriteStartElement("Setting");
                    {
                        xtw.WriteStartElement("balance");
                        {
                            xtw.WriteValue(string.Format("{0:0.00}", objLi.CalOutStandingBalance(quotes, fees, payments)));
                        }
                        xtw.WriteEndElement();
                        xtw.WriteStartElement("origin_route");
                        {
                            xtw.WriteValue(itinerary[0].origin_rcd);
                        }
                        xtw.WriteEndElement();
                        xtw.WriteStartElement("origin_country");
                        {
                            xtw.WriteValue(objLi.FindOriginCountry(CacheHelper.CacheOrigin(), objUv.OriginRcd));
                        }
                        xtw.WriteEndElement();
                        xtw.WriteStartElement("show_insurance_on_web_flag");
                        {
                            xtw.WriteValue(show_insurance_on_web_flag);
                        }
                        xtw.WriteEndElement();
                        xtw.WriteStartElement("currency_rcd");
                        {
                            xtw.WriteValue(bookingHeader.currency_rcd);
                        }
                        xtw.WriteEndElement();
                        xtw.WriteStartElement("OneWay");
                        {
                            xtw.WriteValue(objUv.OneWay);
                        }
                        xtw.WriteEndElement();
                        xtw.WriteStartElement("LangCulture");
                        {
                            xtw.WriteValue(B2CSession.LanCode.ToLower());
                        }
                        xtw.WriteEndElement();
                        //Get Form of payment time limit flag.
                        tempXml = objService.GetFormOfPayments(tikAeroB2C.Classes.Language.CurrentCode().ToUpper());
                        //Get Paypal Flag from web.config
                        String allowPaypal = "0";
                        if (!string.IsNullOrEmpty(B2CSetting.AllowPaypal))
                        {
                            allowPaypal = B2CSetting.AllowPaypal;
                        }
                        xtw.WriteStartElement("AllowPaypal");
                        {
                            xtw.WriteValue(allowPaypal.ToString());
                        }
                        xtw.WriteEndElement();
                        //Get Paypal Total Repeat for Pay from web.config
                        String totalRepeat = "0";
                        if (!string.IsNullOrEmpty(B2CSetting.PaypalRepeat))
                        {
                            totalRepeat = B2CSetting.PaypalRepeat;
                        }
                        xtw.WriteStartElement("PaypalRepeat");
                        {
                            xtw.WriteValue(totalRepeat.ToString());
                        }
                        xtw.WriteEndElement();
                        using (StringReader sr = new StringReader(tempXml))
                        {
                            xtw.WriteStartElement("FormOfPayment");
                            {
                                XPathDocument xmlDoc = new XPathDocument(sr);
                                XPathNavigator nv = xmlDoc.CreateNavigator();
                                foreach (XPathNavigator n in nv.Select("FormOfPayments/GetFormOfPayments"))
                                {
                                    strFOP = XmlHelper.XpathValueNullToEmpty(n, "form_of_payment_rcd");
                                    xtw.WriteStartElement(strFOP);
                                    {
                                        xtw.WriteStartElement("timelimit_flag");
                                        {
                                            xtw.WriteValue(GetTimeLimitFlag(strFOP, bookingHeader.booking_date_time, XmlHelper.XpathValueNullToInt16(n, "timelimit_hours"), itinerary, remarks, objAgents[0].agency_timezone, objAgents[0].system_setting_timezone).ToString());
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("timelimit_hours");
                                        {
                                            xtw.WriteValue(XmlHelper.XpathValueNullToZero(n, "timelimit_hours"));
                                        }
                                        xtw.WriteEndElement();

                                        if (strFOP.ToString().ToUpper() == "DIW")
                                        {
                                            xtw.WriteStartElement("FormOfPaymentSubtypeFees");
                                            {
                                                //GetFormOfPaymentSubTypeFee(xtw, objService, objUv, bookingHeader, strFOP);
                                                xtw.WriteStartElement("Fee");
                                                {
                                                    GetFormOfPaymentFeeXML(xtw, strFOP, bookingHeader, itinerary, quotes, fees, payments, objService);
                                                }
                                                xtw.WriteEndElement();
                                            }
                                            xtw.WriteEndElement();
                                        }

                                        xtw.WriteStartElement("GatewayRepeat");
                                        {
                                            xtw.WriteValue(objUv.GatewayRepeat.ToString());
                                        }
                                        xtw.WriteEndElement();
                                    }
                                    xtw.WriteEndElement();
                                }
                            }
                            xtw.WriteEndElement();
                        }

                    }
                    xtw.WriteEndElement();

                    //Add Agency information.
                    XmlReaderSettings xmlsetting = new XmlReaderSettings();
                    xmlsetting.IgnoreWhitespace = true;

                    tempXml = XmlHelper.Serialize(objAgents, false);
                    using (XmlReader reader = XmlReader.Create(new StringReader(tempXml), xmlsetting))
                    {
                        while (!reader.EOF)
                        {
                            if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("Agent"))
                            {
                                xtw.WriteNode(reader, false);
                                break;
                            }
                            else
                            {
                                reader.Read();
                            }
                        }
                    }
                }
                xtw.WriteEndElement();
            }
        }
        private void LoadCreditCardType(ServiceClient objService,
                                        Library objLi,
                                        B2CVariable objUv,
                                        BookingHeader bookingHeader,
                                        Quotes quotes,
                                        Fees fees,
                                        Payments payments,
                                        Itinerary itinerary)
        {
            //Load Credit Card Type
            StringBuilder stb = new StringBuilder();
            ListItem objList;

            string formOfPayment = string.Empty;
            string formOfPaymentSubtype = string.Empty;
            string strFeeLevel = string.Empty;
            string tempXml = string.Empty;

            short sOdFlag = 0;
            short sMinimumFeeFlag = 0;

            decimal dFeePercentage = 0;
            decimal dFeeAmount = 0;
            decimal dFeeAmountIncl = 0;

            decimal dFee = 0;
            decimal dFeeIncl = 0;

            int iFactor = 0;

            tempXml = objService.GetFormOfPaymentSubTypes("CC", tikAeroB2C.Classes.Language.CurrentCode().ToUpper());

            using (StringWriter stw = new StringWriter())
            {
                XmlWriterSettings xmlSetting = new XmlWriterSettings();
                xmlSetting.OmitXmlDeclaration = true;
                xmlSetting.Indent = false;
                using (XmlWriter xtw = XmlWriter.Create(stw, xmlSetting))
                {
                    xtw.WriteStartElement("FormOfPaymentSubtypeFees");
                    {
                        GetFormOfPaymentSubTypeFee(xtw, objService, objUv, bookingHeader, "CC");
                    }
                    xtw.WriteEndElement();
                }
                objUv.FormOfPaymentFee = stw.ToString();
            }

            using (StringReader srd = new StringReader("<Payment>" + tempXml + objUv.FormOfPaymentFee + "</Payment>"))
            {
                XPathDocument xmlDoc = new XPathDocument(srd);
                XPathNavigator nv = xmlDoc.CreateNavigator();

                foreach (XPathNavigator n in nv.Select("Payment/FormOfPayments/GetFormOfPayments[internet_payment_flag = 1]"))
                {
                    objList = new ListItem();

                    formOfPayment = n.SelectSingleNode("form_of_payment_rcd").InnerXml;
                    formOfPaymentSubtype = n.SelectSingleNode("form_of_payment_subtype_rcd").InnerXml.ToUpper();
                    stb.Append(formOfPaymentSubtype + "{}");
                    stb.Append(formOfPayment + "{}");
                    stb.Append(n.SelectSingleNode("cvv_required_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_cvv_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_expiry_date_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("expiry_date_required_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("address_required_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_address_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_issue_date_flag").InnerXml + "{}");
                    stb.Append(n.SelectSingleNode("display_issue_number_flag").InnerXml + "{}");

                    dFeeAmountIncl = 0;
                    dFeeAmount = 0;

                    strFeeLevel = string.Empty;
                    sOdFlag = 0;
                    sMinimumFeeFlag = 0;
                    dFeePercentage = 0;
                    dFee = 0;
                    dFeeIncl = 0;
                    string strDisplayName = string.Empty;

                    //Find fee with route. If not find go to find fee without route.
                    string condition = "Payment/FormOfPaymentSubtypeFees/Fee[translate(fee_rcd,'abcdefghijklmnopqrstuvwxyz' , 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = '{0}']";
                    XPathNodeIterator x = nv.Select(string.Format(condition + "[currency_rcd = '{1}'][origin_rcd = '{2}'][destination_rcd = '{3}']",
                        formOfPaymentSubtype, bookingHeader.currency_rcd, objUv.OriginRcd, objUv.DestinationRcd));

                    if (x.Count == 0) x = nv.Select(string.Format(condition + "[currency_rcd = '{1}']",
                            formOfPaymentSubtype, bookingHeader.currency_rcd));

                    foreach (XPathNavigator nf in x)
                    {
                        payments.FindFee(nf,
                                       ref strFeeLevel,
                                       ref sOdFlag,
                                       ref sMinimumFeeFlag,
                                       ref dFeePercentage,
                                       ref dFee,
                                       ref dFeeIncl,
                                       ref strDisplayName);

                        dFeeAmount = dFee;
                        dFeeAmountIncl = dFeeIncl;
                        break;
                    }

                    iFactor = payments.calCalculateCreditCardFee(ref dFeeAmount,
                                                                ref dFeeAmountIncl,
                                                                itinerary,
                                                                dFeePercentage,
                                                                strFeeLevel,
                                                                sOdFlag,
                                                                sMinimumFeeFlag,
                                                                objLi.CalOutStandingBalance(quotes, fees, payments),
                                                                objUv.Adult,
                                                                objUv.Child,
                                                                objUv.Infant);

                    stb.Append(dFeeAmountIncl + "{}");
                    stb.Append(dFeeAmount + "{}");
                    stb.Append(n.SelectSingleNode("validate_document_number_flag").InnerXml + "{}");
                    stb.Append(string.Format("{0:0.00}", dFeePercentage) + "{}");
                    stb.Append(string.Format("{0:0}", sMinimumFeeFlag) + "{}");
                    stb.Append(string.Format("{0:0}", iFactor) + "{}");
                    stb.Append(string.Format("{0:0.00}", dFeeAmount) + "{}");
                    stb.Append(string.Format("{0:0.00}", dFeeAmountIncl));

                    objList.Value = stb.ToString();
                    objList.Text = n.SelectSingleNode("display_name").InnerXml;

                    if (!string.IsNullOrEmpty(B2CSetting.DefaultCardType))
                    {
                        if (formOfPaymentSubtype.Equals(B2CSetting.DefaultCardType))
                        {
                            objList.Selected = true;
                        }
                    }
                    if (optCardType != null)
                    {
                        optCardType.Items.Add(objList);
                    }
                    objList = null;
                    //Clear 
                    stb.Remove(0, stb.Length);
                }
            }
        }

        protected void LoadFareSummary(StringWriter stw,
                                        ServiceClient objService,
                                        Library objLi,
                                        BookingHeader bookingHeader,
                                        Itinerary itinerary,
                                        Passengers passengers,
                                        Quotes quotes,
                                        Fees fees,
                                        Mappings mappings,
                                        Taxes taxes)
        {
            using (XmlWriter xtw = XmlWriter.Create(stw))
            {
                //Get Exchange currency
                if (!string.IsNullOrEmpty(B2CSetting.DefaultCurrency))
                {
                    try
                    {
                        double dblExchangeRate = objService.ExchangeRateRead(bookingHeader.currency_rcd, B2CSetting.DefaultCurrency);
                        for (int i = 0; i < itinerary.Count; i++)
                        {
                            itinerary[i].currency_rate = dblExchangeRate;
                        }
                    }
                    catch
                    {
                        //for any exception as currency does not match ,etc...
                    }
                }
                xtw.WriteStartElement("Booking");
                {
                    objLi.BuiltBookingXml(xtw, null, itinerary, passengers, quotes, fees, mappings, null, null, null, taxes);
                }
                xtw.WriteEndElement();
            }
        }

        private void GetFormOfPaymentFeeXML(XmlWriter xtw,
                                                string formOfPaymentRcd,
                                                BookingHeader bookingHeader,
                                                Itinerary itinerary,
                                                Quotes quotes,
                                                Fees fees,
                                                Payments payments,
                                                ServiceClient objService)
        {
            ServiceClient objClient = new ServiceClient();
            Payments objPayments = new Payments();
            Library objLi = new Library();
            StringBuilder strFeeXML = new StringBuilder();
            string strFeeLevel = string.Empty;

            short sOdFlag = 0;
            short sMinimumFeeFlag = 0;

            decimal dFeePercentage = 0;
            decimal dFeeAmount = 0;
            decimal dFeeAmountIncl = 0;
            decimal OutStandingBalance = 0;
            string strXML = string.Empty;
            string strDisplayName = string.Empty;
            DataSet ds = objService.GetFormOfPaymentSubtypeFees(formOfPaymentRcd, string.Empty, bookingHeader.currency_rcd, bookingHeader.agency_code, DateTime.MinValue);
            if (ds != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "FormOfPaymentSubtypeFees";
                strXML = ds.GetXml();
            }

            if (string.IsNullOrEmpty(strXML) == false)
            {
                XPathDocument xmlDoc = new XPathDocument(new StringReader(strXML));
                XPathNavigator nv = xmlDoc.CreateNavigator();
                if (OutStandingBalance == 0)
                {
                    OutStandingBalance = objLi.CalOutStandingBalance(quotes, fees, payments);
                }
                foreach (XPathNavigator n in nv.Select("FormOfPaymentSubtypeFees/Fee[currency_rcd = '" + bookingHeader.currency_rcd + "']"))
                {

                    objPayments.FindFee(n,
                                       ref strFeeLevel,
                                       ref sOdFlag,
                                       ref sMinimumFeeFlag,
                                       ref dFeePercentage,
                                       ref dFeeAmount,
                                       ref dFeeAmountIncl,
                                       ref strDisplayName);

                    objPayments.calCalculateCreditCardFee(ref dFeeAmount,
                                                          ref dFeeAmountIncl,
                                                          itinerary,
                                                          dFeePercentage,
                                                          strFeeLevel,
                                                          sOdFlag,
                                                          sMinimumFeeFlag,
                                                          OutStandingBalance,
                                                          (short)bookingHeader.number_of_adults,
                                                          (short)bookingHeader.number_of_children,
                                                          (short)bookingHeader.number_of_infants);
                    xtw.WriteStartElement("fee_rcd");
                    {
                        xtw.WriteValue(objLi.getXPathNodevalue(n, "fee_rcd", Library.xmlReturnType.value));
                    }
                    xtw.WriteEndElement();
                    xtw.WriteStartElement("display_name");
                    {
                        xtw.WriteValue(objLi.getXPathNodevalue(n, "display_name", Library.xmlReturnType.value));
                    }
                    xtw.WriteEndElement();
                    xtw.WriteStartElement("fee_amount_incl");
                    {
                        xtw.WriteValue(dFeeAmountIncl.ToString());
                    }
                    xtw.WriteEndElement();
                    xtw.WriteStartElement("fee_amount");
                    {
                        xtw.WriteValue(dFeeAmount.ToString());
                    }
                    xtw.WriteEndElement();
                    break;
                }
            }
        }
    }
}