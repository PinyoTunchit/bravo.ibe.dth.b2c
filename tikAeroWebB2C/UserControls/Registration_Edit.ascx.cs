using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Xml.XPath;
using System.Text;
using System.IO;
using tikSystem.Web.Library;
using tikAeroB2C.Classes;

namespace tikAeroB2C.UserControls
{
    public partial class Registration_Edit : System.Web.UI.UserControl
    {
        string strPaxRole = string.Empty;
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Classes.Helper objHp = new Classes.Helper();
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                try
                {
                    string serverPath = Server.MapPath("~") + @"\xsl\";
                    FillContactInformation();
                    if (B2CSession.Client == null) 
                    { 
                        Response.Write("Session has been expired. <br /> Please login again"); return; 
                    }
                    else
                    {
                        FillInformation();
                    }
                }
                catch (Exception ex)
                {
                    objHp.SendErrorEmail(ex, string.Empty);
                    //throw ex;
                }
            }
        }
        private void FillInformation()
        {
            Client sClient = B2CSession.Client;
            Library ObjLi = new Library();
            Itinerary itinerary = B2CSession.Itinerary;

            Quotes quotes = B2CSession.Quotes;
            Fees fees = B2CSession.Fees;
            Mappings mappings = B2CSession.Mappings;
            txtFirstName.Value = sClient.firstname;
            txtLastName.Value = sClient.lastname;
            txtMobilePhone.Value = sClient.phone_mobile;
            txtHomePhone.Value = sClient.phone_home;
            txtBusinessPhone.Value = sClient.phone_business;
            txtEmail.Value = sClient.contact_email;
            
            //optLanguage
            //optClasses.Language.Value = sClient.language_rcd;  
            txtAddress1.Value = sClient.address_line1;
            txtAddressline2.Value = sClient.address_line2;
            if (txtState != null)
            {
                if (string.IsNullOrEmpty(sClient.state) == false)
                {
                    txtState.Value = sClient.state;
                }
            }
            txtZipCode.Value = sClient.zip_code;
            txtcity.Value =sClient.city  ;

            if (hdCurrentPassword != null)
               hdCurrentPassword.Value = sClient.client_password;

            //txtPassword.Value = sClient.client_password;
            //txtReconfirmPassword.Value = sClient.client_password;
            if (txtMobileEmail != null)
            {
                txtMobileEmail.Value = sClient.mobile_email;
            }
            
            bool isFound = false;
            string value = "";
            value = sClient.title_rcd.ToLower(); 

            for (int i = 0; i < ddlTitle.Items.Count && (!isFound); i++)
            {
                if (ddlTitle.Items[i].Value.ToLower().IndexOf(value + "|") >= 0)
                {
                    ddlTitle.Items[i].Selected = true;
                    isFound = true;
                }
            }

            if (ddlCountry.Items.FindByValue(sClient.country_rcd) != null)
            {
                ddlCountry.Items.FindByValue(sClient.country_rcd).Selected = true;
            }

            if (optLanguage.Items.FindByValue(sClient.language_rcd) != null)
            {
                optLanguage.Items.FindByValue(sClient.language_rcd).Selected = true;
            }

            ServiceClient obj = new ServiceClient();
            obj.objService = B2CSession.AgentService;
            DataSet ds = obj.GetClientPassenger(sClient.booking_id.ToString() , sClient.client_profile_id.ToString() , sClient.client_number.ToString() );
            Library objLi = new Library();
            string xmlPassenger = "";
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataView dv = new DataView(ds.Tables["Passenger"], "passenger_role_rcd = 'MYSELF'", "passenger_profile_id", DataViewRowState.CurrentRows);
                DataView dv2 = new DataView(ds.Tables["Passenger"], "passenger_role_rcd <> 'MYSELF'", "passenger_profile_id", DataViewRowState.CurrentRows);

                dv2.Sort = "passenger_type_rcd,lastname,firstname";
            
                if (dv2.ToTable().Rows.Count > 0)
                {
                    xmlPassenger = new Helper().ConvertDataTableToXML(dv.ToTable());

                    if (xmlPassenger == "<NewDataSet />")
                        xmlPassenger = xmlPassenger.Replace("<NewDataSet />", "<NewDataSet>");
                    else
                        xmlPassenger = xmlPassenger.Replace("</NewDataSet>", "");
                    
                    xmlPassenger = xmlPassenger + new Helper().ConvertDataTableToXML(dv2.ToTable()).Replace("<NewDataSet>", "");
                }
                else
                {
                    xmlPassenger = new Helper().ConvertDataTableToXML(ds.Tables["Passenger"]);
                }
                Passenger p = (Passenger)XmlHelper.Deserialize(xmlPassenger, typeof(Passenger)); 
                B2CSession.Passenger = p;
                string serverPath = Server.MapPath("~") + @"\xsl\";

                Helper objXsl = new Helper();
                System.Xml.Xsl.XslTransform objTransform = null;
                System.Xml.Xsl.XsltArgumentList objArgument = objXsl.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                objTransform = objXsl.GetXSLDocument("PassengerAdd");
                dvPassengerAdd.InnerHtml = objLi.RenderHtml(objTransform, objArgument, BuiltPassengerInputHtml(xmlPassenger));
           
            }   
        }
        private void FillContactInformation()
        {
            try
            {
                XPathDocument xmlDoc;
                XPathNavigator nv;
                ListItem objList;

                Library objLi = new Library();

                Titles objTitles = CacheHelper.CachePassengerTitle();
                string titleRcd = string.Empty;
                for (int i = 0; i < objTitles.Count; i++)
                {
                    objList = new ListItem();
                    titleRcd = objTitles[i].title_rcd;
                    objList.Value = titleRcd + "|" + objTitles[i].gender_type_rcd;
                    objList.Text = objTitles[i].display_name;

                    ddlTitle.Items.Add(objList);
                    ddlPsgTitle.Items.Add(objList);
                    objList = null;
                }
                nv = null;
                xmlDoc = null;

                //Set Country and nationality
                Countries objCountries = CacheHelper.CacheCountry();

                //Get From config first
                string[] strCountryOrder = null;
                if (!string.IsNullOrEmpty(B2CSetting.CountryOrder))
                {
                    strCountryOrder = B2CSetting.CountryOrder.ToUpper().Split(',');
                    for (int i = 0; i < strCountryOrder.Length; i++)
                    {
                        objList = new ListItem();
                        objList.Value = strCountryOrder[i].ToString();
                        for (int j = 0; j < objCountries.Count; j++)
                        {
                            if (objCountries[j].country_rcd.Equals(strCountryOrder[i].ToString().Trim()))
                            {
                                objList.Text = objCountries[j].display_name;
                                break;
                            }
                        }

                        if (optNationality != null)
                            optNationality.Items.Add(objList);

                        if (ddlCountry != null)
                            ddlCountry.Items.Add(objList);

                        if (opIssueCountry != null)
                        {
                            opIssueCountry.Items.Add(objList);
                        }
                        
                        objList = null;
                    }
                }

                for (int i = 0; i < objCountries.Count; i++)
                {
                    if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                    {
                        objList = new ListItem();
                        objList.Value = objCountries[i].country_rcd;
                        objList.Text = objCountries[i].display_name;


                        if (ddlCountry != null)
                            ddlCountry.Items.Add(objList);

                        objList = null;
                    }
                }

                // National
                for (int i = 0; i < objCountries.Count; i++)
                {
                    ListItem objListNational;
                    if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                    {
                        if (objCountries[i].nationality_country_flag == 1)
                        {
                            objListNational = new ListItem();
                            objListNational.Value = objCountries[i].country_rcd;
                            objListNational.Text = objCountries[i].display_name;


                            if (optNationality != null)
                                optNationality.Items.Add(objListNational);

                            objListNational = null;
                        }
                    }
                }


                // Issuring country
                for (int i = 0; i < objCountries.Count; i++)
                {
                    ListItem objListIssue;
                    if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                    {
                        if (objCountries[i].issue_country_flag == 1)
                        {
                            objListIssue = new ListItem();
                            objListIssue.Value = objCountries[i].country_rcd;
                            objListIssue.Text = objCountries[i].display_name;

                            HtmlSelect ddl = (HtmlSelect)this.FindControl("opIssueCountry");

                            if (ddl != null)
                            {
                               ddl.Items.Add(objListIssue);
                            }


                            objListIssue = null;
                        }
                    }
                }



                nv = null;
                xmlDoc = null;

                Documents documents = CacheHelper.CacheDocumentType();
                if (documents != null)
                {
                    for (int i = 0; i < documents.Count; i++)
                    {
                        objList = new ListItem();
                        objList.Value = documents[i].document_type_rcd;
                        objList.Text = documents[i].display_name;

                        optDocumentType.Items.Add(objList);

                        objList = null;
                    }
                }

                ServiceClient objClientService = new ServiceClient();
                objClientService.objService = B2CSession.AgentService;
                strPaxRole = objClientService.GetPassengerRole(tikAeroB2C.Classes.Language.CurrentCode().ToUpper());

                //Construct Document Type string
                xmlDoc = new XPathDocument(new StringReader(strPaxRole));
                nv = xmlDoc.CreateNavigator();

                StringBuilder stbResult = new StringBuilder();

                foreach (XPathNavigator n in nv.Select("NewDataSet/PassengerRole[passenger_role_rcd != 'MYSELF']"))
                {
                    objList = new ListItem();
                    objList.Value = objLi.getXPathNodevalue(n, "passenger_role_rcd", Library.xmlReturnType.value);
                    objList.Text = objLi.getXPathNodevalue(n, "display_name", Library.xmlReturnType.value);

                    ddlPassengerRole.Items.Add(objList);

                    objList = null;
                }

                //Set Language
                Languages objLanguage = CacheHelper.CacheLanguage();
                for (int i = 0; i < objLanguage.Count; i++)
                {
                    objList = new ListItem();
                    objList.Value = objLanguage[i].language_rcd;
                    objList.Text = objLanguage[i].display_name;

                    optLanguage.Items.Add(objList);
                    objList = null;
                }
            }
            catch(Exception ex)
            {
                Helper objHelper = new Helper();
                objHelper.SendErrorEmail(ex, strPaxRole);
            }

        }
        private string BuiltPassengerInputHtml(string xml)
        {
            
            try
            {
                XPathDocument xmlDoc;
                XPathNavigator nv;

                StringBuilder str = new StringBuilder();
                Library li = new Library();

                string tempXML = string.Empty;

                str.Append("<Passenger>");
                str.Append("<setup>");

                Documents documents = CacheHelper.CacheDocumentType();
                if (documents != null && documents.Count > 0)
                {
                    xmlDoc = new XPathDocument(new StringReader(documents.GetXml()));
                    nv = xmlDoc.CreateNavigator();
                    str.Append(nv.SelectSingleNode("DocumentTypes").InnerXml);
                    nv = null;
                    xmlDoc = null;
                }

                Titles objTitles = CacheHelper.CachePassengerTitle();
                if (objTitles != null && objTitles.Count > 0)
                {
                    tempXML = objTitles.GetXml();
                    xmlDoc = new XPathDocument(new StringReader(tempXML));
                    nv = xmlDoc.CreateNavigator();
                    str.Append(nv.SelectSingleNode("NewDataSet").InnerXml);
                    nv = null;
                    xmlDoc = null;
                }
                
                Countries objCountries = CacheHelper.CacheCountry();
                if (objCountries != null && objCountries.Count > 0)
                {
                    xmlDoc = new XPathDocument(new StringReader(objCountries.GetXml()));
                    nv = xmlDoc.CreateNavigator();

                    if (!string.IsNullOrEmpty(B2CSetting.CountryOrder))
                    {
                        string[] strCountryOrder = B2CSetting.CountryOrder.ToUpper().Split(',');
                        //Get from config to display first order of country.
                        for (int i = 0; i < strCountryOrder.Length; i++)
                        {
                            str.Append(nv.SelectSingleNode("Countries/Countrys[country_rcd = '" + strCountryOrder[i].Trim() + "']").OuterXml);
                        }
                        //Get country apart from the configuration
                        foreach (XPathNavigator n in nv.Select("Countries/Countrys"))
                        {
                            if (B2CSetting.CountryOrder.Contains(n.SelectSingleNode("country_rcd").InnerXml) == false)
                            {
                                str.Append(n.OuterXml);
                            }
                        }
                    }
                    else
                    {
                        str.Append(nv.SelectSingleNode("Countries").InnerXml);
                    }

                    nv = null;
                    xmlDoc = null;
                }

                if (string.IsNullOrEmpty(strPaxRole) == false)
                {
                    xmlDoc = new XPathDocument(new StringReader(strPaxRole));
                    nv = xmlDoc.CreateNavigator();
                    str.Append(nv.SelectSingleNode("NewDataSet").InnerXml);
                    nv = null;
                    xmlDoc = null;
                }
                str.Append("</setup>");

                if (string.IsNullOrEmpty(xml) == false)
                {
                    //Get Passenger Information
                    xmlDoc = new XPathDocument(new StringReader(xml));
                    nv = xmlDoc.CreateNavigator();
                    str.Append(nv.SelectSingleNode("NewDataSet").OuterXml);
                    nv = null;
                    xmlDoc = null;
                }
                
                str.Append("</Passenger>");

                li = null;

                return str.ToString();
            }
            catch(Exception ex)
            {
                Helper objHelper = new Helper();
                if (string.IsNullOrEmpty(xml))
                { objHelper.SendErrorEmail(ex, "NO XML FOUND"); }
                else
                { objHelper.SendErrorEmail(ex, xml.Replace("<", "&lt;").Replace(">", "&gt;")); }
                throw;
            }
            
        }
    }
}