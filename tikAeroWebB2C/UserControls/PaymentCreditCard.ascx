<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentCreditCard.ascx.cs" Inherits="tikAeroB2C.UserControls.PaymentCreditCard" %>
<div class="xouter">
	<div class="xcontainer">
		<div class="xinner multiplepayment">
			<div id="dvMultiFormCCPayment">
				<div class="MultiplePaymentbyCreditCard">
				    <div class="boxborder">
				      <div class="displaycard">
				        <img src="App_Themes/Default/Images/creditcard.png" alt="We accept Mastercard, Visa, Visa Electron, American Express, Switch, Solo, Delta and Discover" title="We accept Mastercard, Visa, Visa Electron, American Express, Switch, Solo, Delta and Discover" />
				      </div>
				
				      <div class="PassengerDetails">
				
				        <div class="Topic PaymentTopic">
				          <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_3", "Credit Card Detail", _stdLanguage)%>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="ctl00_optCardType">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_7", "Credit Card Type", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <select id="optMultiCardType" onchange="ActivateMultiCCControl();" runat="server">
				              <option value="" class="watermarkOn"></option>
				            </select>
				            <div class="mandatory"></div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="txtMultiCardNumber">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_10", "Credit Card Number", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <input id="txtMultiCardNumber" type="text" />
				            <div class="mandatory">*</div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="txtMultiNameOnCard">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_6", "Name On Card", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <input id ="txtMultiNameOnCard" type="text" onkeypress="EmptyErrorMessage();"/>
				            <div class="mandatory">*</div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail" id="dvMultiIssueNumber">
				          <label for="txtIssueNumber">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_8", "Issue Number", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <input id="txtMultiIssueNumber" type="text" />
				            <div class="mandatory"></div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail" id="dvMultiIssuDate">
				          <label for="optIssueMonth">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_9", "Issue Date(if present)", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <select id="optMultiIssueMonth" class="CardExpiryDate">
				              <option value="" class="watermarkOn"></option>
				              <option selected="selected" value="1">1</option>
				              <option value="2">2</option>
				              <option value="3">3</option>
				              <option value="4">4</option>
				              <option value="5">5</option>
				              <option value="6">6</option>
				              <option value="7">7</option>
				              <option value="8">8</option>
				              <option value="9">9</option>
				              <option value="10">10</option>
				              <option value="11">11</option>
				              <option value="12">12</option>
				            </select>
				
				            <select id="optMultiIssueYear" class="CardExpiryDate">
				              <option value="" class="watermarkOn"></option>
				              <option selected="selected" value="2008">2008</option>
				              <option value="2009">2009</option>
				              <option value="2010">2010</option>
				              <option value="2011">2011</option>
				              <option value="2012">2012</option>
				              <option value="2013">2013</option>
				              <option value="2014">2014</option>
				              <option value="2015">2015</option>
				              <option value="2016">2016</option>
				              <option value="2017">2017</option>
				              <option value="2018">2018</option>
				              <option value="2019">2019</option>
				              <option value="2020">2020</option>
				              <option value="2021">2021</option>
				              <option value="2022">2022</option>
				            </select>
				
				            <div class="mandatory"></div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				
				        <div class="ContactDetail" id="dvMultiExpiryDate">
				          <label for="optExpiredMonth">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_11", "Expiry Date", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <select id="optMultiExpiredMonth" class="CardExpiryDate" onchange="EmptyErrorMessage();">
				              <option value="" class="watermarkOn"></option>
				              <option selected="selected" value="01">1</option>
				              <option value="02">2</option>
				              <option value="03">3</option>
				              <option value="04">4</option>
				              <option value="05">5</option>
				              <option value="06">6</option>
				              <option value="07">7</option>
				              <option value="08">8</option>
				              <option value="09">9</option>
				              <option value="10">10</option>
				              <option value="11">11</option>
				              <option value="12">12</option>
				            </select>
				
				            <select id="optMultiExpiredYear" class="CardExpiryDate" onchange="EmptyErrorMessage();">
				              <option value="" class="watermarkOn"></option>
				            </select>
				
				            <div class="mandatory"></div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail Left" id="dvMultiCvv">
				          <label for="txtCvv">
				            CVV:
				          </label>
				
				          <div class="InformationInput">
				            <input id="txtMultiCvv" type="text" class="InputboxCVV" maxlength="4" onkeypress="EmptyErrorMessage();"/>
				            <div class="mandatory">
				              <div class="MultiDisplayCVV"></div>
				            </div>
				          </div>
				
				        </div>
				
				        <div class="clear-all"></div>
				
				        <div class="SpecialInformation">
							<a href="javascript:CreateWnd('HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/cdcSecurity.html', 500, 588, false);">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_25", "Information on credit card security", _stdLanguage)%>
							</a>
				        </div>
				
				        <div class="clear-all"></div>
				      </div>
				
				      <!-- Address Detail Section -->
				      <div class="PassengerDetails" id="dvMultiAddress">
				        <div class="Topic PaymentTopic">Address Detail</div>
				
				        <div class="ContactDetail">
				          <label for="chkMyname">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_5", "Use contact address", _stdLanguage)%>
				          </label>
				
				          <div class="checkbox">
				            <input id="chkMultiMyname" type="checkbox" onclick="LoadMultiMyName();"/>
				            <div class="mandatory"></div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="txtAddress1">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_16", "Address Line 1", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <input id="txtMultiAddress1" type="text" onkeypress="EmptyErrorMessage();"/>
				            <div class="mandatory">*</div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="txtAddress2">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_17", "Address Line 2 (Optional)", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <input id="txtMultiAddress2" type="text"/>
				            <div class="mandatory"></div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="txtCity">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_20", "Province/County", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <input id="txtMultiCity" type="text" onkeypress="EmptyErrorMessage();"/>
				            <div class="mandatory"></div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="txtCounty">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_19", "Town/City", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <input id="txtMultiCounty" type="text"/>
				            <div class="mandatory">*</div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="txtPostCode">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_22", "Postal Code", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <input id="txtMultiPostCode" type="text" onkeypress="EmptyErrorMessage();"/>
				            <div class="mandatory">*</div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				        <div class="ContactDetail">
				          <label for="ctl00_optCountry">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_23", "Country", _stdLanguage)%>
				          </label>
				
				          <div class="InformationInput">
				            <select id="optMultiCountry" runat="server" onchange="EmptyErrorMessage();">
				              <option value="" class="watermarkOn"></option>
				            </select>
				            <div class="mandatory">*</div>
				          </div>
				          <div class="clear-all"></div>
				        </div>
				
				      </div>
				
				      <div id="dvMultiCCConfirm" class="optional">
				        <div class="Remember FirstLine">
				          <input id="chkMultiPayCreditCard" type="checkbox" />
				          <label for="chkMultiPayCreditCard">
				            I have read, understood and accept all
				          </label>
				          <a href="HTML/<%=tikAeroB2C.Classes.Language.CurrentCode()%>/popup/Agreement.html" target="_blank">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_34", "Terms and Conditions.", _stdLanguage)%>
				          </a>
				        </div>
				        <div class="clear-all"></div>
				      </div>
				
				      <div class="clear-all"></div>
				    </div>
					
					<div id="dvMultiCCButton" class="BTN-Search">
						<div class="ButtonAlignLeft">
							<a href="javascript:CloseDialog();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
								<span>
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
								</span>
							</a>
						</div>
					
						<div class="ButtonAlignRight">
							<a href="javascript:PaymentMultipleForm();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>" class="defaultbutton">
								<span>
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>
								</span>
							</a>
						</div>
					
					</div>
				</div>
				
				<div id="dvFormCCSummary" class="YourSelection" runat="server"></div>
					
				<div class="clear-all"></div>

			</div>
		</div>
	</div>
</div>