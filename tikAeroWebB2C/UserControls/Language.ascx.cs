using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;
using System.Net;
using System.Net.Security;
namespace tikAeroB2C.UserControls
{
    public partial class Language : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ReadConfig
            if (IsPostBack == false || B2CSession.LanCode != null)
            {
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                    Languages objLanguage = new Languages();
                    DataSet ds = objLanguage.ReadLanguageConfig("B2C");

                    TabBar.Text = GenTab(ds, objLanguage.GetLanguageServiceUrl());

                    if (ds != null)
                    {
                        ds.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Classes.Helper objHp = new Classes.Helper();
                    objHp.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }

            }
        }

        public string GenTab(DataSet ds, string urlPath)
        {
            if (ds == null) return "";
            if (ds.Tables.Count <= 0) return "";
            string strControls = "";
            string Content = "";
            string bar = "|";
            string ret = "";
            string strLangWebServiceUrl = urlPath;

            strLangWebServiceUrl = strLangWebServiceUrl.Replace(strLangWebServiceUrl.Split('/')[strLangWebServiceUrl.Split('/').Length - 1], "");
            strLangWebServiceUrl += "images/flags/";
            int rowindex = 0;

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (Convert.ToString("" + dr["ShowFlag"]) == "0")
                {
                    Content = Convert.ToString("" + dr["Text"]);
                    if (rowindex < ds.Tables[0].Rows.Count - 1) { bar = "|"; } else { bar = ""; }
                }
                else
                {
                    Content = "<img onclick=\"Changelang('" + Convert.ToString("" + dr["Culture_ID"]) + "');\" title='" + Convert.ToString("" + dr["Text"]) + "' src='" + strLangWebServiceUrl + Convert.ToString("" + dr["Flag_Url"]) + "' border=0 />";
                    bar = "&nbsp;";
                }

                strControls = Content + bar;
                ret += strControls;
                rowindex++;

            }
            return ret;
        }


    }
}