using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace tikAeroB2C.UserControls
{
    public partial class HtmlContainer : System.Web.UI.UserControl
    {
        public string _parameter = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                try 
                {
                    Page.Response.Write("<iframe frameborder=\"0\" scrolling=\"no\" id=\"frmHtmlContainer\" src=\"" + _parameter + "\" width=\"100%\" onload=\"resizeFrame(document.getElementById('frmHtmlContainer'));\"></iframe>");
                }
                catch (Exception ex)
                {
                    Classes.Helper objHp = new Classes.Helper();
                    objHp.SendErrorEmail(ex, "Html Container<br/>" + _parameter);
                    throw ex;
                }
            }
        }
    }
}