using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.XPath;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;

namespace tikAeroB2C.UserControls
{
    public partial class PassengerDetail : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                //clear session fare
                Session["FareAllID"] = null;

                Classes.Helper objHp = new Classes.Helper();
                //Load Language Dictionary
                _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();
                try
                {
                    B2CVariable objUv = B2CSession.Variable;
                    Passengers passengers = B2CSession.Passengers;
                    Agents objAgents = B2CSession.Agents;
                    Mappings mappings = B2CSession.Mappings;

                    if (objUv == null || objHp.SessionTimeout() == true || objAgents[0] == null || mappings == null || passengers == null || passengers.Count == 0)
                    {
                        if (objUv != null)
                        {
                            objUv.CurrentStep = 1;
                        }
                    }
                    else
                    {
                        Library objLi = new Library();

                        BookingHeader bookingHeader = B2CSession.BookingHeader;
                        Itinerary itinerary = B2CSession.Itinerary;
                        Remarks remarks = B2CSession.Remarks;
                        Quotes quotes = B2CSession.Quotes;
                        Fees fees = B2CSession.Fees;
                        Services services = B2CSession.Services;
                        Taxes taxes = B2CSession.Taxes;
                        Client objClient = B2CSession.Client;


                        System.Xml.Xsl.XslTransform objTransform = null;
                        System.Xml.Xsl.XsltArgumentList objArgument = objHp.AddLanguageXsltArgumentList("tik:Language", _stdLanguage);

                        objUv.CurrentStep = 4;
                        if (stContactTitle != null)
                        {
                            //Fill Client information if client logon
                            objHp.FillClientToHeader(bookingHeader, objClient);
                            //Fill Contact information
                            FillContactInformation(bookingHeader, objAgents[0].country_rcd, tikAeroB2C.Classes.Language.CurrentCode());
                        }

                        //Set default value to passenger
                        FillPassengerDefaultValue(passengers, objClient, objAgents[0]);

                        //Clear Insurance Fee when request new quote.
                        Fee objInsFee = B2CSession.InsuaranceFee;
                        fees.ClearInsuranceFee(objInsFee);

                        //*********************************************************************************************************
                        //  Show passenger input information
                        using (StringWriter stw = new StringWriter())
                        {
                            using (XmlWriter xtw = XmlWriter.Create(stw))
                            {
                                xtw.WriteStartElement("Booking");
                                {
                                    BuiltPassengerInputXml(xtw, mappings, itinerary, fees, objUv.OriginRcd, objUv.DestinationRcd, objUv.Agency_Code, objUv.OneWay);
                                    objLi.BuiltBookingXml(xtw, null, null, passengers, null, null, null, null, null, null, null);
                                }
                                xtw.WriteEndElement();
                            }
                            objTransform = objHp.GetXSLDocument("PassengerInput");
                            string strxml = stw.ToString();

                            dvPassengerInput.InnerHtml = objLi.RenderHtml(objTransform, objArgument, strxml);
                        }

                        //***********************************************************************************************************

                        //  Show Remarks information
                        if (remarks != null)
                            for (int i = 0; i < remarks.Count; i++)
                            {
                                if (remarks[i].remark_type_rcd.ToUpper().Equals("B2CREQ"))
                                {
                                    if (txaAirlineRemark != null)
                                    {
                                        txaAirlineRemark.InnerText = remarks[i].remark_text;
                                    }
                                }
                                else if (remarks[i].remark_type_rcd.ToUpper().Equals("AUXOTH"))
                                {
                                    if (txaItineraryRemark != null)
                                    {
                                        txaItineraryRemark.InnerText = remarks[i].remark_text;
                                    }
                                }
                            }
                        //***********************************************************************************************************

                        //  Show flight information and Fare Information.

                        using (StringWriter stw = new StringWriter())
                        {
                            using (XmlWriter xtw = XmlWriter.Create(stw))
                            {
                                xtw.WriteStartElement("Booking");
                                {
                                    BuiltFlightInputXml(xtw, mappings, bookingHeader, passengers[0].passenger_id);
                                    objLi.BuiltBookingXml(xtw, null, itinerary, null, null, null, mappings, services, null, null, null);
                                }
                                xtw.WriteEndElement();
                            }

                            objTransform = objHp.GetXSLDocument("PassengerDetailFlight");
                            dvFlightInformation.InnerHtml = objLi.RenderHtml(objTransform, objArgument, stw.ToString());
                        }

                        if (dvFareSummary != null)
                        {
                            //*************************************************************************************************************
                            //  Show fare summary
                            using (StringWriter stw = new StringWriter())
                            {
                                using (XmlWriter xtw = XmlWriter.Create(stw))
                                {
                                    xtw.WriteStartElement("Booking");
                                    {
                                        objLi.BuiltBookingXml(xtw, null, itinerary, passengers, quotes, fees, mappings, null, null, null, taxes);
                                    }
                                    xtw.WriteEndElement();
                                }
                                objTransform = objHp.GetXSLDocument("FareSummary");
                                dvFareSummary.InnerHtml = objLi.RenderHtml(objTransform, objArgument, stw.ToString());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    objHp.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
            }
        }

        private void FillContactInformation(BookingHeader bookingHeader, string strDefaultCountry, string strDefaultLanguage)
        {
            ListItem objList;

            Library objLi = new Library();

            string titleRcd = string.Empty;
            Titles objTitles = CacheHelper.CachePassengerTitle();

            for (int i = 0; i < objTitles.Count; i++)
            {
                objList = new ListItem();
                titleRcd = objTitles[i].title_rcd;
                objList.Value = titleRcd + "|" + objTitles[i].gender_type_rcd;
                objList.Text = objTitles[i].display_name;

                //Company Name
                if (bookingHeader.invoice_receiver.Length > 0)
                {
                    txtInvoiceReceiver.Value = bookingHeader.invoice_receiver;
                }

                //CIN
                if (bookingHeader.cost_center.Length > 0)
                {
                    txtCIN.Value = bookingHeader.cost_center;
                }

                //EU VAT
                if (bookingHeader.project_number.Length > 0)
                {
                    txtEUVat.Value = bookingHeader.project_number;
                }

                //TAX Id
                if (bookingHeader.tax_id.Length > 0)
                {
                    txtTIN.Value = bookingHeader.tax_id;
                }

                if (bookingHeader.title_rcd.Length == 0)
                {
                    //Set default value from web.config.
                    if (B2CSetting.DefaultTitle.ToUpper().Equals(titleRcd.ToUpper()))
                    { objList.Selected = true; }
                }
                else
                {
                    //Selected value from DB.
                    if (bookingHeader.title_rcd.Equals(titleRcd))
                    { objList.Selected = true; }
                }
                stContactTitle.Items.Add(objList);
                objList = null;
            }

            if (txtClientProfileId != null)
            {
                txtClientProfileId.Value = bookingHeader.client_profile_id.ToString();
            }

            if (bookingHeader.client_number > 0 && txtClientNumber != null)
            {
                //Lock Control
                DisableControl();
                txtClientNumber.Value = bookingHeader.client_number.ToString();
            }
            if (txtContactFirstname != null)
            {
                txtContactFirstname.Value = bookingHeader.firstname;
            }
            if (txtContactMiddlename != null)
            {
                txtContactMiddlename.Value = bookingHeader.middlename;
            }
            if (txtContactLastname != null)
            {
                txtContactLastname.Value = bookingHeader.lastname;
            }
            if (txtContactMobile != null)
            {
                txtContactMobile.Value = bookingHeader.phone_mobile;
            }
            if (txtContactHome != null)
            {
                txtContactHome.Value = bookingHeader.phone_home;
            }
            if (txtContactBusiness != null)
            {
                txtContactBusiness.Value = bookingHeader.phone_business;
            }
            if (txtContactEmail != null)
            {
                txtContactEmail.Value = bookingHeader.contact_email;
            }
            if (txtEmailConfirm != null)
            {
                txtEmailConfirm.Value = bookingHeader.contact_email;
            }
            if (txtContactEmail2 != null)
            {
                txtContactEmail2.Value = bookingHeader.contact_email_cc;
            }
            if (txtMobileEmail != null)
            {
                txtMobileEmail.Value = bookingHeader.mobile_email;
            }
            if (txtContactAddress1 != null)
            {
                txtContactAddress1.Value = bookingHeader.address_line1;
            }
            if (txtContactAddress2 != null)
            {
                txtContactAddress2.Value = bookingHeader.address_line2;
            }
            if (txtContactZip != null)
            {
                txtContactZip.Value = bookingHeader.zip_code;
            }
            if (txtContactCity != null)
            {
                txtContactCity.Value = bookingHeader.city;
            }
            if (txtContactState != null)
            {
                txtContactState.Value = bookingHeader.state;
            }

            if (chkNewsLetter != null)
            {
                chkNewsLetter.Checked = bookingHeader.newsletter_flag == 1 ? true : false;
            }

            if (stContactCountry != null)
            {
                if (string.IsNullOrEmpty(bookingHeader.country_rcd))
                {
                    //Set default country
                    bookingHeader.country_rcd = strDefaultCountry;
                }

                Countries objCountries = CacheHelper.CacheCountry();
                //Get From config first
                string[] strCountryOrder = null;
                if (!string.IsNullOrEmpty(B2CSetting.CountryOrder))
                {
                    strCountryOrder = B2CSetting.CountryOrder.ToUpper().Split(',');
                    for (int i = 0; i < strCountryOrder.Length; i++)
                    {
                        objList = new ListItem();
                        objList.Value = strCountryOrder[i].ToString();
                        for (int j = 0; j < objCountries.Count; j++)
                        {
                            if (objCountries[j].country_rcd.Equals(strCountryOrder[i].ToString().Trim()))
                            {
                                objList.Text = objCountries[j].display_name;
                            }
                        }

                        if (objList.Value == bookingHeader.country_rcd)
                        { objList.Selected = true; }
                        stContactCountry.Items.Add(objList);
                    }
                }
                //Get from DB
                for (int i = 0; i < objCountries.Count; i++)
                {
                    if (strCountryOrder == null || B2CSetting.CountryOrder.Contains(objCountries[i].country_rcd) == false)
                    {
                        objList = new ListItem();
                        objList.Value = objCountries[i].country_rcd;
                        objList.Text = objCountries[i].display_name;

                        if (objList.Value == bookingHeader.country_rcd)
                        { objList.Selected = true; }
                        stContactCountry.Items.Add(objList);
                    }
                }
            }
            if (stContactLanguage != null)
            {
                if (bookingHeader.language_rcd.Length == 0)
                {
                    //Set default Language
                    bookingHeader.language_rcd = strDefaultLanguage;
                }

                Languages objLanguage = CacheHelper.CacheLanguage();
                for (int i = 0; i < objLanguage.Count; i++)
                {
                    objList = new ListItem();
                    objList.Value = objLanguage[i].language_rcd;
                    objList.Text = objLanguage[i].display_name;

                    if (objList.Value.ToUpper() == bookingHeader.language_rcd)
                    { objList.Selected = true; }

                    stContactLanguage.Items.Add(objList);
                }
            }




        }
        private void DisableControl()
        {
            if (stContactTitle != null)
            {
                stContactTitle.Disabled = true;
            }
            if (txtContactFirstname != null)
            {
                txtContactFirstname.Disabled = true;
            }
            if (txtContactLastname != null)
            {
                txtContactLastname.Disabled = true;
            }
        }
        private void BuiltPassengerInputXml(XmlWriter xtw,
                                            Mappings mapping,
                                            Itinerary itinerary,
                                            Fees fees,
                                            string originRcd,
                                            string destinationRcd,
                                            string agencyCode,
                                            bool isOneway)
        {
            StringBuilder str = new StringBuilder();
            Library li = new Library();

            string tempXML = string.Empty;

            XmlReaderSettings setting = new XmlReaderSettings();
            setting.IgnoreWhitespace = true;

            #region Title
            xtw.WriteStartElement("Setting");
            {
                xtw.WriteStartElement("DefaultTitle");
                {
                    xtw.WriteValue(B2CSetting.DefaultTitle.ToUpper());
                }
                xtw.WriteEndElement();
                Routes rDestination = CacheHelper.CacheDestination();

                for (int i = 0; i <= rDestination.Count - 1; i++)
                {
                    if (rDestination[i].origin_rcd == originRcd & rDestination[i].destination_rcd == destinationRcd)
                    {
                        xtw.WriteStartElement("require_document_details_flag");
                        {
                            xtw.WriteValue(Convert.ToByte(rDestination[i].require_document_details_flag));
                        }
                        xtw.WriteEndElement();

                        //-------------------------------------------
                        xtw.WriteStartElement("require_date_of_birth_flag");
                        {
                            xtw.WriteValue(Convert.ToByte(rDestination[i].require_date_of_birth_flag));
                        }
                        xtw.WriteEndElement();
                        //--------------------------------
                        xtw.WriteStartElement("show_insurance_on_web_flag");
                        {
                            xtw.WriteValue(Convert.ToByte(rDestination[i].show_insurance_on_web_flag));
                        }
                        xtw.WriteEndElement();
                        break;
                    }
                }
            }
            xtw.WriteEndElement();
            #endregion

            #region Document Type
            Documents documents = CacheHelper.CacheDocumentType();
            using (StringReader srd = new StringReader(documents.GetXml()))
            {
                if (srd.Peek() > -1)
                {
                    using (XmlReader reader = XmlReader.Create(srd, setting))
                    {
                        while (!reader.EOF)
                        {
                            if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("DocumentType"))
                            {
                                xtw.WriteNode(reader, false);
                            }
                            else
                            {
                                reader.Read();
                            }
                        }
                    }
                }
            }
            #endregion

            #region Get passenger title
            Titles objTitles = CacheHelper.CachePassengerTitle();
            using (StringReader srd = new StringReader(objTitles.GetXml()))
            {
                if (srd.Peek() > -1)
                {
                    using (XmlReader reader = XmlReader.Create(srd, setting))
                    {
                        while (!reader.EOF)
                        {
                            if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("PassengerTitels"))
                            {
                                xtw.WriteNode(reader, false);
                            }
                            else
                            {
                                reader.Read();
                            }

                        }
                    }
                }
            }
            #endregion

            #region Get Country
            Countries objCountries = CacheHelper.CacheCountry();
            using (StringReader srd = new StringReader(objCountries.GetXml()))
            {
                if (srd.Peek() > -1)
                {
                    using (XmlReader reader = XmlReader.Create(srd, setting))
                    {
                        while (!reader.EOF)
                        {
                            if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("Countrys"))
                            {
                                xtw.WriteNode(reader, false);
                            }
                            else
                            {
                                reader.Read();
                            }
                        }
                    }
                }
            }
            #endregion

            #region Baggage Fee
            if (B2CSetting.UseBaggageCalculation == "1")
            {
                xtw.WriteStartElement("BaggageFees");
                {
                    Classes.Helper objHp = new Classes.Helper();
                    xtw.WriteStartElement("Outward");
                    {
                        //Departure Baggage Fees.
                        Guid gSegmentId = Guid.Empty;
                        for (int i = 0; i < itinerary.Count; i++)
                        {
                            if (originRcd.Equals(itinerary[i].od_origin_rcd) & destinationRcd.Equals(itinerary[i].od_destination_rcd))
                            {
                                gSegmentId = itinerary[i].booking_segment_id;
                                break;
                            }
                        }

                        //Get Piece allowance
                        objHp.FindPieceAllowance(ref xtw, mapping, gSegmentId);

                        if (gSegmentId.Equals(Guid.Empty) == false)
                        {
                            Fees objOutWardFee;
                            if (B2CSession.BaggageFeeDepart == null)
                            {
                                objOutWardFee = new Fees();
                                B2CSession.BaggageFeeDepart = objOutWardFee;
                            }
                            else
                            {
                                objOutWardFee = B2CSession.BaggageFeeDepart;
                                if (objOutWardFee != null)
                                {
                                    objOutWardFee.Clear();
                                }
                            }

                            objOutWardFee.objService = B2CSession.AgentService;
                            objOutWardFee.GetBaggageFeeOptions(mapping,
                                                                gSegmentId,
                                                                Guid.Empty,
                                                                agencyCode,
                                                                tikAeroB2C.Classes.Language.CurrentCode().ToUpper(),
                                                                0,
                                                                null,
                                                                false,
                                                                false);
                            objOutWardFee.objService = null;

                            if (objOutWardFee != null && objOutWardFee.Count > 0)
                            {
                                //Find Selected fee.
                                int j;
                                for (j = 0; j < fees.Count; j++)
                                {
                                    if (fees[j].booking_segment_id.Equals(gSegmentId) &
                                        fees[j].passenger_id.Equals(mapping[0].passenger_id) &
                                        (fees[j].fee_category_rcd != null && fees[j].fee_category_rcd.Equals("BAGSALES")))
                                    {
                                        break;
                                    }
                                }

                                for (int i = 0; i < objOutWardFee.Count; i++)
                                {
                                    xtw.WriteStartElement("Fee");
                                    {
                                        xtw.WriteStartElement("fee_id");
                                        {
                                            xtw.WriteValue(objOutWardFee[i].fee_id.ToString());
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("booking_segment_id");
                                        {
                                            xtw.WriteValue(objOutWardFee[i].booking_segment_id.ToString());
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("fee_rcd");
                                        {
                                            xtw.WriteValue(objOutWardFee[i].fee_rcd);
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("currency_rcd");
                                        {
                                            xtw.WriteValue(objOutWardFee[i].currency_rcd);
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("display_name");
                                        {
                                            xtw.WriteValue(objOutWardFee[i].display_name);
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("number_of_units");
                                        {
                                            xtw.WriteValue(objOutWardFee[i].number_of_units.ToString());
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("total_amount");
                                        {
                                            xtw.WriteValue(objOutWardFee[i].total_amount.ToString());
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("total_amount_incl");
                                        {
                                            xtw.WriteValue(objOutWardFee[i].total_amount_incl.ToString());
                                        }
                                        xtw.WriteEndElement();
                                        xtw.WriteStartElement("selected");
                                        {
                                            if (fees.Count > 0 &&
                                                fees.Count != j &&
                                                objOutWardFee[i].passenger_id.Equals(fees[j].passenger_id) &&
                                                objOutWardFee[i].booking_segment_id.Equals(fees[j].booking_segment_id) &&
                                                objOutWardFee[i].fee_category_rcd.Equals(fees[j].fee_category_rcd) &&
                                                objOutWardFee[i].number_of_units.Equals(fees[j].number_of_units))
                                            {
                                                xtw.WriteValue(true);
                                            }
                                            else
                                            {
                                                xtw.WriteValue(false);
                                            }
                                        }
                                        xtw.WriteEndElement();
                                    }
                                    xtw.WriteEndElement();
                                }
                            }
                        }
                    }
                    xtw.WriteEndElement();
                    if (isOneway == false)
                    {
                        xtw.WriteStartElement("Return");
                        {
                            //Return Baggage Fees.
                            Guid gSegmentId = Guid.Empty;
                            for (int i = 0; i < itinerary.Count; i++)
                            {
                                if (originRcd.Equals(itinerary[i].od_destination_rcd) & destinationRcd.Equals(itinerary[i].od_origin_rcd))
                                {
                                    gSegmentId = itinerary[i].booking_segment_id;
                                    break;
                                }
                            }

                            //Get Piece allowance
                            objHp.FindPieceAllowance(ref xtw, mapping, gSegmentId);

                            if (gSegmentId.Equals(Guid.Empty) == false)
                            {
                                Fees objReturnFee;
                                if (B2CSession.BaggageFeeReturn == null)
                                {
                                    objReturnFee = new Fees();
                                    B2CSession.BaggageFeeReturn = objReturnFee;
                                }
                                else
                                {
                                    objReturnFee = B2CSession.BaggageFeeReturn;
                                    objReturnFee.Clear();
                                }
                                objReturnFee.objService = B2CSession.AgentService;
                                objReturnFee.GetBaggageFeeOptions(mapping,
                                                                      gSegmentId,
                                                                      Guid.Empty,
                                                                      agencyCode,
                                                                      tikAeroB2C.Classes.Language.CurrentCode().ToUpper(),
                                                                      0,
                                                                      null,
                                                                      false,
                                                                      false);
                                objReturnFee.objService = null;
                                if (objReturnFee != null && objReturnFee.Count > 0)
                                {
                                    //Find Selected fee.
                                    int j;
                                    for (j = 0; j < fees.Count; j++)
                                    {
                                        if (fees[j].booking_segment_id.Equals(gSegmentId) &
                                            fees[j].passenger_id.Equals(mapping[0].passenger_id) &
                                            (fees[j].fee_category_rcd != null && fees[j].fee_category_rcd.Equals("BAGSALES")))
                                        {
                                            break;
                                        }
                                    }

                                    for (int i = 0; i < objReturnFee.Count; i++)
                                    {
                                        xtw.WriteStartElement("Fee");
                                        {
                                            xtw.WriteStartElement("fee_id");
                                            {
                                                xtw.WriteValue(objReturnFee[i].fee_id.ToString());
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("booking_segment_id");
                                            {
                                                xtw.WriteValue(objReturnFee[i].booking_segment_id.ToString());
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("fee_rcd");
                                            {
                                                xtw.WriteValue(objReturnFee[i].fee_rcd);
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("currency_rcd");
                                            {
                                                xtw.WriteValue(objReturnFee[i].currency_rcd);
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("display_name");
                                            {
                                                xtw.WriteValue(objReturnFee[i].display_name);
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("number_of_units");
                                            {
                                                xtw.WriteValue(objReturnFee[i].number_of_units.ToString());
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("total_amount");
                                            {
                                                xtw.WriteValue(objReturnFee[i].total_amount.ToString());
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("total_amount_incl");
                                            {
                                                xtw.WriteValue(objReturnFee[i].total_amount_incl.ToString());
                                            }
                                            xtw.WriteEndElement();
                                            xtw.WriteStartElement("selected");
                                            {
                                                if (fees.Count > 0 &&
                                                    fees.Count != j &&
                                                    objReturnFee[i].passenger_id.Equals(fees[j].passenger_id) &&
                                                    objReturnFee[i].booking_segment_id.Equals(fees[j].booking_segment_id) &&
                                                    objReturnFee[i].fee_category_rcd.Equals(fees[j].fee_category_rcd) &&
                                                    objReturnFee[i].number_of_units.Equals(fees[j].number_of_units))
                                                {
                                                    xtw.WriteValue(true);
                                                }
                                                else
                                                {
                                                    xtw.WriteValue(false);
                                                }
                                            }
                                            xtw.WriteEndElement();
                                        }
                                        xtw.WriteEndElement();
                                    }
                                }

                            }
                        }
                        xtw.WriteEndElement();
                    }
                }
                xtw.WriteEndElement();
            }
            #endregion

        }
        private void BuiltFlightInputXml(XmlWriter xtw, Mappings mappings, BookingHeader bookingHeader, Guid passengerId)
        {
            Library li = new Library();
            Fees objFees = new Fees();
            Agents objAgents = B2CSession.Agents;
            Routes routesDes = CacheHelper.CacheDestination();
            B2CVariable objUv = B2CSession.Variable;

            xtw.WriteStartElement("Setting");
            {
                xtw.WriteStartElement("selected_passenger_id");
                {
                    xtw.WriteValue(passengerId.ToString());
                }
                xtw.WriteEndElement();

                xtw.WriteStartElement("b2b_allow_seat_assignment_flag");
                {
                    xtw.WriteValue(objAgents[0].b2b_allow_seat_assignment_flag.ToString());
                }
                xtw.WriteEndElement();

                xtw.WriteStartElement("b2b_allow_service_flag");
                {
                    xtw.WriteValue(objAgents[0].b2b_allow_service_flag.ToString());
                }
                xtw.WriteEndElement();
            }
            xtw.WriteEndElement();

            // Show or Not show SSR from agency && route
            #region Show or Not show SSR from agency && route

            //show ssr
            //Get SSR Information
            if (objAgents[0].b2b_allow_service_flag == 1)
            {
                Route route = routesDes.GetRoute(objUv.OriginRcd, objUv.DestinationRcd);
                if (route != null)
                {
                    if (route.special_service_fee_flag == true)
                    {
                        string strSsrGroup = B2CSetting.SsrGroup;
                        if (string.IsNullOrEmpty(strSsrGroup) == false)
                        {
                            objFees.objService = B2CSession.AgentService;
                            Services services = CacheHelper.CacheSpecialServiceRef();
                            using (StringReader sr = new StringReader(objFees.SegmentFee(objAgents[0], routesDes, bookingHeader, mappings, strSsrGroup.Split(','), services, 0, 0, tikAeroB2C.Classes.Language.CurrentCode().ToUpper(), true, false)))
                            {
                                if (sr.Peek() > -1)
                                {
                                    XmlReaderSettings setting = new XmlReaderSettings();
                                    setting.IgnoreWhitespace = true;
                                    using (XmlReader reader = XmlReader.Create(sr, setting))
                                    {
                                        while (!reader.EOF)
                                        {
                                            if (reader.NodeType == XmlNodeType.Element)
                                            { xtw.WriteNode(reader, false); }
                                            else
                                            { reader.Read(); }
                                        }
                                    }

                                }
                            }
                            objFees.objService = null;
                        }
                    }
                }
            }

            #endregion

        }
        private void FillPassengerDefaultValue(Passengers passengers, Client client, Agent agent)
        {
            bool bFilled = false;
            if (passengers != null && client != null)
            {
                Clients objClient = new Clients();
                objClient.objService = B2CSession.AgentService;

                DataSet ds = objClient.GetClientPassenger(B2CSession.BookingHeader.booking_id.ToString(), string.Empty, client.client_number.ToString());
                DataRow[] rows = ds.Tables[0].Select("passenger_role_rcd = 'MYSELF'");

                Passenger pax = new Passenger();
                DataHelper.ConvertDataRowtoObject(rows[0], pax);


                foreach (Passenger p in passengers)
                {
                    if (string.IsNullOrEmpty(p.nationality_rcd))
                        p.nationality_rcd = agent.country_rcd;

                    if (string.IsNullOrEmpty(p.passport_issue_country_rcd))
                        p.passport_issue_country_rcd = agent.country_rcd;

                    if (bFilled == false && p.client_number == 0 && client.client_number > 0 &&
                        p.lastname.Length == 0 && p.passenger_type_rcd.Equals(pax.passenger_type_rcd))
                    {
                        p.client_number = client.client_number;

                        if (string.IsNullOrEmpty(p.title_rcd))
                        {
                            p.title_rcd = pax.title_rcd;
                            p.gender_type_rcd = pax.gender_type_rcd;
                        }
                        if (string.IsNullOrEmpty(p.firstname))
                            p.firstname = pax.firstname;

                        if (string.IsNullOrEmpty(p.middlename))
                            p.middlename = pax.middlename;

                        if (string.IsNullOrEmpty(p.lastname))
                            p.lastname = pax.lastname;

                        if (p.date_of_birth == DateTime.MinValue)
                            p.date_of_birth = pax.date_of_birth;

                        if (p.client_profile_id == Guid.Empty)
                            p.client_profile_id = client.client_profile_id;

                        if (p.passenger_profile_id == Guid.Empty)
                            p.passenger_profile_id = client.passenger_profile_id;

                        if (p.vip_flag == 0)
                            p.vip_flag = pax.vip_flag;

                        if (string.IsNullOrEmpty(p.nationality_rcd) == true)
                            p.nationality_rcd = pax.nationality_rcd;

                        if (string.IsNullOrEmpty(p.passport_issue_country_rcd) == true)
                            p.passport_issue_country_rcd = pax.passport_issue_country_rcd;

                        if (string.IsNullOrEmpty(p.document_type_rcd) == true)
                            p.document_type_rcd = pax.document_type_rcd;

                        if (string.IsNullOrEmpty(p.passport_number) == true)
                            p.passport_number = pax.passport_number;

                        if (string.IsNullOrEmpty(p.passport_issue_place) == true)
                            p.passport_issue_place = pax.passport_issue_place;

                        if (p.passport_issue_date == DateTime.MinValue)
                            p.passport_issue_date = pax.passport_issue_date;

                        if (p.passport_expiry_date == DateTime.MinValue)
                            p.passport_expiry_date = pax.passport_expiry_date;

                        if (p.passenger_weight == 0)
                            p.passenger_weight = pax.passenger_weight;

                        if (string.IsNullOrEmpty(p.member_level_rcd) == true)
                            p.member_level_rcd = pax.member_level_rcd;

                        if (string.IsNullOrEmpty(p.passport_birth_place) == true)
                            p.passport_birth_place = pax.passport_birth_place;

                        if (string.IsNullOrEmpty(p.contact_email) == true)
                            p.contact_email = client.contact_email;

                        if (string.IsNullOrEmpty(p.mobile_email) == true)
                            p.mobile_email = client.mobile_email;

                        bFilled = true;
                    }

                }
            }
        }
    }
}