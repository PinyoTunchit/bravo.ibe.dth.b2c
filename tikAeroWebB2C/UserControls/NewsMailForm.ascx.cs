using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace tikAeroB2C.UserControls
{
    public partial class NewsMailForm : System.Web.UI.UserControl
    {
        public System.Collections.Specialized.StringDictionary _stdLanguage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                try
                {
                    //Load Language Dictionary
                    _stdLanguage = tikAeroB2C.Classes.Language.GetLanguageDictionary();

                    if (B2CSession.SenderEmailFrom != null) txtMailFrom.Value = B2CSession.SenderEmailFrom;
                    if (B2CSession.SenderEmailTo != null) txtMailTo.Value = B2CSession.SenderEmailTo;
                    if (B2CSession.SearchRegister != null)
                    {
                        DataTable dtMail = B2CSession.SearchRegister;
                        if (dtMail.Rows.Count > 1)
                            lblBcc.InnerText = dtMail.Rows.Count.ToString() + " Receivers";
                        else
                            lblBcc.InnerText = dtMail.Rows.Count.ToString() + " Receiver";
                    }
                }
                catch (Exception ex)
                {
                    Classes.Helper objHp = new Classes.Helper();
                    objHp.SendErrorEmail(ex, string.Empty);
                    throw ex;
                }
                
            }
        }
    }
}