using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;
using System.Collections.Specialized;
using System.Xml;
using System.Net;
using System.Diagnostics;
namespace tikAeroB2C
{
    public partial class _Default : System.Web.UI.Page
    {
        protected string ResultType = String.Empty;
        protected string CalendarToControls = String.Empty;
        protected string DateFormat = String.Empty;
        public System.Collections.Specialized.StringDictionary _stdLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper objHelper = new Helper();
            try
            {
                //Set output Caching
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.AppendCacheExtension("no-store, must-revalidate");
                Response.AppendHeader("Pragma", "no-cache");
                Response.AppendHeader("Expires", "0");

                if (Page.Request["langculture"] != null)
                {
                    B2CSession.LanCode = Page.Request["langculture"];
                }
                else
                {
                    if (Page.Request["hdLang"] != null)
                    {
                        B2CSession.LanCode = Page.Request["hdLang"];
                    }
                }

                B2CVariable objUv = B2CSession.Variable;
                Library objLi = new Library();

                if (objUv == null || objHelper.SessionTimeout() == true)
                {
                    tikAeroB2C.WebService.B2cService objService = new tikAeroB2C.WebService.B2cService();
                    objService.ClearSession();
                    B2CSession.Variable = null;
                    objUv = new B2CVariable();
                    objUv.CurrentStep = 1;
                    B2CSession.Variable = objUv;
                }

                if (!string.IsNullOrEmpty(B2CSetting.DefaultAgencyCode) && string.IsNullOrEmpty(objUv.Agency_Code))
                {
                    objUv.Agency_Code = B2CSetting.DefaultAgencyCode;
                }

                //Read Agnecy from Cache..
                if (string.IsNullOrEmpty(objUv.Agency_Code) == false)
                {
                    //Set Default user information.
                    Agent objAgent = CacheHelper.CacheAgency(objUv.Agency_Code);
                    if (objAgent != null && objAgent.default_user_account_id != Guid.Empty)
                    {
                        objUv.UserId = objAgent.default_user_account_id;
                    }
                    else
                    {
                        if (B2CSetting.UserId != Guid.Empty)
                        {
                            objUv.UserId = B2CSetting.UserId;
                        }
                    }
                }

                objHelper.LanguageInitialize();
                //Load Language Dictionary
                _stdLanguage = Classes.Language.GetLanguageDictionary();

                if (!string.IsNullOrEmpty(Request.Form["result"]) && !string.IsNullOrEmpty(Request.Form["bookingID"]))//Post result from payment gate way
                {
                    BookingHeader bookingHeader = new BookingHeader();
                    Itinerary itinerary = new Itinerary();
                    Passengers passengers = new Passengers();
                    Quotes quotes = new Quotes();
                    Fees fees = new Fees();
                    Mappings mappings = new Mappings();
                    Services services = new Services();
                    Remarks remarks = new Remarks();
                    Payments payments = new Payments();
                    Taxes taxes = new Taxes();

                    string strGetBooking = string.Empty;
                    Guid booking_id = !string.IsNullOrEmpty(Request.Form["bookingID"]) ? new Guid(Request.Form["bookingID"]) : Guid.Empty;
                    objHelper.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;
                    ServiceClient obj = new ServiceClient();
                    obj.objService = B2CSession.AgentService;
                    strGetBooking = obj.GetBooking(booking_id);
                    objLi.FillBooking(strGetBooking,
                                      ref bookingHeader,
                                      ref passengers,
                                      ref itinerary,
                                      ref mappings,
                                      ref payments,
                                      ref remarks,
                                      ref taxes,
                                      ref quotes,
                                      ref fees,
                                      ref services);
                    B2CSession.BookingHeader = bookingHeader;
                    B2CSession.Itinerary = itinerary;
                    B2CSession.Passengers = passengers;
                    B2CSession.Quotes = quotes;
                    B2CSession.Fees = fees;
                    B2CSession.Mappings = mappings;
                    B2CSession.Services = services;
                    B2CSession.Remarks = remarks;
                    B2CSession.Payments = payments;
                    B2CSession.Taxes = taxes;

                    if (Request.Form["result"].ToUpper() == "SUCCESS")
                    {
                        objUv.CurrentStep = 10;
                        objHelper.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);
                        objUv.GatewayRepeat = 0;
                    }
                    else
                        objUv.CurrentStep = 5;
                    B2CSession.Variable = objUv;
                }

                if (IsPostBack == false || B2CSession.LanCode != null)
                {
                    if (!string.IsNullOrEmpty(B2CSetting.OutputCalendarControl))
                        ResultType = B2CSetting.OutputCalendarControl;
                    if (!string.IsNullOrEmpty(B2CSetting.CalendarToControls))
                        CalendarToControls = B2CSetting.CalendarToControls;
                    if (!string.IsNullOrEmpty(B2CSetting.DateFormat))
                        DateFormat = B2CSetting.DateFormat;

                    RedirectUrl();

                    //Validate cookies
                    HttpCookie myCookie = new HttpCookie("logAgency");
                    myCookie = Request.Cookies["logAgency"];
                    if (myCookie != null && string.IsNullOrEmpty(myCookie.Value) == false)
                    {
                        objUv.Agency_Code = objHelper.DecodeDataBase64(Server.UrlDecode(myCookie.Value));
                    }
                    else if (Page.Request["ao"] != null && string.IsNullOrEmpty(Page.Request["ao"]) == false)
                    {
                        objUv.Agency_Code = Page.Request["ao"].ToUpper();
                    }

                    //Set CurrentStep for direct to client profile registration 
                    if (Request.QueryString["cr"] == "l")
                    {
                        objUv.CurrentStep = 13;
                    }
                    //Check to not allowd to goto other step if session agency is different from web service agency.
                    Agents objAgents = B2CSession.Agents;
                    if (B2CSession.AgentService != null && objAgents != null && objUv.Agency_Code != objAgents[0].agency_code && objUv.CurrentStep > 2)
                    {
                        objUv.CurrentStep = 1;
                        objAgents = null;
                    }

                    //Write Language to page
                    string noFlight = Classes.Language.Value("No Flight", "No Flight", _stdLanguage);
                    string operateFlight = Classes.Language.Value("Operate Flight", "Operate Flight", _stdLanguage);
                    string strJson = "<script type=\"text/javascript\">" +
                                            "SetLanguage(\'{" + objHelper.GetLanguagePageScript(_stdLanguage) +
                                            ",\"NoFlight\" : \"" + noFlight + "\",\"OperateFlight\" : \"" + operateFlight + "\"" + "}');" +
                                         "</script>";
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "languageScript", strJson);

                    //Render Menu
                    dvMenu.InnerHtml = objLi.GenerateControlString("UserControls/menu.ascx", string.Empty);

                    if (objUv.CurrentStep == 1)
                    {
                        try
                        {
                            //Check if this is come from payment gateway redirect.
                            if (Request["TRANSIDMERCHANT"] != null && Request["AMOUNT"] != null && Request["SESSIONID"] != null && Request["STATUSCODE"] != null)
                            {
                                //For IBBRI
                                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("DokuPaymentSetting");
                                if (Setting["subFOP"].ToString() == "IBBRI")
                                {
                                    string strPaymentResult = DokuPaymentResponse(objUv, null, null, null, null, null, null, null, null, null, null, null, null);
                                    if (strPaymentResult.Length > 0)
                                    {
                                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/BookingSummary.ascx", strPaymentResult);
                                        objUv.CurrentStep = 10;
                                        B2CSession.Variable = objUv;
                                    }
                                    else dvContainer.InnerHtml = "Payment result is empty.";
                                }
                            }
                            else
                            {
                                objUv.PaymentGateway = string.Empty;
                                //Load home page user control
                                dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/home.ascx", string.Empty);
                            }
                        }
                        catch (Exception ex)
                        {
                            dvContainer.InnerHtml = string.Format("{0}", ex.Message);
                        }

                    }
                    else if (objUv.CurrentStep == 2)
                    {
                        //Load availability page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/Availability.ascx", string.Empty);
                    }
                    else if (objUv.CurrentStep == 3)
                    {
                        //Load FlightSummary page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/FlightSummary.ascx", string.Empty);
                    }
                    else if (objUv.CurrentStep == 4)
                    {
                        //Load PassengerDetail page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/PassengerDetail.ascx", string.Empty);
                    }
                    else if (objUv.CurrentStep == 5)
                    {
                        BookingHeader bookingHeader = B2CSession.BookingHeader;
                        Itinerary itinerary = B2CSession.Itinerary;
                        Passengers passengers = B2CSession.Passengers;
                        Quotes quotes = B2CSession.Quotes;
                        Fees fees = B2CSession.Fees;
                        Mappings mappings = B2CSession.Mappings;
                        Services services = B2CSession.Services;
                        Remarks remarks = B2CSession.Remarks;
                        Payments payments = B2CSession.Payments;
                        Taxes taxes = B2CSession.Taxes;

                        Payments objPayment = B2CSession.CcPayment;
                        Fees objPaymentFees = B2CSession.PaymentFees;
                        if (objPayment != null)
                        {
                            string result = string.Empty;
                            objPayment.objService = B2CSession.AgentService;
                            result = PaymentResponse(objUv,
                                                     bookingHeader,
                                                     itinerary,
                                                     passengers,
                                                     quotes,
                                                     fees,
                                                     objPaymentFees,
                                                     mappings,
                                                     services,
                                                     remarks,
                                                     payments,
                                                     objPayment,
                                                     taxes);
                            objPayment.objService = null;
                            if (result.Length > 0 && result != "{700}")
                            {
                                //call Ace insurance : policy request
                                try
                                {
                                    bool IsACEsuccess = false;
                                    if (fees != null && fees.Count > 0)
                                    {
                                        bool IsFoundACEFee = false;

                                        for (int i = 0; i < fees.Count; i++)
                                        {
                                            if (fees[i].fee_rcd.Equals("INSU"))
                                            {
                                                IsFoundACEFee = true;
                                                break;
                                            }
                                        }

                                        if (IsFoundACEFee)
                                        {
                                            tikAeroB2C.WebService.B2cService objService = new tikAeroB2C.WebService.B2cService();
                                            IsACEsuccess = objService.AcePolicyRequest(bookingHeader, itinerary, objService.CreateACEPassenger(), fees, payments);
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }

                                dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/BookingSummary.ascx", result);
                                objUv.CurrentStep = 10;
                                B2CSession.Variable = objUv;
                            }
                            else
                            {
                                //Load PassengerDetail page control when refresh 
                                dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/Payment.ascx", string.Empty);
                            }
                        }
                        else
                        {
                            //Load PassengerDetail page control when refresh 
                            dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/Payment.ascx", string.Empty);
                            if (!string.IsNullOrEmpty(Request.Form["result"]) && Request.Form["result"].ToUpper() == "ERROR")
                            {
                                StringBuilder html = new StringBuilder();
                                html.Append("<input id='errorCode' type='hidden' value='" + Request.Form["errorCode"] + "'/>");
                                html.Append("<input id='errorResponse' type='hidden' value='" + Request.Form["errorResponse"].Replace("'", "&#39;") + "'/>");
                                dvContainer.InnerHtml += html.ToString();
                            }
                        }
                        //Remove Clear session check
                        Session.Remove("bHttps");
                        Session.Remove("CcPayment");
                        Session.Remove("PaymentFees");
                    }
                    else if (objUv.CurrentStep == 7)
                    {
                        //Load PassengerDetail page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/MyBooking.ascx", string.Empty);
                        //Remove Clear session check
                        Session.Remove("bHttps");
                    }
                    else if (objUv.CurrentStep == 6)
                    {
                        string strUrl = B2CSetting.CobUrl;
                        Uri cUri = Request.Url;
                        Uri uri;

                        if (Uri.TryCreate(strUrl, UriKind.Relative, out uri))
                        {
                            uri = new Uri(string.Format("{0}://{1}{2}", cUri.Scheme, cUri.Host, strUrl.StartsWith("/") ? strUrl : "/" + strUrl));
                            strUrl = string.Format("{0}", uri.AbsolutePath);
                        }
                        else if (Uri.TryCreate(strUrl, UriKind.Absolute, out uri))
                        {

                        }
                        else
                        {
                            strUrl = string.Format("{0}", "/COB/default.aspx");
                        }

                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/Cob.ascx", strUrl + "?id=||" +
                                                                                                      objUv.UserId.ToString() + "|" +
                                                                                                      B2CSetting.DefaultAgencyCode + "|" +
                                                                                                      B2CSession.LanCode + "|" +
                                                                                                      "0");
                        Session.Remove("bHttps");
                    }
                    else if (objUv.CurrentStep == 8)
                    {
                        //Load PassengerDetail page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/MilleageDetail.ascx", string.Empty);
                    }
                    else if (objUv.CurrentStep == 9)
                    {
                        //Load PassengerDetail page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/ForgetPassword.ascx", string.Empty);
                    }
                    else if (objUv.CurrentStep == 10)
                    {
                        //Load PassengerDetail page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/BookingSummary.ascx", string.Empty);
                    }
                    else if (objUv.CurrentStep == 11)
                    {
                        //Load PassengerDetail page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/SeatMap.ascx", string.Empty);
                    }
                    else if (objUv.CurrentStep == 12)
                    {
                        //Load PassengerDetail page control when refresh 
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/LowFareFinder.ascx", string.Empty);
                    }
                    else if (objUv.CurrentStep == 13)
                    {
                        //Load BookingSummary page control when refresh for client profile only
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/BookingSummary.ascx", string.Empty);
                    }

                    getOriginDestination();
                    GetBinRange();
                }
            }
            catch (System.Threading.ThreadAbortException tab)
            { }
            catch (Exception ex)
            {
                objHelper.SendErrorEmail(ex, string.Empty);
            }

        }
        private void getOriginDestination()
        {

            Routes routes = CacheHelper.CacheDestination();

            //Load Destination Airport to Javascript Array
            StringBuilder sbRoute = new StringBuilder();
            for (int i = 0; i < routes.Count; i++)
            {
                if (routes[i].b2c_flag == true)
                {
                    sbRoute.Append("\"" + routes[i].destination_rcd + "|" +
                                        routes[i].display_name + "|" +
                                        routes[i].origin_rcd + "|" +
                                        routes[i].day_range + "|" +
                                        routes[i].currency_rcd + "\",");

                }
            }
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scDestination", "var arrDestination = new Array(" + sbRoute.ToString().TrimEnd(',') + ");", true);
        }
        private bool RedirectUrl()
        {
            string strUrl = string.Empty;
            Uri url = Request.Url;
            bool isWWW = B2CSetting.UseForceWWW;

            string scheme = url.Scheme.ToLower();
            int step = B2CSession.Variable.CurrentStep;
            int https = B2CSetting.HttpsAtStep;

            if (step >= https && https > 0 && scheme == "http")
            {
                strUrl = string.Format("https{0}", url.AbsoluteUri.Substring(4));
            }
            else if (step < https && scheme == "https")
            {
                strUrl = string.Format("http{0}", url.AbsoluteUri.Substring(5));
            }

            if (isWWW)
            {
                if (!url.Host.ToLower().StartsWith("www.") && !HttpHelper.IsLocalhost(url.Host) && !HttpHelper.IsIpAddress(url.Host))
                {
                    strUrl = url.AbsoluteUri.Replace("://", "://www.");
                }
            }
            if (strUrl.Length > 0)
            {
                Response.Redirect(strUrl, true);
            }
            return false;
        }
        private void GetBinRange()
        {
            StringBuilder stbBinRange = new StringBuilder();
            Payments objPayments = new Payments();

            objPayments.objService = B2CSession.AgentService;

            DataSet ds = objPayments.GetBinRangeSearch(string.Empty, "A", SecurityHelper.GenerateSessionlessToken());

            if (ds != null && ds.Tables.Count > 0)
            {
                DataRow[] dr = ds.Tables[0].Select();
                stbBinRange.Append("var cards = new Array();\n");
                for (int i = 0; i < dr.Length; i++)
                {
                    stbBinRange.Append("cards [" + i + "] = {name: \"" + dr[i]["form_of_payment_subtype_rcd"].ToString() + "\",");
                    stbBinRange.Append("length: \"" + dr[i]["length_from"].ToString() + ", " + dr[i]["length_to"].ToString() + "\",");
                    stbBinRange.Append("prefixes: \"" + dr[i]["range_from"].ToString() + "-" + dr[i]["range_to"].ToString() + "\",");
                    stbBinRange.Append("checkdigit: true};");
                }
            }

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "BinRange", "<script language=\"javascript\" type=\"text/javascript\">" + stbBinRange.ToString() + "</script>");
        }
        // Start edit for enable call from outside (aspx)
        //private string GetHtml(string strUrl)
        public string GetHtml(string strUrl)
        // End edit for enable call from outside (aspx)
        {
            try
            {
                if (!string.IsNullOrEmpty(strUrl))
                {
                    string content;

                    System.Net.WebRequest webRequest = System.Net.WebRequest.Create(strUrl);
                    System.Net.WebResponse webResponse = webRequest.GetResponse();

                    System.IO.StreamReader sr = new StreamReader(webResponse.GetResponseStream(), System.Text.Encoding.GetEncoding("UTF-8"));

                    content = sr.ReadToEnd();
                    webResponse.Close();

                    //display it in this page
                    sr.Close();
                    sr.Dispose();
                    return content;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }
        #region Web CC payment Response
        private string PaymentResponse(B2CVariable objUv,
                                       BookingHeader bookingHeader,
                                       Itinerary itinerary,
                                       Passengers passengers,
                                       Quotes quotes,
                                       Fees fees,
                                       Fees objPaymentFees,
                                       Mappings mappings,
                                       Services services,
                                       Remarks remarks,
                                       Payments payments,
                                       Payments ccPayment,
                                       Taxes taxes)
        {
            if (objUv.PaymentGateway == "EQP")
            { return EquipayPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "JCC")
            { return JCCPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "VISA")
            { return VisaPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "ETZ")
            { return ETranzactPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "ITS")
            { return InterswitchPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "OGN")
            { return Ogone3DPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "Tatra")
            { return TatraPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "OGNECM")
            { return OgoneECommercePaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "DOKU" || objUv.PaymentGateway == "IBBRI")
            { return DokuPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }
            else if (objUv.PaymentGateway == "SOFORT")
            {
                return this.SofortPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes);
            }
            //else if (objUv.PaymentGateway == "BBL")
            //{ return BBLPaymentResponse(objUv, bookingHeader, itinerary, passengers, quotes, fees, objPaymentFees, mappings, services, remarks, payments, ccPayment, taxes); }

            else
            { return string.Empty; }
        }
        private string EquipayPaymentResponse(B2CVariable objUv,
                                              BookingHeader bookingHeader,
                                              Itinerary itinerary,
                                              Passengers passengers,
                                              Quotes quotes,
                                              Fees fees,
                                              Fees objPaymentFees,
                                              Mappings mappings,
                                              Services services,
                                              Remarks remarks,
                                              Payments payments,
                                              Payments ccPayment,
                                              Taxes taxes)
        {
            if (Request["Ref"] != null)
            {
                if (objUv.PaymentReference.Equals(Request["Ref"]))
                {
                    if (ccPayment != null)
                    {
                        string result = string.Empty;
                        //ccPayment[0].transaction_reference = Request.Form["PayRef"];
                        //ccPayment[0].approval_code = Request.Form["AuthId"];
                        return ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                                  ref itinerary,
                                                                  ref passengers,
                                                                  ref quotes,
                                                                  ref fees,
                                                                  ref objPaymentFees,
                                                                  ref mappings,
                                                                  ref services,
                                                                  ref remarks,
                                                                  ref payments,
                                                                  ref taxes,
                                                                  Classes.Language.CurrentCode().ToUpper());
                    }
                }
            }
            return string.Empty;
        }
        private string JCCPaymentResponse(B2CVariable objUv,
                                          BookingHeader bookingHeader,
                                          Itinerary itinerary,
                                          Passengers passengers,
                                          Quotes quotes,
                                          Fees fees,
                                          Fees objPaymentFees,
                                          Mappings mappings,
                                          Services services,
                                          Remarks remarks,
                                          Payments payments,
                                          Payments ccPayment,
                                          Taxes taxes)
        {

            if (Request.Form["ResponseCode"] != null)
            {
                if (objUv.PaymentReference.Equals(Request.Form["OrderID"]))
                {
                    if (Convert.ToInt16(Request.Form["ResponseCode"]) == 1)
                    {
                        if (ccPayment != null)
                        {
                            string result = string.Empty;
                            ccPayment[0].transaction_reference = Request.Form["ReferenceNo"];
                            ccPayment[0].approval_code = Request.Form["AuthCode"];

                            return ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                                      ref itinerary,
                                                                      ref passengers,
                                                                      ref quotes,
                                                                      ref fees,
                                                                      ref objPaymentFees,
                                                                      ref mappings,
                                                                      ref services,
                                                                      ref remarks,
                                                                      ref payments,
                                                                      ref taxes,
                                                                      Classes.Language.CurrentCode().ToUpper());
                        }
                    }
                }
            }
            return string.Empty;
        }

        #region VisaPaymentResponse
        private string VisaPaymentResponse(B2CVariable objUv,
                                            BookingHeader bookingHeader,
                                            Itinerary itinerary,
                                            Passengers passengers,
                                            Quotes quotes,
                                            Fees fees,
                                            Fees objPaymentFees,
                                            Mappings mappings,
                                            Services services,
                                            Remarks remarks,
                                            Payments payments,
                                            Payments ccPayment,
                                            Taxes taxes)
        {
            string ResponseCode = Convert.ToString(Request.Form["ResponseCode"]);

            string strResult = string.Empty;
            string PaymentState = "DECLINED";
            string ResponseText = "";

            //Request.Form["TransactionType"]
            string trans_desc = itinerary[0].airline_rcd + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date.ToString("dd MMM yyyy") + " " + itinerary[0].departure_time.ToString("00:00");
            if (itinerary.Count > 1)
                trans_desc += " :: " +
                                itinerary[1].airline_rcd + " " + itinerary[1].flight_number + " " + itinerary[1].departure_date.ToString("dd MMM yyyy") + " " + itinerary[1].departure_time.ToString("00:00");

            if (objUv.PaymentReference.Equals(Request.Form["OrderID"]))
            {
                if (ResponseCode != "")
                {
                    if (Request.Form["PurchaseAmount"].ToString() != (ccPayment[0].payment_amount.ToString("#0.00")).Replace(".", ""))
                    {
                        ResponseText = "Invalid return purchase amount form Verified By Visa " + Request.Form["PurchaseAmount"].ToString();
                    }
                    else if (!(ResponseCode == "000" ||
                                ResponseCode == "001" ||
                                ResponseCode == "003" ||
                                ResponseCode == "007"))
                    {
                        ResponseText = Request.Form["ResponseDescription"] + " :" + Request.Form["ResponseCode"];
                    }
                    else
                    {
                        if (ccPayment != null)
                        {
                            ccPayment[0].transaction_reference = Request.Form["ShopOrderId"];
                            ccPayment[0].approval_code = Request.Form["ApprovalCode"];
                            ccPayment[0].document_number = Request.Form["PAN"];

                            PaymentState = "PAID";
                            ResponseText = "APPROVED";
                            strResult = ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                                           ref itinerary,
                                                                           ref passengers,
                                                                           ref quotes,
                                                                           ref fees,
                                                                           ref objPaymentFees,
                                                                           ref mappings,
                                                                           ref services,
                                                                           ref remarks,
                                                                           ref payments,
                                                                           ref taxes,
                                                                           Classes.Language.CurrentCode().ToUpper());
                        }
                    }
                }
            }
            Boolean updateLogResult = ccPayment.objService.UpdatePaymentApproval(Request.Form["ApprovalCode"],
                                                                               objUv.PaymentReference,
                                                                               Request.Form["ShopOrderId"],
                                                                               trans_desc,
                                                                               "", "", "", "", "",
                                                                               ResponseText,
                                                                               PaymentState,
                                                                               ResponseCode,
                                                                               ResponseCode,
                                                                               Request.Form["ResponseDescription"],
                                                                               B2CSession.PaymentApprovalID,
                                                                               B2CSession.PaymentRequestStreamText,
                                                                               Request.Form["xmlmsg"],
                                                                               Request.Form["PAN"],
                                                                               "VISA");
            if ((ResponseText != "") && (ResponseText != "APPROVED"))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "PaymentResponse", "<script language='javascript'>" +
                                " alert('!!! " + ResponseText + " !!!'); </script>");
            }
            return strResult;
        }
        #endregion
        #region ETranzactPaymentResponse
        private string ETranzactPaymentResponse(B2CVariable objUv,
                                                BookingHeader bookingHeader,
                                                Itinerary itinerary,
                                                Passengers passengers,
                                                Quotes quotes,
                                                Fees fees,
                                                Fees objPaymentFees,
                                                Mappings mappings,
                                                Services services,
                                                Remarks remarks,
                                                Payments payments,
                                                Payments ccPayment,
                                                Taxes taxes)
        {
            string strResult = string.Empty;
            string PaymentState = "DECLINED";
            string ResponseText = "";

            string trans_desc = itinerary[0].airline_rcd + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date.ToString("dd MMM yyyy") + " " + itinerary[0].departure_time.ToString("00:00");
            if (itinerary.Count > 1)
                trans_desc += " :: " +
                                itinerary[1].airline_rcd + " " + itinerary[1].flight_number + " " + itinerary[1].departure_date.ToString("dd MMM yyyy") + " " + itinerary[1].departure_time.ToString("00:00");

            if (Request.Form["FINAL_CHECKSUM"] != null && Request.Form["FINAL_CHECKSUM"] != "")
            {
                System.Security.Cryptography.MD5 objEncrypt = System.Security.Cryptography.MD5.Create();
                UTF8Encoding encoder = new UTF8Encoding();

                byte[] data = objEncrypt.ComputeHash(encoder.GetBytes(Request.Form["SUCCESS"] + B2CSession.ETZEncrypt));
                StringBuilder sBuilder = new StringBuilder();
                foreach (byte byt in data)
                {
                    sBuilder.Append(byt.ToString("x2"));
                }

                if (Request.Form["FINAL_CHECKSUM"].ToString().ToUpper() != sBuilder.ToString().ToUpper())
                {
                    ResponseText = "Invalid Data Encryption result";
                }
                else if (Convert.ToDouble(Request.Form["AMOUNT"]) != Convert.ToDouble(ccPayment[0].payment_amount))
                {
                    ResponseText = "Invalid return purchase amount form eTranzact " + Request.Form["AMOUNT"].ToString();
                }
                else
                {
                    if (objUv.PaymentReference.Equals(Request.Form["TRANSACTION_ID"]))
                    {
                        if (Convert.ToInt16(Request.Form["SUCCESS"]) != 0)
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "PaymentResponse", "<script language='javascript'>" +
                                    " ErrorETranzactGateway(" + Request.Form["SUCCESS"] + "); </script>");
                        }
                        else
                        {
                            if (ccPayment != null)
                            {

                                string CardNo = Request.Form["CARD4"];
                                if (CardNo.Length < 8)
                                {
                                    for (int i = 0; i < 16 - CardNo.Length; i++)
                                    {
                                        CardNo = "0" + CardNo;
                                    }
                                }
                                ccPayment[0].transaction_reference = Request.Form["TRANSACTION_ID"];
                                ccPayment[0].approval_code = Request.Form["MERCHANT_CODE"];
                                ccPayment[0].document_number = CardNo;

                                PaymentState = "PAID";
                                ResponseText = "APPROVED";
                                strResult = ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                                              ref itinerary,
                                                                              ref passengers,
                                                                              ref quotes,
                                                                              ref fees,
                                                                              ref objPaymentFees,
                                                                              ref mappings,
                                                                              ref services,
                                                                              ref remarks,
                                                                              ref payments,
                                                                              ref taxes,
                                                                              Classes.Language.CurrentCode().ToUpper());
                            }
                        }
                    }
                }
            }
            Boolean updateLogResult = ccPayment.objService.UpdatePaymentApproval(Request.Form["TRANSACTION_ID"],
                                                                                objUv.PaymentReference,
                                                                                Request.Form["TERMINAL_ID"],
                                                                                trans_desc,
                                                                                "", "", "", "", "",
                                                                                ResponseText,
                                                                                PaymentState,
                                                                                Request.Form["SUCCESS"],
                                                                                Request.Form["SUCCESS"],
                                                                                Request.Form["DESCRIPTION"],
                                                                                B2CSession.PaymentApprovalID,
                                                                                B2CSession.PaymentRequestStreamText,
                                                                                Request.Form["xmlmsg"],
                                                                                Request.Form["CARD4"],
                                                                                "");
            if ((ResponseText != "") && (ResponseText != "APPROVED"))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "PaymentResponse", "<script language='javascript'>" +
                                " alert('!!! " + ResponseText + " !!!'); </script>");
            }
            return strResult;
        }
        #endregion
        #region InterswitchPaymentResponse
        private string InterswitchPaymentResponse(B2CVariable objUv,
                                                  BookingHeader bookingHeader,
                                                  Itinerary itinerary,
                                                  Passengers passengers,
                                                  Quotes quotes,
                                                  Fees fees,
                                                  Fees objPaymentFees,
                                                  Mappings mappings,
                                                  Services services,
                                                  Remarks remarks,
                                                  Payments payments,
                                                  Payments ccPayment,
                                                  Taxes taxes)
        {
            if (Request["ECHO"] == null || Request["ECHO"] == "" || B2CSession.ITSEncrypt == null)
                return string.Empty;

            string strResult = string.Empty;
            string PaymentState = "DECLINED";
            string ResponseText = "";

            //Request.Form["payRef"]
            string trans_desc = itinerary[0].airline_rcd + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date.ToString("dd MMM yyyy") + " " + itinerary[0].departure_time.ToString("00:00");
            if (itinerary.Count > 1)
                trans_desc += " :: " +
                                itinerary[1].airline_rcd + " " + itinerary[1].flight_number + " " + itinerary[1].departure_date.ToString("dd MMM yyyy") + " " + itinerary[1].departure_time.ToString("00:00");


            if (B2CSession.ITSEncrypt != Request.Form["ECHO"].ToString().ToUpper())
            {
                ResponseText = "Invalid Data Encryption result";
            }
            else
            {
                B2CSession.ITSEncrypt = null;
                if (objUv.PaymentReference.Equals(Request.Form["TXNREF"]))
                {
                    if (Request.Form["apprAmt"].ToString() != (ccPayment[0].payment_amount.ToString("#0.00")).Replace(".", ""))
                    {
                        ResponseText = "Invalid return approve amount form Interswitch " + Request.Form["apprAmt"].ToString();
                    }
                    else if (Request.Form["RESP"] != "00")
                    {
                        ResponseText = Request.Form["DESC"] + " :" + Request.Form["RESP"];
                    }
                    else  //Request.Form["RESP"] == "00"
                    {
                        if (ccPayment != null)
                        {
                            string CardNo = Request.Form["cardNum"];
                            if (CardNo.Length < 8)
                            {
                                for (int i = 0; i < 16 - CardNo.Length; i++)
                                {
                                    CardNo = "0" + CardNo;
                                }
                            }
                            ccPayment[0].transaction_reference = Request.Form["payRef"];
                            ccPayment[0].approval_code = Request.Form["retRef"];
                            ccPayment[0].document_number = CardNo;
                            if (Request.Form["apprAmt"] != "")
                                ccPayment[0].document_amount = Convert.ToDecimal(Request.Form["apprAmt"]);

                            PaymentState = "PAID";
                            ResponseText = "APPROVED";
                            strResult = ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                                           ref itinerary,
                                                                           ref passengers,
                                                                           ref quotes,
                                                                           ref fees,
                                                                           ref objPaymentFees,
                                                                           ref mappings,
                                                                           ref services,
                                                                           ref remarks,
                                                                           ref payments,
                                                                           ref taxes,
                                                                           Classes.Language.CurrentCode().ToUpper());
                        }
                    }
                }
            }
            Boolean updateLogResult = ccPayment.objService.UpdatePaymentApproval(Request.Form["retRef"],
                                                                                objUv.PaymentReference,
                                                                                Request.Form["TXNREF"],
                                                                                trans_desc,
                                                                                "", "", "", "", "",
                                                                                ResponseText,
                                                                                PaymentState,
                                                                                Request.Form["RESP"],
                                                                                Request.Form["RESP"],
                                                                                Request.Form["DESC"],
                                                                                B2CSession.PaymentApprovalID,
                                                                                B2CSession.PaymentRequestStreamText,
                                                                                Request.Form["xmlmsg"],
                                                                                Request.Form["cardNum"],
                                                                                "");
            if ((ResponseText != "") && (ResponseText != "APPROVED"))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "PaymentResponse", "<script language='javascript'>" +
                                " alert('!!! " + ResponseText + " !!!'); </script>");
            }
            return strResult;
        }
        #endregion
        #region DokuPaymentResponse
        private string DokuPaymentResponse(B2CVariable objUv,
                                            BookingHeader bookingHeader,
                                            Itinerary itinerary,
                                            Passengers passengers,
                                            Quotes quotes,
                                            Fees fees,
                                            Fees objPaymentFees,
                                            Mappings mappings,
                                            Services services,
                                            Remarks remarks,
                                            Payments payments,
                                            Payments ccPayment,
                                            Taxes taxes)
        {
            string strResult = string.Empty;
            try
            {

                if (ccPayment == null)
                {
                    ccPayment = new Payments();
                }
                else
                {
                    ccPayment.Clear();
                }

                if (B2CSession.AgentService == null || ccPayment.objService == null)
                {
                    //Initial service
                    Helper objHelper = new Helper();
                    objHelper.ApplicationInitialize(objUv.Agency_Code, objUv.UserId);
                    B2CSession.Variable = objUv;

                    ccPayment.objService = B2CSession.AgentService;
                }

                string transidmerchant = Request.Form["TRANSIDMERCHANT"];
                DataSet ds = ccPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, "", "", "",
                                                transidmerchant, "", "", "", "", "", "", 0, 0, false);

                Library objLi = new Library();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string chkResponse = row["response_text"] != null ? row["response_text"].ToString().Trim() : "";
                    if (chkResponse == "Success")
                    {
                        string RefID = row["booking_id"] != null ? row["booking_id"].ToString().Trim() : "";
                        if (string.IsNullOrEmpty(RefID))
                        {
                            string[] arr = row["transaction_description"] != null ? row["transaction_description"].ToString().Trim().Split(',') : new string[1];
                            if (arr.Length > 0) RefID = arr[0];
                        }

                        Guid booking_id = new Guid(RefID);

                        Booking objBooking = new Booking();
                        objBooking.objService = B2CSession.AgentService;
                        objBooking.BookingRead(booking_id);


                        using (StringWriter stw = new StringWriter())
                        {
                            using (XmlWriter xtw = XmlWriter.Create(stw))
                            {
                                xtw.WriteStartElement("Booking");
                                {
                                    objLi.BuiltBookingXml(xtw, objBooking.BookingHeader, objBooking.Itinerary, objBooking.Passengers, objBooking.Quotes, objBooking.Fees, objBooking.Mappings, objBooking.Services, objBooking.Remarks, objBooking.Payments, objBooking.Taxes);
                                }
                                xtw.WriteEndElement();
                            }
                            strResult = stw.ToString();
                        }
                    }
                    else
                    {
                        dvContainer.InnerHtml = objLi.GenerateControlString("UserControls/Payment.ascx", string.Empty);
                    }
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "PaymentResponse", "<script language='javascript'>" +
                                   " alert('!!! " + "Invalid result" + " !!!'); </script>");
                }

                return strResult;
            }
            catch (Exception ex)
            {
                return string.Format("{0}: {1}", ex.Message, ex.StackTrace);
            }
        }
        #endregion
        #region Ogone3DPaymentResponse
        private string Ogone3DPaymentResponse(B2CVariable objUv,
                                              BookingHeader bookingHeader,
                                              Itinerary itinerary,
                                              Passengers passengers,
                                              Quotes quotes,
                                              Fees fees,
                                              Fees objPaymentFees,
                                              Mappings mappings,
                                              Services services,
                                              Remarks remarks,
                                              Payments payments,
                                              Payments ccPayment,
                                              Taxes taxes)
        {
            string ret = "";

            Helper objHelper = new Helper();
            Boolean updateLogResult = false;
            string PaymentState = "DECLINE";
            string ResponseCode = "DECLINE";
            string ResponseText = string.Empty;
            string payID = string.Empty;
            string orderID = string.Empty;
            string responseStream = string.Empty;
            string trans_desc = string.Empty;
            string strURL = string.Empty;

            try
            {
                if (Request["OGNRef"] == null || Request["OGNRef"] == "")
                {
                    objHelper.SendErrorEmail("srtInput", "&&Request[OGNRef] is null&&", "", "", "Ogone3DPaymentResponse");
                    return ret;
                }

                if (Session["RequestStream"] != null)
                {
                    strURL = Session["RequestStream"].ToString();
                }

                if (itinerary.Count > 0)
                {
                    trans_desc = itinerary[0].airline_rcd + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date.ToString("dd MMM yyyy") + " " + itinerary[0].departure_time.ToString("00:00");
                }
                else
                {
                    objHelper.SendErrorEmail("srtInput", "&&itinerary is null&&", "", "", "Ogone3DPaymentResponse");
                }

                responseStream = Request.RawUrl + "\r\n";
                string SecretKey = "OGONE3D" + "{" + bookingHeader.booking_id.ToString().ToUpper().Replace("{", "").Replace("}", "") + "}";
                System.Security.Cryptography.MD5 objEncrypt = System.Security.Cryptography.MD5.Create();
                UTF8Encoding encoder = new UTF8Encoding();
                byte[] data = objEncrypt.ComputeHash(encoder.GetBytes(SecretKey));
                StringBuilder sBuilder = new StringBuilder();
                foreach (byte byt in data)
                {
                    sBuilder.Append(byt.ToString("x2"));
                }
                SecretKey = sBuilder.ToString().ToUpper();

                if (Request["OGNRef"] != null)
                {
                    if (Request["OGNRef"].ToString() == SecretKey && objUv.PaymentReference != null && objUv.PaymentReference == "TRUE")
                    {
                        if (ccPayment != null)
                        {
                            objUv.PaymentReference = null;

                            //Save Success information.
                            string strPaymentResult;
                            ServiceClient objService = new ServiceClient();
                            objService.objService = B2CSession.AgentService;

                            PaymentState = "PAID";
                            ResponseCode = "APPROVED";
                            ResponseText = "APPROVED";

                            strPaymentResult = objService.SaveBookingCreditCard(bookingHeader.booking_id.ToString(),
                                                                                bookingHeader,
                                                                                itinerary,
                                                                                passengers,
                                                                                remarks,
                                                                                ccPayment,
                                                                                mappings,
                                                                                services,
                                                                                taxes,
                                                                                fees,
                                                                                objPaymentFees,
                                                                                "3D-APPROVE=1|" + Request["orderID"] + "|" + Request["PAYID"] + "|" + Request["ACCEPTANCE"] + "|" + responseStream,
                                                                                string.Empty,
                                                                                string.Empty,
                                                                                string.Empty,
                                                                                string.Empty,
                                                                                true,
                                                                                false,
                                                                                false,
                                                                                Classes.Language.CurrentCode().ToUpper());

                            if (strPaymentResult.Length > 0)
                            {
                                Classes.Helper objHp = new Classes.Helper();
                                Library objLi = new Library();
                                System.Xml.XPath.XPathDocument xmlDoc = new System.Xml.XPath.XPathDocument(new StringReader(strPaymentResult));
                                System.Xml.XPath.XPathNavigator nv = xmlDoc.CreateNavigator();

                                if (nv.Select("Booking/BookingHeader").Count > 0)
                                {
                                    //Success Payment.
                                    //Get Xml String and return out to generate control
                                    objLi.FillBooking(strPaymentResult,
                                                      ref bookingHeader,
                                                      ref passengers,
                                                      ref itinerary,
                                                      ref mappings,
                                                      ref payments,
                                                      ref remarks,
                                                      ref taxes,
                                                      ref quotes,
                                                      ref fees,
                                                      ref services);
                                    objUv.FormOfPaymentFee = string.Empty;

                                    //Send passenger itinerary by email.
                                    objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                                    objUv.CurrentStep = 5;

                                    //Remove Session
                                    B2CSession.RemoveBookingSession();
                                    //Generate booking summary control.

                                    updateLogResult = ccPayment.objService.UpdatePaymentApproval(Page.Request["OGNRef"],
                                               objUv.PaymentReference,
                                               Page.Request["OGNRef"],//transaction reference
                                               trans_desc,
                                               "", "", "", "", "",
                                               ResponseText,
                                               PaymentState,
                                               ResponseCode,
                                               ResponseCode,
                                               strPaymentResult,
                                               B2CSession.PaymentApprovalID,
                                               "",
                                               "",
                                               "",
                                               "");

                                    return strPaymentResult;
                                }
                                else
                                {
                                    objHelper.SendErrorEmail("srtInput", "&&nv.Select(Booking/BookingHeader).Count < 0 on BookingHeader.booking_id = " + bookingHeader.booking_id + "&&", "return {700}", "", "Ogone3DPaymentResponse");
                                    return "{700}";
                                }
                            }
                            else
                            {
                                ResponseText = "DECLINE";
                                string strInputMsg = "BookingHeader.booking_id :" + bookingHeader.booking_id + "<br/>";
                                strInputMsg += "Itinerary.Count : " + itinerary.Count + "<br/>";
                                strInputMsg += "Passengers.Count : " + passengers.Count + "<br/>";
                                strInputMsg += "Quotes.Count : " + quotes.Count + "<br/>";
                                strInputMsg += "Fees.Count : " + fees.Count + "<br/>";
                                strInputMsg += "ObjPaymentFees.Count : " + objPaymentFees.Count + "<br/>";
                                strInputMsg += "Mappings.Count : " + mappings.Count + "<br/>";
                                strInputMsg += "Services.Count : " + services.Count + "<br/>";
                                strInputMsg += "Remarks.Count : " + remarks.Count + "<br/>";
                                strInputMsg += "Payments.Count : " + payments.Count + "<br/>";
                                strInputMsg += "Taxes.Count : " + taxes.Count + "<br/>";

                                string strErrorMsg = "SaveBookingCreditCard fail (strResult is empty) on BookingHeader.booking_id :" + bookingHeader.booking_id + "\r\n";
                                objHelper.SendErrorEmail(strInputMsg, strErrorMsg, "", "Ogone3DPaymentResponse", "SaveBookingCreditCard");
                                ResponseText += " : " + strErrorMsg + strInputMsg.Replace("<br/>", "\r\n");
                                responseStream += ResponseText + "\r\n";
                            }
                        }
                        else
                        {
                            objHelper.SendErrorEmail("srtInput", "&&ccPayment is null on BookingHeader.booking_id = " + bookingHeader.booking_id + "&&", "", "", "Ogone3DPaymentResponse");
                        }
                    }
                    else
                    {
                        objHelper.SendErrorEmail("srtInput", "&&Request[OGNRef].ToString() : " + Request["OGNRef"] + " is not match SecretKey : " + SecretKey + " && objUv.PaymentReference != null && objUv.PaymentReference == \"TRUE\" on BookingHeader.booking_id = " + bookingHeader.booking_id + "&&", "", "", "Ogone3DPaymentResponse");
                    }
                }
                objUv.PaymentReference = null;
            }
            catch (Exception ex)
            {
                // update approval
                updateLogResult = ccPayment.objService.UpdatePaymentApproval(Page.Request["OGNRef"],
                                                objUv.PaymentReference,
                                                Page.Request["OGNRef"],//transaction reference
                                                trans_desc,
                                                "", "", "", "", "",
                                                ResponseText,
                                                PaymentState,
                                                ResponseCode,
                                                ResponseCode,
                                                "",
                                                B2CSession.PaymentApprovalID,
                                                strURL,
                                                responseStream,
                                                "",
                                                "");

                objHelper.SendErrorEmail(Page.Request["OGNRef"], ex.Message, ex.StackTrace, "", "Ogone3DPaymentResponse");
            }

            return ret;
        }
        #endregion

        private string TatraPaymentResponse(B2CVariable objUv,
                                            BookingHeader bookingHeader,
                                            Itinerary itinerary,
                                            Passengers passengers,
                                            Quotes quotes,
                                            Fees fees,
                                            Fees objPaymentFees,
                                            Mappings mappings,
                                            Services services,
                                            Remarks remarks,
                                            Payments payments,
                                            Payments ccPayment,
                                            Taxes taxes)
        {
            string strResult = string.Empty;
            string PaymentState = "DECLINED";
            string ResponseText = "";
            try
            {
                if (Request["RES"] != null)
                {
                    if (bookingHeader.booking_number.ToString().Equals(Request["VS"]))
                    {
                        if (Request["RES"] == "OK")
                        {
                            if (ccPayment != null)
                            {
                                string result = string.Empty;
                                ccPayment[0].transaction_reference = Request["VS"];
                                ccPayment[0].approval_code = Request["AC"];
                                ccPayment[0].payment_reference = Request["VS"];
                                PaymentState = "PAID";
                                ResponseText = "APPROVED";
                                strResult = ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                                               ref itinerary,
                                                                               ref passengers,
                                                                               ref quotes,
                                                                               ref fees,
                                                                               ref objPaymentFees,
                                                                               ref mappings,
                                                                               ref services,
                                                                               ref remarks,
                                                                               ref payments,
                                                                               ref taxes,
                                                                               Classes.Language.CurrentCode().ToUpper());
                            }
                        }
                    }
                }


                Boolean updateLogResult = ccPayment.objService.UpdatePaymentApproval(Request["AC"],
                                               objUv.PaymentReference,
                                               Request["VS"],
                                               bookingHeader.contact_name + " " + bookingHeader.contact_email + " " + bookingHeader.phone_mobile + " " + bookingHeader.firstname + " " + bookingHeader.lastname + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date + " " + itinerary[0].departure_time,
                                               "", "", "", "", "",
                                               ResponseText,
                                               PaymentState,
                                               Request["AC"],
                                               Request["AC"],
                                               Request["SIGN"],
                                               B2CSession.PaymentApprovalID,
                                               B2CSession.PaymentRequestStreamText,
                                               Request.QueryString.ToString(),// Note save return form Tatra
                                               "0000000000000000",
                                               "");

            }
            catch (Exception ex)
            {
                Boolean updateLogResult = ccPayment.objService.UpdatePaymentApproval(Request["AC"],
                                              objUv.PaymentReference,
                                              Request["VS"],
                                              bookingHeader.contact_name + " " + bookingHeader.contact_email + " " + bookingHeader.phone_mobile + " " + bookingHeader.firstname + " " + bookingHeader.lastname + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date + " " + itinerary[0].departure_time,
                                              "", ex.ToString(), "", "", "",
                                              ResponseText,
                                              PaymentState,
                                              Request["AC"],
                                              Request["AC"],
                                              Request["SIGN"],
                                              B2CSession.PaymentApprovalID,
                                              B2CSession.PaymentRequestStreamText,
                                              Request.QueryString.ToString(),// Note save return form Tatra
                                              "0000000000000000",
                                              "");

                throw ex;
            }
            return strResult;

        }

        private string OgoneECommercePaymentResponse(B2CVariable objUv,
                                                     BookingHeader bookingHeader,
                                                     Itinerary itinerary,
                                                     Passengers passengers,
                                                     Quotes quotes,
                                                     Fees fees,
                                                     Fees objPaymentFees,
                                                     Mappings mappings,
                                                     Services services,
                                                     Remarks remarks,
                                                     Payments payments,
                                                     Payments ccPayment,
                                                     Taxes taxes)
        {
            string ret = "";

            Helper objHelper = new Helper();
            StringBuilder strlog = new StringBuilder();
            Boolean updateLogResult = false;
            string PaymentState = "DECLINE";
            string ResponseCode = "DECLINE";
            string ResponseText = string.Empty;
            string payID = string.Empty;
            string orderID = string.Empty;
            string responseStream = string.Empty;
            string trans_desc = string.Empty;
            string strURL = string.Empty;

            try
            {
                if (Request["Ref"] != null)
                {
                    if (Session["RequestStream"] != null)
                    {
                        strURL = Session["RequestStream"].ToString();
                    }
                    else
                    {
                        objHelper.SendErrorEmail("srtInput", "RequestStream is null", "", "", "OgoneECommercePaymentResponse");
                        return "";
                    }

                    if (Request["PAYID"] != null)
                    {
                        payID = Request["PAYID"].ToString();
                    }

                    if (Request["orderID"] != null)
                    {
                        orderID = Request["orderID"].ToString();
                    }

                    if (itinerary.Count > 0)
                    {
                        trans_desc = itinerary[0].airline_rcd + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date.ToString("dd MMM yyyy") + " " + itinerary[0].departure_time.ToString("00:00");
                    }
                    else
                    {
                        strlog.Append("&&itinerary is null");
                        objHelper.SendErrorEmail("srtInput", strlog.ToString(), "", "", "OgoneECommercePaymentResponse");
                    }

                    responseStream = Request.RawUrl + "\r\n";

                    if (Request["Ref"] == "SUCCESS")
                    {
                        if (ccPayment != null)
                        {
                            string result = string.Empty;
                            //ccPayment[0].transaction_reference = Request["VS"];
                            ccPayment[0].approval_code = "1";

                            PaymentState = "PAID";
                            ResponseCode = "APPROVED";
                            ResponseText = "APPROVED";

                            ret = ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                                     ref itinerary,
                                                                     ref passengers,
                                                                     ref quotes,
                                                                     ref fees,
                                                                     ref objPaymentFees,
                                                                     ref mappings,
                                                                     ref services,
                                                                     ref remarks,
                                                                     ref payments,
                                                                     ref taxes,
                                                                     Classes.Language.CurrentCode().ToUpper());

                            if (ret.Length == 0)
                            {
                                ResponseText = "DECLINE";
                                string strInputMsg = "BookingHeader.booking_id :" + bookingHeader.booking_id + "<br/>";
                                strInputMsg += "Itinerary.Count : " + itinerary.Count + "<br/>";
                                strInputMsg += "Passengers.Count : " + passengers.Count + "<br/>";
                                strInputMsg += "Quotes.Count : " + quotes.Count + "<br/>";
                                strInputMsg += "Fees.Count : " + fees.Count + "<br/>";
                                strInputMsg += "ObjPaymentFees.Count : " + objPaymentFees.Count + "<br/>";
                                strInputMsg += "Mappings.Count : " + mappings.Count + "<br/>";
                                strInputMsg += "Services.Count : " + services.Count + "<br/>";
                                strInputMsg += "Remarks.Count : " + remarks.Count + "<br/>";
                                strInputMsg += "Payments.Count : " + payments.Count + "<br/>";
                                strInputMsg += "Taxes.Count : " + taxes.Count + "<br/>";

                                string strErrorMsg = "PaymentCreditCardOffline fail (strResult is empty) on payment id : " + payID + ", order id : " + orderID + " \r\n";
                                objHelper.SendErrorEmail(strInputMsg, strErrorMsg, "", "OgoneECommercePaymentResponse", "PaymentCreditCardOffline");
                                ResponseText += " : " + strErrorMsg + strInputMsg.Replace("<br/>", "\r\n");
                                responseStream += ResponseText + "\r\n";
                            }

                            //Send passenger itinerary by email.
                            Classes.Helper objHp = new Classes.Helper();
                            objHp.SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);

                            objUv.CurrentStep = 5;

                            //Remove Session
                            Session.Remove("Itinerary");
                            Session.Remove("Passengers");
                            Session.Remove("Quotes");
                            Session.Remove("Fees");
                            Session.Remove("Mappings");
                            Session.Remove("Services");
                            Session.Remove("Remarks");
                            Session.Remove("Payments");
                            Session.Remove("Taxes");
                        }
                        else
                        {
                            strlog.Append("&&ccPayment is null");
                            objHelper.SendErrorEmail("srtInput", strlog.ToString(), "", "", "OgoneECommercePaymentResponse");
                        }
                    }
                    else
                    {
                        //  return unsuccess
                        strlog.Append("&&return unsuccess - request[ref] is not equal success");
                    }

                    updateLogResult = ccPayment.objService.UpdatePaymentApproval(Page.Request["SHASIGN"],
                                               objUv.PaymentReference,
                                               payID,//transaction reference
                                               trans_desc,
                                               "", "", "", "", "",
                                               ResponseText,
                                               PaymentState,
                                               ResponseCode,
                                               ResponseCode,
                                               "",
                                               B2CSession.PaymentApprovalID,
                                               strURL,
                                               responseStream,
                                               "",
                                               "");
                }
                else
                {
                    //request ref is null
                    strlog.Append("&&request[ref] is null");
                }
            }
            catch (Exception ex)
            {
                // update approval
                updateLogResult = ccPayment.objService.UpdatePaymentApproval(Page.Request["SHASIGN"],
                                                objUv.PaymentReference,
                                                payID,//transaction reference
                                                trans_desc,
                                                "", "", "", "", "",
                                                ResponseText,
                                                PaymentState,
                                                ResponseCode,
                                                ResponseCode,
                                                "",
                                                B2CSession.PaymentApprovalID,
                                                strURL,
                                                responseStream,
                                                "",
                                                "");

                objHelper.SendErrorEmail(ex, strlog.ToString());
            }

            return ret;
        }

        private string SofortPaymentResponse(B2CVariable objUv, BookingHeader bookingHeader, Itinerary itinerary, Passengers passengers, Quotes quotes, Fees fees, Fees objPaymentFees, Mappings mappings, Services services, Remarks remarks, Payments payments, Payments ccPayment, Taxes taxes)
        {
            string result = string.Empty;
            tikSystem.Web.PaymentGateway.PaymentGatewayFactory.GateWayType gateway = tikSystem.Web.PaymentGateway.PaymentGatewayFactory.GateWayType.Sofort;
            tikSystem.Web.Contact.PaymentGateway objGateway = tikSystem.Web.PaymentGateway.PaymentGatewayFactory.GetGatePaymentGateway(gateway);
            objGateway.PaymentResponseReference = objUv.PaymentReference;



            if (objGateway.PaymentResponse() == tikSystem.Web.Contact.ResponseStatus.Success)
            {
                ccPayment[0].approval_code = "1";

                result = ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                             ref itinerary,
                                                             ref passengers,
                                                             ref quotes,
                                                             ref fees,
                                                             ref objPaymentFees,
                                                             ref mappings,
                                                             ref services,
                                                             ref remarks,
                                                             ref payments,
                                                             ref taxes,
                                                             Classes.Language.CurrentCode().ToUpper());

                new Helper().SendItineraryByMail(bookingHeader, string.Empty, objUv.UserId, _stdLanguage);
                objUv.CurrentStep = 5;
                this.Session.Remove("Itinerary");
                this.Session.Remove("Passengers");
                this.Session.Remove("Quotes");
                this.Session.Remove("Fees");
                this.Session.Remove("Mappings");
                this.Session.Remove("Services");
                this.Session.Remove("Remarks");
                this.Session.Remove("Payments");
                this.Session.Remove("Taxes");
            }
            return result;
        }

        #region ESewaPaymentResponse
        private string ESewaPaymentResponse(B2CVariable objUv,
                                                BookingHeader bookingHeader,
                                                Itinerary itinerary,
                                                Passengers passengers,
                                                Quotes quotes,
                                                Fees fees,
                                                Fees objPaymentFees,
                                                Mappings mappings,
                                                Services services,
                                                Remarks remarks,
                                                Payments payments,
                                                Payments ccPayment,
                                                Taxes taxes)
        {
            string strResult = string.Empty;
            string PaymentState = "DECLINED";
            string ResponseText = "";

            //string trans_desc = itinerary[0].airline_rcd + " " + itinerary[0].flight_number + " " + itinerary[0].departure_date.ToString("dd MMM yyyy") + " " + itinerary[0].departure_time.ToString("00:00");
            //if (itinerary.Count > 1)
            //    trans_desc += " :: " +
            //                    itinerary[1].airline_rcd + " " + itinerary[1].flight_number + " " + itinerary[1].departure_date.ToString("dd MMM yyyy") + " " + itinerary[1].departure_time.ToString("00:00");

            if (Request.Form["Re"] != null && Request.Form["Re"] != "")
            {
                //System.Security.Cryptography.MD5 objEncrypt = System.Security.Cryptography.MD5.Create();
                //UTF8Encoding encoder = new UTF8Encoding();

                //byte[] data = objEncrypt.ComputeHash(encoder.GetBytes(Request.Form["SUCCESS"] + B2CSession.ETZEncrypt));
                //StringBuilder sBuilder = new StringBuilder();
                //foreach (byte byt in data)
                //{
                //    sBuilder.Append(byt.ToString("x2"));
                //}

                //if (Request.Form["FINAL_CHECKSUM"].ToString().ToUpper() != sBuilder.ToString().ToUpper())
                //{
                //    ResponseText = "Invalid Data Encryption result";
                //}
                //else if (Convert.ToDouble(Request.Form["AMOUNT"]) != Convert.ToDouble(ccPayment[0].payment_amount))
                //{
                //    ResponseText = "Invalid return purchase amount form eTranzact " + Request.Form["AMOUNT"].ToString();
                //}
                //else
                //{
                if (objUv.PaymentReference.Equals(Request.Form["TRANSACTION_ID"]))
                {
                    if (Convert.ToInt16(Request.Form["SUCCESS"]) != 0)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "PaymentResponse", "<script language='javascript'>" +
                                " ErrorETranzactGateway(" + Request.Form["SUCCESS"] + "); </script>");
                    }
                    else
                    {
                        if (ccPayment != null)
                        {

                            string CardNo = Request.Form["CARD4"];
                            if (CardNo.Length < 8)
                            {
                                for (int i = 0; i < 16 - CardNo.Length; i++)
                                {
                                    CardNo = "0" + CardNo;
                                }
                            }
                            ccPayment[0].transaction_reference = Request.Form["TRANSACTION_ID"];
                            ccPayment[0].approval_code = Request.Form["MERCHANT_CODE"];
                            ccPayment[0].document_number = CardNo;

                            PaymentState = "PAID";
                            ResponseText = "APPROVED";
                            strResult = ccPayment.PaymentCreditCardOffline(ref bookingHeader,
                                                                          ref itinerary,
                                                                          ref passengers,
                                                                          ref quotes,
                                                                          ref fees,
                                                                          ref objPaymentFees,
                                                                          ref mappings,
                                                                          ref services,
                                                                          ref remarks,
                                                                          ref payments,
                                                                          ref taxes,
                                                                          Classes.Language.CurrentCode().ToUpper());
                        }
                    }
                }
            }
            //}
            Boolean updateLogResult = ccPayment.objService.UpdatePaymentApproval(Request.Form["TRANSACTION_ID"],
                                                                                objUv.PaymentReference,
                                                                                Request.Form["TERMINAL_ID"],
                                                                                "",//trans_desc,
                                                                                "", "", "", "", "",
                                                                                ResponseText,
                                                                                PaymentState,
                                                                                Request.Form["SUCCESS"],
                                                                                Request.Form["SUCCESS"],
                                                                                Request.Form["DESCRIPTION"],
                                                                                B2CSession.PaymentApprovalID,
                                                                                B2CSession.PaymentRequestStreamText,
                                                                                Request.Form["xmlmsg"],
                                                                                Request.Form["CARD4"],
                                                                                "");
            if ((ResponseText != "") && (ResponseText != "APPROVED"))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "PaymentResponse", "<script language='javascript'>" +
                                " alert('!!! " + ResponseText + " !!!'); </script>");
            }
            return strResult;
        }
        #endregion

        #endregion

    }
}
