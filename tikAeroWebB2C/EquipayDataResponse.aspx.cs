using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using System.IO;

namespace tikAeroB2C
{
    public partial class EquipayDataResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            B2CVariable objUv = B2CSession.Variable;
            string strPath = Server.MapPath("~") + @"\Log\" + String.Format("{0:yyyyMMdd}", DateTime.Now) + ".log";
            StreamWriter objWriter = new StreamWriter(strPath);

            if (objUv != null)
            { 
                Page.Response.Write("Session OK<br/>");
                objWriter.WriteLine("Session OK<br/>");
            }
            else
            { 
                Page.Response.Write("Session Null<br/>");
                objWriter.WriteLine("Session Null<br/>");
            }

            if (Request.Form["successcode"] == null)
            { 
                Page.Response.Write("Not OK<br/>");
                objWriter.WriteLine("Not OK<br/>");
            }
            else
            { 
                Page.Response.Write("OK<br/>");
                Page.Response.Write(Request.Form["successcode"] + "<br/>");

                objWriter.WriteLine("OK<br/>");
                objWriter.WriteLine(Request.Form["successcode"] + "<br/>");
            }
            objWriter.Close();
        }
    }
}
