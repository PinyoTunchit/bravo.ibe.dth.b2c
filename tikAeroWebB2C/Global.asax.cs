using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Text;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using System.Collections.Specialized;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace tikAeroB2C
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }
        protected void Session_End(object sender, EventArgs e)
        {
            //Release Seat
            //BookingHeader bookingHeader = B2CSession.BookingHeader;
            BookingHeader bookingHeader = Session[Cnum.CnumSession.BookingHeader] as BookingHeader;

            if (bookingHeader != null && bookingHeader.booking_id != Guid.Empty)
            {
                ServiceClient obj = new ServiceClient();
                obj.ReleaseSessionlessFlightInventorySession(string.Empty, string.Empty, string.Empty, bookingHeader.booking_id.ToString(), false, true, true, SecurityHelper.GenerateSessionlessToken());
            }
        }
    }
}