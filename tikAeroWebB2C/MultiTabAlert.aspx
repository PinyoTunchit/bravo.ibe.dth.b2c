<%@ Page Language="C#" AutoEventWireup="true" Inherits="tikAeroB2C.MultiTabAlert" Async="true"
    EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="KEYWORDS" content="airline, booking flight, online flight booking" />
    <meta name="DESCRIPTION" content="Fly Peach" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link href="App_Themes/Default/Images/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<link href="App_Themes/Default/Style/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="App_Themes/Default/Style/multitab.css" />
	<link rel="stylesheet" type="text/css" href="App_Themes/Default/Style/style.css" />
    <script type="text/javascript" src="Scripts/jQuery/jquery-1.6.1.min.js"></script>
        <script src="Scripts/MultipleTab.js" type="text/javascript"></script>
        <script src="Scripts/home.js" type="text/javascript"></script>

    <title>Peach | Peach</title>


<%--        <script type="text/javascript">
            $(function () {
                var browser = get_browser();
                var version = get_browser_version();

                if (browser == null) browser = "";
                if (version == null) version = 0;
                if ((browser.toLowerCase() == "opera" && parseInt(version) < 15) || (browser.toLowerCase() == "msie" && parseInt(version) == 9)) {
                }
                else {
                    $('#ClearTabId').hide();
                }
            });

    </script>
--%>
</head>
<body>

	<div class="Wrapper">
		<div class="xouter">
			<div class="xcontainer">
				<div class="xinner">
					<div class="MultipleTab">
						<div class="Logo">
							<img src="App_Themes/Default/Images/mainlogo.png" alt="Peach" />
						</div>
						
						<div class="clear-all"></div>
					
						<div class="MultiTabTitle">Multiple Tab Alert</div>
						
						<div class="MultipletabContent">
							
                          <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_27000", "Thank you for booking with Peach.", _stdLanguage)%>
							<br />
							Flypeach.com does not support use of multiple tabs.
							<br />
							Please close this tab and only use the page originally opened.
							<br />
							Thank you for your understanding and wish to see you soon on Peach.
						</div>
		
						<div class="clear-all"></div>
						
					</div>
					
<%--                    <input id="ClearTabId" type="button"  value="ClaerSession" onclick="ClaerSession();"/>
--%>
				</div>
			</div>
		</div>		
		
	</div>


</body>
</html>
