using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Configuration;
using System.Text;
using System.Xml;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;

namespace tikAeroB2C
{
    /// <summary>
    /// Summary description for service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class service : System.Web.Services.WebService
    {
        [WebMethod(EnableSession=true)]
        public XmlDocument GetAirport(string strAgencyCode, string strPassword)
        {
            return GetXMLAirport(strAgencyCode, strPassword, "EN");            
        }

        [WebMethod(EnableSession = true)]
        public XmlDocument GetAirportMultiLanguage(string strAgencyCode, string strPassword, string languageCode)
        {
            return GetXMLAirport(strAgencyCode, strPassword, languageCode);
        }

        private XmlDocument GetXMLAirport(string strAgencyCode, string strPassword, string languageCode)
        {
            ServiceClient objService = new ServiceClient();
            StringBuilder stb = new StringBuilder();

            Agents objAgent = new Agents();

            objService.initializeWebService(strAgencyCode, ref objAgent);
            B2CSession.AgentService = (TikAeroXMLwebservice)objService.objService;

            if (objAgent != null && objAgent.Count > 0 && objAgent[0].agency_password.Trim().Equals(strPassword.Trim()))
            {
                stb.Append("<Airports>");
                //Get Origin Airport.
                Routes routes = objService.GetOrigins(languageCode.ToUpper(), true, false, false, false, false);
               
                if (routes != null && routes.Count > 0)
                {
                    stb.Append(routes.GetXml());
                }

                routes.Clear();
                routes = null;
                //Get Destination Airport.
                routes = objService.GetDestination(languageCode.ToUpper(), true, false, false, false, false);

                if (routes != null && routes.Count > 0)
                {
                    stb.Append(routes.GetXml());
                }
                routes = null;

                stb.Append("</Airports>");

                Session.Remove("AgentService");

                //HttpUtility.HtmlEncode(
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(stb.ToString());

                return xmlDoc;
            }
            else
            {
                Session.Remove("AgentService");
                return null;
            }
        }

    }
}
