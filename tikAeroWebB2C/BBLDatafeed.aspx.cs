﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Specialized;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAeroB2C.Classes;

namespace tikAeroB2C
{
    public partial class BBLDatafeed : System.Web.UI.Page
    {
        private string strReplyStream = "";

        private string RequestStream()
        {
            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("BBLPaymentSetting");
            StringBuilder stbHtml = new StringBuilder();
            stbHtml.Append("<form name='payFormCcard' method='post' action='" + Setting["ActionURL"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='merchantId' value='" + Setting["merchantId"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='payMethod' value='" + Setting["payMethod"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='payType' value='" + Setting["payType"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='lang' value='" + Setting["lang"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='successUrl' value='" + Setting["successUrl"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='failUrl' value='" + Setting["failUrl"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='cancelUrl' value='" + Setting["cancelUrl"] + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='currCode' value='" + "%currCode%" + "'>" + Environment.NewLine);

            stbHtml.Append("<input type='hidden' name='orderRef' value='" + "%orderRef%" + "'>" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='amount' value='" + "%amount%" + "' >" + Environment.NewLine);
            stbHtml.Append("<input type='hidden' name='remark' value='" + "%remark%" + "'>" + Environment.NewLine);
            stbHtml.Append("</form>" + Environment.NewLine);

            return stbHtml.ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Form.Count > 0)
                {
                    strReplyStream = "BBLDatafeed.aspx?";
                    int i = 0;
                    for (i = 0; i < Request.Form.Count - 1; i++)
                    {
                        strReplyStream += Request.Form.Keys[i] + "=" + Request.Form[i] + "&";
                    }
                    strReplyStream += Request.Form.Keys[i] + "=" + Request.Form[i];//last value no add "&"

                    writeToLogFile(strReplyStream + "::" + Session.SessionID);

                    if (Request.Form["Ref"] != null && Request.Form["Amt"] != null && Request.Form["successcode"] != null)
                    {
                        VerifyPayment();
                    }
                }
            }
        }

        private void VerifyPayment()
        {
            if (Request.Form["Ref"] == "")
            {
                Response.Write("Stop 1-1");//(Request.Form["Ref"] == ""
                return;
            }

            string Ref = Request.Form["Ref"].ToString();

            ServiceClient objService = new ServiceClient();
            Agents objAgent = new Agents();
            Payments objPayment = new Payments();

            objService.initializeWebService(ConfigurationManager.AppSettings["DefaultAgencyCode"], ref objAgent);
            objPayment.objService = (TikAeroXMLwebservice)objService.objService;

            DataSet ds = objPayment.objService.GetPaymentApprovals(DateTime.Now.AddDays(-1), DateTime.Now, "", "", "",
                                                                    Ref, "", "", "", "", "", "", 0, 0, false);

            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                Response.Write("Stop 1-2");//Not Found TRANSIDMERCHANT in Database
                return;
            }

            string strColumns = "";
            foreach (DataColumn dc in ds.Tables[0].Columns)
            {
                strColumns += dc.ColumnName + "=" + ds.Tables[0].Rows[0][dc] + "&";
            }
            writeToLogFile(strColumns);


            if (Request.Form["Amt"] == "")
            {
                Response.Write("Stop 1-3");//(Request.Form["Amt"] == ""
                return;
            }
            if (Convert.ToString(ds.Tables[0].Rows[0]["payment_amount"]) == "")
            {
                Response.Write("Stop 1-4");//Request AMOUNT not correct
                return;
            }
            Double payment_amount = Convert.ToDouble(ds.Tables[0].Rows[0]["payment_amount"]);
            if (Convert.ToDouble(Request.Form["Amt"].Trim()) != payment_amount)
            {
                Response.Write("Stop 1-4");//Request AMOUNT not correct
                return;
            }

            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("BBLPaymentSetting");
            string strTransCurrency = Setting[Convert.ToString(ds.Tables[0].Rows[0]["currency_rcd"])];
            if (Request.Form["Cur"].Trim() != strTransCurrency)
            {
                Response.Write("Stop 1-5");//Request CURRENCY not correct
                return;
            }

            if (Convert.ToString(ds.Tables[0].Rows[0]["response_code"]) != "")
            {
                Response.Write("Stop 1-8");//Status of Transactoin is expired
                return;
            }

            string strRequestStream = RequestStream();

            strRequestStream = strRequestStream.Replace("%currCode%", strTransCurrency);
            strRequestStream = strRequestStream.Replace("%orderRef%", Ref);
            strRequestStream = strRequestStream.Replace("%amount%", payment_amount.ToString("#0.00"));
            strRequestStream = strRequestStream.Replace("%remark%", Request.Form["remark"]);

            string PaymentState = "DECLINED";
            if (Request.Form["successcode"].ToString().Trim() == "0")
            {
                PaymentState = "PAID";
            }

            string strCardNo = Request.Form["cc0104"] + "***" + Request.Form["cc1316"];
            objPayment.objService.UpdatePaymentApproval(Request.Form["AuthId"],
                                                        Ref,
                                                        Request.Form["PayRef"],//iPay Payment Reference Number
                                                        Convert.ToString(ds.Tables[0].Rows[0]["transaction_description"]),
                                                        "", "", "","", "",
                                                        Request.Form["successcode"].ToString(),
                                                        PaymentState,
                                                        Request.Form["successcode"].ToString(),
                                                        "",
                                                        Request.Form["Holder"].ToString(),
                                                        Convert.ToString(ds.Tables[0].Rows[0]["payment_approval_id"]),
                                                        strRequestStream,
                                                        strReplyStream,
                                                        strCardNo);
            Response.Write("OK");
        }

        public static void writeToLogFile(string logMessage)
        {
            try
            {
                NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("BBLPaymentSetting");
                string strLogMessage = string.Empty;
                string strLogFile = string.Empty;
                StreamWriter swLog;

                if (Setting != null && Setting["logFilePath"] != null)
                {
                    string serverpath = HttpContext.Current.Server.MapPath("~") + @"\" + @Setting["logFilePath"];
                    strLogFile = serverpath + @"\BBLDatafeed_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                    strLogMessage = string.Format("{0} {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), logMessage);//+Environment.NewLine

                    if (Directory.Exists(serverpath))
                    {
                        if (!File.Exists(strLogFile))
                        {
                            swLog = new StreamWriter(strLogFile);
                        }
                        else
                        {
                            swLog = File.AppendText(strLogFile);
                        }

                        swLog.WriteLine(strLogMessage);
                        swLog.Flush();
                        swLog.Close();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}