<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="tikAeroB2C._Default" Async="true" EnableViewState="false" %>
<%@ Register TagPrefix="Language" TagName="Language" src="~/UserControls/Language.ascx" %>
<%@ Register TagPrefix="AvailabilitySearch" TagName="AvailabilitySearch" src="~/UserControls/SearchAvailability.ascx" %>
<%@ Register Src="UserControls/ClientLogon.ascx" TagName="ClientLogon" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="KEYWORDS" content="airline, booking flight, online flight booking" />
    <meta name="DESCRIPTION" content="Style Airways, Fly with Style by Style Airways" />

    <!--Style link-->
    <style type="text/css">            
        .calendar_icon            
        {            
            background-image: url('App_Themes/Default/Images/icon_calendar.gif');   
            background-repeat: no-repeat;         
            cursor:pointer;   
            width: 23px;    
            height: 40px; 
            border:0;                             
       }
	</style>
    <link href="App_Themes/Default/Images/favicon.ico" type="image/x-icon" rel="shortcut icon" />
    <link href="App_Themes/Default/style/ui.daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="App_Themes/Default/style/jquery-ui-1.7.1.custom.css" rel="stylesheet" type="text/css" title="ui-theme" />
    <link href="App_Themes/Default/Style/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/main.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/popup.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/style.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/ui.daterangepicker.css" rel="stylesheet" type="text/css" />

    <!--Jquery link-->
    <script type="text/javascript" src="Scripts/jQuery/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="Scripts/jQuery/jquery.maskedinput-1.2.2.min.js"></script>
    <script type="text/javascript" src="Scripts/jQuery/jquery.base64.min.js"></script>
    <script type="text/javascript" src="Scripts/jQuery/jquery.collapse.js"></script>
    
    <!--IBE Javascript-->
    <script type="text/javascript" src="Scripts/GlobalSetup.js"></script>
    <script type="text/javascript" src="Scripts/Main.js"></script>
    <script type="text/javascript" src="Scripts/uxCalendar.js"></script>
    <script type="text/javascript" src="Scripts/Availability.js"></script>
    <script type="text/javascript" src="Scripts/FlightSummary.js"></script>
    <script type="text/javascript" src="Scripts/PassengerDetail.js"></script>
    <script type="text/javascript" src="Scripts/SeatMap.js"></script>
    <script type="text/javascript" src="Scripts/payment.js"></script>
    <script type="text/javascript" src="Scripts/Registration.js"></script>
    <script type="text/javascript" src="Scripts/NewsRegisterDetail.js"></script>
    <script type="text/javascript" src="Scripts/GroupBooking.js"></script>
    <script type="text/javascript" src="Scripts/CharterFlight.js"></script>
    <script type="text/javascript" src="Scripts/BonVoyageBreak.js"></script>
    <script type="text/javascript" src="Scripts/Insurance.js"></script>

    <!--[if IE 7]>
	<style type="text/css">
		.xouter{
		position:relative;
		overflow:hidden;
		}
		.xcontainer{left:0;position:fixed;}
	</style>
	<![endif]-->


    <!--CMS Section-->
    <!--BeginTitle-->
    <title>Style Airways</title>
    <!--EndTitle-->
    <!--BeginKeywords-->
    <!--EndKeywords-->
    <!--BeginDescription-->
    <!--EndDescription-->
    <!--<script language="javascript" src="HTML/scripts/forCMS.js"></script>-->
    <!--CMS Section-->

  </head>
  <!--<body onbeforeunload="CloseSession();">-->
  <body>
    <form name="frmMain" id="frmMain" runat="server">
      <%    
          string lang = "";
          if ((Request.QueryString["lang"] != null) && (Request.QueryString["lang"] != ""))
          {
            lang = Request.QueryString["lang"].ToString();
          }

          if (Session["login"] != null) 
          {
              if (((bool)Session["login"] == true) && (Request.QueryString["Finish"] != "Yes"))
              {
                Response.Redirect("html/htmlEditor.aspx?lang=" + lang + "&pagename=HomeFlightSearch&pageback=../default");
              }
          }
      %>

      <asp:ScriptManager ID="smService" runat="server" ScriptMode="Release" EnablePartialRendering="false">
        <Services>
          <asp:ServiceReference Path="WebService/B2cService.asmx" />
          <asp:ServiceReference Path="WebService/WebUIRender.asmx" />
        </Services>
      </asp:ScriptManager>

      <!--Fading-->
      <input type="hidden" id="hdLang" name="hdLang"/>
      <div id="dvProgressBar" class="DisableWindow"></div>

      <!--Start Error Box-->
		<div id="dvMessageBox" class="ErrorBox">
			<div class="xouter">
				<div class="xcontainer">
					<div class="xinner LoadingBox">
						
							<div class="ErrorBoxHeader"></div>
							<div id="dvMessageIcon" class="ErrorBoxMid">
								<img src="App_Themes/Default/Images/alert.png" alt=""/>
							</div>
							<div id="dvErrorMessage" class="ErrorBoxBot RedFontB">
								Error Text
							</div>
							<div class="clear-all"></div>
							<div class="AjaxButton" onclick="CloseMessageBox();">
								<div class="BTN-Left"></div>
								<div class="BTN-Middle" id="dvMessageButtonText">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_13", "CLOSE", _stdLanguage)%>
								</div>
								<div class="BTN-Right"></div>
								<div class="clear-all"></div>
							</div>
						
					</div>
				</div>
			</div>
		</div>
      <!--End Error Box-->

      <!--progress bar-->
      <div id="dvFormHolder" class="IndicatorPax"></div>


      <div id="dvLoad" class="ProgressBox">
        <div class="xouter">
          <div class="xcontainer">
            <div class="xinner LoadingBox">
              <div class="LoadingBoxHeader"></div>
              <div class="clear-all" ></div>

              <div class="LoadingBoxMid">
                <img src="App_Themes/Default/Images/progressbar.gif" alt=""/>
              </div>

              <div class="LoadingStep RedTopic">
                <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_39", "Progress Message 1", _stdLanguage)%>
              </div>
              <div class="LoadingStep RedTopic">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_48", "Progress Message 2", _stdLanguage)%>
              </div>
              <div class="LoadingStep RedTopic">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_28", "Progress Message 3", _stdLanguage)%>
              </div>
              <div class="LoadingStep RedTopic">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_83", "Progress Message 4", _stdLanguage)%>
              </div>
              <div class="LoadingBoxFoot"></div>

              <div class="clear-all"></div>

              <div class="advertisement">
                <img src="App_Themes/Default/Images/advertisement.jpg" alt="Hotel worldwide" title="Hotel worldwide" />

                <div class="advertisementDetail">
                  <div class="texthead">
                    Hotels worldwide
                  </div>
                  <div>
                    Choose from over 250,000 hotels worldwide.
                    Online Hotel Discount Reservations Worldwide.
                  </div>
                </div>
                <div class="clear-all"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- <div id="dvLoad" class="Indicator" style="display:none;">
			<img src="App_Themes/Default/Images/mainlogo.png" alt="Style Airways" title="Style Airways"/>
			<div class="clear-all" ></div>
            <img src="App_Themes/Default/Images/progressbar.gif" alt=""/>    
        </div> -->
      <!--Loading Bar-->

      <div class="clear-all"></div>

		<div id="dvLoadBar" class="ProgressBarBox">
			<div class="xouter">
				<div class="xcontainer">
					<div class="xinner LoadingBox">
						<div class="LoadingBoxHeader"></div>
						<div class="LoadingBoxMid">
							<img src="App_Themes/Default/Images/progressbar.gif" alt=""/>
						</div>
						<div class="bookinginform">
							<div class="RedTopic bookinginform">Thank you for booking with Style Airways</div>
							<div class="RedFont">
								<div>Please wait while we process your booking or payment process.</div>
								<div>This will take approximately 1 minute  and </div>
								<div>during this time you should not use any of the buttons on your browser.</div>
							</div>
						</div>
						<div class="LoadingBoxFoot"></div>
					</div>
				</div>
			</div>
		</div>

      <div class="Wrapper">


        <div class="Header">
          <div class="mainlogo">
            <a href="http://www.flywithstyle.com/fws/b2c/default.aspx">
              <img src="App_Themes/Default/Images/mainlogo.png" alt="Style Airways" title="Style Airways" />
            </a>
          </div>

          <div class="HeaderMenu">
            <!--Menu Section-->
            <div id="dvMenu" class="menu" runat="server"></div>
          </div>
        </div>
        <div class="clear-all"></div>

        <div class="TopMenu">
          <ul>
            <li>
              <a href="javascript:loadHome();">
                <%=tikAeroB2C.Classes.Language.Value("Menu_7", "Home", _stdLanguage)%>
              </a>
            </li>

            <li>
              <a href="#">
                <%=tikAeroB2C.Classes.Language.Value("Menu_8", "Flight Info", _stdLanguage)%>
              </a>
            </li>
            <li>
              <a href="#">
                <%=tikAeroB2C.Classes.Language.Value("Menu_9", "Timetable", _stdLanguage)%>
              </a>
            </li>
            <li>
              <a href="#">
                <%=tikAeroB2C.Classes.Language.Value("Menu_10", "About Us", _stdLanguage)%>
              </a>
            </li>
            <li>
              <a href="#">
                <%=tikAeroB2C.Classes.Language.Value("Menu_11", "FAQs", _stdLanguage)%>
              </a>
            </li>
            <li>
              <a href="javascript:LoadRegistration();">
                <%=tikAeroB2C.Classes.Language.Value("Menu_12", "Join Frequent Flyer", _stdLanguage)%>
              </a>
            </li>
          </ul>
        </div>

          <Language:Language runat="server" ID="lnLanguage" /> 
        <div class="clear-all"></div>

        <div class="content-default">


          <!--Start Main Contend-->
          <div id="dvContainer" runat="server"></div>
          <!--End Main Contend-->

          <!--Start FFP Login Menu-->
          <div id="dvClientInfo" style="display:none;"></div>
          <!--End FFP Login Menu-->

            <!--Display fare summary for left menu-->
            <div id="dvFareSummary" style="display:none;"></div>
            <!-- Start Booking Summary -->
            <div id="dvAvaiQuote" class="YourSelection" style="display:none;">
                <div class="boxheader">
                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_Summary_1", "Your Selection", _stdLanguage)%>
                </div>
        
                <div id="dvFareLine_Outward"></div>
                <div id="dvFareLine_Return"></div>
        
                <div class="summary">
		            <div class="summary_total">
                        <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_Summary_2", "Total Price", _stdLanguage)%>
                    </div>
		            <div id="dv_SubtotalFare" class="summary_price">0</div>
	            </div>
            </div>
            <!--Display fare summary for left menu-->

          <!--Start Avai Section (Find at SearchAvailability.ascx)-->
          <div id="dvAvailabilitySearch" class="SearchBox">
            <AvailabilitySearch:AvailabilitySearch runat="server" ID="AvailabilitySearch1" />
            <div class="clear-all"></div>
          </div>
          <!--End Avai Section-->


          <div class="clear-all">
            <!--control bottom "WrapperBody"-->
          </div>
        </div>
        <!--End WrapperBody-->

        <div class="Footer">
          <div class="copyright">
            &copy; <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_17", "Copyright 2011 Style Airways. All Rights Reserved.", _stdLanguage)%>
          </div>

          <div class="SystemBy">
            <div class="systemtext">
                <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_19", "System by", _stdLanguage)%>
            </div>
            <div class="Mercator">
              <a href="http://www.mercator.com" target="_blank">
                <img src="App_Themes/Default/Images/Mercator-Logo.png" alt="System by Mercator" title="System by Mercator" />
              </a>
            </div>
          </div>
        </div>

        <div class="clear-all">
          <!--control bottom "Wrapper"-->
        </div>
      </div>
      <!--End Wrapper-->

    
    <!--START script for SearchAvailability calendar Only--> 
    <script language="javascript" type="text/javascript">

        var ResultType = "<%= ResultType %>";
        var CalendarToControls = "<%= CalendarToControls %>";
        var DateFormat = "<%= DateFormat %>";

        $(function () {

            $('#calendar').daterangepicker().click(function () {
                $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
            });

            $('#calendar2').daterangepicker().click(function () {
                $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
            });

        });

    </script>
    <script type="text/javascript" src="Scripts/JQuery/jquery-ui-1.7.1.custom.min.js" defer></script>
    <script type="text/javascript" src="Scripts/JQuery/daterangepicker.jQuery.js" defer></script>
     <!--END script for SearchAvailability calendar Only-->

    </form>
    <script type="text/javascript">
        //**********************************************************
        //Call function when every DOM is load
        $(document).ready(FormLoadOperation);

  </script>
  </body>
  
</html>
