﻿function LoadMyBooking() {
    if (MultipleTab.IsCorrectTab()) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadMyBooking(SuccessLoadMyBooking, showError, showTimeOut);
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}

function SuccessLoadMyBooking(result) {
    if (result == "{002}") {
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        var objContainer = document.getElementById("dvContainer");

        objContainer.innerHTML = result;

        DisplayQuoteSummary("", "", "");
        ShowSearchPannel(true);
        ShowProgressBar(false);
        objContainer = null;
    }
}
function LoadBookingDetail(bookingId, errorMSG) {
    if (bookingId == '')
        ShowMessageBox(errorMSG, 0, '');
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadBookingDetail(bookingId, SuccessLoadBookingDetail, showError, showTimeOut);
    }
}
function SuccessLoadBookingDetail(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objContainer = document.getElementById("dvContainer");

            objContainer.innerHTML = result;

            ShowSearchPannel(false);
            window.scrollTo(0, 0);
            objContainer = null;
        }
    }
}
function GetPagingFFP(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingFFP(pageindex, SuccessGetPaging, showError, showTimeOut);
}

function GetHistoryBooking(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingBooking(pageindex, 'true', SuccessGetPaging, showError, showTimeOut);
}

function SuccessGetPaging(result) {
    ShowProgressBar(false);
    var objContainer = document.getElementById("ctl00_dvHistory");

    objContainer.innerHTML = result;
    objContainer = null;
}

function GetLifeBooking(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingBooking(pageindex, 'false', SuccessGetLifeBooking, showError, showTimeOut);
}

function SuccessGetLifeBooking(result) {
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            ShowProgressBar(false);
            var objContainer = document.getElementById("ctl00_dvLifeBooking");

            objContainer.innerHTML = result;
            objContainer = null;
        }
    }
}
function ShowHistory() {
    HideLiveBooking();
    document.getElementById("historyBooking").style.display = "block";
    document.getElementById("HistoryTab1").className = 'TabConererLeft';
    document.getElementById("HistoryTab2").className = 'TabConererContent';
    document.getElementById("HistoryTab3").className = 'TabConererRight';

}

function HideHistory() {
    document.getElementById("historyBooking").style.display = "none";
    document.getElementById("HistoryTab1").className = 'TabActiveConererLeft';
    document.getElementById("HistoryTab2").className = 'TabActiveConererContent';
    document.getElementById("HistoryTab3").className = 'TabActiveConererRight';
}

function ShowLiveBooking() {
    document.getElementById("liveBooking").style.display = "block";
    document.getElementById("LiveTab1").className = 'TabConererLeft';
    document.getElementById("LiveTab2").className = 'TabConererContent';
    document.getElementById("LiveTab3").className = 'TabConererRight';

    HideHistory();
}

function HideLiveBooking() {
    document.getElementById("liveBooking").style.display = "none";
    document.getElementById("LiveTab1").className = 'TabActiveConererLeft';
    document.getElementById("LiveTab2").className = 'TabActiveConererContent';
    document.getElementById("LiveTab3").className = 'TabActiveConererRight';
}
function MyBookingSearch() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.MyBookingSearch(document.getElementById('ctl00_txtBookingCode').value, SuccessMyBookingSearch, showError, showTimeOut);
}
function SuccessMyBookingSearch(result) {
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objContainer = document.getElementById("dvSearch");
            var objBox = document.getElementById("dvResultBox");
            var objBookingBox = document.getElementById("dvBookingBox");

            objBox.style.display = 'block';
            objBookingBox.style.display = 'none';
            objContainer.innerHTML = result;

            ShowSearchPannel(false);
            ShowProgressBar(false);
            objContainer = null;
        }
    }

}
function SubmitEnterSearch(myfield, e) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        MyBookingSearch();
        return false;
    }
    else
        return true;
}