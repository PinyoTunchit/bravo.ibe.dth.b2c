﻿function GetQuoteSummary(objChk, strType) {
    var objOutwards = document.getElementsByName("Outward");
    var objReturns = document.getElementsByName("Return");
    var lenOutward = objOutwards.length;
    var lenReturn = objReturns.length;
    var objClassName = "";

    var sOutward = "";
    for (var i = 0; i < lenOutward; i++) {
        if (objOutwards[i] != null) {
            //alert("objOutwards :" + objOutwards[i].checked);
            sOutward = objOutwards[i].id;
            if (objOutwards[i].checked == true) {
                document.getElementById(sOutward.substring(3)).className = "SearchFlightSelection";
            }
            else {
                objClassName = "BodyCOL" + (5 + parseInt(sOutward.substring(sOutward.length - 1)));
                document.getElementById(sOutward.substring(3)).className = objClassName;
            }
        }
    }

    var sReturn = "";
    for (var i = 0; i < lenReturn; i++) {
        if (objReturns[i] != null) {
            //alert("objReturns :" + objReturns[i].checked);
            sReturn = objReturns[i].id;
            if (objReturns[i].checked == true) {
                document.getElementById(sReturn.substring(3)).className = "SearchFlightSelection";
            }
            else {
                objClassName = "BodyCOL" + (5 + parseInt(sReturn.substring(sReturn.length - 1)));
                document.getElementById(sReturn.substring(3)).className = objClassName;
            }
        }
    }
    var strXml = "";
    var strParam = "";
    var strAirlineRcd = "";
    var strFlightNumber = "";
    var strFlightId = "";
    var strOrigin = "";
    var strDest = "";
    var strFareId = "";
    var strFlightConnectId = "";
    var dtDepartture = "";
    var dtArrival = "";
    var departTime = "";
    var arrivalTime = "";

    var strTransitAirlineRcd = "";
    var strTransitFlightNumber = "";
    var dtTransitDepartureDate = "";
    var dtTransitArrivalDate = "";
    var transitDepartTime = "";
    var transitArrivalTime = "";
    var strTransitAirport;
    var strTransitFareId = "";
    var strBookingClassRcd = "";
    var currency_rcd = "";

    var arrFlightInfo;
    var arrFlightDetail;

    if (objChk.checked == true) {
        strParam = objChk.value;
        if (strParam.length > 0) {
            arrFlightInfo = strParam.split("|");
            for (var i = 0; i < arrFlightInfo.length; i++) {
                arrFlightDetail = arrFlightInfo[i].split(":");
                if (arrFlightDetail[0] == "flight_id") {
                    strFlightId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "airline_rcd") {
                    strAirlineRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "flight_number") {
                    strFlightNumber = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "origin_rcd") {
                    strOrigin = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "destination_rcd") {
                    strDest = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "fare_id") {
                    strFareId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_flight_id") {
                    strFlightConnectId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "departure_date") {
                    dtDepartture = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "planned_departure_time") {
                    departTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "planned_arrival_time") {
                    arrivalTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_airline_rcd") {
                    strTransitAirlineRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_flight_number") {
                    strTransitFlightNumber = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_departure_date") {
                    dtTransitDepartureDate = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_planned_departure_time") {
                    transitDepartTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_planned_arrival_time") {
                    transitArrivalTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_airport_rcd") {
                    strTransitAirport = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_fare_id") {
                    strTransitFareId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "booking_class_rcd") {
                    strBookingClassRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "arrival_date") {
                    dtArrival = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_arrival_date") {
                    dtTransitArrivalDate = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "currency_rcd") {
                    currency_rcd = arrFlightDetail[1];
                }
            }
            if (strOrigin.length > 0) {
                strXml = "<flights>" +
                            "<flight>" +
                                "<flight_id>" + strFlightId + "</flight_id>" +
                                "<airline_rcd>" + strAirlineRcd + "</airline_rcd>" +
                                "<flight_number>" + strFlightNumber + "</flight_number>" +
                                "<origin_rcd>" + strOrigin + "</origin_rcd>" +
                                "<destination_rcd>" + strDest + "</destination_rcd>" +
                                "<fare_id>" + strFareId + "</fare_id>" +
                                "<transit_airline_rcd>" + strTransitAirlineRcd + "</transit_airline_rcd>" +
                                "<transit_flight_number>" + strTransitFlightNumber + "</transit_flight_number>" +
                                "<transit_flight_id>" + strFlightConnectId + "</transit_flight_id>" +
                                "<departure_date>" + dtDepartture + "</departure_date>" +
                                "<arrival_date>" + dtArrival + "</arrival_date>" +
                                "<arrival_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtArrival)) + "</arrival_day>" +
                                "<departure_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtDepartture)) + "</departure_day>" +
                                "<planned_departure_time>" + departTime + "</planned_departure_time>" +
                                "<planned_arrival_time>" + arrivalTime + "</planned_arrival_time>" +
                                "<transit_departure_date>" + dtTransitDepartureDate + "</transit_departure_date>" +
                                "<transit_departure_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtTransitDepartureDate)) + "</transit_departure_day>" +
                                "<transit_arrival_date>" + dtTransitArrivalDate + "</transit_arrival_date>" +
                                "<transit_arrival_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtTransitArrivalDate)) + "</transit_arrival_day>" +
                                "<transit_planned_departure_time>" + transitDepartTime + "</transit_planned_departure_time>" +
                                "<transit_planned_arrival_time>" + transitArrivalTime + "</transit_planned_arrival_time>" +
                                "<transit_airport_rcd>" + strTransitAirport + "</transit_airport_rcd>" +
                                "<transit_fare_id>" + strTransitFareId + "</transit_fare_id>" +
                                "<booking_class_rcd>" + strBookingClassRcd + "</booking_class_rcd>" +
                                "<currency_rcd>" + currency_rcd + "</currency_rcd>" +
                            "</flight>" +
                        "</flights>";
                //Call Webservice
                tikAeroB2C.WebService.B2cService.GetQuoteSummary(strXml,
                                                                 strType,
                                                                 SuccessGetQuoteSummary,
                                                                 showError,
                                                                 strType);
            }
        }
    }

}
function SuccessGetQuoteSummary(result, strType) {

    if (result == "{100}" || result == "{101}" || result == "{004}" || result == "{104}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {

        if (strType == "Outward") {
            DisplayQuoteSummary("", result, "");
        }
        else {
            DisplayQuoteSummary("", "", result);
        }
        //Initialize Collape
        InitializeFareSummaryCollapse(strType);
    }

}
function DisplayQuoteSummary(strFareSummaryHtml, strOutwardHtml, strReturnHtml) {

    var objDvFareSummary = document.getElementById("dvFareSummary");
    var objDvAvaiFareSummary = document.getElementById("dvAvaiQuote");
    var objOFareLine = document.getElementById("dvFareLine_Outward");
    var objRFareLine = document.getElementById("dvFareLine_Return");
    var objSummary = document.getElementById("dvYourSelection");

    if (strFareSummaryHtml.length == 0 & strOutwardHtml.length == 0 & strReturnHtml.length == 0) {

        objSummary.style.display = "none";
        if (objOFareLine != null) {
            objOFareLine.innerHTML = "";
        }
        if (objRFareLine != null) {
            objRFareLine.innerHTML = "";
        }
        if (objDvFareSummary != null) {
            objDvFareSummary.innerHTML = "";
        }
    }
    else {

        objSummary.style.display = "block";

        if (strFareSummaryHtml.length > 0) {
            if (objDvFareSummary != null) {
                objDvFareSummary.style.display = "block";
                objDvFareSummary.innerHTML = strFareSummaryHtml;
            }
        }
        else {
            if (objDvFareSummary != null) {
                objDvFareSummary.style.display = "none";
                objDvFareSummary.innerHTML = "";
            }
        }

        if (objOFareLine != null && objRFareLine != null) {
            if (strOutwardHtml.length > 0 | strReturnHtml.length > 0) {
                if (objDvAvaiFareSummary != null) {
                    objDvAvaiFareSummary.style.display = "block";

                    if (strOutwardHtml.length > 0) {
                        objOFareLine.innerHTML = strOutwardHtml;
                    }

                    if (strReturnHtml.length > 0) {
                        objRFareLine.innerHTML = strReturnHtml;
                    }
                }
            }
            else {
                if (objDvAvaiFareSummary != null) {
                    objDvAvaiFareSummary.style.display = "none";

                    objOFareLine.innerHTML = "";
                    objRFareLine.innerHTML = "";
                }
            }
        }
        //Calculate totoal summary.
        CalculateTotalFare();
    }
}
function CalculateTotalFare() {
    var objhdTotal = document.getElementsByName("hdSubTotal");
    var objDvTotal = document.getElementById("dvTotalFareSummary");
    var objDdSign = document.getElementById("hdCurrencySign");

    var dclTotal = 0;
    if (objhdTotal != null) {
        for (var i = 0; i < objhdTotal.length; i++) {
            dclTotal = dclTotal + parseFloat("0" + GetControlValue(objhdTotal[i]));
        }
    }
    if (objDvTotal != null && objDdSign != null) {
        objDvTotal.innerHTML = GetControlValue(objDdSign) + " " + AddCommas(dclTotal);
    }
}
//*****************************************************
//  Activate Collapse to control.
function InitializeFareSummaryCollapse(strType) {

    //Fare Making Collapse
    $(function () {
        $("#ul_" + strType + "_FareInfo").jqcollapse({
            slide: true,
            speed: 200,
            easing: '',
            hide: false
        });
    });
    
    //Tax Making Collapse
    $(function () {
        $("#ul_" + strType + "_TaxInfo").jqcollapse({
            slide: true,
            speed: 200,
            easing: '',
            hide: false
        });
    });
    
    //Making Fee Collapse
    $(function () {
        $("#ul_" + strType + "_FeeInfo").jqcollapse({
            slide: true,
            speed: 200,
            easing: '',
            hide: false
        });
    });
}
function GetSessionQuoteSummary() {
    tikAeroB2C.WebService.B2cService.GetSessionQuoteSummary(SuccessGetSessionQuoteSummary, showErrorPayment, showTimeOutPayment);
}
function SuccessGetSessionQuoteSummary(result) {

    if (result.length > 0) {
        DisplayQuoteSummary(result, "", "");
        //Initialize Summary collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
}