﻿function SuccessLoadCob(result) {
    if (result == "{002}") {
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else if (result == "{000}") {
        LoadSecure(true);
    }
    else {
        var objContainer = document.getElementById("dvContainer");

        objContainer.innerHTML = result;

        HideHeaderMenu(true);
        ShowSearchPannel(false);
        ShowProgressBar(false);
        objContainer = null;
    }
}

function LoadCob(bParameter, bookingId) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadCob(bParameter, bookingId, SuccessLoadCob, showError, showTimeOut);
}