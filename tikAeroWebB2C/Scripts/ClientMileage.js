﻿function LoadMilleageDetail() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadMilleageDetail(SuccessLoadMilleageDetail, showError, showTimeOut);
}
function SuccessLoadMilleageDetail(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;

    DisplayQuoteSummary("", "", "");
    ShowProgressBar(false);
    objContainer = null;
}
