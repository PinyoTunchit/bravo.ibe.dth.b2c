function Tab() {
    this.tabId = "";
    this.status = "";
}

function checkSessionTab(isSigleTab, redirectURL) {
        var mainTab = getMainTab();
        var tab = getSessionTab();
        var tabs = getLocalTabs();

        if (tab == null && tabs == null)
            createTab();
        else if (tab == null && tabs != null && mainTab == null)
            createTab();
        else if (tab == null && tabs != null && mainTab != null)
            createSessionTab();
        else if (tab != null && tabs != null && mainTab != null)
            updateSessionTab(tab);
        else if (tab != null && tabs != null && mainTab == null)
            updateTab(tab);
        else
            updateTab(tab);

        mainTab = getMainTab();
        tab = getSessionTab();
        tabs = getLocalTabs();
        var isMain = isMainTab(mainTab, tab);
        if (isSigleTab == true && isMain == false) {
            //clearLocalTabs();
            //setSessionUnload();
            window.location = redirectURL;
        }

//        if (mainTab != null) document.writeln("maintab==> id:" + mainTab.tabId + ", status:" + mainTab.status + "<br/>");
//        if (tab != null) document.writeln("session tab==> id:" + tab.tabId + ", status:" + tab.status + "<br/>");
//        document.writeln("isMain: " + isMain + "<br/>");
}

function createTab() {
    var tab = new Tab();
    tab.tabId = generateGuid();
    tab.status = "loaded";
    setSessionTab(tab);
    setLocalTabs(tab);
}

function updateTab(tab) {
    tab.status = "loaded";
    setSessionTab(tab);
    setLocalTabs(tab);
}

function createSessionTab() {
    var tab = new Tab();
    tab.tabId = generateGuid();
    tab.status = "loaded";
    setSessionTab(tab);
}

function updateSessionTab(tab) {
    tab.status = "loaded";
    setSessionTab(tab);
}

function getSessionTab() {
    return JSON.parse(sessionStorage.getItem("tabId"));
}

function setSessionTab(tab) {
    sessionStorage.setItem("tabId", JSON.stringify(tab));
}

function getLocalTabs() {
    var tabs = localStorage.getItem("tabs");
    if (tabs != null) { tabs = JSON.parse(tabs); }
    return tabs;
}

function getMainTab() {
    var tab = null;
    var tabs = getLocalTabs();
    if (tabs != null) {
        for (var i = 0; i < tabs.length; i++) {
            if (tabs[i].status == "loaded") {
                tab = tabs[i];
                break;
            }
        }
    }
    return tab;
}

function isMainTab(mainTab, sessionTab) {
    var result = false;
    if (mainTab != null && sessionTab != null)
        if (mainTab.tabId == sessionTab.tabId) result = true;
    return result;
}

function setLocalTabs(tab) {
    var tabs = getLocalTabs();
    if (tabs != null) {
        for (var i = 0; i < tabs.length; i++) {
            var t = tabs[i];
            if (tab.tabId == t.tabId) {
                tabs.splice(i, 1);
                i--;
            }
        }
        tabs.push(tab);
    }
    else {
        tabs = new Array(tab);
    }
    localStorage.setItem("tabs", JSON.stringify(tabs));
}

function clearLocalTabs() {
    localStorage.removeItem("tabs");
}

function generateGuid() {
    var result, i, j;
    result = '';
    for (j = 0; j < 32; j++) {
        if (j == 8 || j == 12 || j == 16 || j == 20)
            result = result + '-';
        i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
        result = result + i;
    }
    return result;
}

function setSessionUnload() {
    var tab = this.getSessionTab();
    if (tab == null) return;
    tab.status = "unloaded";
    this.setSessionTab(tab);
    this.setLocalTabs(tab);
}

function removeTabsUnload() {
    var tabs = getLocalTabs();
    if (tabs == null) return;
    for (var i = 0; i < tabs.length; i++) {
        var tab = tabs[i];
        if (tab.status == "unloaded") {
            tabs.splice(i, 1);
            i--;
        }
    }
    localStorage.setItem("tabs", JSON.stringify(tabs));
}