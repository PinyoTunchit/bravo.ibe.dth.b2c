﻿//***************************************************
//  Select Passenger Function Section
//***************************************************
function GetClient(position, bGetMyself) {

    if (position == 0) {
        position = iPaxSelectPosition;
    }
    var objLastname = document.getElementById("hdLastname_" + position);
    var objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    var objClientNo = document.getElementById("ctl00_txtClientNumber");

    ShowProgressBar(true);

    if (GetControlValue(objClientNo).length > 0 && bGetMyself == false) {
        tikAeroB2C.WebService.B2cService.GetClient(GetControlValue(objClientNo), "", true, SuccessGetClient, showError, showTimeOut);
    }
    else if (bGetMyself == true && (GetControlValue(objClientProfileId).length > 0 || GetControlValue(objClientProfileId) != "00000000-0000-0000-0000-000000000000")) {

        iPaxSelectPosition = position;
        var objClientNumber = document.getElementById(FindControlName("input", "txtClientNumber"));

        tikAeroB2C.WebService.B2cService.GetClient(GetControlValue(objClientNumber), GetControlValue(objLastname), false, SuccessGetClientMyself, showError, showTimeOut);
        objClientNumber = null;
    }
    else {
        //Field client number is empty.
        //Show error message.
        LockPassengerInput(false);
        ShowProgressBar(false);
        ClearPassengerList();
    }
    objClientNo = null;
    objLastname = null;
    objClientProfileId = null;
}

function SuccessGetClient(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objMessage = document.getElementById("dvFormHolder");
            var objContainer = document.getElementById("dvProgressBar");

            //Insert passenger form content.
            objMessage.innerHTML = result;

            var objCheckBox = document.getElementsByName("chkClient");
            if (objCheckBox.length > 1) {
                //Show Fading.
                objContainer.style.display = "block";
                objMessage.style.display = "block";
                //Get Total Amount of passenger
                document.getElementById("dvTotalPax").innerHTML = 1;
            }
            else {
                //Find only myself.
                objContainer.style.display = "none";
                objMessage.style.display = "none";
                //Default select passenger.
                if (objCheckBox.length > 0)
                { objCheckBox[0].checked = true; }
                GetSelectedClientPosition();
            }
            objCheckBox = null;

            objMessage = null;
        }
    }

}
function SuccessGetClientMyself(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objMessage = document.getElementById("dvFormHolder");
            var objContainer = document.getElementById("dvProgressBar");

            //Insert passenger form content.
            objMessage.innerHTML = result;

            var objCheckBox = document.getElementsByName("chkClient");
            if (objCheckBox.length > 0) {
                //Find only myself.
                objContainer.style.display = "none";
                objMessage.style.display = "none";
                //Default select passenger.
                if (objCheckBox.length > 0)
                { objCheckBox[0].checked = true; }
                GetSelectedClientPosition();
            }

            objCheckBox = null;
            objMessage = null;
        }
    }

}
function TotalPassengerAvailable() {
    var objPax = document.getElementsByName("txtLastname");
    var pCount = 0;

    for (var i = 0; i < objPax.length; i++) {
        if (objPax[i].value.length == 0) {
            pCount++;
        }
    }
    return pCount;
}