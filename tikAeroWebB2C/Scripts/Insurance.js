function ShowInsurance()
{
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ShowInsurance(SuccessShowInsurance, showError, showTimeOut);
}

function SuccessShowInsurance(result)
{
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");
    
    ShowProgressBar(false);
    
    //Show Fading.
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    
    //Insert passenger form content.
    objMessage.innerHTML = result;
    objMessage = null;
    objContainer = null;
}

function CloseInsurance(isNo)
{
    var objDisable = document.getElementById("dvProgressBar");
    var objMessage = document.getElementById("dvFormHolder");
    if (isNo) {
        var objrdNo = document.getElementById("rdNo");
        objrdNo.checked = true;
        objrdNo = null;
        ClearInsuarance();
    }
    
    objDisable.style.display = "none";
    objMessage.style.display = "none";
    objMessage.innerHTML = "";
    
    objDisable = null;
    objMessage = null;
}

function SelectInsuarance(productid, premiumProduct, label) 
{
    var objInsuranceCost = document.getElementById("dvInsuranceCost");
    var objhdProductID = document.getElementById("hdProductID");
    var objInsuranceLabel = document.getElementById("dvInsuranceLabel");
    var objrdYes = document.getElementById("rdYes");
    var objdvPayment = document.getElementById("dvPayment");
    var objdvInsurance = document.getElementById("dvInsurance"); 

    objrdYes.checked = true;

    objInsuranceCost.innerHTML = premiumProduct;
    objInsuranceLabel.innerHTML = label;
    objhdProductID.value = productid;
    objdvPayment.style.display = "block";
    objdvInsurance.style.display = "block";

    objInsuranceCost = null;
    objhdProductID = null;
    objInsuranceLabel = null;
    objrdYes = null;
    objdvPayment = null;

    CloseInsurance(false);
    ShowOnlyOnePaymentTab('cc');
    showhidelayer('CreditCard'); 
    activateCreditCardControl();
}

function NoInsuarance() {
    var objbtnConfirm = document.getElementById("btnConfirm");
    var objdvPayment = document.getElementById("dvPayment");

    objbtnConfirm.style.display = "none";
    dvPayment.style.display = "block";

    objbtnConfirm = null;
    objdvPayment = null;

    ClearInsuarance();
}

function UseInsuarance() {
    var objbtnConfirm = document.getElementById("btnConfirm");
    var objhdProductID = document.getElementById("hdProductID");
    var objdvPayment = document.getElementById("dvPayment");

    if (objhdProductID.value == "") {
        dvPayment.style.display = "none";
    }

    objbtnConfirm.style.display = "block";

    objbtnConfirm = null;
    objdvPayment = null;
    objhdProductID = null;

}

function ClearInsuarance() {
    var objhdProductID = document.getElementById("hdProductID");
    var objInsuranceCost = document.getElementById("dvInsuranceCost");
    var objInsuranceLabel = document.getElementById("dvInsuranceLabel");
    var objdvInsurance = document.getElementById("dvInsurance"); 

    objhdProductID.value = "";
    objInsuranceCost.innerHTML = "";
    objInsuranceLabel.innerHTML = "";
    objdvInsurance.style.display = "none";

    objhdProductID = null;
    objInsuranceCost = null;
    objInsuranceLabel = null;
    objdvInsurance = null;

    ShowAllPaymentTab();
}

function ShowOnlyOnePaymentTab(paymentTab) {
    var objdvCCTab = document.getElementById("dvCCTab");
    var objdvVoucherTab = document.getElementById("dvVoucherTab");
    var objdvBookNowTab = document.getElementById("dvBookNowTab");
    var objdvExternal = document.getElementById("dvExternal");

    objdvCCTab.style.display = "none";
    objdvVoucherTab.style.display = "none";
    objdvBookNowTab.style.display = "none";
    objdvExternal.style.display = "none";

    switch (paymentTab) {
        case "cc":
            objdvCCTab.style.display = "block";
            break;
        case "vc":
            objdvVoucherTab.style.display = "block";
            break;
        case "bn":
            objdvBookNowTab.style.display = "block";
            break;
        case "ex":
            objdvExternal.style.display = "block";
            break;
    }

    objdvCCTab = null;
    objdvVoucherTab = null;
    objdvBookNowTab = null;
    objdvExternal = null;
}

function ShowAllPaymentTab() {
    var objdvCCTab = document.getElementById("dvCCTab");
    var objdvVoucherTab = document.getElementById("dvVoucherTab");
    var objdvBookNowTab = document.getElementById("dvBookNowTab");
    var objdvExternal = document.getElementById("dvExternal");

    objdvCCTab.style.display = "block";
    objdvVoucherTab.style.display = "block";
    objdvBookNowTab.style.display = "block";
    objdvExternal.style.display = "block";

    objdvCCTab = null;
    objdvVoucherTab = null;
    objdvBookNowTab = null;
    objdvExternal = null;
}

function ShowInsuranceSummary() {
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");
    var objInsuarance = document.getElementById("hdInsuarance");

    if (objInsuarance != null) {
        ShowProgressBar(false);

        //Show Fading.
        objContainer.style.display = "block";
        objMessage.style.display = "block";

        objMessage.innerHTML = "<div class='xouter'>" + objInsuarance.innerHTML + "</div>";
    }
    objMessage = null;
    objContainer = null;
    objInsuarance = null;
}

function ClosePopup() {
    var objDisable = document.getElementById("dvProgressBar");
    var objMessage = document.getElementById("dvFormHolder");

    objDisable.style.display = "none";
    objMessage.style.display = "none";
    objMessage.innerHTML = "";

    objDisable = null;
    objMessage = null;
}
