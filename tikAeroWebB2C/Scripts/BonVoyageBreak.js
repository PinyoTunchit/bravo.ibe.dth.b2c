function LoadBreaks(cont)
{
    ShowProgressBar();
    //tikAeroB2C.WebService.B2cService.LoadBreaks(SuccessLoadBreaks, showError, showTimeOut);
    tikAeroB2C.WebService.B2cService.LoadHtml(cont, SuccessLoadBreaks, showError, showTimeOut);
}

function SuccessLoadBreaks(result)
{   
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    
    scroll(0,0);
    
    ShowPannel(true);
    ShowProgressBar();
    obj = null;
}

function GetBreakContent(cont)
{
    ShowProgressBar();
    tikAeroB2C.WebService.B2cService.LoadHtml(cont, SuccessGetBreakContent, showError, showTimeOut);
}

function SuccessGetBreakContent(result)
{   
    var obj = document.getElementById("dvResultBreak");
    obj.innerHTML = result;
    
    scroll(0,0);
    
    ShowProgressBar();
    obj = null;
}

////////////////// BonVoyageBreak
function LoadBonVoyageBreak()
{
    ShowProgressBar();
    tikAeroB2C.WebService.B2cService.LoadBonVoyageBreak(SuccessLoadBonVoyageBreak, showError, showTimeOut);
}

function SuccessLoadBonVoyageBreak(result)
{   
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    
    scroll(0,0);
    
    ShowPannel(false);
    ShowProgressBar();
    obj = null;
}

function ValidateBonVoyageBreak(o){
	var Status = true;
	var e 	= '';
	var ln 	= document.getElementById(o+'txtLeadName');
	var ph 	= document.getElementById(o+'txtTelephone');
	var em 	= document.getElementById(o+'txtEmailAddress');
	var emReEnter = document.getElementById(o+'txtEmailAddressReEnter');	
	
	var an	= document.getElementById(o+'txtAdultNumber');
	var cn	= document.getElementById(o+'txtChildrenNumber');
	var ca =  document.getElementById(o+'txtChildrenAge');

	//somm add
	var ht1	= document.getElementById('ddlHotel1');
	var ht2	= document.getElementById('ddlHotel2');
	//end somm add

	if (trim(ln.value).length == 0)
	{
		Status =  false;
		e += "<li>Please supply a valid Lead Name.</li>";
		ln.className = 'error';
	}
	else
	{
	    ln.className = '';
	}
	
	if (trim(ph.value).length == 0){
		Status =  false;
		e += "<li>Please supply a valid Telephone No.</li>";
		ph.className = 'error';
	}
	else
	{
		ph.className = '';
	}	
		//--->  Email
	if (trim(em.value)==0)
	{
		Status =  false;
		e += "<li>Please supply a valid Email</li>";
		em.className = 'error';
	}
	else if (ValidEmail(em.value)!= true)
	{
		Status =  false;
		e += "<li>Please supply a valid Email format</li>";
		em.className = 'error';
    }
    else
    {
		em.className = '';
	}
    
    if (em.value != emReEnter.value)
    {
		Status =  false;
		e += "<li>Please supply a valid Confirm Email</li>";
		em.className = 'error';
	}	
	
    if ((ht1.options[ht1.selectedIndex].text) == (ht2.options[ht2.selectedIndex].text))
	{
	    Status =  false;
		e += "<li>Please select different hotel.</li>";
		ht2.className = 'error';
	}
	else
	{
		ht2.className = '';
	}

	if (trim(an.value).length == 0)
	{
		Status =  false;
		e += "<li>Please supply a valid Number of Adults.</li>";
		an.className = 'error';
	}
	else
	{
		an.className = '';
	}
	
	if (trim(cn.value).length != 0 && trim(ca.value).length == 0)
	{
		Status =  false;
		e += "<li>Please supply a valid Ages for Children under 12 Years.</li>";
		ca.className = 'error';
	}
	else
	{
		ca.className = '';
	}
	
	var Container = document.getElementById(o+'LabError');
	if (e.length > 0) 
	{		
		Container.innerHTML = '<ul>' + e + '</ul>';
	}
	else
	{
		Container.innerHTML = '';
	}
	
	return Status;
}

function SendMailBonVoyageBreak(o)
{
    Status = true;
    Status = ValidateBonVoyageBreak(o);

    if (Status==true)
    { 
        var Title = document.getElementById(o+'ddlTitle').options[document.getElementById(o+'ddlTitle').selectedIndex].value;
        Title =   Title.split("|")[0] ;
        var lead_name = document.getElementById(o+'txtLeadName').value;
        var telephone = document.getElementById(o+'txtTelephone').value;
        var email = document.getElementById(o+'txtEmailAddress').value;
        
        var ddlTravelingFrom = document.getElementById('ddlTravelingFrom').options[document.getElementById('ddlTravelingFrom').selectedIndex].text;        
        var ddlDestination = document.getElementById('ddlDestination').options[document.getElementById('ddlDestination').selectedIndex].text;
        var ddlHotel1 = document.getElementById('ddlHotel1').options[document.getElementById('ddlHotel1').selectedIndex].text;
        var ddlHotel2 = document.getElementById('ddlHotel2').options[document.getElementById('ddlHotel2').selectedIndex].text;
        var ddlRoomType = document.getElementById('ddlRoomType').options[document.getElementById('ddlRoomType').selectedIndex].text;
        var ddlOptions = document.getElementById('ddlOptions').options[document.getElementById('ddlOptions').selectedIndex].value;
        
        var txtAdultNumber = document.getElementById(o+'txtAdultNumber').value;
        var txtChildrenNumber = document.getElementById(o+'txtChildrenNumber').value;
        var txtChildrenAge = document.getElementById(o+'txtChildrenAge').value;             
        var txtNightNumber = document.getElementById(o+'txtNightNumber').value; 
        
        var ctlArrivalDate = BonVoyageBreakGetDate('3');
        var cmbPreferedArrivalTime = document.getElementById('cmbPreferedArrivalTime').options[document.getElementById('cmbPreferedArrivalTime').selectedIndex].value;
        var ctlDepatureDate = BonVoyageBreakGetDate('4');
        var cmbPreferedDepartureTime = document.getElementById('cmbPreferedDepartureTime').options[document.getElementById('cmbPreferedDepartureTime').selectedIndex].value;        
              
        var ddlCarHireGroup = '';//document.getElementById('ddlCarHireGroup').options[document.getElementById('ddlCarHireGroup').selectedIndex].value;
        var ddlRentalPeriod = '';//document.getElementById('ddlRentalPeriod').options[document.getElementById('ddlRentalPeriod').selectedIndex].value;
        var ddlPickUp = '';//document.getElementById('ddlPickUp').options[document.getElementById('ddlPickUp').selectedIndex].value;
        var ddlDropOff = '';//document.getElementById('ddlDropOff').options[document.getElementById('ddlDropOff').selectedIndex].value;        
        if (ddlCarHireGroup == "Not Required")
        {
            ddlRentalPeriod ="";
            ddlPickUp ="";
            ddlDropOff ="";
        }
        var txtAdditionalInformation = document.getElementById(o+'txtAdditionalInformation').value;   
        
        ShowProgressBar();
        tikAeroB2C.WebService.B2cService.SendMailBonVoyageBreak( Title, lead_name, telephone, email, ddlTravelingFrom, ddlDestination, ddlHotel1, ddlHotel2, ddlRoomType, ddlOptions, txtAdultNumber, txtChildrenNumber, txtChildrenAge, txtNightNumber, ctlArrivalDate, cmbPreferedArrivalTime, ctlDepatureDate, cmbPreferedDepartureTime, ddlCarHireGroup, ddlRentalPeriod, ddlPickUp, ddlDropOff, txtAdditionalInformation, SuccessSendMailBonVoyageBreak, showError, showTimeOut);
    }
}

function SuccessSendMailBonVoyageBreak(result)
{       
    ShowProgressBar();
    if (result=='1')
    {
       BreakMessageBox("Thank you for your enquiry,<br> we will get back to you shortly " , 1,'loadHome');            
    }
    else   
    {
       BreakMessageBox("Can't sendmail!!", 0, ''); 
    }  
}

function BreakMessageBox(strMessage, iType, callBack)
{
    var obj = parent.document.getElementById("dvProgressBar");
    var objErrorMsg = parent.document.getElementById("dvErrorMessage");
    var objIcon = parent.document.getElementById("dvMessageIcon");
    var objMessage = parent.document.getElementById("dvMessageBox");

    parent.document.getElementById("dvLoadBar").style.display = "none";
    parent.document.getElementById("dvLoad").style.display = "none";
    
    //Show and hide Icon.
    if(iType != 0)
    {objIcon.style.display = "none";}
        

    //Show or hide fading.
    if(obj.style.display == "none")
    {    
        objErrorMsg.innerHTML = strMessage;
        obj.style.display = "block";
        objMessage.style.display = "block";
        objMessage.setAttribute('CallBack', callBack);
    }
    else
    {
        // Callback Function
        if(objMessage.getAttribute("CallBack") != null && objMessage.getAttribute("CallBack") != '')
        {
            if(objMessage.getAttribute("CallBack") == 'loadHome')
            {
                obj.style.display = "none"; 
                objMessage.style.display = "none";
                loadHome();
            }
        }
        else
        {
            obj.style.display = "none"; 
            objMessage.style.display = "none";
        }
    }
    
    obj = null;
    objErrorMsg = null;
    objIcon = null;
    objMessage = null;    
}

function BonVoyageBreakGetDate(o)
{ 
    var ddlMY=document.getElementById('ddlMY_'+o);
    var ddlDate=document.getElementById('ddlDate_'+o);
    return ddlDate.options[ddlDate.selectedIndex].value+ '/'+ ddlMY.options[ddlMY.selectedIndex].value.substring(4) + '/' +ddlMY.options[ddlMY.selectedIndex].value.substring(0,4);
}

function SyncSelectWithValueHotel(selMaster, obj1name, obj2name)
{    
	var selectedValue = selMaster.options[selMaster.selectedIndex].text;
	var obj1 = document.getElementById(obj1name);
	var obj2 = document.getElementById(obj2name);
	switch(selectedValue)
	{
	    case "Guernsey":
	        //top dll 
	        obj1.options.length = 0;
	        obj1.options[0] = new Option("St Pierre Park Hotel", "St Pierre Park Hotel");
	        obj1.options[1] = new Option("The Old Government House Hotel & Spa", "The Old Government House Hotel & Spa");
	        obj1.options[2] = new Option("De Havelet Hotel", "De Havelet Hotel");
	        obj1.options[3] = new Option("Duke of Richmond", "Duke of Richmond");
	        obj1.options[4] = new Option("Hougue du Pommier Hotel", "Hougue du Pommier Hotel");
	        obj1.options[5] = new Option("La Villette Hotel", "La Villette Hotel");
	        obj1.options[6] = new Option("Les Rocquettes Hotel", "Les Rocquettes Hotel");
	        obj1.options[7] = new Option("Moore's Central Hotel", "Moore's Central Hotel");
	        obj1.options[8] = new Option("Peninsula Hotel", "Peninsula Hotel");

	        obj1.options[9] = new Option("Duke of Normandie Hotel", "Duke of Normandie Hotel");
	        obj1.options[10] = new Option("Le Ch\êne Hotel", "Le Ch\êne Hotel");

	        //below dll
	        obj2.options.length = 0;
	        obj2.options[0] = new Option("St Pierre Park Hotel", "St Pierre Park Hotel");
	        obj2.options[1] = new Option("The Old Government House Hotel & Spa", "The Old Government House Hotel & Spa");
	        obj2.options[2] = new Option("De Havelet Hotel", "De Havelet Hotel");
	        obj2.options[3] = new Option("Duke of Richmond", "Duke of Richmond");
	        obj2.options[4] = new Option("Hougue du Pommier Hotel", "Hougue du Pommier Hotel");
	        obj2.options[5] = new Option("La Villette Hotel", "La Villette Hotel");
	        obj2.options[6] = new Option("Les Rocquettes Hotel", "Les Rocquettes Hotel");
	        obj2.options[7] = new Option("Moore's Central Hotel", "Moore's Central Hotel");
	        obj2.options[8] = new Option("Peninsula Hotel", "Peninsula Hotel");

	        obj2.options[9] = new Option("Duke of Normandie Hotel", "Duke of Normandie Hotel");
	        obj2.options[10] = new Option("Le Ch\êne Hotel", "Le Ch\êne Hotel");
	        break;
	    case "Alderney":
	        //top dll
	        obj1.options.length = 0;
	        obj1.options[0] = new Option("Belle Vue Hotel", "Belle Vue Hotel");
	        obj1.options[1] = new Option("St.Annes Guest House", "St.Annes Guest House");
	        obj1.options[2] = new Option("Victoria Hotel", "Victoria Hotel");

	        //below dll
	        obj2.options.length = 0;
	        obj2.options[0] = new Option("Belle Vue Hotel", "Belle Vue Hotel");
	        obj2.options[1] = new Option("St.Annes Guest House", "St.Annes Guest House");
	        obj2.options[2] = new Option("Victoria Hotel", "Victoria Hotel");
	        break;
	    case "Brittany":
	        //top dll 
	        obj1.options.length = 0;
	        obj1.options[0] = new Option("Le Grand Hotel", "Le Grand Hotel");
	        obj1.options[1] = new Option("Hotel Crystal", "Hotel Crystal");
	        obj1.options[2] = new Option("Hotel Les Tilleuls", "Hotel Les Tilleuls");
	        obj1.options[3] = new Option("Hotel Beaufort", "Hotel Beaufort");
	        obj1.options[4] = new Option("Hotel Central", "Hotel Central");
	        obj1.options[5] = new Option("Hotel de la Cite", "Hotel de la Cite");
	        obj1.options[6] = new Option("Hotel de France et de Chateaubriand", "Hotel de France et de Chateaubriand");
	        obj1.options[7] = new Option("Hotel D'Avaugour", "Hotel D'Avaugour");
	        obj1.options[8] = new Option("Hotel Jerzual", "Hotel Jerzual");
	        obj1.options[9] = new Option("Hotel Arvor", "Hotel Arvor");
	        obj1.options[10] = new Option("Domaine des Ormes", "Domaine des Ormes");

	        //below dll
	        obj2.options.length = 0;
	        obj2.options[0] = new Option("Le Grand Hotel", "Le Grand Hotel");
	        obj2.options[1] = new Option("Hotel Crystal", "Hotel Crystal");
	        obj2.options[2] = new Option("Hotel Les Tilleuls", "Hotel Les Tilleuls");
	        obj2.options[3] = new Option("Hotel Beaufort", "Hotel Beaufort");
	        obj2.options[4] = new Option("Hotel Central", "Hotel Central");
	        obj2.options[5] = new Option("Hotel de la Cite", "Hotel de la Cite");
	        obj2.options[6] = new Option("Hotel de France et de Chateaubriand", "Hotel de France et de Chateaubriand");
	        obj2.options[7] = new Option("Hotel D'Avaugour", "Hotel D'Avaugour");
	        obj2.options[8] = new Option("Hotel Jerzual", "Hotel Jerzual");
	        obj2.options[9] = new Option("Hotel Arvor", "Hotel Arvor");
	        obj2.options[10] = new Option("Domaine des Ormes", "Domaine des Ormes");
	        break;
	    case "Jersey":
	        //top dll 
	        obj1.options.length = 0;
	        obj1.options[0] = new Option("Atlantic Hotel", "Atlantic Hotel");
	        obj1.options[1] = new Option("Hotel de France", "Hotel de France");
	        obj1.options[2] = new Option("Pomme d' Or Hotel", "Pomme d' Or Hotel");
	        obj1.options[3] = new Option("The Grand Jersey", "The Grand Jersey");
	        obj1.options[4] = new Option("Apollo Hotel", "Apollo Hotel");
	        obj1.options[5] = new Option("Hotel Revere", "Hotel Revere");
	        obj1.options[6] = new Option("Merton Hotel", "Merton Hotel");
	        obj1.options[7] = new Option("Monterey", "Monterey");
	        obj1.options[8] = new Option("The Royal Hotel", "The Royal Hotel");
	        obj1.options[9] = new Option("Uplands Hotel", "Uplands Hotel");

	        //below dll
	        obj2.options.length = 0;
	        obj2.options[0] = new Option("Atlantic Hotel", "Atlantic Hotel");
	        obj2.options[1] = new Option("Hotel de France", "Hotel de France");
	        obj2.options[2] = new Option("Pomme d' Or Hotel", "Pomme d' Or Hotel");
	        obj2.options[3] = new Option("The Grand Jersey", "The Grand Jersey");
	        obj2.options[4] = new Option("Apollo Hotel", "Apollo Hotel");
	        obj2.options[5] = new Option("Hotel Revere", "Hotel Revere");
	        obj2.options[6] = new Option("Merton Hotel", "Merton Hotel");
	        obj2.options[7] = new Option("Monterey", "Monterey");
	        obj2.options[8] = new Option("The Royal Hotel", "The Royal Hotel");
	        obj2.options[9] = new Option("Uplands Hotel", "Uplands Hotel");
	        break;	
		default :
			break;
	}
}	

function SetOptions2(o)
{
    var oo = document.getElementById('ddlDestination');
	if (oo.options[oo.selectedIndex].value == 'Brittany')
	{
	    o.selectedIndex = 0;
	}
}

function ShowHideBreak(o)
{    
	if (o.options[o.selectedIndex].value == 'Not Required')
	{
		document.getElementById('ra').style.visibility = 'hidden';
		document.getElementById('rb').style.visibility = 'hidden';
		document.getElementById('rc').style.visibility = 'hidden';
	}
	else
	{
		document.getElementById('ra').style.visibility = 'visible';
		document.getElementById('rb').style.visibility = 'visible';
		document.getElementById('rc').style.visibility = 'visible';
	}
}


