
function HideHotelSection()
{
    document.getElementById("dvInCity").style.display = "none";
    document.getElementById("liAirportName").style.display = "none";
    document.getElementById("liNearAttract").style.display = "none";
    document.getElementById("dvOptionalSearch").style.display = "none";
}
function ShowInCity()
{
    //Hide All Section
    HideHotelSection();
    document.getElementById("dvInCity").style.display = "block";
}
function ShowAirportName()
{
    //Hide All Section
    HideHotelSection();
    document.getElementById("liAirportName").style.display = "block";
}
function ShowNearAttract()
{
    //Hide All Section
    HideHotelSection();
    document.getElementById("liNearAttract").style.display = "block";
}
function ShowOptionalSearch()
{
    document.getElementById("dvOptionalSearch").style.display = "block";
}