function LoadCharterFlight()
{
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadCharterFlight(SuccessLoadCharterFlight, showError, showTimeOut);
}

function SuccessLoadCharterFlight(result)
{   
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    
    scroll(0,0);
    
    ShowPannel(false);
    ShowProgressBar(false);
    obj = null;
}

function ValidateCharterFlight(o){
	var Status = true;
	var e 	= '';
	var fn 	= document.getElementById(o +'tbLeadPassengerFirstName');
	var ln 	= document.getElementById(o+'tbLeadPassengerLastName');
	var pm 	= document.getElementById(o+'tbLeadPassengerContactMobile');
	var ph 	= document.getElementById(o+'tbLeadPassengerContactPhoneNumber');
	var p = trim(pm.value) + trim(ph.value);
	var em 	= document.getElementById(o+'tbLeadPassengerEmailAddress');
	var emReEnter = document.getElementById(o+'tbLeadPassengerEmailAddressReEnter');
	var ad1 = document.getElementById(o+'tbLeadPassengerAddress1');
	var cty = document.getElementById(o+'tbLeadPassengerPostalTown');
	var pc 	= document.getElementById(o+'tbLeadPassengerPostcode');	

	//--->  First Name
	if (trim(fn.value)==0)
	{	
		Status =  false;
		e += "<li>Please supply a valid First Name</li>";
		fn.className = 'error';
	}
	else
	{
		fn.className = '';
	}	
	//--->  Surname
	if (trim(ln.value)==0)
	{
		Status =  false;
		e += "<li>Please supply a valid Surname</li>";
		ln.className = 'error';
	}
	else
	{
		ln.className = '';
	}
	//--->  Telephone Number
	if (p==0)
	{
		Status =  false;
		e += "<li>Please supply a valid Telephone Number</li>";
		pm.className = 'error';
		ph.className = 'error';
	}
	else
	{
		pm.className = '';
		ph.className = '';
	}
	//--->  Email
	if (trim(em.value)==0)
	{
		Status =  false;
		e += "<li>Please supply a valid Email</li>";
		em.className = 'error';
	}
	else if (ValidEmail(em.value)!= true)
	{
		Status =  false;
		e += "<li>Please supply a valid Email format</li>";
		em.className = 'error';
    }
    else
    {
		em.className = '';
	}
    
    if (em.value != emReEnter.value)
    {
		Status =  false;
		e += "<li>Please supply a valid Confirm Email</li>";
		em.className = 'error';
	}
	//--->  Address 1
	if (trim(ad1.value) == 0)
	{
		Status = false;
		e += "<li>Please supply a valid Address 1</li>";
		ad1.className = 'error';
	}
	else
	{
		ad1.className = '';
	}
	//--->  Town/City
	if (trim(cty.value) == 0){
		Status =  false;
		e += "<li>Please supply a valid Town/City</li>";
		cty.className = 'error';
	}
	else
	{
		cty.className = '';
	}
	//--->  Postal Code
	if (trim(pc.value)==0){
		Status =  false;
		e += "<li>Please supply a valid Postcode</li>";
		pc.className = 'error';
	}
	else
	{
		pc.className = '';
	}
	
	if (e.length > 0) 
	{
		Container = document.getElementById(o +'LabError');
		Container.innerHTML = 	'<ul>' + e + '</ul>';	
		return Status;
	}
	else
	{
		return Status;
	}
}

function SendMailCharterFlight(o)
{
    Status = true;
    Status = ValidateCharterFlight(o);

    if (Status==true)
    {
        var Title = document.getElementById(o+'ddlLeadPassengerTitle').options[document.getElementById(o+'ddlLeadPassengerTitle').selectedIndex].value;

        Title =   Title.split("|")[0] ;

        var first_name = document.getElementById(o+'tbLeadPassengerFirstName').value;
        var last_name = document.getElementById(o+'tbLeadPassengerLastName').value;
        var phoneMobile = document.getElementById(o+'tbLeadPassengerContactMobile').value;
        var phoneHome = document.getElementById(o+'tbLeadPassengerContactPhoneNumber').value;
        var contactEmail = document.getElementById(o+'tbLeadPassengerEmailAddress').value;
       // var language = '' ;
        var addressline1 = document.getElementById(o+'tbLeadPassengerAddress1').value;  
        var addressline2 = document.getElementById(o+'tbLeadPassengerAddress2').value;
        var zipCode = document.getElementById(o+'tbLeadPassengerPostcode').value;
        var city = document.getElementById(o+'tbLeadPassengerPostalTown').value;

        var tboCompanyName = document.getElementById(o+'tboCompanyName').value;  
        var tboTotalPassenger = document.getElementById(o+'tboTotalPassenger').value;
        var txtAdditionalInformation = document.getElementById(o+'txtAdditionalInformation').value;
        var tboOrigin = document.getElementById(o+'tboOrigin').value;
        var tboDestination = document.getElementById(o+'tboDestination').value;

        var cmbTypeOfRequest = document.getElementById('cmbTypeOfRequest').options[document.getElementById('cmbTypeOfRequest').selectedIndex].value;
        var cmbTypeOfTrip = document.getElementById('cmbTypeOfTrip').options[document.getElementById('cmbTypeOfTrip').selectedIndex].value;

         var cmbPreferedDepartureTime = document.getElementById('cmbPreferedDepartureTime').options[document.getElementById('cmbPreferedDepartureTime').selectedIndex].value;
         var cmbPreferedReturnDepartureTime = document.getElementById('cmbPreferedReturnDepartureTime').options[document.getElementById('cmbPreferedReturnDepartureTime').selectedIndex].value;

        //var language = document.getElementById(o+'optLanguage').options[document.getElementById(o+'optLanguage').selectedIndex].value;
        var country = document.getElementById(o+'ddlLeadPassengerCountry').options[document.getElementById(o+'ddlLeadPassengerCountry').selectedIndex].text;

        var ctlDepatureDate = CharterFlightGetDate('3');
        var ctlReturnDate = CharterFlightGetDate('4');

        tikAeroB2C.WebService.B2cService.SendMailCharterFlight( Title, first_name, last_name, addressline1, addressline2, city, country, zipCode,phoneHome, contactEmail, tboOrigin, tboDestination, ctlDepatureDate, ctlReturnDate, txtAdditionalInformation, tboCompanyName, cmbTypeOfRequest, cmbTypeOfTrip, cmbPreferedDepartureTime, cmbPreferedReturnDepartureTime, phoneMobile, country, tboTotalPassenger, SuccessSendMailCharterFlight, showError, showTimeOut) ;
    }
}

function CharterFlightGetDate(o)
{ 
    var ddlMY=document.getElementById('ddlMY_'+o);
    var ddlDate=document.getElementById('ddlDate_'+o);
    return ddlDate.options[ddlDate.selectedIndex].value+ '/'+ ddlMY.options[ddlMY.selectedIndex].value.substring(4) + '/' +ddlMY.options[ddlMY.selectedIndex].value.substring(0,4);
}

function SuccessSendMailCharterFlight(result)
{   
    if (result=='1')
    {
       ShowMessageBox("Thank you for your enquiry,<br> we will get back to you shortly " , 1,'loadHome');            
    }
    else   
    {   
        ShowMessageBox("Can't sendmail!!", 0, ''); 
    }
}