//*********************************************************
//  This file is used for global setup in javascript
//*********************************************************

//***************************************************
// Set MaskEdit for date format.
var strDateFormat = "99/99/9999";

//***************************************************
//  Used for setting phone number validation format
var strPhoneNumberFormat = "^([0-9\\+\\_]+)$";

//***************************************************
//  Cookies Timeout for multiple B2c AgencyCode.
var iAgencyCodeExpDays = 5;

//***************************************************
// Set Age range for date of birth check.
var min_adult = 12 * 12;
var min_child = 24;
var max_age = 120;

//***************************************************
//  Cookies Expiry day.
var iCookiesExpDays = 7;
