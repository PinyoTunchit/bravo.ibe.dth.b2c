//-----------------------Function Call Sample----------------------------------------------
//function TestScript()
//{
//    tikAeroB2C.WebService.B2cService.HelloWorld(showResult, showError, showTimeOut);    
//}
//-----------------------------------------------------------------------------------------
var isClose = true;
var today = new Date();
var objLanguage;

Date.prototype.format = function (format) {
    var date = this;
    if (!format)
        format = "MM/dd/yyyy";
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    format = format.replace("MM", month.toString().padL(2, "0"));
    if (format.indexOf("yyyy") > -1)
        format = format.replace("yyyy", year.toString());
    else if (format.indexOf("yy") > -1)
        format = format.replace("yy", year.toString().substr(2, 2));

    format = format.replace("dd", date.getDate().toString().padL(2, "0"));
    var hours = date.getHours();
    if (format.indexOf("t") > -1) {
        if (hours > 11)
            format = format.replace("t", "pm");
        else
            format = format.replace("t", "am");
    }
    if (format.indexOf("HH") > -1)
        format = format.replace("HH", hours.toString().padL(2, "0"));
    if (format.indexOf("hh") > -1) {
        if (hours > 12) hours - 12;
        if (hours == 0) hours = 12;
        format = format.replace("hh", hours.toString().padL(2, "0"));
    }
    if (format.indexOf("mm") > -1)
        format = format.replace("mm", date.getMinutes().toString().padL(2, "0"));
    if (format.indexOf("ss") > -1)
        format = format.replace("ss", date.getSeconds().toString().padL(2, "0"));
    return format;
};


//**********************************************************
//Get Client Ip Address
var g_strIpAddress = "";
GetIpAddress();

//**********************************************************
//Detect keyboard key
$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        var objHolder = document.getElementById("dvFormHolder");
        if (objHolder.style.display == "block") {
            CloseDialog();
        }
    }
});

function GetIpAddress() {
    //var strHttp = window.location.href.split("//")[0];
    //if (strHttp == "http:") {
    //    $.getJSON(strHttp + "//www.tiksystems.com/getip.aspx?callback=?",
    //    function (data) {
    //        g_strIpAddress = data.ip;
    //    });
    //}

}
function showError(result) {
    ShowProgressBar(true);
    if (result != null)
    { alert(result.get_message()); }
}
function showTimeOut(result) {
    ShowProgressBar(true);
    alert("System Timeout !!");
}
function showErrorPayment(result) {
    ShowLoadBar(false);
    alert(result.get_message());
}
function showTimeOutPayment(result) {
    ShowLoadBar(false);
    alert("System Timeout !!");
}
//-------------------------------------------
//  Used to clear session and release seat
//  when close browser.
//-------------------------------------------
function CloseSession() {
    if (isClose == true) {
        tikAeroB2C.WebService.B2cService.ClearSession(showError, showTimeOut);
    }
    else
    { isClose = true; }
}

document.onkeydown = checkKeycode;
document.onmousedown = somefunction;

function checkKeycode(e) {
    var keycode;
    if (window.event)
    { keycode = window.event.keyCode; }
    else if (e)
    { keycode = e.which; }

    //Check F5
    if (keycode == 116) {
        isClose = false;
    }
}
function somefunction(e) {
    if (navigator.appName.indexOf('Microsoft') == 0) {
        var tagName = "";
        if (window.event)
        { tagName = window.event.srcElement.tagName; }
        else if (e)
        { tagName = e.target.tagName; }

        if (tagName != null) {
            if (tagName.toUpperCase() == "A")
            { isClose = false; }
        }

    }
}
//---------------------------
//End Clear session function.
//---------------------------

function ClearOptions(selectObj) {
    var selectParentNode = selectObj.parentNode;
    var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
    selectParentNode.replaceChild(newSelectObj, selectObj);
    return newSelectObj;
}
function FindControlName(tagName, ctrlName) {
    var strName = "";
    var objCtrl = document.getElementsByTagName(tagName);

    for (var iCount = 0; iCount < objCtrl.length; iCount++) {
        if (objCtrl[iCount].id.indexOf(ctrlName) != -1) {
            strName = objCtrl[iCount].id.toString();
            break;
        }
    }

    objCtrl = null;
    return strName;
}
function SetComboValue(ctrlName, strValue) {
    var ctrCombo = document.getElementById(ctrlName);
    if (ctrCombo != null) {
        for (var iCount = 0; iCount < ctrCombo.length; ++iCount) {
            if (ctrCombo.options[iCount].value == strValue) {
                ctrCombo.selectedIndex = iCount;
                if (iCount == 0 && (strValue == objLanguage.Default_Value_2 || strValue == objLanguage.Default_Value_3)) {
                    $("#" + ctrlName).addClass("watermarkOn");
                }
                else {
                    $("#" + ctrlName).removeClass("watermarkOn");
                }
                break;
            }
        }
    }
    ctrCombo = null;
}
function SetComboSplitValue(ctrlName, strValue, index) {
    var ctrCombo = document.getElementById(ctrlName);
    for (var iCount = 0; iCount < ctrCombo.length; ++iCount) {
        if (ctrCombo.options[iCount].value.split("|")[index] == strValue) {
            ctrCombo.selectedIndex = iCount;
            if (iCount == 0 && (strValue == objLanguage.Default_Value_2 || strValue == objLanguage.Default_Value_3)) {
                $("#" + ctrlName).addClass("watermarkOn");
            }
            else {
                $("#" + ctrlName).removeClass("watermarkOn");
            }
            break;
        }
    }

    ctrCombo = null;
}
function ShowProgressBar(bValue) {
    var obj = document.getElementById("dvProgressBar");
    var objLoader = document.getElementById("dvLoad");
    if (obj != null) {
        document.getElementById("dvLoadBar").style.display = "none";
        
        if (bValue == true) {
            objLoader.style.display = "block";
            obj.style.display = "block";
        }
        else {
            if (document.getElementById("dvMessageBox").style.display == "none" || document.getElementById("dvMessageBox").style.display == "") {
                var objFormHolder = document.getElementById("dvFormHolder");
                if (objFormHolder != null && (objFormHolder.style.display == "none" || objFormHolder.style.display == "")) {
                    obj.style.display = "none";
                }
            }
            objLoader.style.display = "none";
        }
        obj = null;
        objLoader = null;
    }
}
function ShowLoadBar(bValue) {
    var obj = document.getElementById("dvProgressBar");
    var objLoader = document.getElementById("dvLoadBar");

    document.getElementById("dvLoad").style.display = "none";
    document.getElementById("dvMessageBox").style.display = "none";

    if (bValue == true) {
        objLoader.style.display = "block";
        obj.style.display = "block";
    }
    else {
        objLoader.style.display = "none";
        obj.style.display = "none";
    }

    obj = null;
    objLoader = null;
}
function loadHome() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadHome(SuccessLoadHomePage, showError, showTimeOut);
}
function SuccessLoadHomePage(result) {
    if (result.length > 0) {
        var objContainer = document.getElementById("dvContainer");

        objContainer.innerHTML = result;

        DisplayQuoteSummary("", "", "");

        ShowSearchPannel(true);
        ShowProgressBar(false);
        objContainer = null;
    }
}

function FindOptionIndexByValue(ctrName, value) {
    var objOption = document.getElementById(ctrName);
    var iCount = 0;
    for (iCount = 0; iCount < objOption.length; ++iCount) {
        if (objOption.options[iCount].value == value) {
            iCount;
            break;
        }
    }
    return iCount;
}

function ReformatDate(strDate) {
    if (strDate == objLanguage.default_value_1 || strDate == GetDateMask() || strDate.length == 0) {
        return "";
    }
    else {
        var arrDate = strDate.split("/");
        if (arrDate.length == 3) {
            var day = arrDate[0];
            var month = arrDate[1];
            var year = arrDate[2];

            if (IsNumeric(day) == false || IsNumeric(month) == false || IsNumeric(year) == false) {
                return "";
            }
            else {
                return year + "-" + month + "-" + day;
            }
        }
        else {
            return "";
        }
    }
}
function ReformatDateXml(strDate) {
    if (strDate == objLanguage.default_value_1 || strDate == GetDateMask() || strDate.length == 0) {
        return "0001-01-01T00:00:00";
    }
    else {
        var day = strDate.substring(0, 2);
        var month = strDate.substring(3, 5);
        var year = strDate.substring(6, 10);

        return year + "-" + month + "-" + day + "T00:00:00";
    }
}

function ReformatXmlViewDate(strDate) {
    if (strDate.length > 0) {
        var day = strDate.substring(8, 10);
        var month = strDate.substring(5, 7);
        var year = strDate.substring(0, 4);

        return day + "/" + month + "/" + year;
    }
    else {
        return "";
    }
}
function ReformatYYYYMMDDToDashDate(strDate) {
    if (strDate.length > 0) {
        var day = strDate.substring(6, 8);
        var month = strDate.substring(4, 6);
        var year = strDate.substring(0, 4);

        return year + "-" + month + "-" + day;
    }
    else {
        return "";
    }
}
function DateMaskFormat(strDate) {
    if (strDate.length == 0) {
        return objLanguage.default_value_1;
    }
    else {
        return strDate;
    }
}
function LTrim(value) {
    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");
}

function RTrim(value) {
    var re = /((\s*\S+)*)\s*/;
    return value.replace(re, "$1");
}

function trim(value) {
    return LTrim(RTrim(value));
}
function leadingZero(nr) {
    if (nr < 10) nr = "0" + nr;
    return nr;
}
function printReport(reportType, reportTitle) {
    window.open("print.aspx?type=" + reportType, reportTitle, "scrollbars=1,status=1,toolbar=0", "");
}
function EmailItinerary() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.SendItineraryEmail(SuccessEmailItinerary, showError, showTimeOut);
}
function SuccessEmailItinerary(result) {
    ShowProgressBar(false);
    ShowMessageBox("Email Sent..", 1, '');
}
function setCookie(strName, strValue, dExpires, strPath, strDomain, bSecure) {
    var strCookieText = escape(strName) + '=' + escape(strValue);
    strCookieText += (dExpires ? '; EXPIRES=' + dExpires.toGMTString() : '');
    strCookieText += (strPath ? '; PATH=' + strPath : '');
    strCookieText += (strDomain ? '; DOMAIN=' + strDomain : '');
    strCookieText += (bSecure ? '; SECURE' : '');

    document.cookie = strCookieText;
}
function getCookie(strName) {
    var strValue = null;
    if (document.cookie)	   //only if exists
    {
        var arr = document.cookie.split((escape(strName) + '='));
        if (2 <= arr.length) {
            var arr2 = arr[1].split(';');
            strValue = unescape(arr2[0]);
        }
    }
    return strValue;
}
function deleteCookie(strName) {
    var tmp = getCookie(strName);
    if (tmp)
    { setCookie(strName, tmp, (new Date(1))); }
}
function IsFutureDate(strDate) {
    var bResult = false;

    //Reformat Date value to be yyyy-MM-DD
    strDate = ReformatDate(strDate);

    if (strDate.length > 0) {
        var arrDate = strDate.split("-");
        var dDate = new Date(arrDate[0], arrDate[1] - 1, arrDate[2]);
        var dToday = new Date();

        if (dDate > dToday) {
            bResult = true;
        }
        else {
            bResult = false;
        }
        dDate = null;
        dToday = null;
    }
    else {
        bResult = true;
    }

    return bResult;
}
function IsPastDate(strDate) {
    var bResult = false;

    //Reformat Date value to be yyyy-MM-DD
    strDate = ReformatDate(strDate);

    if (strDate.length > 0) {
        var arrDate = strDate.split("-");
        var dDate = new Date(arrDate[0], arrDate[1] - 1, arrDate[2]);
        var dToday = new Date();

        if (dDate < dToday) {
            bResult = true;
        }
        else {
            bResult = false;
        }
        dDate = null;
        dToday = null;
    }
    else {
        bResult = true;
    }

    return bResult;
}
function ShowMessageBox(strMessage, iType, callBack) {
    //iType
    //  0 = Error.
    //  1 = Information.

    var obj = document.getElementById("dvProgressBar");
    var objErrorMsg = document.getElementById("dvErrorMessage");
    var objIcon = document.getElementById("dvMessageIcon");
    var objMessage = document.getElementById("dvMessageBox");

    document.getElementById("dvLoadBar").style.display = "none";
    document.getElementById("dvLoad").style.display = "none";

    //Show and hide Icon.
    if (iType != 0)
    { objIcon.style.display = "none"; }

    //Show or hide fading.
    objErrorMsg.innerHTML = strMessage;
    obj.style.display = "block";
    objMessage.style.display = "block";
    objMessage.setAttribute('CallBack', callBack);
    
    obj = null;
    objErrorMsg = null;
    objIcon = null;
    objMessage = null;

}
function LoadCob(bParameter, bookingId) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadCob(bParameter, bookingId, SuccessLoadCob, showError, showTimeOut);
}
function SuccessLoadCob(result) {
    if (result != "{000}") {
        var objContainer = document.getElementById("dvContainer");

        objContainer.innerHTML = result;

        ShowSearchPannel(false);
        ShowProgressBar(false);
        objContainer = null;
    }
    else {
        LoadSecure(true);
    }
}
function LoadHtml(strHtmlName) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadHtml(strHtmlName, SuccessLoadHtml, showError, showTimeOut);
}
function SuccessLoadHtml(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;

    ShowProgressBar(false);
    objContainer = null;

}
function Changelang(strLanguage) {
    tikAeroB2C.WebService.B2cService.SetLanguage(strLanguage, SuccessChangelang, showError, showTimeOut);
}
function SuccessChangelang(result) {
    var objLang = document.getElementById("hdLang");
    objLang.value = result;
    objLang = null;

    document.forms[0].submit();
}
function getRequestParameter(strName) {
    strName = strName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

    var regexS = "[\\?&]" + strName + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);

    if (results == null)
        return "";
    else
        return results[1];
}
function LoadCMS(strUrl) {
    var strParameter = getRequestParameter("langculture");
    var strParameterlang = getRequestParameter("lang");

    if (strParameter.length > 0) {
        strUrl = strUrl + "?langculture=" + strParameter + "&lang=" + strParameterlang;
    }

    ShowProgressBar(true);
    window.location.href = strUrl;
    //tikAeroB2C.WebService.B2cService.LoadCms(strUrl, SuccessLoadCMS, showError, showTimeOut);
}
function SuccessLoadCMS(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;

    ShowProgressBar(false);
    objContainer = null;
}

function CreateWnd(file, width, height, resize) {
    var doCenter = false;

    attribs = "";

    if (resize) size = "true"; else size = "no";

    for (var item in window)
    { if (item == "screen") { doCenter = true; break; } }

    if (doCenter) {
        if (screen.width <= width || screen.height <= height) size = "yes";

        WndTop = (screen.height - height) / 2;
        WndLeft = (screen.width - width) / 2;
        attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," +
		"status=no,toolbar=no,directories=no,menubar=no,location=no,dialog=yes,chrome, resizable=false, minimizable=yes,top=" + WndTop + ",left=" + WndLeft;
    }
    else {
        if (navigator.appName == "Netscape" && navigator.javaEnabled()) {
            var toolkit = java.awt.Toolkit.getDefaultToolkit();
            var screen_size = toolkit.getScreenSize();
            if (screen_size.width <= width || screen_size.height <= height) size = "yes";

            WndTop = (screen_size.height - height) / 2;
            WndLeft = (screen_size.width - width) / 2;
            attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," +
			"status=no,toolbar=no,directories=no,menubar=no,location=no,dialog=yes,chrome, resizable=false, minimizable=yes,top=" + WndTop + ",left=" + WndLeft;
        }
        else {
            size = "yes";
            attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," +
			"status=no,toolbar=no,directories=no,menubar=no,location=no,dialog=yes,chrome, resizable=false, minimizable=yes";
        }
    }


    // create the window
    window.open(file, "", attribs);

}

function dateDiff(p_Interval, p_Date1, p_Date2, p_firstdayofweek, p_firstweekofyear) {

    if (!isDate(p_Date1)) { return "invalid date: '" + p_Date1 + "'"; }
    if (!isDate(p_Date2)) { return "invalid date: '" + p_Date2 + "'"; }
    var dt1 = new Date(p_Date1);
    var dt2 = new Date(p_Date2);
    // get ms between dates (UTC) and make into "difference" date
    var iDiffMS = dt2.valueOf() - dt1.valueOf();
    var dtDiff = new Date(iDiffMS);

    // calc various diffs
    var nYears = dt2.getUTCFullYear() - dt1.getUTCFullYear();
    var nMonths = dt2.getUTCMonth() - dt1.getUTCMonth() + (nYears != 0 ? nYears * 12 : 0);
    var nQuarters = parseInt(nMonths / 3); //<<-- different than VBScript, which watches rollover not completion

    var nMilliseconds = iDiffMS;
    var nSeconds = parseInt(iDiffMS / 1000);
    var nMinutes = parseInt(nSeconds / 60);
    var nHours = parseInt(nMinutes / 60);
    var nDays = parseInt(nHours / 24);
    var nWeeks = parseInt(nDays / 7);


    // return requested difference
    var iDiff = 0;
    switch (p_Interval.toLowerCase()) {
        case "yyyy": return nYears;
        case "q": return nQuarters;
        case "m": return nMonths;
        case "y": 		// day of year
        case "d": return nDays;
        case "w": return nDays;
        case "ww": return nWeeks; 	// week of year	// <-- inaccurate, WW should count calendar weeks (# of sundays) between
        case "h": return nHours;
        case "n": return nMinutes;
        case "s": return nSeconds;
        case "ms": return nMilliseconds; // millisecond	// <-- extension for JS, NOT available in VBScript
        default: return "invalid interval: '" + p_Interval + "'";
    }
}
function isDate(p_Expression) {
    return !isNaN(new Date(p_Expression)); 	// <<--- this needs checking
}
function CheckDateFormat(strDate) {
    var fdate = strDate.split('/');

    if (fdate.length < 3) return false;

    var lenStr = strDate.replace('/', '');
    lenStr = lenStr.replace('/', '');

    if (lenStr.toString().length != 8) return false;

    return true;
}
function padZeros(theNumber, max) {
    var numStr = String(theNumber);

    while (numStr.length < max) {
        numStr = '0' + numStr;
    }

    return numStr;
}

function ClientLogon() {
    var objClientId = document.getElementById('txtClientID');
    var objPassword = document.getElementById('txtPassword');

    if (GetControlValue(objClientId).length == 0 || GetControlValue(objClientId).length == 0) {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_52, "Login Failed"), 0, '');
    }
    else if (IsNumeric(GetControlValue(objClientId)) == true && GetControlValue(objClientId).length != 8) {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_138, "Invalid Client number"), 0, '');
    }
    else if (ValidEmail(GetControlValue(objClientId)) == false && IsNumeric(GetControlValue(objClientId)) == false) {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_94, "Invalid Client number"), 0, '');
    }
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ClientLogon(objClientId.value,
                                                 objPassword.value,
                                                 SuccessClientLogon,
                                                 showError,
                                                 showTimeOut);
    } 
}
function SuccessClientLogon(result) {
    if (result.length > 0) {
        if (result == "{000}") {
            //Success Login.
            //Load My Booking Information
            tikAeroB2C.WebService.B2cService.LoadMyBooking(SuccessLoadMyBooking, showError, showTimeOut);
            //Load Client information
            tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);

            //****************************************************************
            //  Used in case of using login dialog to close dialog when login
            //  success.
            //****************************************************************
            ShowProgressBar(false);
            var objMessage = document.getElementById("dvFormHolder");

            //Insert passenger form content.
            objMessage.innerHTML = "";
            objMessage.style.display = "none";
            objMessage = null;

            DisplayQuoteSummary("", "", "");
        }
        else {
            //Failed Login
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_52, "Login Failed"), 0, '');
        }
    }

}

function SuccessLoadClientInformation(result) {
    if (result.length > 0) {

        ShowClientLogonMenu(true, result);
    }
}
function ClientLogonDialog() {
    CloseDialog();

    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.ClientLogon(document.getElementById('txtDialogClientID').value,
        document.getElementById('txtDialogPassword').value, SuccessClientLogon, showError, showTimeOut);

}

function CloseDialog() {
    var objMessage = document.getElementById("dvFormHolder");
    objMessage.style.display = "none";
    objMessage.innerHTML = "";
    var objContainer = document.getElementById("dvProgressBar");
    objContainer.style.display = "none";
}

function MyBookingSearch() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.MyBookingSearch(document.getElementById('txtBookingCode').value, SuccessMyBookingSearch, showError, showTimeOut);
}
function SuccessMyBookingSearch(result) {
    var objContainer = document.getElementById("dvSearch");
    var objBox = document.getElementById("dvResultBox");
    var objBookingBox = document.getElementById("dvBookingBox");

    objBox.style.display = 'block';
    objBookingBox.style.display = 'none';
    objContainer.innerHTML = result;

    ShowSearchPannel(false);
    ShowProgressBar(false);
    objContainer = null;

}

function LoadMyBooking() {

    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadMyBooking(SuccessLoadMyBooking, showError, showTimeOut);
}
function SuccessLoadMyBooking(result) {

    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {

            //Booking object is null.(Session time out).
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
        else {
            var objContainer = document.getElementById("dvContainer");

            objContainer.innerHTML = result;

            ShowSearchPannel(true);
            DisplayQuoteSummary("", "", "");
            objContainer = null;
        }
    }
    ShowProgressBar(false);
}

function LoadMilleageDetail() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadMilleageDetail(SuccessLoadMilleageDetail, showError, showTimeOut);
}
function SuccessLoadMilleageDetail(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;

    DisplayQuoteSummary("", "", "");
    ShowProgressBar(false);
    objContainer = null;
}

function LoadBookingDetail(bookingId, errorMSG) {
    if (bookingId == '')
        ShowMessageBox(errorMSG, 0, '');
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadBookingDetail(bookingId, SuccessLoadBookingDetail, showError, showTimeOut);
    }
}
function SuccessLoadBookingDetail(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;

    ShowProgressBar(false);
    window.scrollTo(0, 0);
    objContainer = null;
}

function ClientLogOff() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ClientLogOff(SuccessClientLogOff, showError, showTimeOut);
}
function SuccessClientLogOff(result) {
    if (result == true) {
        //Clear Cookies
        deleteCookie("coFFP");
        //Hide client menu.
        ShowClientLogonMenu(false, "");
        //Load B2C Menu
        LoadMenu();
        //Load Home page
        loadHome();
        ShowProgressBar(false);
    }
}

function SubmitEnterUser(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            ClientLogon();
        else
            ClientLogonDialog();
        return false;
    }
    else
        return true;
}

function LoadForgetPassword() {
    CloseDialog();
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadForgetPassword(SuccessLoadForgetPassword, showError, showTimeOut);
}
function SuccessLoadForgetPassword(result) {

    if (result.length > 0) {
        if (result == "{203}") {

            //Load Forget password failed.
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
        else {
            var objContainer = document.getElementById("dvContainer");
            objContainer.innerHTML = result;
            objContainer = null;
        }
    }
    ShowProgressBar(false);
}

function ForgetPassword() {
    ShowProgressBar(true);
    var url = window.location;
    tikAeroB2C.WebService.B2cService.ForgetPassword(document.getElementById('txtUserID').value
        , url.href, SuccessForgetPassword, showError, showTimeOut);
}
function SuccessForgetPassword(result) {
    if (result.length > 0) {
        if (result == "100") {
            ShowMessageBox("Error,Client id isn't valid", 0, "");
        }
        else if (result == "101") {
            ShowMessageBox("Error,Cann't reset password.", 0, "");
        }
        else
        { ShowMessageBox("New password has been sent.", 1, 'loadHome'); }
    }
    ShowProgressBar(false);
}

function IsNumeric(input) {
    return (input - 0) == input && input.length > 0;
}

function OpenTicketDetail(booking_id, passengerId, bookingSegmentId) {
    window.open("TicketPopUp.aspx?bid=" + booking_id + "&pid=" + passengerId + "&sid=" + bookingSegmentId);
}

function SubmitEnterSearch(myfield, e) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        MyBookingSearch();
        return false;
    }
    else
        return true;
}

function LoadDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadClientDialog(SuccessLoadClientDialog, showError, showTimeOut);
}

function SuccessLoadClientDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");


    //Insert passenger form content.
    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;

    InitializeWaterMark("txtClientID", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtPassword", objLanguage.default_value_2, "password");
}
function SucessLoadAvailabilitySearch(result) {
    var objAvailabilitySearch = document.getElementById("dvAvailabilitySearch");
    objAvailabilitySearch.innerHTML = result;
    objAvailabilitySearch = null;

}

function LoadMenu() {
    tikAeroB2C.WebService.B2cService.LoadMenu(SuccessLoadMenu, showError, showTimeOut);
}
function SuccessLoadMenu(result) {
    var objContainer = document.getElementById("dvMenu");

    objContainer.innerHTML = result;
    objContainer = null;
}

function resizeFrame(f) {
    f.style.height = f.contentWindow.document.body.scrollHeight + "px";
    f.style.width = f.contentWindow.document.body.scrollWidth + "px";
}
function isNum(string) {
    var numericExpression = /^[0-9]+$/;
    if (string.match(numericExpression)) {
        return true;
    } else {
        return false;
    }
}
function IsChar(string) {
    var CharExpression = /^[a-zA-Z]+$/;
    if (string.match(CharExpression)) {
        return true;
    } else {
        return false;
    }
}

function IsAlphaNumeric(string) {
    var CharExpression = /^[a-zA-Z0-9]+$/;
    if (string.match(CharExpression)) {
        return true;
    } else {
        return false;
    }
}

function ContainNumeric(value) {
    for (var i = 0; i < value.length; i++) {
        if (isNum(value.charAt(i)) == true) {
            return true;
        }
    }
    return false;
}
function FilterNonNumeric(value) {
    var charMatch = "";
    if (value.length > 0) {
        for (var i = 0; i < value.length; i++) {
            if (isNum(value.charAt(i)) == true)
                charMatch += value.charAt(i);
        }
    }
    return charMatch;
}
function OnlyCharacter(value) {
    for (var i = 0; i < value.length; i++) {
        if (IsChar(value.charAt(i)) == false) {
            return false;
        }
    }
    return true;
}
function ReplaceSpecialCharacter(value) {
    var strNewValue = new String(value);
    strNewValue = strNewValue.replace(/[^a-zA-Z 0-9]+/g, '');
    strNewValue = strNewValue.replace(/\s+/g, '');

    return strNewValue;
}

function GetPagingFFP(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingFFP(pageindex, SuccessGetPaging, showError, showTimeOut);
}

function GetHistoryBooking(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingBooking(pageindex, 'true', SuccessGetPaging, showError, showTimeOut);
}

function SuccessGetPaging(result) {
    ShowProgressBar(false);
    var objContainer = document.getElementById("dvPaging");

    objContainer.innerHTML = result;
    objContainer = null;
}

function GetLifeBooking(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingBooking(pageindex, 'false', SuccessGetLifeBooking, showError, showTimeOut);
}

function SuccessGetLifeBooking(result) {
    ShowProgressBar(false);
    var objContainer = document.getElementById("dvLifePage");

    objContainer.innerHTML = result;
    objContainer = null;
}

function ShowHistory() {
    HideLiveBooking();
    document.getElementById("historyBooking").style.display = "block";
    document.getElementById("HistoryTab1").className = 'TabConererLeft';
    document.getElementById("HistoryTab2").className = 'TabConererContent';
    document.getElementById("HistoryTab3").className = 'TabConererRight';

}

function HideHistory() {
    document.getElementById("historyBooking").style.display = "none";
    document.getElementById("HistoryTab1").className = 'TabActiveConererLeft';
    document.getElementById("HistoryTab2").className = 'TabActiveConererContent';
    document.getElementById("HistoryTab3").className = 'TabActiveConererRight';
}

function ShowLiveBooking() {
    document.getElementById("liveBooking").style.display = "block";
    document.getElementById("LiveTab1").className = 'TabConererLeft';
    document.getElementById("LiveTab2").className = 'TabConererContent';
    document.getElementById("LiveTab3").className = 'TabConererRight';

    HideHistory();
}

function HideLiveBooking() {
    document.getElementById("liveBooking").style.display = "none";
    document.getElementById("LiveTab1").className = 'TabActiveConererLeft';
    document.getElementById("LiveTab2").className = 'TabActiveConererContent';
    document.getElementById("LiveTab3").className = 'TabActiveConererRight';
}
function FormLoadOperation() {

    //----------This function will be turn on only when use cookiesless only.
    //tikAeroB2C.WebService.B2cService.set_path("WebService/B2cService.asmx");
    //tikAeroB2C.WebService.WebUIRender.set_path("WebService/WebUIRender.asmx");
    //-----------------------------------------------------------------------------
    RefreshSetting();
    DisplayQuoteSummary("", "", "");
    GenerateFromQueryString();

    // Delete cookie cross app
    //deleteCookie('cOutside');

}
function ReadFFPCookies() {
    var strCookies = getCookie("coFFP");
    if (strCookies != null) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ClientLoad(strCookies, SuccessClientLogon, showError, showTimeOut);
    }
}
function EmailItineraryInput(strDefaultMail) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ShowEmailSender(SuccessEmailItineraryInput, showError, strDefaultMail);
}
function SuccessEmailItineraryInput(result, strDefauleEmail) {
    ShowProgressBar(false);
    if (result.length > 0) {
        var objMessage = document.getElementById("dvFormHolder");
        var objContainer = document.getElementById("dvProgressBar");

        //Insert passenger form content.
        objMessage.innerHTML = result;

        document.getElementById("txtEmail").value = strDefauleEmail;

        objMessage.style.display = "block";
        objContainer.style.display = "block";

        objMessage = null;
        objContainer = null;
    }
}
function CloseEmailSender() {
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");
    //Insert passenger form content.
    objMessage.innerHTML = "";

    objMessage.style.display = "none";
    objContainer.style.display = "none";

    objMessage = null;
    objContainer = null;
}
function SendItineraryEmailInput() {
    //Close Email Sender.
    var strEmail = document.getElementById("txtEmail").value;

    CloseEmailSender();
    if (ValidEmail(strEmail) == true) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SendItineraryEmailOptional(strEmail, SuccessEmailItinerary, showError, showTimeOut);
    }
    else {
        ShowMessageBox("Invalid Email address !!", 0, '');
    }

}
function ShowPaymentFfpInfo() {
    //var objRedMenuFlyer = document.getElementById("dvRedMenu")
    //document.getElementById("dvFfpPointSummary").style.display = objRedMenuFlyer.style.display;
    //objRedMenuFlyer = null;
    if (document.getElementById("dvPointTotal") != null) {
        var dblCurrentPoint = parseFloat(document.getElementById("dvCurrentPoint").innerHTML.replace(/\,/g, ''));
        var dblRedeemPoint = parseFloat(document.getElementById("dvPointTotal").innerHTML);

        document.getElementById("dvFfpRedeemSummary").style.display = "block";
        document.getElementById("dvRedeemClientId").innerHTML = document.getElementById("hdClientNumber").value;
        document.getElementById("dvRedeemCurrentPoint").innerHTML = dblCurrentPoint;
        document.getElementById("dvRedeemPoint").innerHTML = dblRedeemPoint;
        document.getElementById("dvRedeemBalance").innerHTML = dblCurrentPoint - dblRedeemPoint;

        //Check FFP Balance if it is enough.
        if (dblCurrentPoint >= dblRedeemPoint)
        { }
        else {
            document.getElementById("btmPaymentCC").style.display = "none";
            document.getElementById("chkPayRedeem").style.display = "none";
        }
    }

}
function AddCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function RemoveCommas(strNumber) {
    if (strNumber.replace != undefined)
        return strNumber.replace(/\,/g, "");
    else
        return strNumber;
}
function SetLanguage(strJSON) {
    objLanguage = eval("(" + strJSON + ")");
}

function validateChdInfBirthDate(checkedDate, birthDate, paxType) {
    var strErrMessage = "";
    var strErrMessage = "";

    var arrDate = ReformatDate(birthDate).split("-");
    var db = arrDate[2];
    var mb = arrDate[1];
    var yb = arrDate[0];

    arrDate = ReformatDate(checkedDate).split("-");
    var dd = arrDate[2];
    var md = arrDate[1];
    var yd = arrDate[0];

    if (paxType.toUpperCase() == "ADULT") {
        var tmp_year = parseInt(yd) - (min_adult / 12);

        //check year
        var d = new Date();
        var tmpd = d.getFullYear() - parseInt(yb);
        if (parseInt(yb) == 0 || parseFloat(mb) == 0 || parseFloat(db) == 0) {
            return false;
        } else if (tmpd > parseInt(max_age)) {
            return false;
        } else if (parseInt(yb) < tmp_year) {
            return true;
        } else if (parseInt(yb) == tmp_year) {
            //check month
            if (parseFloat(mb) < parseFloat(md)) {
                return true;
            } else if (parseFloat(mb) == parseFloat(md)) {
                //check date
                if ((parseFloat(db) <= parseFloat(dd)) && parseFloat(db) <= 31) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        //-----------------------------------------------------------------------------------
    } else if (paxType.toUpperCase() == "CHD") {

        var tmp_max_year = parseInt(yd) - (min_adult / 12);
        var tmp_min_year = parseInt(yd) - (min_child / 12);

        //check year
        if (parseInt(yb) == 0 || parseFloat(mb) == 0 || parseFloat(db) == 0) {
            return false;
        } else if (parseInt(yb) > tmp_max_year & parseInt(yb) < tmp_min_year) {
            return true;
        } else if (parseInt(yb) == tmp_min_year) {
            //check month
            if (parseFloat(mb) < parseFloat(md)) {
                return true;
            } else if (parseFloat(mb) == parseFloat(md)) {
                //check date
                if ((parseFloat(db) <= parseFloat(dd)) && parseFloat(db) <= 31) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if (parseInt(yb) == tmp_max_year) {
            //check month
            if (parseFloat(mb) > parseFloat(md)) {
                return true;
            } else if (parseFloat(mb) == parseFloat(md)) {
                //check date			
                if ((parseFloat(db) > parseFloat(dd)) && parseFloat(db) <= 31) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        //--------------------------------------------------------------------------------------
    } else if (paxType.toUpperCase() == "INF") {
        var tmp_year = parseInt(yd) - (min_child / 12);
        //check year

        if (parseInt(yb) == 0 || parseFloat(mb) == 0 || parseFloat(db) == 0) {
            return false;
        } else if (parseInt(yb) > tmp_year) {
            //check month
            if (parseFloat(mb) < parseFloat(md)) {
                return true;
            } else if (parseFloat(mb) == parseFloat(md)) {
                //check date
                if ((parseFloat(db) > parseFloat(dd)) && parseFloat(db) <= 31) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
            //ret = true;
        } else if (parseInt(yb) == tmp_year) {
            //check month
            if (parseFloat(mb) > parseFloat(md)) {
                return true;
            } else if (parseFloat(mb) == parseFloat(md)) {

                //check date
                if ((parseFloat(db) > parseFloat(dd)) && parseFloat(db) <= 31) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    return true;
}

function ReformatXmlToYYYYMMDD(strDate) {
    if (strDate.length > 0) {
        var day = strDate.substring(8, 10);
        var month = strDate.substring(5, 7);
        var year = strDate.substring(0, 4);

        return year + month + day;
    }
    else {
        return "";
    }
}
function LoadSecure(value) {
    isClose = false;
    var strHttp = "";

    if (value == true) {
        strHttp = "https://";
    }
    else {
        strHttp = "http://";
    }
    window.location.href = strHttp + RemoveUrlParameter(window.location.href.split("//")[1]);
}
function ShowClientLogon() {
    tikAeroB2C.WebService.B2cService.ShowClientLogon(SuccessShowClientLogon, showError, showTimeOut);
}
function SuccessShowClientLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}
function FormatTimeNumber(strTime) {
    var dateLength = strTime.toString().length;
    if (dateLength > 0) {
        if (dateLength == 3) {
            return "0" + strTime.toString().substring(0, 1) + ":" + strTime.toString().substring(1, 3);
        }
        else {
            return strTime.toString().substring(0, 2) + ":" + strTime.toString().substring(2, 4);
        }
    }
    else {
        return "";
    }
}
function FormatDateString(strDate) {
    var day = strDate.toString().substring(6, 8);
    var month = strDate.toString().substring(4, 6);
    var year = strDate.toString().substring(0, 4);

    var dDate = new Date(year, month - 1, day);

    return GetDayName(dDate) + " " + day + " " + GetMonthName(strDate) + " " + year;
}
function FormatDashDateString(strDate) {
    var day = strDate.toString().substring(8, 10);
    var month = strDate.toString().substring(5, 7);
    var year = strDate.toString().substring(0, 4);

    var dDate = new Date(year, month - 1, day);

    return GetDayName(dDate) + " " + day + " " + GetMonthName(dDate) + " " + year;
}
function GetDayName(dDate) {
    var d = [objLanguage.Date_Display_14,
            objLanguage.Date_Display_15,
            objLanguage.Date_Display_16,
            objLanguage.Date_Display_17,
            objLanguage.Date_Display_18,
            objLanguage.Date_Display_19,
            objLanguage.Date_Display_20];

    return d[dDate.getDay()];
}
function GetMonthName(dDate) {
    var m = [objLanguage.Date_Display_2,
            objLanguage.Date_Display_3,
            objLanguage.Date_Display_4,
            objLanguage.Date_Display_5,
            objLanguage.Date_Display_6,
            objLanguage.Date_Display_7,
            objLanguage.Date_Display_8,
            objLanguage.Date_Display_9,
            objLanguage.Date_Display_10,
            objLanguage.Date_Display_11,
            objLanguage.Date_Display_12,
            objLanguage.Date_Display_13];
    return m[dDate.getMonth()];
}
function ValidateInput(strValue) {
    var regExp = /^[A-Za-z]$/;
    if (strValue != null && strValue != "") {
        for (var i = 0; i < strValue.length; i++) {
            if (!strValue.charAt(i).match(regExp)) {
                return false;
            }
        }
    }
    else {
        return false;
    }
    return true;
}

function IsSSecurePage() {
    var loc = new String(window.parent.document.location);

    if (loc.indexOf("https://") === -1)
        return false;
    else
        return true;
}

// Start Yai add Login Cross App Cookie
function LoadB2BLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2BLogon(SuccessLoadB2BLogon, showError, showTimeOut);
}

function SuccessLoadB2BLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2BLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2BLogonDialog(SuccessLoadB2BLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2BLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function LoadB2BAdminLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2BAdminLogon(SuccessLoadB2BAdminLogon, showError, showTimeOut);
}

function SuccessLoadB2BAdminLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2BAdminLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2BAdminLogonDialog(SuccessLoadB2BAdminLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2BAdminLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    //Insert passenger form content.
    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function LoadB2ELogon() {
    tikAeroB2C.WebService.B2cService.LoadB2ELogon(SuccessLoadB2ELogon, showError, showTimeOut);
}

function SuccessLoadB2ELogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2ELogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2ELogonDialog(SuccessLoadB2ELogonDialog, showError, showTimeOut);
}

function SuccessLoadB2ELogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function LoadB2EAdminLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2EAdminLogon(SuccessLoadB2EAdminLogon, showError, showTimeOut);
}

function SuccessLoadB2EAdminLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2EAdminLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2EAdminLogonDialog(SuccessLoadB2EAdminLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2EAdminLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function AgencyLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.AgencyLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogLogonID').value, document.getElementById('txtDialogPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.AgencyLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtLogonID').value, document.getElementById('txtPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyAdminLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.AgencyAdminLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogAdminLogonID').value, document.getElementById('txtDialogPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyAdminLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.AgencyAdminLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtAdminLogonID').value, document.getElementById('txtPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function SuccessAgencyLogon(result) {
    var arrResult = result.split(",");

    if (arrResult.length == 2 && arrResult[0] == 'Error') {
        ShowProgressBar(false);
        if (arrResult[1] == "0") {
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
        else {
            ShowMessageBox(arrResult[1], 0, '');
        }
    }
    else {
        window.location.replace(result);
    }
}

function CorporateLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.CorporateLogon(document.getElementById('txtDialogCompanyCode').value,
        document.getElementById('txtDialogLogonID').value, document.getElementById('txtDialogPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.CorporateLogon(document.getElementById('txtCompanyCode').value,
        document.getElementById('txtLogonID').value, document.getElementById('txtPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateAdminLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.CorporateAdminLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogAdminLogonID').value, document.getElementById('txtDialogPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateAdminLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.CorporateAdminLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtAdminLogonID').value, document.getElementById('txtPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function SuccessCorporateLogon(result) {
    var arrResult = result.split(",");

    if (arrResult.length == 2 && arrResult[0] == 'Error') {
        ShowProgressBar(false);
        if (arrResult[1] == "0") {
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
        else {
            ShowMessageBox(arrResult[1], 0, '');
        }
    }
    else {
        window.location.replace(result);
    }
}

function SubmitEnterAgencyLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            AgencyLogon();
        else
            AgencyLogonDialog();
        return false;
    }
    else
        return true;
}

function SubmitEnterCorporateLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            CorporateLogon();
        else
            CorporateLogonDialog();
        return false;
    }
    else
        return true;
}

function SubmitEnterAgencyAdminLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            AgencyAdminLogon();
        else
            AgencyAdminLogonDialog();
        return false;
    }
    else
        return true;
}

function SubmitEnterCorporateAdminLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            CorporateAdminLogon();
        else
            CorporateAdminLogonDialog();
        return false;
    }
    else
        return true;
}
function RemoveUrlParameter(strUrl) {
    var arr = strUrl.split("?");
    return arr[0];
}
function GetVirtualDirectory() {
    var url = window.location.href;
    var url_parts = url.split("/");

    url_parts[url_parts.length - 1] = "";

    var current_page_virtual_path = url_parts.join("/");
    return current_page_virtual_path;
}
function DateValid(strDate) {
    var Days = 30;

    strDate = ReformatDate(strDate);
    if (strDate.length < 1) {
        return false;
    }
    else {
        var DateSplit = strDate.split('-');
        var iDay = parseInt(DateSplit[2], 10);
        var iMonth = parseInt(DateSplit[1], 10);
        var iYear = parseInt(DateSplit[0], 10);

        try {
            if (iMonth == 1 || iMonth == 3 || iMonth == 5 || iMonth == 7 || iMonth == 8 || iMonth == 10 || iMonth == 12)
                Days = 31;
            else if (iMonth == 2)
                Days = DaysInMonth(iYear, iMonth);
            else if (iMonth == 4 || iMonth == 6 || iMonth == 9 || iMonth == 11)
                Days = 30;

            if (iDay > Days || iDay < 1) {
                ShowMessageBox("Enter a Valid Day.", 1, '');
                return false;
            }
            if (iMonth > 12 || iMonth < 1) {
                ShowMessageBox("Enter a Valid Month.", 1, '');
                return false;
            }
            if (iYear.toString().length < 4 || iYear < 1800) {
                ShowMessageBox("Enter a Valid Year.", 1, '');
                return false;
            }
        }
        catch (ex) {
            ShowMessageBox(ex, 1, '');
            return false;
        }
    }
}
function DisplayMessage(obj, strDefaultMsg) {
    if (obj == null)
    { return strDefaultMsg; }
    else
    { return obj; }
}
function RefreshSetting() {
    var url = window.location.href.toLowerCase();
    if (url.indexOf("searchavailability") != -1) {
        CloseSession();
    }
    tikAeroB2C.WebService.B2cService.GetCurrentStep(SuccessRefreshSetting, showErrorPayment, showTimeOutPayment);
}
function SuccessRefreshSetting(result) {
    if (Number(result) == 1) {
        ShowSearchPannel(true);
    }
    else if (Number(result) == 4) {

        //Display quote information
        GetSessionQuoteSummary();
        //Read Cookies to Control
        GetContactInformationFromCookies();
        //Set passenger input information
        SetPassengerDetail();
        //Load Client information
        tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);

    }
    else if (Number(result) == 5) {

    SetPaymentContent();
    }
    else if (Number(result) == 7) {
        //Load Client information when refresh
        tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);
    }
}
function InitializeDateMaskEdit(inputName) {
    $("#" + inputName).mask(strDateFormat);
}
function ValidPhoneNumber(strPhone) {
    var res = false;
    reg = new RegExp(strPhoneNumberFormat);
    res = (reg.test(strPhone));
    return (res);
}
function ValidEmail(strEmail) {
    var a = false;
    var res = false;

    if (typeof (RegExp) == 'function') {
        var b = new RegExp('abc');
        if (b.test('abc') == true) {
            a = true;
        }
    }
    if (a == true) {
        reg = new RegExp('^([a-zA-Z0-9\\-\\.\\_]+)' + '(\\@)([a-zA-Z0-9\\-\\.]+)' + '(\\.)([a-zA-Z;]{2,4})$');
        res = (reg.test(strEmail));
    }
    else {
        res = (strEmail.search('@') >= 1 &&
		strEmail.lastIndexOf('.') > strEmail.search('@') &&
		strEmail.lastIndexOf('.') >= strEmail.length - 5);
    }
    return (res);
}
function ShowSearchPannel(bShow) {
    var objSearchBox = document.getElementById("dvAvailabilitySearch");

    if (bShow == true) {
        objSearchBox.style.display = "block";
    }
    else {
        objSearchBox.style.display = "none";
    }
    objSearchBox = null;
}
function GetArrayParamValue(arrParam, strName) {
    var arr;
    var arrDetail;
    if (arrParam.length > 0) {
        arr = arrParam.split("|");
        for (var i = 0; i < arr.length; i++) {
            arrDetail = arr[i].split(":");
            if (arrDetail[0] == strName) {
                return arrDetail[1];
            }
        }
    }
    return "";
}
function ReplaceSpecialCharacter(value) {
    var strNewValue = new String(value);
    strNewValue = strNewValue.replace(/[^a-zA-Z 0-9]+/g, '');
    strNewValue = strNewValue.replace(/\s+/g, '');

    return strNewValue;
}
function ContainSpecialCharacter(strValue) {
    var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?~_";
    for (var i = 0; i < strValue.length; i++) {
        if (iChars.indexOf(strValue.charAt(i)) != -1) {
            return true;
        }
    }
    return false;
}
function ConvertToValidXmlData(strValue) {
    var str = "";
    var iCharValue;
    for (var i = 0; i < strValue.length; i++) {
        iCharValue = strValue.charCodeAt(i);
        if (iCharValue < 32 || iCharValue > 127) {
            str = str + "&#" + iCharValue + ";";
        }
        else {
            str = str + strValue.charAt(i).replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace('"', "&quot;").replace("'", "&apos;");
        }
    }
    return str;
}
function GetDayOfWeek(strDate) {
    if (strDate.length > 0) {
        var arrDate = strDate.split("-");
        var dtDate = new Date(arrDate[0] + '/' + arrDate[1] + '/' + arrDate[2]);
        return dtDate.getDay();
    }
    else {
        return "";
    }
}

//*****************************************************************************************
//  Collapse event click call.
function Collapse_Click(strName) {

    var arrName = strName.split("_");
    if ($("#" + strName + " img").attr("src") == "App_Themes/Default/Images/collapse.gif") {

        $("#" + strName + " img").attr("src", "App_Themes/Default/Images/expand.gif");

        if (arrName[0] == "PassengerDetailList") {
            document.getElementById("PassengerName_" + arrName[1]).style.display = "block";
            document.getElementById("PassengerName_" + arrName[1]).innerHTML = document.getElementById("txtName_" + arrName[1]).value + "  " + document.getElementById("txtLastname_" + arrName[1]).value;
        }

    }
    else {

        $("#" + strName + " img").attr("src", "App_Themes/Default/Images/collapse.gif");

        if (arrName[0] == "PassengerDetailList") {
            document.getElementById("PassengerName_" + arrName[1]).style.display = "none";
            document.getElementById("PassengerName_" + arrName[1]).innerHTML = "";
        }

    }


}
//*****************************************************
//  Activate Collapse to control.
function InitializeFareSummaryCollapse(strType) {

    //Tax Making Collapse
    $(function () {
        $("#ul_" + strType + "_TaxInfo").jqcollapse({
            slide: true,
            speed: 400,
            easing: ''
        });
    });

    //Making Fee Collapse
    $(function () {
        $("#ul_" + strType + "_FeeInfo").jqcollapse({
            slide: true,
            speed: 400,
            easing: ''
        });
    });
}

function GetControlValue(obj) {

    if (obj != null) {
        var strValue = trim(obj.value);
        if (strValue == objLanguage.default_value_2 || strValue == objLanguage.default_value_3 || strValue == GetDateMask()) {
            return "";
        }
        else {
            return strValue;
        }
    }
    else {
        return "";
    }
}
function GetControlInnerHtml(obj) {

    if (obj != null) {
        return trim(obj.innerHTML);
    }
    else {
        return "";
    }
}
function GetSelectedOption(obj) {
    if (obj != null && obj.options.length > 0) {
        return obj.options[obj.selectedIndex].value;
    }
    else {
        return "";
    }
}
function GetSelectedOptionText(obj) {
    if (obj != null && obj.options.length > 0) {
        return obj.options[obj.selectedIndex].text;
    }
    else {
        return "";
    }
}

function InitializeWaterMark(ctrlName, strDefaultValue, ctrlType) {
    //Set Initia value
    var ctrl = document.getElementById(ctrlName);
    if (ctrl != null) {
        if (ctrlType == "input") {
            // Define what happens when the textbox comes under focus
            // Remove the watermark class and clear the box
            $("#" + ctrlName).focus(function () {
                $(this).filter(function () {
                    // We only want this to apply if there's not 
                    // something actually entered

                    $(this).removeClass("watermarkOn");

                    return $(this).val() == "" || $(this).val() == strDefaultValue;
                }).val("");

            });

            // Define what happens when the textbox loses focus
            // Add the watermark class and default text
            $("#" + ctrlName).blur(function () {
                $(this).filter(function () {
                    // We only want this to apply if there's not
                    // something actually entered
                    if ($(this).val() == "") {
                        $(this).addClass("watermarkOn");
                    }
                    else {
                        $(this).removeClass("watermarkOn");
                    }
                    return $(this).val() == "" || $(this).val() == GetDateMask();
                }).val(strDefaultValue);
            });

            if (GetControlValue(ctrl).length == 0) {
                $("#" + ctrlName).addClass("watermarkOn");
                ctrl.value = strDefaultValue;
            }
        }
        else if (ctrlType == "select") {
            var myDdl = document.getElementById(ctrlName);

            if (myDdl.options[0].value.length == 0) {
                if (myDdl.options[0].innerHTML.length == 0) {
                    myDdl.options[0].innerHTML = strDefaultValue;
                }
            }
            else {
                myDdl.options[0].className = "";
            }

            for (var i = 1; i < myDdl.options.length; i++) {
                myDdl.options[i].className = "watermarkOff";
            }

            if (myDdl.selectedIndex == 0 && GetSelectedOption(myDdl).length == 0) {
                $("#" + ctrlName).addClass("watermarkOn");
            }

            $("#" + ctrlName).change(function () {
                if (myDdl.selectedIndex == 0 && GetSelectedOption(myDdl).length == 0) {
                    $(this).addClass("watermarkOn");
                }
                else {
                    $(this).removeClass("watermarkOn");
                }
            });
        }
        else if (ctrlType == "password") {


        }
    }
}
function ContainWhiteSpace(str) {
    var whiteSpaceExp = /\s/g;
    if (whiteSpaceExp.test(str))
        return true;
    else
        return false;
}
function GetDateMask() {

    var strMask = "";
    for (var i = 0; i < strDateFormat.length; i++) {
        if (strDateFormat.charAt(i) == "*" || strDateFormat.charAt(i) == "9") {
            strMask = strMask + "_";
        }
        else {
            strMask = strMask + strDateFormat.charAt(i);
        }
    }
    return strMask;
}
function GetSessionQuoteSummary() {
    tikAeroB2C.WebService.B2cService.GetSessionQuoteSummary(SuccessGetSessionQuoteSummary, showErrorPayment, showTimeOutPayment);
}
function SuccessGetSessionQuoteSummary(result) {

    if (result.length > 0) {
        DisplayQuoteSummary(result, "", "");
        //Initialize Summary collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
}
function ShowClientLogonMenu(bValue, strValue) {

    var obj = document.getElementById("dvClientInfo");
    if (bValue == true && strValue.length > 0) {
        obj.style.display = "block";
        obj.innerHTML = strValue;
    }
    else {
        obj.style.display = "block";
        obj.innerHTML = "";
    }
}
function ChangeToDateString(dt) {

    var flightDate = "";
    var d = dt.getDate().toString();
    var m = (dt.getMonth() + 1).toString();

    flightDate = dt.getFullYear().toString();
    if (m.length == 1) {
        flightDate = flightDate + "0" + m;
    }
    else {
        flightDate = flightDate + m;
    }

    if (d.length == 1) {
        flightDate = flightDate + "0" + d;
    }
    else {
        flightDate = flightDate + d;
    }
    return flightDate;
}
function CloseMessageBox() {

    var obj = document.getElementById("dvProgressBar");
    var objMessage = document.getElementById("dvMessageBox");
    var objFormHolder = document.getElementById("dvFormHolder");

    if (objFormHolder != null && (objFormHolder.style.display == "none" || objFormHolder.style.display == "")) {
        obj.style.display = "none";
    }
    objMessage.style.display = "none";
    //Run callback function.
    if (objMessage.getAttribute("CallBack") != null && objMessage.getAttribute("CallBack") != '') {
        if (objMessage.getAttribute("CallBack") == 'loadHome') {
            obj.style.display = "none";
            objMessage.style.display = "none";
            loadHome();
        }
        if (objMessage.getAttribute("CallBack") == 'reload') {
            location.reload(true);
        }
    }

    obj = null;
    objMessage = null;
}

//*************************************************************************
//  Age culculation.
function DaysInMonth(Y, M) {
    with (new Date(Y, M, 1, 12)) {
        setDate(0);
        return getDate();
    }
}
function GetAge(date1, date2) {
    var y1 = date1.getFullYear(),
        m1 = date1.getMonth(), 
        d1 = date1.getDate(),
	    y2 = date2.getFullYear(),
        m2 = date2.getMonth(),
        d2 = date2.getDate();

    if (d1 < d2) {
        m1--;
        d1 += DaysInMonth(y2, m2);
    }
    if (m1 < m2) {
        y1--;
        m1 += 12;
    }
    return [y1 - y2, m1 - m2, d1 - d2];
}
function CalculateAge(strDate) {

    if (strDate.length > 0) {
        var dat = new Date();
        var curday = dat.getDate();
        var curmon = dat.getMonth() + 1;
        var curyear = dat.getFullYear();

        var arrCurrentDate = ReformatDate(strDate).split("-");
        var calday = arrCurrentDate[2];
        var calmon = arrCurrentDate[1];
        var calyear = arrCurrentDate[0];
        if (curday == "" || curmon == "" || curyear == "" || calday == "" || calmon == "" || calyear == "") {
            return "";
        }
        else {
            var curd = new Date(curyear, curmon - 1, curday);
            var cald = new Date(calyear, calmon - 1, calday);
            var diff = Date.UTC(curyear, curmon, curday, 0, 0, 0) - Date.UTC(calyear, calmon, calday, 0, 0, 0);
            var dife = GetAge(curd, cald);
           
            return "{\"year\":\"" + dife[0] + "\", \"month\":\"" + dife[1] + "\", \"day\":\"" + dife[2] + "\"}";   
        }
    }
}
//*************************************************************************


