﻿//*--------------------------------------------------------
//*** AJAX functional call.
function GetLowFareFinder() {
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    var arrAirport = objDestination.options[objDestination.selectedIndex].value.split("|");

    var strOrigin = arrAirport[0];
    var strDestination = arrAirport[2];
    var objOneWay = document.getElementById("optOneWay");

    var dtDateFrom = getdatefrom();
    var dtDateTo = 0;

    if (objOneWay.checked == false)
    { dtDateTo = getdateto(); }

    //Start Check Minimum Date that can select low fare finder.
    var d = Number(dtDateFrom.toString().substring(6, 8));
    var m = Number(dtDateFrom.toString().substring(4, 6));
    var y = Number(dtDateFrom.toString().substring(0, 4));

    var iDate = new Date(y, m - 1, d);

    var todayDate = new Date();
    var cDate = new Date(todayDate.getYear(), todayDate.getMonth(), todayDate.getDate());
    //End Check Minimum Date that can select low fare finder.

    if (dtDateTo != 0 && (dtDateFrom > dtDateTo)) {

        ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
    }
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.GetLowFareFinder(strOrigin, strDestination, dtDateFrom, dtDateTo, SuccessGetLowFareFinder, showError, dtDateFrom + "|" + dtDateTo);
    }
    objDestination = null;
}
function GetNextLowFareFinder(searchDate, flightType) {
    ShowProgressBar(true);
    if (flightType == 'Return')
        tikAeroB2C.WebService.B2cService.GetLowFareFinder("", "", "0", searchDate, SuccessGetLowFareFinder, showError, showTimeOut);
    else if (flightType == 'OutWard')
        tikAeroB2C.WebService.B2cService.GetLowFareFinder("", "", searchDate, "0", SuccessGetLowFareFinder, showError, showTimeOut);
}
function SearchLowFareSingleMonth(flightDate, strFlightType) {

    if (strFlightType == "Outward") {

        //setFromDate(flightDate);
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SearchLowFareSingleMonth(flightDate, true, SuccessGetLowFareFinder, showError, getdatefrom() + "|" + getdateto());
    }
    else {

        //setToDate(flightDate);
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SearchLowFareSingleMonth(flightDate, false, SuccessGetLowFareFinder, showError, getdatefrom() + "|" + getdateto());
    }
}
function SuccessGetLowFareFinder(result, strSelectDate) {
    if (result.length > 0) {
        if (result == "{102}") {
            //Invalid Required Parameter.
            ShowMessageBox(objLanguage.Alert_Message_55, 0, '');
        }
        else if (result == "{103}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else if (result == "{002}") {
            loadHome();
        }
        else {
            var obj = document.getElementById("dvContainer");
            obj.innerHTML = result;
            var arrDate = strSelectDate.split("|");
            var objOutWare = document.getElementsByName("optLowfare_Outward");
            var objReturn = document.getElementsByName("optLowfare_Return");

            var todayDate = new Date();
            var cDate = new Date(todayDate.getYear(), todayDate.getMonth(), todayDate.getDate());

            var strSelectDate;
            var d = Number(strSelectDate.substring(6, 8));
            var m = Number(strSelectDate.substring(4, 6));
            var y = Number(strSelectDate.substring(0, 4));

            var iDate;
            var objJSON;

            DisplayQuoteSummary("", "", "");
            if (objOutWare != null && objOutWare.length > 0) {
                for (var i = 0; i < objOutWare.length; i++) {
                    objJSON = eval("(" + objOutWare[i].value + ")");
                    strSelectDate = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                    var d = Number(strSelectDate.substring(6, 8));
                    var m = Number(strSelectDate.substring(4, 6));
                    var y = Number(strSelectDate.substring(0, 4));

                    iDate = new Date(y, m - 1, d);
                    //Remove date at are not match the 21 days
                    if (dateDiff("d", cDate, iDate) <= 21) {
                        objOutWare[i].style.display = "none";
                    }
                    //Select at selected date
                    if (strSelectDate == arrDate[0]) {
                        objOutWare[i].checked = true;
                    }
                    iDate = null;
                }
                //Highlight Selected date
                LffHighLight("Outward", true);
                GetSelectLowFareSummary("Outward");
            }

            if (objReturn != null && objReturn.length > 0) {
                for (var i = 0; i < objReturn.length; i++) {
                    objJSON = eval("(" + objReturn[i].value + ")");
                    strSelectDate = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                    var d = Number(strSelectDate.substring(6, 8));
                    var m = Number(strSelectDate.substring(4, 6));
                    var y = Number(strSelectDate.substring(0, 4));

                    iDate = new Date(y, m - 1, d);
                    //Remove date at are not match the 21 days
                    if (dateDiff("d", cDate, iDate) <= 21) {
                        objReturn[i].style.display = "none";
                    }
                    //Select at selected date
                    if (strSelectDate == arrDate[1]) {
                        objReturn[i].checked = true;
                    }
                }
                //Highlight Selected date
                LffHighLight("Return", true);
                GetSelectLowFareSummary("Return");
            }
            ShowSearchPannel(true);
            objOutWare = null;
            objReturn = null;
            ShowProgressBar(false);
        }
    }
}

function GetLowFareFinderSummary(strFligtType, objFlightParam) {
    switch (strFligtType) {
        case "Outward":
            tmpOutFare_id = objFlightParam.fare_id.replace('{', '');
            tmpOutFare_id = tmpOutFare_id.replace('}', '');
            tmpOutFlight_id = objFlightParam.flight_id.replace('{', '');
            tmpOutFlight_id = tmpOutFlight_id.replace('}', '');
            break;
        case "Return":
            tmpRetFare_id = objFlightParam.fare_id.replace('{', '');
            tmpRetFare_id = tmpRetFare_id.replace('}', '');
            tmpRetFlight_id = objFlightParam.flight_id.replace('{', '');
            tmpRetFlight_id = tmpRetFlight_id.replace('}', '');
            break;
    }

    var strXml = "<flights>" +
                         "<flight>" +
                            "<airline_rcd>" + objFlightParam.airline_rcd + "</airline_rcd>" +
                            "<flight_number>" + objFlightParam.flight_number + "</flight_number>" +
                            "<flight_id>" + objFlightParam.flight_id + "</flight_id>" +
                            "<origin_rcd>" + objFlightParam.origin_rcd + "</origin_rcd>" +
                            "<destination_rcd>" + objFlightParam.destination_rcd + "</destination_rcd>" +
                            "<booking_class_rcd>" + objFlightParam.booking_class_rcd + "</booking_class_rcd>" +
                            "<fare_id>" + objFlightParam.fare_id + "</fare_id>" +
                            "<transit_flight_id>" + objFlightParam.transit_flight_id + "</transit_flight_id>" +
                            "<departure_date>" + objFlightParam.departure_date + "</departure_date>" +
                            "<arrival_date>" + objFlightParam.arrival_date + "</arrival_date>" +
                            "<transit_departure_date>" + objFlightParam.transit_departure_date + "</transit_departure_date>" +
                            "<transit_arrival_date>" + objFlightParam.transit_arrival_date + "</transit_arrival_date>" +
                            "<transit_airport_rcd>" + objFlightParam.transit_airport_rcd + "</transit_airport_rcd>" +
                            "<planned_departure_time>" + objFlightParam.planned_departure_time + "</planned_departure_time>" +
                            "<planned_arrival_time>" + objFlightParam.planned_arrival_time + "</planned_arrival_time>" +
                              "<transit_fare_id>" + objFlightParam.transit_fare_id + "</transit_fare_id>" +
                             "<transit_airline_rcd>" + objFlightParam.transit_airline_rcd + "</transit_airline_rcd>" +
                            "<transit_flight_number>" + objFlightParam.transit_flight_number + "</transit_flight_number>" +
                            "<transit_arrival_date>" + objFlightParam.transit_arrival_date + "</transit_arrival_date>" +
                            "<transit_planned_departure_time>" + objFlightParam.transit_planned_departure_time + "</transit_planned_departure_time>" +
                            "<transit_planned_arrival_time>" + objFlightParam.transit_planned_arrival_time + "</transit_planned_arrival_time>" +
                        "</flight>" +
                    "</flights>";    //Call Webservice
    tikAeroB2C.WebService.B2cService.GetQuoteSummary(strXml,
                                                     strFligtType,
                                                     SuccessGetLowFareFinderSummary,
                                                     showError,
                                                     strFligtType);
}

function SuccessGetLowFareFinderSummary(result, strType) {

    if (result == "{100}" || result == "{101}" || result == "{004}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
    }
    else {

        if (strType == "Outward") {
            DisplayQuoteSummary("", result, "");
        }
        else {
            DisplayQuoteSummary("", "", result);
        }
        //Initialize Collape
        InitializeFareSummaryCollapse(strType);
    }

}

//*----------------------------------------------------
//** Non AJAX functional call.
function GetSelectLowFareSummary(strFlightType) {
    var objOpt = document.getElementsByName("optLowfare_" + strFlightType);
    var objHd = document.getElementsByName("hdTime_" + strFlightType);
    var objJSON = null;

    if (objOpt != null && objOpt.length > 0) {
        for (var i = 0; i < objOpt.length; i++) {
            if (objOpt[i].checked == true) {
                objJSON = eval("(" + objHd[i].value + ")");
                //Call display summary.
                GetLowFareFinderSummary(strFlightType, objJSON);
                break;
            }
        }
    }
}
function LffHighLight(strName, bSelect) {
    var objOptLowFare = document.getElementsByName("optLowfare_" + strName);
    var strId = "";
    if (objOptLowFare != null) {
        for (var i = 0; i < objOptLowFare.length; i++) {
            if (objOptLowFare[i].checked == true) {
                strId = objOptLowFare[i].id.split("_")[2];
            }
        }
    }

    var objTd = document.getElementById("td_" + strName + "_" + strId);
    var ObjSpn = document.getElementById("spnLowfare_" + strName + "_" + strId);
    if (bSelect == true) {
        if (objTd != null)
        { objTd.className = "Select"; }

        if (ObjSpn != null) {
            if (ObjSpn.className == "LowestFare") {
                ObjSpn.className = "LowestFareselect"; 
            }
            else {
                ObjSpn.className = "Lowfarepriceselect"; 
            }
        }
    }
    else {
        if (objTd != null)
        { objTd.className = ""; }
        if (ObjSpn != null) {
            if (ObjSpn.className == "LowestFareselect") {
                ObjSpn.className = "LowestFare"; 
            }
            else {
                ObjSpn.className = "Lowfareprice"; 
            }    
        }
    }

    objOptLowFare = null;
}

function SelectLlf(strName) {
    LffHighLight(strName.split("_")[0], false);
    document.getElementById("optLowfare_" + strName).checked = true;
    LffHighLight(strName.split("_")[0], true);
    return false;
}
