﻿
// Start Search Flight Availability from Query string
function GenerateFromQueryString() {
    if (window.location.search.substring(1) != "") {
        // Check My Booking First
        if (checkMyBookingString()) {
            LoadCob(false, '');
        }
        else if (CheckClientLogon()) {
            LoadDialog();
        }
        else if (CheckRegisterLogon()) {
            LoadRegistration();
        }
        else if (existEachQueryString("sn")) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.LoadManageBooking(queryStringValue("sn"), SuccessLoadCob, showError, showTimeOut);
        }
        else {
            RefreshSetting();
        }
    }
    else {
        RefreshSetting();
    }
}

function IsJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
//multipletab
function SetBookingId(id) {
    window.localStorage.setItem("bookingid", id);
}

function GetBookingId() {
    try {
        if (window.localStorage) return window.localStorage.getItem("bookingid");
    }
    catch (e) {
        return undefined;
    }
}

function SetStep(step) {
    sessionStorage.setItem("currentstep", step);
    window.name = step;
}

function GetStep() {
    if (window.name == sessionStorage.getItem("currentstep"))
        return sessionStorage.getItem("currentstep");
    else
        return window.name;
}

function IsMultipleTab(clientStep, serverStep) {
    if (clientStep == serverStep) {
        return false;
    }
    else {
        MultiTabAlert();
    }
}
function MultiTabAlert() {
    var tmpLangcode = "en";
    if (LanguageCode.split("-").length == 2)
        tmpLangcode = LanguageCode.split("-")[0];
    window.open("http://sorrypage.flypeach.com/" + tmpLangcode + "/multi_tab_alert.html", "_self");
}

function IsMultipleTabBookId() {
    var booking_id = document.getElementById("hdBooking_id");
    var id = GetBookingId();
    var found = false;
    if (id == null || id == "") {
        SetBookingId(booking_id);
        found = true;
    }
    else {
        found = false;
        MultiTabAlert();
    }
}

function IsMultipleTabOpen(objJSON) {
    return false;
}
function GetTabNumber() {
    //Check unmber of tab open from cookies
    var iNumberOfCurrentTab = getCookie("cGetTab");

    var dtExpired = new Date();
    dtExpired.setDate(dtExpired.getDate() + 1);

    if (iNumberOfCurrentTab != null && iNumberOfCurrentTab.length > 0) {

        iNumberOfCurrentTab = ++iNumberOfCurrentTab;
    }
    else {

        iNumberOfCurrentTab = 1;
    }

    //Set current tab open
    setCookie("cGetTab", iNumberOfCurrentTab, dtExpired, "", "", false);
    return iNumberOfCurrentTab;
}

function ClarNumberOfTab() {
    //Check unmber of tab open from cookies
    var iNumberOfCurrentTab = getCookie("cGetTab");

    if (iNumberOfCurrentTab != null && iNumberOfCurrentTab.length > 0) {

        if (parseInt(iNumberOfCurrentTab) == 1) {
            //Delete tab count if only one tab is open.
            deleteCookie("cGetTab");
        }
        else {
            iNumberOfCurrentTab = --iNumberOfCurrentTab;
            //Set current tab open
            var dtExpired = new Date();
            dtExpired.setDate(dtExpired.getDate() + 1);
            setCookie("cGetTab", iNumberOfCurrentTab, dtExpired, "", "", false); 
        }
    }
}
function LoadMainPage() {
    ShowProgressBar(true);
    CloseSession();
    tikAeroB2C.WebService.B2cService.ClearSession(SuccessLoadMainPage, showError, showTimeOut);
}
function SuccessLoadMainPage(result) {
    window.location.href = "http://www.flypeach.com/";
    ShowProgressBar(false);
}

