﻿function LoadB2BLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2BLogon(SuccessLoadB2BLogon, showError, showTimeOut);
}

function SuccessLoadB2BLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2BLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2BLogonDialog(SuccessLoadB2BLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2BLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function LoadB2BAdminLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2BAdminLogon(SuccessLoadB2BAdminLogon, showError, showTimeOut);
}

function SuccessLoadB2BAdminLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2BAdminLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2BAdminLogonDialog(SuccessLoadB2BAdminLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2BAdminLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    //Insert passenger form content.
    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}
function AgencyLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.AgencyLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogLogonID').value, document.getElementById('txtDialogPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.AgencyLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtLogonID').value, document.getElementById('txtPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyAdminLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.AgencyAdminLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogAdminLogonID').value, document.getElementById('txtDialogPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyAdminLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.AgencyAdminLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtAdminLogonID').value, document.getElementById('txtPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function SuccessAgencyLogon(result) {
    var arrResult = result.split(",");

    if (arrResult.length == 2 && arrResult[0] == 'Error') {
        ShowProgressBar(false);
        if (arrResult[1] == "0") {
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
        else {
            ShowMessageBox(arrResult[1], 0, '');
        }
    }
    else {
        window.location.replace(result);
    }
}
function SubmitEnterAgencyLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            AgencyLogon();
        else
            AgencyLogonDialog();
        return false;
    }
    else
        return true;
}
function SubmitEnterAgencyAdminLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            AgencyAdminLogon();
        else
            AgencyAdminLogonDialog();
        return false;
    }
    else
        return true;
}