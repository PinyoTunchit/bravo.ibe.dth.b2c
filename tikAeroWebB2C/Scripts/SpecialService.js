﻿function FillSpecialService() {
    var objFeeId = document.getElementsByName("hdSsrFeeId");
    var objSsrCode = document.getElementsByName("hdSsrCode");
    var objSsrOnRequestFlag = document.getElementsByName("hdSsrOnRequestFlag");
    var objSsrSegment;
    var strXml;
    var objSsr;
    strXml = "<booking>";

    //Loop through fee type
    for (var i = 0; i < objFeeId.length; i++) {
        //Loop throught number of selection box(number of passenger + number of segment).
        objSsrSegment = document.getElementsByName("seSsrAmount_" + (i + 1));

        for (var j = 0; j < objSsrSegment.length; j++) {
            //Loop Through segment(if not married flight it will loop only one time per segment).
            objSsr = eval("([" + objSsrSegment[j].options[objSsrSegment[j].selectedIndex].value + "])");
            for (var k = 0; k < objSsr.length; k++) {
                strXml = strXml +
                        "<service>" +
                            "<passenger_id>" + objSsr[k].passenger_id + "</passenger_id>" +
                            "<booking_segment_id>" + objSsr[k].booking_segment_id + "</booking_segment_id>" +
                            "<origin_rcd>" + objSsr[k].origin_rcd + "</origin_rcd>" +
                            "<destination_rcd>" + objSsr[k].destination_rcd + "</destination_rcd>" +
                            "<fee_id>" + objFeeId[i].value + "</fee_id>" +
                            "<special_service_rcd>" + objSsrCode[i].value + "</special_service_rcd>" +
                            "<service_text>" + document.getElementById("spnSsrDisplayName_" + (i + 1)).innerHTML + "</service_text>" +
                            "<number_of_units>" + objSsrSegment[j].options[objSsrSegment[j].selectedIndex].text + "</number_of_units>" +
                            "<service_on_request_flag>" + objSsrOnRequestFlag[i].value + "</service_on_request_flag>" +
                        "</service>";
            }
            objSsr = null;
        }
        objSsrSegment = null;
    }
    strXml = strXml + "</booking>";
    CloseDialog();
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.FillSpecialService(strXml, true, SuccessFillSpecialService, showError, showTimeOut);

    objFeeId = null;
    objSsrCode = null;
    objSsrOnRequestFlag = null;
}
function SuccessFillSpecialService(result) {
    var objFareSummary = document.getElementById(FindControlName("div", "dvFareSummary"));
    if (objFareSummary != null) {
        objFareSummary.innerHTML = result;
        objFareSummary = null;

        //Initilalise Summary Collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
    ShowProgressBar(false);
}
function CalculatePaxSsr(iRow) {
    var objSelect = document.getElementsByName("seSsrAmount_" + iRow);
    var objSpn = null;
    var dclTotal = 0;

    for (var i = 0; i < objSelect.length; i++) {
        if (objSelect[i].style.display == "block" || objSelect[i].style.display == "") {
            objSpn = document.getElementById("spnSsrFeeAmount_" + iRow + "_" + objSelect[i].id.split("_")[2]);
            dclTotal = dclTotal + (parseInt(objSelect[i].options[objSelect[i].selectedIndex].text) * parseFloat("0" + objSpn.innerHTML));
        }
    }
}

function ShowSpecialService(strPaxId) {
    if (strPaxId.length > 0) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ShowSpecialService(strPaxId, SuccessShowSpecialService, showError, showTimeOut);
    }

}
function SuccessShowSpecialService(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        var objMessage = document.getElementById("dvFormHolder");
        var objContainer = document.getElementById("dvProgressBar");

        //Insert passenger form content.
        objMessage.innerHTML = result;

        //Calculate Service.
        CalculateSpecialService();

        //Show Fading.
        objContainer.style.display = "block";
        objMessage.style.display = "block";

        objMessage = null;
        objContainer = null;
    }
}
function CalculateSpecialService() {
    var objFeeId = document.getElementsByName("hdSsrFeeId");
    var objSsrSegment;

    //Loop through fee type
    for (var i = 0; i < objFeeId.length; i++) {
        //Loop throught number of selection box(number of passenger + number of segment).
        objSsrSegment = document.getElementsByName("seSsrAmount_" + (i + 1));
        //Calculate Total Ssr per passenger.
        CalculatePaxSsr((i + 1));
        objSsrSegment = null;
    }

    objFeeId = null;
}