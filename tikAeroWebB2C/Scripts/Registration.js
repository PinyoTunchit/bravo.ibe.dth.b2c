﻿// JScript File

function LoadRegistration() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadRegistration(SuccessloadRegistration, showError, showTimeOut);
}
function LoadRegistration_Edit() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_Edit, showError, showTimeOut);
}
function LoadRegistration_EditPassword() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_EditPassword, showError, showTimeOut);
}
function LoadRegistration_EditPasswordDis() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_EditPasswordDis, showError, showTimeOut);
}
function SuccessloadRegistration(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;

    ShowSearchPannel(false);

    InitializeDateFormat('ctl00_');
    InitializeRegistrationWaterMark("ctl00_");

    DisplayQuoteSummary("", "", "");
    ShowSearchPannel(true);
    ShowProgressBar(false);
    objContainer = null;
}
function SuccessloadRegistration_Edit(result) {

    SetRegistrationLoad(result);

    //passenger profile Collapse
    var objPaxLastname = document.getElementsByName("txtPsgLastName");
    if (objPaxLastname.length > 0) {

        for (i = 1; i <= objPaxLastname.length; i++) {
            $(function () {
                $("#ulPassenger_" + i).jqcollapse({
                    slide: true,
                    speed: 400,
                    easing: ''
                });
            });
        }
        $("#ulPassenger_1").find("a").click();
    }

    //Hide password information
    ShowChangePassword(false);

    InitializeDateFormatPassenger();
    InitializeRegistrationWaterMark("ctl00_");
    

    DisplayQuoteSummary("", "", "");
    ShowSearchPannel(true);
    ShowProgressBar(false);
    objContainer = null;
    scroll(0, 0);
}
function SuccessloadRegistration_EditPassword(result) {
    SetRegistrationLoad(result);
    ChangePassword('ctl00_');

    //Hide passenger profile detail information.
    var objdvPassengerWrapper = document.getElementById("dvPassengerProfileWrapper");
    if (objdvPassengerWrapper != null) {
        objdvPassengerWrapper.style.display = "none";
    }

    ShowSearchPannel(false);
    DisplayQuoteSummary("", "", "");
    ShowProgressBar(false);
    objContainer = null;
}
function SuccessloadRegistration_EditPasswordDis(result) {

    SetRegistrationLoad(result);
    ShowProgressBar(false);
    objContainer = null;
}
function SetRegistrationLoad(result) {
    var objContainer = document.getElementById("dvContainer");
    objContainer.innerHTML = result;

    var objpassword = document.getElementById("ctl00_txtPassword");
    var objReconfirmpassword = document.getElementById("ctl00_txtReconfirmPassword");

    objpassword.value = "sssssssss";
    objReconfirmpassword.value = "sssssssss";

    InitializePassengerAddWaterMark("ctl00_");
    InitializeDateFormatPassengerAdd('ctl00_');

    disabledField('ctl00_');
}
function SuccessCreateClientProfile(result) {

    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "01") {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, '');
        }
        else if (result == "02") {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_87, "Email already exist!!"), 0, '');
        }
        else {
            ClearData('ctl00_');
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_88, "Your profile has been established as ") + result, 1, 'loadHome');
        }
    }
    else {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, '');
    }
}
function SuccessClientProfileSave(result) {
    if (result == true) {
        ShowProgressBar(false);
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_89, "Save Success"), 0, '');
        LoadRegistration_Edit();
    }
    else {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, '');
    }
}
function SuccessClientPasswordSave(result) {
    if (result == true) {
        ShowProgressBar(false);
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_89, "Save Success"), 1, '');
        LoadRegistration_EditPasswordDis();
    }
    else {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, '');
    }
}
function SuccessPassengerSave(result) {
    if (result == true) {
        ShowProgressBar(false);
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_89, "Save Success"), 0, '');
        LoadRegistration_Edit();
    }
    else {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, '');
    }
}

function CreateClientProfile(o) {
    Status = true
    Status = ValidateRegister(o);

    if (Status == true) {
        var Title = GetSelectedOption(document.getElementById(o + 'ddlTitle'));

        var gender = 'M';
        var first_name = GetControlValue(document.getElementById(o + 'txtFirstName'));
        var last_name = GetControlValue(document.getElementById(o + 'txtLastName'));
        var dateOfBirth = GetControlValue(document.getElementById(o + 'txtDateofBirth'));
        var passportnumber = GetControlValue(document.getElementById(o + 'txtDocumentNumber'));
        var nationality = GetSelectedOption(document.getElementById(o + 'optNationality'));
        var documenttype = GetSelectedOption(document.getElementById(o + 'optDocumentType'));
        var PassengerType = GetSelectedOption(document.getElementById('ddlPassengerType'));

        var passportIssuePlace = GetControlValue(document.getElementById(o + 'txtPlaceOfIssue'));
        var passportBirthPlace = GetControlValue(document.getElementById(o + 'txtPlaceOfBirth'));
        var passportIssueDate = GetControlValue(document.getElementById(o + 'txtIssueDate'));
        var passportExpiryDate = GetControlValue(document.getElementById(o + 'txtExpiryDate'));
        var client_password = GetControlValue(document.getElementById(o + 'txtPassword'));
        var phoneMobile = GetControlValue(document.getElementById(o + 'txtMobilePhone'));
        var phoneHome = GetControlValue(document.getElementById(o + 'txtHomePhone'));
        var phoneBusiness = GetControlValue(document.getElementById(o + 'txtBusinessPhone'));
        var contactEmail = GetControlValue(document.getElementById(o + 'txtEmail'));
        var mobileEmail = GetControlValue(document.getElementById(o + 'txtMobileEmail'));
        var language = GetSelectedOption(document.getElementById(o + 'optLanguage'));

        var addressline1 = GetControlValue(document.getElementById(o + 'txtAddress1'));
        var addressline2 = GetControlValue(document.getElementById(o + 'txtAddressline2'));
        var zipCode = GetControlValue(document.getElementById(o + 'txtZipCode'));
        var city = GetControlValue(document.getElementById(o + 'txtcity'));
        var state = GetControlValue(document.getElementById(o + 'txtState'));

        var country = GetSelectedOption(document.getElementById(o + 'ddlCountry'));
        var url = window.location;
        var success;
        first_name = ReplaceStringSave(first_name);
        last_name = ReplaceStringSave(last_name);

        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.CreateClientProfile(Title,
                                                            gender,
                                                            first_name,
                                                            last_name,
                                                            ReformatDate(dateOfBirth),
                                                            nationality,
                                                            documenttype,
                                                            passportnumber,
                                                            passportIssuePlace,
                                                            passportBirthPlace,
                                                            ReformatDate(passportIssueDate),
                                                            ReformatDate(passportExpiryDate),
                                                            client_password,
                                                            phoneMobile,
                                                            phoneHome,
                                                            phoneBusiness,
                                                            contactEmail,
                                                            language,
                                                            addressline1,
                                                            addressline2,
                                                            zipCode,
                                                            city,
                                                            country,
                                                            PassengerType,
                                                            String(url.href),
                                                            mobileEmail,
                                                            state,
                                                            SuccessCreateClientProfile,
                                                            showError,
                                                            showTimeOut);
    }
}
function ReplaceStringSave(str) {
    str = str.replace(".", "");
    str = str.replace("-", "");
    str = str.replace(" ", "");
    str = str.replace("  ", "");
    str = str.replace("   ", "");
    str = str.replace("'", "");
    str = str.replace("_", "");
    return str;
}
function ClearData(o) {
    document.getElementById(o + "txtFirstName").value = "";
    document.getElementById(o + "txtLastName").value = "";
    document.getElementById(o + "txtDateofBirth").value = objLanguage.default_value_1;
    document.getElementById(o + "txtIssueDate").value = objLanguage.default_value_1;
    document.getElementById(o + "txtExpiryDate").value = objLanguage.default_value_1;
    document.getElementById(o + "txtDocumentNumber").value = "";
    document.getElementById(o + "txtPlaceOfIssue").value = "";
    document.getElementById(o + "txtPlaceOfBirth").value = "";

    document.getElementById(o + "txtPassword").value = "";
    document.getElementById(o + "txtMobilePhone").value = "";
    document.getElementById(o + "txtHomePhone").value = "";
    document.getElementById(o + "txtBusinessPhone").value = "";
    document.getElementById(o + "txtEmail").value = "";
    document.getElementById(o + "txtMobileEmail").value = "";

    document.getElementById(o + "txtAddress1").value = "";
    document.getElementById(o + "txtAddressline2").value = "";
    document.getElementById(o + "txtZipCode").value = "";
    document.getElementById(o + "txtcity").value = "";
    document.getElementById(o + "txtState").value = "";
    document.getElementById(o + "txtReconfirmPassword").value = "";

    document.getElementById(o + "LabError").value = "";
    document.getElementById(o + "cboConfirm").checked = false;
}

function ClearPassenger(o) {
    var passportIssuePlace = document.getElementById(o + "txtPsgPlaceOfIssue").value;
    var passportBirthPlace = document.getElementById(o + "txtPsgPlaceOfBirth").value;
    var passportIssueDate = document.getElementById(o + "txtPsgIssueDate").value;
    var passportExpiryDate = document.getElementById(o + "txtPsgExpiryDate").value;

    document.getElementById(o + "txtPsgFirstName").value = "";
    document.getElementById(o + "txtPsgLastName").value = "";
    document.getElementById(o + "TxtPsgDateofBirth").value = objLanguage.default_value_1;
    document.getElementById(o + "txtPsgDocumentNumber").value = "";

    document.getElementById(o + "txtPsgPlaceOfIssue").value = "";
    document.getElementById(o + "txtPsgPlaceOfBirth").value = "";
    document.getElementById(o + "txtPsgIssueDate").value = objLanguage.default_value_1;
    document.getElementById(o + "txtPsgExpiryDate").value = objLanguage.default_value_1;

    document.getElementById(o + "LabError").value = "";
}
function CreateClientProfileresult(result) {
    if (result == "loadmaintainprofile") {
        loadmaintainprofile();
    }
    else {
        var errorResult = result.split(",")
        if (errorResult.length == 2 && errorResult[0] == "Error") {
            if (errorResult[1] == "session_expired") {
                loadLogout();
            }
            else {
                cleanErrorpanel();
                var pnError = document.getElementById("pnError");
                pnError.innerHTML += "- " + errorResult[1] + "<br />";
            }
        }
        else {
            document.getElementById("content").innerHTML = result;
        }
    }
}
function ClientProfileSave(o, iCount) {
    var Status = true;
    if (iCount == 0) {
        Status = ValidateRegister_Edit(o);
    }
    else {
        Status = ValidatePassenger(iCount);
    }

    if (Status == true) {
        var Title = GetSelectedOption(document.getElementById(o + "ddlTitle"));

        var gender = "M";
        var first_name = GetControlValue(document.getElementById(o + "txtFirstName"));
        var last_name = GetControlValue(document.getElementById(o + "txtLastName"));
        var client_password = GetControlValue(document.getElementById(o + "txtPassword"));
        var phoneMobile = GetControlValue(document.getElementById(o + "txtMobilePhone"));
        var phoneHome = GetControlValue(document.getElementById(o + "txtHomePhone"));
        var phoneBusiness = GetControlValue(document.getElementById(o + "txtBusinessPhone"));
        var contactEmail = GetControlValue(document.getElementById(o + "txtEmail"));
        var mobileEmail = GetControlValue(document.getElementById(o + "txtMobileEmail"));
        var language = GetSelectedOption(document.getElementById(o + "optLanguage"));

        var addressline1 = GetControlValue(document.getElementById(o + "txtAddress1"));
        var addressline2 = GetControlValue(document.getElementById(o + "txtAddressline2"));
        var zipCode = GetControlValue(document.getElementById(o + "txtZipCode"));
        var city = GetControlValue(document.getElementById(o + "txtcity"));
        var state = GetControlValue(document.getElementById(o + "txtState"));

        var country = GetSelectedOption(document.getElementById(o + "ddlCountry"));

        var nationality = GetSelectedOption(document.getElementById("optNationality_1"));
        var optDocumentType = GetControlValue(document.getElementById("optDocumentType_1"));
        var PassengerRole = GetSelectedOption(document.getElementById("ddlPassengerRole_1"));
        var PassengerType = GetSelectedOption(document.getElementById("ddlPassengerType_1"));

        var passportIssuePlace = GetControlValue(document.getElementById("txtPsgPlaceOfIssue_1"));
        var passportBirthPlace = GetControlValue(document.getElementById("txtPsgPlaceOfBirth_1"));

        var passportIssueDate = GetControlValue(document.getElementById("txtPsgIssueDate_1"));
        var passportExpiryDate = GetControlValue(document.getElementById("txtPsgExpiryDate_1"));
        var dateOfBirth = GetControlValue(document.getElementById("TxtPsgDateofBirth_1"));
        var passportnumber = GetControlValue(document.getElementById("txtPsgDocumentNumber_1"));

        var url = window.location;
        var success = false;

        var TitlePsg = "";
        var genderPsg = "M";
        var first_namePsg = "";
        var last_namePsg = "";
        var dateOfBirthPsg = "";
        var passportnumberPsg = "";
        var nationalityPsg = "";
        var documenttypePsg = "";
        var PassengerRolePsg = "";
        var PassengerTypePsg = "";
        var passportIssuePlacePsg = "";
        var passportBirthPlacePsg = "";
        var passportIssueDatePsg = "";
        var passportExpiryDatePsg = "";
        var hdPassengerIdPsg = "";
        var hdpassenger_profile_idPsg = "";
        var hdclient_profile_idPsg = "";
        var hdclient_numberPsg = "";
        var hdcreate_byPsg = "";
        var hdupdate_byPsg = "";
        var hdupdate_date_timePsg = "";
        var hducreate_date_timePsg = "";
        var hdwheelchair_flagPsg = "";
        var hdvip_flagPsg = "";
        var hdpassenger_weightPsg = "";
        var strPassenger = "";

        first_name = ReplaceStringSave(first_name);
        last_name = ReplaceStringSave(last_name);

        var datetime = new Date();
        if (iCount != 0) {
            TitlePsg = GetSelectedOption(document.getElementById("ddlPsgTitle" + "_" + iCount));
            genderPsg = "M";
            first_namePsg = GetControlValue(document.getElementById("txtPsgFirstName" + "_" + iCount));
            last_namePsg = GetControlValue(document.getElementById("txtPsgLastName" + "_" + iCount));
            dateOfBirthPsg = GetControlValue(document.getElementById("TxtPsgDateofBirth" + "_" + iCount));
            passportnumberPsg = GetControlValue(document.getElementById("txtPsgDocumentNumber" + "_" + iCount));
            nationalityPsg = GetSelectedOption(document.getElementById("optNationality" + "_" + iCount));
            documenttypePsg = GetSelectedOption(document.getElementById("optDocumentType" + "_" + iCount));
            PassengerRolePsg = GetSelectedOption(document.getElementById("ddlPassengerRole" + "_" + iCount));
            PassengerTypePsg = GetSelectedOption(document.getElementById("ddlPassengerType" + "_" + iCount));

            passportIssuePlacePsg = GetControlValue(document.getElementById("txtPsgPlaceOfIssue" + "_" + iCount));
            passportBirthPlacePsg = GetControlValue(document.getElementById("txtPsgPlaceOfBirth" + "_" + iCount));
            passportIssueDatePsg = GetControlValue(document.getElementById("txtPsgIssueDate" + "_" + iCount));
            passportExpiryDatePsg = GetControlValue(document.getElementById("txtPsgExpiryDate" + "_" + iCount));

            hdPassengerIdPsg = GetControlValue(document.getElementById("hdPassengerId" + "_" + iCount));
            hdpassenger_profile_idPsg = GetControlValue(document.getElementById("hdpassenger_profile_id" + "_" + iCount));
            hdclient_profile_idPsg = GetControlValue(document.getElementById("hdclient_profile_id" + "_" + iCount));
            hdclient_numberPsg = GetControlValue(document.getElementById("hdclient_number" + "_" + iCount));
            hdwheelchair_flagPsg = GetControlValue(document.getElementById("hdwheelchair_flag" + "_" + iCount));
            hdvip_flagPsg = GetControlValue(document.getElementById("hdvip_flag" + "_" + iCount));
            hdpassenger_weightPsg = GetControlValue(document.getElementById("hdpassenger_weight" + "_" + iCount));

            first_namePsg = ReplaceStringSave(first_namePsg);
            last_namePsg = ReplaceStringSave(last_namePsg);

            if (hdwheelchair_flagPsg == "") {
                hdwheelchair_flagPsg = 0;
            }

            strPassenger = "<ArrayOfPassenger> " +
                                "<Passenger>" +
                                    "<passenger_id>" + hdPassengerIdPsg + "</passenger_id>" +
                                    "<booking_id>00000000-0000-0000-0000-000000000000</booking_id>" +
                                    "<client_number>" + hdclient_numberPsg + "</client_number>" +
                                    "<passenger_profile_id>" + hdpassenger_profile_idPsg + "</passenger_profile_id>" +
                                    "<passenger_type_rcd>" + PassengerTypePsg + "</passenger_type_rcd>" +
                                    "<lastname>" + last_namePsg.toUpperCase() + "</lastname>" +
                                    "<firstname>" + first_namePsg.toUpperCase() + "</firstname>" +
                                    "<title_rcd>" + TitlePsg.split("|")[0] + "</title_rcd>" +
                                    "<gender_type_rcd>" + TitlePsg.split("|")[1] + "</gender_type_rcd>" +
                                    "<nationality_rcd>" + nationalityPsg + "</nationality_rcd>" +
                                    "<passport_number>" + passportnumberPsg + "</passport_number>" +
                                    "<passport_issue_date>" + ReformatDateXml(passportIssueDatePsg) + "</passport_issue_date>" +
                                    "<passport_expiry_date>" + ReformatDateXml(passportExpiryDatePsg) + "</passport_expiry_date>" +
                                    "<passport_issue_place>" + passportIssuePlacePsg + "</passport_issue_place>" +
                                    "<passport_birth_place>" + passportBirthPlacePsg + "</passport_birth_place>" +
                                    "<document_type_rcd>" + documenttypePsg + "</document_type_rcd>" +
                                    "<date_of_birth>" + ReformatDateXml(dateOfBirthPsg) + "</date_of_birth>" +
                                    "<wheelchair_flag>" + hdwheelchair_flagPsg + "</wheelchair_flag>" +
                                    "<vip_flag>" + hdvip_flagPsg + "</vip_flag>" +
                                    "<create_date_time>0001-01-01T00:00:00</create_date_time>" +
                                    "<update_by>***update_by***</update_by>" +
                                    "<update_date_time>" + datetime.format("yyyy-MM-dd'T'HH:MM:ss") + "</update_date_time>" +
                                    "<passenger_weight>" + hdpassenger_weightPsg + "</passenger_weight>" +
                                    "<client_profile_id>***client_profile_id***</client_profile_id>" +
                                    "<passenger_role_rcd>" + PassengerRolePsg + "</passenger_role_rcd>" +
                                "</Passenger>" +
                            "</ArrayOfPassenger>";
        }

        ShowProgressBar(true);
        if (document.getElementById("dvShowpassenger").style.display == "none") {
            tikAeroB2C.WebService.B2cService.ClientProfileSave(Title,
                                                                gender,
                                                                first_name,
                                                                last_name,
                                                                client_password,
                                                                phoneMobile,
                                                                phoneHome,
                                                                phoneBusiness,
                                                                contactEmail,
                                                                language,
                                                                addressline1,
                                                                addressline2,
                                                                zipCode,
                                                                city,
                                                                country,
                                                                nationality,
                                                                optDocumentType,
                                                                PassengerRole,
                                                                PassengerType,
                                                                passportIssuePlace,
                                                                passportBirthPlace,
                                                                ReformatDate(passportIssueDate),
                                                                ReformatDate(passportExpiryDate),
                                                                ReformatDate(dateOfBirth),
                                                                passportnumber,
                                                                strPassenger,
                                                                url.href,
                                                                mobileEmail,
                                                                state,
                                                                SuccessClientPasswordSave,
                                                                showError,
                                                                showTimeOut);
        }
        else {
            tikAeroB2C.WebService.B2cService.ClientProfileSave(Title,
                                                                gender,
                                                                first_name,
                                                                last_name,
                                                                client_password,
                                                                phoneMobile,
                                                                phoneHome,
                                                                phoneBusiness,
                                                                contactEmail,
                                                                language,
                                                                addressline1,
                                                                addressline2,
                                                                zipCode,
                                                                city,
                                                                country,
                                                                nationality,
                                                                optDocumentType,
                                                                PassengerRole,
                                                                PassengerType,
                                                                passportIssuePlace,
                                                                passportBirthPlace,
                                                                ReformatDate(passportIssueDate),
                                                                ReformatDate(passportExpiryDate),
                                                                ReformatDate(dateOfBirth),
                                                                passportnumber,
                                                                strPassenger,
                                                                url.href,
                                                                mobileEmail,
                                                                state,
                                                                SuccessClientProfileSave,
                                                                showError,
                                                                showTimeOut);
        }
    }
}


function NewPassenger(o) {
    var Status = true;

    Status = ValidateNewPassenger(o);
    if (Status == true) {
        var Title = GetSelectedOption(document.getElementById(o + "ddlPsgTitle"));

        var gender = "M";
        var first_name = GetControlValue(document.getElementById(o + "txtPsgFirstName"));
        var last_name = GetControlValue(document.getElementById(o + "txtPsgLastName"));
        var dateOfBirth = GetControlValue(document.getElementById(o + "TxtPsgDateofBirth"));
        var passportnumber = GetControlValue(document.getElementById(o + "txtPsgDocumentNumber"));
        var nationality = GetSelectedOption(document.getElementById(o + "optNationality"));
        var documenttype = GetSelectedOption(document.getElementById(o + "optDocumentType"));
        var PassengerRole = GetSelectedOption(document.getElementById(o + "ddlPassengerRole"));
        var PassengerType = GetSelectedOption(document.getElementById(o + "ddlPassengerType"));

        var passportIssuePlace = GetControlValue(document.getElementById(o + "txtPsgPlaceOfIssue"));
        var passportBirthPlace = GetControlValue(document.getElementById(o + "txtPsgPlaceOfBirth"));
        var passportIssueDate = GetControlValue(document.getElementById(o + "txtPsgIssueDate"));
        var passportExpiryDate = GetControlValue(document.getElementById(o + "txtPsgExpiryDate"));

        first_name = ReplaceStringSave(first_name);
        last_name = ReplaceStringSave(last_name);

        var url = window.location;
        var success = false;

        tikAeroB2C.WebService.B2cService.PassengerAdd(Title,
                                                    first_name,
                                                    last_name,
                                                    dateOfBirth,
                                                    documenttype,
                                                    passportnumber,
                                                    passportIssuePlace,
                                                    passportBirthPlace,
                                                    passportIssueDate,
                                                    passportExpiryDate,
                                                    nationality,
                                                    PassengerRole,
                                                    PassengerType,
                                                    SuccessPassengerSave,
                                                    showError,
                                                    showTimeOut);
    }
}

function ValidateRegister(o) {
    var Status = true;
    var e = "";
    var fn = trim(GetControlValue(document.getElementById(o + "txtFirstName")));
    var ln = trim(GetControlValue(document.getElementById(o + "txtLastName")));
    var pm = document.getElementById(o + "txtMobilePhone");
    var ph = document.getElementById(o + "txtHomePhone");
    var pb = document.getElementById(o + "txtBusinessPhone");
    var p = trim(GetControlValue(pm)) + trim(GetControlValue(ph)) + trim(GetControlValue(pb));
    var em = trim(GetControlValue(document.getElementById(o + "txtEmail")));
    var strMobileEmail = trim(GetControlValue(document.getElementById(o + "txtMobileEmail")));
    
    var ad1 = trim(GetControlValue(document.getElementById(o + "txtAddress1")));
    var cty = trim(GetControlValue(document.getElementById(o + "txtcity")));
    var pc = trim(GetControlValue(document.getElementById(o + "txtZipCode")));
    var ctr = document.getElementById(o + "ddlCountry");

    var pws = trim(GetControlValue(document.getElementById(o + "txtPassword")));
    var rePws = trim(GetControlValue(document.getElementById(o + "txtReconfirmPassword")));

    var dn = trim(GetControlValue(document.getElementById(o + "txtDocumentNumber")));
    var pi = trim(GetControlValue(document.getElementById(o + "txtPlaceOfIssue")));
    var pb = trim(GetControlValue(document.getElementById(o + "txtPlaceOfBirth")));
    var id = trim(GetControlValue(document.getElementById(o + "txtIssueDate")));
    var ed = trim(GetControlValue(document.getElementById(o + "txtExpiryDate")));
    var dateOfBirth = GetControlValue(document.getElementById(o + "txtDateofBirth"));

    //First Name
    if (fn.length == 0 || IsChar(fn) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_90, "Please supply a valid First Name") + "</li>";
        document.getElementById(o + "txtFirstName").className = "error";
    }
    else {
        document.getElementById(o + "txtFirstName").className = "";
    }

    //Surname
    if (ln.length == 0 || IsChar(ln) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_91, "Please supply a valid Surname") + "</li>";
        document.getElementById(o + "txtLastName").className = "error";
    }
    else {
        document.getElementById(o + "txtLastName").className = "";
    }

    //Telephone Number
    if (p.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_92, "Please supply a valid Telephone Number") + "</li>";
        pm.className = "error";
        ph.className = "error";
        pb.className = "error";
    }
    else {
        pm.className = "";
        ph.className = "";
        pb.className = "";
    }

    //Email
    if (em.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_93, "Please supply a valid Email") + "</li>";
        document.getElementById(o + "txtEmail").className = "error";
    }
    else if (ValidEmail(document.getElementById(o + "txtEmail").value) != true) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_94, "Please supply a valid Email format") + "</li>";
        document.getElementById(o + "txtEmail").className = "error";
    }
    else {
        document.getElementById(o + "txtEmail").className = "";
    }

    //Mobile email
    if (strMobileEmail.length > 0) {
        if (ValidEmail(strMobileEmail) != true) {
            Status = false;
            e += "<li>" + DisplayMessage(objLanguage.Alert_Message_94, "Please supply a valid Email format") + "</li>";
            document.getElementById(o + "txtMobileEmail").className = "error";
        }
    }
    
    //Address 1
    if (ad1.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_95, "Please supply a valid Address 1") + "</li>";
        document.getElementById(o + "txtAddress1").className = "error";
    }
    else {
        document.getElementById(o + "txtAddress1").className = "";
    }

    //Town/City
    if (cty.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_96, "Please supply a valid Town/City") + "</li>";
        document.getElementById(o + "txtcity").className = "error";
    }
    else {
        document.getElementById(o + "txtcity").className = "";
    }

    //Postal Code
    if (pc.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_97, "Please supply a valid ZipCode") + "</li>";
        document.getElementById(o + "txtZipCode").className = "error";
    }
    else {
        document.getElementById(o + "txtZipCode").className = "";
    }

    //Pasword
    if (pws.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
    }

    //Password
    if (rePws.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_99, "Please supply a valid Reconfirm Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
    }

    if (pws != rePws) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
        document.getElementById(o + "txtReconfirmPassword").className = "";
    }

    if (document.getElementById(o + "cboConfirm").checked == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_100, "Please accept Terms and Conditions") + "</li>";
    }

    //Document Section.
    if (dn.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_101, "Please supply a valid Document") + "</li>";
        document.getElementById(o + "txtDocumentNumber").className = "error";
    }
    else {
        document.getElementById(o + "txtDocumentNumber").className = "";
    }

    if (pi.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_102, "Please supply a valid PlaceOfIssue") + "</li>";
        document.getElementById(o + "txtPlaceOfIssue").className = "error";
    }
    else {
        document.getElementById(o + "txtPlaceOfIssue").className = "";
    }

    if (pb.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_103, "Please supply a valid PlaceOfBirth") + "</li>";
        document.getElementById(o + "txtPlaceOfBirth").className = "error";
    }
    else {
        document.getElementById(o + "txtPlaceOfBirth").className = "";
    }

    if (id.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_104, "Please supply a valid IssueDate") + "</li>";
        document.getElementById(o + "txtIssueDate").className = "error";
    }
    else if (id == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_104, "Please supply a valid IssueDate") + "</li>";
        document.getElementById(o + "txtIssueDate").className = "error";
    }
    else {
        document.getElementById(o + "txtIssueDate").className = "";
    }

    if (ed.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid ExpiryDate") + "</li>";
        document.getElementById(o + "txtExpiryDate").className = "error";
    }
    else if (ed == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid ExpiryDate") + "</li>";
        document.getElementById(o + "txtExpiryDate").className = "error";
    }
    else {
        document.getElementById(o + "txtExpiryDate").className = "";
    }


    if (dateOfBirth.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "txtDateofBirth").className = "error";
    }
    else if (dateOfBirth == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "txtDateofBirth").className = "error";
    }
    else {
        document.getElementById(o + "txtDateofBirth").className = "";
    }

    var PassengerType = GetSelectedOption(document.getElementById("ddlPassengerType"));
    if (IsPastDate(id) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_107, "Issue Date Should be in the past") + "</li>";
        document.getElementById(o + "txtIssueDate").className = "error";
    }

    if (IsFutureDate(ed) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_108, "Document is expired") + "</li>";
        document.getElementById(o + "txtExpiryDate").className = "error";
    }
    if (IsPastDate(dateOfBirth) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_109, "Birth Date Should be in the past") + "</li>";
        document.getElementById(o + "txtDateofBirth").className = "error";
    }

    if (DateValidate(id) == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_104, "Please supply a valid IssueDate") + "</li>";
        document.getElementById(o + "txtIssueDate").className = "error";
    }
    if (DateValidate(ed) == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid ExpiryDate") + "</li>";
        document.getElementById(o + "txtExpiryDate").className = "error";
    }
    if (DateValidate(dateOfBirth) == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "txtDateofBirth").className = "error";
    }

    var todayDate = new Date();
    var chDate = validateChdInfBirthDate(todayDate.getDate() + "/" + (todayDate.getMonth() + 1) + "/" + todayDate.getYear(), dateOfBirth, PassengerType);
    if (chDate.length > 0) {
        Status = false;
        e += "<li>" + chDate + "</li>";
        document.getElementById(o + "txtDateofBirth").className = "error";
    }

    if (e.length > 0) {
        Container = document.getElementById(o + "LabError");
        Container.innerHTML = "<ul>" + e + "</ul>";
        return Status;
    }
    else {
        return Status;
    }
}
function ValidateRegister_Edit(o) {
    var Status = true;
    var e = "";

    var fn = trim(GetControlValue(document.getElementById(o + "txtFirstName")));
    var ln = trim(GetControlValue(document.getElementById(o + "txtLastName")));
    var pm = document.getElementById(o + "txtMobilePhone");
    var ph = document.getElementById(o + "txtHomePhone");
    var pb = document.getElementById(o + "txtBusinessPhone");

    var p = trim(GetControlValue(pm)) + trim(GetControlValue(ph)) + trim(GetControlValue(pb));

    var em = trim(GetControlValue(document.getElementById(o + "txtEmail")));
    var strMobileEmail = trim(GetControlValue(document.getElementById(o + "txtMobileEmail")));
    var ad1 = trim(GetControlValue(document.getElementById(o + "txtAddress1")));
    var cty = trim(GetControlValue(document.getElementById(o + "txtcity")));

    var pc = trim(GetControlValue(document.getElementById(o + "txtZipCode")));
    var ctr = document.getElementById(o + "ddlCountry");
    var pws = trim(GetControlValue(document.getElementById(o + "txtPassword")));
    var rePws = trim(GetControlValue(document.getElementById(o + "txtReconfirmPassword")));

    //First Name
    if (fn.length == 0 || IsChar(fn) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_90, "Please supply a valid First Name") + "</li>";
        document.getElementById(o + "txtFirstName").className = "error";
    }
    else {
        document.getElementById(o + "txtFirstName").className = "";
    }

    //Surname
    if (ln.length == 0 || IsChar(ln) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_91, "Please supply a valid Surname") + "</li>";
        document.getElementById(o + "txtLastName").className = "error";
    } else {
        document.getElementById(o + "txtLastName").className = "";
    }

    //Telephone Number
    if (p == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_92, "Please supply a valid Telephone Number") + "</li>";
        pm.className = "error";
        ph.className = "error";
        pb.className = "error";
    }
    else {
        pm.className = "";
        ph.className = "";
        pb.className = "";
    }

    //Email
    if (em.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_93, "Please supply a valid Email") + "</li>";
        document.getElementById(o + "txtEmail").className = "error";
    }
    else if (ValidEmail(document.getElementById(o + "txtEmail").value) != true) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_94, "Please supply a valid Email format") + "</li>";
        document.getElementById(o + "txtEmail").className = "error";
    }
    else {
        document.getElementById(o + "txtEmail").className = "";
    }

    //Mobile email
    if (strMobileEmail.length > 0) {

        if (ValidEmail(strMobileEmail) != true) {
            Status = false;
            e += "<li>" + DisplayMessage(objLanguage.Alert_Message_94, "Please supply a valid Email format") + "</li>";
            document.getElementById(o + "txtMobileEmail").className = "error";
        }
    }

    //Address 1
    if (ad1.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_95, "Please supply a valid Address 1") + "</li>";
        document.getElementById(o + "txtAddress1").className = "error";
    }
    else {
        document.getElementById(o + "txtAddress1").className = "";
    }

    //Town/City
    if (cty.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_96, "Please supply a valid Town/City") + "</li>";
        document.getElementById(o + "txtcity").className = "error";
    }
    else {
        document.getElementById(o + "txtcity").className = "";
    }

    //County
    /*
    if (dt==0){
    Status =  false;
    e += "<li>Please supply a valid County</li>"
    document.getElementById(o+'_ctlPassengerContactInfo_district').className = 'error';
    }else{
    document.getElementById(o+'_ctlPassengerContactInfo_district').className = '';
    }*/

    //Postal Code
    if (pc.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_97, "Please supply a valid ZipCode") + "</li>";
        document.getElementById(o + "txtZipCode").className = "error";
    } else {
        document.getElementById(o + "txtZipCode").className = "";
    }

    //Pasword
    if (pws.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
        document.getElementById(o + "txtReconfirmPassword").className = "";
    }
    if (rePws.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
        document.getElementById(o + "txtReconfirmPassword").className = "";
    }
    if (pws != rePws) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
        document.getElementById(o + "txtReconfirmPassword").className = "";
    }

    if (e.length > 0) {
        var Container = document.getElementById(o + "LabError");
        Container.innerHTML = "<ul>" + e + "</ul>";

        return Status;
    }
    else {
        return Status;
    }
}
function ValidatePassenger(o) {
    var Status = true;
    var e = "";

    var tt = GetSelectedOption(document.getElementById("ddlPsgTitle" + "_" + o));
    var pr = GetSelectedOption(document.getElementById("ddlPassengerRole" + "_" + o));
    var fn = trim(GetControlValue(document.getElementById("txtPsgFirstName" + "_" + o)));
    var ln = trim(GetControlValue(document.getElementById("txtPsgLastName" + "_" + o)));

    var dn = trim(GetControlValue(document.getElementById("txtPsgDocumentNumber" + "_" + o)));
    var pi = trim(GetControlValue(document.getElementById("txtPsgPlaceOfIssue" + "_" + o)));
    var pb = trim(GetControlValue(document.getElementById("txtPsgPlaceOfBirth" + "_" + o)));
    var id = trim(GetControlValue(document.getElementById("txtPsgIssueDate" + "_" + o)));
    var ed = trim(GetControlValue(document.getElementById("txtPsgExpiryDate" + "_" + o)));
    var dateOfBirth = GetControlValue(document.getElementById("TxtPsgDateofBirth" + "_" + o));

    //--->  Title
    if (tt.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_136, "Please supply a valid Title") + "</li>";
        document.getElementById("ddlPsgTitle" + "_" + o).className = 'error';
    }
    else {
        document.getElementById("ddlPsgTitle" + "_" + o).className = '';
    }

    //--->  role
    if (pr.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_137, "Please supply a valid role") + "</li>";
        document.getElementById("ddlPassengerRole" + "_" + o).className = 'error';
    }
    else {
        document.getElementById("ddlPassengerRole" + "_" + o).className = '';
    }

    //First Name
    if (fn == 0 || IsChar(fn) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_90, "Please supply a valid First Name") + "</li>";
        document.getElementById("txtPsgFirstName" + "_" + o).className = "error";
    }
    else {
        document.getElementById("txtPsgFirstName" + "_" + o).className = "";
    }

    //Surname
    if (ln == 0 || IsChar(ln) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_91, "Please supply a valid Surname") + "</li>";
        document.getElementById("txtPsgLastName" + "_" + o).className = "error";
    }
    else {
        document.getElementById("txtPsgLastName" + "_" + o).className = "";
    }

    //Document 
    if (dn == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_101, "Please supply a valid DocumentNumber") + "</li>";
        document.getElementById("txtPsgDocumentNumber" + "_" + o).className = "error";
    }
    else {
        document.getElementById("txtPsgDocumentNumber" + "_" + o).className = "";
    }
    if (pi == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_102, "Please supply a valid Place Of Issue") + "</li>";
        document.getElementById("txtPsgPlaceOfIssue" + "_" + o).className = "error";
    }
    else {
        document.getElementById("txtPsgPlaceOfIssue" + "_" + o).className = "";
    }
    if (pb == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_103, "Please supply a valid Place Of Birth") + "</li>";
        document.getElementById("txtPsgPlaceOfBirth" + "_" + o).className = "error";
    }
    else {
        document.getElementById("txtPsgPlaceOfBirth" + "_" + o).className = "";
    }
    if (id == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_104, "Please supply a valid Issue Date") + "</li>";
        document.getElementById("txtPsgIssueDate" + "_" + o).className = "error";
    }
    else if (id == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_104, "Please supply a valid IssueDate") + "</li>";
        document.getElementById("txtPsgIssueDate" + "_" + o).className = "error";
    }
    else {
        document.getElementById("txtPsgIssueDate" + "_" + o).className = "";
    }
    if (ed == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid Expiry Date") + "</li>";
        document.getElementById("txtPsgExpiryDate" + "_" + o).className = "error";
    }
    else if (ed == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid ExpiryDate") + "</li>";
        document.getElementById("txtPsgExpiryDate" + "_" + o).className = "error";
    }
    else {
        document.getElementById("txtPsgExpiryDate" + "_" + o).className = "";
    }

    if (dateOfBirth == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid Date of Birth") + "</li>";
        document.getElementById("TxtPsgDateofBirth" + "_" + o).className = "error";
    }
    else if (dateOfBirth == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById("TxtPsgDateofBirth" + "_" + o).className = "error";
    }
    else {
        document.getElementById("TxtPsgDateofBirth" + "_" + o).className = "";
    }

    if (IsPastDate(id) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_110, "Issue Date Should be in the past for passenger").replace("{NO}", padZeros(o, 3)) + "<br/>";
        document.getElementById("txtPsgIssueDate" + "_" + o).className = "error";
    }
    if (IsFutureDate(ed) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_111, "Document is expired for passenger").replace("{NO}", padZeros(o, 3)) + ".<br/>";
        document.getElementById("txtPsgExpiryDate" + "_" + o).className = "error";
    }
    if (IsPastDate(dateOfBirth) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_112, "Birth Date Should be in the past for passenger").replace("{NO}", padZeros(o, 3)) + "<br/>";
        document.getElementById('TxtPsgDateofBirth' + '_' + o).className = 'error';
    }
    if (DateValidate(id) == 0) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_113, "Please supply a valid IssueDate for passenger").replace("{NO}", padZeros(o, 3)) + "<br/>";
        document.getElementById("txtPsgIssueDate" + "_" + o).className = "error";
    }
    if (DateValidate(ed) == 0) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_114, "Please supply a valid ExpiryDate for passenger").replace("{NO}", padZeros(o, 3)) + "<br/>";
        document.getElementById("txtPsgExpiryDate" + "_" + o).className = "error";
    }
    if (DateValidate(dateOfBirth) == 0) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_115, "Please supply a valid DateofBirth for passenger").replace("{NO}", padZeros(o, 3)) + "<br/>";
        document.getElementById("TxtPsgDateofBirth" + "_" + o).className = "error";
    }
    if (e.length > 0) {
        Container = document.getElementById("txtPsgError_" + o);
        Container.innerHTML = "<ul>" + e + "</ul>";
        return Status;
    }
    else {
        return Status;
    }
}
function ValidateNewPassenger(o) {
    var Status = true;
    var e = "";

    var fn = trim(GetControlValue(document.getElementById(o + "txtPsgFirstName")));
    var ln = trim(GetControlValue(document.getElementById(o + "txtPsgLastName")));
    var ctr = document.getElementById(o + "ddlCountry");
    var dn = trim(GetControlValue(document.getElementById(o + "txtPsgDocumentNumber")));
    var pi = trim(GetControlValue(document.getElementById(o + "txtPsgPlaceOfIssue")));
    var pb = trim(GetControlValue(document.getElementById(o + "txtPsgPlaceOfBirth")));
    var id = trim(GetControlValue(document.getElementById(o + "txtPsgIssueDate")));
    var ed = trim(GetControlValue(document.getElementById(o + "txtPsgExpiryDate")));
    var dateOfBirth = GetControlValue(document.getElementById(o + "TxtPsgDateofBirth"));

    //--->  Title
    /*if (trim(tt.options[tt.selectedIndex].value)==0){
    Status =  false;
    e += "<li>Please supply a valid Title</li>"
    document.getElementById(o+'_ctlPassengerContactInfo_cmbTitelsRcd').className = 'error';
    }else{
    document.getElementById(o+'_ctlPassengerContactInfo_cmbTitelsRcd').className = '';
    }*/

    //First Name
    if (fn == 0 || IsChar(fn) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_90, "Please supply a valid First Name") + "</li>";
        document.getElementById(o + "txtPsgFirstName").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgFirstName").className = "";
    }

    //Surname
    if (ln == 0 || IsChar(ln) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_91, "Please supply a valid Surname.") + "</li>";
        document.getElementById(o + "txtPsgLastName").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgLastName").className = "";
    }

    //Document 
    if (dn == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_101, "Please supply a valid DocumentNumber") + "</li>";
        document.getElementById(o + "txtPsgDocumentNumber").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgDocumentNumber").className = "";
    }
    if (pi == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_102, "Please supply a valid PlaceOfIssue") + "</li>";
        document.getElementById(o + "txtPsgPlaceOfIssue").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgPlaceOfIssue").className = "";
    }

    if (pb == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_103, "Please supply a valid PlaceOfBirth") + "</li>";
        document.getElementById(o + "txtPsgPlaceOfBirth").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgPlaceOfBirth").className = "";
    }
    if (id == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_104, "Please supply a valid IssueDate") + "</li>";
        document.getElementById(o + "txtPsgIssueDate").className = "error";
    }
    else if (id == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_104, "Please supply a valid IssueDate") + "</li>";
        document.getElementById(o + "txtPsgIssueDate").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgIssueDate").className = "";
    }
    if (ed == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid ExpiryDate") + "</li>";
        document.getElementById(o + "txtPsgExpiryDate").className = "error";
    }
    else if (ed == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid ExpiryDate") + "</li>";
        document.getElementById(o + "txtPsgExpiryDate").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgExpiryDate").className = "";
    }

    if (dateOfBirth == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "TxtPsgDateofBirth").className = "error";
    }
    else if (dateOfBirth == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "TxtPsgDateofBirth").className = "error";
    }
    else {
        document.getElementById(o + "TxtPsgDateofBirth").className = "";
    }

    if (IsPastDate(id) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_107, "Issue Date Should be in the past") + "<br/>";
        document.getElementById(o + "txtPsgIssueDate").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgIssueDate").className = "";
    }

    if (IsFutureDate(ed) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_108, "Document is expired") + "<br/>";
        document.getElementById(o + "txtPsgExpiryDate").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgExpiryDate").className = "";
    }
    if (IsPastDate(dateOfBirth) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_109, "Birth Date Should be in the past") + "<br/>";
        document.getElementById(o + "TxtPsgDateofBirth").className = "error";
    }
    else {
        document.getElementById(o + "TxtPsgDateofBirth").className = "";
    }
    if (DateValidate(id) == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_104, "Please supply a valid IssueDate") + "</li>";
        document.getElementById(o + "txtPsgIssueDate").className = "error";
    }
    if (DateValidate(ed) == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid ExpiryDate") + "</li>";
        document.getElementById(o + "txtPsgExpiryDate").className = "error";
    }
    if (DateValidate(dateOfBirth) == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "TxtPsgDateofBirth").className = "error";
    }
    if (e.length > 0) {
        Container = document.getElementById(o + 'ErrorNewPassenger');
        Container.innerHTML = "<ul>" + e + "</ul>";

        return Status;
    }
    else {
        return Status;
    }
}

function InitializeDateFormat(o) {
    InitializeDateMaskEdit(o + "txtDateofBirth");
    InitializeDateMaskEdit(o + "txtIssueDate");
    InitializeDateMaskEdit(o + "txtExpiryDate");
}
function InitializeDateFormatPassenger() {
    var iPaxcount = document.getElementsByName("hdPassengerId").length;
    for (var iCount = 1; iCount <= iPaxcount; iCount++) {
        InitializeDateMaskEdit("TxtPsgDateofBirth" + "_" + iCount);
        InitializeDateMaskEdit("txtPsgIssueDate" + "_" + iCount);
        InitializeDateMaskEdit("txtPsgExpiryDate" + "_" + iCount);
    }
}
function InitializeDateFormatPassengerAdd(o) {
    InitializeDateMaskEdit(o + "TxtPsgDateofBirth");
    InitializeDateMaskEdit(o + "txtPsgIssueDate");
    InitializeDateMaskEdit(o + "txtPsgExpiryDate");

}
function ControlBtt() {

    var objdv = document.getElementById("dvNewPassenger");
    if (objdv != null) {
        if (objdv.style.display == "none") {
            var objFirstName = document.getElementById("ctl00_txtPsgLastName");
            objdv.style.display = "block";

            objFirstName.focus();

            //Watermark at date of birth
            InitializeWaterMark("ctl00_TxtPsgDateofBirth", objLanguage.default_value_1, "input");

            var pos = findPos(objFirstName);
            window.scrollTo(0, parseInt(pos[1]) - 150);

            //Set passenger number.
            var objPaxId = document.getElementsByName("hdPassengerId");
            var spnPaxNo = document.getElementById("spnPaxNo")
            if (spnPaxNo != null) {
                if (objPaxId != null) {
                    spnPaxNo.innerHTML = padZeros(objPaxId.length + 1, 3);
                }
                else {
                    spnPaxNo.innerHTML = "001";
                }
            }
        }
        else {
            objdv.style.display = "none";
        }
    }
}

function enableField(o) {


    if (document.getElementById("dvShowpassenger").style.display == "none") {
        document.getElementById(o + "txtReconfirmPassword").disabled = false;
        document.getElementById(o + "txtPassword").disabled = false;
    }
    else {
        document.getElementById(o + "ddlTitle").disabled = false;
        document.getElementById(o + "txtFirstName").disabled = false;
        document.getElementById(o + "txtLastName").disabled = false;
        document.getElementById(o + "txtReconfirmPassword").disabled = false;
        document.getElementById(o + "txtPassword").disabled = false;
        document.getElementById(o + "txtMobilePhone").disabled = false;
        document.getElementById(o + "txtHomePhone").disabled = false;
        document.getElementById(o + "txtBusinessPhone").disabled = false;
        document.getElementById(o + "txtEmail").disabled = false;
        document.getElementById(o + "txtMobileEmail").disabled = false;
        
        document.getElementById(o + "optLanguage").disabled = false;
        document.getElementById(o + "txtAddress1").disabled = false;
        document.getElementById(o + "txtAddressline2").disabled = false;
        document.getElementById(o + "txtZipCode").disabled = false;
        document.getElementById(o + "txtcity").disabled = false;
        document.getElementById(o + "txtState").disabled = false;
        document.getElementById(o + "ddlCountry").disabled = false;
    }
}
function ChangePassword(o) {

    //show password information
    ShowChangePassword(true);
    document.getElementById(o + "txtReconfirmPassword").disabled = false;
    document.getElementById(o + "txtPassword").disabled = false;
}
function disabledField(o) {

    document.getElementById(o + "ddlTitle").disabled = true;
    document.getElementById(o + "txtFirstName").disabled = true;
    document.getElementById(o + "txtLastName").disabled = true;
    document.getElementById(o + "txtReconfirmPassword").disabled = true;
    document.getElementById(o + "txtPassword").disabled = true;
    document.getElementById(o + "txtMobilePhone").disabled = true;
    document.getElementById(o + "txtHomePhone").disabled = true;
    document.getElementById(o + "txtBusinessPhone").disabled = true;
    document.getElementById(o + "txtEmail").disabled = true;
    document.getElementById(o + "txtMobileEmail").disabled = true;
    document.getElementById(o + "optLanguage").disabled = true;
    document.getElementById(o + "txtAddress1").disabled = true;
    document.getElementById(o + "txtAddressline2").disabled = true;
    document.getElementById(o + "txtZipCode").disabled = true;
    document.getElementById(o + "txtcity").disabled = true;
    document.getElementById(o + "txtState").disabled = true;
    document.getElementById(o + "ddlCountry").disabled = true;
    var iPaxcount = document.getElementsByName("hdPassengerId").length;

    for (var iCount = 1; iCount <= iPaxcount; iCount++) {
        document.getElementById("txtPsgFirstName" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgLastName" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgDocumentNumber" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgPlaceOfIssue" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgPlaceOfBirth" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgIssueDate" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgExpiryDate" + "_" + iCount).disabled = true;
        document.getElementById("TxtPsgDateofBirth" + "_" + iCount).disabled = true;
        document.getElementById("ddlPsgTitle" + "_" + iCount).disabled = true;
        document.getElementById("optNationality" + "_" + iCount).disabled = true;
        document.getElementById("optDocumentType" + "_" + iCount).disabled = true;
        document.getElementById("ddlPassengerRole" + "_" + iCount).disabled = true;
        document.getElementById("ddlPassengerType" + "_" + iCount).disabled = true;

    }
}
function enableFieldPassenger(o) {

    if ($("#ulPassenger_" + o + " img").attr("src") == "App_Themes/Default/Images/expand.gif") {
        $("#ulPassenger_" + o).find("a").click();
    }

    document.getElementById("txtPsgFirstName" + "_" + o).disabled = false;
    document.getElementById("txtPsgLastName" + "_" + o).disabled = false;

    document.getElementById("txtPsgDocumentNumber" + "_" + o).disabled = false;
    document.getElementById("txtPsgPlaceOfIssue" + "_" + o).disabled = false;
    document.getElementById("txtPsgPlaceOfBirth" + "_" + o).disabled = false;
    document.getElementById("txtPsgIssueDate" + "_" + o).disabled = false;
    document.getElementById("txtPsgExpiryDate" + "_" + o).disabled = false;
    document.getElementById("TxtPsgDateofBirth" + "_" + o).disabled = false;
    document.getElementById("ddlPsgTitle" + "_" + o).disabled = false;
    document.getElementById("optNationality" + "_" + o).disabled = false;
    document.getElementById("optDocumentType" + "_" + o).disabled = false;
    document.getElementById("ddlPassengerType" + "_" + o).disabled = false;
    if (o != 1) {
        document.getElementById("ddlPassengerRole" + "_" + o).disabled = false;
    }
}
function disabledFieldPassenger(o) {

    document.getElementById("txtPsgFirstName" + "_" + o).disabled = true;
    document.getElementById("txtPsgLastName" + "_" + o).disabled = true;

    document.getElementById("txtPsgDocumentNumber" + "_" + o).disabled = true;
    document.getElementById("txtPsgPlaceOfIssue" + "_" + o).disabled = true;
    document.getElementById("txtPsgPlaceOfBirth" + "_" + o).disabled = true;
    document.getElementById("txtPsgIssueDate" + "_" + o).disabled = true;
    document.getElementById("txtPsgExpiryDate" + "_" + o).disabled = true;
    document.getElementById("TxtPsgDateofBirth" + "_" + o).disabled = true;
    document.getElementById("ddlPsgTitle" + "_" + o).disabled = true;
    document.getElementById("optNationality" + "_" + o).disabled = true;
    document.getElementById("optDocumentType" + "_" + o).disabled = true;
    document.getElementById("ddlPassengerRole" + "_" + o).disabled = true;
    document.getElementById("ddlPassengerType" + "_" + o).disabled = true;
}

function DateValidate(value) {
    var bResult = true;
    if (value == "") return bResult;
    var RegExPattern = /^(?=\d)(?:(?!(?:(?:0?[5-9]|1[0-4])(?:\.|-|\/)10(?:\.|-|\/)(?:1582))|(?:(?:0?[3-9]|1[0-3])(?:\.|-|\/)0?9(?:\.|-|\/)(?:1752)))(31(?!(?:\.|-|\/)(?:0?[2469]|11))|30(?!(?:\.|-|\/)0?2)|(?:29(?:(?!(?:\.|-|\/)0?2(?:\.|-|\/))|(?=\D0?2\D(?:(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|2[0-8]|1\d|0?[1-9])([-.\/])(1[012]|(?:0?[1-9]))\2((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?:$|(?=\x20\d)\x20)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$/;
    if (!RegExPattern.test(value)) {
        bResult = false;
        return bResult;
    }
    else {
        bResult = true;
        return bResult;
    }
}

function IfAlphaOnly(s) {
    var i
    var b = false
    var c = "";
    if (s.length != 0) {
        for (i = 0; i < s.length; i++) {
            c = s.charCodeAt(i);
            if ((c > 47 && c < 58)) {
                return false;
            }
            else if (((c > 96 && c < 123) || (c > 64 && c < 91)) == false) {
                return false;
            }
        }
        b = true
    }
    else {
        b = false;
    }

    return b
}

function CheckCharacter() {
    if ((String.fromCharCode(event.keyCode) >= "a" &&
    String.fromCharCode(event.keyCode) <= "z") || (String.fromCharCode(event.keyCode) >= "A" &&
    String.fromCharCode(event.keyCode) <= "Z")) {
        return true;
    }
    else {
        if (String.fromCharCode(event.keyCode) == " ") {
            return true;
        }
        else if (String.fromCharCode(event.keyCode) == ".") {
            return true;
        }
        else if (String.fromCharCode(event.keyCode) == "-") {
            return true;
        }
        else if (String.fromCharCode(event.keyCode) == " ") {
            return true;
        }
        else if (String.fromCharCode(event.keyCode) == "'") {
            return true;
        }
        else if (String.fromCharCode(event.keyCode) == "_") {
            return true;
        }
        else {
            return false;
        }
    }
}

function InitializeRegistrationWaterMark(o) {

    //Passenger profile information.
    InitializeWaterMark(o + "ddlTitle", objLanguage.default_value_2, "select");
    InitializeWaterMark("ddlPassengerType", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "txtLastName", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtFirstName", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtDateofBirth", objLanguage.default_value_1, "input");
    InitializeWaterMark(o + "optNationality", objLanguage.default_value_2, "select");

    //Document information.
    InitializeWaterMark(o + "optDocumentType", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "txtDocumentNumber", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtPlaceOfIssue", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtPlaceOfBirth", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtIssueDate", objLanguage.default_value_1, "input");
    InitializeWaterMark(o + "txtExpiryDate", objLanguage.default_value_1, "input");

    //Contact detail.

    InitializeWaterMark(o + "txtMobilePhone", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtHomePhone", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "txtBusinessPhone", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "txtEmail", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtMobileEmail", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "optLanguage", objLanguage.default_value_2, "select");

    InitializeWaterMark(o + "txtAddress1", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtAddressline2", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "txtZipCode", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtcity", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtState", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "ddlCountry", objLanguage.default_value_2, "select");

    InitializeWaterMark(o + "txtPassword", objLanguage.default_value_1, "password");
    InitializeWaterMark(o + "txtReconfirmPassword", objLanguage.default_value_1, "password");

    //Passenger profile
    var objIds = document.getElementsByName("hdPassengerId");
    for (var i = 1; i <= objIds.length; i++) {

        InitializeWaterMark("ddlPsgTitle_" + i, objLanguage.default_value_2, "select");
        InitializeWaterMark("ddlPassengerType_" + i, objLanguage.default_value_2, "select");
        InitializeWaterMark("ddlPassengerRole_" + i, objLanguage.default_value_2, "select");

        InitializeWaterMark("txtPsgFirstName_" + i, objLanguage.default_value_2, "input");
        InitializeWaterMark("txtPsgLastName_" + i, objLanguage.default_value_2, "input");

        InitializeWaterMark("optNationality_" + i, objLanguage.default_value_2, "select");
        InitializeWaterMark("TxtPsgDateofBirth_" + i, objLanguage.default_value_1, "input");
        InitializeWaterMark("txtPsgDocumentNumber_" + i, objLanguage.default_value_2, "input");

        InitializeWaterMark("optDocumentType_" + i, objLanguage.default_value_2, "select");
        InitializeWaterMark("txtPsgPlaceOfBirth_" + i, objLanguage.default_value_2, "input");
        InitializeWaterMark("txtPsgPlaceOfIssue_" + i, objLanguage.default_value_2, "input");

        InitializeWaterMark("txtPsgExpiryDate_" + i, objLanguage.default_value_1, "input");
        InitializeWaterMark("txtPsgIssueDate_" + i, objLanguage.default_value_1, "input");

    }
}

function ShowChangePassword(bValue) {
    var objPasswordSec = document.getElementById("dvRegPassword");
    var objClientInfo = document.getElementById("ClientProfileInfo");
    if (bValue == true) {

        if (objClientInfo != null) {
            objClientInfo.style.display = "none";
        }
        if (objPasswordSec != null) {
            objPasswordSec.style.display = "block";
        }

     }
    else {
        if (objClientInfo != null) {
            objClientInfo.style.display = "block";
        }
        if (objPasswordSec != null) {
            objPasswordSec.style.display = "none";
        }
    }

}
function InitializePassengerAddWaterMark(o) {
    //Passenger profile
    InitializeWaterMark(o + "ddlPsgTitle", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "ddlPassengerType", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "ddlPassengerRole", objLanguage.default_value_2, "select");

    InitializeWaterMark(o + "txtPsgFirstName", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtPsgLastName", objLanguage.default_value_2, "input");

    InitializeWaterMark(o + "optNationality", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "TxtPsgDateofBirth", objLanguage.default_value_1, "input");
    InitializeWaterMark(o + "txtPsgDocumentNumber", objLanguage.default_value_2, "input");

    InitializeWaterMark(o + "optDocumentType", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "txtPsgPlaceOfBirth", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtPsgPlaceOfIssue", objLanguage.default_value_2, "input");

    InitializeWaterMark(o + "txtPsgExpiryDate", objLanguage.default_value_1, "input");
    InitializeWaterMark(o + "txtPsgIssueDate", objLanguage.default_value_1, "input");
}