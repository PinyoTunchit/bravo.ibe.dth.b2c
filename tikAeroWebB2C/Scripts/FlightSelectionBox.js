﻿function getDestination() {
    var strCtrl = FindControlName("select", "optDestination");
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var objDestination = document.getElementById(strCtrl);
    var strSelectedOrigin = "";

    objDestination.className = "";
    //Add destination from selected origin
    strSelectedOrigin = objOrigin.options[objOrigin.selectedIndex].value;
    objDestination = ClearOptions(objDestination);  //Clear previous value

    //Add watermark option
    var optDefault = document.createElement("option");
    optDefault.value = "";
    optDefault.className = "watermarkOn";
    if (navigator.appName.indexOf('Microsoft') == 0)
    { optDefault.innerText = objLanguage.default_value_7; }
    else
    { optDefault.text = objLanguage.default_value_7; }
    objDestination.appendChild(optDefault);

    //Filled in route information.
    for (var iCount = 0; iCount < arrDestination.length; iCount++) {
        if (arrDestination[iCount].split("|")[2] == strSelectedOrigin.split("|")[0]) {
            //Allowed only the destination that have the same
            //origin as selected origin.
            var opt = document.createElement("option");
            opt.value = arrDestination[iCount].split("|")[2] + "|" +
                        objOrigin.options[objOrigin.selectedIndex].text + "|" +
                        arrDestination[iCount].split("|")[0] + "|" +
                        arrDestination[iCount].split("|")[1] + "|" +
                        arrDestination[iCount].split("|")[3] + "|" +
                        arrDestination[iCount].split("|")[4];

            //Add display text to option
            if (navigator.appName.indexOf('Microsoft') == 0) {
                opt.innerText = arrDestination[iCount].split("|")[1];
            }
            else
            { opt.text = arrDestination[iCount].split("|")[1]; }

            objDestination.appendChild(opt);
            opt = null;
        }
    }
    $("#" + strCtrl).unbind();
    InitializeWaterMark(strCtrl, objLanguage.default_value_7, "select");

    objOrigin = null;
    objDestination = null;
}
function SearchAvailability(origin, destination) {

    if (MultipleTab.IsCorrectTab()) {

        //Set Insurance popup
        SetShowInsurance("0");

        var objOneWay = document.getElementById("optOneWay");
        var objAdult = document.getElementById("optAdult");
        var objChild = document.getElementById("optChild");
        var objInfant = document.getElementById("optInfant");
        var objBoardingClass = document.getElementById("optBoardingClass");
        var objPromoCode = document.getElementById("txtPromoCode");
        var objYP = document.getElementById("optYP");
        var objSearchTypeDate = document.getElementById("rdSearchTypeDate");

        var isOneWay = objOneWay.checked.toString();
        var iAdult = parseInt(objAdult.options[objAdult.selectedIndex].value);
        var iChild = parseInt(objChild.options[objChild.selectedIndex].value);
        var iInfant = parseInt(objInfant.options[objInfant.selectedIndex].value);
        var strBoardingClass = objBoardingClass.options[objBoardingClass.selectedIndex].value;
        var dtDateFrom = "";
        var dtDateTo = "";
        var strPromoCode = "";
        var iOther = 0;
        var otherType = "";
        var currencyCode = "";
        var strSearchType = "FARE";

        // End Check Ip address for Default Currency 
        if (objYP != null) {
            iOther = parseInt(objYP.options[objYP.selectedIndex].value);
            otherType = "YP";
        }

        if (objPromoCode != null) {
            strPromoCode = objPromoCode.value;
        }

        //get currency from parameter
        if (existEachQueryString("currency")) {
            currencyCode = queryStringValue("currency");
        }

        //Call Availability web service
        if (iInfant > iAdult) {
            ShowMessageBox(objLanguage.Alert_Message_2, 0, '');
        }
        else if ((iAdult + iChild + iInfant) > 9) {
            ShowMessageBox(objLanguage.Alert_Message_53.replace("{NO}", 9), 0, '');
        }
        else if ((iAdult + iChild + iInfant + iOther) == 0) {
            ShowMessageBox(objLanguage.Alert_Message_6, 0, '');
        }
        else {
            ShowProgressBar(true);

            var strOri = "";
            var strDest = "";
            if (origin.length == 0 & destination.length == 0) {
                var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
                var strOrigin = GetSelectedOption(objOrigin);

                var objDestination = document.getElementById(FindControlName("select", "optDestination"));
                var strDestination = GetSelectedOption(objDestination);

                strOri = strDestination.split("|")[0];
                strDest = strDestination.split("|")[2];

                if (strDestination.split("|")[5] != null && strDestination.split("|")[5] != 'undefined' && currencyCode == "") {
                    currencyCode = strDestination.split("|")[5];
                }

                objOrigin = null;
                objDestination = null;
            }
            else {
                strOri = origin;
                strDest = destination;
            }

            if (strOri.length == 0 || strDest.length == 0) {
                ShowMessageBox(objLanguage.Alert_Message_54, 0, '');
            }
            else {
                if (objSearchTypeDate != null && objSearchTypeDate.checked == false) {

                    dtDateFrom = getdatefrom(); // getMonthfrom();
                    dtDateTo = getdateto(); //getMonthto();
                    var todayDate = new Date();

                    tikAeroB2C.WebService.B2cService.GetLowFareFinderMonth(strOri,
                                                                       strDest,
                                                                       dtDateFrom,
                                                                       dtDateTo,
                                                                       isOneWay,
                                                                       iAdult,
                                                                       iChild,
                                                                       iInfant,
                                                                       strBoardingClass,
                                                                       currencyCode,
                                                                       strPromoCode,
                                                                       strSearchType,
                                                                       iOther,
                                                                       otherType,
                                                                       g_strIpAddress,
                                                                       ChangeToDateString(todayDate),
                                                                       SuccessGetLowFareFinder,
                                                                       showError,
                                                                       getdatefrom() + "|" + getdateto());
                }
                else {

                    dtDateFrom = getdatefrom();
                    dtDateTo = getdateto();
                    tikAeroB2C.WebService.B2cService.GetAvailability(strOri,
                                                                 strDest,
                                                                 dtDateFrom,
                                                                 dtDateTo,
                                                                 isOneWay,
                                                                 "0",
                                                                 iAdult,
                                                                 iChild,
                                                                 iInfant,
                                                                 strBoardingClass,
                                                                 currencyCode,
                                                                 strPromoCode,
                                                                 strSearchType,
                                                                 iOther,
                                                                 otherType,
                                                                 g_strIpAddress,
                                                                 SuccessSearchFlight,
                                                                 showError,
                                                                 showTimeOut);
                }
            }
        }

        objOrigin = null;
        objDestination = null;
        objOneWay = null;
        objAdult = null;
        objChild = null;
        objInfant = null;
        objBookingClass = null;
        objPromoCode = null;
        return true;
    }
    else {
        MultipleTab.Redirect();
        return false;
    }

}
