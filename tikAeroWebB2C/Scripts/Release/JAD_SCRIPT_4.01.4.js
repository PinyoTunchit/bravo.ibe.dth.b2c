﻿var tmpOutFlight_id = "";
var tmpOutFare_id = "";
var tmpRetFlight_id = "";
var tmpRetFare_id = "";
var LanguageCode = "";

function searchFlightAvailFromQueryString() {
    //ori=LGW&des=GCI&dep=2010-01-23&ret=2010-01-26&adt=1&chd=1&inf=1&pro=ww&bcls=Y&ft=L&currency=GBP

    if (!checkValidQueryStringForSearch()) {
    }
    else {   // If parameters are OK.
        // For check all set default value
        var bPass = true;
        var bLff = false;

        // Set Origin DropDownList            
        var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
        if (objOrigin != null) {
            var origin = queryStringValue("ori");
            var canSet = setSelectedOriginIndex(objOrigin, origin);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Destination DropDownLis before set value from query string.
        getDestination();

        // Set Destination DropDownList            
        var objDestination = document.getElementById(FindControlName("select", "optDestination"));
        if (objDestination != null) {
            var destination = queryStringValue("des");
            var canSet = setSelectedDestinationIndex(objDestination, destination);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Radio One-Way or Return, and Calendar DropDownList
        if (existEachQueryString("dep") && existEachQueryString("ret")) {   // If there are both Departure Date and Return Date, then set return
            // Set Radio
            var objReturn = document.getElementById("optReturn");
            if (objReturn != null) {
                objReturn.checked = true;
            }

            // Set Departure Date
            var ddlMY_1 = document.getElementById('ddlMY_1');
            var depDate = queryStringValue("dep");
            if (ddlMY_1 != null) {
                var canSet = setSelectedIndex(ddlMY_1, generateYearMonth(depDate));
                if (!canSet) {
                    bPass = false;
                }
                else {
                    hiddenCalendar(1, ddlMY_1);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_1 = document.getElementById('ddlDate_1');
            if (ddlDate_1 != null) {
                var canSet = setSelectedIndex(ddlDate_1, generateDate(depDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }

            // Set Return Date
            var ddlMY_2 = document.getElementById('ddlMY_2');
            var retDate = queryStringValue("ret");
            if (ddlMY_2 != null) {
                var canSet = setSelectedIndex(ddlMY_2, generateYearMonth(retDate));
                if (!canSet) {
                    bPass = false;
                }
                else {
                    hiddenCalendar(2, ddlMY_2);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_2 = document.getElementById('ddlDate_2');
            if (ddlDate_2 != null) {
                var canSet = setSelectedIndex(ddlDate_2, generateDate(retDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }
        else if (existEachQueryString("dep") && !existEachQueryString("ret")) { // If there is only Departure Date, then set to One Way
            // Set Radio
            var objOneWay = document.getElementById("optOneWay");
            if (objOneWay != null) {
                objOneWay.checked = true;
            }

            // Set Departure Date
            var ddlMY_1 = document.getElementById('ddlMY_1');
            var depDate = queryStringValue("dep");
            if (ddlMY_1 != null) {
                var canSet = setSelectedIndex(ddlMY_1, generateYearMonth(depDate));
                if (!canSet) {
                    bPass = false;
                }
                else {
                    hiddenCalendar(1, ddlMY_1);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_1 = document.getElementById('ddlDate_1');
            if (ddlDate_1 != null) {
                var canSet = setSelectedIndex(ddlDate_1, generateDate(depDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }
        ShowHideCalendar();

        // Set Adult Pax to Adult DropDownList
        var objAdult = document.getElementById("optAdult");
        if (objAdult != null) {
            var adult = queryStringValue("adt");
            var canSet = setSelectedIndex(objAdult, adult);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Child Pax to Child DropDownList
        var objChild = document.getElementById("optChild");
        if (objChild != null) {
            var child = queryStringValue("chd");
            var canSet = setSelectedIndex(objChild, child);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Infant Pax to Infant DropDownList
        var objInfant = document.getElementById("optInfant");
        if (objInfant != null) {
            var infant = queryStringValue("inf");
            var canSet = setSelectedIndex(objInfant, infant);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Boarding Class DropDownList
        var objBoardingClass = document.getElementById("optBoardingClass");
        if (objBoardingClass != null && existEachQueryString("bcls")) {
            var bclass = queryStringValue("bcls");
            var canSet = setSelectedIndex(objBoardingClass, bclass);
            if (!canSet) {
                bPass = false;
            }
        }

        // Set Promotion Code
        var objPromo = document.getElementById("txtPromoCode");
        if (objPromo != null && existEachQueryString("pro")) {
            var strPromo = queryStringValue("pro");
            objPromo.value = strPromo;
        }

        // Set Search Type (Availibility or lowfare finder)
        if (existEachQueryString("bLFF")) {
            bLff = queryStringValue("bLFF");
        }

        // Set Currency Class DropDownList
        var obtCurrency = document.getElementById(FindControlName("select", "optCurrency"));
        if (obtCurrency != null) {
            if (obtCurrency != null && existEachQueryString("currency")) {
                var strCurrency = queryStringValue("currency");
                var canSet = setSelectedIndex(obtCurrency, strCurrency);
                if (!canSet) {
                    bPass = false;
                }
            }
        }

        //Write agency code cookies.
        if (existEachQueryString("ao")) {

            var dtExpired = new Date();
            dtExpired.setMonth(dtExpired.getMonth() + 1);

            if (getCookie("logAgency") == null) {
                var strAgencyCode = queryStringValue("ao");
                // setCookie("logAgency", $.base64.encode(strAgencyCode), dtExpired, "", "", false);
            }
        }

        if (bPass == true) {
            if (bLff == "true") {
                var objSearchTypeCal = document.getElementById("rdSearchTypeCal");
                if (objSearchTypeCal != null) {
                    objSearchTypeCal.checked = true;
                }
            }
            return SearchAvailability(origin, destination);
        }
    }
}

//*------------------------------------------------------------
//** Helper Class
function setSelectedOriginIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        var optValue = obj.options[i].value.split("|");
        if (optValue[0] == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}

function setSelectedDestinationIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        var optValue = obj.options[i].value.split("|");
        if (optValue[2] == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}

function setSelectedIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        if (obj.options[i].value == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}

function queryStringValue(strKey, strDefault) {
    if (strDefault == null)
    { strDefault = ""; }

    strKey = strKey.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

    var regex = new RegExp("[\\?&]" + strKey + "=([^&#]*)");
    var qs = regex.exec(window.location.href);

    if (qs == null)
    { return strDefault; }
    else
    { return qs[1]; }
}

function checkValidQueryStringForSearch() {
    var result = true;

    // Parameter "ori" is Origin Acronym. Required.
    if (!existEachQueryString("ori")) {
        result = false;
    }
    else {
        if (queryStringValue("ori") == "") {
            result = false;
        }
    }

    // Parameter "des" is Destination Acronym. Required.
    if (!existEachQueryString("des")) {
        result = false;
    }
    else {
        if (queryStringValue("des") == "") {
            result = false;
        }
    }

    // Parameter "dep" is Departure Date. Its format is yyyy-mm-dd. Required.
    if (!existEachQueryString("dep")) {
        result = false;
    }
    else {
        var dep = queryStringValue("dep");
        if (dep == "") {
            result = false;
        }
        else {
            if (!validateQueryStringDate(dep)) {
                result = false;
            }
        }
    }

    // Parameter "ret" is Return Date. Its format is yyyy-mm-dd. Required if round trip.
    if (existEachQueryString("ret")) {
        var ret = queryStringValue("ret");
        if (ret == "") {
            result = false;
        }
        else {
            if (!validateQueryStringDate(ret)) {
                result = false;
            }
        }
    }

    // Parameter "adt" is Adult Passenger amount. Required.
    if (!existEachQueryString("adt")) {
        result = false;
    }
    else {
        var adult = queryStringValue("adt");
        if (adult == "") {
            result = false;
        }
        else {
            if (!isNum(adult)) {
                result = false;
            }
        }
    }

    // Parameter "chd" is Child Passenger amount. Required.
    if (!existEachQueryString("chd")) {
        result = false;
    }
    else {
        var child = queryStringValue("chd");
        if (child == "") {
            result = false;
        }
        else {
            if (!isNum(child)) {
                result = false;
            }
        }
    }

    // Parameter "inf" is Infant Passenger amount. Required.
    if (!existEachQueryString("inf")) {
        result = false;
    }
    else {
        var infant = queryStringValue("inf");
        if (infant == "") {
            result = false;
        }
        else {
            if (!isNum(infant)) {
                result = false;
            }
        }
    }

    // Parameter "pro" is Promotion Code.
    if (existEachQueryString("pro")) {
        var procode = queryStringValue("pro");
        if (procode == "") {
            result = false;
        }
    }

    // Parameter "bcls" is Boarding Class.
    if (existEachQueryString("bcls")) {
        var bclass = queryStringValue("bcls");
        if (bclass == "") {
            result = false;
        }
    }

    // Parameter "ft" is Fare Type.
    if (existEachQueryString("ft")) {
        var faretype = queryStringValue("ft");
        if (faretype == "") {
            result = false;
        }
    }

    return result;
}

function existEachQueryString(qryValue) {
    var hu = window.location.search.substring(1);
    var gy = hu.split("&");
    var found = false;
    for (i = 0; i < gy.length; i++) {
        ft = gy[i].split("=");
        if (ft[0] == qryValue) {
            found = true;
        }
    }
    return found;
}

function validateQueryStringDate(dtValue) {
    var splitDate = dtValue.split("-");
    var refDate = new Date(splitDate[1] + "/" + splitDate[2] + "/" + splitDate[0]);
    if (splitDate[1] < 1 || splitDate[1] > 12 || refDate.getDate() != splitDate[2] || splitDate[0].length != 4 || (!/^20/.test(splitDate[0]))) {
        return false;
    }
    else {
        return true;
    }
}
function isNum(string) {
    var numericExpression = /^[0-9]+$/;
    if (string.match(numericExpression)) {
        return true;
    } else {
        return false;
    }
}
function generateYearMonth(strDate) {
    var arrDate = strDate.split("-");
    return arrDate[0] + arrDate[1];
}
function generateDate(strDate) {
    var arrDate = strDate.split("-");
    return arrDate[2];
}
function checkMyBookingString() {
    // mb=l

    MultipleTab.ClearID();

    if (existEachQueryString("mb") && queryStringValue("mb") == "l") {
        return true;
    }
    else {
        return false;
    }
}
function CheckRegisterLogon() {
    // mb=l
    if (existEachQueryString("rg") && queryStringValue("rg") == "l") {
        return true;
    }
    else {
        return false;
    }
}


function GetAvailabilityFromLowFareFinder() {
    var objOptLowFare;

    var strParam = "";
    var strDateFrom = 0;
    var strDateTo = 0;
    var dblOutwarFare = 0;
    var dblRetrunFare = 0;
    var objJSON;

    if (MultipleTab.IsCorrectTab()) {
        //-- Outward Flight
        objOptLowFare = document.getElementsByName("optLowfare_Outward");
        if (objOptLowFare != null) {
            for (var i = 0; i < objOptLowFare.length; i++) {
                if (objOptLowFare[i].checked == true) {
                    objJSON = eval("(" + objOptLowFare[i].value + ")");

                    strParam = objJSON.origin_rcd + "|" + objJSON.origin_name + "|" + objJSON.destination_rcd + "|1";
                    strDateFrom = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                    dblOutwarFare = objJSON.total_adult_fare;
                    var objOutwardNetTotal = document.getElementById("spnFareNetTotal_Outward");
                    if (objOutwardNetTotal != null) {
                        dblOutwarFare = parseFloat(objOutwardNetTotal.innerHTML);
                        objOutwardNetTotal = null;
                    }
                }
            }
        }
        objOptLowFare = null;

        //-- Return Flight
        objOptLowFare = document.getElementsByName("optLowfare_Return");
        if (objOptLowFare != null) {
            for (var i = 0; i < objOptLowFare.length; i++) {
                if (objOptLowFare[i].checked == true) {
                    objJSON = eval("(" + objOptLowFare[i].value + ")");

                    strDateTo = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                    dblRetrunFare = objJSON.total_adult_fare;
                    var objReturnNetTotal = document.getElementById("spnFareNetTotal_Return");
                    if (objReturnNetTotal != null) {
                        dblRetrunFare = parseFloat(objReturnNetTotal.innerHTML);
                        objReturnNetTotal = null;
                    }
                }
            }
        }
        objOptLowFare = null;

        if (strDateFrom != "") {
            if (strDateFrom > strDateTo && strDateTo != 0) {
                //Please check your return Date! It must be equal to or higher than the Departure Date
                ShowMessageBox(objLanguage.Alert_Message_5, 0, '');
            }
            else {
                ShowProgressBar(true);
                tikAeroB2C.WebService.B2cService.GetAvailabilityFromLowFareFinder(strParam,
                                                                          strDateFrom,
                                                                          strDateTo,
                                                                          false,
                                                                          dblOutwarFare,
                                                                          dblRetrunFare,
                                                                          tmpOutFlight_id + ":" + tmpOutFare_id,
                                                                          tmpRetFlight_id + ":" + tmpRetFare_id,
                                                                          SuccessGetAvailabilityFromLowFareFinder,
                                                                          showError,
                                                                          dblOutwarFare + "|" + dblRetrunFare);
            }
        }
        else {
            //"Please select outward flight"
            ShowMessageBox(objLanguage.Alert_Message_57, 0, '');
        }
    }
    else {
        MultipleTab.Redirect();
    }
}

function SuccessGetAvailabilityFromLowFareFinder(result, strFareAmount) {
    if (result.length > 0) {

        if (result == "{201}" || result == "{102}") {
            //Booking object is null.(Session time out).
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var obj = document.getElementById("dvContainer");
            obj.innerHTML = result.split("{}")[1];

            var chkOutward = document.getElementsByName("Outward");
            var chkReturn = document.getElementsByName("Return");
            var arrLowestAmount = strFareAmount.split("|");

            if (chkOutward != null && chkOutward.length > 0) {
                chkOutward[0].checked = true;
            }
            if (chkReturn != null && chkReturn.length > 0) {
                chkReturn[0].checked = true;
            }

            //preselect at the lowest far.
            var arrLowestAmount = strFareAmount.split("|");
            var dclOutboundFare = parseFloat(arrLowestAmount[0]);
            var dclReturnFare = parseFloat(arrLowestAmount[1]);
            var objOut = document.getElementsByName("Outward");
            var objReturn = document.getElementsByName("Return");
            var dclTotalFareAmount = 0;
            var arrParam;
            var isFoundOutFare = false;
            var isFoundReturnFare = false;
            //loop to preselect the lowest fare.
            if (objOut != null) {
                for (var i = 0; i < objOut.length; i++) {
                    arrParam = objOut[i].value.split("|");
                    if (arrParam.length > 0) {
                        dclTotalFareAmount = parseFloat(arrParam[20].split(":")[1]) + parseFloat(arrParam[21].split(":")[1]);
                        if (dclTotalFareAmount == dclOutboundFare) {
                            objOut[i].checked = true;
                            isFoundOutFare = true;
                            break;
                        }
                    }
                }
            }

            //Clear Param value.
            arrParam = null;
            dclTotalFareAmount = 0;
            if (objReturn != null) {
                for (var i = 0; i < objReturn.length; i++) {
                    arrParam = objReturn[i].value.split("|");
                    if (arrParam.length > 0) {
                        dclTotalFareAmount = parseFloat(arrParam[20].split(":")[1]) + parseFloat(arrParam[21].split(":")[1]);
                        if (dclTotalFareAmount == dclReturnFare) {
                            objReturn[i].checked = true;
                            isFoundReturnFare = true;
                            break;
                        }
                    }
                }
            }

            //Show Cobo-flight information
            ShowComboflightInformation();

            // Show Flight tool tip.
            ToolTipColor();
            ClickToExpand();

            var optReturn = document.getElementById("optReturn");
            //Move scroll to top page.
            if (!isFoundOutFare || (!isFoundReturnFare && optReturn.checked)) {
                ShowMessageBox(objLanguage.Alert_Message_162, 0, '');
                //Select at lowest fare.
                SelectLowestFare();

                //Clear Fare Summary
                DisplayQuoteSummary("", "", "");
            }
            //Select at lowest fare.
            //SelectLowestFare();

            //Clear Fare Summary
            //DisplayQuoteSummary("", "", "");
        }
    }
    ShowProgressBar(false);
}

function TotalDayOfmonth(month, year) {
    return Math.round(((new Date(year, month)) - (new Date(year, month - 1))) / 86400000);
}

function genValuesDDL(obj, strDate) {
    ddl = document.getElementById(obj);
    clearDDL(ddl);
    for (var i = 1; i <= TotalDayOfmonth(strDate.substring(0, 2), strDate.substring(2, 6)) ; i++) {
        var opt = document.createElement("option");
        if (i < 10) {
            i = "0" + i;
        }
        opt.text = i;
        opt.value = i;
        ddl.options.add(opt);
    }
}

function setFromDate(strDate) {

    genValuesDDL("ddlDate_1", strDate);
    SetComboValue("ddlMY_1", strDate.substring(0, 6));
    SetComboValue("ddlDate_1", strDate.substring(6, 8));
}
function setToDate(strDate) {

    genValuesDDL("ddlDate_2", strDate);
    SetComboValue("ddlMY_2", strDate.substring(0, 6));
    SetComboValue("ddlDate_2", strDate.substring(6, 8));
}

function SelectTime(strObj) {
    if (MultipleTab.IsCorrectTab()) {
        if (strObj.length > 0) {
            var obj = document.getElementById(strObj);
            if (obj != null && obj.parentNode.parentNode.parentNode != null) {
                var strType = obj.parentNode.id.split("_")[1];
                var objJSON = eval("(" + obj.value + ")");
                GetLowFareFinderSummary(strType, objJSON);
            }

        }
    }
    else {
        MultipleTab.Redirect();
    }
}
function SearchAvailabilityParameter() {
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var strOrigin = objOrigin.options[objOrigin.selectedIndex].value;
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    var objOneWay = document.getElementById("optOneWay");
    var objAdult = document.getElementById("optAdult");
    var objChild = document.getElementById("optChild");
    var objInfant = document.getElementById("optInfant");
    var objBoardingClass = document.getElementById("optBoardingClass");
    var objPromoCode = document.getElementById("txtPromoCode");
    var objYP = document.getElementById("optYP");


    var strDestination = objDestination.options[objDestination.selectedIndex].value;
    var isOneWay = objOneWay.checked;
    var iAdult = parseInt(objAdult.options[objAdult.selectedIndex].value);
    var iChild = parseInt(objChild.options[objChild.selectedIndex].value);
    var iInfant = parseInt(objInfant.options[objInfant.selectedIndex].value);
    var strBoardingClass = objBoardingClass.options[objBoardingClass.selectedIndex].value;
    var currencyCode = strOrigin.split("|")[1];
    var dtDateFrom = getdatefrom();
    var dtDateTo = getdateto();
    var strPromoCode = "";
    var iOther = 0;
    var otherType = "";

    // Check Ip address for Default Currency 
    var obtCurrency = document.getElementById("AvailabilitySearch1_optCurrency");

    if (obtCurrency != null) {
        currencyCode = obtCurrency.options[obtCurrency.selectedIndex].value;
    }
    // End Check Ip address for Default Currency 
    if (objYP != null) {
        iOther = parseInt(objYP.options[objYP.selectedIndex].value);
        otherType = "YP";
    }

    if (objPromoCode != null) {
        strPromoCode = objPromoCode.value;
    }

    //Call Availability web service
    if (iInfant > iAdult) {
        //Infant Can't be more than adult.
        ShowMessageBox(objLanguage.Alert_Message_2, 0, '');
    }
    else if ((iAdult + iChild + iInfant) > 12) {
        //Total passenger select can't be more than 12
        ShowMessageBox(objLanguage.objLanguage.Alert_Message_53.replace("{NO}", 12), 0, '');
    }
    else if ((iAdult + iChild + iInfant + iOther) == 0) {
        //Please select number of passenger.
        ShowMessageBox(objLanguage.Alert_Message_6, 0, '');
    }
    else {
        //Popup new window for search result

        var strParameter = "?ori=" + strOrigin.split("|")[0] +
                            "&des=" + strDestination.split("|")[2] +
                            "&dep=" + ReformatYYYYMMDDToDashDate(dtDateFrom) +
                            ((isOneWay == false) ? "&ret=" + ReformatYYYYMMDDToDashDate(dtDateTo) : "") +
                            "&adt=" + iAdult +
                            "&chd=" + iChild +
                            "&inf=" + iInfant +
                            "&currency=" + currencyCode +
                            "&langculture=" + queryStringValue("langculture", "en-us");

        var strUrl = GetVirtualDirectory() + "default.aspx" + strParameter;
        window.open(strUrl, "", "scrollbars=1,status=1,toolbar=0,resizable=1", "");
    }

    objOrigin = null;
    objDestination = null;
    objOneWay = null;
    objAdult = null;
    objChild = null;
    objInfant = null;
    objBookingClass = null;
    objPromoCode = null;
}

function SelectLowestFare() {

    var objOut = document.getElementsByName("Outward");
    var objReturn = document.getElementsByName("Return");
    var dclFareAmount = 0;
    var arrFareValue;
    var arrFare;
    var arrTax;
    var j = 0;

    //Search For outbound.
    if (objOut != null) {
        for (var i = 0; i < objOut.length; i++) {

            arrFareValue = objOut[i].value.split("|");
            arrFare = arrFareValue[21].split(":");
            arrTax = arrFareValue[20].split(":");

            //Get Total Fare plus Tax
            if (dclFareAmount == 0) {
                j = i;
                dclFareAmount = parseFloat(arrFare[1]) + parseFloat(arrTax[1]);
            }
            else if (dclFareAmount > (parseFloat(arrFare[1]) + parseFloat(arrTax[1]))) {
                j = i;
                dclFareAmount = parseFloat(arrFare[1]) + parseFloat(arrTax[1]);
            }
        }

        if (objOut.length > 0) {
            objOut[j].checked = true;
            GetQuoteSummary(objOut[j], "Outward");
        }
    }

    //Search For return.
    dclFareAmount = 0;
    j = 0;
    if (objReturn != null) {
        for (var i = 0; i < objReturn.length; i++) {

            arrFareValue = objReturn[i].value.split("|");
            arrFare = arrFareValue[21].split(":");
            arrTax = arrFareValue[20].split(":");

            //Get Total Fare plus Tax
            if (dclFareAmount == 0) {
                j = i;
                dclFareAmount = parseFloat(arrFare[1]) + parseFloat(arrTax[1]);
            }
            else if (dclFareAmount > (parseFloat(arrFare[1]) + parseFloat(arrTax[1]))) {
                j = i;
                dclFareAmount = parseFloat(arrFare[1]) + parseFloat(arrTax[1]);
            }
        }

        if (objReturn.length > 0) {
            objReturn[j].checked = true;
            GetQuoteSummary(objReturn[j], "Return");
        }
    }
}

function SuccessSearchFlight(result) {

    if (result.length > 0) {

        if (result == "{002}") {
            loadHome();
        }
        else if (result == "{003}") {
            //Invalid Required Parameter.
            ShowMessageBox(objLanguage.Alert_Message_55, 0, '');
        }
        else if (result == "{001}") {
            LoadSecure(false);
        }
        else {
            if (result == "{004}") {
                //System Error. Please try again.
                ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
            }
            else {

                //Set Insurance popup
                SetShowInsurance("0");

                //Display Html
                var obj = document.getElementById("dvContainer");
                obj.innerHTML = result;
                obj = null;

                //Select at lowest fare.
                SelectLowestFare();

                //Clear Fare Summary
                DisplayQuoteSummary("", "", "");
                //Hide Select buttom when client is on hold
                var objClientOnHold = document.getElementById("dvClientOnHold");
                if (objClientOnHold.value == 1) {
                    document.getElementById("dvAvaiNext").style.display = "none";
                }
                else {
                    document.getElementById("dvAvaiNext").style.display = "block";
                }
                objClientOnHold = null;

                //Hide Previous 7 day if the current and selecte date are the same.
                var todayDate = new Date();
                var strCurrentDate = ChangeToDateString(todayDate);

                var objOutWardSelectDate = document.getElementById("Outward_hdSelectedDate");
                var objReturnSelectDate = document.getElementById("Return_hdSelectedDate");

                if (objOutWardSelectDate != null) {
                    if (strCurrentDate == GetControlValue(objOutWardSelectDate)) {
                        document.getElementById("Outward_DatePrevious").style.display = "none";
                    }
                    else {
                        document.getElementById("Outward_DatePrevious").style.display = "block";
                    }
                }

                if (objReturnSelectDate != null) {
                    if (strCurrentDate == GetControlValue(objReturnSelectDate)) {
                        document.getElementById("Return_DatePrevious").style.display = "none";
                    }
                    else {
                        document.getElementById("Return_DatePrevious").style.display = "block";
                    }
                }

                // Show Flight tool tip.
                ToolTipColor();

                ClickToExpand();

                //Move scroll to top page.


                //Rename search button.
                var btmSearch = document.getElementById("spnSearchCap");
                if (btmSearch != null) {
                    btmSearch.innerHTML = objLanguage.Alert_Message_138;  //"Refine your search";
                }

                //Set Calendar Date.
                var objFlightDateOutward = document.getElementById("Outward_hdSelectedDate");
                var objFlightDateReturn = document.getElementById("Return_hdSelectedDate");

                if (objFlightDateOutward != null) {
                    setFromDate(objFlightDateOutward.value);
                }
                if (objFlightDateReturn != null) {
                    setToDate(objFlightDateReturn.value);
                }

                ShowSearchPannel(true);
                ShowProgressBar(false);

                ShowComboflightInformation();

                if (UseGoogleManager() == true) {
                    dataLayer.push({ 'event': 'SearchResultPage' });
                }
                else {
                    //google analytic
                    if (_gaq) {
                        _gaq.push(['_trackPageview', '/booking/search-result.html']);
                    }
                }



            }
        }
    }
}

function ShowComboflightInformation() {
    //Show Cobo-flight information
    var objTransitCount = document.getElementById("hdNumberOfTransit");
    var objComboInfo = document.getElementById("dvComboFlightInfo");
    if (objTransitCount != null && objComboInfo != null) {
        if (parseInt(objTransitCount.value) > 0) {
            objComboInfo.style.display = "block";
        }
        else {
            objComboInfo.style.display = "none";
        }
    }
}

function SearchSingleFlight(flightDate, strFlightType) {

    var objFlightDate;
    if (MultipleTab.IsCorrectTab()) {
        if (strFlightType == "Outward") {
            objFlightDate = document.getElementById("Return_hdSelectedDate");
        }
        else {
            objFlightDate = document.getElementById("Outward_hdSelectedDate");
        }
        if (objFlightDate != null &&
        ((strFlightType == "Return" && parseInt(flightDate) < parseInt(objFlightDate.value)))) {

        }
        else {
            var todayDate = new Date();
            var cDate = ChangeToDateString(todayDate);
            var sDate = flightDate;

            if (sDate <= cDate) {
                flightDate = ChangeToDateString(todayDate);
            }

            if (strFlightType == "Outward") {

                setFromDate(flightDate);
                ShowProgressBar(true);
                tikAeroB2C.WebService.B2cService.SearchSingleFlight(flightDate, true, SuccessSearchFlight, showError, showTimeOut);
            }
            else {

                setToDate(flightDate);
                ShowProgressBar(true);
                tikAeroB2C.WebService.B2cService.SearchSingleFlight(flightDate, false, SuccessSearchFlight, showError, showTimeOut);
            }
            document.getElementById("dvAvaiNext").style.display = "none";
        }
    }
    else {
        MultipleTab.Redirect();
    }
}

function SelectFlight() {

    if (MultipleTab.IsCorrectTab()) {

        //Set Insurance popup
        SetShowInsurance("0");

        // Clear Callback
        var objMessage = document.getElementById("dvMessageBox");
        if (objMessage != null)
            objMessage.setAttribute('CallBack', "");

        var objOut = document.getElementsByName("Outward");
        var objReturn = document.getElementsByName("Return");
        var bShowConfirm = true;
        var arrId;

        var oComboFlightInfo = document.getElementById("dvComboFlightInfo");
        var oComboFlightCheckbox = document.getElementById("chkComboFlight");


        if (objOut != null) {
            for (var i = 0; i < objOut.length; i++) {
                if (objOut[i].checked == true) {
                    arrId = objOut[i].id.split("_");
                    if (arrId.length == 2) {
                        if (arrId[1] != 1) {
                            bShowConfirm = false;
                        }
                    }
                    else {
                        bShowConfirm = false;
                    }
                    break;
                }
            }
        }

        if (objReturn != null) {
            for (var i = 0; i < objReturn.length; i++) {
                if (objReturn[i].checked == true) {
                    arrId = objReturn[i].id.split("_");
                    if (arrId.length == 2) {
                        if (arrId[1] != 1) {
                            bShowConfirm = false;
                        }
                    }
                    else {
                        bShowConfirm = false;
                    }
                    break;
                }
            }
        }


        if (oComboFlightInfo.style.display == "block") {
            if (oComboFlightCheckbox.checked == true) {
                ConfirmSelectFlight();
            }
            else {
                ShowMessageBox(objLanguage.Alert_Message_157 + "<BR><BR> " + objLanguage.Alert_Message_158, 0, '');
            }
        }
        else {
            ConfirmSelectFlight();
        }

    }
    else {
        MultipleTab.Redirect();
    }


}

function ConfirmSelectFlight() {
    var chkOutward = document.getElementsByName("Outward");
    var OutwardParam = "";
    var OutwardDate = 0;
    var OutwardTime = 0;
    var OutwardArrivalTime = 0;


    var chkReturn = document.getElementsByName("Return");
    var ReturnParam = "";
    var ReturnDate = 0;
    var ReturnTime = 0;
    var ReturnArrivalTime = 0;

    var iCount = 0;

    var objSelectedDate = document.getElementsByName("hdSelectedDate");

    CloseMessageBox();
    if (objSelectedDate.length == 2 & (chkReturn == null || chkReturn.length == 0)) {
        //Please select return flight.
        ShowMessageBox("Please select return flight.", 0, '');
    }
    else {
        if (chkOutward != null) {
            iCount = chkOutward.length;
            if (iCount > 0) {
                for (var i = 0; i < iCount; i++) {
                    if (chkOutward[i].checked == true) {
                        OutwardParam = chkOutward[i].value;
                        break;
                    }
                }
                if (OutwardParam.length > 0) {
                    arrFlightInfo = OutwardParam.split("|");
                    for (var i = 0; i < arrFlightInfo.length; i++) {
                        arrFlightDetail = arrFlightInfo[i].split(":");
                        if (arrFlightDetail[0] == "departure_date") {
                            OutwardDate = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_departure_time") {
                            OutwardTime = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_arrival_time") {
                            OutwardArrivalTime = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "transit_planned_arrival_time") {
                            if (arrFlightDetail[1].length > 0) {
                                OutwardArrivalTime = parseInt(arrFlightDetail[1]);
                            }
                        }
                    }
                }
            }
        }

        iCount = 0;
        if (chkReturn != null) {
            iCount = chkReturn.length;
            if (iCount > 0) {
                for (var i = 0; i < iCount; i++) {
                    if (chkReturn[i].checked == true) {
                        ReturnParam = chkReturn[i].value;
                        break;
                    }
                }
                if (ReturnParam.length > 0) {
                    arrFlightInfo = ReturnParam.split("|");
                    for (var i = 0; i < arrFlightInfo.length; i++) {
                        arrFlightDetail = arrFlightInfo[i].split(":");
                        if (arrFlightDetail[0] == "departure_date") {
                            ReturnDate = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_departure_time") {
                            ReturnTime = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_arrival_time") {
                            ReturnArrivalTime = parseInt(arrFlightDetail[1]);
                        }
                    }
                }
            }
        }

        if (OutwardParam.length == 0) {
            //Please Select your flight of origin.
            ShowMessageBox(objLanguage.Alert_Message_57, 0, '');
        }
        else if (objSelectedDate.length == 2 && ReturnParam.length == 0) {
            //Please select return flight.
            ShowMessageBox("Please select return flight.", 0, '');
        }
        else if (ReturnParam.length != 0 && (OutwardDate > ReturnDate)) {
            //Please check your return Date! It must be equal to or higher than the Departure Date.
            ShowMessageBox(objLanguage.Alert_Message_5, 0, '');
        }
        else if (ReturnParam.length != 0 && (OutwardTime > ReturnTime) && (OutwardDate == ReturnDate)) {
            ////Please check your return Time! It must be equal to or higher than the Departure Time.
            ShowMessageBox(objLanguage.Alert_Message_58, 0, '');
        }
        else if (ReturnParam.length != 0 && (OutwardArrivalTime >= ReturnTime) && (OutwardDate == ReturnDate)) {
            ////Please check your return Time! It must be equal to or higher than the Departure Time.
            ShowMessageBox(objLanguage.Alert_Message_58, 0, '');
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.AddFlight(OutwardParam, ReturnParam, g_strIpAddress, SuccessSelectFlight, showError, showTimeOut);
        }

        chkOutward = null;
        chkReturn = null;
    }
}

function ShowTermUpFareSell() {
    var url = "http://www.flypeach.com/conditionsofcarriage.aspx?source=fareup_banner_enus";

    if ($('#hdLang').val().toLowerCase() == "ja-jp") {
        url = "http://www.flypeach.com/jp/ja-jp/conditionsofcarriage.aspx?source=fareup_banner_jajp";
    }

    if ($('#hdLang').val().toLowerCase() == "ko-kr") {
        url = "http://www.flypeach.com/kr/corporate/conditionsofcarriage.aspx?source=fareup_banner_kokr";
    }

    if ($('#hdLang').val().toLowerCase() == "zh-hk") {
        url = "http://www.flypeach.com/hk/corporate/conditionsofcarriage.aspx?source=fareup_banner_zhhk";
    }

    if ($('#hdLang').val().toLowerCase() == "zh-tw") {
        url = "http://www.flypeach.com/tw/conditionsofcarriage.aspx?source=fareup_banner_zhtw";
    }

    if ($('#hdLang').val().toLowerCase() == "zh-cn") {
        url = "http://www.flypeach.com/cn/conditionsofcarriage.aspx?source=fareup_banner_zhcn";
    }

    window.open(url, "_blank");
}
function LoadB2BLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2BLogon(SuccessLoadB2BLogon, showError, showTimeOut);
}

function SuccessLoadB2BLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2BLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2BLogonDialog(SuccessLoadB2BLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2BLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function LoadB2BAdminLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2BAdminLogon(SuccessLoadB2BAdminLogon, showError, showTimeOut);
}

function SuccessLoadB2BAdminLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2BAdminLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2BAdminLogonDialog(SuccessLoadB2BAdminLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2BAdminLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    //Insert passenger form content.
    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}
function AgencyLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.AgencyLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogLogonID').value, document.getElementById('txtDialogPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.AgencyLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtLogonID').value, document.getElementById('txtPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyAdminLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.AgencyAdminLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogAdminLogonID').value, document.getElementById('txtDialogPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function AgencyAdminLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.AgencyAdminLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtAdminLogonID').value, document.getElementById('txtPassword').value, SuccessAgencyLogon, showError, showTimeOut);

}

function SuccessAgencyLogon(result) {
    var arrResult = result.split(",");

    if (arrResult.length == 2 && arrResult[0] == 'Error') {
        ShowProgressBar(false);
        if (arrResult[1] == "0") {
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
        else {
            ShowMessageBox(arrResult[1], 0, '');
        }
    }
    else {
        window.location.replace(result);
    }
}
function SubmitEnterAgencyLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            AgencyLogon();
        else
            AgencyLogonDialog();
        return false;
    }
    else
        return true;
}
function SubmitEnterAgencyAdminLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            AgencyAdminLogon();
        else
            AgencyAdminLogonDialog();
        return false;
    }
    else
        return true;
}
function LoadB2ELogon() {
    tikAeroB2C.WebService.B2cService.LoadB2ELogon(SuccessLoadB2ELogon, showError, showTimeOut);
}

function SuccessLoadB2ELogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2ELogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2ELogonDialog(SuccessLoadB2ELogonDialog, showError, showTimeOut);
}

function SuccessLoadB2ELogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function LoadB2EAdminLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2EAdminLogon(SuccessLoadB2EAdminLogon, showError, showTimeOut);
}

function SuccessLoadB2EAdminLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2EAdminLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2EAdminLogonDialog(SuccessLoadB2EAdminLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2EAdminLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}
function CorporateLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.CorporateLogon(document.getElementById('txtDialogCompanyCode').value,
        document.getElementById('txtDialogLogonID').value, document.getElementById('txtDialogPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.CorporateLogon(document.getElementById('txtCompanyCode').value,
        document.getElementById('txtLogonID').value, document.getElementById('txtPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateAdminLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.CorporateAdminLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogAdminLogonID').value, document.getElementById('txtDialogPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateAdminLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.CorporateAdminLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtAdminLogonID').value, document.getElementById('txtPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function SuccessCorporateLogon(result) {
    var arrResult = result.split(",");

    if (arrResult.length == 2 && arrResult[0] == 'Error') {
        ShowProgressBar(false);
        if (arrResult[1] == "0") {
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
        else {
            ShowMessageBox(arrResult[1], 0, '');
        }
    }
    else {
        window.location.replace(result);
    }
}
function SubmitEnterCorporateLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            CorporateLogon();
        else
            CorporateLogonDialog();
        return false;
    }
    else
        return true;
}
function SubmitEnterCorporateAdminLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            CorporateAdminLogon();
        else
            CorporateAdminLogonDialog();
        return false;
    }
    else
        return true;
}
function CalculateBaggageFee(bReturn) {
    var objBagFee;
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPaxSelectPosition);
    var hdSegmentId;

    if (bReturn == false) {
        objBagFee = document.getElementById("BaggageDepart");
        hdSegmentId = document.getElementById("hdOutwardSegmentId");
    }
    else {
        objBagFee = document.getElementById("BaggageReturn");
        hdSegmentId = document.getElementById("hdReturnSegmentId");
    }

    if (objBagFee != null && hdSegmentId != null) {
        var iFeeUnit = GetSelectedOption(objBagFee);
        if (iFeeUnit.length > 0) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.CalculateBaggageFee(parseInt(iFeeUnit), GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), bReturn, true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.ClearBaggageFee(GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
    }
}

function CalculateBaggageSeat(baggageID, paxIndex) {
    var objBagFee = document.getElementById(baggageID);
    var hdPassengerId = document.getElementById("hdPassengerId" + paxIndex);
    var hdSegmentId = document.getElementById("hdSegmentId" + paxIndex);

    if (objBagFee != null && hdSegmentId != null) {
        var iFeeUnit = GetSelectedOption(objBagFee);
        if (iFeeUnit.length > 0) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.CalculateBaggageFee(parseInt(iFeeUnit), GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), true, true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.ClearBaggageFee(GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
    }
}

function SuccessCalculateBaggageFee(result) {
    if (result.length > 0) {

        DisplayQuoteSummary(result, "", "");

        //Initialize Summary collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
    ShowProgressBar(false);
}
function GetBaggageFeeOptions(iPosition) {
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPosition);
    //Get Baggage list.
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetBaggageFeeOptions(GetControlValue(hdPassengerId), SuccessGetBaggageFeeOptions, showError, showTimeOut);
}

function SuccessGetBaggageFeeOptions(result) {

    if (result.length > 0) {
        if (result == "{201}") {
            //Booking object is null.(Session time out).
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objJson = eval("(" + result + ")");
            var strFeeDisplayText = "";
            var strCurrencySign = "";

            //Fill baggage origin list.
            var objDept = document.getElementById("BaggageDepart");
            if (objDept != null) {
                objDept = ClearOptions(objDept);

                var optDefaultOut = document.createElement("option");
                optDefaultOut.className = "watermarkOn";
                optDefaultOut.value = "";
                if (objJson[0].BaggageOutAllowance != null && objJson[0].BaggageOutAllowance > 0) {
                    if (objJson[0].BaggageOutAllowance > 1) {
                        optDefaultOut.innerHTML = objJson[0].BaggageOutAllowance + " " + objLanguage.default_value_9 + " " + objLanguage.default_value_10;
                    }
                    else {
                        optDefaultOut.innerHTML = objJson[0].BaggageOutAllowance + " " + objLanguage.default_value_8 + " " + objLanguage.default_value_10;
                    }
                }
                else {
                    optDefaultOut.innerHTML = objLanguage.default_value_7;
                }
                objDept.appendChild(optDefaultOut);

                //Loop through list.
                for (var i = 0; i < objJson[0].BaggageOut.length; i++) {

                    var opt = document.createElement("option");

                    if (objJson[0].BaggageOut[i].currency_rcd == "JPY") {
                        strCurrencySign = "&#165;";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "HKD") {
                        strCurrencySign = "HK$";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "TWD") {
                        strCurrencySign = "NT$";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "CNY") {
                        strCurrencySign = "RMB";
                    }
                    else {
                        strCurrencySign = "&#8361;";
                    }

                    if (objJson[0].BaggageOut[i].number_of_units > 1) {
                        strFeeDisplayText = objJson[0].BaggageOut[i].number_of_units + objLanguage.default_value_9 + " " + strCurrencySign + " " + Math.round(objJson[0].BaggageOut[i].total_amount_incl);
                    }
                    else {
                        strFeeDisplayText = objJson[0].BaggageOut[i].number_of_units + objLanguage.default_value_8 + " " + strCurrencySign + " " + Math.round(objJson[0].BaggageOut[i].total_amount_incl);
                    }

                    if (objJson[0].BaggageOut[i].Selected == true) {
                        opt.selected = true;
                    }

                    opt.value = objJson[0].BaggageOut[i].number_of_units;
                    opt.innerHTML = strFeeDisplayText;
                    objDept.appendChild(opt);
                }

                //Add Event;
                $("#BaggageDepart").unbind();
                $("#BaggageDepart").change(function () {
                    CalculateBaggageFee(false);
                    SetPassengerValue();
                });
            }

            //Fill baggage Destination list.
            var objReturn = document.getElementById("BaggageReturn");
            if (objReturn != null) {
                objReturn = ClearOptions(objReturn);

                var optDefaultReturn = document.createElement("option");
                optDefaultReturn.className = "watermarkOn";
                optDefaultReturn.value = "";
                if (objJson[0].BaggageReturnAllowance != null && objJson[0].BaggageReturnAllowance > 0) {
                    if (objJson[0].BaggageReturnAllowance > 1) {
                        optDefaultReturn.innerHTML = objJson[0].BaggageReturnAllowance + " " + objLanguage.default_value_9 + " " + objLanguage.default_value_10;
                    }
                    else {
                        optDefaultReturn.innerHTML = objJson[0].BaggageReturnAllowance + " " + objLanguage.default_value_8 + " " + objLanguage.default_value_10;
                    }
                }
                else {
                    optDefaultReturn.innerHTML = objLanguage.default_value_7;
                }
                objReturn.appendChild(optDefaultReturn);

                //Loop through list.
                for (var i = 0; i < objJson[0].BaggageReturn.length; i++) {

                    var opt = document.createElement("option");

                    if (objJson[0].BaggageReturn[i].currency_rcd == "JPY") {
                        strCurrencySign = "&#165;";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "HKD") {
                        strCurrencySign = "HK$";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "TWD") {
                        strCurrencySign = "NT$";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "CNY") {
                        strCurrencySign = "RMB";
                    }
                    else {
                        strCurrencySign = "&#8361;";
                    }

                    if (objJson[0].BaggageReturn[i].number_of_units > 1) {
                        strFeeDisplayText = objJson[0].BaggageReturn[i].number_of_units + objLanguage.default_value_9 + " " + strCurrencySign + " " + Math.round(objJson[0].BaggageReturn[i].total_amount_incl);
                    }
                    else {
                        strFeeDisplayText = objJson[0].BaggageReturn[i].number_of_units + objLanguage.default_value_8 + " " + strCurrencySign + " " + Math.round(objJson[0].BaggageReturn[i].total_amount_incl);
                    }

                    if (objJson[0].BaggageReturn[i].Selected == true) {
                        opt.selected = true;
                    }

                    opt.value = objJson[0].BaggageReturn[i].number_of_units;
                    opt.innerHTML = strFeeDisplayText;
                    objReturn.appendChild(opt);
                }

                //Add Event;
                $("#BaggageReturn").unbind();
                $("#BaggageReturn").change(function () {
                    CalculateBaggageFee(true);
                    SetPassengerValue();
                });
            }
        }
    }
    ShowProgressBar(false);
}
function SuccessLoadCob(result) {
    if (result == "{002}") {
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else if (result == "{000}") {
        LoadSecure(true);
    }
    else {
        var objContainer = document.getElementById("dvContainer");

        objContainer.innerHTML = result;

        HideHeaderMenu(true);
        ShowSearchPannel(false);
        ShowProgressBar(false);
        objContainer = null;
    }
}

function LoadCob(bParameter, bookingId) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadCob(bParameter, bookingId, SuccessLoadCob, showError, showTimeOut);
}
function CheckClientLogon() {
    // mb=l
    if (existEachQueryString("cl") && queryStringValue("cl") == "l") {
        return true;
    }
    else {
        return false;
    }
}
function ClientLogon() {
    if (MultipleTab.IsCorrectTab()) {
        var objClientId = document.getElementById('txtClientID');
        var objPassword = document.getElementById('txtPassword');

        if (GetControlValue(objClientId).length == 0 || GetControlValue(objClientId).length == 0) {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_52, "Login Failed"), 0, '');
        }
        else if (IsNumeric(GetControlValue(objClientId)) == true && GetControlValue(objClientId).length != 8) {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_143, "Invalid Client number"), 0, '');
        }
        else if (ValidEmail(GetControlValue(objClientId)) == false && IsNumeric(GetControlValue(objClientId)) == false) {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_94, "Invalid Email"), 0, '');
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.ClientLogon(objClientId.value,
                                                 objPassword.value,
                                                 SuccessClientLogon,
                                                 showError,
                                                 showTimeOut);
        }
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
function SuccessClientLogon(result) {
    if (result.length > 0) {
        if (result == "{000}") {

            if (IsSSecurePage() == true) {
                //Success Login.
                //Load My Booking Information
                ShowProgressBar(true);
                //Load My Profile
                tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_Edit, showError, showTimeOut);

                //Load Client information
                tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);

                //Reload menu.
                LoadMenu();

                //****************************************************************
                //  Used in case of using login dialog to close dialog when login
                //  success.
                //****************************************************************
                // ShowProgressBar(false);
                var objMessage = document.getElementById("dvFormHolder");

                //Insert passenger form content.
                objMessage.innerHTML = "";
                objMessage.style.display = "none";
                objMessage = null;
            }
            else {
                LoadSecure(true);
            }
        }
        else {
            //Failed Login
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_52, "Login Failed"), 0, '');
        }
    }

}
function LoadClientInformation() {
    var ClientProfileId = document.getElementById("ClientProfileId");
    if (ClientProfileId.value.length > 0) {
        // Load Client information
        tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);
    }
}
function SuccessLoadClientInformation(result) {
    if (result.length > 0) {
        ShowClientLogonMenu(true, result);
        ToolTipColor();
    }
}
function ClientLogonDialog() {
    CloseDialog();

    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.ClientLogon(document.getElementById('txtDialogClientID').value,
        document.getElementById('txtDialogPassword').value, SuccessClientLogon, showError, showTimeOut);

}
function ShowClientLogonMenu(bValue, strValue) {

    var obj = document.getElementById("dvClientInfo");
    var objFFP = document.getElementById("dvPromoCode");
    if (bValue == true) {
        obj.style.display = "block";
        if (strValue)
            obj.innerHTML = strValue;

        if (objFFP != null) {
            objFFP.style.display = "block";
        }
    }
    else {
        obj.style.display = "none";
        obj.innerHTML = "";
        if (objFFP != null) {
            objFFP.style.display = "none";
        }
    }
}
function ClientLogOff() {
    if (MultipleTab.IsCorrectTab()) {
        ShowProgressBar(true);
        //Set Insurance popup
        SetShowInsurance("0");
        tikAeroB2C.WebService.B2cService.ClientLogOff(SuccessClientLogOff, showError, showTimeOut);
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
function SuccessClientLogOff(result) {
    if (result == true) {
        //Clear Cookies
        deleteCookie("coFFP");

        //Hide client menu.
        ShowClientLogonMenu(false, "");

        //Load B2C Menu
        LoadMenu();
        //Load Home page
        loadHome();
        //ShowProgressBar(false);
    }
}
function LoadDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadClientDialog(SuccessLoadClientDialog, showError, showTimeOut);
}

function SuccessLoadClientDialog(result) {

    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    //Insert passenger form content.
    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;

    InitializeWaterMark("txtClientID", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtPassword", objLanguage.default_value_2, "password");
}
function ReadFFPCookies() {
    var strCookies = getCookie("coFFP");
    if (strCookies != null) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ClientLoad(strCookies, SuccessClientLogon, showError, showTimeOut);
    }
}
function ShowClientLogon() {
    tikAeroB2C.WebService.B2cService.ShowClientLogon(SuccessShowClientLogon, showError, showTimeOut);
}
function SuccessShowClientLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}
function LoadMilleageDetail() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadMilleageDetail(SuccessLoadMilleageDetail, showError, showTimeOut);
}
function SuccessLoadMilleageDetail(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;

    DisplayQuoteSummary("", "", "");
    ShowProgressBar(false);
    objContainer = null;
}
//***************************************************
//  Select Passenger Function Section
//***************************************************
function GetClient(position, bGetMyself) {

    if (position == 0) {
        position = iPaxSelectPosition;
    }
    var objLastname = document.getElementById("hdLastname_" + position);
    var objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    var objClientNo = document.getElementById("ctl00_txtClientNumber");

    ShowProgressBar(true);

    if (GetControlValue(objClientNo).length > 0 && bGetMyself == false) {
        tikAeroB2C.WebService.B2cService.GetClient(GetControlValue(objClientNo), "", true, SuccessGetClient, showError, showTimeOut);
    }
    else if (bGetMyself == true && (GetControlValue(objClientProfileId).length > 0 || GetControlValue(objClientProfileId) != "00000000-0000-0000-0000-000000000000")) {

        iPaxSelectPosition = position;
        var objClientNumber = document.getElementById(FindControlName("input", "txtClientNumber"));

        tikAeroB2C.WebService.B2cService.GetClient(GetControlValue(objClientNumber), GetControlValue(objLastname), false, SuccessGetClientMyself, showError, showTimeOut);
        objClientNumber = null;
    }
    else {
        //Field client number is empty.
        //Show error message.
        LockPassengerInput(false);
        ShowProgressBar(false);
        ClearPassengerList();
    }
    objClientNo = null;
    objLastname = null;
    objClientProfileId = null;
}

function SuccessGetClient(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objMessage = document.getElementById("dvFormHolder");
            var objContainer = document.getElementById("dvProgressBar");

            //Insert passenger form content.
            objMessage.innerHTML = result;

            var objCheckBox = document.getElementsByName("chkClient");
            if (objCheckBox.length > 1) {
                //Show Fading.
                objContainer.style.display = "block";
                objMessage.style.display = "block";
                //Get Total Amount of passenger
                document.getElementById("dvTotalPax").innerHTML = 1;
            }
            else {
                //Find only myself.
                objContainer.style.display = "none";
                objMessage.style.display = "none";
                //Default select passenger.
                if (objCheckBox.length > 0)
                { objCheckBox[0].checked = true; }
                GetSelectedClientPosition();
            }
            objCheckBox = null;

            objMessage = null;
        }
    }

}
function SuccessGetClientMyself(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objMessage = document.getElementById("dvFormHolder");
            var objContainer = document.getElementById("dvProgressBar");

            //Insert passenger form content.
            objMessage.innerHTML = result;

            var objCheckBox = document.getElementsByName("chkClient");
            if (objCheckBox.length > 0) {
                //Find only myself.
                objContainer.style.display = "none";
                objMessage.style.display = "none";
                //Default select passenger.
                if (objCheckBox.length > 0)
                { objCheckBox[0].checked = true; }
                GetSelectedClientPosition();
            }

            objCheckBox = null;
            objMessage = null;
        }
    }

}
function TotalPassengerAvailable() {
    var objPax = document.getElementsByName("txtLastname");
    var pCount = 0;

    for (var i = 0; i < objPax.length; i++) {
        if (objPax[i].value.length == 0) {
            pCount++;
        }
    }
    return pCount;
}
function getDestination() {
    var strCtrl = FindControlName("select", "optDestination");
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var objDestination = document.getElementById(strCtrl);
    var strSelectedOrigin = "";

    objDestination.className = "";
    //Add destination from selected origin
    strSelectedOrigin = objOrigin.options[objOrigin.selectedIndex].value;
    objDestination = ClearOptions(objDestination);  //Clear previous value

    //Add watermark option
    var optDefault = document.createElement("option");
    optDefault.value = "";
    optDefault.className = "watermarkOn";
    if (navigator.appName.indexOf('Microsoft') == 0)
    { optDefault.innerText = objLanguage.default_value_7; }
    else
    { optDefault.text = objLanguage.default_value_7; }
    objDestination.appendChild(optDefault);

    //Filled in route information.
    for (var iCount = 0; iCount < arrDestination.length; iCount++) {
        if (arrDestination[iCount].split("|")[2] == strSelectedOrigin.split("|")[0]) {
            //Allowed only the destination that have the same
            //origin as selected origin.
            var opt = document.createElement("option");
            opt.value = arrDestination[iCount].split("|")[2] + "|" +
                        objOrigin.options[objOrigin.selectedIndex].text + "|" +
                        arrDestination[iCount].split("|")[0] + "|" +
                        arrDestination[iCount].split("|")[1] + "|" +
                        arrDestination[iCount].split("|")[3] + "|" +
                        arrDestination[iCount].split("|")[4];

            //Add display text to option
            if (navigator.appName.indexOf('Microsoft') == 0) {
                opt.innerText = arrDestination[iCount].split("|")[1];
            }
            else { opt.text = arrDestination[iCount].split("|")[1]; }

            objDestination.appendChild(opt);
            opt = null;
        }
    }
    $("#" + strCtrl).unbind();
    InitializeWaterMark(strCtrl, objLanguage.default_value_7, "select");

    objOrigin = null;
    objDestination = null;
}
function SearchAvailability(origin, destination) {

    if (MultipleTab.IsCorrectTab()) {

        //Set Insurance popup
        SetShowInsurance("0");

        var objOneWay = document.getElementById("optOneWay");
        var objAdult = document.getElementById("optAdult");
        var objChild = document.getElementById("optChild");
        var objInfant = document.getElementById("optInfant");
        var objBoardingClass = document.getElementById("optBoardingClass");
        var objPromoCode = document.getElementById("txtPromoCode");
        var objYP = document.getElementById("optYP");
        var objSearchTypeDate = document.getElementById("rdSearchTypeDate");

        var isOneWay = objOneWay.checked.toString();
        var iAdult = parseInt(objAdult.options[objAdult.selectedIndex].value);
        var iChild = parseInt(objChild.options[objChild.selectedIndex].value);
        var iInfant = parseInt(objInfant.options[objInfant.selectedIndex].value);
        var strBoardingClass = objBoardingClass.options[objBoardingClass.selectedIndex].value;
        var dtDateFrom = "";
        var dtDateTo = "";
        var strPromoCode = "";
        var iOther = 0;
        var otherType = "";
        var currencyCode = "";
        var strSearchType = "FARE";

        // End Check Ip address for Default Currency 
        if (objYP != null) {
            iOther = parseInt(objYP.options[objYP.selectedIndex].value);
            otherType = "YP";
        }

        if (objPromoCode != null) {
            strPromoCode = objPromoCode.value;
        }

        //get currency from parameter
        if (existEachQueryString("currency")) {
            currencyCode = queryStringValue("currency");
        }

        //Call Availability web service
        if (iInfant > iAdult) {
            ShowMessageBox(objLanguage.Alert_Message_2, 0, '');
        }
        else if ((iAdult + iChild + iInfant) > 9) {
            ShowMessageBox(objLanguage.Alert_Message_53.replace("{NO}", 9), 0, '');
        }
        else if ((iAdult + iChild + iInfant + iOther) == 0) {
            ShowMessageBox(objLanguage.Alert_Message_6, 0, '');
        }
        else {
            ShowProgressBar(true);

            var strOri = "";
            var strDest = "";
            if (origin.length == 0 & destination.length == 0) {
                var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
                var strOrigin = GetSelectedOption(objOrigin);

                var objDestination = document.getElementById(FindControlName("select", "optDestination"));
                var strDestination = GetSelectedOption(objDestination);

                strOri = strDestination.split("|")[0];
                strDest = strDestination.split("|")[2];

                if (strDestination.split("|")[5] != null && strDestination.split("|")[5] != 'undefined' && currencyCode == "") {
                    currencyCode = strDestination.split("|")[5];
                }

                objOrigin = null;
                objDestination = null;
            }
            else {
                strOri = origin;
                strDest = destination;
            }

            if (strOri.length == 0 || strDest.length == 0) {
                ShowMessageBox(objLanguage.Alert_Message_54, 0, '');
            }
            else {
                if (objSearchTypeDate != null && objSearchTypeDate.checked == false) {

                    dtDateFrom = getdatefrom(); // getMonthfrom();
                    dtDateTo = getdateto(); //getMonthto();
                    var todayDate = new Date();

                    tikAeroB2C.WebService.B2cService.GetLowFareFinderMonth(strOri,
                                                                       strDest,
                                                                       dtDateFrom,
                                                                       dtDateTo,
                                                                       isOneWay,
                                                                       iAdult,
                                                                       iChild,
                                                                       iInfant,
                                                                       strBoardingClass,
                                                                       currencyCode,
                                                                       strPromoCode,
                                                                       strSearchType,
                                                                       iOther,
                                                                       otherType,
                                                                       g_strIpAddress,
                                                                       ChangeToDateString(todayDate),
                                                                       SuccessGetLowFareFinder,
                                                                       showError,
                                                                       getdatefrom() + "|" + getdateto());
                }
                else {

                    dtDateFrom = getdatefrom();
                    dtDateTo = getdateto();
                    tikAeroB2C.WebService.B2cService.GetAvailability(strOri,
                                                                 strDest,
                                                                 dtDateFrom,
                                                                 dtDateTo,
                                                                 isOneWay,
                                                                 "0",
                                                                 iAdult,
                                                                 iChild,
                                                                 iInfant,
                                                                 strBoardingClass,
                                                                 currencyCode,
                                                                 strPromoCode,
                                                                 strSearchType,
                                                                 iOther,
                                                                 otherType,
                                                                 g_strIpAddress,
                                                                 SuccessSearchFlight,
                                                                 showError,
                                                                 showTimeOut);
                }
            }
        }

        objOrigin = null;
        objDestination = null;
        objOneWay = null;
        objAdult = null;
        objChild = null;
        objInfant = null;
        objBookingClass = null;
        objPromoCode = null;
        return true;
    }
    else {
        MultipleTab.Redirect();
        return false;
    }

}
function BackToAvailability() {

    if (MultipleTab.IsCorrectTab()) {

        ShowProgressBar(true);
        //Set Insurance popup
        SetShowInsurance("0");
        //SuccessSearchFlight() is in the Availability.js
        tikAeroB2C.WebService.B2cService.AvailabilityGetSession(SuccessSearchFlight, showError, showTimeOut);
    }
    else {
        MultipleTab.Redirect();
    }
}

function GetPassengerDetail() {

    if (MultipleTab.IsCorrectTab()) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.GetPassengerDetail(SuccessGetPassengerDetail, showError, showTimeOut);
    }
    else {
        window.location = "MultiTabAlert.aspx";
    }

}

function SuccessGetPassengerDetail(result) {

    if (result.length > 0) {
        var obj = document.getElementById("dvContainer");
        if (result == "{002}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else if (result != "{001}") {
            obj.innerHTML = result;
            SetPassengerDetail();

            RemoveWellNetFeeDisplay();
            //Display quote information
            GetSessionQuoteSummary();
            ShowProgressBar(false);
            obj = null;
            ToolTipColor();

        }
        else {
            LoadSecure(false);
        }
    }
    else {
        ShowProgressBar(false);
    }

}

function LoadContactTitle() {
    var objSelect = document.getElementById("stTitle_1");
    var objContactTitle = document.getElementById("stContactTitle");

    //Populate contact detail title.
    for (var jCount = 0; jCount < objSelect.length; jCount++) {
        objContactTitle.options[objContactTitle.options.length] = new Option(objSelect.options[jCount].text, objSelect.options[jCount].value);
    }
    objSelect = null;
    objContactTitle = null;
}

function LoadContactCountry() {
    var objSelect = document.getElementById("stNational_1");
    var objContactCountry = document.getElementById("stContactCountry");

    //Populate contact national.
    for (var jCount = 0; jCount < objSelect.length; jCount++) {
        objContactCountry.options[objContactCountry.options.length] = new Option(objSelect.options[jCount].text, objSelect.options[jCount].value);
    }
    objSelect = null;
    objContactCountry = null;
}

function GetContactInformationFromCookies() {
    var objCookies = getCookie("coContact");
    var objChk = document.getElementById("chkRemember");

    if (objCookies != null) {

        var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
        var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
        var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
        var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
        var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
        var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
        var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
        var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
        var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
        var objCity = document.getElementById(FindControlName("input", "txtContactCity"));

        var objTaxId = document.getElementById(FindControlName("input", "txtTIN"));
        var objInvoiceReceiver = document.getElementById(FindControlName("input", "txtInvoiceReceiver"));
        var txtEUVat = document.getElementById(FindControlName("input", "txtEUVat"));
        var txtCIN = document.getElementById(FindControlName("input", "txtCIN"));
        var objOptionalEmail = document.getElementById(FindControlName("input", "txtContactEmail2"));
        var objMobileEmail = document.getElementById(FindControlName("input", "txtMobileEmail"));
        var objState = document.getElementById(FindControlName("input", "txtContactState"));

        var strResult = $.base64.decode(objCookies);
        if (strResult.length > 0 && objFirstName.value.length == 0) {

            var strContactInfo = strResult.split("{}");
            if (strContactInfo.length == 20) {
                objChk.checked = true;

                //Get passenger contact information.
                SetComboValue(FindControlName("select", "stContactTitle"), strContactInfo[0]);
                objFirstName.value = strContactInfo[1];
                objLastName.value = strContactInfo[2];
                objMobile.value = strContactInfo[3];
                objHome.value = strContactInfo[4];
                objBusiness.value = strContactInfo[5];
                objEmail.value = strContactInfo[6];
                objAddress1.value = strContactInfo[7];
                objAddress2.value = strContactInfo[8];
                objZip.value = strContactInfo[9];
                objCity.value = strContactInfo[10];
                SetComboValue(FindControlName("select", "stContactCountry"), strContactInfo[11]);

                if (objTaxId != null) {
                    objTaxId.value = strContactInfo[13];
                }
                if (objInvoiceReceiver != null) {
                    objInvoiceReceiver.value = strContactInfo[14];
                }
                if (txtEUVat != null) {
                    txtEUVat.value = strContactInfo[15];
                }
                if (txtCIN != null) {
                    txtCIN.value = strContactInfo[16];
                }
                if (objOptionalEmail != null) {
                    objOptionalEmail.value = strContactInfo[17];
                }
                if (objMobileEmail != null) {
                    objMobileEmail.value = strContactInfo[18];
                }
                objState.value = strContactInfo[19];
            }
        }
    }
    else {
        objChk.checked = false;
    }

    objCookies = null;
    objChk = null;
}

function ShowFlightSummary() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ShowFlightSummary(SuccessShowFlightSummary, showError, showTimeOut);
}
function SuccessShowFlightSummary(result) {
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    obj = null;
    scroll(0, 0);
    ShowProgressBar(false);
}
//*********************************************************
//  This file is used for global setup in javascript
//*********************************************************

//***************************************************
// Set MaskEdit for date format.
var strDateFormat = "9999/99/99";

//***************************************************
//  Used for setting phone number validation format
var strPhoneNumberFormat = "^[0-9]([0-9\\_\\#\\-]+)$";

//***************************************************
//  Cookies Timeout for multiple B2c AgencyCode.
var iAgencyCodeExpDays = 5;

//***************************************************
// Set Age range for date of birth check.
//var min_adult = 12 * 12;
//var min_child = 24;
//var max_age = 120;
// Set Age range for date of birth check.
var min_adult = 12;
var min_child = 2;
var max_age = 120;
var min_inf = 8; // day


//**************************************************
// Set Wellnet Fee Code.  && Voucher
var gWellNetFeeCode = "CONV";
var VoucherFeeCode = "VOUCHER";
var DIWFeeCode = "YW";
//***************************************************
//  Cookies Expiry day.
var iCookiesExpDays = 7;
// GetIpAddress URL
//www.tiksystems.com/getip.aspx?callback=?
var gIpAddressUrl = "119.11.156.11/GetIP.aspx?callback=?";
//Set the number if infant limit per seat row.
var gTotalRowInfantLimit = 1;


// Start Search Flight Availability from Query string
function GenerateFromQueryString() {
    if (window.location.search.substring(1) != "") {
        // Check My Booking First
        if (checkMyBookingString()) {
            LoadCob(false, '');
        }
        else if (CheckClientLogon()) {
            LoadDialog();
        }
        else if (CheckRegisterLogon()) {
            LoadRegistration();
        }
        else if (existEachQueryString("sn")) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.LoadManageBooking(queryStringValue("sn"), SuccessLoadCob, showError, showTimeOut);
        }
        else {
            RefreshSetting();
        }
    }
    else {
        RefreshSetting();
    }
}

function IsJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
//multipletab
function SetBookingId(id) {
    window.localStorage.setItem("bookingid", id);
}

function GetBookingId() {
    try {
        if (window.localStorage) return window.localStorage.getItem("bookingid");
    }
    catch (e) {
        return undefined;
    }
}

function SetStep(step) {
    sessionStorage.setItem("currentstep", step);
    window.name = step;
}

function GetStep() {
    if (window.name == sessionStorage.getItem("currentstep"))
        return sessionStorage.getItem("currentstep");
    else
        return window.name;
}

function IsMultipleTab(clientStep, serverStep) {
    if (clientStep == serverStep) {
        return false;
    }
    else {
        MultiTabAlert();
    }
}
function MultiTabAlert() {
    var tmpLangcode = "en";
    if (LanguageCode.split("-").length == 2)
        tmpLangcode = LanguageCode.split("-")[0];
    window.open("http://sorrypage.flypeach.com/" + tmpLangcode + "/multi_tab_alert.html", "_self");
}

function IsMultipleTabBookId() {
    var booking_id = document.getElementById("hdBooking_id");
    var id = GetBookingId();
    var found = false;
    if (id == null || id == "") {
        SetBookingId(booking_id);
        found = true;
    }
    else {
        found = false;
        MultiTabAlert();
    }
}

function IsMultipleTabOpen(objJSON) {
    return false;
}
function GetTabNumber() {
    //Check unmber of tab open from cookies
    var iNumberOfCurrentTab = getCookie("cGetTab");

    var dtExpired = new Date();
    dtExpired.setDate(dtExpired.getDate() + 1);

    if (iNumberOfCurrentTab != null && iNumberOfCurrentTab.length > 0) {

        iNumberOfCurrentTab = ++iNumberOfCurrentTab;
    }
    else {

        iNumberOfCurrentTab = 1;
    }

    //Set current tab open
    setCookie("cGetTab", iNumberOfCurrentTab, dtExpired, "", "", false);
    return iNumberOfCurrentTab;
}

function ClarNumberOfTab() {
    //Check unmber of tab open from cookies
    var iNumberOfCurrentTab = getCookie("cGetTab");

    if (iNumberOfCurrentTab != null && iNumberOfCurrentTab.length > 0) {

        if (parseInt(iNumberOfCurrentTab) == 1) {
            //Delete tab count if only one tab is open.
            deleteCookie("cGetTab");
        }
        else {
            iNumberOfCurrentTab = --iNumberOfCurrentTab;
            //Set current tab open
            var dtExpired = new Date();
            dtExpired.setDate(dtExpired.getDate() + 1);
            setCookie("cGetTab", iNumberOfCurrentTab, dtExpired, "", "", false);
        }
    }
}
function LoadMainPage() {
    ShowProgressBar(true);
    CloseSession();
    tikAeroB2C.WebService.B2cService.ClearSession(SuccessLoadMainPage, showError, showTimeOut);
}
function SuccessLoadMainPage(result) {
    window.location.href = "http://www.flypeach.com/";
    ShowProgressBar(false);
}

var state = "";
function ACEInsuranceRequestQuote(message) {
    var show_insurance_on_web_flag = document.getElementById("show_insurance_on_web_flag");
    // valid  setting before call ACE
    var Origin_route = document.getElementById("hdOrigin_route");
    var validLang = false;
    state = message;

    if (Origin_route) {
        // for route hk
        if (Origin_route.value == "HKG") {
            if ($('#hdLang').val().toLowerCase() == "en-us" || $('#hdLang').val().toLowerCase() == "zh-hk") {
                // for 18 july
                validLang = true;
            }
        }
            // for other route
        else {
            if ($('#hdLang').val().toLowerCase() == "en-us" || $('#hdLang').val().toLowerCase() == "ja-jp") {
                validLang = true;
            }
        }
    }

    // call ACE
    if (show_insurance_on_web_flag && show_insurance_on_web_flag.value == "1" && validLang == true) {

        var objRefresh = document.getElementById("dvInsuranceReferesh");
        var objInsDesc = document.getElementById("dvInsuranceDetail");
        var objInsProgressBar = document.getElementById("dvInsuranceProgressBar");
        var objInsuContend = document.getElementById("dvInsuranceContent");

        objInsuContend.style.display = "block";
        objInsProgressBar.style.display = "block";
        objRefresh.style.display = "none";
        objInsDesc.style.display = "none";

        var todayDate = new Date();
        //ShowProgressBar(true);

        $.ajax({
            type: "POST",
            url: "WebService/B2cService.asmx/ACEQuoteRequest",
            data: "{'dtDateTime':'" + todayDate.getFullYear() + "-" + (todayDate.getMonth() + 1) + " - " + todayDate.getDate() + "   " + todayDate.getHours() + "  :  " + todayDate.getMinutes() + " :  " + todayDate.getSeconds() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {

                if (result.length > 0) {
                    SuccessACEInsuranceRequestQuote(result);
                }

            },
            timeout: 50000, //50 second timeout
            error: function (result) {
            }
        });

    }
}

function SuccessACEInsuranceRequestQuote(result) {
    var objRefresh = document.getElementById("dvInsuranceReferesh");
    var objInsDesc = document.getElementById("dvInsuranceDetail");
    var objInsDescHK = document.getElementById("dvInsuranceDetailHK");
    var objInsProgressBar = document.getElementById("dvInsuranceProgressBar");
    var objErrorMsg = document.getElementById("dvInsuErrorMessage");
    var objInsuContend = document.getElementById("dvInsuranceContent");

    var objInsureDetailDomes = document.getElementById("dvInsureDetailDomes");
    var objInsureDetailInter = document.getElementById("dvInsureDetailInter");

    var objDiscloseDomes = document.getElementById("dvDiscloseDomes");
    var objDiscloseInter = document.getElementById("olDiscloseInter");

    var objliLiabilityDTA = document.getElementById("liLiabilityDTA");
    var objliLiabilityOTA = document.getElementById("liLiabilityOTA");

    var objInsuCoversDTA = document.getElementById("dvInsuranceCoversDomes");
    var objInsuCoversOTA = document.getElementById("dvInsuranceCoversInter");

    //HTML Document information.
    var objHtmlHeader = document.getElementById("ifHeadDocument");
    var objHtmlDocument = document.getElementById("ifDocument");

    var InsurancePopup = document.getElementById("InsurancePopup");

    if (result.length > 0) {
        var objJson = eval("(" + result + ")");
        var objYes = document.getElementById("optGetInsurance");
        var objNo = document.getElementById("optNoInsurance");
        var objYesHK = document.getElementById("optGetInsuranceHK");
        var objNoHK = document.getElementById("optNoInsuranceHK");

        if (objJson.ErrorCode == "400" || objJson.ErrorCode == "401" || objJson.ErrorCode == "004" || objJson.ErrorCode == "404") {
            objInsuContend.style.display = "none";
            objRefresh.style.display = "block";
            objInsDesc.style.display = "none";
            objErrorMsg.style.display = "block";
            objErrorMsg.innerHTML = objJson.Message;
            objNo.checked = true;

            if (state != "refresh" && objJson.FeeFound == true) {
                tikAeroB2C.WebService.B2cService.CalculateInsuranceFee(true, false, SuccessCalculateInsuranceFee, showErrorPayment, showTimeOutPayment);
            }
        }
        else if (objJson.ErrorCode == "402" || objJson.ErrorCode == "403") {
            objInsuContend.style.display = "none";
            objRefresh.style.display = "none";
            objInsDesc.style.display = "none";
            objErrorMsg.style.display = "block";
            objNo.checked = true;
            //Insurance Allowed only with return flight.
            objErrorMsg.innerHTML = objLanguage.Alert_Message_141;

            if (state != "refresh" && objJson.FeeFound == true) {
                tikAeroB2C.WebService.B2cService.CalculateInsuranceFee(true, false, SuccessCalculateInsuranceFee, showErrorPayment, showTimeOutPayment);
            }
        }
        else {
            objRefresh.style.display = "none";
            //objInsDesc.style.display = "block";
            objErrorMsg.style.display = "none";
            objErrorMsg.innerHTML = "";

            var objOneWay = document.getElementById("hdOneWay");
            var isOneWay = "true";
            if (objOneWay != null)
                isOneWay = objOneWay.value;

            if (GetShowInsurance() == "0" || GetShowInsurance() == null) {
                if (Origin_route.value != "HKG" && isOneWay == "false") {
                    SetShowInsurance("1");
                    ShowHidePopUpBox(InsurancePopup, true);
                }
            }

            var objQuote = document.getElementById("dvInsAmount");
            var objCurrency = document.getElementById("dvInsCurrency");


            //Set purpose value.
            var objDvPurpose = document.getElementById("dvPurpose");
            var objConfirmDetail = document.getElementById("dvInsuranceConfirmDetail");
            var objConfirmDetailHK = document.getElementById("dvInsuranceConfirmDetailHK");

            if (objDvPurpose != null) {
                // for japan
                if (objJson.FlightType == "OTA" && objJson.Origin != "HKG") {

                    // show content JP
                    objInsDesc.style.display = "block";
                    objConfirmDetail.style.display = "block";

                    if (objliLiabilityOTA != null) {
                        objliLiabilityOTA.style.display = "block";
                    }

                    if (objliLiabilityDTA != null) {
                        objliLiabilityDTA.style.display = "none";
                    }

                    objInsureDetailDomes.style.display = "none";
                    objInsureDetailInter.style.display = "block";

                    objDiscloseDomes.style.display = "none";
                    objDiscloseInter.style.display = "block";

                    objInsuCoversDTA.style.display = "none";
                    objInsuCoversOTA.style.display = "block";

                    objDvPurpose.style.display = "block";

                    objHtmlHeader.src = CreateHtmlDocumentLink(objHtmlHeader, "headwordinginternational.html");
                    objHtmlDocument.src = CreateHtmlDocumentLink(objHtmlDocument, "boxwordinginternational.html");

                    // show fee inter not HK
                    if (objQuote != null) {
                        if (objJson.FlightType == "OTA") {
                            objQuote.innerHTML = numberFormat(objJson.Message, 0);
                        }
                        else {
                            objQuote.innerHTML = numberFormat(Math.round(objJson.Message), 0);
                        }
                    }
                    if (objCurrency != null) {
                        objCurrency.innerHTML = "&#165;"; //objJson.CurrencyCode;
                    }
                }
                    // for hong kong
                else if (objJson.FlightType == "OTA" && objJson.Origin == "HKG") {
                    var objQuoteHK = document.getElementById("dvInsAmountHK");
                    var objCurrencyHK = document.getElementById("dvInsCurrencyHK");

                    objInsDescHK.style.display = "block";
                    objConfirmDetailHK.style.display = "block";

                    // if route = Hong Kong show cc default
                    if (state != "refresh") {
                        if (objYesHK != null) {
                            objYesHK.checked = true;
                        }
                    }

                    InsuranceShowHideFormOfPayment(true);

                    if (state != "refresh") {
                        CalculateInsuranceFee('optGetInsuranceHK', 'optNoInsuranceHK', 'dvInsuranceConfirmDetailHK');
                    }

                    // show currency
                    if (objQuoteHK != null) {
                        objQuoteHK.innerHTML = numberFormat(objJson.Message, 0);
                    }
                    if (objCurrencyHK != null) {
                        objCurrencyHK.innerHTML = "HK&#36;"; //objJson.CurrencyCode;
                    }
                }
                else {
                    // show JP content
                    objInsDesc.style.display = "block";
                    objConfirmDetail.style.display = "block";

                    if (objliLiabilityOTA != null) {
                        objliLiabilityOTA.style.display = "none";
                    }

                    if (objliLiabilityDTA != null) {
                        objliLiabilityDTA.style.display = "block";
                    }

                    objInsureDetailDomes.style.display = "block";
                    objInsureDetailInter.style.display = "none";

                    objDiscloseDomes.style.display = "block";
                    objDiscloseInter.style.display = "none";

                    objInsuCoversDTA.style.display = "block";
                    objInsuCoversOTA.style.display = "none";

                    objDvPurpose.style.display = "none";

                    if (objQuote != null) {
                        if (objJson.FlightType == "OTA") {
                            objQuote.innerHTML = numberFormat(objJson.Message, 0);
                        }
                        else {
                            objQuote.innerHTML = numberFormat(Math.round(objJson.Message), 0);
                        }
                    }
                    if (objCurrency != null) {
                        objCurrency.innerHTML = "&#165;"; //objJson.CurrencyCode;
                    }


                }

            }


            //If insurance fee found in the booing select yes.

            if (objJson.FeeFound == true) {
                if (objJson.Origin != "HKG") {
                    if (objYes != null) {
                        objYes.checked = true;
                    }
                }
                else {
                    if (objYesHK != null) {
                        objYesHK.checked = true;
                    }
                }

                if (objConfirmDetail != null) {
                    objConfirmDetail.style.display = "block";
                }
                //Show only Credit card.
                InsuranceShowHideFormOfPayment(true);
            }
            else {
                // if route = Hong Kong
                if (objJson.Origin == "HKG") {
                    // refresh no fee  keep no
                    if (state == "refresh" && objNoHK != null) {
                        objNoHK.checked = true;
                    }

                    if (objConfirmDetail != null) {
                        objConfirmDetail.style.display = "none";
                    }
                }
                else {
                    if (objYes != null) {
                        objYes.checked = true;
                        objYes.onclick();
                    }
                    //
                    //if (objConfirmDetail != null) {
                    //    objConfirmDetail.style.display = "none";
                    //}

                    //InsuranceShowHideFormOfPayment(false);
                }
                // end HK

            }

        }

    }
    else {
        objInsDesc.style.display = "none";
        objRefresh.style.display = "block";
        objErrorMsg.style.display = "none";
        objErrorMsg.innerHTML = "";
    }
    objInsProgressBar.style.display = "none";
}

function numberFormat(nStr, fix, prefix) {
    var prefix = prefix || '';
    nStr += '';
    nStr = parseFloat(nStr).toFixed(fix);
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';

    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    return prefix + x1 + x2;
}

function CalculateInsuranceFee(optGetInsurance, optNoInsurance, dvInsuranceConfirmDetail) {
    //ShowProgressBar(true);
    Origin_route = document.getElementById("hdOrigin_route");
    var objChkYes;
    var objChkNo;
    var objConfirmDetail;

    if (optGetInsurance == null || optGetInsurance == 'undefined') {
        if (Origin_route.value == "HKG") {
            objChkYes = document.getElementById("optGetInsuranceHK");
            objChkNo = document.getElementById("optNoInsuranceHK");
            objConfirmDetail = document.getElementById("dvInsuranceConfirmDetailHK");
        }
        else {
            objChkYes = document.getElementById("optGetInsurance");
            objChkNo = document.getElementById("optNoInsurance");
            objConfirmDetail = document.getElementById("dvInsuranceConfirmDetail");
        }
    }
    else {
        objChkYes = document.getElementById(optGetInsurance);
        objChkNo = document.getElementById(optNoInsurance);
        objConfirmDetail = document.getElementById(dvInsuranceConfirmDetail);
    }

    if (objChkYes != null && objChkNo != null && objConfirmDetail != null) {
        objChkYes.setAttribute('disabled', true);
        objChkNo.setAttribute('disabled', true);

        var txtPostCode = document.getElementById('txtPostCode')
        if (txtPostCode != null) {
            setTimeout(function () {
                txtPostCode.focus()
            }, 10);
        }

        if (objChkYes.checked == true) {
            if (document.getElementById('CreditCard').style.display == 'none') {
                showhidelayer('CreditCard');
                activateCreditCardControl();
            }
            //When select buy insurance have to default at Credit card payment.
            InsuranceShowHideFormOfPayment(true);

            //Show insurance detail.
            objConfirmDetail.style.display = "block";
            tikAeroB2C.WebService.B2cService.CalculateInsuranceFee(true, true, SuccessCalculateInsuranceFee, showErrorPayment, showTimeOutPayment);
        }
        else if (objChkNo.checked == true) {
            InsuranceShowHideFormOfPayment(false);

            //Hide insurance detail.
            objConfirmDetail.style.display = "none";
            tikAeroB2C.WebService.B2cService.CalculateInsuranceFee(true, false, SuccessCalculateInsuranceFee, showErrorPayment, showTimeOutPayment);
        }
    }
}

function SuccessCalculateInsuranceFee(result) {
    if (result.length > 0) {

        var objBookNow = document.getElementById('dvcTLater');
        var objCardNumber = document.getElementById("txtCardNumber");

        SuccessGetSessionQuoteSummary(result);
        //DisplayQuoteSummary(result, "", "");

        //Initialize Summary collapse.
        //InitializeFareSummaryCollapse("Outward");
        //InitializeFareSummaryCollapse("Return");

        //Initialize Total Additonal collapse
        //$(function () {
        //    $("#ulAdditionalFee").jqcollapse({
        //        slide: true,
        //        speed: 400,
        //        easing: ''
        //    });
        //});

        //if (objBookNow != null) {
        //    if (objBookNow.className == "BookNowPayLater") {
        //        showhidelayer('BookNow');
        //    }
        //}

        //Check Credit card fee incause cc number is not clear.
        if (objCardNumber.value.length >= 6)
            validateCC(objCardNumber.value, true);

        var chkAcceptDisclose = document.getElementById("chkAcceptDisclose");
        if (chkAcceptDisclose != null)
            chkAcceptDisclose.checked = false;
    }
    var objChkYes = document.getElementById('optGetInsurance');
    var objChkNo = document.getElementById('optNoInsurance');
    var objYesHK = document.getElementById("optGetInsuranceHK");
    var objNoHK = document.getElementById("optNoInsuranceHK");

    if (objChkYes != null)
        objChkYes.removeAttribute('disabled');
    if (objChkNo != null)
        objChkNo.removeAttribute('disabled');

    if (objYesHK != null)
        objYesHK.removeAttribute('disabled');
    if (objNoHK != null)
        objNoHK.removeAttribute('disabled');

    if (document.getElementById("dvDIWButton") != null && document.getElementById("dvDIWButton").style.display == "block")
        ShowYahooFee();
    //ShowProgressBar(false);
}

function InsuranceShowHideFormOfPayment(bShowCC) {

    if (bShowCC == true) {
        //showhidelayer('CreditCard');
        if (document.getElementById('dvBookNowTab') != null) {
            document.getElementById('BookNow').style.display = 'none';
        }

        if (document.getElementById('dvcTVoucher') != null) {
            document.getElementById('Voucher').style.display = 'none';
        }
    }
    else {
        if (document.getElementById('dvBookNowTab') != null) {
            document.getElementById('dvBookNowTab').style.display = 'block';
        }

        if (document.getElementById('dvcTVoucher') != null) {
            document.getElementById('dvcTVoucher').style.display = 'block';
        }
    }

}
function CreateHtmlDocumentLink(objIFrame, strFileName) {
    var strPath = "";
    if (objIFrame != null) {
        var arrPath = objIFrame.src.split("/");
        if (arrPath != null && arrPath.length > 0) {
            for (var i = 0; i < arrPath.length - 1; i++) {
                strPath = strPath + arrPath[i] + "/";
            }
        }
    }
    return strPath + strFileName;
}

function ShowHidePopUpBox(objBox, isShow) {
    var obj = document.getElementById("dvDisableInsurance");
    if (isShow) {
        obj.style.display = "block";
        objBox.style.display = "block";
    }
    else {
        obj.style.display = "none";
        objBox.style.display = "none";
    }

    obj = null;
    objBox = null;
}

function ClosePopupInsurance() {
    var InsurancePopup = document.getElementById("InsurancePopup");
    ShowHidePopUpBox(InsurancePopup, false);
}

function SetShowInsurance(value) {
    sessionStorage.setItem("isShow", value);
}

function GetShowInsurance() {
    return sessionStorage.getItem("isShow");
}

function ShowInsuranceProgressBar(bValue) {
    var obj = document.getElementById("dvDisableInsurance");
    var objLoader = document.getElementById("dvLoad");
    if (obj != null) {
        document.getElementById("dvLoadBar").style.display = "none";

        if (bValue == true) {
            objLoader.style.display = "block";
            obj.style.display = "block";
        }
        else {
            if (document.getElementById("dvMessageBox").style.display == "none" || document.getElementById("dvMessageBox").style.display == "") {
                var objFormHolder = document.getElementById("dvFormHolder");
                if (objFormHolder != null && (objFormHolder.style.display == "none" || objFormHolder.style.display == "")) {
                    obj.style.display = "none";
                }
            }
            objLoader.style.display = "none";
        }
        obj = null;
        objLoader = null;
    }
}
//*--------------------------------------------------------
//*** AJAX functional call.
function GetLowFareFinder() {
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    var arrAirport = objDestination.options[objDestination.selectedIndex].value.split("|");

    var strOrigin = arrAirport[0];
    var strDestination = arrAirport[2];
    var objOneWay = document.getElementById("optOneWay");

    var dtDateFrom = getdatefrom();
    var dtDateTo = 0;

    if (objOneWay.checked == false)
    { dtDateTo = getdateto(); }

    //Start Check Minimum Date that can select low fare finder.
    var d = Number(dtDateFrom.toString().substring(6, 8));
    var m = Number(dtDateFrom.toString().substring(4, 6));
    var y = Number(dtDateFrom.toString().substring(0, 4));

    var iDate = new Date(y, m - 1, d);

    var todayDate = new Date();
    var cDate = new Date(todayDate.getYear(), todayDate.getMonth(), todayDate.getDate());
    //End Check Minimum Date that can select low fare finder.

    if (dtDateTo != 0 && (dtDateFrom > dtDateTo)) {

        ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
    }
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.GetLowFareFinder(strOrigin, strDestination, dtDateFrom, dtDateTo, SuccessGetLowFareFinder, showError, dtDateFrom + "|" + dtDateTo);
    }
    objDestination = null;
}
function GetNextLowFareFinder(searchDate, flightType) {
    ShowProgressBar(true);
    if (flightType == 'Return')
        tikAeroB2C.WebService.B2cService.GetLowFareFinder("", "", "0", searchDate, SuccessGetLowFareFinder, showError, showTimeOut);
    else if (flightType == 'OutWard')
        tikAeroB2C.WebService.B2cService.GetLowFareFinder("", "", searchDate, "0", SuccessGetLowFareFinder, showError, showTimeOut);
}
function SearchLowFareSingleMonth(flightDate, strFlightType) {

    if (strFlightType == "Outward") {

        //setFromDate(flightDate);
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SearchLowFareSingleMonth(flightDate, true, SuccessGetLowFareFinder, showError, getdatefrom() + "|" + getdateto());
    }
    else {

        //setToDate(flightDate);
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SearchLowFareSingleMonth(flightDate, false, SuccessGetLowFareFinder, showError, getdatefrom() + "|" + getdateto());
    }
}
function SuccessGetLowFareFinder(result, strSelectDate) {
    if (result.length > 0) {
        if (result == "{102}") {
            //Invalid Required Parameter.
            ShowMessageBox(objLanguage.Alert_Message_55, 0, '');
        }
        else if (result == "{103}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else if (result == "{002}") {
            loadHome();
        }
        else {
            var obj = document.getElementById("dvContainer");
            obj.innerHTML = result;
            var arrDate = strSelectDate.split("|");
            var objOutWare = document.getElementsByName("optLowfare_Outward");
            var objReturn = document.getElementsByName("optLowfare_Return");

            var todayDate = new Date();
            var cDate = new Date(todayDate.getYear(), todayDate.getMonth(), todayDate.getDate());

            var strSelectDate;
            var d = Number(strSelectDate.substring(6, 8));
            var m = Number(strSelectDate.substring(4, 6));
            var y = Number(strSelectDate.substring(0, 4));

            var iDate;
            var objJSON;

            DisplayQuoteSummary("", "", "");
            if (objOutWare != null && objOutWare.length > 0) {
                for (var i = 0; i < objOutWare.length; i++) {
                    objJSON = eval("(" + objOutWare[i].value + ")");
                    strSelectDate = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                    var d = Number(strSelectDate.substring(6, 8));
                    var m = Number(strSelectDate.substring(4, 6));
                    var y = Number(strSelectDate.substring(0, 4));

                    iDate = new Date(y, m - 1, d);
                    //Remove date at are not match the 21 days
                    if (dateDiff("d", cDate, iDate) <= 21) {
                        objOutWare[i].style.display = "none";
                    }
                    //Select at selected date
                    if (strSelectDate == arrDate[0]) {
                        objOutWare[i].checked = true;
                    }
                    iDate = null;
                }
                //Highlight Selected date
                LffHighLight("Outward", true);
                GetSelectLowFareSummary("Outward");
            }

            if (objReturn != null && objReturn.length > 0) {
                for (var i = 0; i < objReturn.length; i++) {
                    objJSON = eval("(" + objReturn[i].value + ")");
                    strSelectDate = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                    var d = Number(strSelectDate.substring(6, 8));
                    var m = Number(strSelectDate.substring(4, 6));
                    var y = Number(strSelectDate.substring(0, 4));

                    iDate = new Date(y, m - 1, d);
                    //Remove date at are not match the 21 days
                    if (dateDiff("d", cDate, iDate) <= 21) {
                        objReturn[i].style.display = "none";
                    }
                    //Select at selected date
                    if (strSelectDate == arrDate[1]) {
                        objReturn[i].checked = true;
                    }
                }
                //Highlight Selected date
                LffHighLight("Return", true);
                GetSelectLowFareSummary("Return");
            }
            ShowSearchPannel(true);
            objOutWare = null;
            objReturn = null;
            ShowProgressBar(false);
        }
    }
}

function GetLowFareFinderSummary(strFligtType, objFlightParam) {
    switch (strFligtType) {
        case "Outward":
            tmpOutFare_id = objFlightParam.fare_id.replace('{', '');
            tmpOutFare_id = tmpOutFare_id.replace('}', '');
            tmpOutFlight_id = objFlightParam.flight_id.replace('{', '');
            tmpOutFlight_id = tmpOutFlight_id.replace('}', '');
            break;
        case "Return":
            tmpRetFare_id = objFlightParam.fare_id.replace('{', '');
            tmpRetFare_id = tmpRetFare_id.replace('}', '');
            tmpRetFlight_id = objFlightParam.flight_id.replace('{', '');
            tmpRetFlight_id = tmpRetFlight_id.replace('}', '');
            break;
    }

    var strXml = "<flights>" +
                         "<flight>" +
                            "<airline_rcd>" + objFlightParam.airline_rcd + "</airline_rcd>" +
                            "<flight_number>" + objFlightParam.flight_number + "</flight_number>" +
                            "<flight_id>" + objFlightParam.flight_id + "</flight_id>" +
                            "<origin_rcd>" + objFlightParam.origin_rcd + "</origin_rcd>" +
                            "<destination_rcd>" + objFlightParam.destination_rcd + "</destination_rcd>" +
                            "<booking_class_rcd>" + objFlightParam.booking_class_rcd + "</booking_class_rcd>" +
                            "<fare_id>" + objFlightParam.fare_id + "</fare_id>" +
                            "<transit_flight_id>" + objFlightParam.transit_flight_id + "</transit_flight_id>" +
                            "<departure_date>" + objFlightParam.departure_date + "</departure_date>" +
                            "<arrival_date>" + objFlightParam.arrival_date + "</arrival_date>" +
                            "<transit_departure_date>" + objFlightParam.transit_departure_date + "</transit_departure_date>" +
                            "<transit_arrival_date>" + objFlightParam.transit_arrival_date + "</transit_arrival_date>" +
                            "<transit_airport_rcd>" + objFlightParam.transit_airport_rcd + "</transit_airport_rcd>" +
                            "<planned_departure_time>" + objFlightParam.planned_departure_time + "</planned_departure_time>" +
                            "<planned_arrival_time>" + objFlightParam.planned_arrival_time + "</planned_arrival_time>" +
                              "<transit_fare_id>" + objFlightParam.transit_fare_id + "</transit_fare_id>" +
                             "<transit_airline_rcd>" + objFlightParam.transit_airline_rcd + "</transit_airline_rcd>" +
                            "<transit_flight_number>" + objFlightParam.transit_flight_number + "</transit_flight_number>" +
                            "<transit_arrival_date>" + objFlightParam.transit_arrival_date + "</transit_arrival_date>" +
                            "<transit_planned_departure_time>" + objFlightParam.transit_planned_departure_time + "</transit_planned_departure_time>" +
                            "<transit_planned_arrival_time>" + objFlightParam.transit_planned_arrival_time + "</transit_planned_arrival_time>" +
                        "</flight>" +
                    "</flights>";    //Call Webservice
    tikAeroB2C.WebService.B2cService.GetQuoteSummary(strXml,
                                                     strFligtType,
                                                     SuccessGetLowFareFinderSummary,
                                                     showError,
                                                     strFligtType);
}

function SuccessGetLowFareFinderSummary(result, strType) {

    if (result == "{100}" || result == "{101}" || result == "{004}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
    }
    else {

        if (strType == "Outward") {
            DisplayQuoteSummary("", result, "");
        }
        else {
            DisplayQuoteSummary("", "", result);
        }
        //Initialize Collape
        InitializeFareSummaryCollapse(strType);
    }

}

//*----------------------------------------------------
//** Non AJAX functional call.
function GetSelectLowFareSummary(strFlightType) {
    var objOpt = document.getElementsByName("optLowfare_" + strFlightType);
    var objHd = document.getElementsByName("hdTime_" + strFlightType);
    var objJSON = null;

    if (objOpt != null && objOpt.length > 0) {
        for (var i = 0; i < objOpt.length; i++) {
            if (objOpt[i].checked == true) {
                objJSON = eval("(" + objHd[i].value + ")");
                //Call display summary.
                GetLowFareFinderSummary(strFlightType, objJSON);
                break;
            }
        }
    }
}
function LffHighLight(strName, bSelect) {
    var objOptLowFare = document.getElementsByName("optLowfare_" + strName);
    var strId = "";
    if (objOptLowFare != null) {
        for (var i = 0; i < objOptLowFare.length; i++) {
            if (objOptLowFare[i].checked == true) {
                strId = objOptLowFare[i].id.split("_")[2];
            }
        }
    }

    var objTd = document.getElementById("td_" + strName + "_" + strId);
    var ObjSpn = document.getElementById("spnLowfare_" + strName + "_" + strId);
    if (bSelect == true) {
        if (objTd != null)
        { objTd.className = "Select"; }

        if (ObjSpn != null) {
            if (ObjSpn.className == "LowestFare") {
                ObjSpn.className = "LowestFareselect";
            }
            else {
                ObjSpn.className = "Lowfarepriceselect";
            }
        }
    }
    else {
        if (objTd != null)
        { objTd.className = ""; }
        if (ObjSpn != null) {
            if (ObjSpn.className == "LowestFareselect") {
                ObjSpn.className = "LowestFare";
            }
            else {
                ObjSpn.className = "Lowfareprice";
            }
        }
    }

    objOptLowFare = null;
}

function SelectLlf(strName) {
    LffHighLight(strName.split("_")[0], false);
    document.getElementById("optLowfare_" + strName).checked = true;
    LffHighLight(strName.split("_")[0], true);
    return false;
}
//-----------------------Function Call Sample----------------------------------------------
//function TestScript()
//{
//    tikAeroB2C.WebService.B2cService.HelloWorld(showResult, showError, showTimeOut);    
//}
//-----------------------------------------------------------------------------------------
var isClose = true;
var today = new Date();
var objLanguage;

Date.prototype.format = function (format) {
    var date = this;
    if (!format)
        format = "MM/dd/yyyy";
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    format = format.replace("MM", month.toString().padL(2, "0"));
    if (format.indexOf("yyyy") > -1)
        format = format.replace("yyyy", year.toString());
    else if (format.indexOf("yy") > -1)
        format = format.replace("yy", year.toString().substr(2, 2));

    format = format.replace("dd", date.getDate().toString().padL(2, "0"));
    var hours = date.getHours();
    if (format.indexOf("t") > -1) {
        if (hours > 11)
            format = format.replace("t", "pm");
        else
            format = format.replace("t", "am");
    }
    if (format.indexOf("HH") > -1)
        format = format.replace("HH", hours.toString().padL(2, "0"));
    if (format.indexOf("hh") > -1) {
        if (hours > 12) hours - 12;
        if (hours == 0) hours = 12;
        format = format.replace("hh", hours.toString().padL(2, "0"));
    }
    if (format.indexOf("mm") > -1)
        format = format.replace("mm", date.getMinutes().toString().padL(2, "0"));
    if (format.indexOf("ss") > -1)
        format = format.replace("ss", date.getSeconds().toString().padL(2, "0"));
    return format;
};
//**********************************************************
//Get Client Ip Address
var g_strIpAddress = "";

//**********************************************************
//Detect keyboard key
$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        var objHolder = document.getElementById("dvFormHolder");
        if (objHolder.style.display == "block") {
            CloseDialog();
        }
    }
});

function showError(result) {
    ShowProgressBar(true);
    if (result != null) {
        ShowMessageBox(result.get_message(), 0, 'loadHome');
    }
}
function showTimeOut(result) {
    ShowProgressBar(true);
    ShowMessageBox(objLanguage.Alert_Message_161, 0, "");
}
function showErrorPayment(result) {
    ShowLoadBar(false, false);
    ShowMessageBox(objLanguage.Alert_Message_160, 0, 'loadHome');
}
function showTimeOutPayment(result) {
    ShowLoadBar(false, false);
    ShowMessageBox(objLanguage.Alert_Message_161, 0, "");
}
//-------------------------------------------
//  Used to clear session and release seat
//  when close browser.
//-------------------------------------------
function CloseSession() {
    if (isClose == true) {
        tikAeroB2C.WebService.B2cService.ClearSession(showError, showTimeOut);
    }
    else { isClose = true; }
}

document.onkeydown = checkKeycode;
document.onmousedown = somefunction;

//window.onbeforeunload = function (oEvent) {
//    ClarNumberOfTab();
//};

function checkKeycode(e) {
    var keycode;
    if (window.event)
    { keycode = window.event.keyCode; }
    else if (e)
    { keycode = e.which; }

    //Check F5
    if (keycode == 116) {
        isClose = false;
    }
}
function somefunction(e) {
    if (navigator.appName.indexOf('Microsoft') == 0) {
        var tagName = "";
        if (window.event)
        { tagName = window.event.srcElement.tagName; }
        else if (e)
        { tagName = e.target.tagName; }

        if (tagName != null) {
            if (tagName.toUpperCase() == "A")
            { isClose = false; }
        }

    }
}
//---------------------------
//End Clear session function.
//---------------------------

//--------------------------------------------------------
// Tooltip function used instead html popup pages.
//--------------------------------------------------------

//quick function for tooltip color match
function fixToolTipColor() {
    //grab the bg color from the tooltip content - set top border of pointer to same
    $('.fg-tooltip-pointer-down-inner').each(function () {
        var bWidth = $('.fg-tooltip-pointer-down-inner').css('borderTopWidth');
        var bColor = $(this).parents('.fg-tooltip').css('backgroundColor');
        $(this).css('border-top', bWidth + ' solid ' + bColor);
    });
}

function ToolTipColor() {
    //dom ready
    $(function () {
        //fix color 
        fixToolTipColor();
        //show/hide them on hover
        $('.fg-tooltip').hide();
        $('.tooltip').hover(
			function () { $(this).parent().prev(':hidden').fadeIn(); },
			function () { $(this).parent().prev(':visible').fadeOut(); }
		);
    });

}

ToolTipColor();

//-------------------------------------------------------
// End Tooltip function
//-------------------------------------------------------

function ClearOptions(selectObj) {
    var selectParentNode = selectObj.parentNode;
    var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
    selectParentNode.replaceChild(newSelectObj, selectObj);
    return newSelectObj;
}
function FindControlName(tagName, ctrlName) {
    var strName = "";
    var objCtrl = document.getElementsByTagName(tagName);

    for (var iCount = 0; iCount < objCtrl.length; iCount++) {
        if (objCtrl[iCount].id.indexOf(ctrlName) != -1) {
            strName = objCtrl[iCount].id.toString();
            break;
        }
    }

    objCtrl = null;
    return strName;
}
function SetComboValue(ctrlName, strValue) {
    var ctrCombo = document.getElementById(ctrlName);
    if (ctrCombo != null) {
        for (var iCount = 0; iCount < ctrCombo.length; ++iCount) {
            if (ctrCombo.options[iCount].value == strValue) {
                ctrCombo.selectedIndex = iCount;
                if (iCount == 0 && (strValue == objLanguage.Default_Value_2 || strValue == objLanguage.Default_Value_3)) {
                    $("#" + ctrlName).addClass("watermarkOn");
                }
                else {
                    $("#" + ctrlName).removeClass("watermarkOn");
                }
                break;
            }
        }
    }
    ctrCombo = null;
}
function SetComboSplitValue(ctrlName, strValue, index) {
    var ctrCombo = document.getElementById(ctrlName);
    for (var iCount = 0; iCount < ctrCombo.length; ++iCount) {
        if (ctrCombo.options[iCount].value.split("|")[index] == strValue) {
            ctrCombo.selectedIndex = iCount;
            if (iCount == 0 && (strValue == objLanguage.Default_Value_2 || strValue == objLanguage.Default_Value_3)) {
                $("#" + ctrlName).addClass("watermarkOn");
            }
            else {
                $("#" + ctrlName).removeClass("watermarkOn");
            }
            break;
        }
    }

    ctrCombo = null;
}
function ShowProgressBar(bValue) {
    var obj = document.getElementById("dvProgressBar");
    var objLoader = document.getElementById("dvLoad");
    if (obj != null) {
        document.getElementById("dvLoadBar").style.display = "none";

        if (bValue == true) {
            objLoader.style.display = "block";
            obj.style.display = "block";
        }
        else {
            if (document.getElementById("dvMessageBox").style.display == "none" || document.getElementById("dvMessageBox").style.display == "") {
                var objFormHolder = document.getElementById("dvFormHolder");
                if (objFormHolder != null && (objFormHolder.style.display == "none" || objFormHolder.style.display == "")) {
                    obj.style.display = "none";
                }
            }
            objLoader.style.display = "none";
        }
        obj = null;
        objLoader = null;
    }
}
function ShowLoadBar(bLoad, bInsurance) {
    var obj = document.getElementById("dvProgressBar");
    var objLoader = document.getElementById("dvLoadBar");

    //Message text
    var objPaymentText = document.getElementById("dvPaymentLoadingMessage");
    var objInsuranceText = document.getElementById("dvInsuranceLoadingMessage");

    if (bInsurance == false) {
        document.getElementById("dvPaymentLoadingMessage").style.display = "block";
        document.getElementById("dvInsuranceLoadingMessage").style.display = "none";
    }
    else {
        document.getElementById("dvPaymentLoadingMessage").style.display = "none";
        document.getElementById("dvInsuranceLoadingMessage").style.display = "block";
    }


    document.getElementById("dvLoad").style.display = "none";
    document.getElementById("dvMessageBox").style.display = "none";

    if (bLoad == true) {
        objLoader.style.display = "block";
        obj.style.display = "block";
    }
    else {
        objLoader.style.display = "none";
        obj.style.display = "none";
    }

    obj = null;
    objLoader = null;
}
function loadHome() {
    var strParameterValue = getRequestParameter("langculture");
    if (strParameterValue.length > 0) {
        //var myText = new String(document.location.toString());
        //myText = myText.replace("Finish=Yes&", "");

        var url = window.location.href.split("//")[1];
        document.location.href = "http://" + url;
    }
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadHome(SuccessLoadHomePage, showError, showTimeOut);
    }

}
function SuccessLoadHomePage(result) {
    if (IsSSecurePage() == true) {
        window.location.href = "http://" + window.location.href.split("//")[1];
    }
    else {
        if (result.length > 0) {
            var objContainer = document.getElementById("dvContainer");

            objContainer.innerHTML = result;

            DisplayQuoteSummary("", "", "");

            ShowSearchPannel(true);
            ShowProgressBar(false);
            objContainer = null;
        }
    }
}

function FindOptionIndexByValue(ctrName, value) {
    var objOption = document.getElementById(ctrName);
    var iCount = 0;
    for (iCount = 0; iCount < objOption.length; ++iCount) {
        if (objOption.options[iCount].value == value) {
            iCount;
            break;
        }
    }
    return iCount;
}

function ReformatDate(strDate) {
    if (strDate == objLanguage.default_value_1 || strDate == GetDateMask() || strDate.length == 0) {
        return "";
    }
    else {
        var day = strDate.substring(8, 10);
        var month = strDate.substring(5, 7);
        var year = strDate.substring(0, 4);
        if (IsNumeric(day) == false || IsNumeric(month) == false || IsNumeric(year) == false) {
            return "";
        }
        else {
            return year + "-" + month + "-" + day;
        }
    }
}
function ReformatDateXml(strDate) {
    if (strDate == objLanguage.default_value_1 || strDate == GetDateMask() || strDate.length == 0) {
        return "0001-01-01T00:00:00";
    }
    else {
        var day = strDate.substring(8, 10);
        var month = strDate.substring(5, 7);
        var year = strDate.substring(0, 4);

        return year + "-" + month + "-" + day + "T00:00:00";
    }
}

function ReformatXmlViewDate(strDate) {
    if (strDate.length > 0) {
        var day = strDate.substring(8, 10);
        var month = strDate.substring(5, 7);
        var year = strDate.substring(0, 4);

        return year + "/" + month + "/" + day;
    }
    else {
        return "";
    }
}
function ReformatYYYYMMDDToDashDate(strDate) {
    if (strDate.length > 0) {
        var day = strDate.substring(6, 8);
        var month = strDate.substring(4, 6);
        var year = strDate.substring(0, 4);

        return year + "-" + month + "-" + day;
    }
    else {
        return "";
    }
}
function DateMaskFormat(strDate) {
    if (strDate.length == 0) {
        return objLanguage.default_value_1;
    }
    else {
        return strDate;
    }
}
function LTrim(value) {
    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");
}

function RTrim(value) {
    var re = /((\s*\S+)*)\s*/;
    return value.replace(re, "$1");
}

function trim(value) {
    return LTrim(RTrim(value));
}
function leadingZero(nr) {
    if (nr < 10) nr = "0" + nr;
    return nr;
}
function printReport(reportType, reportTitle) {
    window.open("print.aspx?type=" + reportType, reportTitle, "scrollbars=1,status=1,toolbar=0", "");
}
function EmailItinerary() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.SendItineraryEmail(SuccessEmailItinerary, showError, showTimeOut);
}
function SuccessEmailItinerary(result) {
    ShowProgressBar(false);
    ShowMessageBox("Email Sent..", 1, '');
}
function setCookie(strName, strValue, dExpires, strPath, strDomain, bSecure) {
    var strCookieText = escape(strName) + '=' + escape(strValue);
    strCookieText += (dExpires ? '; EXPIRES=' + dExpires.toGMTString() : '');
    strCookieText += (strPath ? '; PATH=' + strPath : '');
    strCookieText += (strDomain ? '; DOMAIN=' + strDomain : '');
    strCookieText += (bSecure ? '; SECURE' : '');

    document.cookie = strCookieText;
}
function getCookie(strName) {
    var strValue = null;
    if (document.cookie)	   //only if exists
    {
        var arr = document.cookie.split((escape(strName) + '='));
        if (2 <= arr.length) {
            var arr2 = arr[1].split(';');
            strValue = unescape(arr2[0]);
        }
    }
    return strValue;
}
function deleteCookie(strName) {
    var tmp = getCookie(strName);
    if (tmp)
    { setCookie(strName, tmp, (new Date(1))); }
}
function IsFutureDate(strDate, strDateCompare) {
    var bResult = false;

    //Reformat Date value to be yyyy-MM-DD
    strDate = ReformatDate(strDate);

    if (strDate.length > 0) {
        var arrDate = strDate.split("-");
        var dDate = new Date(arrDate[0], arrDate[1] - 1, arrDate[2]);
        var dToday;

        if (strDateCompare != null && strDateCompare.length > 0) {
            arrDate = ReformatDate(strDateCompare).split("-");
            dToday = new Date(arrDate[0], arrDate[1] - 1, arrDate[2]);
        }
        else {
            dToday = new Date();
        }

        if (dDate > dToday) {
            bResult = true;
        }
        else {
            bResult = false;
        }
        dDate = null;
        dToday = null;
    }
    else {
        bResult = true;
    }

    return bResult;
}
function IsPastDate(strDate) {
    var bResult = false;

    //Reformat Date value to be yyyy-MM-DD
    strDate = ReformatDate(strDate);

    if (strDate.length > 0) {
        var arrDate = strDate.split("-");
        var dDate = new Date(arrDate[0], arrDate[1] - 1, arrDate[2]);
        var dToday = new Date();

        if (dDate < dToday) {
            bResult = true;
        }
        else {
            bResult = false;
        }
        dDate = null;
        dToday = null;
    }
    else {
        bResult = true;
    }

    return bResult;
}
function ShowMessageBox(strMessage, iType, callBack) {
    //iType
    //  0 = Error.
    //  1 = Information.

    var obj = document.getElementById("dvProgressBar");
    var objErrorMsg = document.getElementById("dvErrorMessage");
    var objIcon = document.getElementById("dvMessageIcon");
    var objMessage = document.getElementById("dvMessageBox");

    document.getElementById("dvLoadBar").style.display = "none";
    document.getElementById("dvLoad").style.display = "none";

    //Show and hide Icon.
    //    if(iType != 0)
    //    { objIcon.style.display = "none" ;}


    //Show or hide fading.
    objErrorMsg.innerHTML = strMessage;
    obj.style.display = "block";
    objMessage.style.display = "block";
    objMessage.setAttribute('CallBack', callBack);

    obj = null;
    objErrorMsg = null;
    objIcon = null;
    objMessage = null;
    //Scroll to the top page
}
function CloseMessageBox() {

    var obj = document.getElementById("dvProgressBar");
    var objMessage = document.getElementById("dvMessageBox");
    var objFormHolder = document.getElementById("dvFormHolder");

    if (objFormHolder != null && (objFormHolder.style.display == "none" || objFormHolder.style.display == "")) {
        obj.style.display = "none";
    }
    objMessage.style.display = "none";

    //Call back function.
    if (objMessage.getAttribute("CallBack") != null && objMessage.getAttribute("CallBack") != '') {
        var arrCallBack = objMessage.getAttribute("CallBack").split("|");
        if (arrCallBack.length > 0) {
            if (arrCallBack[0] == 'wellnet') {
                if (arrCallBack.length == 2) {
                    var PopupUrl = arrCallBack[1];
                    var viewportwidth;
                    var viewportheight;

                    // the more standards compliant browsers (mozilla/netscape/opera/IE7) 
                    if (typeof window.innerWidth != 'undefined') {
                        viewportwidth = window.innerWidth,
                        viewportheight = window.innerHeight;
                    }
                        // IE6 in standards compliant mode 
                    else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
                        viewportwidth = document.documentElement.clientWidth,
                        viewportheight = document.documentElement.clientHeight;
                    }
                    else {
                        // older versions of IE
                        viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
                        viewportheight = document.getElementsByTagName('body')[0].clientHeight;
                    }

                    if (PopupUrl.length > 0) {
                        window.open(PopupUrl, 'wellnet', "height=" + viewportheight + ",width=" + viewportwidth + ",toolbar=yes,directories=no,status=no, menubar=yes,scrollbars=yes,resizable=yes ,modal=yes");
                    }
                }
            }
            else if (arrCallBack[0] == 'loadHome') {
                obj.style.display = "none";
                objMessage.style.display = "none";
                loadHome();
            }
        }
    }

    //Change back close button of alert message to "OK"
    SetDefaltAlertMessage();

    obj = null;
    objMessage = null;
}

function LoadHtml(strHtmlName) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadHtml(strHtmlName, SuccessLoadHtml, showError, showTimeOut);
}
function SuccessLoadHtml(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;

    ShowProgressBar(false);
    objContainer = null;

}
function Changelang(strLanguage) {
    tikAeroB2C.WebService.B2cService.SetLanguage(strLanguage, SuccessChangelang, showError, showTimeOut);
}
function SuccessChangelang(result) {
    var objLang = document.getElementById("hdLang");
    objLang.value = result;
    objLang = null;

    document.forms[0].submit();
}
function getRequestParameter(strName) {
    strName = strName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

    var regexS = "[\\?&]" + strName + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);

    if (results == null)
        return "";
    else
        return results[1];
}

function CreateWnd(file, width, height, resize) {
    var doCenter = false;

    attribs = "";

    if (resize) size = "true"; else size = "no";

    for (var item in window)
    { if (item == "screen") { doCenter = true; break; } }

    if (doCenter) {
        if (screen.width <= width || screen.height <= height) size = "yes";

        WndTop = (screen.height - height) / 2;
        WndLeft = (screen.width - width) / 2;
        attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," +
		"status=no,toolbar=no,directories=no,menubar=no,location=no,dialog=yes,chrome, resizable=false, minimizable=yes,top=" + WndTop + ",left=" + WndLeft;
    }
    else {
        if (navigator.appName == "Netscape" && navigator.javaEnabled()) {
            var toolkit = java.awt.Toolkit.getDefaultToolkit();
            var screen_size = toolkit.getScreenSize();
            if (screen_size.width <= width || screen_size.height <= height) size = "yes";

            WndTop = (screen_size.height - height) / 2;
            WndLeft = (screen_size.width - width) / 2;
            attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," +
			"status=no,toolbar=no,directories=no,menubar=no,location=no,dialog=yes,chrome, resizable=false, minimizable=yes,top=" + WndTop + ",left=" + WndLeft;
        }
        else {
            size = "yes";
            attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," +
			"status=no,toolbar=no,directories=no,menubar=no,location=no,dialog=yes,chrome, resizable=false, minimizable=yes";
        }
    }


    // create the window
    window.open(file, "", attribs);

}

function dateDiff(p_Interval, p_Date1, p_Date2, p_firstdayofweek, p_firstweekofyear) {

    if (!isDate(p_Date1)) { return "invalid date: '" + p_Date1 + "'"; }
    if (!isDate(p_Date2)) { return "invalid date: '" + p_Date2 + "'"; }
    var dt1 = new Date(p_Date1);
    var dt2 = new Date(p_Date2);
    // get ms between dates (UTC) and make into "difference" date
    var iDiffMS = dt2.valueOf() - dt1.valueOf();
    var dtDiff = new Date(iDiffMS);

    // calc various diffs
    var nYears = dt2.getUTCFullYear() - dt1.getUTCFullYear();
    var nMonths = dt2.getUTCMonth() - dt1.getUTCMonth() + (nYears != 0 ? nYears * 12 : 0);
    var nQuarters = parseInt(nMonths / 3); //<<-- different than VBScript, which watches rollover not completion

    var nMilliseconds = iDiffMS;
    var nSeconds = parseInt(iDiffMS / 1000);
    var nMinutes = parseInt(nSeconds / 60);
    var nHours = parseInt(nMinutes / 60);
    var nDays = parseInt(nHours / 24);
    var nWeeks = parseInt(nDays / 7);


    // return requested difference
    var iDiff = 0;
    switch (p_Interval.toLowerCase()) {
        case "yyyy": return nYears;
        case "q": return nQuarters;
        case "m": return nMonths;
        case "y": 		// day of year
        case "d": return nDays;
        case "w": return nDays;
        case "ww": return nWeeks; 	// week of year	// <-- inaccurate, WW should count calendar weeks (# of sundays) between
        case "h": return nHours;
        case "n": return nMinutes;
        case "s": return nSeconds;
        case "ms": return nMilliseconds; // millisecond	// <-- extension for JS, NOT available in VBScript
        default: return "invalid interval: '" + p_Interval + "'";
    }
}
function isDate(p_Expression) {
    return !isNaN(new Date(p_Expression)); 	// <<--- this needs checking
}
function CheckDateFormat(strDate) {
    var fdate = strDate.split('/');

    if (fdate.length < 3) return false;

    var lenStr = strDate.replace('/', '');
    lenStr = lenStr.replace('/', '');

    if (lenStr.toString().length != 8) return false;

    return true;
}
function padZeros(theNumber, max) {
    var numStr = String(theNumber);

    while (numStr.length < max) {
        numStr = '0' + numStr;
    }

    return numStr;
}

function CloseDialog() {
    var objMessage = document.getElementById("dvFormHolder");
    objMessage.style.display = "none";
    objMessage.innerHTML = "";
    var objContainer = document.getElementById("dvProgressBar");
    objContainer.style.display = "none";
}

function SubmitEnterUser(myfield, e, isDialog) {
    if (MultipleTab.IsCorrectTab()) {
        var keycode;
        if (window.event) keycode = window.event.keyCode;
        else if (e) keycode = e.which;
        else return true;

        if (keycode == 13) {
            if (isDialog == 'false')
                ClientLogon();
            else
                ClientLogonDialog();
            return false;
        }
        else
            return true;
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}

function IsNumeric(input) {
    return (input - 0) == input && input.length > 0;
}

function OpenTicketDetail(booking_id, passengerId, bookingSegmentId) {
    window.open("TicketPopUp.aspx?bid=" + booking_id + "&pid=" + passengerId + "&sid=" + bookingSegmentId);
}

function LoadMenu() {
    tikAeroB2C.WebService.B2cService.LoadMenu(SuccessLoadMenu, showError, showTimeOut);
}
function SuccessLoadMenu(result) {
    var objContainer = document.getElementById("dvMenu");

    objContainer.innerHTML = result;
    objContainer = null;
}

function resizeFrame(f) {
    f.style.height = f.contentWindow.document.body.scrollHeight + "px";
    f.style.width = f.contentWindow.document.body.scrollWidth + "px";
}
function isNum(string) {
    var numericExpression = /^[0-9]+$/;
    if (string.match(numericExpression)) {
        return true;
    } else {
        return false;
    }
}
function IsChar(string) {
    var CharExpression = /^[a-zA-Z]+$/;
    if (string.match(CharExpression)) {
        return true;
    } else {
        return false;
    }
}
function IsCharWithSpace(string) {
    var CharExpression = /^[a-zA-Z,\s]+$/;
    if (string.match(CharExpression)) {
        return true;
    } else {
        return false;
    }
}
function ContainNumeric(value) {
    for (var i = 0; i < value.length; i++) {
        if (isNum(value.charAt(i)) == true) {
            return true;
        }
    }
    return false;
}
function FilterNonNumeric(value) {
    var charMatch = "";
    if (value.length > 0) {
        for (var i = 0; i < value.length; i++) {
            if (isNum(value.charAt(i)) == true)
                charMatch += value.charAt(i);
        }
    }
    return charMatch;
}
function OnlyCharacter(value) {
    for (var i = 0; i < value.length; i++) {
        if (IsChar(value.charAt(i)) == false) {
            return false;
        }
    }
    return true;
}
function IsAlphaNumeric(string) {
    var CharExpression = /^[a-zA-Z0-9]+$/;
    if (string.match(CharExpression)) {
        return true;
    } else {
        return false;
    }
}
function ReplaceSpecialCharacter(value) {
    var strNewValue = new String(value);
    strNewValue = strNewValue.replace(/[^a-zA-Z 0-9]+/g, '');
    strNewValue = strNewValue.replace(/\s+/g, '');

    return strNewValue;
}

function FormLoadOperation() {

    ClickToExpand();

    if ($('#hdLang').val() != "")
        LanguageCode = $('#hdLang').val();

    DisplayQuoteSummary("", "", "");
    GenerateFromQueryString();
}

function EmailItineraryInput(strDefaultMail) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ShowEmailSender(SuccessEmailItineraryInput, showError, strDefaultMail);
}
function SuccessEmailItineraryInput(result, strDefauleEmail) {
    ShowProgressBar(false);
    if (result.length > 0) {
        var objMessage = document.getElementById("dvFormHolder");
        var objContainer = document.getElementById("dvProgressBar");

        //Insert passenger form content.
        objMessage.innerHTML = result;

        document.getElementById("txtEmail").value = strDefauleEmail;

        objMessage.style.display = "block";
        objContainer.style.display = "block";

        objMessage = null;
        objContainer = null;
    }
}
function CloseEmailSender() {
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");
    //Insert passenger form content.
    objMessage.innerHTML = "";

    objMessage.style.display = "none";
    objContainer.style.display = "none";

    objMessage = null;
    objContainer = null;
}
function SendItineraryEmailInput() {
    //Close Email Sender.
    var strEmail = document.getElementById("txtEmail").value;

    CloseEmailSender();
    if (ValidEmail(strEmail) == true) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SendItineraryEmailOptional(strEmail, SuccessEmailItinerary, showError, showTimeOut);
    }
    else {
        ShowMessageBox("Invalid Email address !!", 0, '');
    }

}

function AddCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function RemoveCommas(strNumber) {
    return strNumber.replace(/\,/g, "");
}
function SetLanguage(strJSON) {
    objLanguage = eval("(" + strJSON + ")");
}

var TheDateDiff = {
    inDays: function (d1, d2) {
        var t2 = d2.getTime();
        var t1 = d1.getTime();
        return parseInt((t2 - t1) / (24 * 3600 * 1000));
    },

    inWeeks: function (d1, d2) {
        var t2 = d2.getTime();
        var t1 = d1.getTime();

        return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7));
    },

    inMonths: function (d1, d2) {
        var d1Y = d1.getFullYear();
        var d2Y = d2.getFullYear();
        var d1M = d1.getMonth();
        var d2M = d2.getMonth();

        return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
    },

    inYears: function (d1, d2) {
        return d2.getFullYear() - d1.getFullYear();
    }
};

function IDateDiff(depDate, birthDate, paxType, MIN_YEAR, MAX_YEAR) {

    var dayOfBirth = birthDate.getDate();
    var dayOfDepDate = depDate.getDate();


    var MIN_MONTH = 12 * MIN_YEAR;
    var MAX_MONTH = 12 * MAX_YEAR;

    if (TheDateDiff.inYears(birthDate, depDate) >= MIN_YEAR && TheDateDiff.inYears(birthDate, depDate) <= MAX_YEAR) {

        if (TheDateDiff.inMonths(birthDate, depDate) >= MIN_MONTH && TheDateDiff.inMonths(birthDate, depDate) <= MAX_MONTH) {

            if (TheDateDiff.inYears(birthDate, depDate) == MAX_YEAR && TheDateDiff.inMonths(birthDate, depDate) == MAX_MONTH && dayOfBirth <= dayOfDepDate) {
                return false;
            }
            else if (TheDateDiff.inYears(birthDate, depDate) == MIN_YEAR && TheDateDiff.inMonths(birthDate, depDate) == MIN_MONTH && dayOfBirth > dayOfDepDate) {
                return false;
            }
            else { return true; }
        }
        else { return false; }
    }
    else { return false; }
}

function validateChdInfBirthDate(checkedDate, birthDate, paxType) {


    var strErrMessage = "";
    var result = false;

    var MAX_YEAR = max_age; // max adult
    var MEAN_YEAR = min_adult; // min adult  max child
    var MIN_YEAR = min_child;   // min child   max inf
    var MIN_INF = min_inf;   // min inf unit is day


    var objDate = getDateInformation('ymd', '-', birthDate);
    var db = Number(objDate.day);
    var mb = Number(objDate.month);
    var yb = Number(objDate.year);
    var birthDate = new Date(yb, mb, db);

    objDate = getDateInformation('ymd', '-', checkedDate);
    var dd = Number(objDate.day);
    var md = Number(objDate.month);
    var yd = Number(objDate.year);
    var depDate = new Date(yd, md, dd);

    if (paxType.toUpperCase() == "ADULT") {
        result = IDateDiff(depDate, birthDate, paxType, MEAN_YEAR, MAX_YEAR);
        return result;
    }
    else if (paxType.toUpperCase() == "CHD") {
        result = IDateDiff(depDate, birthDate, paxType, MIN_YEAR, MEAN_YEAR);
        return result;
    }
    else if (paxType.toUpperCase() == "INF") {
        result = IDateDiff(depDate, birthDate, paxType, 0, MIN_YEAR);

        if (!result) {
            return false;
        }
        else {
            if (TheDateDiff.inDays(birthDate, depDate) < MIN_INF)
                return false;
        }

        return result;

    }

}

//formatDate can be only 3 letter d m y, for example dmy, mdy, ymd ...   in lowercase
function getDateInformation(formatDate, separater, strDate) {
    var arrDate = strDate.split(separater);
    var d = formatDate.indexOf('d');
    var m = formatDate.indexOf('m');
    var y = formatDate.indexOf('y');

    return { "day": arrDate[d], "month": arrDate[m], "year": arrDate[y] };
}
function ReformatXmlToYYYYMMDD(strDate) {
    if (strDate.length > 0) {
        var day = strDate.substring(8, 10);
        var month = strDate.substring(5, 7);
        var year = strDate.substring(0, 4);

        return year + month + day;
    }
    else {
        return "";
    }
}

function LoadSecure(value) {
    isClose = false;
    var strHttp = "";

    if (value == true) {
        strHttp = "https://";
    }
    else {
        strHttp = "http://";
    }
    window.location.href = strHttp + RemoveUrlParameter(window.location.href.split("//")[1]);
}

function FormatTimeNumber(strTime) {
    var dateLength = strTime.toString().length;
    if (dateLength > 0) {
        if (dateLength == 3) {
            return "0" + strTime.toString().substring(0, 1) + ":" + strTime.toString().substring(1, 3);
        }
        else {
            return strTime.toString().substring(0, 2) + ":" + strTime.toString().substring(2, 4);
        }
    }
    else {
        return "";
    }
}
function FormatDateString(strDate) {
    var day = strDate.toString().substring(6, 8);
    var month = strDate.toString().substring(4, 6);
    var year = strDate.toString().substring(0, 4);

    var dDate = new Date(year, month - 1, day);

    return GetDayName(dDate) + " " + day + " " + GetMonthName(strDate) + " " + year;
}
function FormatDashDateString(strDate) {
    var day = strDate.toString().substring(8, 10);
    var month = strDate.toString().substring(5, 7);
    var year = strDate.toString().substring(0, 4);

    var dDate = new Date(year, month - 1, day);

    return GetDayName(dDate) + " " + day + " " + GetMonthName(dDate) + " " + year;
}
function GetDayName(dDate) {
    var d = [objLanguage.Date_Display_14,
            objLanguage.Date_Display_15,
            objLanguage.Date_Display_16,
            objLanguage.Date_Display_17,
            objLanguage.Date_Display_18,
            objLanguage.Date_Display_19,
            objLanguage.Date_Display_20];

    return d[dDate.getDay()];
}
function GetMonthName(dDate) {
    var m = [objLanguage.Date_Display_2,
            objLanguage.Date_Display_3,
            objLanguage.Date_Display_4,
            objLanguage.Date_Display_5,
            objLanguage.Date_Display_6,
            objLanguage.Date_Display_7,
            objLanguage.Date_Display_8,
            objLanguage.Date_Display_9,
            objLanguage.Date_Display_10,
            objLanguage.Date_Display_11,
            objLanguage.Date_Display_12,
            objLanguage.Date_Display_13];
    return m[dDate.getMonth()];
}
function ValidateInput(strValue) {
    var regExp = /^[A-Za-z]$/;
    if (strValue != null && strValue != "") {
        for (var i = 0; i < strValue.length; i++) {
            if (!strValue.charAt(i).match(regExp)) {
                return false;
            }
        }
    }
    else {
        return false;
    }
    return true;
}

function IsSSecurePage() {
    var loc = new String(window.parent.document.location);

    if (loc.indexOf("https://") === -1)
        return false;
    else
        return true;
}

function RemoveUrlParameter(strUrl) {
    var arr = strUrl.split("?");
    var langculture = "";
    var ao = "";
    var mb = "";
    var url = "";

    if (existEachQueryString("langculture") && existEachQueryString("ao")) {
        langculture = queryStringValue("langculture");
        ao = queryStringValue("ao");
        url = url + "?langculture=" + langculture + "&ao=" + ao;
    }
    else if (existEachQueryString("langculture")) {
        langculture = queryStringValue("langculture");
        url = url + "?langculture=" + langculture;
    }
    else if (existEachQueryString("ao")) {
        ao = queryStringValue("ao");
        url = url + "?ao=" + ao;
    }

    return arr[0] + url;
}
function GetVirtualDirectory() {
    var url = window.location.href;
    var url_parts = url.split("/");

    url_parts[url_parts.length - 1] = "";

    var current_page_virtual_path = url_parts.join("/");
    return current_page_virtual_path;
}
function DateValid(strDate) {
    var Days = 30;

    strDate = ReformatDate(strDate);
    if (strDate.length < 1) {
        return false;
    }
    else {
        var DateSplit = strDate.split('-');
        var iDay = parseInt(DateSplit[2], 10);
        var iMonth = parseInt(DateSplit[1], 10);
        var iYear = parseInt(DateSplit[0], 10);

        try {
            if (iMonth == 1 || iMonth == 3 || iMonth == 5 || iMonth == 7 || iMonth == 8 || iMonth == 10 || iMonth == 12)
                Days = 31;
            else if (iMonth == 2)
                Days = DaysInMonth(iYear, iMonth);
            else if (iMonth == 4 || iMonth == 6 || iMonth == 9 || iMonth == 11)
                Days = 30;

            if (iDay > Days || iDay < 1) {
                ShowMessageBox("Enter a Valid Day.", 1, '');
                return false;
            }
            if (iMonth > 12 || iMonth < 1) {
                ShowMessageBox("Enter a Valid Month.", 1, '');
                return false;
            }
            if (iYear.toString().length < 4 || iYear < 1800) {
                ShowMessageBox("Enter a Valid Year.", 1, '');
                return false;
            }
        }
        catch (ex) {
            ShowMessageBox(ex, 1, '');
            return false;
        }
    }
}
function DisplayMessage(obj, strDefaultMsg) {
    if (obj == null)
    { return strDefaultMsg; }
    else
    { return obj; }
}
function RefreshSetting() {
    var url = window.location.href.toLowerCase();
    if (url.indexOf("searchavailability") != -1) {
        //CloseSession();
    }

    tikAeroB2C.WebService.B2cService.GetCurrentState(SuccessRefreshSetting, showErrorPayment, showTimeOutPayment);
}

function SuccessRefreshSetting(result) {
    var objJSON = eval("(" + result + ")");


    if (objJSON != null) {

        if (checkValidQueryStringForSearch() == true) {
            MultipleTab.ClearID();
        }


        MultipleTab.AssignInitialTab(objJSON.SessionId, false);

        var check = searchFlightAvailFromQueryString();


        if (check == false) return check;

        if (IsMultipleTabOpen(objJSON) == false) {

            if (Number(objJSON.CurrentStep) == 1 && checkValidQueryStringForSearch() == true) {


                if (UseGoogleManager() == true) {
                    dataLayer.push({ 'event': 'SearchFlightPage' });
                }


            }
            else {
                if (Number(objJSON.CurrentStep) == 1 | Number(objJSON.CurrentStep) == 2) {
                    if (Number(objJSON.CurrentStep) == 1 && objJSON.ClientProfile == true) {
                        ShowProgressBar(true);
                        //Load My Profile
                        tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_Edit, showError, showTimeOut);

                        //Load Client information
                        tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);

                        //Reload menu.
                        LoadMenu();
                    }
                    else {
                        HideHeaderMenu(false);

                        if (Number(objJSON.CurrentStep) == 1 && objJSON.Gateway != "")
                            ShowSearchPannel(false);
                        else
                            ShowSearchPannel(true);

                        //google analytic
                        if (UseGoogleManager() == true) {
                            if (Number(objJSON.CurrentStep) == 1) {
                                dataLayer.push({ 'event': 'SearchFlightPage' });
                            }
                        }

                        if (Number(objJSON.CurrentStep) == 2) {

                            var objOutwards = document.getElementsByName("Outward");
                            var objReturns = document.getElementsByName("Return");
                            var btmSearch = document.getElementById("spnSearchCap");

                            //Show Cobo-flight information
                            ShowComboflightInformation();

                            //Load Client information
                            tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);

                            //Rename search button.
                            if (btmSearch != null) {
                                btmSearch.innerHTML = objLanguage.Alert_Message_138;  //"Refine your search";
                            }

                            //Select at lowest fare.
                            SelectLowestFare();

                            //Clear Fare Summary
                            DisplayQuoteSummary("", "", "");
                        }

                    }

                }
                else if (Number(objJSON.CurrentStep) == 4) {

                    //google analytic
                    if (UseGoogleManager() == true) {
                        dataLayer.push({ 'event': 'PassengerDetailPage' });
                    }

                    //Display quote information
                    GetSessionQuoteSummary();
                    //Read Cookies to Control
                    GetContactInformationFromCookies();
                    //Set passenger input information
                    SetPassengerDetail();
                    //Load Client information
                    tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);
                }
                else if (Number(objJSON.CurrentStep) == 5) {

                    SetPaymentContent();

                    InitializePaymentWaterMark();

                    //Call ACE insurance quote.
                    ACEInsuranceRequestQuote("refresh");

                    //Detect keyboard key for Credit Card number.
                    AddCCKeyEvent();

                    //Display quote information
                    GetSessionQuoteSummary();
                }
                else if (Number(objJSON.CurrentStep) == 6) {
                    HideHeaderMenu(true);
                    MultipleTab.ClearID();
                }
                else if (Number(objJSON.CurrentStep) == 7) {
                    //Load Client information when refresh
                    tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);
                    ShowSearchPannel(true);
                }
                else if (Number(objJSON.CurrentStep) == 10) {
                    if (window.name != null && window.name.length > 0) {

                        var objStateParam = eval("(" + window.name + ")");
                        if (objStateParam != null && objStateParam.BookingId != null && objStateParam.BookingId.length > 0) {
                            LoadCob(true, objStateParam.BookingId);
                            objStateParam.BookingId = "";
                        }

                    }
                    ShowSearchPannel(false);
                }
                else if (Number(objJSON.CurrentStep) == 11) {
                    //Display quote information
                    GetSessionQuoteSummary();

                    //Baggage watermark
                    var objCtrl = document.getElementsByTagName("select");
                    for (var iCount = 0; iCount < objCtrl.length; iCount++) {
                        if (objCtrl[iCount].id.indexOf("baggage") != -1) {
                            InitializeWaterMark(objCtrl[iCount].id.toString(), objLanguage.default_value_7, "select");
                        }
                    }
                }
                else if (Number(objJSON.CurrentStep) == 12) {
                    ShowSearchPannel(true);
                }
            }
        }
    }
    else {
        HideHeaderMenu(false);
        ShowSearchPannel(true);
    }

    objJSON = null;

    //Initialize Flight selection box watermark
    InitializeWaterMark("AvailabilitySearch1_optOrigin", objLanguage.default_value_7, "select");
    InitializeWaterMark("AvailabilitySearch1_optDestination", objLanguage.default_value_7, "select");
}
function InitializeDateMaskEdit(inputName) {
    $("#" + inputName).mask(strDateFormat);
}
function ValidPhoneNumber(strPhone) {
    var res = false;
    reg = new RegExp(strPhoneNumberFormat);
    res = (reg.test(strPhone));
    if (res) {
        var num = parseInt(strPhone.replace(/_/gi, "").replace(/#/gi, "").replace(/-/gi, "")) || 0;
        if (num == 0)
            res = false;
    }
    return (res);
}
function ValidEmail(strEmail) {
    var a = false;
    var res = false;

    if (typeof (RegExp) == 'function') {
        var b = new RegExp('abc');
        if (b.test('abc') == true) {
            a = true;
        }
    }
    if (a == true) {
        reg = new RegExp('^([a-zA-Z0-9\\-\\.\\_]+)' + '(\\@)([a-zA-Z0-9\\-\\.]+)' + '(\\.)([a-zA-Z;]{2,4})$');
        res = (reg.test(strEmail));
    }
    else {
        res = (strEmail.search('@') >= 1 &&
		strEmail.lastIndexOf('.') > strEmail.search('@') &&
		strEmail.lastIndexOf('.') >= strEmail.length - 5);
    }
    return (res);
}
function ShowSearchPannel(bShow) {
    var objSearchBox = document.getElementById("dvAvailabilitySearch");

    if (bShow == true) {
        objSearchBox.style.display = "block";
    }
    else {
        objSearchBox.style.display = "none";
    }

    objSearchBox = null;
}
function GetArrayParamValue(arrParam, strName) {
    var arr;
    var arrDetail;
    if (arrParam.length > 0) {
        arr = arrParam.split("|");
        for (var i = 0; i < arr.length; i++) {
            arrDetail = arr[i].split(":");
            if (arrDetail[0] == strName) {
                return arrDetail[1];
            }
        }
    }
    return "";
}
function ReplaceSpecialCharacter(value) {
    var strNewValue = new String(value);
    strNewValue = strNewValue.replace(/[^a-zA-Z 0-9]+/g, '');
    strNewValue = strNewValue.replace(/\s+/g, '');

    return strNewValue;
}
function ContainSpecialCharacter(strValue) {
    var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?~_";
    for (var i = 0; i < strValue.length; i++) {
        if (iChars.indexOf(strValue.charAt(i)) != -1) {
            return true;
        }
    }
    return false;
}
function ConvertToValidXmlData(strValue) {
    var str = "";
    var iCharValue;
    for (var i = 0; i < strValue.length; i++) {
        iCharValue = strValue.charCodeAt(i);
        if (iCharValue < 32 || iCharValue > 127) {
            str = str + "&#" + iCharValue + ";";
        }
        else {
            str = str + strValue.charAt(i).replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace('"', "&quot;").replace("'", "&apos;");
        }
    }
    return str;
}
function GetDayOfWeek(strDate) {
    if (strDate.length > 0) {
        var arrDate = strDate.split("-");
        var dtDate = new Date(arrDate[0] + '/' + arrDate[1] + '/' + arrDate[2]);
        return dtDate.getDay();
    }
    else {
        return "";
    }
}

//*****************************************************************************************
//  Collapse event click call.
function Collapse_Click(strName) {

    var arrName = strName.split("_");
    if ($("#" + strName + " img").attr("src") == "App_Themes/Default/Images/collapse.png") {

        $("#" + strName + " img").attr("src", "App_Themes/Default/Images/expand.png");

        if (arrName[0] == "PassengerDetailList") {
            document.getElementById("PassengerName_" + arrName[1]).style.display = "block";
            document.getElementById("PassengerName_" + arrName[1]).innerHTML = document.getElementById("txtName_" + arrName[1]).value + "  " + document.getElementById("txtLastname_" + arrName[1]).value;
        }

    }
    else {

        $("#" + strName + " img").attr("src", "App_Themes/Default/Images/collapse.png");

        if (arrName[0] == "PassengerDetailList") {
            document.getElementById("PassengerName_" + arrName[1]).style.display = "none";
            document.getElementById("PassengerName_" + arrName[1]).innerHTML = "";
        }
    }
}


function GetControlValue(obj) {

    if (obj != null) {
        var strValue = trim(obj.value);
        if (strValue == objLanguage.default_value_1 || strValue == objLanguage.default_value_2 || strValue == objLanguage.default_value_3 || strValue == GetDateMask()) {
            return "";
        }
        else {
            return strValue;
        }
    }
    else {
        return "";
    }
}
function GetControlInnerHtml(obj) {

    if (obj != null) {
        return trim(obj.innerHTML);
    }
    else {
        return "";
    }
}
function GetSelectedOption(obj) {
    if (obj != null && obj.options.length > 0) {
        return obj.options[obj.selectedIndex].value;
    }
    else {
        return "";
    }
}
function GetSelectedOptionText(obj) {
    if (obj != null && obj.options.length > 0) {
        return obj.options[obj.selectedIndex].text;
    }
    else {
        return "";
    }
}
function InitializeWaterMark(ctrlName, strDefaultValue, ctrlType) {
    //Set Initia value
    var ctrl = document.getElementById(ctrlName);
    if (ctrl != null) {
        if (ctrlType == "input") {
            // Define what happens when the textbox comes under focus
            // Remove the watermark class and clear the box
            $("#" + ctrlName).focus(function () {
                $(this).filter(function () {
                    // We only want this to apply if there's not 
                    // something actually entered

                    $(this).removeClass("watermarkOn");

                    return $(this).val() == "" || $(this).val() == strDefaultValue;
                }).val("");

            });

            // Define what happens when the textbox loses focus
            // Add the watermark class and default text
            $("#" + ctrlName).blur(function () {
                $(this).filter(function () {
                    // We only want this to apply if there's not
                    // something actually entered
                    if ($(this).val() == "") {
                        $(this).addClass("watermarkOn");
                    }
                    else {
                        $(this).removeClass("watermarkOn");
                    }
                    return $(this).val() == "" || $(this).val() == GetDateMask();
                }).val(strDefaultValue);
            });

            if (GetControlValue(ctrl).length == 0) {
                $("#" + ctrlName).addClass("watermarkOn");
                ctrl.value = strDefaultValue;
            }
        }
        else if (ctrlType == "select") {
            var myDdl = document.getElementById(ctrlName);

            if (myDdl.options[0].value.length == 0) {
                if (myDdl.options[0].innerHTML.length == 0) {
                    myDdl.options[0].innerHTML = strDefaultValue;
                }
            }
            else {
                myDdl.options[0].className = "";
            }

            for (var i = 1; i < myDdl.options.length; i++) {
                myDdl.options[i].className = "watermarkOff";
            }

            if (myDdl.selectedIndex == 0 && GetSelectedOption(myDdl).length == 0) {
                $("#" + ctrlName).addClass("watermarkOn");
            }

            $("#" + ctrlName).change(function () {
                if (myDdl.selectedIndex == 0 && GetSelectedOption(myDdl).length == 0) {
                    $(this).addClass("watermarkOn");
                }
                else {
                    $(this).removeClass("watermarkOn");
                }
            });
        }
        else if (ctrlType == "password") {


        }
    }
}
function ContainWhiteSpace(str) {
    var whiteSpaceExp = /\s/g;
    if (whiteSpaceExp.test(str))
        return true;
    else
        return false;
}
function GetDateMask() {

    var strMask = "";
    for (var i = 0; i < strDateFormat.length; i++) {
        if (strDateFormat.charAt(i) == "*" || strDateFormat.charAt(i) == "9") {
            strMask = strMask + "_";
        }
        else {
            strMask = strMask + strDateFormat.charAt(i);
        }
    }
    return strMask;
}

function SubmitAnalyticPageThankYou(subtype) {
    //google analytic
    if (UseGoogleManager() == true) {
        SubmitAnalyticPageThankYou_tagManager(subtype);
    }
    else {
        SubmitAnalyticPageThankYou_analytic();
    }
}
function SubmitAnalyticPageThankYou_tagManager(subtype) {

    var obj = document.getElementsByName("FlightFareAmount");
    var objTax = document.getElementsByName("TaxAmount");
    var tax = 0.00;
    var objCharge_name = document.getElementsByName("hdCharge_name");

    var BagAmount = document.getElementsByName("BagAmount");
    var BagDisplay_name = document.getElementsByName("BagDisplay_name");
    var BagCount = document.getElementsByName("BagCount");

    var SeatAmount = document.getElementsByName("SeatAmount");
    var SeatDisplay_name = document.getElementsByName("SeatDisplay_name");
    var SeatCount = document.getElementsByName("SeatCount");

    var OtherAmount = document.getElementsByName("OtherAmount");
    var OtherDisplay_name = document.getElementsByName("OtherDisplay_name");

    var objAirport = document.getElementsByName("hdFlightCode");
    var objFlightNumber = document.getElementsByName("hdFlightNumber");
    var objTicketNumber = document.getElementsByName("hdTicketNumber");
    var objTicketTotal = document.getElementsByName("hdTicketTotal");
    var AllTicketTotal = document.getElementsByName("hdAllTicketTotal");
    var TicketCount = document.getElementsByName("hdTicketCount");
    var objOdAirport = document.getElementsByName("hdOdAirport");
    var objPaxNo = document.getElementsByName("hdPaxNo");
    var spnBookingId = document.getElementById("spnBookingId");
    var AgencyCode = document.getElementById("hdAgencyCode");
    var TransactionDate = document.getElementById("hdTransactionDate");

    //google analytic
    if (dataLayer)
        dataLayer.push({ 'event': 'ThankYouPage' });

    if (spnBookingId) {// check for wellnet

        var gtm_products = [];
        var strTempOdAirport = "";
        var strPrefix = "";
        var prodName = "";


        if (obj != null && objAirport != null && objFlightNumber != null && objOdAirport != null) {
            for (var i = 0; i < obj.length; i++) {
                //Identify whether it is outbound or inbound.
                if (strTempOdAirport != GetControlValue(objOdAirport[i])) {
                    if (strTempOdAirport.length == 0) {
                        //Outbound
                        strPrefix = "1";
                    }
                    else {
                        //Inbound
                        strPrefix = "2";
                    }
                    strTempOdAirport = GetControlValue(objOdAirport[i]);
                }
            }
        }

        if (strPrefix == "1") {
            prodName = strPrefix + strTempOdAirport.split('-')[0] + strTempOdAirport.split('-')[1];
        }
        else if (strPrefix == "2") {
            prodName = strPrefix + strTempOdAirport.split('-')[1] + strTempOdAirport.split('-')[0];
        }

        // Loop Tax Itin
        if (objTax != null) {
            for (var i = 0; i < objTax.length; i++) {
                tax = Number(tax) + Number(GetControlValue(objTax[i]));
            }
        }
        // Loop Ticket in Itin
        if (objTicketNumber != null) {
            for (var i = 0; i < objTicketNumber.length; i++) {
                // add TransactionProduct Data
                if (GetControlValue(AllTicketTotal[i]) > 0) {
                    gtm_products.push({
                        'name': prodName,
                        'sku': 'TKT',
                        'category': 'ticket',
                        'price': GetControlValue(AllTicketTotal[i]),
                        'quantity': GetControlValue(TicketCount[i])
                    });
                }
            }
        }

        // Loop Bag
        if (BagAmount != null) {
            for (var i = 0; i < BagAmount.length; i++) {
                // add TransactionProduct Data
                if (GetControlValue(BagAmount[i]) > 0) {
                    gtm_products.push({
                        'name': prodName,
                        'sku': GetControlValue(BagDisplay_name[i]),
                        'category': 'fee',
                        'price': GetControlValue(BagAmount[i]),
                        'quantity': GetControlValue(BagCount[i])
                    });
                }
            }
        }

        // Loop Seat
        if (SeatAmount != null) {
            for (var i = 0; i < SeatAmount.length; i++) {
                // add TransactionProduct Data
                if (GetControlValue(SeatAmount[i]) > 0) {
                    gtm_products.push({
                        'name': prodName,
                        'sku': GetControlValue(SeatDisplay_name[i]),
                        'category': 'fee',
                        'price': GetControlValue(SeatAmount[i]),
                        'quantity': GetControlValue(SeatCount[i])
                    });
                }
            }
        }

        // Loop Other
        if (OtherAmount != null) {
            for (var i = 0; i < OtherAmount.length; i++) {
                // add TransactionProduct Data
                gtm_products.push({
                    'name': prodName,
                    'sku': GetControlValue(OtherDisplay_name[i]),
                    'category': 'fee',
                    'price': GetControlValue(OtherAmount[i]),
                    'quantity': '1'
                });
            }
        }

        // add data layer
        dataLayer.push({
            'transactionId': document.getElementById("spnBookingId").innerHTML,
            'transactionDate': TransactionDate.value,
            'transactionAffiliation': AgencyCode.value,
            'transactionTotal': GetControlValue(document.getElementById("hdTitcketTotal")),
            'transactionTax': tax,
            'transactionShipping': '',
            'transactionPaymentType': subtype,
            'transactionCurrency': document.getElementById("hdCurrency_rcd").value,
            'transactionPromoCode': '',
            'transactionProducts': gtm_products,
            'event': 'trackTrans'
        });


    } // end check for wellnet
}

function SubmitAnalyticPageThankYou_analytic() {
    _gaq.push(['_addTrans',
            document.getElementById("spnBookingId").innerHTML, // this is the ID wich you use in your system and should be unique 
    // you could use for instance the first ticket number
            'Peach Airlines',
            GetControlValue(document.getElementById("hdTitcketTotal")), // the total price of the transaction 
            '', // you could provide tax information or leave blank
            '', // there are no shipping costs, so leave blank
            GetControlValue(document.getElementById("hdBookingCity")), // city of the customer
            '', // state or province, if not available leave blank
            GetControlValue(document.getElementById("hdBookingCountry")) // country of the customer
    ]);

    // add item might be called for every item in the shopping cart 
    // where your ecommerce engine loops through each item in the cart and 
    // prints out _addItem for each 
    var obj = document.getElementsByName("FlightFareAmount");
    var objAirport = document.getElementsByName("hdFlightCode");
    var objFlightNumber = document.getElementsByName("hdFlightNumber");
    var objOdAirport = document.getElementsByName("hdOdAirport");

    var strTempOdAirport = "";
    var strPrefix = "";
    if (obj != null && objAirport != null && objFlightNumber != null && objOdAirport != null) {
        for (var i = 0; i < obj.length; i++) {

            //Identify whether it is outbound or inbound.
            if (strTempOdAirport != GetControlValue(objOdAirport[i])) {
                if (strTempOdAirport.length == 0) {
                    //Outbound
                    strPrefix = "1";
                }
                else {
                    //Inbound
                    strPrefix = "2";
                }
                strTempOdAirport = GetControlValue(objOdAirport[i]);
            }

            //Sumit analytic code.
            _gaq.push(['_addItem',
            document.getElementById("spnBookingId").innerHTML, // this is the same id as above 
            strPrefix + GetControlValue(objAirport[i]), // SKU/code, in our case the flight route, unless
            // you have a route identifier 
            '', // if you have a route-id, then put the above flight route here, else leave blank
            GetControlValue(objFlightNumber[i]),   // flight number of the outbound flight
            GetControlValue(obj[i]),          // unit price of the flight 
            '1'               // quantity should always be 1, since a ticket is always for one person
            ]);
        }
        _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

        //Google Analytic  (2)
        if (_gaq) {
            _gaq.push(['_trackPageview', '/booking/thank_you-page.html']);
        }

    }
}


function ChangeToDateString(dt) {

    var flightDate = "";
    var d = dt.getDate().toString();
    var m = (dt.getMonth() + 1).toString();

    flightDate = dt.getFullYear().toString();
    if (m.length == 1) {
        flightDate = flightDate + "0" + m;
    }
    else {
        flightDate = flightDate + m;
    }

    if (d.length == 1) {
        flightDate = flightDate + "0" + d;
    }
    else {
        flightDate = flightDate + d;
    }
    return flightDate;
}

//*************************************************************************
//  Age culculation.
function DaysInMonth(Y, M) {
    with (new Date(Y, M, 1, 12)) {
        setDate(0);
        return getDate();
    }
}
function GetAge(date1, date2) {
    var y1 = date1.getFullYear(),
        m1 = date1.getMonth(),
        d1 = date1.getDate(),
	    y2 = date2.getFullYear(),
        m2 = date2.getMonth(),
        d2 = date2.getDate();

    if (d1 < d2) {
        m1--;
        d1 += DaysInMonth(y2, m2);
    }
    if (m1 < m2) {
        y1--;
        m1 += 12;
    }
    return [y1 - y2, m1 - m2, d1 - d2];
}
function CalculateAge(strDate) {

    if (strDate.length > 0) {
        var dat = new Date();
        var curday = dat.getDate();
        var curmon = dat.getMonth() + 1;
        var curyear = dat.getFullYear();

        var arrCurrentDate = ReformatDate(strDate).split("-");
        var calday = arrCurrentDate[2];
        var calmon = arrCurrentDate[1];
        var calyear = arrCurrentDate[0];
        if (curday == "" || curmon == "" || curyear == "" || calday == "" || calmon == "" || calyear == "") {
            return "";
        }
        else {
            var curd = new Date(curyear, curmon - 1, curday);
            var cald = new Date(calyear, calmon - 1, calday);
            var diff = Date.UTC(curyear, curmon, curday, 0, 0, 0) - Date.UTC(calyear, calmon, calday, 0, 0, 0);
            var dife = GetAge(curd, cald);

            return "{\"year\":\"" + dife[0] + "\", \"month\":\"" + dife[1] + "\", \"day\":\"" + dife[2] + "\"}";
        }
    }
}

//*************************************************************************************************
//  Confirm And Alert Function

function ShowConfirm(strMessage, strParameter, fnc) {
    var objdivConfirmBtm = document.getElementById("dvConfirmOK");
    if (objdivConfirmBtm != null) {
        objdivConfirmBtm.style.display = "block";
    }

    var objDvCloseMessage = document.getElementById("dvMessageButtonText");
    if (objDvCloseMessage != null) {
        objDvCloseMessage.innerHTML = objLanguage.default_value_13;
    }

    ShowMessageBox(strMessage, 0, "");
    $("#dvConfirmOK").unbind();
    jQuery("#dvConfirmOK").bind("click",
                                strParameter,
                                fnc);
    //Scroll to the top page
    // scroll(0, 0);
}

function SetDefaltAlertMessage() {
    //Close Conform Button.
    var objdivConfirmBtm = document.getElementById("dvConfirmOK");
    if (objdivConfirmBtm != null) {
        objdivConfirmBtm.style.display = "none";
    }

    //Change back close button of alert message to "OK"
    var objDvCloseMessage = document.getElementById("dvMessageButtonText");
    if (objDvCloseMessage != null) {
        objDvCloseMessage.innerHTML = objLanguage.default_value_11;
    }
}
function HideHeaderMenu(bHide) {
    var objHeaderMenu = document.getElementById("dvHeaderMenu");
    if (objHeaderMenu != null) {
        if (bHide == true)
        { objHeaderMenu.style.display = "none"; }
        else
        { objHeaderMenu.style.display = "block"; }
    }
}
//-------------------------------------------------------
// Click To Expand For Happy Plus
//-------------------------------------------------------
function ClickToExpand() {
    $(function () {
        $("#tabs").tabs({
            selected: -1,
            collapsible: true
        });

        $(document).click(function (e) {
            if ($(e.target).parents('#tabs').length == 0) {
                $('#tabs ul li.ui-state-active a').trigger('click');
            }
        });
    });

}

//-------------------------------------------------------
// End Click To Expand For Happy Plus
//-------------------------------------------------------

function UseGoogleManager() {
    var hostname = document.location.hostname.toLowerCase();
    if (hostname.indexOf("test") != -1 || hostname.indexOf("flywithstyle") != -1 || hostname.indexOf("localhost") != -1) {
        return true;
    }
    else {
        return false;
    }
}
function isValidJson(json) {
    try {
        JSON.parse(json);
        return true;
    } catch (e) {
        return false;
    }
}
var MultipleTab = (function (window) {

    function GeterateRamdom() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    function guid() {
        return GeterateRamdom() + GeterateRamdom() + '-' + GeterateRamdom() + '-' + GeterateRamdom() + '-' + GeterateRamdom() + '-' + GeterateRamdom() + GeterateRamdom() + GeterateRamdom();
    }

    function AssignInitialTab(sessionID, bForceAssign) {
        var arrLocalValue = localStorage.getItem("TabID");
        var strSessionId = "";

        if (isValidJson(arrLocalValue)) {

            if (arrLocalValue != null && arrLocalValue.length > 0) {
                arrLocalValue = JSON.parse(arrLocalValue);
                if (arrLocalValue != null && arrLocalValue.length > 0) {
                    if (arrLocalValue[0].id != undefined) {
                        var arr = arrLocalValue[0].id.split("|");
                        strSessionId = arr[0];
                    }
                }
            }

            var generateGUID = guid();
            var arrLocal;
            var obj;

            if (strSessionId != sessionID || bForceAssign == true) {
                //Remove all array value when first load to clear Temp.
                localStorage.clear();
                //Create array of localstorage.
                arrLocal = new Array();
                obj = new Object();

                obj.id = sessionID + "|" + generateGUID;
                obj.sessionId = sessionID + "|" + generateGUID;
                obj.main = true;
                arrLocal.push(obj);

                obj = null;
                sessionStorage.setItem("TabID", sessionID + "|" + generateGUID);
            }
            else {
                arrLocal = JSON.parse(localStorage.getItem("TabID"));
                if (arrLocal != null && arrLocal.length > 0) {
                    //Find is main available.
                    var bFindMain = false;
                    for (var i = 0; i < arrLocal.length; i++) {
                        if (arrLocal[i].main == true) {
                            if (arrLocal[i].sessionId == sessionStorage.getItem("TabID") &&
                                arrLocal[i].id == sessionStorage.getItem("TabID")) {
                                break;
                            }
                        }
                        else {

                            //Clear session storage.
                            localStorage.clear();
                            arrLocal = [];

                            var globalId = sessionID + "|" + guid();
                            obj = new Object();

                            obj.id = globalId;
                            obj.sessionId = globalId;
                            obj.main = true;
                            arrLocal.push(obj);

                            sessionStorage.setItem("TabID", globalId);
                            obj = null;
                            break;
                        }
                    }
                }
            }
            localStorage.setItem("TabID", JSON.stringify(arrLocal));
            arrLocal = null;
        }
        else {
            ShowMessageBox("Invalid JSON Format", 0, '');
        }
    }

    function IsCorrectTab() {
        var arrLocal = JSON.parse(localStorage.getItem("TabID"));

        if (sessionStorage.getItem("TabID") != null) {
            for (var i = 0; i < arrLocal.length; i++) {
                if (arrLocal[i].sessionId == sessionStorage.getItem("TabID") &&
                    arrLocal[i].id == sessionStorage.getItem("TabID")) {
                    if (arrLocal[i].main == true) {
                        return true;
                    }
                }
                else {

                    return false;
                }
            }
        }

        //Assign New ID
        if (arrLocal.length > 0) {
            var globalId = arrLocal[0].id;
            AssignInitialTab(globalId.split("|")[0], true);
        }

        return true;
    }

    function langCode() {
        var tmpLangcode = "en";
        if (LanguageCode.split("-").length == 2)
            tmpLangcode = LanguageCode.split("-")[0];
        return tmpLangcode;
    }
    function Redirect() {
        window.location = "MultiTabAlert.aspx";
    }

    function ClearID() {

        if (localStorage.getItem("TabID") != null) {
            //Get Array of local storage.
            var arrLocal = JSON.parse(localStorage.getItem("TabID"));
            for (var i = 0; i < arrLocal.length; i++) {
                if (arrLocal[i].sessionId == sessionStorage.getItem("TabID") &&
                    arrLocal[i].id == sessionStorage.getItem("TabID")) {

                    arrLocal[i].main = false;
                    //sessionStorage.removeItem("TabID")
                    localStorage.setItem("TabID", JSON.stringify(arrLocal));

                    break;
                }
            }

        }
    }
    return {
        AssignInitialTab: AssignInitialTab,
        IsCorrectTab: IsCorrectTab,
        ClearID: ClearID,
        Redirect: Redirect

    };
})(window);

function LoadMyBooking() {
    if (MultipleTab.IsCorrectTab()) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadMyBooking(SuccessLoadMyBooking, showError, showTimeOut);
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}

function SuccessLoadMyBooking(result) {
    if (result == "{002}") {
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        var objContainer = document.getElementById("dvContainer");

        objContainer.innerHTML = result;

        DisplayQuoteSummary("", "", "");
        ShowSearchPannel(true);
        ShowProgressBar(false);
        objContainer = null;
    }
}
function LoadBookingDetail(bookingId, errorMSG) {
    if (bookingId == '')
        ShowMessageBox(errorMSG, 0, '');
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadBookingDetail(bookingId, SuccessLoadBookingDetail, showError, showTimeOut);
    }
}
function SuccessLoadBookingDetail(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objContainer = document.getElementById("dvContainer");

            objContainer.innerHTML = result;

            ShowSearchPannel(false);
            window.scrollTo(0, 0);
            objContainer = null;
        }
    }
}
function GetPagingFFP(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingFFP(pageindex, SuccessGetPaging, showError, showTimeOut);
}

function GetHistoryBooking(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingBooking(pageindex, 'true', SuccessGetPaging, showError, showTimeOut);
}

function SuccessGetPaging(result) {
    ShowProgressBar(false);
    var objContainer = document.getElementById("ctl00_dvHistory");

    objContainer.innerHTML = result;
    objContainer = null;
}

function GetLifeBooking(pageindex) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPagingBooking(pageindex, 'false', SuccessGetLifeBooking, showError, showTimeOut);
}

function SuccessGetLifeBooking(result) {
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            ShowProgressBar(false);
            var objContainer = document.getElementById("ctl00_dvLifeBooking");

            objContainer.innerHTML = result;
            objContainer = null;
        }
    }
}
function ShowHistory() {
    HideLiveBooking();
    document.getElementById("historyBooking").style.display = "block";
    document.getElementById("HistoryTab1").className = 'TabConererLeft';
    document.getElementById("HistoryTab2").className = 'TabConererContent';
    document.getElementById("HistoryTab3").className = 'TabConererRight';

}

function HideHistory() {
    document.getElementById("historyBooking").style.display = "none";
    document.getElementById("HistoryTab1").className = 'TabActiveConererLeft';
    document.getElementById("HistoryTab2").className = 'TabActiveConererContent';
    document.getElementById("HistoryTab3").className = 'TabActiveConererRight';
}

function ShowLiveBooking() {
    document.getElementById("liveBooking").style.display = "block";
    document.getElementById("LiveTab1").className = 'TabConererLeft';
    document.getElementById("LiveTab2").className = 'TabConererContent';
    document.getElementById("LiveTab3").className = 'TabConererRight';

    HideHistory();
}

function HideLiveBooking() {
    document.getElementById("liveBooking").style.display = "none";
    document.getElementById("LiveTab1").className = 'TabActiveConererLeft';
    document.getElementById("LiveTab2").className = 'TabActiveConererContent';
    document.getElementById("LiveTab3").className = 'TabActiveConererRight';
}
function MyBookingSearch() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.MyBookingSearch(document.getElementById('ctl00_txtBookingCode').value, SuccessMyBookingSearch, showError, showTimeOut);
}
function SuccessMyBookingSearch(result) {
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var objContainer = document.getElementById("dvSearch");
            var objBox = document.getElementById("dvResultBox");
            var objBookingBox = document.getElementById("dvBookingBox");

            objBox.style.display = 'block';
            objBookingBox.style.display = 'none';
            objContainer.innerHTML = result;

            ShowSearchPannel(false);
            ShowProgressBar(false);
            objContainer = null;
        }
    }

}
function SubmitEnterSearch(myfield, e) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        MyBookingSearch();
        return false;
    }
    else
        return true;
}

function GetNewsRegisterDetail() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetNewsRegisterDetail(SuccessGetNewsRegisterDetail, showError, showTimeOut);
}

function SuccessGetNewsRegisterDetail(result) {
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;

    scroll(0, 0);

    ShowPannel(false);
    ShowProgressBar(false);
    obj = null;
}

function GetNewsAdmin() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetNewsAdmin(SuccessGetNewsAdmin, showError, showTimeOut);
}

function SuccessGetNewsAdmin(result) {
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;

    scroll(0, 0);

    ShowProgressBar(false);
    obj = null;
}

function SearchNewsRegister(page) {
    var strResult = SearchNewsRegisterCondition();
    if (strResult.length > 0) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SearchNewsRegister(strResult, page, SuccessSearchNewsRegister, showError, showTimeOut);
    }
}

function SuccessSearchNewsRegister(result) {
    var obj = document.getElementById(FindControlName("div", "dvSearchResult")); //document.getElementById("dvContainer"); 
    obj.innerHTML = result;

    scroll(0, 0);

    ShowProgressBar(false);
    obj = null;
}

function SearchNewsRegisterCondition() {
    var strCountry = "";
    var objCountry = document.getElementsByName("chkCountry");
    for (var i = 0; i < objCountry.length; i++) {
        if (objCountry[i].checked == true) {
            if (strCountry != "") {
                strCountry = strCountry + " or ";
            }
            strCountry = strCountry + "(Country_name like '%" + objCountry[i].value + "%')";
        }
    }

    var strDeparture = "";
    var objDeparture = document.getElementsByName("chkListDeparture");
    for (var i = 0; i < objDeparture.length; i++) {
        if (objDeparture[i].checked == true) {
            if (strDeparture != "") {
                strDeparture = strDeparture + " or ";
            }
            strDeparture = strDeparture + "(Departure_name like '%" + objDeparture[i].value + "%')";
        }
    }

    var strDestination = "";
    var objDestination = document.getElementsByName("chkListDestination");
    for (var i = 0; i < objDestination.length; i++) {
        if (objDestination[i].checked == true) {
            if (strDestination != "") {
                strDestination = strDestination + " or ";
            }
            strDestination = strDestination + "(Destination_name like '%" + objDestination[i].value + "%')";
        }
    }

    var travelfor = "";
    var objTravelfor = document.getElementsByName("rdbTravelfor");
    for (var i = 0; i < objTravelfor.length; i++) {
        if (objTravelfor[i].checked == true) {
            travelfor = " Travel_for = '" + objTravelfor[i].value + "' ";
            i = objTravelfor.length;
        }
    }

    var havechildren = "";
    var objHaschild = document.getElementsByName("rdbHaschild");
    for (var i = 0; i < objHaschild.length; i++) {
        if (objHaschild[i].checked == true) {
            havechildren = " Have_childern ='" + objHaschild[i].value + "' ";
            i = objHaschild.length;
        }
    }

    var strEmail = "";
    var objEmail = document.getElementById(FindControlName("input", "txtEmail"));
    if (objEmail.value.length != 0) {
        strEmail = " (Register_email like '%" + objEmail.value + "%') ";
    }

    var strEmailFlag = "";
    var objEmailFlag = document.getElementById(FindControlName("select", "ddlStatus"));
    if (objEmailFlag.value != "-1") {
        if (objEmailFlag.value == "1") {
            strEmailFlag = "(email_flag ='1')";
        }
        else {
            strEmailFlag = "(email_flag is null)";
        }
    }

    var strDateRenge = "";
    var objDateRegis = document.getElementById("chkDateRegis");
    if (objDateRegis.checked == true) {
        strDateRenge = " Register_date between  #" + NewsRegisterGetDate('3') + "# and #" + NewsRegisterGetDate('4') + "# ";
    }

    ////////////////////////////////////////  end of 11111111111        
    var strCondition = "";
    ////////////////////////////////////////  start of 2222222222   
    if (strCountry != "") {
        strCondition = strCondition + " and (" + strCountry + ")";
    }
    if (strDeparture != "") {
        strCondition = strCondition + " and (" + strDeparture + ")";
    }
    if (strDestination != "") {
        strCondition = strCondition + " and (" + strDestination + ")";
    }
    if (travelfor != "") {
        strCondition = strCondition + " and (" + travelfor + ")";
    }
    if (havechildren != "") {
        strCondition = strCondition + " and (" + havechildren + ")";
    }
    if (strEmail != "") {
        strCondition = strCondition + " and (" + strEmail + ")";
    }
    if (strEmailFlag != "") {
        strCondition = strCondition + " and (" + strEmailFlag + ")";
    }
    if (strDateRenge != "") {
        strCondition = strCondition + " and (" + strDateRenge + ")";
    }

    ////////////////////////////////////////  end of 22222222222222 
    if (strCondition != "") {
        strCondition = " where 1=1 " + strCondition;
    }
    ////////////////////////////////////////  start of 33333333 

    objCountry = null;
    objDeparture = null;
    objDestination = null;
    objTravelfor = null;
    objHaschild = null;
    objEmail = null;

    return strCondition;
}

function NewsRegisterGetDate(o) {
    var ddlMY = document.getElementById('ddlMY_' + o);
    var ddlDate = document.getElementById('ddlDate_' + o);
    return ddlMY.options[ddlMY.selectedIndex].value.substring(0, 4) + '-' + ddlMY.options[ddlMY.selectedIndex].value.substring(4) + '-' + ddlDate.options[ddlDate.selectedIndex].value;
}

function validDepartureAll(flagchk) {
    var items = document.getElementsByTagName("INPUT");
    for (var i = 0; i < items.length; i++) {
        if (items[i].id.indexOf('chkListDeparture_') == 0) {
            items[i].checked = flagchk;
        }
    }
}

function validDestinationsAll(flagchk) {
    var items = document.getElementsByTagName("INPUT");
    for (var i = 0; i < items.length; i++) {
        if (items[i].id.indexOf('chkListDestination_') == 0) {
            items[i].checked = flagchk;
        }
    }
}

function validCountryAll(flagchk) {
    var items = document.getElementsByTagName("INPUT");
    for (var i = 0; i < items.length; i++) {
        if (items[i].id.indexOf('chkCountry_') == 0) {
            items[i].checked = flagchk;
        }
    }
}

function validSearchAll(flagchk) {
    var items = document.getElementsByTagName("INPUT");
    for (var i = 0; i < items.length; i++) {
        if (items[i].id.indexOf('chkSearchAll_') == 0) {
            items[i].checked = flagchk;
        }
    }
}
//* End Yua Section

function ShowNewsRegisterInfoError(strMessage) {
    var objError = document.getElementById("dvPaxError");
    objError.innerHTML = strMessage;
    objError = null;
}

function SaveNewsRegisterDetail() {
    var strResult = FillNewsRegisterDetail(); //Validate and fill data.

    if (strResult.length > 0) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.FillNewsRegisterData(strResult, SuccessSaveNewsRegisterDetail, showError, showTimeOut);
    }
}

function SuccessSaveNewsRegisterDetail(result) {
    var objDivInput = document.getElementById("dvInput");
    var objDivResultComplete = document.getElementById("dvResultComplete");

    document.getElementById("submitXML").innerHTML = "";
    if (result == "complete") {
        objDivInput.style.display = "none";
        objDivResultComplete.style.display = "block";
    }
    else {
        document.getElementById("submitXML").innerHTML = result;
    }

    scroll(0, 0);

    ShowProgressBar(false);
    objDivInput = null;
    objDivResultComplete = null;
}

function FillNewsRegisterDetail() {
    var bPass = true;

    document.getElementById("spErrName").innerHTML = "*";
    document.getElementById("spErrEmail").innerHTML = "*";
    document.getElementById("spErrPostcode").innerHTML = "*";
    document.getElementById("spErrCountry").innerHTML = "*";
    document.getElementById("spErrDeparture").innerHTML = "";
    document.getElementById("spErrDestination").innerHTML = "";

    var objName = document.getElementById(FindControlName("input", "txtName"));
    var objEmail = document.getElementById(FindControlName("input", "txtEmail"));
    var objPostcode = document.getElementById(FindControlName("input", "txtPostcode"));
    var objCountry = document.getElementById(FindControlName("select", "ddlCountry"));

    var objIPaddress = document.getElementById(FindControlName("input", "txtIPaddress"));
    var objBrowser = document.getElementById(FindControlName("input", "txtBrowser"));

    var strDeparture = "";
    var objDeparture = document.getElementsByName("chkListDeparture");

    for (var i = 0; i < objDeparture.length; i++) {
        if (objDeparture[i].checked == true) {
            if (strDeparture != "") {
                strDeparture = strDeparture + ",";
            }
            strDeparture = strDeparture + objDeparture[i].value;
        }
    }

    var strDestination = "";
    var objDestination = document.getElementsByName("chkListDestination");
    for (var i = 0; i < objDestination.length; i++) {
        if (objDestination[i].checked == true) {
            if (strDestination != "") {
                strDestination = strDestination + ",";
            }
            strDestination = strDestination + objDestination[i].value;
        }
    }

    var travelfor = "";
    var objTravelfor = document.getElementsByName("rdbTravelfor");
    for (var i = 0; i < objTravelfor.length; i++) {
        if (objTravelfor[i].checked == true) {
            travelfor = objTravelfor[i].value;
            i = objTravelfor.length;
        }
    }

    var havechildren = "";
    var objHaschild = document.getElementsByName("rdbHaschild");
    for (var i = 0; i < objHaschild.length; i++) {
        if (objHaschild[i].checked == true) {
            havechildren = objHaschild[i].value;
            i = objHaschild.length;
        }
    }
    //objDestination[1].checked = true; //yua

    var strXml = "";
    var strError = "";

    //Add Contact detail information to xml.

    if (objName.value.length == 0) {
        bPass = false;
        document.getElementById("spErrName").innerHTML = "* Name required. !!";
    }
    if (objEmail.value.length == 0) {
        bPass = false;
        document.getElementById("spErrEmail").innerHTML = "* Email required. !!";
    }
    else if (ValidEmail(objEmail.value) == false) {
        bPass = false;
        document.getElementById("spErrEmail").innerHTML = "* Supply valid Email address. !!";
    }
    if (objPostcode.value.length == 0) {
        bPass = false;
        document.getElementById("spErrPostcode").innerHTML = "* Post code required. !!";
    }
    if (objCountry.value == "-1") {
        bPass = false;
        document.getElementById("spErrCountry").innerHTML = "* Please select your country. !!";
    }
    if (strDeparture == "") {
        bPass = false;
        document.getElementById("spErrDeparture").innerHTML = "* Select at least one departure point. !!";
    }
    if (strDestination == "") {
        bPass = false;
        document.getElementById("spErrDestination").innerHTML = "* Select at least one destination for receiving news & offers. !!";
    }

    if (bPass == true) {
        strXml = "<register>" +
                    "<contact>" +
                        "<name>" + objName.value + "</name>" +
                        "<email>" + objEmail.value + "</email>" +
                        "<postcode>" + objPostcode.value + "</postcode>" +
                        "<country>" + objCountry.options[objCountry.selectedIndex].text + "</country>" +
                        "<departure>" + strDeparture + "</departure>" +
                        "<destination>" + strDestination + "</destination>" +
                        "<travelfor>" + travelfor + "</travelfor>" +
                        "<havechildren>" + havechildren + "</havechildren>" +
                        "<ipaddress>" + objIPaddress.value + "</ipaddress>" +
                        "<browser>" + objBrowser.value + "</browser>" +
                    "</contact>" +
               "</register>";
    }
    else {
        //Failed criteria check go out of the loop.
        ShowNewsRegisterInfoError(strError);
        strXml = "";
    }

    //document.getElementById("submitXML").innerHTML = strXml; 

    objName = null;
    objEmail = null;
    objPostcode = null;
    objCountry = null;
    objIPaddress = null;
    objBrowser = null;
    objDeparture = null;
    objDestination = null;

    return strXml;
}

function ShowUnsubscribe() {
    var objDivInput = document.getElementById("dvInput");
    var objDivUnsubscribe = document.getElementById("dvUnsubscribe");

    objDivInput.style.display = "none";
    objDivUnsubscribe.style.display = "block";

    scroll(0, 0);

    objDivInput = null;
    objDivUnsubscribe = null;
}


function SaveUnsubscribe() {
    var bPass = true;
    document.getElementById("spErrEmailUnsubscribe").innerHTML = "*";

    var objEmail = document.getElementById(FindControlName("input", "txtEmailUnsubscribe"));
    if (objEmail.value.length == 0) {
        bPass = false;
        document.getElementById("spErrEmailUnsubscribe").innerHTML = "* Email required. !!";
    }
    else if (ValidEmail(objEmail.value) == false) {
        bPass = false;
        document.getElementById("spErrEmailUnsubscribe").innerHTML = "* Supply valid Email address. !!";
    }

    if (bPass == true) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SaveUnsubscribe(objEmail.value, SuccessSaveUnsubscribe, showError, showTimeOut);
    }
}

function SuccessSaveUnsubscribe(result) {
    var objDivUnsubscribe = document.getElementById("dvUnsubscribe");
    var objDivResultComplete = document.getElementById("dvResultUnsubscribe");

    if (result == "complete") {
        objDivUnsubscribe.style.display = "none";
        objDivResultComplete.style.display = "block";
    }

    scroll(0, 0);

    ShowProgressBar(false);
    objDivUnsubscribe = null;
    objDivResultComplete = null;
}

function NewsAdminLogin() {
    var strResult = chkNewsAdminLogin(); //Validate and fill data.

    if (strResult.length > 0) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.NewsAdminLogin(strResult, SuccessNewsAdminLogin, showError, showTimeOut);
    }
}

function SuccessNewsAdminLogin(result) {
    var objDivContainer = document.getElementById("dvContainer");
    var objDivResult = document.getElementById("dvSubContainer");

    if (result == "complete") {
        objDivContainer.innerHTML = objDivResult.innerHTML;
    }
    else {
        ShowNewsRegisterInfoError(result);
    }

    scroll(0, 0);

    ShowProgressBar(false);
    objDivContainer = null;
    objDivResult = null;
}

function SubmitNewsAdminLogin(myfield, e) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        NewsAdminLogin();
        return false;
    }
    else
        return true;
}
function chkNewsAdminLogin() {
    var bPass = true;

    document.getElementById("spErrUserId").innerHTML = "*";
    document.getElementById("spErrPassword").innerHTML = "*";

    var objUserId = document.getElementById(FindControlName("input", "txtUserId"));
    var objPassword = document.getElementById(FindControlName("input", "txtPassword"));

    var strXml = "";
    var strError = "";
    ShowNewsRegisterInfoError(strError);

    //Add Contact detail information to xml.

    if (objUserId.value.length == 0) {
        bPass = false;
        document.getElementById("spErrUserId").innerHTML = "* User Id required!!";
    }
    if (objPassword.value.length == 0) {
        bPass = false;
        document.getElementById("spErrPassword").innerHTML = "* Password required !!";
    }

    if (bPass == true) {
        strXml = "<newsLogin>" +
                        "<UserId>" + objUserId.value + "</UserId>" +
                        "<Password>" + objPassword.value + "</Password>" +
                 "</newsLogin>";
    }
    else {
        //Failed criteria check go out of the loop.
        ShowNewsRegisterInfoError(strError);
        strXml = "";
    }

    //document.getElementById("submitXML").innerHTML = strXml; 

    objUserId = null;
    objPassword = null;

    return strXml;
}

var numOfselected = 0;
function GetEditResgisterID() {
    numOfselected = 0;
    var strRegister_id = "";
    var objRegister_id = document.getElementsByName("chkRegister_id");

    for (var i = 0; i < objRegister_id.length; i++) {
        if (objRegister_id[i].checked == true) {
            if (strRegister_id != "") {
                strRegister_id = strRegister_id + ",";
            }
            numOfselected = numOfselected + 1;
            strRegister_id = strRegister_id + objRegister_id[i].value;
        }
    }

    return strRegister_id;
}

function DeleteNewsRegister() {
    var strRegister_id = GetEditResgisterID();

    objRegister_id = null;
    if (strRegister_id.length > 0) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.DeleteNewsRegister(strRegister_id, SuccessDeleteNewsRegister, showError, showTimeOut);
    }
}

function SuccessDeleteNewsRegister(result) {
    ShowProgressBar(false);
    SearchNewsRegister();
}

function SendMailNewsRegister() {
    OpenCenterPopUp('..\MailForm.aspx', 'popup', '750', '350');
}

function OpenCenterPopUp(url, windowName, intPopUpWidth, intPopUpHeight) {
    var winl = (screen.width - intPopUpWidth) / 2;
    var wint = (screen.height - intPopUpHeight) / 2;
    window.open(
			url,
			windowName,
			"'status=yes,scrollbars=yes,resizable=yes,top=" + wint + ",left=" + winl + ",width=" + intPopUpWidth + ",height=" + intPopUpHeight + "'");
}

function GetNewsMailForm() {
    var objDivContainer = document.getElementById("dvContainer");
    var objDivInputMail = document.getElementById("dvInputMail");
    var objDivTemp = document.getElementById("dvTemp");

    document.getElementById("txtMailFrom").value = document.getElementById("SenderEmailFrom").value;
    document.getElementById("txtMailTo").value = document.getElementById("SenderEmailTo").value;

    document.getElementById("txtMailBCC").value = GetEditResgisterID();

    document.getElementById("txtMailAttachFile").value = "";
    document.getElementById("txtMailSubject").value = "";
    document.getElementById("txtMailMessage").value = "";

    var lblBcc = document.getElementById("lblBcc");
    if (numOfselected > 1) {
        lblBcc.innerHTML = numOfselected + " Receivers";
    }
    else {
        lblBcc.innerHTML = numOfselected + " Receiver";
    }

    objDivTemp.innerHTML = objDivContainer.innerHTML;
    objDivContainer.innerHTML = objDivInputMail.innerHTML;
    objDivInputMail.innerHTML = objDivTemp.innerHTML;
    objDivTemp.innerHTML = "";

    scroll(0, 0);
    lblBcc = null;
    objDivTemp = null;
    objDivContainer = null;
    objDivInputMail = null;
}

function SuccessGetNewsMailForm(result) {
    var obj = document.getElementById("dvSendmail");
    obj.innerHTML = result;

    scroll(0, 0);

    ShowProgressBar(false);
    obj = null;
}

function CloseMailForm() {
    var objDivResult = document.getElementById("dvContainer");
    var objDivSendmail = document.getElementById("dvInputMail");
    var objDivTemp = document.getElementById("dvTemp");

    objDivTemp.innerHTML = objDivResult.innerHTML;
    objDivResult.innerHTML = objDivSendmail.innerHTML;
    objDivSendmail.innerHTML = objDivTemp.innerHTML;
    objDivTemp.innerHTML = "";

    scroll(0, 0);
    objDivResult = null;
    objDivTemp = null;
}

function SendMailComplete() {
    var objDivContainer = document.getElementById("dvContainer");
    var objDivInputMail = document.getElementById("dvInputMail");
    var objDivResultMailComplete = document.getElementById("dvResultMailComplete");

    objDivInputMail.innerHTML = objDivContainer.innerHTML;
    objDivContainer.innerHTML = objDivResultMailComplete.innerHTML;
    objDivContainer = null;
    objDivInputMail = null;
    objDivResultMailComplete = null;
}

function NewsRegisterSendMail() {
    var strResult = FillNewsRegisterMail(); //Validate and fill data.

    if (strResult.length > 0) {
        ShowProgressBar(true);
        document.getElementById("txtMailAttachFile").value = "";
        document.getElementById("txtMailSubject").value = "";
        document.getElementById("txtMailMessage").value = "";
        tikAeroB2C.WebService.B2cService.NewsRegisterSendMail(strResult, SuccessNewsRegisterSendMail, showError, showTimeOut);
    }
}

function SuccessNewsRegisterSendMail(result) {
    ShowProgressBar(false);
    if (result == "complete") {
        SendMailComplete();
    }
    else {
        document.getElementById("spErrMailMessage").innerHTML = result;
    }
}

function FillNewsRegisterMail() {
    var bPass = true;

    document.getElementById("spErrMailFrom").innerHTML = "";
    document.getElementById("spErrMailTo").innerHTML = "";
    document.getElementById("spErrMailSubject").innerHTML = "";
    document.getElementById("spErrMailMessage").innerHTML = "";
    document.getElementById("spErrMailBCC").innerHTML = "";

    var objMailFrom = document.getElementById(FindControlName("input", "txtMailFrom"));
    var objMailTo = document.getElementById(FindControlName("input", "txtMailTo"));
    var objMailSubject = document.getElementById(FindControlName("input", "txtMailSubject"));
    var objMailMessage = document.getElementById(FindControlName("textarea", "txtMailMessage"));

    var objMailBcc = document.getElementById(FindControlName("input", "txtMailBCC"));
    var objMailAddBcc = document.getElementById(FindControlName("textarea", "txtMailAddBcc"));

    var strXml = "";
    var strError = "";

    if (objMailFrom.value.length == 0) {
        bPass = false;
        document.getElementById("spErrMailFrom").innerHTML = "<br>Mail From required. !!";
    }
    else if (ValidEmail(objMailFrom.value) == false) {
        bPass = false;
        document.getElementById("spErrMailFrom").innerHTML = "<br>Mail From is not valid Email address. !!";
    }
    if (objMailTo.value.length == 0) {
        bPass = false;
        document.getElementById("spErrMailTo").innerHTML = "<br>Mail To required. !!";
    }
    else if (ValidEmail(objMailTo.value) == false) {
        bPass = false;
        document.getElementById("spErrMailTo").innerHTML = "<br>Mail To is not valid Email address. !!";
    }

    var strMailAddBCC = objMailAddBcc.value;
    while (strMailAddBCC.indexOf(",") > 0) {
        strMailAddBCC = strMailAddBCC.replace(',', ';');
    }
    var strArrMailBCC = strMailAddBCC.split(";");

    strMailAddBCC = "";
    for (var i = 0; i < strArrMailBCC.length; i++) {
        if (trim(strArrMailBCC[i]) != "") {
            if (ValidEmail(strArrMailBCC[i]) == false) {
                bPass = false;
                document.getElementById("spErrMailBCC").innerHTML = "<br>" + strArrMailBCC[i] + " :Mail To is not valid Email address. !!";
                i = strArrMailBCC.length;
            }
            if (strMailAddBCC != "") {
                strMailAddBCC = strMailAddBCC + "; ";
            }
            strMailAddBCC = strMailAddBCC + trim(strArrMailBCC[i]);
        }
    }

    if (objMailSubject.value.length == 0) {
        bPass = false;
        document.getElementById("spErrMailSubject").innerHTML = "<br>Mail Subject required. !!";
    }
    if (objMailMessage.value.length == 0) {
        bPass = false;
        document.getElementById("spErrMailMessage").innerHTML = "<br>Mail Message required. !!";
    }

    if (bPass == true) {
        strXml = "<SendMail>" +
                        "<MailFrom>" + objMailFrom.value + "</MailFrom>" +
                        "<MailTo>" + objMailTo.value + "</MailTo>" +
                        "<MailSubject>" + objMailSubject.value + "</MailSubject>" +
                        "<MailMessage>" + objMailMessage.value + "</MailMessage>" +
                        "<MailBcc>" + objMailBcc.value + "</MailBcc>" +
                        "<MailAddBcc>" + strMailAddBCC + "</MailAddBcc>" +
                  "</SendMail>";
    }
    else {
        ShowNewsRegisterInfoError(strError);
        strXml = "";
    }

    objMailFrom = null;
    objMailTo = null;
    objMailSubject = null;
    objMailMessage = null;

    objMailBcc = null;
    objMailAddBcc = null;
    objMailAttachFile = null;

    return strXml;
}

function ShowDateRegis() {
    var objDateRegis = document.getElementById("chkDateRegis");
    var objDivDate = document.getElementById("dvDate");
    if (objDateRegis.checked == true) {
        objDivDate.style.display = "block";
    }
    else {
        objDivDate.style.display = "none";
    }
}

function SendRegisterMail() {
    if (document.getElementById("txtMailAttachFile").value != "") {
        var strResult = FillNewsRegisterMail();
        if (strResult.length > 0) {
            document.forms[0].submit();
        }
    }
    else {
        NewsRegisterSendMail();
    }
    document.getElementById("txtMailAttachFile").value = "";
    document.getElementById("txtMailSubject").value = "";
    document.getElementById("txtMailMessage").value = "";
}
var iPaxSelectPosition = 1;

function SuccessSelectFlight(result) {
    var arrResult = result.split("{!}");

    //Set Insurance popup
    SetShowInsurance("0");

    if (arrResult.length == 1) {
        if (result == "{000}") {

            if (UseGoogleManager() == true) {
            }
            else {
                //google analytic
                if (_gaq) {
                    _gaq.push(['_trackPageview', '/booking/passenger-details.html']);
                }
            }

            if (document.location.hostname == "localhost")
                LoadSecure(false);
            else
                LoadSecure(true);

        }
        else if (result == "{200}") {
            //Please select return flight.
            ShowMessageBox("Please select return flight.", 0, '');
        }
        else if (result == "{201}") {

            //Booking object is null.(Session time out).
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
    }
    else {
        if (arrResult[0] == "0") {

            if (UseGoogleManager() == true) {
            }
            else {
                //google analytic
                if (_gaq) {
                    _gaq.push(['_trackPageview', '/booking/passenger-details.html']);
                }
            }

            if (document.location.hostname == "localhost")
                LoadSecure(false);
            else
                LoadSecure(true);
        }
        else if (arrResult[0] == "101") {
            //US Message
            ShowMessageBox(objLanguage.Alert_Message_60.replace("{NO}", arrResult[1]));
        }
        else if (arrResult[0] == "200") {
            //Infant over limit
            ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
        }
    }

}

function ClosePassengerList() {
    var objDisable = document.getElementById("dvProgressBar");
    var objMessage = document.getElementById("dvFormHolder");

    objDisable.style.display = "none";
    objMessage.style.display = "none";
    objMessage.innerHTML = "";

    objDisable = null;
    objMessage = null;
}

function ShowPassengerInfoError(strMessage) {
    var objError = document.getElementById("dvPaxError");
    objError.innerHTML = strMessage;
    objError = null;
}

function LockPassengerInput(bValue) {
    //Input Control
    var txtClientNo = document.getElementById("txtClientNoInput");
    var stTitle = document.getElementById("stTitleInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");
    var txtBirthDate = document.getElementById("txtBirthDateInput");
    var stNationality = document.getElementById("stNational");
    var stDocumentType = document.getElementById("stDocumentType");
    var txtDocumentNo = document.getElementById("txtDocumentNumber");
    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
    var txtIssueDate = document.getElementById("txtIssueDate");
    var txtExpiryDate = document.getElementById("txtExpiryDate");
    var stIssueCountry = document.getElementById("stIssueCountry");
    var chkCopy = document.getElementById("chkCopy");

    txtClientNo.disabled = bValue;
    stTitle.disabled = bValue;

    if (txtFirstName != null) {
        txtFirstName.disabled = bValue;
    }

    if (txtLastName != null) {
        txtLastName.disabled = bValue;
    }
}

function ClearAllData() {
    var iPaxcount = document.getElementsByName("hdPassengerId").length;

    var objTitle;
    var objFirstName;
    var objLastName;
    var objClientProfileId;
    var objPassengerProfileId;
    var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
    var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
    var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
    var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
    var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
    var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
    var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
    var objCity = document.getElementById(FindControlName("input", "txtContactCity"));
    var objCountry = document.getElementById(FindControlName("select", "stContactCountry"));
    var objContactLanguage = document.getElementById(FindControlName("select", "stContactLanguage"));

    var objClientNo;
    var objNational;
    var objDocType;
    var objDocNo;
    var objIssuePlace;
    var objIssueDate;
    var objExpiryDate;
    var objBirthDate;
    var objWeight;
    var objBirthPlace;
    var objIssueCountry;

    //Clear passenger information.
    for (var iCount = 1; iCount <= iPaxcount; iCount++) {
        objClientNo = document.getElementById("txtClientNo_" + iCount);
        objTitle = document.getElementById("stTitle_" + iCount);
        objClientProfileId = document.getElementById("hdClientProfileId_" + iCount);
        objPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iCount);
        objFirstName = document.getElementById("txtName_" + iCount);
        objLastName = document.getElementById("txtLastname_" + iCount);
        objNational = document.getElementById("stNational_" + iCount);
        objDocType = document.getElementById("stDocumentType_" + iCount);
        objDocNo = document.getElementById("txtDocNumber_" + iCount);
        objIssuePlace = document.getElementById("txtIssuePlace_" + iCount);

        objIssueDate = document.getElementById("txtIssueDate_" + iCount);
        objExpiryDate = document.getElementById("txtExpiryDate_" + iCount);
        objBirthDate = document.getElementById("txtBirthDate_" + iCount);
        objWeight = document.getElementById("txtWeight_" + iCount);
        objBirthPlace = document.getElementById("txtBirthPlace_" + iCount);
        objIssueCountry = document.getElementById("stIssueCountry_" + iCount);

        if (objClientNo != null) {
            objClientNo.value = "";
        }

        if (objFirstName != null) {
            objFirstName.value = "";
        }

        if (objLastName != null) {
            objLastName.value = "";
        }

        if (objClientProfileId != null) {
            objClientProfileId.value = "";
        }

        if (objPassengerProfileId != null) {
            objPassengerProfileId.value = "";
        }
        if (objWeight != null) {
            objWeight.value = "";
        }
        if (objBirthPlace != null) {
            objBirthPlace.value = "";
        }

        if (objTitle != null) {
            objTitle.options[0].selected = true;
        }
        if (objNational != null) {
            objNational.options[0].selected = true;
        }
        if (objDocType != null) {
            objDocType.options[0].selected = true;
        }
        if (objDocNo != null) {
            objDocNo.value = "";
        }
        if (objIssuePlace != null) {
            objIssuePlace.value = "";
        }

        if (objIssueDate != null) {
            objIssueDate.value = objLanguage.default_value_1;
        }

        if (objExpiryDate != null) {
            objExpiryDate.value = objLanguage.default_value_1;
        }
        if (objBirthDate != null) {
            objBirthDate.value = objLanguage.default_value_1;
        }

        if (objIssueCountry != null) {
            objIssueCountry.options[0].selected = true;
        }

        objClientNo = null;
        objTitle = null;
        objFirstName = null;
        objLastName = null;
        objNational = null;
        objDocType = null;
        objDocNo = null;
        objIssuePlace = null;

        objIssueDate = null;
        objExpiryDate = null;
        objBirthDate = null;
        objWeight = null;
        objBirthPlace = null;
        objIssueCountry = null;
    }


    //Clear contact information.
    objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
    objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));

    if (objClientNo != null) {
        objClientNo.value = "";
    }

    if (objClientProfileId != null) {
        objClientProfileId.value = "";
    }

    if (objTitle != null) {
        objTitle.value = "";
    }

    if (objFirstName != null) {
        objFirstName.value = "";
    }

    if (objLastName != null) {
        objLastName.value = "";
    }

    if (objMobile != null) {
        objMobile.value = "";
    }

    if (objHome != null) {
        objHome.value = "";
    }

    if (objBusiness != null) {
        objBusiness.value = "";
    }

    if (objEmail != null) {
        objEmail.value = "";
    }

    if (objAddress1 != null) {
        objAddress1.value = "";
    }

    if (objAddress2 != null) {
        objAddress2.value = "";
    }
    if (objZip != null) {
        objZip.value = "";
    }
    if (objCity != null) {
        objCity.value = "";
    }
    if (objCountry != null) {
        objCountry.options[0].selected = true;
    }
    if (objContactLanguage != null) {
        objContactLanguage.options[0].selected = true;
    }


    //Clear error message.
    ShowPassengerInfoError("");
    ClearErrorMsg();

    objClientNo = null;
    objClientProfileId = null;
    objPassengerProfileId = null;
    objTitle = null;
    objFirstName = null;
    objLastName = null;
    objMobile = null;
    objHome = null;
    objBusiness = null;
    objEmail = null;
    objAddress1 = null;
    objAddress2 = null;
    objZip = null;
    objCity = null;
    objCountry = null;
    objContactLanguage = null;

    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ClearPassengerData(SuccessClearData, showError, showTimeOut);
}

function ClearPassengerList() {

    //Input Control
    var txtClientNo = document.getElementById("txtClientNoInput");
    var stTitle = document.getElementById("stTitleInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");
    var txtBirthDate = document.getElementById("txtBirthDateInput");
    var txtBirthPlace = document.getElementById("txtPlaseOfBirth");
    var stNationality = document.getElementById("stNational");
    var stDocumentType = document.getElementById("stDocumentType");
    var txtDocumentNo = document.getElementById("txtDocumentNumber");
    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
    var txtIssueDate = document.getElementById("txtIssueDate");
    var txtExpiryDate = document.getElementById("txtExpiryDate");
    var txtWeight = document.getElementById("txtWeight");
    var stIssueCountry = document.getElementById("stIssueCountry");
    var chkCopy = document.getElementById("chkCopy");

    //Hidden passenger info.
    var objPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iPaxSelectPosition);
    var objClientProfileId = document.getElementById("hdClientProfileId_" + iPaxSelectPosition);
    var objVipFlag = document.getElementById("hd_Vip_flag_" + iPaxSelectPosition);
    var hdMemberLevel = document.getElementById("hdMemberLevelRcd_" + iPaxSelectPosition);

    //Show Water Mark
    GetPassengerValue(iPaxSelectPosition);

    $("#txtFirstNameInput").addClass("watermarkOn");
    $("#txtLastnameInput").addClass("watermarkOn");

    //Clear Input info.
    if (stTitle != null) {
        stTitle.selectedIndex = 0;
    }
    if (stNationality != null) {
        stNationality.options[0].selected = true;
    }
    if (stDocumentType != null) {
        stDocumentType.options[0].selected = true;
    }

    if (txtClientNo != null) {
        txtClientNo.value = "";
    }
    if (txtFirstName != null) {
        txtFirstName.value = objLanguage.default_value_2;
    }
    if (txtLastName != null) {
        txtLastName.value = objLanguage.default_value_2;
    }
    if (txtDocumentNo != null) {
        txtDocumentNo.value = "";
    }
    if (txtPlaceOfIssue != null) {
        txtPlaceOfIssue.value = "";
    }
    if (txtBirthPlace != null) {
        txtBirthPlace.value = "";
    }
    if (hdMemberLevel != null) {
        hdMemberLevel.value = "";
    }
    if (txtWeight != null) {
        txtWeight.value = 0;
    }

    if (txtBirthDate != null) {
        txtBirthDate.value = objLanguage.default_value_1;
    }
    if (txtIssueDate != null) {
        txtIssueDate.value = objLanguage.default_value_1;
    }
    if (txtExpiryDate != null) {
        txtExpiryDate.value = objLanguage.default_value_1;
    }

    if (stIssueCountry != null) {
        stIssueCountry.options[0].selected = true;
    }

    //If first passenger is clear checkbox have to unchecked.
    if (iPaxSelectPosition == 1) {

        if (chkCopy != null) {
            chkCopy.checked = false;
        }
    }

    //Set Value to hidden.
    SetPassengerValue();
    //Clear Hidden info.
    objPassengerProfileId.value = "00000000-0000-0000-0000-000000000000";
    objClientProfileId.value = "00000000-0000-0000-0000-000000000000";
    objVipFlag.value = 0;

    //Unlock input
    LockPassengerInput(false);
}
function SuccessClearData() {
    ShowProgressBar(false);
    loadHome();
}
function SavePassengerDetail() {

    if (MultipleTab.IsCorrectTab()) {
        var strResult = FillPassengerDetail(); //Validate and fill data.
        if (strResult.length > 0) {

            //google analytic
            if (UseGoogleManager() == true) {
                dataLayer.push({ 'event': 'PopupComfirmPaymentPage' });
            }

            //            ShowConfirm(objLanguage.Alert_Message_145 + "\n" + objLanguage.Alert_Message_146,
            //                    { param: strResult },
            //                    ConfirmSavePassengerDetail);

            ConfirmSavePassengerDetail(strResult);
        }

    }
    else {
        MultipleTab.Redirect();
    }

}

function ConfirmSavePassengerDetail(e) {

    if (MultipleTab.IsCorrectTab()) {
        var strResult = e; //.data.param;

        CloseMessageBox();
        if (strResult.length > 0) {
            ShowProgressBar(true);

            //Save cookies information.
            var objChk = document.getElementById("chkRemember");

            if (objChk.checked == true) {
                //Save Contact Detail to Cookies.
                ContactDetailToCookies();
            }
            else {
                //Delete Cookies.
                deleteCookie("coContact");
            }
            objChk = null;

            //Call webservice
            tikAeroB2C.WebService.B2cService.FillPassengerData(strResult, true, SuccessSavePassengerDetail, showError, showTimeOut);
        }

    }
    else {
        MultipleTab.Redirect();
    }

}

function FillPassengerDetail() {
    var iPaxcount = document.getElementsByName("hdPassengerId").length;
    var bPass = true;

    var objTitle;
    var objPassengerId;
    var objFirstName;
    var objLastName;
    var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
    var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
    var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
    var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
    var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
    var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
    var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
    var objCity = document.getElementById(FindControlName("input", "txtContactCity"));
    var objState = document.getElementById(FindControlName("input", "txtContactState"));
    var objCountry = document.getElementById(FindControlName("select", "stContactCountry"));
    var objContactLanguage = document.getElementById(FindControlName("select", "stContactLanguage"));
    var objMobileEmail = document.getElementById(FindControlName("input", "txtMobileEmail"));

    var objClientNo;
    var objPassengerProfileId;
    var objClientProfileId;
    var objVipFlag;
    var objNational;
    var objDocType;
    var objDocNo;
    var objIssuePlace;
    var objIssueDate;
    var objExpiryDate;
    var objBirthDate;
    var objWeight;
    var objBirthPlace;
    var objMemberLevel;
    var objIssueCountry;

    var strXml = "";
    var strPassenger = "";
    var strError = "";
    var strContactError = "";
    var strDupName = "";
    var strFirstName = "";
    var strLastName = "";

    var date_of_birth = "";
    var strMsgCHDINF = "";

    //Add passenger information to xml.
    for (var iCount = 1; iCount <= iPaxcount; iCount++) {
        objPassengerId = document.getElementById("hdPassengerId_" + iCount);
        objPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iCount);
        objClientProfileId = document.getElementById("hdClientProfileId_" + iCount);
        objVipFlag = document.getElementById("hd_Vip_flag_" + iCount);
        objClientNo = document.getElementById("hdClientNo_" + iCount);
        objTitle = document.getElementById("hdTitle_" + iCount);
        objFirstName = document.getElementById("hdName_" + iCount);
        objLastName = document.getElementById("hdLastname_" + iCount);
        objNational = document.getElementById("hdNational_" + iCount);
        objDocType = document.getElementById("hdDocumentType_" + iCount);
        objDocNo = document.getElementById("hdDocNumber_" + iCount);
        objIssuePlace = document.getElementById("hdIssuePlace_" + iCount);

        objIssueDate = document.getElementById("hdIssueDate_" + iCount);
        objExpiryDate = document.getElementById("hdExpiryDate_" + iCount);
        objBirthDate = document.getElementById("hdBirthDate_" + iCount);
        objWeight = document.getElementById("hdWeight_" + iCount);
        objBirthPlace = document.getElementById("hdBirthPlace_" + iCount);
        objPaxType = document.getElementById("hdPaxType_" + iCount);
        objMemberLevel = document.getElementById("hdMemberLevelRcd_" + iCount);
        objIssueCountry = document.getElementById("hdIssueCountry_" + iCount);

        if (GetControlValue(objTitle) == "|" || GetControlValue(objTitle).length == 0) {
            bPass = false;
            strError = strError + objLanguage.Alert_Message_125.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>";  //"- Please select title for passenger  "
        }

        if (GetControlValue(objFirstName).length == 0 || IsChar(GetControlValue(objFirstName)) == false) {
            bPass = false;
            strError = strError + objLanguage.Alert_Message_26.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>";  //"- Firstname is required for passenger "
        }
        if (GetControlValue(objLastName).length == 0 || IsChar(GetControlValue(objLastName)) == false) {
            bPass = false;
            strError = strError + objLanguage.Alert_Message_27.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Lastname is required for passenger "
        }
        if (GetControlValue(objFirstName).length > 0 & ContainWhiteSpace(GetControlValue(objFirstName)) == true) {
            bPass = false;
            strError = strError + objLanguage.Alert_Message_117.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- White is not allowed in firstname for passenger "
        }

        if (GetControlValue(objLastName).length > 0 & ContainWhiteSpace(GetControlValue(objLastName)) == true) {
            bPass = false;
            strError = strError + objLanguage.Alert_Message_118.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- White is not allowed in Lastname for passenger "
        }
        //Date Validation-------------------------------------------------------------------------------------------------------------------------------
        if (document.getElementById("txtIssueDate") != null) {
            if (DateValid(GetControlValue(objIssueDate)) == false) {
                bPass = false;
                strError = strError + objLanguage.Alert_Message_18.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Please supply valid issue date "
            }
            else {
                if (IsPastDate(GetControlValue(objIssueDate)) == false) {
                    bPass = false;
                    strError = strError + objLanguage.Alert_Message_19.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Issue Date Should be in the past for passenger "
                }
            }
        }

        if (document.getElementById("txtExpiryDate") != null) {
            if (DateValid(GetControlValue(objExpiryDate)) == false) {
                bPass = false;
                strError = strError + objLanguage.Alert_Message_17.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Please supply valid expiry date "
            }
            else {
                var objLastDeptDate = document.getElementById("spnLDD");
                var strDeptDate = GetControlValue(objLastDeptDate);
                if (IsFutureDate(GetControlValue(objExpiryDate), strDeptDate) == false) {
                    bPass = false;
                    strError = strError + objLanguage.Alert_Message_20.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Document is expired for passenger "
                }
            }
        }

        if (document.getElementById("txtBirthDateInput") != null) {
            if (DateValid(GetControlValue(objBirthDate)) == false) {
                bPass = false;
                strError = strError + objLanguage.Alert_Message_14.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Please supply valid date of birth."
            }
            else {
                if (IsPastDate(GetControlValue(objBirthDate)) == false) {
                    bPass = false;
                    strError = strError + objLanguage.Alert_Message_62.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Birth Date Should be in the past for passenger "
                }
                if (GetControlValue(objBirthDate).length == 0) {
                    bPass = false;
                    strError = strError + objLanguage.Alert_Message_63.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Birth Date is required for passenger "
                }

                if ((GetControlValue(objPaxType) == "ADULT" & ReformatDate(GetControlValue(objBirthDate)).length > 0) || GetControlValue(objPaxType) == "CHD" || GetControlValue(objPaxType) == "INF" || GetControlValue(objPaxType) == "YP") {
                    date_of_birth = (GetControlValue(objBirthDate) == objLanguage.default_value_1) ? "" : GetControlValue(objBirthDate);
                    if (date_of_birth.length > 0) {
                        var objDeptDate = document.getElementById("spnDD");
                        if (validateChdInfBirthDate(ReformatDate(GetControlValue(objDeptDate)), ReformatDate(date_of_birth), GetControlValue(objPaxType)) == false) {
                            //Select message by passenger type.
                            if (GetControlValue(objPaxType) == "CHD") {
                                //Alert for child
                                strMsgCHDINF = objLanguage.Alert_Message_22;
                            }
                            else if (GetControlValue(objPaxType) == "INF") {
                                //Alert for infant
                                strMsgCHDINF = objLanguage.Alert_Message_23;
                            }
                            else {
                                //Alert for Adult
                                strMsgCHDINF = objLanguage.Alert_Message_116;
                            }
                            bPass = false;
                            strError = strError + strMsgCHDINF.replace('{NO}', padZeros(iCount, 2)) + ".<div class='clear-all'></div>";
                        }
                        objDeptDate = null;
                    }
                }
            }
        }
        //Valudate Document number
        if (document.getElementById("txtDocumentNumber") != null) {
            if (GetControlValue(objDocNo).length == 0 || IsAlphaNumeric(GetControlValue(objDocNo)) == false) {
                bPass = false;
                strError = strError + objLanguage.Alert_Message_101.replace("{NO}", padZeros(iCount, 2)) + "<div class='clear-all'></div>"; //"Passenger Firstname" + padZeros(iCount, 3) + " is not valid.<div class='clear-all'></div>";;
            }
        }
        //Issue palce
        if (document.getElementById("txtPlaseOfIssue") != null) {
            if (GetControlValue(objIssuePlace).length == 0) {
                bPass = false;
                strError = strError + objLanguage.Alert_Message_16.replace("{NO}", padZeros(iCount, 2)) + "<div class='clear-all'></div>"; //"Passenger Firstname" + padZeros(iCount, 3) + " is not valid.<div class='clear-all'></div>";;
            }
        }
        //------------------------------------------------------------------------------------------------------
        //Document Validation
        if (document.getElementById("stIssueCountry") != null) {
            if (GetControlValue(objIssueCountry).length == 0) {
                bPass = false;
                strError = strError + objLanguage.Alert_Message_147.replace("{NO}", padZeros(iCount, 2)) + ".<br/>";  //"- Issue country required"
            }
        }
        //Nationality Validation
        if (document.getElementById("stNational") != null) {
            if (GetControlValue(objNational).length == 0) {
                bPass = false;
                strError = strError + objLanguage.Alert_Message_127.replace("{NO}", padZeros(iCount, 2)) + ".<br/>";  //"- Issue country required"
            }
        }
        //----------------------------------------------------------------------------------------------------
        if (objFirstName != null) {
            if (GetControlValue(objFirstName).length > 0) {
                if (ContainSpecialCharacter(GetControlValue(objFirstName)) == true) {
                    bPass = false;
                    strError = strError + objLanguage.Alert_Message_26.replace("{NO}", padZeros(iCount, 2)) + "<div class='clear-all'></div>"; //"Passenger Firstname" + padZeros(iCount, 3) + " is not valid.<div class='clear-all'></div>";;
                }
            }
        }

        if (objLastName != null) {
            if (GetControlValue(objLastName).length > 0) {
                if (ContainSpecialCharacter(GetControlValue(objLastName)) == true) {
                    bPass = false;
                    strError = strError + objLanguage.Alert_Message_13.replace("{NO}", padZeros(iCount, 2)) + "<div class='clear-all'></div>"; //"Passenger Lastname" + padZeros(iCount, 3) + " is not valid.<div class='clear-all'></div>";;
                }
            }
        }

        strFirstName = ReplaceSpecialCharacter(GetControlValue(objFirstName)).toUpperCase();
        strLastName = ReplaceSpecialCharacter(GetControlValue(objLastName)).toUpperCase();

        strDupName = GetDuplicatePassenger(strLastName, strFirstName, iCount - 1);
        if (strDupName.length > 0) {
            bPass = false;
            strError = strError + objLanguage.Alert_Message_64.replace("{NO}", padZeros(iCount, 2)) + ".<div class='clear-all'></div>"; //"- Passenger " + strDupName + "  already selected "
        }
        if (bPass == true) {
            //Pass criteria check.
            strPassenger = strPassenger +
                            "<passenger>" +
                                    "<passenger_id>" + GetControlValue(objPassengerId) + "</passenger_id>" +
                                    "<passenger_profile_id>" + GetControlValue(objPassengerProfileId) + "</passenger_profile_id>" +
                                    "<client_profile_id>" + GetControlValue(objClientProfileId) + "</client_profile_id>" +
                                    "<client_no>" + ((objClientNo.value.length == 8) ? objClientNo.value : "") + "</client_no>" +
                                    "<firstname>" + ConvertToValidXmlData(strFirstName).toUpperCase() + "</firstname>" +
                                    "<lastname>" + ConvertToValidXmlData(strLastName).toUpperCase() + "</lastname>" +
                                    "<title_rcd>" + GetControlValue(objTitle).toUpperCase() + "</title_rcd>" +
                                    "<nationality_rcd>" + GetControlValue(objNational) + "</nationality_rcd>" +
                                    "<document_type_rcd>" + ((GetControlValue(objDocNo).length > 0) ? "P" : "") + "</document_type_rcd>" +
                                    "<document_no>" + ConvertToValidXmlData(GetControlValue(objDocNo)) + "</document_no>" +
                                    "<issue_place>" + ConvertToValidXmlData(GetControlValue(objIssuePlace)) + "</issue_place>" +
                                    "<issue_date>" + ReformatDate(GetControlValue(objIssueDate)) + "</issue_date>" +
                                    "<expiry_date>" + ReformatDate(GetControlValue(objExpiryDate)) + "</expiry_date>" +
                                    "<date_of_birth>" + ReformatDate(GetControlValue(objBirthDate)) + "</date_of_birth>" +
                                    "<passenger_weight>" + ConvertToValidXmlData(GetControlValue(objWeight)) + "</passenger_weight>" +
                                    "<passport_birth_place>" + ConvertToValidXmlData(GetControlValue(objBirthPlace)) + "</passport_birth_place>" +
                                    "<vip_flag>" + GetControlValue(objVipFlag) + "</vip_flag>" +
                                    "<member_level_rcd>" + GetControlValue(objMemberLevel) + "</member_level_rcd>" +
                                    "<passport_issue_country_rcd>" + GetControlValue(objIssueCountry) + "</passport_issue_country_rcd>" +
                               "</passenger>";
        }

        objClientNo = null;
        objTitle = null;
        objFirstName = null;
        objLastName = null;
        objNational = null;
        objDocType = null;
        objDocNo = null;
        objIssuePlace = null;

        objIssueDate = null;
        objExpiryDate = null;
        objBirthDate = null;
        objWeight = null;
        objBirthPlace = null;
        objIssueCountry = null;
    }

    //Add Contact detail information to xml.
    objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
    objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));

    var txtEUVat = document.getElementById(FindControlName("input", "txtEUVat"));
    var txtCIN = document.getElementById(FindControlName("input", "txtCIN"));

    if (GetSelectedOption(objTitle).length == 0) {
        bPass = false;

        strContactError = strContactError + objLanguage.Alert_Message_122 + "<div class='clear-all'></div>"; //"* Title Required!!";
        // document.getElementById("spErrFirstname").innerHTML = "*";
    }

    if (objFirstName != null) {
        if (GetControlValue(objFirstName).length == 0) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_66 + "<div class='clear-all'></div>"; //"* Firstname required!!";
        }
        else {
            if (ContainSpecialCharacter(GetControlValue(objFirstName)) | IsChar(GetControlValue(objFirstName)) == false) {
                bPass = false;
                strContactError = strContactError + objLanguage.Alert_Message_65 + "<div class='clear-all'></div>"; //"* Invalid input!!";
            }
        }

        if (GetControlValue(objFirstName).length > 0 & ContainWhiteSpace(GetControlValue(objFirstName)) == true) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_119 + "<div class='clear-all'></div>"; //"* White is not allowed in firstname"
        }
    }

    if (objLastName != null) {
        if (GetControlValue(objLastName).length == 0) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_68 + "<div class='clear-all'></div>"; //"* Lastname required!!";
        }
        else {
            if (ContainSpecialCharacter(GetControlValue(objLastName)) == true | IsChar(GetControlValue(objLastName)) == false) {
                bPass = false;
                strContactError = strContactError + objLanguage.Alert_Message_67 + "<div class='clear-all'></div>"; //"* Invalid input!!";
            }
        }
        if (GetControlValue(objLastName).length > 0 & ContainWhiteSpace(GetControlValue(objLastName)) == true) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_120 + "<div class='clear-all'></div>"; //"* White is not allowed in last"
        }
    }

    if (objHome != null) {
        if (GetControlValue(objHome).length == 0) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_69 + "<div class='clear-all'></div>"; //"* One of the telephone is required!!";
        }
    }

    if (IsChar(GetControlValue(objFirstName)) == false || IsChar(GetControlValue(objLastName)) == false) {
        bPass = false;
        strContactError = strContactError + objLanguage.Alert_Message_30; //"* Please supply valid contact person!!";
    }

    if (objEmail != null) {
        if (GetControlValue(objEmail).length == 0) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_33 + "<div class='clear-all'></div>"; //"* Email required !!";
        }
        else if (ValidEmail(GetControlValue(objEmail)) == false) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_24 + "<div class='clear-all'></div>"; //"* Supply valid Email address !!";   
        }
    }

    if (GetControlValue(objMobileEmail).length > 0 && ValidEmail(GetControlValue(objMobileEmail)) == false) {
        bPass = false;
        strContactError = strContactError + objLanguage.Alert_Message_24 + "<div class='clear-all'></div>"; //"* Supply valid Email address !!";   
    }

    if (objAddress1 != null) {
        if (GetControlValue(objAddress1).length == 0) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_41 + "<div class='clear-all'></div>"; //"* Address 1 required!!";
        }
    }

    if (objCity != null) {
        if (GetControlValue(objCity).length == 0) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_42 + "<div class='clear-all'></div>"; //"* City required!!";
        }
    }

    if (objZip != null) {
        if (GetControlValue(objZip).length == 0) {
            bPass = false;
            strContactError = strContactError + objLanguage.Alert_Message_43 + "<div class='clear-all'></div>"; //"* Post code required!!";
        }
    }

    if (objMobile != null) {
        if (GetControlValue(objMobile).length > 0) {
            if (ValidPhoneNumber(GetControlValue(objMobile)) == false) {
                strContactError = strContactError + objLanguage.Alert_Message_31 + "<div class='clear-all'></div>";
                bPass = false;
            }
        }
    }

    if (objHome != null) {
        if (GetControlValue(objHome).length > 0) {
            if (ValidPhoneNumber(GetControlValue(objHome)) == false) {
                strContactError = strContactError + objLanguage.Alert_Message_32 + "<div class='clear-all'></div>";
                bPass = false;
            }
        }
    }

    //if (objBusiness != null) {
    //    if (GetControlValue(objBusiness).length > 0) {
    //        if (ValidPhoneNumber(GetControlValue(objBusiness)) == false) {
    //            strContactError = strContactError + objLanguage.Alert_Message_69 + "<div class='clear-all'></div>";
    //            bPass = false;
    //        }
    //    }
    //}

    if (GetSelectedOption(objCountry).length == 0) {
        bPass = false;

        strContactError = strContactError + objLanguage.Alert_Message_123 + "<div class='clear-all'></div>"; //"* Please select contact country!!";
        document.getElementById("spErrFirstname").innerHTML = "*";
    }


    if (bPass == true) {
        //All validation pass goto next step.
        var todayDate = new Date();
        strXml = "<booking>" +
                    "<contact>" +
                        "<client_profile_id>" + GetControlValue(objClientProfileId) + "</client_profile_id>" +
                        "<client_number>" + GetControlValue(objClientNo) + "</client_number>" +
                        "<title>" + GetSelectedOption(objTitle) + "</title>" +
                        "<firstname>" + ConvertToValidXmlData(GetControlValue(objFirstName)) + "</firstname>" +
                        "<lastname>" + ConvertToValidXmlData(GetControlValue(objLastName)) + "</lastname>" +
                        "<mobile>" + ConvertToValidXmlData(GetControlValue(objMobile)) + "</mobile>" +
                        "<home>" + ConvertToValidXmlData(GetControlValue(objHome)) + "</home>" +
                        "<business>" + ConvertToValidXmlData(GetControlValue(objBusiness)) + "</business>" +
                        "<email>" + ConvertToValidXmlData(GetControlValue(objEmail)) + "</email>" +
                        "<addresss_1>" + ConvertToValidXmlData(GetControlValue(objAddress1)) + "</addresss_1>" +
                        "<addresss_2>" + ConvertToValidXmlData(GetControlValue(objAddress2)) + "</addresss_2>" +
                        "<street>" + ConvertToValidXmlData(GetControlValue(objAddress2)) + "</street>" +
                        "<zip>" + ConvertToValidXmlData(GetControlValue(objZip)) + "</zip>" +
                        "<city>" + ConvertToValidXmlData(GetControlValue(objCity)) + "</city>" +
                        "<state>" + ConvertToValidXmlData(GetControlValue(objState)) + "</state>" +
                        "<country>" + GetSelectedOption(objCountry) + "</country>" +
                        "<country_displayname>" + GetSelectedOptionText(objCountry) + "</country_displayname>" +
                        "<language>" + GetSelectedOption(objContactLanguage) + "</language>" +
                        "<newsletter_flag>0</newsletter_flag>" +
                        "<mobile_email>" + GetControlValue(objMobileEmail) + "</mobile_email>" +
                    "</contact>" +
                    strPassenger +
                 "</booking>";
    }
    else {
        //Failed criteria check go out of the loop.
        ShowMessageBox(strContactError + "<div class='clear-all'></div>" + strError, 0, '');
        strXml = "";
    }

    return strXml;
}
function CopyContactToPassenger() {
    var objChk = document.getElementById("chkCopy");
    var objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    var objCTitle = document.getElementById(FindControlName("select", "stContactTitle")); //Contact Title.
    var objCFirstName = document.getElementById(FindControlName("input", "txtContactFirstname")); //Contact firstname.
    var objCLastName = document.getElementById(FindControlName("input", "txtContactLastname")); //Contact lastname.

    //Input Informatiob
    var stTitle = document.getElementById("stTitleInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");

    var objTitle;
    //Passener hidden information.
    var objTitle;
    var objFirstName;
    var objLastName;

    var iPaxcount = document.getElementsByName("hdPassengerId").length;

    if (objChk.checked == true) {

        //Passener hidden information.
        objTitle = document.getElementById("hdTitle_1");
        objFirstName = document.getElementById("hdName_1");
        objLastName = document.getElementById("hdLastname_1");

        if (objClientProfileId.value.length == 0 || objClientProfileId.value == "00000000-0000-0000-0000-000000000000") {
            //Fill Hidden information.
            if (GetSelectedOption(objCTitle).length > 0) {
                objTitle.value = GetSelectedOption(objCTitle).toUpperCase();
            }
            if (GetControlValue(objCFirstName).length > 0) {
                objFirstName.value = GetControlValue(objCFirstName).toUpperCase();
            }
            if (GetControlValue(objCLastName).length > 0) {
                objLastName.value = GetControlValue(objCLastName).toUpperCase();
            }

            GetPassengerValue(1);

            if (document.getElementById("dvPaxTabName") != null) {
                if (GetControlValue(objFirstName).length > 0 & GetControlValue(objLastName).length > 0) {
                    document.getElementById("dvPaxTabName").innerHTML = GetControlValue(objLastName) + "  " + GetControlValue(objFirstName) + "  " + GetSelectedOption(objCTitle).split("|")[0];
                }
                else {
                    document.getElementById("dvPaxTabName").innerHTML = objLanguage.default_value_4;
                }
            }
        }
        else {
            //Get Client Information(Myself).
            GetClient(1, true);
        }
    }
    else {
        //Clear Passenger information.
        iPaxSelectPosition = 1;
        ClearPassengerList();
    }

}

function ContactDetailToCookies() {
    var objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
    var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
    var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
    var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
    var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
    var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
    var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
    var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
    var objCity = document.getElementById(FindControlName("input", "txtContactCity"));
    var objCountry = document.getElementById(FindControlName("select", "stContactCountry"));
    var objContactLanguage = document.getElementById(FindControlName("select", "stContactLanguage"));

    var objTaxId = document.getElementById(FindControlName("input", "txtTIN"));
    var objInvoiceReceiver = document.getElementById(FindControlName("input", "txtInvoiceReceiver"));
    var txtEUVat = document.getElementById(FindControlName("input", "txtEUVat"));
    var txtCIN = document.getElementById(FindControlName("input", "txtCIN"));
    var objOptionalEmail = document.getElementById(FindControlName("input", "txtContactEmail2"));
    var objMobileEmail = document.getElementById(FindControlName("input", "txtMobileEmail"));
    var objState = document.getElementById(FindControlName("input", "txtContactState"));

    var cookiesValue = GetSelectedOption(objTitle) + "{}" +
                       GetControlValue(objFirstName) + "{}" +
                       GetControlValue(objLastName) + "{}" +
                       GetControlValue(objMobile) + "{}" +
                       GetControlValue(objHome) + "{}" +
                       GetControlValue(objBusiness) + "{}" +
                       GetControlValue(objEmail) + "{}" +
                       GetControlValue(objAddress1) + "{}" +
                       GetControlValue(objAddress2) + "{}" +
                       GetControlValue(objZip) + "{}" +
                       GetControlValue(objCity) + "{}" +
                       GetSelectedOption(objCountry) + "{}" +
                       GetSelectedOption(objContactLanguage) + "{}" +
                       GetControlValue(objTaxId) + "{}" +
                       GetControlValue(objInvoiceReceiver) + "{}" +
                       GetControlValue(txtEUVat) + "{}" +
                       GetControlValue(txtCIN) + "{}" +
                       GetControlValue(objOptionalEmail) + "{}" +
                       GetControlValue(objMobileEmail) + "{}" +
                       GetControlValue(objState);

    var dtExpired = new Date();

    //Add 5 days Expiry date to cookies.
    dtExpired.setDate(dtExpired.getDate() + iCookiesExpDays);
    setCookie("coContact", $.base64.encode(cookiesValue), dtExpired, "", "", false);

    dtExpired = null;
}

function CopyClientToContact(objClient) {
    var objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
    var objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
    var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
    var objContactHome = document.getElementById(FindControlName("input", "txtContactHome"));
    var objContactBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
    var objContactEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
    var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
    var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
    var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
    var objCity = document.getElementById(FindControlName("input", "txtContactCity"));

    //Copy first client information to contact detail
    objClientNo.value = objClient.client_number;
    objClientProfileId.value = objClient.client_profile_id;
    SetComboValue(FindControlName("select", "stContactTitle"), objClient.title_rcd + "|" + objClient.gender_type_rcd);
    objFirstName.value = objClient.firstname;
    objLastName.value = objClient.lastname;
    objMobile.value = objClient.phone_mobile;
    objContactHome.value = objClient.phone_home;
    objContactBusiness.value = objClient.phone_business;
    objContactEmail.value = objClient.contact_email;
    objAddress1.value = objClient.address_line1;
    objAddress2.value = objClient.address_line2;
    objZip.value = objClient.zip_code;
    objCity.value = objClient.city;
    SetComboValue(FindControlName("select", "stContactCountry"), objClient.nationality_rcd);

    objClientNo = null;
    objClientProfileId = null;
    objFirstName = null;
    objLastName = null;
    objMobile = null;
    objContactHome = null;
    objContactBusiness = null;
    objContactEmail = null;
    objAddress1 = null;
    objAddress2 = null;
    objZip = null;
    objCity = null;
}

function GetSelectedClient() {
    var objCheckBox = document.getElementsByName("chkClient");
    var bFound = false;
    var strClientNumber;
    //iCount = Search Box Count.
    //jCount = Passenger Form Count.   
    for (var iCount = 0; iCount < objCheckBox.length; iCount++) {
        if (objCheckBox[iCount].checked == true) {
            var objTotalPax = document.getElementById("dvTotalPax");
            for (var jCount = 1; jCount <= document.getElementsByName("txtLastname").length; jCount++) {
                if ((document.getElementById("tdPaxType_" + jCount).innerHTML == document.getElementById("dvListPaxType_" + (iCount + 1)).innerHTML)) {
                    //Hidden Field Data
                    document.getElementById("hdClientProfileId_" + jCount).value = document.getElementById("hdListClientProfileId_" + (iCount + 1)).value;
                    document.getElementById("hdPassengerProfileId_" + jCount).value = document.getElementById("hdListPassengerProfileId_" + (iCount + 1)).value;
                    document.getElementById("hd_Vip_flag_" + jCount).value = document.getElementById("hdListVipFlag_" + (iCount + 1)).value;

                    //Non Hidden Field Data
                    strClientNumber = document.getElementById("hdListClientNo_" + (iCount + 1)).value;
                    document.getElementById("txtClientNo_" + jCount).value = strClientNumber;
                    SetComboValue("stTitle_" + jCount, document.getElementById("hdListGender_" + (iCount + 1)).value);
                    document.getElementById("txtLastname_" + jCount).value = document.getElementById("dvListLastName_" + (iCount + 1)).innerHTML;
                    document.getElementById("txtName_" + jCount).value = document.getElementById("dvListFirstName_" + (iCount + 1)).innerHTML;
                    SetComboValue("stNational_" + jCount, document.getElementById("hdListNationality_" + (iCount + 1)).value);
                    SetComboValue("stDocumentType_" + jCount, document.getElementById("hdListDocumentType_" + (iCount + 1)).value);
                    document.getElementById("txtDocNumber_" + jCount).value = document.getElementById("hdListDocumentNo_" + (iCount + 1)).value;
                    document.getElementById("txtIssuePlace_" + jCount).value = document.getElementById("hdListIssuePlace_" + (iCount + 1)).value;
                    document.getElementById("txtIssueDate_" + jCount).value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListIssueDate_" + (iCount + 1)).value));
                    document.getElementById("txtExpiryDate_" + jCount).value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListExpiryDate_" + (iCount + 1)).value));
                    document.getElementById("txtBirthDate_" + jCount).value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListDateOfBirth_" + (iCount + 1)).value));
                    document.getElementById("txtWeight_" + jCount).value = document.getElementById("hdListWeight_" + (iCount + 1)).value;
                    document.getElementById("hdMemberLevelRcd_" + jCount).value = document.getElementById("hdListMemberLevel_" + (iCount + 1)).value;
                    SetComboValue("stIssueCountry_" + jCount, document.getElementById("hdListIssueCountry_" + (iCount + 1)).value);

                    //Lock Control.
                    LockPassengerInput(true);
                    bFound = true;
                    break;
                }
            }

        }
    }

    ClosePassengerList();
    document.getElementById("dvFormHolder").innerHTML = "";
    var objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
    if (bFound == true && objClientNo.value != document.getElementById("txtClientNo_1").value) {
        //Get Myself to contact detail.
        GetClientProfile(strClientNumber);
    }
    objClientNo = null;

    objCheckBox = null;
}

function GetSelectedClientPosition() {
    if (iPaxSelectPosition > 0) {
        var objCheckBox = document.getElementsByName("chkClient");
        var bFound = false;
        var strClientNumber;
        //iCount = Search Box Count.

        for (var iCount = 0; iCount < objCheckBox.length; iCount++) {
            if (objCheckBox[iCount].checked == true) {
                var objTotalPax = document.getElementById("dvTotalPax");
                if ((GetControlValue(document.getElementById("hdPaxType_" + iPaxSelectPosition)) == GetControlInnerHtml(document.getElementById("dvListPaxType_" + (iCount + 1))))) {

                    //Input Control
                    var txtClientNo = document.getElementById("txtClientNoInput");
                    var txtFirstName = document.getElementById("txtFirstNameInput");
                    var txtLastName = document.getElementById("txtLastnameInput");
                    var txtBirthDate = document.getElementById("txtBirthDateInput");
                    var txtBirthPlace = document.getElementById("txtPlaseOfBirth");

                    var txtDocumentNo = document.getElementById("txtDocumentNumber");
                    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
                    var txtIssueDate = document.getElementById("txtIssueDate");
                    var txtExpiryDate = document.getElementById("txtExpiryDate");
                    var txtWeight = document.getElementById("txtWeight");

                    //Hidden passenger info.
                    var objPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iPaxSelectPosition);
                    var objClientProfileId = document.getElementById("hdClientProfileId_" + iPaxSelectPosition);
                    var objVipFlag = document.getElementById("hd_Vip_flag_" + iPaxSelectPosition);
                    var hdMemberLevel = document.getElementById("hdMemberLevelRcd_" + iPaxSelectPosition);

                    //Hidden Field Data
                    if (objClientProfileId != null) {
                        objClientProfileId.value = GetControlValue(document.getElementById("hdListClientProfileId_" + (iCount + 1)));
                    }
                    if (objPassengerProfileId != null) {
                        objPassengerProfileId.value = GetControlValue(document.getElementById("hdListPassengerProfileId_" + (iCount + 1)));
                    }
                    if (objVipFlag != null) {
                        objVipFlag.value = GetControlValue(document.getElementById("hdListVipFlag_" + (iCount + 1)));
                    }

                    //Non Hidden Field Data
                    if (txtClientNo != null) {
                        strClientNumber = GetControlValue(document.getElementById("hdListClientNo_" + (iCount + 1)));
                        txtClientNo.value = strClientNumber;
                    }
                    if (txtLastName != null) {
                        txtLastName.value = GetControlInnerHtml(document.getElementById("dvListLastName_" + (iCount + 1)));
                    }
                    if (txtFirstName != null) {
                        txtFirstName.value = GetControlInnerHtml(document.getElementById("dvListFirstName_" + (iCount + 1)));
                    }
                    if (txtDocumentNo != null) {
                        txtDocumentNo.value = GetControlValue(document.getElementById("hdListDocumentNo_" + (iCount + 1)));
                    }
                    if (txtPlaceOfIssue != null) {
                        txtPlaceOfIssue.value = GetControlValue(document.getElementById("hdListIssuePlace_" + (iCount + 1)));
                    }
                    if (txtIssueDate != null) {
                        txtIssueDate.value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListIssueDate_" + (iCount + 1)).value));
                    }
                    if (txtExpiryDate != null) {
                        txtExpiryDate.value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListExpiryDate_" + (iCount + 1)).value));
                    }
                    if (txtBirthDate != null) {
                        txtBirthDate.value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListDateOfBirth_" + (iCount + 1)).value));
                    }
                    if (txtBirthPlace != null) {
                        txtBirthPlace.value = GetControlValue(document.getElementById("hdListBirthPlace" + (iCount + 1)));
                    }
                    if (txtWeight != null) {
                        txtWeight.value = GetControlValue(document.getElementById("hdListWeight_" + (iCount + 1)));
                    }
                    if (hdMemberLevel != null) {
                        hdMemberLevel.value = GetControlValue(document.getElementById("hdListMemberLevel_" + (iCount + 1)));
                    }

                    SetComboSplitValue("stTitleInput", GetControlValue(document.getElementById("hdListGender_" + (iCount + 1))).split("|")[0], 0);
                    SetComboValue("stNational", GetControlValue(document.getElementById("hdListNationality_" + (iCount + 1))));
                    SetComboValue("stDocumentType", GetControlValue(document.getElementById("hdListDocumentType_" + (iCount + 1))));
                    SetComboValue("stIssueCountry", GetControlValue(document.getElementById("hdListIssueCountry_" + (iCount + 1))));

                    //Set Value to hidden field.
                    SetPassengerValue();
                    //Lock Control.
                    LockPassengerInput(true);

                    bFound = true;
                    break;
                }
            }
        }

        ClosePassengerList();

        var objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
        if (bFound == true && objClientNo.value != document.getElementById("hdClientNo_1").value) {
            //Get Myself to contact detail.
            GetClientProfile(strClientNumber);
        }
        objClientNo = null;
        objCheckBox = null;
    }
}

function CheckPassengerSelect(strCtrl) {
    var objPaxSelecter = document.getElementById("dvPaxSelecter");
    var objTotalPax = document.getElementById("dvTotalPax");
    var objCheck = document.getElementById(strCtrl);

    //Add and subtract number of selected passenger.
    if (objCheck.checked == true) {
        if (objPaxSelecter.innerHTML >= objTotalPax.innerHTML) {
            //Excess the limit amount of passenger.
            objCheck.checked = false;
        }
        else {
            //Add number of selected passenger.
            objPaxSelecter.innerHTML = parseInt(objPaxSelecter.innerHTML) + 1;
        }
    }
    else {
        //sub number of selected passenger.
        objPaxSelecter.innerHTML = parseInt(objPaxSelecter.innerHTML) - 1;
    }


    objPaxSelecter = null;
    objTotalPax = null;
    objCheck = null;
}

function GetClientProfile(strClientNumber) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetClientProfile(strClientNumber, SuccessGetClientProfile, showError, showTimeOut);
}

function SuccessGetClientProfile(result) {
    var objClient = eval("(" + result + ")");
    var clientProfileId = document.getElementById("");

    CopyClientToContact(objClient[0]);
    ShowProgressBar(false);
}
function IsTypeAvailable(strType) {
    var objLastname = document.getElementsByName("txtLastname");
    var bAvailable = false;

    for (var i = 0; i < objLastname.length; i++) {
        if (objLastname[i].value.length == 0 && strType == document.getElementById("tdPaxType_" + (i + 1)).innerHTML) {
            bAvailable = true;
            break;
        }
    }

    objLastname = null;
    objPaxType = null;

    return bAvailable;
}

function GetDuplicatePassenger(strLastname, strName, iPosition) {
    var objLastname = document.getElementsByName("txtLastname");
    var objName = document.getElementsByName("txtName");

    for (var i = 0; i < objLastname.length; i++) {
        if (objLastname[i].value.length > 0 && objName[i].value.length > 0) {
            if (strLastname == objLastname[i].value.toUpperCase() && strName == objName[i].value.toUpperCase() && iPosition != i) {
                return strLastname + "/" + strName;
            }
        }
    }
    return "";
}

function GetAccuralQuote() {
    tikAeroB2C.WebService.B2cService.AccuralQuote(SuccessAccuralQuote, showError, showTimeOut);
}
function SuccessAccuralQuote(result) {
    if (result.length > 0) {
        var objAccualSummary = document.getElementById("dvAccualSummary");
        objAccualSummary.innerHTML = result;
        objAccualSummary = null;
    }
}

function SetPassengerValue() {
    //Hidden Control.
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPaxSelectPosition);
    var hdPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iPaxSelectPosition);
    var hdClientProfileId = document.getElementById("hdClientProfileId_" + iPaxSelectPosition);
    var hdVipFlag = document.getElementById("hd_Vip_flag_" + iPaxSelectPosition);
    var hdClientNo = document.getElementById("hdClientNo_" + iPaxSelectPosition);
    var hdTitle = document.getElementById("hdTitle_" + iPaxSelectPosition);
    var hdFirstName = document.getElementById("hdName_" + iPaxSelectPosition);
    var hdLastName = document.getElementById("hdLastname_" + iPaxSelectPosition);
    var hdBirthDate = document.getElementById("hdBirthDate_" + iPaxSelectPosition);
    var hdBirthPlace = document.getElementById("hdBirthPlace_" + iPaxSelectPosition);
    var hdMemberLevel = document.getElementById("hdMemberLevelRcd_" + iPaxSelectPosition);
    var hdNationality = document.getElementById("hdNational_" + iPaxSelectPosition);
    var hdDocumentType = document.getElementById("hdDocumentType_" + iPaxSelectPosition);
    var hdDocumentNo = document.getElementById("hdDocNumber_" + iPaxSelectPosition);
    var hdPlaceOfIssue = document.getElementById("hdIssuePlace_" + iPaxSelectPosition);
    var hdIssueDate = document.getElementById("hdIssueDate_" + iPaxSelectPosition);
    var hdExpiryDate = document.getElementById("hdExpiryDate_" + iPaxSelectPosition);
    var hdWeight = document.getElementById("hdWeight_" + iPaxSelectPosition);
    var hdIssueCountry = document.getElementById("hdIssueCountry_" + iPaxSelectPosition);

    //Input Control
    var txtClientNo = document.getElementById("txtClientNoInput");
    var stTitle = document.getElementById("stTitleInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");
    var txtBirthDate = document.getElementById("txtBirthDateInput");
    var txtBirthPlace = document.getElementById("txtPlaseOfBirth");
    var stNationality = document.getElementById("stNational");
    var stDocumentType = document.getElementById("stDocumentType");
    var txtDocumentNo = document.getElementById("txtDocumentNumber");
    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
    var txtIssueDate = document.getElementById("txtIssueDate");
    var txtExpiryDate = document.getElementById("txtExpiryDate");
    var txtWeight = document.getElementById("txtWeight");
    var stIssueCountry = document.getElementById("stIssueCountry");

    if (hdClientNo != null) {
        hdClientNo.value = GetControlValue(txtClientNo);
    }
    if (hdTitle != null) {
        hdTitle.value = GetSelectedOption(stTitle);
    }
    if (hdFirstName != null) {
        if (GetControlValue(txtFirstName).length > 0) {
            txtFirstName.value = txtFirstName.value.toUpperCase();
        }
        hdFirstName.value = GetControlValue(txtFirstName);
    }
    if (hdLastName != null) {
        if (GetControlValue(txtLastName).length > 0) {
            txtLastName.value = txtLastName.value.toUpperCase();
        }
        hdLastName.value = GetControlValue(txtLastName);
    }
    if (hdBirthDate != null) {
        hdBirthDate.value = GetControlValue(txtBirthDate);
    }
    if (hdBirthPlace != null) {
        hdBirthPlace.value = GetControlValue(txtBirthPlace);
    }

    if (hdNationality != null) {
        hdNationality.value = GetSelectedOption(stNationality);
    }
    if (hdDocumentType != null) {
        hdDocumentType.value = GetSelectedOption(stDocumentType);
    }
    if (hdDocumentNo != null) {
        hdDocumentNo.value = GetControlValue(txtDocumentNo);
    }
    if (hdPlaceOfIssue != null) {
        hdPlaceOfIssue.value = GetControlValue(txtPlaceOfIssue);
    }
    if (hdIssueDate != null) {
        hdIssueDate.value = GetControlValue(txtIssueDate);
    }
    if (hdExpiryDate != null) {
        hdExpiryDate.value = GetControlValue(txtExpiryDate);
    }
    if (hdWeight != null) {
        hdWeight.value = GetControlValue(txtWeight);
    }
    if (hdIssueCountry != null) {
        hdIssueCountry.value = GetSelectedOption(stIssueCountry);
    }


    //Set Name To current Tab Header
    SetInputnameToTab(iPaxSelectPosition);

}

function SetInputnameToTab(iPosition) {
    var objPaxTabName = document.getElementById("dvPaxTabName");
    var objCurrentTitle = document.getElementById("hdTitle_" + iPosition);
    var objCurrentFirstName = document.getElementById("hdName_" + iPosition);
    var objCurrentLastName = document.getElementById("hdLastname_" + iPosition);
    if (objPaxTabName != null) {
        if (GetControlValue(objCurrentFirstName).length == 0 & GetControlValue(objCurrentLastName).length == 0) {
            objPaxTabName.innerHTML = objLanguage.default_value_4;
        }
        else {
            objPaxTabName.innerHTML = GetControlValue(objCurrentLastName).toUpperCase() + " " + GetControlValue(objCurrentFirstName).toUpperCase() + " " + GetControlValue(objCurrentTitle).split("|")[0];
        }
    }
}
function GetPassengerValue(iPosition) {

    //Hidden Control.
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPosition);
    var hdPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iPosition);
    var hdClientProfileId = document.getElementById("hdClientProfileId_" + iPosition);
    var hdVipFlag = document.getElementById("hd_Vip_flag_" + iPosition);
    var hdClientNo = document.getElementById("hdClientNo_" + iPosition);
    var hdTitle = document.getElementById("hdTitle_" + iPosition);
    var hdFirstName = document.getElementById("hdName_" + iPosition);
    var hdLastName = document.getElementById("hdLastname_" + iPosition);
    var hdBirthDate = document.getElementById("hdBirthDate_" + iPosition);
    var hdBirthPlace = document.getElementById("hdBirthPlace_" + iPosition);
    var hdMemberLevel = document.getElementById("hdMemberLevelRcd_" + iPosition);
    var hdNationality = document.getElementById("hdNational_" + iPosition);
    var hdDocumentType = document.getElementById("hdDocumentType_" + iPosition);
    var hdDocumentNo = document.getElementById("hdDocNumber_" + iPosition);
    var hdPlaceOfIssue = document.getElementById("hdIssuePlace_" + iPosition);
    var hdIssueDate = document.getElementById("hdIssueDate_" + iPosition);
    var hdExpiryDate = document.getElementById("hdExpiryDate_" + iPosition);
    var hdPaxType = document.getElementById("hdPaxType_" + iPosition);
    var hdWeight = document.getElementById("hdWeight_" + iPosition);
    var hdIssueCountry = document.getElementById("hdIssueCountry_" + iPosition);

    //Input Control
    var txtClientNo = document.getElementById("txtClientNoInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");
    var txtBirthDate = document.getElementById("txtBirthDateInput");
    var txtBirthPlace = document.getElementById("txtPlaseOfBirth");
    var txtDocumentNo = document.getElementById("txtDocumentNumber");
    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
    var txtIssueDate = document.getElementById("txtIssueDate");
    var txtExpiryDate = document.getElementById("txtExpiryDate");
    var txtWeight = document.getElementById("txtWeight");

    var dvPaxType = document.getElementById("dvPaxType");

    //Clear Tab select
    var iCountTotalPax = document.getElementsByName("hdPassengerId").length;
    for (var i = 1; i <= iCountTotalPax; i++) {
        document.getElementById("ulPaxTab_" + i).className = "disable";
    }

    //Set Active Tab
    document.getElementById("ulPaxTab_" + iPosition).className = "active";


    //Set Selected row number
    iPaxSelectPosition = iPosition;

    if (GetControlValue(hdTitle) == "|") {
        SetComboValue("stTitleInput", "");
    }
    else {
        SetComboValue("stTitleInput", GetControlValue(hdTitle));
    }
    SetComboValue("stNational", GetControlValue(hdNationality));
    SetComboValue("stDocumentType", GetControlValue(hdDocumentType));
    SetComboValue("stIssueCountry", GetControlValue(hdIssueCountry));

    if (txtClientNo != null) {
        if (GetControlValue(hdClientNo).length == 0) {
            txtClientNo.value = objLanguage.default_value_2;
            $("#txtClientNoInput").addClass("watermarkOn");
        }
        else {
            txtClientNo.value = GetControlValue(hdClientNo);
            $("#txtClientNoInput").removeClass("watermarkOn");
        }

    }

    if (txtFirstName != null) {
        if (GetControlValue(hdFirstName).length == 0) {
            txtFirstName.value = objLanguage.default_value_2;
            $("#txtFirstNameInput").addClass("watermarkOn");
        }
        else {
            txtFirstName.value = GetControlValue(hdFirstName);
            $("#txtFirstNameInput").removeClass("watermarkOn");
        }

    }

    if (txtLastName != null) {
        if (GetControlValue(hdLastName).length == 0) {
            txtLastName.value = objLanguage.default_value_2;
            $("#txtLastnameInput").addClass("watermarkOn");
        }
        else {
            txtLastName.value = GetControlValue(hdLastName);
            $("#txtLastnameInput").removeClass("watermarkOn");
        }
    }

    if (txtBirthDate != null) {
        if (GetControlValue(hdBirthDate).length == 0 || GetControlValue(hdBirthDate) == GetDateMask() || GetControlValue(hdBirthDate) == objLanguage.default_value_1) {
            txtBirthDate.value = objLanguage.default_value_1;
            $("#txtBirthDateInput").addClass("watermarkOn");
        }
        else {
            txtBirthDate.value = GetControlValue(hdBirthDate);
            $("#txtBirthDateInput").removeClass("watermarkOn");
        }

    }

    if (txtBirthPlace != null) {
        if (GetControlValue(hdBirthPlace).length == 0) {
            txtBirthPlace.value = objLanguage.default_value_2;
            $("#txtPlaseOfBirth").addClass("watermarkOn");
        }
        else {
            txtBirthPlace.value = GetControlValue(hdBirthPlace);
            $("#txtPlaseOfBirth").removeClass("watermarkOn");
        }
    }

    if (txtDocumentNo != null) {
        if (GetControlValue(hdDocumentNo).length == 0) {
            txtDocumentNo.value = objLanguage.default_value_2;
            $("#txtDocumentNumber").addClass("watermarkOn");
        }
        else {
            txtDocumentNo.value = GetControlValue(hdDocumentNo);
            $("#txtDocumentNumber").removeClass("watermarkOn");
        }
    }

    if (txtPlaceOfIssue != null) {
        if (GetControlValue(hdPlaceOfIssue).length == 0) {
            txtPlaceOfIssue.value = objLanguage.default_value_2;
            $("#txtPlaseOfIssue").addClass("watermarkOn");
        }
        else {
            txtPlaceOfIssue.value = GetControlValue(hdPlaceOfIssue);
            $("#txtPlaseOfIssue").removeClass("watermarkOn");
        }

    }

    if (txtIssueDate != null) {
        if (GetControlValue(hdIssueDate).length == 0 || GetControlValue(hdIssueDate) == GetDateMask() || GetControlValue(hdIssueDate) == objLanguage.default_value_1) {
            txtIssueDate.value = objLanguage.default_value_1;
            $("#txtIssueDate").addClass("watermarkOn");
        }
        else {
            txtIssueDate.value = GetControlValue(hdIssueDate);
            $("#txtIssueDate").removeClass("watermarkOn");
        }
    }

    if (txtExpiryDate != null) {
        if (GetControlValue(hdExpiryDate).length == 0 || GetControlValue(hdExpiryDate) == GetDateMask() || GetControlValue(hdExpiryDate) == objLanguage.default_value_1) {
            txtExpiryDate.value = objLanguage.default_value_1;
            $("#txtExpiryDate").addClass("watermarkOn");
        }
        else {
            txtExpiryDate.value = GetControlValue(hdExpiryDate);
            $("#txtExpiryDate").removeClass("watermarkOn");
        }
    }

    if (txtWeight != null) {
        if (GetControlValue(hdWeight).length == 0) {
            txtWeight.value = objLanguage.default_value_3;
            $("#txtWeight").addClass("watermarkOn");
        }
        else {
            txtWeight.value = GetControlValue(hdWeight);
            $("#txtWeight").removeClass("watermarkOn");
        }
    }

    //Display PAX type.
    var objDvBaggage = document.getElementById("dvBaggage");

    if (GetControlValue(hdPaxType) == "ADULT") {
        dvPaxType.innerHTML = "ADULT";
        if (objDvBaggage != null) {
            objDvBaggage.style.display = "block";
        }
    }
    else if (GetControlValue(hdPaxType) == "CHD") {
        dvPaxType.innerHTML = "CHILD";
        if (objDvBaggage != null) {
            objDvBaggage.style.display = "block";
        }
    }
    else {
        dvPaxType.innerHTML = "INFANT";
        if (objDvBaggage != null) {
            objDvBaggage.style.display = "none";
        }


    }

    if (GetControlValue(hdClientNo).length > 0 && GetControlValue(hdClientNo) != "0") {
        LockPassengerInput(true);
    }
    else {
        LockPassengerInput(false);
    }

    //Set Name To current Tab Header
    var objPaxTabName = document.getElementById("dvPaxTabName");
    var objCurrentTitle = document.getElementById("hdTitle_" + iPosition);
    var objCurrentFirstName = document.getElementById("hdName_" + iPosition);
    var objCurrentLastName = document.getElementById("hdLastname_" + iPosition);
    if (objPaxTabName != null) {
        if (GetControlValue(objCurrentFirstName).length == 0 & GetControlValue(objCurrentLastName).length == 0) {
            objPaxTabName.innerHTML = objLanguage.default_value_4;
        }
        else {
            objPaxTabName.innerHTML = GetControlValue(objCurrentLastName).toUpperCase() + " " + GetControlValue(objCurrentFirstName).toUpperCase() + " " + GetControlValue(objCurrentTitle).split("|")[0];
        }
    }

}
function ClearErrorMsg() {
    if (document.getElementById("spErrFirstname") != null) {
        document.getElementById("spErrFirstname").innerHTML = "";
    }
    if (document.getElementById("spErrLastname") != null) {
        document.getElementById("spErrLastname").innerHTML = "";
    }
    if (document.getElementById("spErrHome") != null) {
        document.getElementById("spErrHome").innerHTML = "";
    }
    if (document.getElementById("spErrEmail") != null) {
        document.getElementById("spErrEmail").innerHTML = "";
    }
    if (document.getElementById("spErrAddress1") != null) {
        document.getElementById("spErrAddress1").innerHTML = "";
    }
    if (document.getElementById("spErrCity") != null) {
        document.getElementById("spErrCity").innerHTML = "";
    }
    if (document.getElementById("spErrZip") != null) {
        document.getElementById("spErrZip").innerHTML = "";
    }
    if (document.getElementById("spMobileEmail") != null) {
        document.getElementById("spMobileEmail").innerHTML = "";
    }
}

function CookiesSameAsContact() {
    var objCookies = getCookie("coContact");


    if (objCookies != null) {

        var objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
        var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
        var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
        var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
        var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
        var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
        var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
        var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
        var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
        var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
        var objCity = document.getElementById(FindControlName("input", "txtContactCity"));
        var objCountry = document.getElementById(FindControlName("select", "stContactCountry"));

        var objTaxId = document.getElementById(FindControlName("input", "txtTIN"));
        var objInvoiceReceiver = document.getElementById(FindControlName("input", "txtInvoiceReceiver"));
        var txtEUVat = document.getElementById(FindControlName("input", "txtEUVat"));
        var txtCIN = document.getElementById(FindControlName("input", "txtCIN"));
        var objOptionalEmail = document.getElementById(FindControlName("input", "txtContactEmail2"));
        var objMobileEmail = document.getElementById(FindControlName("input", "txtMobileEmail"));
        var objState = document.getElementById(FindControlName("input", "txtContactState"));

        var strResult = $.base64.decode(objCookies);
        if (strResult.length > 0) {

            var strContactInfo = strResult.split("{}");
            if (strContactInfo.length == 20) {
                //Get passenger contact information.

                if (GetSelectedOption(objTitle) == strContactInfo[0] &
                    GetControlValue(objFirstName) == strContactInfo[1] &
                    GetControlValue(objLastName) == strContactInfo[2] &
                    GetControlValue(objMobile) == strContactInfo[3] &
                    GetControlValue(objHome) == strContactInfo[4] &
                    GetControlValue(objBusiness) == strContactInfo[5] &
                    GetControlValue(objEmail) == strContactInfo[6] &
                    GetControlValue(objAddress1) == strContactInfo[7] &
                    GetControlValue(objAddress2) == strContactInfo[8] &
                    GetControlValue(objZip) == strContactInfo[9] &
                    GetControlValue(objCity) == strContactInfo[10] &
                    GetSelectedOption(objCountry) == strContactInfo[11] &
                    (objTaxId == null || GetControlValue(objTaxId) == strContactInfo[13]) &
                    (objInvoiceReceiver == null || GetControlValue(objInvoiceReceiver) == strContactInfo[14]) &
                    (txtEUVat == null || GetControlValue(txtEUVat) == strContactInfo[15]) &
                    (txtCIN == null || GetControlValue(txtCIN) == strContactInfo[16]) &
                    (objOptionalEmail == null || GetControlValue(objOptionalEmail) == strContactInfo[17]) &
                    (objMobileEmail == null || GetControlValue(objMobileEmail) == strContactInfo[18]) &
                    (objState == null || GetControlValue(objState) == strContactInfo[19])) {

                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    }
    else {
        return false;
    }

    objCookies = null;
    objChk = null;
}


function SetPassengerDetail() {
    GetContactInformationFromCookies();
    scroll(0, 0);

    var objChk = document.getElementById("chkRemember");
    var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
    var objPaxFirstName;
    var objPaxLastName;

    var iPaxcount = document.getElementsByName("hdPassengerId").length;
    var objIAmPassenger = document.getElementById("chkCopy");

    objPaxFirstName = document.getElementById("hdName_1");
    objPaxLastName = document.getElementById("hdLastname_1");

    if (GetControlValue(objPaxFirstName).length > 0 && GetControlValue(objPaxLastName).length > 0 &&
        GetControlValue(objFirstName).toUpperCase() != GetControlValue(objPaxFirstName).toUpperCase() && GetControlValue(objLastName).toUpperCase() != GetControlValue(objPaxLastName).toUpperCase()) {

        objIAmPassenger.checked = false;

        //        for (var i = 1; i <= iPaxcount; i++) {
        //            objPaxFirstName = document.getElementById("hdName_" + i);
        //            objPaxLastName = document.getElementById("hdLastname_" + i);

        //            if (GetControlValue(objFirstName).toUpperCase() == GetControlValue(objPaxFirstName).toUpperCase() &&
        //                GetControlValue(objLastName).toUpperCase() == GetControlValue(objPaxLastName).toUpperCase()) {

        //                var objIAmPassenger = document.getElementById("chkCopy");
        //                objIAmPassenger.checked = true;
        //                objIAmPassenger = null;

        //                break;
        //            }
        //        }
    }
    else {
        objIAmPassenger.checked = true;
    }

    //Lock all control that are client.
    var objProfileId = document.getElementsByName("hdClientProfileId");
    for (var iCount = 1; iCount <= objProfileId.length; iCount++) {
        if (document.getElementById("txtClientNoInput").value.length > 0 &&
            document.getElementById("txtFirstNameInput").value.length > 0 &&
            document.getElementById("txtLastnameInput").value.length > 0) {
            //Lock Control.
            LockPassengerInput(true);
        }
    }

    //Set Current Tab position to 1
    GetPassengerValue(1);

    //Initial mask edit
    InitializePassengerMaskEdit();

    //Initialize Watermark.
    InitializeContactWaterMark();

    //Add Baggage onChange even.
    $("#BaggageDepart").change(function () {
        CalculateBaggageFee(false);
        SetPassengerValue();
    });
    $("#BaggageReturn").change(function () {
        CalculateBaggageFee(true);
        SetPassengerValue();
    });

    //JQuery for contact detail
    $("#" + FindControlName("select", "stContactTitle")).change(function () {

        var objChkFirstPax = document.getElementById("chkCopy");
        if (objChkFirstPax.checked == true) {
            var objPaxTitle = document.getElementById("stTitleInput");
            var objContactTitle = document.getElementById(this.id);

            GetPassengerValue(1);
            objPaxTitle.selectedIndex = objContactTitle.selectedIndex;
            SetPassengerValue();
            SetInputnameToTab(iPaxSelectPosition);
        }
    });

    $("#" + FindControlName("input", "txtContactLastname")).keyup(function () {

        var objChkFirstPax = document.getElementById("chkCopy");

        if (objChkFirstPax.checked == true) {
            var objPaxLastname = document.getElementById("txtLastnameInput");
            var objContactLastname = document.getElementById(this.id);

            GetPassengerValue(1);
            objPaxLastname.value = objContactLastname.value.toUpperCase();
            SetPassengerValue();
            SetInputnameToTab(iPaxSelectPosition);
        }
    });

    $("#" + FindControlName("input", "txtContactFirstname")).keyup(function () {

        var objChkFirstPax = document.getElementById("chkCopy");

        if (objChkFirstPax.checked == true) {
            var objPaxFirstname = document.getElementById("txtFirstNameInput");
            var objContactFirstname = document.getElementById(this.id);

            GetPassengerValue(1);
            objPaxFirstname.value = objContactFirstname.value.toUpperCase();
            SetPassengerValue();
            SetInputnameToTab(iPaxSelectPosition);
        }
    });
    //JQuery for passenger.
    $("#" + FindControlName("select", "stTitleInput")).change(function () {

        SelectUsedFirstPassenger();

    });

    $("#" + FindControlName("input", "txtLastnameInput")).keyup(function () {

        SelectUsedFirstPassenger();
    });

    $("#" + FindControlName("input", "txtFirstNameInput")).keyup(function () {

        SelectUsedFirstPassenger();
    });

    //Check Remember if cookes information is the same.
    if (CookiesSameAsContact() == true) {
        var objLastname = document.getElementById("txtLastnameInput");
        var objFirstname = document.getElementById("txtFirstNameInput");

        if (GetControlValue(objLastname).length == 0 & GetControlValue(objFirstname).length == 0) {
            objIAmPassenger.checked = true;
            CopyContactToPassenger();
        }
        objChk.checked = true;
    }
    else {
        objChk.checked = false;
    }

    objProfileId = null;

    objFirstName = null;
    objLastName = null;
    objPaxFirstName = null;
    objPaxLastName = null;
}
function InitializePassengerMaskEdit() {
    InitializeDateMaskEdit("txtIssueDate");
    InitializeDateMaskEdit("txtExpiryDate");
    InitializeDateMaskEdit("txtBirthDateInput");
}
function InitializeContactWaterMark() {

    //Contact information
    InitializeWaterMark("ctl00_stContactTitle", objLanguage.default_value_7, "select");
    InitializeWaterMark("ctl00_txtContactFirstname", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactLastname", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactMobile", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactHome", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactBusiness", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactEmail", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactAddress1", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactAddress2", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactZip", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactCity", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtTIN", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtInvoiceReceiver", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtEUVat", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtCIN", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactEmail2", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtMobileEmail", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactState", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_stContactCountry", objLanguage.default_value_2, "select");

    //Passenger Information
    InitializeWaterMark("stTitleInput", objLanguage.default_value_7, "select");
    InitializeWaterMark("txtClientNoInput", objLanguage.default_value_3, "input");
    InitializeWaterMark("txtFirstNameInput", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtLastnameInput", objLanguage.default_value_2, "input");

    if (document.getElementById("txtBirthDateInput") != null) {
        InitializeWaterMark("txtBirthDateInput", objLanguage.default_value_1, "input");
    }
    if (document.getElementById("txtPlaseOfBirth") != null) {
        InitializeWaterMark("txtPlaseOfBirth", objLanguage.default_value_2, "input");
    }
    if (document.getElementById("txtDocumentNumber") != null) {
        InitializeWaterMark("txtDocumentNumber", objLanguage.default_value_2, "input");
    }
    if (document.getElementById("txtPlaseOfIssue") != null) {
        InitializeWaterMark("txtPlaseOfIssue", objLanguage.default_value_2, "input");
    }
    if (document.getElementById("txtIssueDate") != null) {
        InitializeWaterMark("txtIssueDate", objLanguage.default_value_1, "input");
    }
    if (document.getElementById("txtExpiryDate") != null) {
        InitializeWaterMark("txtExpiryDate", objLanguage.default_value_1, "input");
    }

    if (document.getElementById("txtWeight") != null) {
        InitializeWaterMark("txtWeight", objLanguage.default_value_3, "input");
    }

    if (document.getElementById("stNational") != null) {
        InitializeWaterMark("stNational", objLanguage.default_value_2, "select");
    }

    if (document.getElementById("stDocumentType") != null) {
        InitializeWaterMark("stDocumentType", objLanguage.default_value_3, "select");
    }

    if (document.getElementById("stIssueCountry") != null) {
        InitializeWaterMark("stIssueCountry", objLanguage.default_value_2, "select");
    }
    //Baggage watermark
    InitializeWaterMark("BaggageDepart", objLanguage.default_value_7, "select");
    InitializeWaterMark("BaggageReturn", objLanguage.default_value_7, "select");
}
function SelectUsedFirstPassenger() {
    if (iPaxSelectPosition == 1) {
        var objChkFirstPax = document.getElementById("chkCopy");

        var objPaxTitle = document.getElementById("stTitleInput");
        var objContactTitle = document.getElementById(FindControlName("select", "stContactTitle"));

        var objPaxFirstname = document.getElementById("txtFirstNameInput");
        var objContactFirstname = document.getElementById(FindControlName("input", "txtContactFirstname"));

        var objPaxLastname = document.getElementById("txtLastnameInput");
        var objContactLastname = document.getElementById(FindControlName("input", "txtContactLastname"));

        if (objPaxTitle.selectedIndex != objContactTitle.selectedIndex ||
            GetControlValue(objPaxFirstname).toUpperCase() != GetControlValue(objContactFirstname).toUpperCase() ||
            GetControlValue(objPaxLastname).toUpperCase() != GetControlValue(objContactLastname).toUpperCase()) {

            objChkFirstPax.checked = false;
        }
        else {
            objChkFirstPax.checked = true;
        }
    }
}

// JScript File
var ccErrorNo = 0;
var ccErrors = new Array();

function paymentCreditCard() {

    if (MultipleTab.IsCorrectTab()) {
        Origin_route = document.getElementById("hdOrigin_route");
        var objChk = document.getElementById("chkPayCreditCard");
        var objconfirmInsu = document.getElementById("chkAcceptDisclose");
        var show_insurance_on_web_flag = document.getElementById("show_insurance_on_web_flag");

        var objChkYes;
        var objChkNo;
        var objInsDetail;

        if (Origin_route.value == "HKG") {
            objChkYes = document.getElementById("optGetInsuranceHK");
            objChkNo = document.getElementById("optNoInsuranceHK");
            objInsDetail = document.getElementById("dvInsuranceDetailHK");
        }
        else {
            objChkYes = document.getElementById("optGetInsurance");
            objChkNo = document.getElementById("optNoInsurance");
            objInsDetail = document.getElementById("dvInsuranceDetail");
        }

        if (objChkYes.checked == false && objChkNo.checked == false && show_insurance_on_web_flag.value == "1" && objInsDetail.style.display == "block") {
            //Please select if you need insurance or not.<br/>For your peace in mind, we recommend you add travel insurance to your itinerary
            ShowMessageBox(objLanguage.Alert_Message_154, 0, '');

        }
        else if (objChkYes.checked == true && objconfirmInsu.checked == false && show_insurance_on_web_flag.value == "1" && objInsDetail.style.display == "block" && Origin_route.value != "HKG") {
            //Please indicate your acceptance of out insurance condition by clicking the box.
            ShowMessageBox(objLanguage.Alert_Message_155, 0, '');

        }
        else if (objChk.checked == true) {

            ShowLoadBar(true, false);
            var objNameOnCard = document.getElementById("txtNameOnCard");
            var objCardType = document.getElementById(FindControlName("select", "optCardType"));
            var objIssueNumber = document.getElementById("txtIssueNumber");
            var objIssueMonth = document.getElementById("optIssueMonth");
            var objIssueYear = document.getElementById("optIssueYear");
            var objCardNumber = document.getElementById("txtCardNumber");
            var objExpiredMonth = document.getElementById("optExpiredMonth");
            var objExpiredYear = document.getElementById("optExpiredYear");
            var objCvv = document.getElementById("txtCvv");
            var objAddress1 = document.getElementById("txtAddress1");
            var objAddress2 = document.getElementById("txtAddress2");
            var objCity = document.getElementById("txtCity");
            var objCounty = document.getElementById("txtCounty");
            var objPostCode = document.getElementById("txtPostCode");
            var objCountry = document.getElementById(FindControlName("select", "optCountry"));
            var objCondition = document.getElementById("chkPayCreditCard");

            var xmlStr = ValidateCreditCard(objNameOnCard,
                                    objCardType,
                                    objIssueNumber,
                                    objIssueMonth,
                                    objIssueYear,
                                    objCardNumber,
                                    objExpiredMonth,
                                    objExpiredYear,
                                    objCvv,
                                    objAddress1,
                                    objAddress2,
                                    objCity,
                                    objCounty,
                                    objPostCode,
                                    objCountry,
                                    objCondition);

            if (xmlStr.length > 0) {
                tikAeroB2C.WebService.B2cService.PaymentCreditCard(xmlStr, SuccessPaymentCreditCard, showErrorPayment, showTimeOutPayment);
            }
        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_49, 0, '');

        }

    }
    else {
        MultipleTab.Redirect();
    }
}

function SuccessPaymentCreditCard(result) {
    var strError = "";

    if (result.length > 0) {
        if (result == "{002}" || result == "{005}") {
            ShowMessageBox(objLanguage.Alert_Message_161, 0, 'loadHome');
        }
        else {
            var obj;
            var arrResult = result.split("{}");
            if (arrResult.length == 1) {

                ShowLoadBar(false, false);

                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;
                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");

                SubmitAnalyticPageThankYou(creditcard_subtype);
                //Call google adword.
                CallAdword();
                // clear tab id
                MultipleTab.ClearID();
                // clear Insurance session
                SetShowInsurance("0");

            }
            else {
                if (arrResult[0] == "200") {
                    //Infant over limit
                    strError = objLanguage.Alert_Message_59;
                }
                else if (arrResult[0] == "3D") {
                    //Redirect to 3D secure    
                    document.write($.base64.decode(arrResult[1]));
                }
                else if (arrResult[0] == "203") {
                    strError = objLanguage.Alert_Message_164;
                }
                else {
                    strError = objLanguage.Alert_Message_47;
                }
                ShowLoadBar(false, false);
                ShowMessageBox(strError, 0, '');
            }
            obj = null;
        }
    }
    else {
        ShowLoadBar(false, false);
        ShowMessageBox(objLanguage.Alert_Message_161, 0, 'loadHome');
    }
}

function activateCreditCardControl() {
    var objCardType = document.getElementById(FindControlName("select", "optCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objCreditCardFare = document.getElementById("dvAdditionalFee");
    var objFareTotal = document.getElementById("dvFareTotal");
    var objExpiredYear = document.getElementById("optExpiredYear");
    var optExpiredMonth = document.getElementById("optExpiredMonth");

    var objCvv = document.getElementById('dvCvv');
    var objExpiryDate = document.getElementById('dvExpiryDate');
    var objDvAddress = document.getElementById('dvAddress');
    var objDvIssueDate = document.getElementById('dvIssuDate');
    var objIssueNumber = document.getElementById('dvIssueNumber');
    var objCardNumber = document.getElementById("txtCardNumber");

    ToolTipColor();

    //Clear Error message.
    EmptyErrorMessage();

    ShowCreditCardField(objCardType,
                        objPaymentValue,
                        objCreditCardFare,
                        objFareTotal,
                        objExpiredYear,
                        optExpiredMonth,
                        objCvv,
                        objExpiryDate,
                        objDvAddress,
                        objDvIssueDate,
                        objIssueNumber,
                        objCardNumber);

    ToolTipColor();


}
function LoadMyName() {
    var objMyname = document.getElementById("chkMyname");
    if (objMyname.checked == true) {
        tikAeroB2C.WebService.B2cService.GetMyName(SuccessLoadName, showError, showTimeOut);
    }
    else {
        //If check bok not checked clear card holder information.
        SuccessLoadName("");
    }
    objMyname = null;
}

function SuccessLoadName(result) {
    var objNameOnCard = document.getElementById("txtNameOnCard");
    var objAddress1 = document.getElementById("txtAddress1");
    var objAddress2 = document.getElementById("txtAddress2");
    var objCity = document.getElementById("txtCity");
    var objCounty = document.getElementById("txtCounty");
    var objPostCode = document.getElementById("txtPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optCountry"));

    SetMyName(result,
              objNameOnCard,
              objAddress1,
              objAddress2,
              objCity,
              objCounty,
              objPostCode,
              objCountry);
}

function SetMyName(arg,
                   objNameOnCard,
                   objAddress1,
                   objAddress2,
                   objCity,
                   objCounty,
                   objPostCode,
                   objCountry) {
    if (arg.length > 0) {
        objNameOnCard.value = arg.split("{}")[0] + " " + arg.split("{}")[1];
        objAddress1.value = arg.split("{}")[2];
        objAddress2.value = arg.split("{}")[3];
        objCounty.value = arg.split("{}")[5];
        objCity.value = arg.split("{}")[6];
        objPostCode.value = arg.split("{}")[9];

        for (icount = 0; icount < objCountry.length; icount++) {
            if (objCountry.options[icount].value == arg.split("{}")[8]) {
                objCountry.selectedIndex = icount;
                break;
            }
        }

        if (GetControlValue(objNameOnCard).length > 0) {
            $("#" + objNameOnCard.id).removeClass("watermarkOn");
        }
        else {
            objNameOnCard.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objAddress1).length > 0) {
            $("#" + objAddress1.id).removeClass("watermarkOn");
        }
        else {
            objAddress1.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objAddress2).length > 0) {
            $("#" + objAddress2.id).removeClass("watermarkOn");
        }
        else {
            objAddress2.value = objLanguage.default_value_3;
        }

        if (GetControlValue(objCity).length > 0) {
            $("#" + objCity.id).removeClass("watermarkOn");
        }
        else {
            objCity.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objCounty).length > 0) {
            $("#" + objCounty.id).removeClass("watermarkOn");
        }
        else {
            objCounty.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objPostCode).length > 0) {
            $("#" + objPostCode.id).removeClass("watermarkOn");
        }
        else {
            objPostCode.value = objLanguage.default_value_2;
        }
    }
    else {

        objNameOnCard.value = objLanguage.default_value_2;
        objAddress1.value = objLanguage.default_value_2;
        objAddress2.value = objLanguage.default_value_3;
        objCity.value = objLanguage.default_value_2;
        objCounty.value = objLanguage.default_value_2;
        objPostCode.value = objLanguage.default_value_2;

        $("#" + objNameOnCard.id).addClass("watermarkOn");
        $("#" + objAddress1.id).addClass("watermarkOn");
        $("#" + objAddress2.id).addClass("watermarkOn");
        $("#" + objCity.id).addClass("watermarkOn");
        $("#" + objCounty.id).addClass("watermarkOn");
        $("#" + objPostCode.id).addClass("watermarkOn");
    }

}
function payment(arg, ctx) {
    //StartAsync(arg,ctx);
}

function checkCreditCard(cardnumber, cardname, bCheckLength) {
    // Establish card type
    var cardType = -1;

    // Ensure that the user has provided a credit card number
    if (cardnumber.length == 0) {
        ccErrorNo = 1;
        return false;
    }

    // Now remove any spaces from the credit card number
    cardnumber = cardnumber.replace(/\s/g, "");

    // Check that the number is numeric
    var cardNo = cardnumber;
    var cardexp = /^[0-9]{6,19}$/;
    if (!cardexp.exec(cardNo)) {
        ccErrorNo = 2;
        return false;
    }

    // The following are the card-specific checks we undertake.
    var PrefixValid = false;

    // We use these for holding the valid lengths and prefixes of a card type
    var prefix = new Array();
    var lengths = new Array();
    var strCardRange = new Array();
    var strCardNo;
    var strCardType = "";

    for (var i = 0; i < cards.length; i++) {
        // Load an array with the valid prefixes for this card
        prefix = cards[i].prefixes.split(",");
        // Now see if any of them match what we have in the card number

        for (j = 0; j < prefix.length; j++) {
            strCardRange = prefix[j].split("-");
            if (strCardRange.length == 1) {
                strCardNo = cardNo.substring(0, strCardRange[0].length);
                if (strCardNo == strCardRange[0]) {
                    strCardType = cards[i].name.toLowerCase();
                    cardType = i;
                    break;
                }
            }
            else {
                strCardNo = cardNo.substring(0, strCardRange[0].length);
                if (strCardNo >= strCardRange[0] && strCardNo <= strCardRange[1]) {
                    strCardType = strCardType = cards[i].name.toLowerCase();
                    cardType = i;
                    break;
                }
            }
        }
        if (strCardType.length > 0) {
            break;
        }
    }

    if (cardname.toLowerCase() == strCardType) {
        PrefixValid = true;
    }

    // If it isn't a valid prefix there's no point at looking at the length
    if (!PrefixValid) {
        ccErrorNo = 3;
        return false;
    }

    if (bCheckLength == true) {
        // If card type not found, report an error
        if (cardType == -1) {
            ccErrorNo = 0;
            return false;
        }

        // Now check the modulus 10 check digit - if required
        if (cards[cardType].checkdigit) {
            var checksum = 0;                                  // running checksum total
            var mychar = "";                                   // next char to process
            var j = 1;                                         // takes value of 1 or 2

            // Process each digit one by one starting at the right
            var calc;
            for (i = cardNo.length - 1; i >= 0; i--) {

                // Extract the next digit and multiply by 1 or 2 on alternative digits.
                calc = Number(cardNo.charAt(i)) * j;

                // If the result is in two digits add 1 to the checksum total
                if (calc > 9) {
                    checksum = checksum + 1;
                    calc = calc - 10;
                }

                // Add the units element to the checksum total
                checksum = checksum + calc;

                // Switch the value of j
                if (j == 1)
                { j = 2; }
                else
                { j = 1; }
            }
            // All done - if checksum is divisible by 10, it is a valid modulus 10.
            // If not, report an error.

            if (checksum % 10 != 0) {
                ccErrorNo = 3;
                return false;
            }
        }
        // See if the length is valid for this card
        var LengthValid = false;
        lengths = cards[cardType].length.split(",");
        for (j = 0; j < lengths.length; j++) {
            if (cardNo.length == lengths[j]) LengthValid = true;
        }

        // See if all is OK by seeing if the length was valid. We only check the 
        // length if all else was hunky dory.
        if (!LengthValid) {
            ccErrorNo = 4;
            return false;
        };
    }
    // The credit card is in the required format.
    return true;
}
function EmptyErrorMessage() {
    document.getElementById("spnError").innerHTML = "";
    document.getElementById("spnVoucherError").innerHTML = "";
}

function validateVoucher(voucherNumber, password) {
    var objVoucherError = document.getElementById("spnVoucherError");
    var errorMessage = "";

    if (voucherNumber.length == 0 || IsNumeric(voucherNumber) == false) {
        errorMessage = errorMessage + objLanguage.Alert_Message_34 + "<div class='clear-all'></div>"; //"- Please supply a valid Voucher Number.
    }
    if (password.length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_35 + "<div class='clear-all'></div>"; //"- Please supply a valid Password.
    }

    if (errorMessage.length == 0) {
        objVoucherError.innerHTML = "";
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ValidateVoucher(voucherNumber, password, successValidateVoucher, showError, showTimeOut);
    }
    else {
        ShowMessageBox(errorMessage, 0, '');
    }
}
function successValidateVoucher(result) {
    var objVoucher = document.getElementById("dvVoucherDetail");
    var objVoucherError = document.getElementById("spnVoucherError");
    var objDvPayVoucher = document.getElementById("dvPayVoucher");

    if (result.length == 0) {
        ShowMessageBox(objLanguage.Alert_Message_36, 0, ''); //"Voucher not found or wrong password!"
    }
    else if (result == "DUP") {
        ShowMessageBox(objLanguage.Alert_Message_75, 0, ''); //"This voucher already exist!";
    }
    else if (result == "{002}") {
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        objVoucher.innerHTML = result;
        objDvPayVoucher.style.display = "block";
        ShowProgressBar(false);
        //Calculate total amount voucher selected.
        CalculateTotalMultiplePayment();
    }

    objDvPayVoucher = null;
    objVoucherError = null;
    objVoucher = null;
}

function paymentVoucher() {

    var objChk = document.getElementById("chkPayVoucher");
    if (objChk.checked == true) {
        if (AceInsuranceCheckPass() == "000") {
        }
        else {
            var objVoucher = document.getElementsByName("nVoucher");
            var objVoucherError = document.getElementById("spnVoucherError");
            var strValue = "";
            var strVoucherId = "";
            var strVoucherNumber = "";
            var strFormOfPayment = "";
            var strPaymentSubType = "";

            if (objVoucher != null) {
                for (var iCount = 0; iCount < objVoucher.length; iCount++) {
                    if (objVoucher[iCount].checked == true) {
                        strValue = objVoucher[iCount].value;
                        break;
                    }
                }
                if (strValue.length > 0) {
                    // var objCondition = document.getElementById("chkPayVoucher");
                    // if (objCondition.checked == true) {
                    ShowLoadBar(true, false);

                    strVoucherId = strValue.split("|")[0];
                    strVoucherNumber = strValue.split("|")[1];
                    strFormOfPayment = strValue.split("|")[2];
                    strPaymentSubType = strValue.split("|")[3];

                    //Process payment voucher.
                    tikAeroB2C.WebService.B2cService.PaymentVoucher(strVoucherId, strVoucherNumber, strFormOfPayment, strPaymentSubType, SuccessPaymentVoucher, showError, showTimeOut);
                    /*}
                    else {
                    ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
                    }
                    objCondition = null;
                    }
                    else {
                    objVoucherError.innerHTML = objLanguage.Alert_Message_76; //"- Please select voucher.";
                    */
                }

            }
            objVoucherError = null;
            objVoucher = null;
        }
    }
    else {
        //Please accept Terms and Conditions
        ShowMessageBox(objLanguage.Alert_Message_49, 0, '');
    }
}
function SuccessPaymentVoucher(result) {
    var obj;
    if (result == "{002}") {
        ShowMessageBox(objLanguage.Alert_Message_161, 0, 'loadHome');
    }
    else {
        if (result.split("{}").length == 1) {

            ShowLoadBar(false, false);


            obj = document.getElementById("dvContainer");
            obj.innerHTML = result;
            //Hide right side information.
            DisplayQuoteSummary("", "", "");
            ShowClientLogonMenu(false, "");

            //Call google analytic.
            SubmitAnalyticPageThankYou('voucher');
            //Call google adword.
            CallAdword();
            scroll(0, 0);
        }
        else {
            ShowLoadBar(false, false);
            obj = document.getElementById("spnVoucherError");
            if (result.split("{}")[1] == "200") {
                //Infant over limit
                obj.innerHTML = objLanguage.Alert_Message_59;
            }
            else {
                obj.innerHTML = result.split("{}")[1];
            }
        }
        obj = null;
    }
}

function showhidelayer(id) {
    Origin_route = document.getElementById("hdOrigin_route");
    var show_insurance_on_web_flag = document.getElementById("show_insurance_on_web_flag");

    var dvCreditCardButton = document.getElementById("dvCreditCardButton");
    var dvVoucherButton = document.getElementById("dvVoucherButton");
    var dvPaylaterButton = document.getElementById("dvPaylaterButton");
    var dvDIWButton = document.getElementById("dvDIWButton");

    var MethodBox = document.getElementById("MethodBox");

    var objChkYes;
    var objChkNo;
    var objInsDetail;

    // Hide All
    if (dvCreditCardButton != null)
        dvCreditCardButton.style.display = 'none';
    if (dvVoucherButton != null)
        dvVoucherButton.style.display = 'none';
    if (dvPaylaterButton != null)
        dvPaylaterButton.style.display = 'none';
    if (dvDIWButton != null)
        dvDIWButton.style.display = 'none';

    if (Origin_route.value == "HKG") {
        objChkYes = document.getElementById("optGetInsuranceHK");
        objChkNo = document.getElementById("optNoInsuranceHK");
        objInsDetail = document.getElementById("dvInsuranceDetailHK");
    }
    else {
        objChkYes = document.getElementById("optGetInsurance");
        objChkNo = document.getElementById("optNoInsurance");
        objInsDetail = document.getElementById("dvInsuranceDetail");
    }

    if (show_insurance_on_web_flag.value == "1" && objInsDetail.style.display == "block") {
        if (id != "CreditCard") {
            if (objChkYes != null && objChkNo != null) {
                objChkNo.checked = true;
                objChkNo.onclick();
            }
        }
        else {
            if (objChkYes != null && objChkNo != null) {
                objChkYes.checked = true;
            }
        }
    }

    var objAdditionalTotal = document.getElementById("divAdditionalFeeTotal");
    var objDvAdditionalFee = document.getElementById("dvAdditionalFee");

    clearPaymentTabClickColor();
    EmptyErrorMessage();

    document.getElementById('dvPaymentBack').style.display = 'none';
    document.getElementById('CreditCard').style.display = 'none';
    document.getElementById('Voucher').style.display = 'none';
    document.getElementById('BookNow').style.display = 'none';
    document.getElementById('DIW').style.display = 'none';

    //Show addition fee information if there is.
    ShowAdditionalFee();

    if (id == 'CreditCard') {
        if (document.getElementById('dvcTCreditCard') != null) {

            document.getElementById('dvcTCreditCard').className = "CreditCard";


            document.getElementById('CreditCard').style.display = 'block';
            if (dvCreditCardButton != null)
                dvCreditCardButton.style.display = 'block';

            RemoveWellNetFeeDisplay();
            //RemoveVoucherFee();

        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'Voucher') {
        if (document.getElementById('dvcTVoucher') != null) {

            document.getElementById('dvcTVoucher').className = "Voucher";


            document.getElementById('Voucher').style.display = 'block';
            if (objAdditionalTotal != null) {
                objAdditionalTotal.style.display = "none";
                objDvAdditionalFee.style.display = "none";
            }

            if (dvVoucherButton != null)
                dvVoucherButton.style.display = 'block';

            RemoveWellNetFeeDisplay();
            HideVoucherDetail();
            //GetVoucherFee();
        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'BookNow') {

        if (AceInsuranceCheckPass() != "000") {
            if (document.getElementById('dvcTLater') != null) {

                document.getElementById('dvcTLater').className = "BookNowPayLater";


                document.getElementById('BookNow').style.display = 'block';
                if (objAdditionalTotal != null) {
                    objAdditionalTotal.style.display = "none";
                    objDvAdditionalFee.style.display = "none";
                }

                if (dvPaylaterButton != null)
                    dvPaylaterButton.style.display = 'block';
                //Display Convient store Fee(Wellnet).
                GetWellNetFeeDisplay(gWellNetFeeCode);
                //RemoveVoucherFee();
            }
            else {
                HideAllPayment();
            }
        }
    }
    else if (id == 'DIW') {
        RemoveWellNetFeeDisplay();
        if (AceInsuranceCheckPass() != "000") {
            if (document.getElementById('dvcTDIW') != null) {

                document.getElementById('dvcTDIW').className = "YahooWallet";


                document.getElementById('DIW').style.display = 'block';

                if (dvDIWButton != null)
                    dvDIWButton.style.display = 'block';
            }
            else {
                HideAllPayment();
            }
        }
    }

}
function clearPaymentTabClickColor() {

    var objTCreditCard = document.getElementById('dvcTCreditCard');


    var objTVoucher = document.getElementById('dvcTVoucher');


    var objTLater = document.getElementById('dvcTLater');

    var objTDIW = document.getElementById('dvcTDIW');


    // Start Yai add External Payment

    var objTExternal = document.getElementById('dvcTExternal');

    // End Yai add External Payment

    if (objTCreditCard != null) {
        objTCreditCard.className = "CreditCardDisable";
    }

    if (objTVoucher != null) {
        objTVoucher.className = "VoucherDisable";
    }

    if (objTLater != null) {
        objTLater.className = "BookNowPayLaterDisable";
    }

    if (objTDIW != null) {
        objTDIW.className = "YahooWalletDisable";
    }

    // Start Yai add External Payment
    if (objTExternal != null) {
        objTExternal.className = "ExternalDisable";
    }
    // End Yai add External Payment

    objTCreditCard = null;

    objTVoucher = null;

    objTLater = null;

    // Start Yai add External Payment
    objTExternal = null;
    // End Yai add External Payment

}

// Start Yai add External Payment
function paymentExternal() {
    var objRefCode = document.getElementById("txtReferenceCode");
    var objConfirmRefCode = document.getElementById("txtConfirmReferenceCode");
    var objSecurityCode = document.getElementById("txtSecurityCode");
    var bSuccess = true;
    var errorMessage = "<div class='clear-all'></div>";

    if (trim(objRefCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_77 + "<div class='clear-all'></div>"; //"- Please supply a valid User ID Klik BCA.
    }
    if (trim(objConfirmRefCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_78 + "<div class='clear-all'></div>"; //"- Please supply a valid Confirm User ID Klik BCA.";
    }
    if ((trim(objRefCode.value) != "") && (trim(objConfirmRefCode.value) != "") && (trim(objRefCode.value) != trim(objConfirmRefCode.value))) {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_79 + "<div class='clear-all'></div>"; //"- Confirm User ID Klik BCA mismatch.";
    }
    if (trim(objSecurityCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_80 + "<div class='clear-all'></div>"; //"- Please supply a valid Security Code.";
    }
    if (bSuccess) {
        var objCondition = document.getElementById("chkPayExternal");

        if (objCondition.checked == true) {
            ShowLoadBar(true, false);
            var strRefCode = trim(objRefCode.value);
            var strSecurityCode = trim(objSecurityCode.value);
            // Check Security first, first param is security code, second parameter is case sensitive checking
            tikAeroB2C.WebService.B2cService.CheckCaptchaSecurity(strSecurityCode, true, SuccessCheckCaptchaSecurity, showErrorPayment, showTimeOutPayment);
            // If it does not need to check Captcha use below instead of above
            //tikAeroB2C.WebService.B2cService.ExternalPayment(false, strRefCode, SuccessPaymentExternal, showErrorPayment, showTimeOutPayment);
        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
        }
        objCondition = null;
    }
    else {
        document.getElementById("spnExternalError").innerHTML = errorMessage;
    }
}

function SuccessCheckCaptchaSecurity(result) {
    var bCaptcha = result;
    if (bCaptcha) {
        var objRefCode = document.getElementById("txtReferenceCode");
        strRefCode = trim(objRefCode.value);
        tikAeroB2C.WebService.B2cService.ExternalPayment(false, strRefCode, SuccessPaymentExternal, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowLoadBar(false, false);
        ShowMessageBox(objLanguage.Alert_Message_81, 0, ''); //"Invalid Security Code !"
    }
}

function SuccessPaymentExternal(result) {
    var obj;
    if (result.split("{}").length == 1) {
        obj = document.getElementById("dvContainer");
        obj.innerHTML = result;

        //Hide right side information.
        DisplayQuoteSummary("", "", "");
        ShowClientLogonMenu(false, "");
        scroll(0, 0);
    }
    else {
        obj = document.getElementById("spnExternalError");
        if (result.split("{}")[1] == "200") {
            //Infant over limit
            obj.innerHTML = objLanguage.Alert_Message_59;
        }
        else {
            obj.innerHTML = result.split("{}")[1];
        }


    }
    obj = null;
    ShowLoadBar(false, false);
}

function refreshCaptcha() {
    tikAeroB2C.WebService.B2cService.RefreshCaptcha(SuccessRefreshCaptcha, showErrorPayment, showTimeOutPayment);
}

function SuccessRefreshCaptcha(result) {
    var objImgSecurity = document.getElementById("imgSecurity");
    objImgSecurity.src = "captimage.aspx?r=" + result;
}
// End Yai add External Payment

function EmptyErrorMessage() {
    document.getElementById("spnError").innerHTML = "";
    document.getElementById("spnVoucherError").innerHTML = "";
}
function SaveBookedNowPayLater() {
    if (MultipleTab.IsCorrectTab()) {
        if (AceInsuranceCheckPass() == "000")
        { }
        else {
            var objChk = document.getElementById("chkPayPostPaid");
            if (objChk.checked == true) {
                ShowLoadBar(true, false);
                var bookDate = new Date().format("yyyyMMddHHmmss");
                //tikAeroB2C.WebService.B2cService.bookNowPaylater(false, bookDate, SuccessSaveBookedNowPayLater, showErrorPayment, showTimeOutPayment);
                tikAeroB2C.WebService.B2cService.WellnetPayment(false, bookDate, SuccessSaveBookedNowPayLater, showErrorPayment, showTimeOutPayment);
            }
            else {
                //Please accept Terms and Conditions
                ShowMessageBox(objLanguage.Alert_Message_49, 0, '');
            }
        }
    }
    else {
        MultipleTab.Redirect();
    }

}
function SuccessSaveBookedNowPayLater(result) {
    var obj;
    if (result.length > 0) {
        if (result == "{002}" || result == "{005}") {
            ShowMessageBox(objLanguage.Alert_Message_161, 0, 'loadHome');
        }
        else if (result == "{706}") {
            ShowMessageBox(objLanguage.Alert_Message_153, 0, "");
        }
        else if (result == "203{!}DUPLICATESEAT") {
            ShowMessageBox(objLanguage.Alert_Message_159, 0, "");
        }
        else if (result == "202{!}SESSIONTIMEOUT") {
            ShowMessageBox(objLanguage.Alert_Message_161, 0, "");
        }
        else if (result == "overdue payment") {
            //alert('Overdue payment, Please select other method of payment');
            SetPaymentContent();
            InitializePaymentWaterMark();
            ShowMessageBox(objLanguage.Alert_Message_135, 0, '');
        }
        else if (result == "paylimit null") {
            //alert('Please contact our call center for payment timelimit information');
            SetPaymentContent();
            InitializePaymentWaterMark();
            ShowMessageBox(objLanguage.Alert_Message_136, 0, '');
        }
        else {
            if (result.split("{}").length == 1) {
                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                // clear tab id
                MultipleTab.ClearID();

                // clear Insurance session
                SetShowInsurance("0");

                //Call google analytic.
                SubmitAnalyticPageThankYou('');

                //start wellnet
                if (result.split("<!--urlPopup :").length > 1) {
                    ShowLoadBar(false, false);
                    var urlPopup = result.split("<!--urlPopup :")[1].replace("-->", "");

                    ShowMessageBox(objLanguage.Alert_Message_140, 0, "wellnet|" + urlPopup);
                }
                else if (result.split("<!--WellnetFailed :").length > 1) {
                    ShowLoadBar(false, false);

                    ShowMessageBox(objLanguage.Alert_Message_163, 0, "");
                }
                else {
                    ShowLoadBar(false, false);
                }
                //end wellnet

                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");
                //Call google adword.
                CallAdword();



            }
            else {
                if (result.split("{}")[1] == "200") {
                    //Infant over limit
                    ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
                }
                else {
                    ShowMessageBox(result.split("{}")[1], 0, '');
                }
            }
            obj = null;

        }
    }
    else {
        ShowLoadBar(false, false);
    }

}
function ClearPaymentInput() {
    var objChkMyName = document.getElementById("chkMyname");
    var objNameOnCard = document.getElementById("txtNameOnCard");
    var objIssueNumber = document.getElementById("txtIssueNumber");
    var objIssueMonth = document.getElementById("optIssueMonth");
    var objIssueYear = document.getElementById("optIssueYear");
    var objCardNumber = document.getElementById("txtCardNumber");
    var objExpiredMonth = document.getElementById("optExpiredMonth");
    var objExpiredYear = document.getElementById("optExpiredYear");
    var objCvv = document.getElementById("txtCvv");
    var objAddress1 = document.getElementById("txtAddress1");
    var objAddress2 = document.getElementById("txtAddress2");
    var objCity = document.getElementById("txtCity");
    var objCounty = document.getElementById("txtCounty");
    var objPostCode = document.getElementById("txtPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optCountry"));


    objChkMyName.checked = false;
    objNameOnCard.value = "";
    objIssueNumber.value = "";
    objIssueMonth.selectedIndex = 0;
    objIssueYear.selectedIndex = 0;
    objCardNumber.value = "";
    objExpiredMonth.selectedIndex = 0;
    objExpiredYear.selectedIndex = 0;
    objCvv.value = "";
    objAddress1.value = "";
    objAddress2.value = "";
    objCity.value = "";
    objCounty.value = "";
    objPostCode.value = "";
    objCountry.selectedIndex = 0;

}

function validateCC(strCardNumber, bCalculate) {
    var objCardType = document.getElementById(FindControlName("select", "optCardType"));
    if (objCardType != null) {
        var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
        var objDvFareTotal = document.getElementById("dvTotalFareSummary");
        var objTotalAdditinalFee = document.getElementById("divAdditionalFeeTotal");
        var objCreditCardValue = document.getElementById("dvCreditCardValue");
        var objCreditCardDetail = document.getElementById("liCreditCardFare");
        var objhdFareTotal = document.getElementById("hdSubTotal");

        var dclFeePercentage = Math.round(parseFloat(objPaymentValue.split("{}")[13]));
        var minimumFeeFlag = parseInt(objPaymentValue.split("{}")[14]);
        var Factor = parseInt(objPaymentValue.split("{}")[15]);
        var dclFeeAmount = Math.round(parseFloat(objPaymentValue.split("{}")[16]));
        var dclFeeAmountIncl = Math.round(parseFloat(objPaymentValue.split("{}")[17]));


        //Find Credit Card Fee
        var dblCreditCardFee = CalculateCreditCardFee(dclFeeAmount,
                                                  dclFeeAmountIncl,
                                                  Math.round(parseFloat(0 + GetControlValue(objhdFareTotal))),
                                                  dclFeePercentage,
                                                  Factor,
                                                  minimumFeeFlag);


        var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;

        var objhdFareTotal = document.getElementById("hdSubTotal");
        var dclFeePercentage = Math.round(parseFloat(objPaymentValue.split("{}")[13]));
        var minimumFeeFlag = parseInt(objPaymentValue.split("{}")[14]);
        var Factor = parseInt(objPaymentValue.split("{}")[15]);
        var dclFeeAmount = Math.round(parseFloat(objPaymentValue.split("{}")[16]));
        var dclFeeAmountIncl = Math.round(parseFloat(objPaymentValue.split("{}")[17]));

        CalculateCCFee(strCardNumber,
                   bCalculate,
                   objCardType,
                   objPaymentValue,
                   objDvFareTotal,
                   objTotalAdditinalFee,
                   objCreditCardValue,
                   objCreditCardDetail,
                   dblCreditCardFee,
                   objhdFareTotal);

        ShowAdditionalFee();
    }
}


function ShowAdditionalFee() {
    var objAdditionalTotal = document.getElementById("divAdditionalFeeTotal");
    var objDvAdditionalFee = document.getElementById("dvAdditionalFee");

    if (objAdditionalTotal != null) {
        var dblAddtionTotal = parseFloat(0 + RemoveCommas(objAdditionalTotal.innerHTML));

        if (dblAddtionTotal > 0) {
            objDvAdditionalFee.style.display = "block";
            //Initialize Total Additonal collapse
            $(function () {
                $("#ulAdditionalFee").jqcollapse({
                    slide: true,
                    speed: 400,
                    easing: '',
                    hide: false
                });
            });
        }
        else {
            objDvAdditionalFee.style.display = "none";
        }
    }
}

function HideAllPaymentTab(bValue) {
    if (bValue == true) {
        document.getElementById("dvCCTab").style.display = "none";
        document.getElementById("dvVoucherTab").style.display = "none";

        //Form of payment section
        document.getElementById("CreditCard").style.display = "none";
        document.getElementById("Voucher").style.display = "none";
    }
    else {
        document.getElementById("dvCCTab").style.display = "block";
        document.getElementById("dvVoucherTab").style.display = "block";

        //Form of payment section
        document.getElementById("CreditCard").style.display = "block";
        document.getElementById("Voucher").style.display = "block";
    }
}
function SaveBookedRedeem() {
    var objCondition = document.getElementById("chkPayRedeem");
    if (objCondition.checked == true) {
        ShowLoadBar(true, false);
        var bookDate = new Date().format("yyyyMMddHHmmss");
        tikAeroB2C.WebService.B2cService.bookNowPaylater(true, bookDate, SuccessSaveBookedRedeem, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
    }
    objCondition = null;
}
function SuccessSaveBookedRedeem(result) {
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    obj = null;
    //If FFP Logon Reload point.
    var strClientProfileId = getCookie("coFFP");
    if (strClientProfileId != null) {
        LoadClientInformation(strClientProfileId);
    }
    ShowLoadBar(false, false);
}
//Start EquiPay payment
function SuccessProcessEquipayPayment(result) {
    var obj = document.getElementsByTagName("body");
    obj[0].innerHTML = result;
    document.payFormCcard.submit();

    //document.write(result);
}

function paymentEft() {
    var objNameOnCard = document.getElementById("txtElvAccountHolder");
    var objAccNumber = document.getElementById("txtElvAccountNumber");
    var objCountry = document.getElementById(FindControlName("select", "optEftCountry"));
    var objBankCode = document.getElementById("txtElvBankCode");
    var objBankName = document.getElementById("txtElvBankName");


    var bSuccess = 1;
    var errorMessage = "<div class='clear-all'></div>";

    if (objNameOnCard.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_82 + "<div class='clear-all'></div>"; //"- Please supply a valid Name.";
        bSuccess = 0;
    }
    if (objBankCode.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_83 + "<div class='clear-all'></div>"; //"- Please supply a valid Bank code.";
        bSuccess = 0;
    }
    if (objBankName.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_84 + "<div class='clear-all'></div>"; //"- Please supply a valid Bank name.";
        bSuccess = 0;
    }
    if (objAccNumber.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_85 + "<div class='clear-all'></div>"; //"- Please supply a valid Account Number.";
        bSuccess = 0;
    }

    if (bSuccess == 1) {
        var objCondition = document.getElementById("chkElvCondition");

        if (objCondition.checked == true) {
            ShowLoadBar(true, false);
            var xmlStr = "<payment>";

            xmlStr += "<form_of_payment_subtype_rcd>EFT</form_of_payment_subtype_rcd>";
            xmlStr += "<form_of_payment_rcd>EFT</form_of_payment_rcd>";
            xmlStr += "<cvv_required_flag>0</cvv_required_flag>";
            xmlStr += "<display_cvv_flag>0</display_cvv_flag>";
            xmlStr += "<display_expiry_date_flag>0</display_expiry_date_flag>";
            xmlStr += "<expiry_date_required_flag>0</expiry_date_required_flag>";
            xmlStr += "<address_required_flag>0</address_required_flag>";
            xmlStr += "<display_address_flag>1</display_address_flag>";
            xmlStr += "<display_issue_date_flag>0</display_issue_date_flag>";
            xmlStr += "<display_issue_number_flag>0</display_issue_number_flag>";

            xmlStr += "<NameOnCard>" + objNameOnCard.value + "</NameOnCard>";
            xmlStr += "<CreditCardNumber>" + objAccNumber.value + "</CreditCardNumber>";
            xmlStr += "<CCDisplayName></CCDisplayName>";

            xmlStr += "<IssueMonth></IssueMonth>";
            xmlStr += "<IssueYear></IssueYear>";
            xmlStr += "<IssueNumber></IssueNumber>";

            xmlStr += "<ExpiryMonth></ExpiryMonth>";
            xmlStr += "<ExpiryYear></ExpiryYear>";

            xmlStr += "<Addr1></Addr1>";
            xmlStr += "<Addr2></Addr2>";
            xmlStr += "<Street></Street>";
            xmlStr += "<State></State>";
            xmlStr += "<City></City>";
            xmlStr += "<County></County>";
            xmlStr += "<ZipCode></ZipCode>";
            xmlStr += "<Country>" + objCountry.options[objCountry.selectedIndex].value + "</Country>";
            xmlStr += "<CVV></CVV>";
            xmlStr += "<fee_amount_incl>0</fee_amount_incl>";
            xmlStr += "<fee_amount>0</fee_amount>";
            xmlStr += "<bank_name>" + objBankName.value + "</bank_name>";
            xmlStr += "<bank_code>" + objBankCode.value + "</bank_code>";
            xmlStr += "<bank_iban></bank_iban>";
            xmlStr += "</payment>";

            tikAeroB2C.WebService.B2cService.PaymentCreditCard(xmlStr, SuccessPaymentCreditCard, showErrorPayment, showTimeOutPayment);
            // tikAeroB2C.WebService.B2cService.EqipayPaymentRequest(xmlStr, SuccessProcessEquipayPayment, showErrorPayment, showTimeOutPayment);
            //tikAeroB2C.WebService.B2cService.JccPaymentRequest(xmlStr, SuccessProcessEquipayPayment, showErrorPayment, showTimeOutPayment);                 
        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //passengerdetail.ascx
        }

        objCondition = null;
    }
    else { document.getElementById("spnError").innerHTML = errorMessage; }
}

function SetPaymentContent() {

    //Show default first tab.
    showhidelayer('CreditCard');

    var objCC = document.getElementById("dvcTCreditCard");
    if (objCC != null && objCC.className == "CreditCard") {
        activateCreditCardControl();
    }

    //Check For Redeem payment
    //  if redeem with fee show form of payment
    //  if only redeem no payment show only redeem tab
    if (document.getElementById("dvPointTotal") != null &&
       parseFloat(0 + document.getElementById("dvFareTotal").innerHTML) > 0 &&
       parseInt(0 + document.getElementById("dvPointTotal").innerHTML) > 0) {
        HideAllPaymentTab(true);
        document.getElementById("dvRedeemTab").style.display = "none";
        document.getElementById("dvCCTab").style.display = "block";

        activateCreditCardControl();
        showhidelayer('CreditCard');
    }
    else if (document.getElementById("dvPointTotal") != null &&
             parseFloat(0 + document.getElementById("dvFareTotal").innerHTML) == 0 &&
             parseInt(0 + document.getElementById("dvPointTotal").innerHTML) > 0) {
        HideAllPaymentTab(true);
        document.getElementById("dvRedeemTab").style.display = "block";
        document.getElementById("dvReedeem").style.display = "block";
    }
    else {
        if (document.getElementById("dvRedeemTab") != null) {
            document.getElementById("dvRedeemTab").style.display = "none";
        }
    }


    //Check EFT Tab();
    ShowPaymentFfpInfo();
    //Clear Passenger title to show only MR ans MRS
    var objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    if (objTitle != null) {
        for (var i = 0; i < objTitle.options.length; i++) {
            if (objTitle.options[i].value != "MR|M" & objTitle.options[i].value != "MRS|F") {
                $("#" + FindControlName("select", "stContactTitle") + " option[value='" + objTitle.options[i].value + "']").remove();
                i = i - 1;
            }
        }
    }

    //Initialize Total Additonal collapse
    $(function () {
        $("#ulAdditionalFee").jqcollapse({
            slide: true,
            speed: 400,
            easing: ''
        });
    });

    scroll(0, 0);

    //Default Check Use contact address.
    var chkUsedContact = document.getElementById("chkMyname");
    if (chkUsedContact != null) {
        chkUsedContact.checked = true;
        LoadMyName();
    }

    //Set Expiry date
    FilledCCYear("optExpiredYear");
}

function AceInsuranceCheckPass() {
    var objInsuranceDetail = document.getElementById("dvInsuranceDetail");
    var objInsuranceRefresh = document.getElementById("dvInsuranceReferesh");

    if (objInsuranceDetail.style.display == "block" && objInsuranceRefresh.style.display == "none") {
        var objYes = document.getElementById("optGetInsurance");
        var objNo = document.getElementById("optNoInsurance");
        var objPurpose = document.getElementById("optPurpose");

        if (objYes.checked == true) {
            return "000"; //Pay insurance
        }
        else if (objYes.checked == true) {

            return "001"; //Please Accept tearm and condition.
        }
        else {
            return "002"; //Pay without insurance.
        }
    }
    else {
        return "002"; //Pay without insurance.
    }

}
function PaymentMultipleForm() {

    if (MultipleTab.IsCorrectTab()) {
        if (AceInsuranceCheckPass() == "000")
        { }
        else {
            var objChk = document.getElementById("chkPayVoucher");
            if (objChk.checked == true) {
                if (VoucherSelect() == true) {
                    var objCreditCard = document.getElementById("dvMultiFormCCPayment");
                    if (EnoughVoucher() == true || (objCreditCard != null && (objCreditCard.style.display == "block" || objCreditCard.style.display == ""))) {
                        var objVoucher = document.getElementsByName("nVoucher");
                        var objVoucherError = document.getElementById("spnVoucherError");


                        var strXml = "";
                        var strCCXml = "";
                        var arrVcValue;
                        var bCCPass = false;

                        //Check if credit card information is complete and successfully validate.
                        if (objCreditCard != null && (objCreditCard.style.display == "block" || objCreditCard.style.display == "")) {

                            var objNameOnCard = document.getElementById("txtMultiNameOnCard");
                            var objCardType = document.getElementById(FindControlName("select", "optMultiCardType"));
                            var objIssueNumber = document.getElementById("txtMultiIssueNumber");
                            var objIssueMonth = document.getElementById("optMultiIssueMonth");
                            var objIssueYear = document.getElementById("optMultiIssueYear");
                            var objCardNumber = document.getElementById("txtMultiCardNumber");
                            var objExpiredMonth = document.getElementById("optMultiExpiredMonth");
                            var objExpiredYear = document.getElementById("optMultiExpiredYear");
                            var objCvv = document.getElementById("txtMultiCvv");
                            var objAddress1 = document.getElementById("txtMultiAddress1");
                            var objAddress2 = document.getElementById("txtMultiAddress2");
                            var objCity = document.getElementById("txtMultiCity");
                            var objCounty = document.getElementById("txtMultiCounty");
                            var objPostCode = document.getElementById("txtMultiPostCode");
                            var objCountry = document.getElementById(FindControlName("select", "optMultiCountry"));
                            var objCondition = document.getElementById("chkMultiPayCreditCard");

                            strCCXml = ValidateCreditCard(objNameOnCard,
                                        objCardType,
                                        objIssueNumber,
                                        objIssueMonth,
                                        objIssueYear,
                                        objCardNumber,
                                        objExpiredMonth,
                                        objExpiredYear,
                                        objCvv,
                                        objAddress1,
                                        objAddress2,
                                        objCity,
                                        objCounty,
                                        objPostCode,
                                        objCountry,
                                        objCondition);

                            if (strCCXml.length > 0) {
                                bCCPass = true;
                            }
                        }
                        else {
                            bCCPass = true;
                        }

                        if (objVoucher != null && bCCPass == true) {
                            if (objVoucher.length > 0) {
                                strXml = "<payments>";
                                for (var iCount = 0; iCount < objVoucher.length; iCount++) {
                                    if (objVoucher[iCount].checked == true) {

                                        if (objVoucher[iCount].value.length > 0) {

                                            arrVcValue = objVoucher[iCount].value.split("|");
                                            strXml = strXml + "<payment>" +
                                                      "<voucher_id>" + arrVcValue[0] + "</voucher_id>" +
                                                      "<voucher_number>" + arrVcValue[1] + "</voucher_number>" +
                                                      "<form_of_payment_rcd>" + arrVcValue[2] + "</form_of_payment_rcd>" +
                                                      "<form_of_payment_subtype_rcd>" + arrVcValue[3] + "</form_of_payment_subtype_rcd>" +
                                                      "<display_name>" + arrVcValue[4] + "</display_name>" +
                                                      "<fee_amount_incl>" + arrVcValue[5] + "</fee_amount_incl>" +
                                                      "<fee_amount>" + arrVcValue[6] + "</fee_amount>" +
                                                  "</payment>";
                                        }

                                    }
                                }

                                //Credit Card xml.
                                strXml = strXml + strCCXml;

                                strXml = strXml + "</payments>";

                                CloseDialog();
                                ShowLoadBar(true, false);
                                //Process payment voucher.
                                tikAeroB2C.WebService.B2cService.PaymentMultipleForm(strXml, SuccessPaymentMultipleForm, showError, showTimeOut);
                            }
                            else {
                                objVoucherError.innerHTML = objLanguage.Alert_Message_76; //"- Please select voucher.";
                            }

                        }

                        objVoucherError = null;
                        objVoucher = null;
                    }
                    else {
                        //Voucher amount is not enough. Show credit card form.
                        LoadFormCreditCard();
                    }
                }
                else {
                    ShowMessageBox(objLanguage.Alert_Message_76, 0, '');
                }
            }
            else {
                //Please accept Terms and Conditions
                ShowMessageBox(objLanguage.Alert_Message_49, 0, '');
            }
        }

    }
    else {
        MultipleTab.Redirect();
    }


}
function SuccessPaymentMultipleForm(result) {

    if (result.length > 0) {
        if (result == "{002}" || result == "{005}") {
            ShowMessageBox(objLanguage.Alert_Message_161, 0, 'loadHome');
        }
        else if (result == "{703}") {
            ShowMessageBox(objLanguage.Alert_Message_37, 0, ''); //"Voucher amount not enough !"
            //Show Credit card section here.
        }
        else {
            if (result.split("{}").length == 1) {
                var obj = document.getElementById("dvContainer");

                ShowLoadBar(false, false);

                obj.innerHTML = result;

                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");

                // clear tab id
                MultipleTab.ClearID();

                // clear Insurance session
                SetShowInsurance("0");

                //Call google analytic.
                SubmitAnalyticPageThankYou(creditcard_subtype);
                //Call google adword.
                CallAdword();

            }
            else {

                if (result.split("{}")[1] == "200") {
                    //Infant over limit
                    ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
                }
                if (result.split("{}")[0] == "203") {
                    ShowMessageBox(objLanguage.Alert_Message_159, 0, '');
                }
                else {
                    ShowMessageBox(result.split("{}")[1], 0, '');
                }


            }
        }

        obj = null;
    }
    else {
        ShowLoadBar(false, false);
    }
}
function LoadFormCreditCard() {
    var objChk = document.getElementsByName("nVoucher");
    var strXml = "";

    ShowProgressBar(true);
    strXml += "<vouchers>";
    for (var i = 0; i < objChk.length; i++) {
        if (objChk[i].checked == true) {
            var arr = objChk[i].value.split("|");
            if (arr.length == 7) {
                strXml += "<voucher>";

                strXml += "<voucher_id>" + arr[0] + "</voucher_id>";
                strXml += "<voucher_number>" + arr[1] + "</voucher_number>";
                strXml += "<form_of_payment_rcd>" + arr[2] + "</form_of_payment_rcd>";
                strXml += "<form_of_payment_subtype_rcd>" + arr[3] + "</form_of_payment_subtype_rcd>";
                strXml += "<display_name>" + arr[4] + "</display_name>";
                strXml += "<fee_amount_incl>" + arr[5] + "</fee_amount_incl>";
                strXml += "<fee_amount>" + arr[6] + "</fee_amount>";

                strXml += "</voucher>";
            }
        }
    }
    strXml += "</vouchers>";
    tikAeroB2C.WebService.B2cService.LoadFormCreditCard(strXml, SuccessLoadFormCreditCard, showError, showTimeOut);
}
function SuccessLoadFormCreditCard(result) {

    ShowProgressBar(false);
    if (result.length > 0) {
        var objHolder = document.getElementById("dvFormHolder");
        var objContainer = document.getElementById("dvProgressBar");

        //Insert passenger form content.
        objHolder.innerHTML = result;

        //Show Fading.
        objContainer.style.display = "block";
        objHolder.style.display = "block";

        ClearErrorMsg();
        objHolder = null;
        objContainer = null;

        //Detect keyboard key for Credit Card number.
        $('#txtMultiCardNumber').bind("cut copy paste", function (e) {
            e.preventDefault();
        });

        $("#txtMultiCardNumber").keyup(function (e) {
            var objCardNumber = document.getElementById("txtMultiCardNumber");
            var objCreditCardFee = document.getElementById("dvMultiCCFee");
            var objliMultiCreditCardFee = document.getElementById("liMultiCreditCardFee");

            objCardNumber.value = FilterNonNumeric(objCardNumber.value);

            if (objCardNumber.value.length == 6) {
                if (objliMultiCreditCardFee != null)
                    objliMultiCreditCardFee.style.display = "block";

                if (e.keyCode == 8) {
                    ValidateMultiCC(objCardNumber.value, false);
                }
                else {
                    ValidateMultiCC(objCardNumber.value, true);

                }
            }
            else if (objCardNumber.value.length < 6 & e.keyCode == 8) {
                if (objliMultiCreditCardFee != null)
                    objliMultiCreditCardFee.style.display = "none";
                ValidateMultiCC(objCardNumber.value, false);
            }
        });

        $('#txtMultiNameOnCard').bind("cut copy paste", function (e) {
            e.preventDefault();
        });

        $('#txtMultiNameOnCard').keyup(function (e) {
            var objNameOnCard = document.getElementById("txtMultiNameOnCard");
            var charMatch = "";
            for (var i = 0; i < objNameOnCard.value.length; i++) {
                if (IsCharWithSpace(objNameOnCard.value.charAt(i))) {
                    charMatch += objNameOnCard.value.charAt(i);
                }
            }
            objNameOnCard.value = charMatch;
        });

        //Make voucher information collapse
        $(function () {
            $("#ulMultipleVcSummary").jqcollapse({
                slide: true,
                speed: 300,
                easing: ''
            });
        });

        //Show Credit card filed that base on card type.
        ActivateMultiCCControl();

        //Initialize CC WaterMark.
        InitializeMultiCCWaterMark();

        //Filter Credit card type by currency.
        FilterCardTypeList(true);

        //Set Expiry date
        FilledCCYear("optMultiExpiredYear");

        var objliMultiCreditCardFee = document.getElementById("liMultiCreditCardFee");
        if (objliMultiCreditCardFee != null)
            objliMultiCreditCardFee.style.display = "none";
    }
}
function InitializeMultiCCWaterMark() {

    //Credit Card
    InitializeWaterMark("txtMultiCardNumber", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiNameOnCard", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiCvv", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiAddress1", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiAddress2", objLanguage.default_value_3, "input");
    InitializeWaterMark("txtMultiCity", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiCounty", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiPostCode", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiIssueNumber", objLanguage.default_value_2, "input");
}
function CalculateTotalMultiplePayment() {
    var objMultiplePaymentMsg = document.getElementById("dvMultipleConfirm");
    if (EnoughVoucher() == false) {
        objMultiplePaymentMsg.style.display = "block";
    }
    else {
        objMultiplePaymentMsg.style.display = "none";
    }
    ShowVoucherFee(0);
}
function EnoughVoucher() {
    var objChk = document.getElementsByName("nVoucher");
    var objHdTotalVC = document.getElementsByName("hdVcAmount");
    var objHdSubTotal = document.getElementById("hdSubTotal");
    var dvCreditCardValue = document.getElementById("dvCreditCardValue");
    var voucherFee = 0;
    if (dvCreditCardValue != null)
        voucherFee = dvCreditCardValue.innerHTML;

    var dblTotal = 0;
    if (objChk != null && objChk.length > 0) {
        for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].checked == true) {
                dblTotal = dblTotal + parseFloat(objHdTotalVC[i].value);
            }
        }
    }
    if (dblTotal < parseFloat(objHdSubTotal.value) + parseFloat(voucherFee)) {
        return false;
    }
    else {
        return true;
    }
}
var creditcard_subtype = "";
function ValidateCreditCard(objNameOnCard,
                            objCardType,
                            objIssueNumber,
                            objIssueMonth,
                            objIssueYear,
                            objCardNumber,
                            objExpiredMonth,
                            objExpiredYear,
                            objCvv,
                            objAddress1,
                            objAddress2,
                            objCity,
                            objCounty,
                            objPostCode,
                            objCountry,
                            objCondition) {

    if (AceInsuranceCheckPass() == "001") {
        ShowMessageBox(objLanguage.Alert_Message_130, 0, ''); //Please accept term and condition
    }
    else if (AceInsuranceCheckPass() == "003") {
        ShowMessageBox(objLanguage.Alert_Message_131, 0, ''); //please select purpose of travel.
    }
    else {
        ccErrors[0] = objLanguage.Alert_Message_72; //"Unknown card type";
        ccErrors[1] = objLanguage.Alert_Message_73; //"No card number provided";
        ccErrors[2] = objLanguage.Alert_Message_39; //"Please supply a valid Credit Card Number.";
        ccErrors[3] = objLanguage.Alert_Message_74; //"Credit Card type mismatch.";
        ccErrors[4] = objLanguage.Alert_Message_74; //"Credit Card type mismatch.";

        var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;

        var expMonth = objExpiredMonth.options[objExpiredMonth.selectedIndex].value;
        var expYear = objExpiredYear.options[objExpiredYear.selectedIndex].value;

        // Insurance ID
        var hdInsuranceID = "";
        if (document.getElementById("hdProductID") != null)
            hdInsuranceID = document.getElementById("hdProductID").value;

        var bSuccess = 1;
        var errorMessage = "<div class='clear-all'></div>";
        var bCheckSum = false;
        var exchange_fee_amount = "";

        if (objPaymentValue.split("{}")[12] == "1")
        { bCheckSum = true; }

        if (expMonth.length == 1)
        { expMonth = '0' + expMonth; }

        if (GetSelectedOption(objCardType).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_72 + "<div class='clear-all'></div>"; //"- Please supply a valid Card type.";
            bSuccess = 0;
        }

        if (GetControlValue(objNameOnCard).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_38 + "<div class='clear-all'></div>"; //"- Please supply a valid Name On Card.";
            bSuccess = 0;
        }
        if (checkCreditCard(GetControlValue(objCardNumber), objPaymentValue.split("{}")[0], bCheckSum) == false) {
            errorMessage = errorMessage + "- " + ccErrors[ccErrorNo] + "<div class='clear-all'></div>";
            bSuccess = 0;
        }

        if ((expYear.length == 0 | expMonth.length == 0) & objPaymentValue.split("{}")[5] == 1) {
            errorMessage = errorMessage + objLanguage.Alert_Message_46 + "<div class='clear-all'></div>"; //"- Invalid Expired Date.";
            bSuccess = 0;
        }
        else {
            if (parseInt(expYear + expMonth) <= parseInt(today.getFullYear().toString() + leadingZero(today.getMonth()).toString())) {
                if (objPaymentValue.split("{}")[5] = 1) {
                    errorMessage = errorMessage + objLanguage.Alert_Message_46 + "<div class='clear-all'></div>"; //"- Invalid Expired Date.";
                    bSuccess = 0;
                }
            }
        }


        if (objPaymentValue.split("{}")[2] == 1 && GetControlValue(objCvv).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_45 + "<div class='clear-all'></div>"; //"- Invalid CCV code.";
            bSuccess = 0;
        }
        if (objPaymentValue.split("{}")[6] == 1) {
            if (GetControlValue(objAddress1).length == 0) {
                errorMessage = errorMessage + objLanguage.Alert_Message_41 + "<div class='clear-all'></div>"; //"- Please supply a valid Address 1.";
                bSuccess = 0;
            }
            if (GetControlValue(objCity).length == 0) {
                errorMessage = errorMessage + objLanguage.Alert_Message_42 + "<div class='clear-all'></div>"; //"- Please supply a valid Town/City.";
                bSuccess = 0;
            }
            if (GetControlValue(objCounty).length == 0) {
                errorMessage = errorMessage + objLanguage.Alert_Message_121 + "<div class='clear-all'></div>"; //"- Please supply a valid province.";
                bSuccess = 0;
            }
            if (GetControlValue(objPostCode).length == 0) {
                errorMessage = errorMessage + objLanguage.Alert_Message_43 + "<div class='clear-all'></div>"; //"- Please supply a valid Postal Code.";
                bSuccess = 0;
            }
        }

        if (document.getElementById("spRate") != null) {
            exchange_fee_amount = document.getElementById("spRate").innerHTML;
        }

        if (bSuccess == 1) {
            ShowLoadBar(true, false);

            //Passing Insurance information
            var objPurpose = document.getElementById("optPurpose");
            var strPurpose = GetSelectedOption(objPurpose);

            var todayDate = new Date();

            creditcard_subtype = objPaymentValue.split("{}")[0];

            var xmlStr = "<payment>";

            xmlStr += "<form_of_payment_subtype_rcd>" + objPaymentValue.split("{}")[0] + "</form_of_payment_subtype_rcd>";
            xmlStr += "<form_of_payment_rcd>" + objPaymentValue.split("{}")[1] + "</form_of_payment_rcd>";
            xmlStr += "<cvv_required_flag>" + objPaymentValue.split("{}")[2] + "</cvv_required_flag>";
            xmlStr += "<display_cvv_flag>" + objPaymentValue.split("{}")[3] + "</display_cvv_flag>";
            xmlStr += "<display_expiry_date_flag>" + objPaymentValue.split("{}")[4] + "</display_expiry_date_flag>";
            xmlStr += "<expiry_date_required_flag>" + objPaymentValue.split("{}")[5] + "</expiry_date_required_flag>";
            xmlStr += "<address_required_flag>" + objPaymentValue.split("{}")[6] + "</address_required_flag>";
            xmlStr += "<display_address_flag>" + objPaymentValue.split("{}")[7] + "</display_address_flag>";
            xmlStr += "<display_issue_date_flag>" + objPaymentValue.split("{}")[8] + "</display_issue_date_flag>";
            xmlStr += "<display_issue_number_flag>" + objPaymentValue.split("{}")[9] + "</display_issue_number_flag>";

            xmlStr += "<NameOnCard>" + ConvertToValidXmlData(GetControlValue(objNameOnCard)) + "</NameOnCard>";
            xmlStr += "<CreditCardNumber>" + GetControlValue(objCardNumber) + "</CreditCardNumber>";
            xmlStr += "<CCDisplayName>" + objCardType.options[objCardType.selectedIndex].Text + "</CCDisplayName>";

            xmlStr += "<IssueMonth>" + objIssueMonth.options[objIssueMonth.selectedIndex].value + "</IssueMonth>";
            xmlStr += "<IssueYear>" + objIssueYear.options[objIssueYear.selectedIndex].value + "</IssueYear>";
            xmlStr += "<IssueNumber>" + GetControlValue(objIssueNumber) + "</IssueNumber>";

            xmlStr += "<ExpiryMonth>" + objExpiredMonth.options[objExpiredMonth.selectedIndex].value + "</ExpiryMonth>";
            xmlStr += "<ExpiryYear>" + objExpiredYear.options[objExpiredYear.selectedIndex].value + "</ExpiryYear>";

            xmlStr += "<Addr1>" + ConvertToValidXmlData(GetControlValue(objAddress1)) + "</Addr1>";
            xmlStr += "<Addr2></Addr2>";
            xmlStr += "<Street>" + ConvertToValidXmlData(GetControlValue(objAddress2)) + "</Street>";
            xmlStr += "<State>" + ConvertToValidXmlData(GetControlValue(objCounty)) + "</State>";
            xmlStr += "<City>" + ConvertToValidXmlData(GetControlValue(objCity)) + "</City>";
            xmlStr += "<County>" + ConvertToValidXmlData(GetControlValue(objCounty)) + "</County>";
            xmlStr += "<ZipCode>" + ConvertToValidXmlData(GetControlValue(objPostCode)) + "</ZipCode>";
            xmlStr += "<Country>" + objCountry.options[objCountry.selectedIndex].value + "</Country>";
            xmlStr += "<CVV>" + ConvertToValidXmlData(GetControlValue(objCvv)) + "</CVV>";
            xmlStr += "<fee_amount_incl>" + objPaymentValue.split("{}")[10] + "</fee_amount_incl>";
            xmlStr += "<fee_amount>" + objPaymentValue.split("{}")[11] + "</fee_amount>";

            xmlStr += "<client_date_time>" + todayDate.getFullYear() + "-" + (todayDate.getMonth() + 1) + "-" + todayDate.getDate() + " " + todayDate.getHours() + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds() + "</client_date_time>";
            xmlStr += "<purpose_of_travel>" + strPurpose + "</purpose_of_travel>";

            xmlStr += "</payment>";

            return xmlStr;
        }
        else {
            ShowMessageBox(errorMessage, 0, '');
            return "";
        }
    }
    return "";
}

function CalculateCCFee(strCardNumber,
                        bCalculate,
                        objCardType,
                        objPaymentValue,
                        objDvFareTotal,
                        objTotalAdditinalFee,
                        objCreditCardValue,
                        objCreditCardDetail,
                        dblCreditCardFee,
                        objhdFareTotal) {

    //Display form of payment fee.
    var objMultiPercentageDisplay = document.getElementById("dvMultipleCCPercentage");
    var objCCCurrency = document.getElementById("dvFareCurrencyDisplay");
    var objCurrency_sign = document.getElementById("hdCurrencySign");

    if (objMultiPercentageDisplay == null) {
        var objPercentageDisplay = document.getElementById("dvCCPercentage");

        if (objPercentageDisplay != null) {
            objPercentageDisplay.innerHTML = GetControlInnerHtml(objCCCurrency) + " " + AddCommas(Math.round(dblCreditCardFee));
        }
    }

    if (objMultiPercentageDisplay != null) {
        objMultiPercentageDisplay.innerHTML = GetControlInnerHtml(objCCCurrency) + " " + AddCommas(Math.round(dblCreditCardFee));
    }

    if (bCalculate == true) {
        if (checkCreditCard(strCardNumber, objPaymentValue.split("{}")[0], false) == true) {
            if (objTotalAdditinalFee != null) {
                objTotalAdditinalFee.innerHTML = AddCommas((Math.round(parseFloat(0 + dblCreditCardFee))));
            }

            objCreditCardValue.innerHTML = AddCommas(Math.round(parseFloat(0 + dblCreditCardFee)));
            objDvFareTotal.innerHTML = objCurrency_sign.value + " " + AddCommas(parseFloat(GetControlValue(objhdFareTotal)) + Math.round(parseFloat(0 + dblCreditCardFee)));

            if (objCreditCardDetail != null) {
                if (dblCreditCardFee > 0) {
                    objCreditCardDetail.style.display = "block";
                }
                else {
                    objCreditCardDetail.style.display = "block";
                }
            }
        }
        else {

            if (objCreditCardDetail != null) {
                objCreditCardValue.innerHTML = 0;
                objCreditCardDetail.style.display = "block";
            }

            if (parseFloat(0 + GetControlValue(objhdFareTotal)) != parseFloat(0 + RemoveCommas(GetControlInnerHtml(objDvFareTotal)))) {
                if (objTotalAdditinalFee != null) {
                    objTotalAdditinalFee.innerHTML = AddCommas((parseFloat(0 + RemoveCommas(objTotalAdditinalFee.innerHTML)) - Math.round(parseFloat(0 + dblCreditCardFee))));
                }
                objDvFareTotal.innerHTML = objCurrency_sign.value + " " + AddCommas(Math.round(parseFloat(0 + GetControlValue(objhdFareTotal))));
            }
        }
    }
    else {

        if (objCreditCardDetail != null) {
            objCreditCardValue.innerHTML = 0;
            objCreditCardDetail.style.display = "block";
        }

        if (parseFloat(0 + GetControlValue(objhdFareTotal)) != parseFloat(0 + RemoveCommas(GetControlInnerHtml(objDvFareTotal)))) {
            if (objTotalAdditinalFee != null) {
                if ((parseFloat(0 + RemoveCommas(objTotalAdditinalFee.innerHTML)) - Math.round(parseFloat(0 + dblCreditCardFee))) < 0) {
                    objTotalAdditinalFee.innerHTML = AddCommas(parseFloat(0));
                }
                else
                    objTotalAdditinalFee.innerHTML = AddCommas((parseFloat(0 + RemoveCommas(objTotalAdditinalFee.innerHTML)) - Math.round(parseFloat(0 + dblCreditCardFee))));
            }
            objDvFareTotal.innerHTML = objCurrency_sign.value + " " + AddCommas(Math.round(parseFloat(0 + GetControlValue(objhdFareTotal))));
        }
        else {
            objDvFareTotal.innerHTML = objCurrency_sign.value + " " + objDvFareTotal.innerHTML;
        }
    }
}

function ShowYahooFee() {
    var objhdDIW = document.getElementById("hd_YW");
    var objYahooValue = document.getElementById("dvCreditCardValue");
    var objFeeDisplay = document.getElementById("dvFeeDisplay");
    var objDvFareTotal = document.getElementById("dvTotalFareSummary");
    var objCurrency_sign = document.getElementById("hdCurrencySign");
    var objhdFareTotal = document.getElementById("hdSubTotal");
    var objspfeeYahoo = document.getElementById("spfeeYahoo");
    var objYahooAdditionalFee = document.getElementById("dvAdditionalFee");
    var objCreditCardDetail = document.getElementById("liCreditCardFare");
    var objCCDisplay = document.getElementById("dvCCDisplay");

    if (objhdDIW != null) {
        var objDIW = objhdDIW.value;
        var feeAmount = AddCommas(Math.round(parseFloat(0 + objDIW.split(";")[1])));
        objYahooValue.innerHTML = feeAmount;
        objFeeDisplay.innerHTML = "- " + objDIW.split(";")[0];
        objFeeDisplay.style.display = "block";
        objCCDisplay.style.display = "none";

        var objTotalAdditinalFee = document.getElementById("divAdditionalFeeTotal");
        if (objTotalAdditinalFee != null) {
            objTotalAdditinalFee.innerHTML = feeAmount;
        }

        if (objCreditCardDetail != null) {
            objCreditCardDetail.style.display = "block";
        }

        objDvFareTotal.innerHTML = objCurrency_sign.value + " " + AddCommas(parseFloat(0 + GetControlValue(objhdFareTotal)) + parseFloat(0 + feeAmount));

        objYahooAdditionalFee.style.display = "block";
        //Initialize Total Additonal collapse
        $(function () {
            $("#ulAdditionalFee").jqcollapse({
                slide: true,
                speed: 400,
                easing: '',
                hide: false
            });
        });


        // add fee text
        objspfeeYahoo.innerHTML = objspfeeYahoo.innerHTML.replace("{fee_amount}", objCurrency_sign.value + " " + feeAmount);
    }
}

function ShowCreditCardField(objCardType,
                            objPaymentValue,
                            objCreditCardFare,
                            objFareTotal,
                            objExpiredYear,
                            optExpiredMonth,
                            objCvv,
                            objExpiryDate,
                            objDvAddress,
                            objDvIssueDate,
                            objIssueNumber,
                            objCardNumber) {

    var ccSplit = objPaymentValue.split("{}");
    if (ccSplit[3] == "1")
    { objCvv.style.display = 'block'; }
    else
    { objCvv.style.display = 'none'; }

    if (objPaymentValue.split("{}")[4] == "1")
    { objExpiryDate.style.display = 'block'; }
    else
    { objExpiryDate.style.display = 'none'; }

    if (objPaymentValue.split("{}")[7] == "1")
    { objDvAddress.style.display = 'block'; }
    else
    { objDvAddress.style.display = 'none'; }

    if (objPaymentValue.split("{}")[8] == "1")
    { objDvIssueDate.style.display = 'block'; }
    else
    { objDvIssueDate.style.display = 'none'; }

    if (objPaymentValue.split("{}")[9] == "1")
    { objIssueNumber.style.display = 'block'; }
    else
    { objIssueNumber.style.display = 'none'; }

    //Clear Card number value
    objCardNumber.value = objLanguage.default_value_2;

    //Set Expiry Year
    if (objExpiredYear.options.length == 0) {
        var y = today.getFullYear();
        var objOption;
        for (var i = 0; i < 20; i++) {
            objOption = new Option(y + i, y + i);
            objExpiredYear.options.add(objOption);
        }
    }
}
function ActivateMultiCCControl() {
    var objCardType = document.getElementById(FindControlName("select", "optMultiCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objCreditCardFare = document.getElementById("dvMultiAdditionalFee");
    var objFareTotal = document.getElementById("dvMultiFareTotal");
    var objExpiredYear = document.getElementById("optMultiExpiredYear");
    var optExpiredMonth = document.getElementById("optMultiExpiredMonth");

    var objCvv = document.getElementById('dvMultiCvv');
    var objExpiryDate = document.getElementById('dvMultiExpiryDate');
    var objDvAddress = document.getElementById('dvMultiAddress');
    var objDvIssueDate = document.getElementById('dvMultiIssuDate');
    var objIssueNumber = document.getElementById('dvMultiIssueNumber');
    var objCardNumber = document.getElementById("txtMultiCardNumber");

    //Clear Error message.    

    ShowCreditCardField(objCardType,
                        objPaymentValue,
                        objCreditCardFare,
                        objFareTotal,
                        objExpiredYear,
                        optExpiredMonth,
                        objCvv,
                        objExpiryDate,
                        objDvAddress,
                        objDvIssueDate,
                        objIssueNumber,
                        objCardNumber);
    ValidateMultiCC("", false);
}
function ValidateMultiCC(strCardNumber, bCalculate) {
    var objCardType = document.getElementById(FindControlName("select", "optMultiCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objDvFareTotal = document.getElementById("dvTotalAdditional");
    var objCreditCardValue = document.getElementById("dvMultiCreditCardValue");
    var dblCreditCardFee = Math.round(parseFloat(objPaymentValue.split("{}")[10]));
    var objhdFareTotal = document.getElementById("hdTotalAdditional");

    var dclFeePercentage = Math.round(parseFloat(objPaymentValue.split("{}")[13]));
    var minimumFeeFlag = parseInt(objPaymentValue.split("{}")[14]);
    var Factor = parseInt(objPaymentValue.split("{}")[15]);
    var dclFeeAmount = Math.round(parseFloat(objPaymentValue.split("{}")[16]));
    var dclFeeAmountIncl = Math.round(parseFloat(objPaymentValue.split("{}")[17]));


    //Find Credit Card Fee
    var dblCreditCardFee = CalculateCreditCardFee(dclFeeAmount,
                                                  dclFeeAmountIncl,
                                                  Math.round(parseFloat(0 + GetControlValue(objhdFareTotal))),
                                                  dclFeePercentage,
                                                  Factor,
                                                  minimumFeeFlag);

    CalculateCCFee(strCardNumber,
                    bCalculate,
                    objCardType,
                    objPaymentValue,
                    objDvFareTotal,
                    objCreditCardValue,
                    objCreditCardValue,
                    null,
                    dblCreditCardFee,
                    objhdFareTotal);

    CalculateMultiFee(bCalculate, dblCreditCardFee, objhdFareTotal, objDvFareTotal, dvMultiCreditCardValue);
}

function VoucherSelect() {
    var objChk = document.getElementsByName("nVoucher");

    if (objChk != null && objChk.length > 0) {
        for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].checked == true) {
                return true;
            }
        }
    }
    return false;
}
function LoadMultiMyName() {
    var objMyname = document.getElementById("chkMultiMyname");
    if (objMyname.checked == true) {
        tikAeroB2C.WebService.B2cService.GetMyName(SuccessLoadMultiMyName, showError, showTimeOut);
    }
    else {
        //If check bok not checked clear card holder information.
        SuccessLoadMultiMyName("");
    }
    objMyname = null;
}
function SuccessLoadMultiMyName(result) {
    var objNameOnCard = document.getElementById("txtMultiNameOnCard");
    var objAddress1 = document.getElementById("txtMultiAddress1");
    var objAddress2 = document.getElementById("txtMultiAddress2");
    var objCity = document.getElementById("txtMultiCity");
    var objCounty = document.getElementById("txtMultiCounty");
    var objPostCode = document.getElementById("txtMultiPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optMultiCountry"));

    SetMyName(result,
              objNameOnCard,
              objAddress1,
              objAddress2,
              objCity,
              objCounty,
              objPostCode,
              objCountry);
}
function CalculateCreditCardFee(feeAmount,
                                feeAmountIncl,
                                saleAmount,
                                feePercentage,
                                Factor,
                                minimumFeeFlag) {

    var dclAmount;
    var dclAmountIncl;
    //Calculate FormOfPaymentFee
    if (feePercentage > 0) {

        var dFee = parseFloat((saleAmount / 100) * feePercentage).toFixed(2);
        if (feeAmountIncl == 0)
        { }
        else if (minimumFeeFlag == 0) {
            if (parseFloat(dFee) > parseFloat(feeAmountIncl)) {
                dFee = feeAmountIncl;
            }
        }
        else {
            if (parseFloat(dFee) < parseFloat(feeAmountIncl)) {
                dFee = feeAmountIncl;
            }
        }
        dclAmount = dFee;
        dclAmountIncl = dFee;
    }
    else {
        dclAmount = feeAmount * Factor;
        dclAmountIncl = feeAmountIncl * Factor;
    }

    return dclAmountIncl;
}
function HideAllPayment() {
    document.getElementById('CreditCard').style.display = 'none';
    document.getElementById('Voucher').style.display = 'none';
    document.getElementById('BookNow').style.display = 'none';

    document.getElementById('dvPaymentBack').style.display = 'block';
}
function GetWellNetFeeDisplay() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.CalculateFee(gWellNetFeeCode, true, SuccessGetWellNetFeeDisplay, showErrorPayment, showTimeOutPayment);
}
function SuccessGetWellNetFeeDisplay(result) {

    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {
            //Calculate payment fee failed due to session timeout.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            SuccessGetSessionQuoteSummary(result);

            var hdFee = document.getElementById("inWellFee");
            var spnFee = document.getElementById("spnWellFee");

            if (hdFee != null && spnFee != null) {
                spnFee.innerHTML = Math.round(parseFloat(hdFee.value));
            }
            if (document.getElementById("ctl00_optCardType") != null) {
                //Reset Credit card Fee display.
                //validateCC("", false);
                //Filter Credit card type by currency.
                FilterCardTypeList(false);
            }

            if (document.getElementById("dvDIWButton") != null && document.getElementById("dvDIWButton").style.display == "block")
                ShowYahooFee();
        }
    }
    ShowProgressBar(false);
}
function RemoveWellNetFeeDisplay() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ClearFeeFromCode(gWellNetFeeCode, true, SuccessGetWellNetFeeDisplay, showErrorPayment, showTimeOutPayment);
}

function ClearAdditionalFee() {

    var objDvFareTotal = document.getElementById("dvFareTotal");
    var objTotalAdditinalFee = document.getElementById("divAdditionalFeeTotal");
    var objhdFareTotal = document.getElementById("hdSubTotal");

    if (parseFloat(0 + GetControlValue(objhdFareTotal)) != parseFloat(0 + RemoveCommas(GetControlInnerHtml(objDvFareTotal)))) {
        if (objTotalAdditinalFee != null) {
            objTotalAdditinalFee.innerHTML = 0;
        }
        if (objDvFareTotal != null)
            objDvFareTotal.innerHTML = AddCommas(Math.round(parseFloat(0 + GetControlValue(objhdFareTotal))).toString());
    }
}
function FilledCCYear(strCtrl) {

    var todayDate = new Date();
    var iCurrentYear = todayDate.getFullYear();
    var objExpYear = document.getElementById(strCtrl);
    for (var i = 0; i < 14; i++) {
        var opt = document.createElement("option");

        opt.value = iCurrentYear;
        if (navigator.appName.indexOf('Microsoft') == 0) {
            opt.innerText = iCurrentYear;
        }
        else {
            opt.text = iCurrentYear;
        }
        objExpYear.appendChild(opt);

        //Increment Year.
        iCurrentYear = iCurrentYear + 1;
    }
    SetComboValue(strCtrl, todayDate.getFullYear());
}

function FilterCardTypeList(bMultiple) {
    var se = null;
    var dvCardTypeLogoNonJPY = null;
    var dvCardTypeJPY = null;

    var strCurrency = GetControlValue(document.getElementById("hdCurrencyCode"));

    var strCardType = "";
    if (bMultiple == false) {
        se = document.getElementById(FindControlName("select", "optCardType"));
        dvCardTypeLogoNonJPY = document.getElementById("dvCardTypeLogoNonJPY");
        dvCardTypeJPY = document.getElementById("dvCardTypeLogoJPY");
    }
    else {
        se = document.getElementById(FindControlName("select", "optMultiCardType"));
        dvCardTypeLogoNonJPY = document.getElementById("dvMultiCardTypeLogoNonJPY");
        dvCardTypeJPY = document.getElementById("dvMultiCardTypeLogoJPY");
    }
    if (se != null) {
        //Show hide cc logo.
        dvCardTypeLogoNonJPY.style.display = "none";
        dvCardTypeJPY.style.display = "block";
    }
}

function AddCCKeyEvent() {
    $('#txtCardNumber').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $("#txtCardNumber").keyup(function (e) {
        var objCardNumber = document.getElementById("txtCardNumber");
        var objCreditCardValue = document.getElementById("dvCreditCardValue");

        objCardNumber.value = FilterNonNumeric(objCardNumber.value);

        if (objCardNumber.value.length == 6) {
            if (e.keyCode == 8) {

                validateCC(objCardNumber.value, false);
                objCreditCardValue.innerHTML = 0;

            }
            else {
                validateCC(objCardNumber.value, true);

            }
        }
        else if (objCardNumber.value.length < 6 & e.keyCode == 8) {

            validateCC(objCardNumber.value, false);
            objCreditCardValue.innerHTML = 0;
        }
    });

    $('#txtNameOnCard').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $('#txtNameOnCard').keyup(function (e) {
        var objNameOnCard = document.getElementById("txtNameOnCard");
        var charMatch = "";
        for (var i = 0; i < objNameOnCard.value.length; i++) {
            if (IsCharWithSpace(objNameOnCard.value.charAt(i))) {
                charMatch += objNameOnCard.value.charAt(i);
            }
        }
        objNameOnCard.value = charMatch;
    });
}

function CallAdword() {
    var objAdWardGoogle = document.getElementById("ifmAdWord_google");
    var objAdWardYahoo = document.getElementById("ifmAdWord_yahoo");
    var strUrl = GetVirtualDirectory();

    objAdWardGoogle.src = strUrl + "html/AdWord_google.htm";
    objAdWardYahoo.src = strUrl + "html/AdWord_yahoo.htm";
}
function InitializePaymentWaterMark() {

    //Credit Card
    InitializeWaterMark("txtCardNumber", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtNameOnCard", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtCvv", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtAddress1", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtAddress2", objLanguage.default_value_3, "input");
    InitializeWaterMark("txtCity", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtCounty", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtPostCode", objLanguage.default_value_2, "input");

    if (document.getElementById("optExpiredMonth") != null) {
        document.getElementById("optExpiredMonth").selectedIndex = 0;
    }

    InitializeWaterMark("ctl00_optCardType", objLanguage.default_value_2, "select");
    InitializeWaterMark("optExpiredMonth", objLanguage.default_value_5, "select");
    InitializeWaterMark("optExpiredYear", objLanguage.default_value_6, "select");

    //Voucher
    InitializeWaterMark("txtVoucherNumber", objLanguage.default_value_2, "input");
    //InitializeWaterMark("txtVoucherPassword", objLanguage.default_value_2, "input");
}
function ShowPaymentFfpInfo() {
    //var objRedMenuFlyer = document.getElementById("dvRedMenu")
    //document.getElementById("dvFfpPointSummary").style.display = objRedMenuFlyer.style.display;
    //objRedMenuFlyer = null;
    if (document.getElementById("dvPointTotal") != null) {
        var dblCurrentPoint = parseFloat(document.getElementById("dvCurrentPoint").innerHTML.replace(/\,/g, ''));
        var dblRedeemPoint = parseFloat(document.getElementById("dvPointTotal").innerHTML);

        document.getElementById("dvFfpRedeemSummary").style.display = "block";
        document.getElementById("dvRedeemClientId").innerHTML = document.getElementById("hdClientNumber").value;
        document.getElementById("dvRedeemCurrentPoint").innerHTML = dblCurrentPoint;
        document.getElementById("dvRedeemPoint").innerHTML = dblRedeemPoint;
        document.getElementById("dvRedeemBalance").innerHTML = dblCurrentPoint - dblRedeemPoint;

        //Check FFP Balance if it is enough.
        if (dblCurrentPoint >= dblRedeemPoint)
        { }
        else {
            document.getElementById("btmPaymentCC").style.display = "none";
            document.getElementById("chkPayRedeem").style.display = "none";
        }
    }

}
function SuccessSavePassengerDetail(result) {

    if (result.length > 0) {
        if (result != "{000}") {

            if (result == "{002}") {
                ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
            }
            else if (result == "{004}") {

                //System Error. Please try again.
                ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
            }
            else if (result == "{404}") {

                //Invalid Date of birth.
                ShowMessageBox("Residue content error after web validation process", 0, '');
            }
            else if (result == "{405}") {

                //Invalid Passenger name.
                ShowMessageBox("Residue content error after web validation process", 0, '');
            }
            else {
                var obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                //google analytic
                if (UseGoogleManager() == true) {
                    dataLayer.push({ 'event': 'PaymentPage' });
                }
                else {
                    if (_gaq) {
                        _gaq.push(['_trackPageview', '/booking/payment-page.html']);
                    }
                }



                // set time out for delay call ins.
                setTimeout(function () {
                    SetPaymentContent();
                }, 100);

                //Set payment water mark.
                InitializePaymentWaterMark();

                ShowProgressBar(false);

                try {
                    //Call ACE insurance quote.
                    ACEInsuranceRequestQuote();
                }
                catch (e) {
                }

                obj = null;

                //Detect keyboard key for Credit Card number.
                AddCCKeyEvent();

            }


            ToolTipColor();
        }
        else {
            LoadSecure(true);
        }
    }
}

function SaveYahooPayment() {
    var objCondition = document.getElementById("chkDIW");

    if (objCondition.checked == true) {
        ShowLoadBar(true, false);

        var objAdditionalTotal = document.getElementById("divAdditionalFeeTotal");
        var objNameOnCard = document.getElementById("txtNameOnCard");
        var dblAddtionTotal = 0;
        if (objAdditionalTotal != null) {
            dblAddtionTotal = parseFloat(0 + RemoveCommas(objAdditionalTotal.innerHTML));
        }

        var xmlStr = "<payment>";
        xmlStr += "<form_of_payment_subtype_rcd>" + "YW" + "</form_of_payment_subtype_rcd>";
        xmlStr += "<form_of_payment_rcd>" + "DIW" + "</form_of_payment_rcd>";
        xmlStr += "<cvv_required_flag>" + "0" + "</cvv_required_flag>";
        xmlStr += "<display_cvv_flag>" + "0" + "</display_cvv_flag>";
        xmlStr += "<display_expiry_date_flag>" + "0" + "</display_expiry_date_flag>";
        xmlStr += "<expiry_date_required_flag>" + "0" + "</expiry_date_required_flag>";
        xmlStr += "<address_required_flag>" + "0" + "</address_required_flag>";
        xmlStr += "<display_address_flag>" + "0" + "</display_address_flag>";
        xmlStr += "<display_issue_date_flag>" + "0" + "</display_issue_date_flag>";
        xmlStr += "<display_issue_number_flag>" + "0" + "</display_issue_number_flag>";

        xmlStr += "<NameOnCard>" + ConvertToValidXmlData(GetControlValue(objNameOnCard)) + "</NameOnCard>";
        xmlStr += "<CreditCardNumber>" + "00000000" + "</CreditCardNumber>";

        xmlStr += "<fee_amount_incl>" + "0" + "</fee_amount_incl>";
        xmlStr += "<fee_amount_incl_gateway>" + dblAddtionTotal + "</fee_amount_incl_gateway>";
        xmlStr += "<fee_amount>" + "0" + "</fee_amount>";
        xmlStr += "</payment>";
        tikAeroB2C.WebService.B2cService.YahooPaymentRequest(xmlStr, SuccessYahooPaymentRequest, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowMessageBox(objLanguage.Alert_Message_49, 0, '');
    }
}

function SuccessYahooPaymentRequest(result) {
    if (result != "") {
        var error = result.split(",");
        if (error.length > 1)
            ShowMessageBox(objLanguage.Alert_Message_165, 0, '');
        else {
            var obj = document.getElementsByTagName("body");
            obj[0].innerHTML = result;
            document.payFormCcard.submit();
        }
    }
}

function ShowVoucherFee(ccFee) {
    var objVoucher = document.getElementsByName("nVoucher");
    var voucherFee = 0;
    var voucherFeeText = 0;
    var objtxtVoucherNumber = document.getElementById("txtVoucherNumber");
    var voucherDisplayFee = "";
    if (objVoucher != null) {
        if (objVoucher.length > 0) {
            for (var iCount = 0; iCount < objVoucher.length; iCount++) {
                arrVcValue = objVoucher[iCount].value.split("|");
                if (objVoucher[iCount].checked == true) {
                    if (objVoucher[iCount].value.length > 0) {
                        if (parseFloat(0 + voucherFee) <= parseFloat(0 + arrVcValue[5])) {
                            voucherFee = arrVcValue[5];
                            voucherDisplayFee = arrVcValue[4];
                        }
                    }

                }

                if (arrVcValue[1] == objtxtVoucherNumber.value)
                    voucherFeeText = arrVcValue[5];
            }
        }
    }

    var objYahooValue = document.getElementById("dvCreditCardValue");
    var objFeeDisplay = document.getElementById("dvFeeDisplay");
    var objDvFareTotal = document.getElementById("dvTotalFareSummary");
    var objCurrency_sign = document.getElementById("hdCurrencySign");
    var objhdFareTotal = document.getElementById("hdSubTotal");
    var objYahooAdditionalFee = document.getElementById("dvAdditionalFee");
    var objCreditCardDetail = document.getElementById("liCreditCardFare");
    var objCCDisplay = document.getElementById("dvCCDisplay");
    var objDvVoucherFee = document.getElementById("dvVoucherFee");
    var objhdFeeText = document.getElementById("hdFeeText");
    var feeAmount = AddCommas(Math.round(parseFloat(0 + voucherFee)));

    if (objDvVoucherFee != null) {
        objDvVoucherFee.innerHTML = objhdFeeText.value.replace("{fee_amount}", AddCommas(parseFloat(0 + voucherFee)));
        objDvVoucherFee.style.display = "block";
    }

    if (voucherFee > 0) {
        objYahooValue.innerHTML = feeAmount;
        objFeeDisplay.innerHTML = "- " + voucherDisplayFee;
        objFeeDisplay.style.display = "block";
        objCCDisplay.style.display = "none";

        var objTotalAdditinalFee = document.getElementById("divAdditionalFeeTotal");
        if (objTotalAdditinalFee != null) {
            objTotalAdditinalFee.innerHTML = feeAmount;
        }

        if (objCreditCardDetail != null) {
            objCreditCardDetail.style.display = "block";
        }

        objYahooAdditionalFee.style.display = "block";
        //Initialize Total Additonal collapse
        $(function () {
            $("#ulAdditionalFee").jqcollapse({
                slide: true,
                speed: 400,
                easing: '',
                hide: false
            });
        });
    }
    else {
        objFeeDisplay.style.display = "none";
        if (objCreditCardDetail != null) {
            objCreditCardDetail.style.display = "none";
        }
        objYahooAdditionalFee.style.display = "none";
    }

    objDvFareTotal.innerHTML = objCurrency_sign.value + " " + AddCommas(parseFloat(0 + GetControlValue(objhdFareTotal)) + parseFloat(0 + voucherFee));
}

function CalculateMultiFee(bCalculate, dclFeeAmountIncl, objhdFareTotal, objDvFareTotal, dvMultiCreditCardValue) {
    var objDvMultiTotalAmount = document.getElementById("dvMultiTotalAmount");
    var objDvFeeDisplay = document.getElementById("dvFeeDisplay");
    var objDvCCDisplay = document.getElementById("dvCCDisplay");
    var objVoucher = document.getElementsByName("nVoucher");
    var hdMultiTotalAmount = document.getElementById("hdMultiTotalAmount");
    var objCurrency_sign = document.getElementById("hdCurrencySign");

    var voucherFee = 0;
    var voucherDisplayFee = "";
    var fee = 0;
    if (objVoucher != null) {
        if (objVoucher.length > 0) {
            for (var iCount = 0; iCount < objVoucher.length; iCount++) {
                if (objVoucher[iCount].checked == true) {
                    if (objVoucher[iCount].value.length > 0) {
                        arrVcValue = objVoucher[iCount].value.split("|");
                        if (parseFloat(0 + voucherFee) <= parseFloat(0 + arrVcValue[5])) {
                            voucherFee = arrVcValue[5];
                            voucherDisplayFee = arrVcValue[4];
                        }
                    }

                }
            }
        }
    }

    if (voucherFee > dclFeeAmountIncl) {
        objDvFeeDisplay.innerHTML = voucherDisplayFee;
        dvMultiCreditCardValue.innerHTML = AddCommas(parseFloat(0 + voucherFee));

        objDvFeeDisplay.style.display = 'block';
        objDvCCDisplay.style.display = 'none';
        fee = voucherFee;
    }
    else {
        if (bCalculate) {
            objDvCCDisplay.style.display = 'block';
            objDvFeeDisplay.style.display = 'none';
            fee = dclFeeAmountIncl;
        }
    }

    if (bCalculate) {
        objDvMultiTotalAmount.innerHTML = AddCommas(parseFloat(0 + RemoveCommas(hdMultiTotalAmount.value)) + parseFloat(0 + fee));
        objDvFareTotal.innerHTML = objCurrency_sign.value + " " + AddCommas(parseFloat(GetControlValue(objhdFareTotal)) + Math.round(parseFloat(0 + fee)));
    }
}

function HideVoucherDetail() {
    var objVoucher = $("#dvVoucherDetail")[0];
    var objDvPayVoucher = $("#dvPayVoucher")[0];
    var txtVoucherNumber = $("#txtVoucherNumber")[0];
    var txtVoucherPassword = $("#txtVoucherPassword")[0];
    var dvVoucherFee = $("#dvVoucherFee")[0];
    var objMultiplePaymentMsg = $("#dvMultipleConfirm")[0];

    if (objVoucher != null)
        objVoucher.innerHTML = "";
    if (objDvPayVoucher != null)
        objDvPayVoucher.style.display = "none";
    if (dvVoucherFee != null)
        dvVoucherFee.style.display = "none";
    if (txtVoucherNumber != null)
        txtVoucherNumber.value = "";
    if (txtVoucherPassword != null)
        txtVoucherPassword.value = "";
    if (objMultiplePaymentMsg != null)
        objMultiplePaymentMsg.style.display = "none";

    InitializePaymentWaterMark();

    tikAeroB2C.WebService.B2cService.ClearVoucher(null, showErrorPayment, showTimeOutPayment);

}
function GetQuoteSummary(objChk, strType) {
    var objOutwards = document.getElementsByName("Outward");
    var objReturns = document.getElementsByName("Return");
    var lenOutward = objOutwards.length;
    var lenReturn = objReturns.length;
    var objClassName = "";

    var sOutward = "";
    for (var i = 0; i < lenOutward; i++) {
        if (objOutwards[i] != null) {
            //alert("objOutwards :" + objOutwards[i].checked);
            sOutward = objOutwards[i].id;
            if (objOutwards[i].checked == true) {
                document.getElementById(sOutward.substring(3)).className = "SearchFlightSelection";
            }
            else {
                objClassName = "BodyCOL" + (5 + parseInt(sOutward.substring(sOutward.length - 1)));
                document.getElementById(sOutward.substring(3)).className = objClassName;
            }
        }
    }

    var sReturn = "";
    for (var i = 0; i < lenReturn; i++) {
        if (objReturns[i] != null) {
            //alert("objReturns :" + objReturns[i].checked);
            sReturn = objReturns[i].id;
            if (objReturns[i].checked == true) {
                document.getElementById(sReturn.substring(3)).className = "SearchFlightSelection";
            }
            else {
                objClassName = "BodyCOL" + (5 + parseInt(sReturn.substring(sReturn.length - 1)));
                document.getElementById(sReturn.substring(3)).className = objClassName;
            }
        }
    }
    var strXml = "";
    var strParam = "";
    var strAirlineRcd = "";
    var strFlightNumber = "";
    var strFlightId = "";
    var strOrigin = "";
    var strDest = "";
    var strFareId = "";
    var strFlightConnectId = "";
    var dtDepartture = "";
    var dtArrival = "";
    var departTime = "";
    var arrivalTime = "";

    var strTransitAirlineRcd = "";
    var strTransitFlightNumber = "";
    var dtTransitDepartureDate = "";
    var dtTransitArrivalDate = "";
    var transitDepartTime = "";
    var transitArrivalTime = "";
    var strTransitAirport;
    var strTransitFareId = "";
    var strBookingClassRcd = "";
    var currency_rcd = "";

    var arrFlightInfo;
    var arrFlightDetail;

    if (objChk.checked == true) {
        strParam = objChk.value;
        if (strParam.length > 0) {
            arrFlightInfo = strParam.split("|");
            for (var i = 0; i < arrFlightInfo.length; i++) {
                arrFlightDetail = arrFlightInfo[i].split(":");
                if (arrFlightDetail[0] == "flight_id") {
                    strFlightId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "airline_rcd") {
                    strAirlineRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "flight_number") {
                    strFlightNumber = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "origin_rcd") {
                    strOrigin = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "destination_rcd") {
                    strDest = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "fare_id") {
                    strFareId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_flight_id") {
                    strFlightConnectId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "departure_date") {
                    dtDepartture = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "planned_departure_time") {
                    departTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "planned_arrival_time") {
                    arrivalTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_airline_rcd") {
                    strTransitAirlineRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_flight_number") {
                    strTransitFlightNumber = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_departure_date") {
                    dtTransitDepartureDate = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_planned_departure_time") {
                    transitDepartTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_planned_arrival_time") {
                    transitArrivalTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_airport_rcd") {
                    strTransitAirport = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_fare_id") {
                    strTransitFareId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "booking_class_rcd") {
                    strBookingClassRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "arrival_date") {
                    dtArrival = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_arrival_date") {
                    dtTransitArrivalDate = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "currency_rcd") {
                    currency_rcd = arrFlightDetail[1];
                }
            }
            if (strOrigin.length > 0) {
                strXml = "<flights>" +
                            "<flight>" +
                                "<flight_id>" + strFlightId + "</flight_id>" +
                                "<airline_rcd>" + strAirlineRcd + "</airline_rcd>" +
                                "<flight_number>" + strFlightNumber + "</flight_number>" +
                                "<origin_rcd>" + strOrigin + "</origin_rcd>" +
                                "<destination_rcd>" + strDest + "</destination_rcd>" +
                                "<fare_id>" + strFareId + "</fare_id>" +
                                "<transit_airline_rcd>" + strTransitAirlineRcd + "</transit_airline_rcd>" +
                                "<transit_flight_number>" + strTransitFlightNumber + "</transit_flight_number>" +
                                "<transit_flight_id>" + strFlightConnectId + "</transit_flight_id>" +
                                "<departure_date>" + dtDepartture + "</departure_date>" +
                                "<arrival_date>" + dtArrival + "</arrival_date>" +
                                "<arrival_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtArrival)) + "</arrival_day>" +
                                "<departure_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtDepartture)) + "</departure_day>" +
                                "<planned_departure_time>" + departTime + "</planned_departure_time>" +
                                "<planned_arrival_time>" + arrivalTime + "</planned_arrival_time>" +
                                "<transit_departure_date>" + dtTransitDepartureDate + "</transit_departure_date>" +
                                "<transit_departure_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtTransitDepartureDate)) + "</transit_departure_day>" +
                                "<transit_arrival_date>" + dtTransitArrivalDate + "</transit_arrival_date>" +
                                "<transit_arrival_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtTransitArrivalDate)) + "</transit_arrival_day>" +
                                "<transit_planned_departure_time>" + transitDepartTime + "</transit_planned_departure_time>" +
                                "<transit_planned_arrival_time>" + transitArrivalTime + "</transit_planned_arrival_time>" +
                                "<transit_airport_rcd>" + strTransitAirport + "</transit_airport_rcd>" +
                                "<transit_fare_id>" + strTransitFareId + "</transit_fare_id>" +
                                "<booking_class_rcd>" + strBookingClassRcd + "</booking_class_rcd>" +
                                "<currency_rcd>" + currency_rcd + "</currency_rcd>" +
                            "</flight>" +
                        "</flights>";
                //Call Webservice
                tikAeroB2C.WebService.B2cService.GetQuoteSummary(strXml,
                                                                 strType,
                                                                 SuccessGetQuoteSummary,
                                                                 showError,
                                                                 strType);
            }
        }
    }

}
function SuccessGetQuoteSummary(result, strType) {

    if (result == "{100}" || result == "{101}" || result == "{004}" || result == "{104}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {

        if (strType == "Outward") {
            DisplayQuoteSummary("", result, "");
        }
        else {
            DisplayQuoteSummary("", "", result);
        }
        //Initialize Collape
        InitializeFareSummaryCollapse(strType);
    }

}
function DisplayQuoteSummary(strFareSummaryHtml, strOutwardHtml, strReturnHtml) {

    var objDvFareSummary = document.getElementById("dvFareSummary");
    var objDvAvaiFareSummary = document.getElementById("dvAvaiQuote");
    var objOFareLine = document.getElementById("dvFareLine_Outward");
    var objRFareLine = document.getElementById("dvFareLine_Return");
    var objSummary = document.getElementById("dvYourSelection");

    if (strFareSummaryHtml.length == 0 & strOutwardHtml.length == 0 & strReturnHtml.length == 0) {

        objSummary.style.display = "none";
        if (objOFareLine != null) {
            objOFareLine.innerHTML = "";
        }
        if (objRFareLine != null) {
            objRFareLine.innerHTML = "";
        }
        if (objDvFareSummary != null) {
            objDvFareSummary.innerHTML = "";
        }
    }
    else {

        objSummary.style.display = "block";

        if (strFareSummaryHtml.length > 0) {
            if (objDvFareSummary != null) {
                objDvFareSummary.style.display = "block";
                objDvFareSummary.innerHTML = strFareSummaryHtml;
            }
        }
        else {
            if (objDvFareSummary != null) {
                objDvFareSummary.style.display = "none";
                objDvFareSummary.innerHTML = "";
            }
        }

        if (objOFareLine != null && objRFareLine != null) {
            if (strOutwardHtml.length > 0 | strReturnHtml.length > 0) {
                if (objDvAvaiFareSummary != null) {
                    objDvAvaiFareSummary.style.display = "block";

                    if (strOutwardHtml.length > 0) {
                        objOFareLine.innerHTML = strOutwardHtml;
                    }

                    if (strReturnHtml.length > 0) {
                        objRFareLine.innerHTML = strReturnHtml;
                    }
                }
            }
            else {
                if (objDvAvaiFareSummary != null) {
                    objDvAvaiFareSummary.style.display = "none";

                    objOFareLine.innerHTML = "";
                    objRFareLine.innerHTML = "";
                }
            }
        }
        //Calculate totoal summary.
        CalculateTotalFare();
    }
}
function CalculateTotalFare() {
    var objhdTotal = document.getElementsByName("hdSubTotal");
    var objDvTotal = document.getElementById("dvTotalFareSummary");
    var objDdSign = document.getElementById("hdCurrencySign");

    var dclTotal = 0;
    if (objhdTotal != null) {
        for (var i = 0; i < objhdTotal.length; i++) {
            dclTotal = dclTotal + parseFloat("0" + GetControlValue(objhdTotal[i]));
        }
    }
    if (objDvTotal != null && objDdSign != null) {
        objDvTotal.innerHTML = GetControlValue(objDdSign) + " " + AddCommas(dclTotal);
    }
}
//*****************************************************
//  Activate Collapse to control.
function InitializeFareSummaryCollapse(strType) {

    //Fare Making Collapse
    $(function () {
        $("#ul_" + strType + "_FareInfo").jqcollapse({
            slide: true,
            speed: 200,
            easing: '',
            hide: false
        });
    });

    //Tax Making Collapse
    $(function () {
        $("#ul_" + strType + "_TaxInfo").jqcollapse({
            slide: true,
            speed: 200,
            easing: '',
            hide: false
        });
    });

    //Making Fee Collapse
    $(function () {
        $("#ul_" + strType + "_FeeInfo").jqcollapse({
            slide: true,
            speed: 200,
            easing: '',
            hide: false
        });
    });
}
function GetSessionQuoteSummary() {
    tikAeroB2C.WebService.B2cService.GetSessionQuoteSummary(SuccessGetSessionQuoteSummary, showErrorPayment, showTimeOutPayment);
}
function SuccessGetSessionQuoteSummary(result) {

    if (result.length > 0) {
        DisplayQuoteSummary(result, "", "");
        //Initialize Summary collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
}
// JScript File

function LoadRegistration() {
    if (MultipleTab.IsCorrectTab()) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadRegistration(SuccessloadRegistration, showError, showTimeOut);
        CloseDialog();
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
function LoadRegistration_Edit() {
    if (MultipleTab.IsCorrectTab()) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_Edit, showError, showTimeOut);
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
function LoadRegistration_EditPassword() {
    if (MultipleTab.IsCorrectTab()) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_EditPassword, showError, showTimeOut);
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
function LoadRegistration_EditPasswordDis() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_EditPasswordDis, showError, showTimeOut);
}
function SuccessloadRegistration(result) {
    var objContainer = document.getElementById("dvContainer");

    objContainer.innerHTML = result;


    ShowSearchPannel(false);

    InitializeDateFormat('ctl00_');
    InitializeRegistrationWaterMark("ctl00_");

    DisplayQuoteSummary("", "", "");

    ShowSearchPannel(true);
    ShowProgressBar(false);

    objContainer = null;
}
function SuccessloadRegistration_Edit(result) {
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            SetRegistrationLoad(result);

            //passenger profile Collapse
            var objPaxLastname = document.getElementsByName("txtPsgLastName");
            if (objPaxLastname.length > 0) {

                for (i = 1; i <= objPaxLastname.length; i++) {
                    $(function () {
                        $("#ulPassenger_" + i).jqcollapse({
                            slide: true,
                            speed: 400,
                            easing: ''
                        });
                    });
                }
                $("#ulPassenger_1").find("a").click();
            }

            //Hide password information
            ShowChangePassword(false);

            InitializeDateFormatPassenger();
            InitializeRegistrationWaterMark("ctl00_");


            ShowSearchPannel(true);
            DisplayQuoteSummary("", "", "");
            ShowProgressBar(false);
            objContainer = null;
            scroll(0, 0);
        }
    }
}
function SuccessloadRegistration_EditPassword(result) {
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            SetRegistrationLoad(result);
            ChangePassword('ctl00_');

            //Hide passenger profile detail information.
            var objdvPassengerWrapper = document.getElementById("dvPassengerProfileWrapper");
            if (objdvPassengerWrapper != null) {
                objdvPassengerWrapper.style.display = "none";
            }

            DisplayQuoteSummary("", "", "");
            ShowSearchPannel(false);
            ShowProgressBar(false);
            objContainer = null;
        }
    }
}
function SuccessloadRegistration_EditPasswordDis(result) {

    SetRegistrationLoad(result);
    ShowProgressBar(false);
    objContainer = null;
}
function SetRegistrationLoad(result) {
    var objContainer = document.getElementById("dvContainer");
    objContainer.innerHTML = result;

    var objpassword = document.getElementById("ctl00_txtPassword");
    var objReconfirmpassword = document.getElementById("ctl00_txtReconfirmPassword");

    objpassword.value = "sssssssss";
    objReconfirmpassword.value = "sssssssss";

    InitializePassengerAddWaterMark("ctl00_");
    InitializeDateFormatPassengerAdd('ctl00_');

    disabledField('ctl00_');
}
function SuccessCreateClientProfile(result) {

    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "01") {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, '');
        }
        else if (result == "02") {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_87, "Email already exist!!"), 0, '');
        }
        else {
            ClearData('ctl00_');
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_88, "Your profile has been established as") + " " + result, 1, 'loadHome');
        }
    }
    else {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, '');
    }
}
function SuccessClientProfileSave(result) {
    if (result == true) {
        ShowProgressBar(false);
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_89, "Save Success"), 0, '');
        LoadRegistration_Edit();
    }
    else {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, 'loadHome');
    }
}
function SuccessClientPasswordSave(result) {
    if (result == true) {
        ShowProgressBar(false);
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_89, "Save Success"), 1, '');
        LoadRegistration_EditPasswordDis();
    }
    else {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, 'loadHome');
    }
}
function SuccessPassengerSave(result) {
    if (result == true) {
        ShowProgressBar(false);
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_89, "Save Success"), 0, '');
        LoadRegistration_Edit();
    }
    else {
        ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_86, "Can't save!!"), 0, '');
    }
}

function CreateClientProfile(o) {
    Status = true;
    Status = ValidateRegister(o);

    if (Status == true) {
        var Title = GetSelectedOption(document.getElementById(o + 'ddlTitle'));

        var gender = 'M';
        var first_name = GetControlValue(document.getElementById(o + 'txtFirstName'));
        var last_name = GetControlValue(document.getElementById(o + 'txtLastName'));
        var dateOfBirth = GetControlValue(document.getElementById(o + 'txtDateofBirth'));
        var passportnumber = GetControlValue(document.getElementById(o + 'txtDocumentNumber'));
        var nationality = GetSelectedOption(document.getElementById(o + 'optNationality'));
        var IssueCountry = GetSelectedOption(document.getElementById(o + 'opIssueCountry'));
        var documenttype = GetSelectedOption(document.getElementById(o + 'optDocumentType'));
        var PassengerType = GetSelectedOption(document.getElementById('ddlPassengerType'));

        var passportIssuePlace = GetControlValue(document.getElementById(o + 'txtPlaceOfIssue'));
        var passportBirthPlace = GetControlValue(document.getElementById(o + 'txtPlaceOfBirth'));
        var passportIssueDate = GetControlValue(document.getElementById(o + 'txtIssueDate'));
        var passportExpiryDate = GetControlValue(document.getElementById(o + 'txtExpiryDate'));
        var client_password = GetControlValue(document.getElementById(o + 'txtPassword'));
        var phoneMobile = GetControlValue(document.getElementById(o + 'txtMobilePhone'));
        var phoneHome = GetControlValue(document.getElementById(o + 'txtHomePhone'));
        var phoneBusiness = GetControlValue(document.getElementById(o + 'txtBusinessPhone'));
        var contactEmail = GetControlValue(document.getElementById(o + 'txtEmail'));
        var mobileEmail = GetControlValue(document.getElementById(o + 'txtMobileEmail'));
        var language = GetSelectedOption(document.getElementById(o + 'optLanguage'));

        var addressline1 = GetControlValue(document.getElementById(o + 'txtAddress1'));
        var addressline2 = GetControlValue(document.getElementById(o + 'txtAddressline2'));
        var zipCode = GetControlValue(document.getElementById(o + 'txtZipCode'));
        var city = GetControlValue(document.getElementById(o + 'txtcity'));
        var state = GetControlValue(document.getElementById(o + 'txtState'));

        var country = GetSelectedOption(document.getElementById(o + 'ddlCountry'));
        var url = window.location;
        var success;
        first_name = ReplaceStringSave(first_name);
        last_name = ReplaceStringSave(last_name);

        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.CreateClientProfile(Title,
                                                            gender,
                                                            first_name,
                                                            last_name,
                                                            ReformatDate(dateOfBirth),
                                                            nationality,
                                                            documenttype,
                                                            passportnumber,
                                                            IssueCountry,
                                                            passportIssuePlace,
                                                            passportBirthPlace,
                                                            ReformatDate(passportIssueDate),
                                                            ReformatDate(passportExpiryDate),
                                                            client_password,
                                                            phoneMobile,
                                                            phoneHome,
                                                            phoneBusiness,
                                                            contactEmail,
                                                            language,
                                                            addressline1,
                                                            addressline2,
                                                            zipCode,
                                                            city,
                                                            country,
                                                            PassengerType,
                                                            String(url.href),
                                                            mobileEmail,
                                                            state,
                                                            SuccessCreateClientProfile,
                                                            showError,
                                                            showTimeOut);
    }
}
function ReplaceStringSave(str) {
    str = str.replace(".", "");
    str = str.replace("-", "");
    str = str.replace(" ", "");
    str = str.replace("  ", "");
    str = str.replace("   ", "");
    str = str.replace("'", "");
    str = str.replace("_", "");
    return str;
}
function ClearData(o) {
    document.getElementById(o + "txtFirstName").value = "";
    document.getElementById(o + "txtLastName").value = "";
    document.getElementById(o + "txtDateofBirth").value = objLanguage.default_value_1;
    document.getElementById(o + "txtIssueDate").value = objLanguage.default_value_1;
    document.getElementById(o + "txtExpiryDate").value = objLanguage.default_value_1;
    document.getElementById(o + "txtDocumentNumber").value = "";
    document.getElementById(o + "txtPlaceOfIssue").value = "";
    document.getElementById(o + "txtPlaceOfBirth").value = "";

    document.getElementById(o + "txtPassword").value = "";
    document.getElementById(o + "txtMobilePhone").value = "";
    document.getElementById(o + "txtHomePhone").value = "";
    document.getElementById(o + "txtBusinessPhone").value = "";
    document.getElementById(o + "txtEmail").value = "";
    document.getElementById(o + "txtMobileEmail").value = "";

    document.getElementById(o + "txtAddress1").value = "";
    document.getElementById(o + "txtAddressline2").value = "";
    document.getElementById(o + "txtZipCode").value = "";
    document.getElementById(o + "txtcity").value = "";
    document.getElementById(o + "txtState").value = "";

    document.getElementById(o + "txtReconfirmPassword").value = "";

    document.getElementById(o + "LabError").value = "";
    document.getElementById(o + "cboConfirm").checked = false;

    InitializeRegistrationWaterMark(o);
}

function ClearPassenger(o) {
    var passportIssuePlace = document.getElementById(o + "txtPsgPlaceOfIssue").value;
    var passportBirthPlace = document.getElementById(o + "txtPsgPlaceOfBirth").value;
    var passportIssueDate = document.getElementById(o + "txtPsgIssueDate").value;
    var passportExpiryDate = document.getElementById(o + "txtPsgExpiryDate").value;

    document.getElementById(o + "txtPsgFirstName").value = "";
    document.getElementById(o + "txtPsgLastName").value = "";
    document.getElementById(o + "TxtPsgDateofBirth").value = objLanguage.default_value_1;
    document.getElementById(o + "txtPsgDocumentNumber").value = "";

    document.getElementById(o + "txtPsgPlaceOfIssue").value = "";
    document.getElementById(o + "txtPsgPlaceOfBirth").value = "";
    document.getElementById(o + "txtPsgIssueDate").value = objLanguage.default_value_1;
    document.getElementById(o + "txtPsgExpiryDate").value = objLanguage.default_value_1;

    document.getElementById(o + "LabError").value = "";
}
function CreateClientProfileresult(result) {
    if (result == "loadmaintainprofile") {
        loadmaintainprofile();
    }
    else {
        var errorResult = result.split(",");
        if (errorResult.length == 2 && errorResult[0] == "Error") {
            if (errorResult[1] == "session_expired") {
                loadLogout();
            }
            else {
                cleanErrorpanel();
                var pnError = document.getElementById("pnError");
                pnError.innerHTML += "- " + errorResult[1] + "<br />";
            }
        }
        else {
            document.getElementById("content").innerHTML = result;
        }
    }
}
function ClientProfileSave(o, iCount) {
    if (MultipleTab.IsCorrectTab()) {
        var Status = true;
        if (iCount == 0) {
            Status = ValidateRegister_Edit(o);
        }
        else {
            Status = ValidatePassenger(iCount);
        }

        if (Status == true) {
            var Title = document.getElementById(o + "ddlTitle").options[document.getElementById(o + "ddlTitle").selectedIndex].value;

            var gender = "M";
            var first_name = GetControlValue(document.getElementById(o + "txtFirstName"));
            var last_name = GetControlValue(document.getElementById(o + "txtLastName"));
            var client_password = GetControlValue(document.getElementById(o + "txtPassword"));
            var phoneMobile = GetControlValue(document.getElementById(o + "txtMobilePhone"));
            var phoneHome = GetControlValue(document.getElementById(o + "txtHomePhone"));
            var phoneBusiness = GetControlValue(document.getElementById(o + "txtBusinessPhone"));
            var contactEmail = GetControlValue(document.getElementById(o + "txtEmail"));
            var mobileEmail = GetControlValue(document.getElementById(o + "txtMobileEmail"));
            var language = GetSelectedOption(document.getElementById(o + "optLanguage"));

            var addressline1 = GetControlValue(document.getElementById(o + "txtAddress1"));
            var addressline2 = GetControlValue(document.getElementById(o + "txtAddressline2"));
            var zipCode = GetControlValue(document.getElementById(o + "txtZipCode"));
            var city = GetControlValue(document.getElementById(o + "txtcity"));
            var state = GetControlValue(document.getElementById(o + "txtState"));

            var country = GetSelectedOption(document.getElementById(o + "ddlCountry"));

            var nationality = GetSelectedOption(document.getElementById("optNationality_1"));
            var optDocumentType = GetControlValue(document.getElementById("optDocumentType_1"));
            var PassengerRole = GetSelectedOption(document.getElementById("ddlPassengerRole_1"));
            var PassengerType = GetSelectedOption(document.getElementById("ddlPassengerType_1"));

            var passportIssuePlace = GetControlValue(document.getElementById("txtPsgPlaceOfIssue_1"));
            var passportBirthPlace = GetControlValue(document.getElementById("txtPsgPlaceOfBirth_1"));

            var passportIssueDate = GetControlValue(document.getElementById("txtPsgIssueDate_1"));
            var passportExpiryDate = GetControlValue(document.getElementById("txtPsgExpiryDate_1"));
            var dateOfBirth = GetControlValue(document.getElementById("TxtPsgDateofBirth_1"));
            var passportnumber = GetControlValue(document.getElementById("txtPsgDocumentNumber_1"));

            var url = window.location;
            var success = false;

            var TitlePsg = "";
            var genderPsg = "M";
            var first_namePsg = "";
            var last_namePsg = "";
            var dateOfBirthPsg = "";
            var passportnumberPsg = "";
            var nationalityPsg = "";
            var documenttypePsg = "";
            var PassengerRolePsg = "";
            var PassengerTypePsg = "";
            var passportIssuePlacePsg = "";
            var passportBirthPlacePsg = "";
            var passportIssueDatePsg = "";
            var passportExpiryDatePsg = "";
            var hdPassengerIdPsg = "";
            var hdpassenger_profile_idPsg = "";
            var hdclient_profile_idPsg = "";
            var hdclient_numberPsg = "";
            var hdcreate_byPsg = "";
            var hdupdate_byPsg = "";
            var hdupdate_date_timePsg = "";
            var hducreate_date_timePsg = "";
            var hdwheelchair_flagPsg = "";
            var hdvip_flagPsg = "";
            var hdpassenger_weightPsg = "";
            var strPassenger = "";
            var passportIssueCountry = "";

            first_name = ReplaceStringSave(first_name);
            last_name = ReplaceStringSave(last_name);

            var datetime = new Date();
            if (iCount != 0) {
                TitlePsg = GetSelectedOption(document.getElementById("ddlPsgTitle" + "_" + iCount));
                genderPsg = "M";
                first_namePsg = GetControlValue(document.getElementById("txtPsgFirstName" + "_" + iCount));
                last_namePsg = GetControlValue(document.getElementById("txtPsgLastName" + "_" + iCount));
                dateOfBirthPsg = GetControlValue(document.getElementById("TxtPsgDateofBirth" + "_" + iCount));
                passportnumberPsg = GetControlValue(document.getElementById("txtPsgDocumentNumber" + "_" + iCount));
                nationalityPsg = GetSelectedOption(document.getElementById("optNationality" + "_" + iCount));
                documenttypePsg = GetSelectedOption(document.getElementById("optDocumentType" + "_" + iCount));
                PassengerRolePsg = GetSelectedOption(document.getElementById("ddlPassengerRole" + "_" + iCount));
                PassengerTypePsg = GetSelectedOption(document.getElementById("ddlPassengerType" + "_" + iCount));

                passportIssuePlacePsg = GetControlValue(document.getElementById("txtPsgPlaceOfIssue" + "_" + iCount));
                passportBirthPlacePsg = GetControlValue(document.getElementById("txtPsgPlaceOfBirth" + "_" + iCount));
                passportIssueDatePsg = GetControlValue(document.getElementById("txtPsgIssueDate" + "_" + iCount));
                passportExpiryDatePsg = GetControlValue(document.getElementById("txtPsgExpiryDate" + "_" + iCount));
                passportIssueCountry = GetSelectedOption(document.getElementById("optIssueCountry" + "_" + iCount));
                hdPassengerIdPsg = GetControlValue(document.getElementById("hdPassengerId" + "_" + iCount));
                hdpassenger_profile_idPsg = GetControlValue(document.getElementById("hdpassenger_profile_id" + "_" + iCount));
                hdclient_profile_idPsg = GetControlValue(document.getElementById("hdclient_profile_id" + "_" + iCount));
                hdclient_numberPsg = GetControlValue(document.getElementById("hdclient_number" + "_" + iCount));
                hdwheelchair_flagPsg = GetControlValue(document.getElementById("hdwheelchair_flag" + "_" + iCount));
                hdvip_flagPsg = GetControlValue(document.getElementById("hdvip_flag" + "_" + iCount));
                hdpassenger_weightPsg = GetControlValue(document.getElementById("hdpassenger_weight" + "_" + iCount));

                first_namePsg = ReplaceStringSave(first_namePsg);
                last_namePsg = ReplaceStringSave(last_namePsg);

                if (hdwheelchair_flagPsg == "") {
                    hdwheelchair_flagPsg = 0;
                }

                strPassenger = "<ArrayOfPassenger> " +
                                "<Passenger>" +
                                    "<passenger_id>" + hdPassengerIdPsg + "</passenger_id>" +
                                    "<booking_id>00000000-0000-0000-0000-000000000000</booking_id>" +
                                    "<client_number>" + hdclient_numberPsg + "</client_number>" +
                                    "<passenger_profile_id>" + hdpassenger_profile_idPsg + "</passenger_profile_id>" +
                                    "<passenger_type_rcd>" + PassengerTypePsg + "</passenger_type_rcd>" +
                                    "<lastname>" + last_namePsg.toUpperCase() + "</lastname>" +
                                    "<firstname>" + first_namePsg.toUpperCase() + "</firstname>" +
                                    "<title_rcd>" + TitlePsg.split("|")[0] + "</title_rcd>" +
                                    "<gender_type_rcd>" + TitlePsg.split("|")[1] + "</gender_type_rcd>" +
                                    "<nationality_rcd>" + nationalityPsg + "</nationality_rcd>" +
                                    "<passport_number>" + passportnumberPsg + "</passport_number>" +
                                    "<passport_issue_country_rcd>" + passportIssueCountry + "</passport_issue_country_rcd>" +
                                    "<passport_issue_date>" + ReformatDateXml(passportIssueDatePsg) + "</passport_issue_date>" +
                                    "<passport_expiry_date>" + ReformatDateXml(passportExpiryDatePsg) + "</passport_expiry_date>" +
                                    "<passport_issue_place>" + passportIssuePlacePsg + "</passport_issue_place>" +
                                    "<passport_birth_place>" + passportBirthPlacePsg + "</passport_birth_place>" +
                                    "<document_type_rcd>" + documenttypePsg + "</document_type_rcd>" +
                                    "<date_of_birth>" + ReformatDateXml(dateOfBirthPsg) + "</date_of_birth>" +
                                    "<wheelchair_flag>" + hdwheelchair_flagPsg + "</wheelchair_flag>" +
                                    "<vip_flag>" + hdvip_flagPsg + "</vip_flag>" +
                                    " <create_date_time>0001-01-01T00:00:00</create_date_time> " +
                                    "<update_by>***update_by***</update_by>" +
                                    "<update_date_time>" + datetime.format("yyyy-MM-dd'T'HH:MM:ss") + "</update_date_time>" +
                                    "<passenger_weight>" + hdpassenger_weightPsg + "</passenger_weight>" +
                                    "<client_profile_id>***client_profile_id***</client_profile_id>" +
                                    "<passenger_role_rcd>" + PassengerRolePsg + "</passenger_role_rcd>" +
                                "</Passenger>" +
                            "</ArrayOfPassenger>";
            }

            ShowProgressBar(true);
            if (document.getElementById("dvShowpassenger").style.display == "none") {
                tikAeroB2C.WebService.B2cService.ClientProfileSave(Title,
                                                                gender,
                                                                first_name,
                                                                last_name,
                                                                client_password,
                                                                phoneMobile,
                                                                phoneHome,
                                                                phoneBusiness,
                                                                contactEmail,
                                                                language,
                                                                addressline1,
                                                                addressline2,
                                                                zipCode,
                                                                city,
                                                                country,
                                                                nationality,
                                                                optDocumentType,
                                                                PassengerRole,
                                                                PassengerType,
                                                                passportIssuePlace,
                                                                passportBirthPlace,
                                                                ReformatDate(passportIssueDate),
                                                                ReformatDate(passportExpiryDate),
                                                                ReformatDate(dateOfBirth),
                                                                passportnumber,
                                                                passportIssueCountry,
                                                                strPassenger,
                                                                url.href,
                                                                mobileEmail,
                                                                state,
                                                                SuccessClientPasswordSave,
                                                                showError,
                                                                showTimeOut);
            }
            else {
                tikAeroB2C.WebService.B2cService.ClientProfileSave(Title,
                                                                gender,
                                                                first_name,
                                                                last_name,
                                                                client_password,
                                                                phoneMobile,
                                                                phoneHome,
                                                                phoneBusiness,
                                                                contactEmail,
                                                                language,
                                                                addressline1,
                                                                addressline2,
                                                                zipCode,
                                                                city,
                                                                country,
                                                                nationality,
                                                                optDocumentType,
                                                                PassengerRole,
                                                                PassengerType,
                                                                passportIssuePlace,
                                                                passportBirthPlace,
                                                                ReformatDate(passportIssueDate),
                                                                ReformatDate(passportExpiryDate),
                                                                ReformatDate(dateOfBirth),
                                                                passportnumber,
                                                                passportIssueCountry,
                                                                strPassenger,
                                                                url.href,
                                                                mobileEmail,
                                                                state,
                                                                SuccessClientProfileSave,
                                                                showError,
                                                                showTimeOut);
            }
        }
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}

function NewPassenger(o) {
    var Status = true;

    Status = ValidateNewPassenger(o);
    if (Status == true) {
        var Title = GetSelectedOption(document.getElementById(o + "ddlPsgTitle"));

        var gender = "M";
        var first_name = GetControlValue(document.getElementById(o + "txtPsgFirstName"));
        var last_name = GetControlValue(document.getElementById(o + "txtPsgLastName"));
        var dateOfBirth = GetControlValue(document.getElementById(o + "TxtPsgDateofBirth"));
        var passportnumber = GetControlValue(document.getElementById(o + "txtPsgDocumentNumber"));
        var nationality = GetSelectedOption(document.getElementById(o + "optNationality"));
        var documenttype = GetSelectedOption(document.getElementById(o + "optDocumentType"));
        var PassengerRole = GetSelectedOption(document.getElementById(o + "ddlPassengerRole"));
        var PassengerType = GetSelectedOption(document.getElementById("ddlPassengerType"));
        var IssueCountry = GetSelectedOption(document.getElementById(o + "opIssueCountry"));

        var passportIssuePlace = GetControlValue(document.getElementById(o + "txtPsgPlaceOfIssue"));
        var passportBirthPlace = GetControlValue(document.getElementById(o + "txtPsgPlaceOfBirth"));
        var passportIssueDate = GetControlValue(document.getElementById(o + "txtPsgIssueDate"));
        var passportExpiryDate = GetControlValue(document.getElementById(o + "txtPsgExpiryDate"));

        first_name = ReplaceStringSave(first_name);
        last_name = ReplaceStringSave(last_name);

        var url = window.location;
        var success = false;

        tikAeroB2C.WebService.B2cService.PassengerAdd(Title,
                                                    first_name,
                                                    last_name,
                                                    dateOfBirth,
                                                    documenttype,
                                                    passportnumber,
                                                    passportIssuePlace,
                                                    passportBirthPlace,
                                                    passportIssueDate,
                                                    passportExpiryDate,
                                                    nationality,
                                                    PassengerRole,
                                                    PassengerType,
                                                    IssueCountry,
                                                    SuccessPassengerSave,
                                                    showError,
                                                    showTimeOut);
    }
}

function ValidateRegister(o) {
    var Status = true;
    var e = "";
    var fn = trim(GetControlValue(document.getElementById(o + "txtFirstName")));
    var ln = trim(GetControlValue(document.getElementById(o + "txtLastName")));
    var PassengerType = GetSelectedOption(document.getElementById('ddlPassengerType'));
    var pm = document.getElementById(o + "txtMobilePhone");
    var ph = document.getElementById(o + "txtHomePhone");
    var pb = document.getElementById(o + "txtBusinessPhone");
    var p = trim(GetControlValue(pm)) + trim(GetControlValue(ph)) + trim(GetControlValue(pb));
    var em = trim(GetControlValue(document.getElementById(o + "txtEmail")));
    var strMobileEmail = trim(GetControlValue(document.getElementById(o + "txtMobileEmail")));

    var ad1 = trim(GetControlValue(document.getElementById(o + "txtAddress1")));
    var cty = trim(GetControlValue(document.getElementById(o + "txtcity")));
    var pc = trim(GetControlValue(document.getElementById(o + "txtZipCode")));
    var ctr = document.getElementById(o + "ddlCountry");

    var pws = trim(GetControlValue(document.getElementById(o + "txtPassword")));
    var rePws = trim(GetControlValue(document.getElementById(o + "txtReconfirmPassword")));

    var dn = trim(GetControlValue(document.getElementById(o + "txtDocumentNumber")));
    var pi = trim(GetControlValue(document.getElementById(o + "txtPlaceOfIssue")));
    var pb = trim(GetControlValue(document.getElementById(o + "txtPlaceOfBirth")));
    var id = trim(GetControlValue(document.getElementById(o + "txtIssueDate")));

    var objExpiryDate = document.getElementById(o + "txtExpiryDate");
    var dateOfBirth = GetControlValue(document.getElementById(o + "txtDateofBirth"));



    //First Name
    if (fn == 0 || IsChar(fn) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_90, "Please supply a valid First Name") + "</li>";
        document.getElementById(o + "txtFirstName").className = "error";
    }
    else {
        document.getElementById(o + "txtFirstName").className = "";
    }

    //Surname
    if (ln == 0 || IsChar(ln) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_91, "Please supply a valid Surname") + "</li>";
        document.getElementById(o + "txtLastName").className = "error";
    }
    else {
        document.getElementById(o + "txtLastName").className = "";
    }

    //Passenger Type
    if (PassengerType == 0 || IsChar(PassengerType) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_148, "Passenger Type is required") + "</li>";
    }

    // Expiry date
    if (objExpiryDate != null) {
        if (GetControlValue(objExpiryDate) == "") {
        }
        else {

            if (DateValid(GetControlValue(objExpiryDate)) == false) {
                Status = false;
                e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid Expiry Date") + "</li>";
            }
            else {
                if (IsPastDate(GetControlValue(objExpiryDate)) == true) {
                    Status = false;
                    e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid Expiry Date") + "</li>";
                }
            }

        }

    }


    //Telephone Number
    if (p == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_92, "Please supply a valid Telephone Number") + "</li>";
        pm.className = "error";
        ph.className = "error";
        pb.className = "error";
    }
    else {
        pm.className = "";
        ph.className = "";
        pb.className = "";
    }

    //Email
    if (em == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_93, "Please supply a valid Email") + "</li>";
        document.getElementById(o + "txtEmail").className = "error";
    }
    else if (ValidEmail(document.getElementById(o + "txtEmail").value) != true) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_94, "Please supply a valid Email format") + "</li>";
        document.getElementById(o + "txtEmail").className = "error";
    }
    else {
        document.getElementById(o + "txtEmail").className = "";
    }

    //Mobile email
    if (strMobileEmail.length > 0) {
        if (ValidEmail(strMobileEmail) != true) {
            Status = false;
            e += "<li>" + DisplayMessage(objLanguage.Alert_Message_94, "Please supply a valid Email format") + "</li>";
            document.getElementById(o + "txtMobileEmail").className = "error";
        }
    }

    //Address 1
    if (ad1 == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_95, "Please supply a valid Address 1") + "</li>";
        document.getElementById(o + "txtAddress1").className = "error";
    }
    else {
        document.getElementById(o + "txtAddress1").className = "";
    }

    //Town/City
    if (cty == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_96, "Please supply a valid Town/City") + "</li>";
        document.getElementById(o + "txtcity").className = "error";
    }
    else {
        document.getElementById(o + "txtcity").className = "";
    }

    //Postal Code
    if (pc == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_97, "Please supply a valid ZipCode") + "</li>";
        document.getElementById(o + "txtZipCode").className = "error";
    }
    else {
        document.getElementById(o + "txtZipCode").className = "";
    }

    //Pasword
    if (pws == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
    }

    //Password
    if (rePws == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_99, "Please supply a valid Reconfirm Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
    }

    if (pws != rePws) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
        document.getElementById(o + "txtReconfirmPassword").className = "";
    }

    if (e.length > 0) {

        ShowMessageBox(e, 0, '');
        return Status;
    }
    else {
        return Status;
    }
}

function ValidateRegister_Edit(o) {
    var Status = true;
    var e = "";

    var fn = trim(GetControlValue(document.getElementById(o + "txtFirstName")));
    var ln = trim(GetControlValue(document.getElementById(o + "txtLastName")));
    var pm = document.getElementById(o + "txtMobilePhone");
    var ph = document.getElementById(o + "txtHomePhone");
    var pb = document.getElementById(o + "txtBusinessPhone");

    var p = trim(GetControlValue(pm)) + trim(GetControlValue(ph)) + trim(GetControlValue(pb));

    var em = trim(GetControlValue(document.getElementById(o + "txtEmail")));
    var strMobileEmail = trim(GetControlValue(document.getElementById(o + "txtMobileEmail")));
    var ad1 = trim(GetControlValue(document.getElementById(o + "txtAddress1")));
    var cty = trim(GetControlValue(document.getElementById(o + "txtcity")));

    var pc = trim(GetControlValue(document.getElementById(o + "txtZipCode")));
    var ctr = document.getElementById(o + "ddlCountry");
    var pws = trim(GetControlValue(document.getElementById(o + "txtPassword")));
    var rePws = trim(GetControlValue(document.getElementById(o + "txtReconfirmPassword")));

    //First Name
    if (fn == 0 || IsChar(fn) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_90, "Please supply a valid First Name") + "</li>";
        document.getElementById(o + "txtFirstName").className = "error";
    }
    else {
        document.getElementById(o + "txtFirstName").className = "";
    }

    //Surname
    if (ln == 0 || IsChar(ln) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_91, "Please supply a valid Surname") + "</li>";
        document.getElementById(o + "txtLastName").className = "error";
    } else {
        document.getElementById(o + "txtLastName").className = "";
    }

    //Telephone Number
    if (p == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_92, "Please supply a valid Telephone Number") + "</li>";
        pm.className = "error";
        ph.className = "error";
        pb.className = "error";
    }
    else {
        pm.className = "";
        ph.className = "";
        pb.className = "";
    }

    //Email
    if (em == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_93, "Please supply a valid Email") + "</li>";
        document.getElementById(o + "txtEmail").className = "error";
    }
    else if (ValidEmail(document.getElementById(o + "txtEmail").value) != true) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_94, "Please supply a valid Email format") + "</li>";
        document.getElementById(o + "txtEmail").className = "error";
    }
    else {
        document.getElementById(o + "txtEmail").className = "";
    }

    //Mobile email
    if (strMobileEmail.length > 0) {

        if (ValidEmail(strMobileEmail) != true) {
            Status = false;
            e += "<li>" + DisplayMessage(objLanguage.Alert_Message_94, "Please supply a valid Email format") + "</li>";
            document.getElementById(o + "txtMobileEmail").className = "error";
        }
    }

    //Address 1
    if (ad1 == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_95, "Please supply a valid Address 1") + "</li>";
        document.getElementById(o + "txtAddress1").className = "error";
    }
    else {
        document.getElementById(o + "txtAddress1").className = "";
    }

    //Town/City
    if (cty == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_96, "Please supply a valid Town/City") + "</li>";
        document.getElementById(o + "txtcity").className = "error";
    }
    else {
        document.getElementById(o + "txtcity").className = "";
    }

    //County
    /*
    if (dt==0){
    Status =  false;
    e += "<li>Please supply a valid County</li>"
    document.getElementById(o+'_ctlPassengerContactInfo_district').className = 'error';
    }else{
    document.getElementById(o+'_ctlPassengerContactInfo_district').className = '';
    }*/

    //Postal Code
    if (pc == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_97, "Please supply a valid ZipCode") + "</li>";
        document.getElementById(o + "txtZipCode").className = "error";
    } else {
        document.getElementById(o + "txtZipCode").className = "";
    }

    //Pasword
    if (pws == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
        document.getElementById(o + "txtReconfirmPassword").className = "";
    }
    if (rePws == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
        document.getElementById(o + "txtReconfirmPassword").className = "";
    }
    if (pws != rePws) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_98, "Please supply a valid Password") + "</li>";
        document.getElementById(o + "txtPassword").className = "error";
        document.getElementById(o + "txtReconfirmPassword").className = "error";
    }
    else {
        document.getElementById(o + "txtPassword").className = "";
        document.getElementById(o + "txtReconfirmPassword").className = "";
    }

    if (e.length > 0) {
        ShowMessageBox(e, 0, '');
        return Status;
    }
    else {
        return Status;
    }
}
// call when edit
function ValidatePassenger(o) {
    var Status = true;
    var e = "";

    var tt = GetSelectedOption(document.getElementById("ddlPsgTitle" + "_" + o));
    var pr = GetSelectedOption(document.getElementById("ddlPassengerRole" + "_" + o));
    var fn = trim(GetControlValue(document.getElementById("txtPsgFirstName" + "_" + o)));
    var ln = trim(GetControlValue(document.getElementById("txtPsgLastName" + "_" + o)));
    var PassengerTypePsg = GetSelectedOption(document.getElementById("ddlPassengerType" + "_" + o));

    var dn = trim(GetControlValue(document.getElementById("txtPsgDocumentNumber" + "_" + o)));
    var pi = trim(GetControlValue(document.getElementById("txtPsgPlaceOfIssue" + "_" + o)));
    var pb = trim(GetControlValue(document.getElementById("txtPsgPlaceOfBirth" + "_" + o)));
    var id = trim(GetControlValue(document.getElementById("txtPsgIssueDate" + "_" + o)));
    var objExpiryDate = document.getElementById("txtPsgExpiryDate" + "_" + o);
    var dateOfBirth = GetControlValue(document.getElementById("TxtPsgDateofBirth" + "_" + o));

    var todayDate = new Date();
    //--->  Title
    if (tt.length == 0) {
        Status = false;
        e += "<li>Please supply a valid Title</li>";
        document.getElementById("ddlPsgTitle" + "_" + o).className = 'error';
    }

    //--->  role
    if (pr.length == 0) {
        Status = false;
        e += "<li>Please supply a valid role</li>";
        document.getElementById("ddlPassengerRole" + "_" + o).className = 'error';
    }

    //First Name
    if (fn == 0 || IsChar(fn) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_90, "Please supply a valid First Name") + "</li>";
        document.getElementById("txtPsgFirstName" + "_" + o).className = "error";
    }

    //Surname
    if (ln == 0 || IsChar(ln) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_91, "Please supply a valid Surname") + "</li>";
        document.getElementById("txtPsgLastName" + "_" + o).className = "error";
    }

    //Passenger Type
    if (PassengerTypePsg.length == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_148, "Passenger type is required") + "</li>";
        document.getElementById("ddlPassengerType" + "_" + o).className = 'error';
    }
    //Can Add remove document information section here.

    if (dateOfBirth == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid Date of Birth") + "</li>";
        document.getElementById("TxtPsgDateofBirth" + "_" + o).className = "error";
    }
    else if (dateOfBirth == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById("TxtPsgDateofBirth" + "_" + o).className = "error";
    }
    else {

        if (validateChdInfBirthDate(todayDate.getFullYear() + "-" + (todayDate.getMonth() + 1) + "-" + todayDate.getDate(),
                                    ReformatDate(dateOfBirth),
                                    PassengerTypePsg) == false) {
            var strMsgCHDINF = "";
            if (PassengerTypePsg == "CHD") {
                //Alert for child
                strMsgCHDINF = objLanguage.Alert_Message_150;
            }
            else if (PassengerTypePsg == "INF") {
                //Alert for infant
                strMsgCHDINF = objLanguage.Alert_Message_151;
            }
            else {
                //Alert for Adult
                strMsgCHDINF = objLanguage.Alert_Message_149;
            }
            Status = false;
            e += "<li>" + strMsgCHDINF + "</li>";
            document.getElementById("TxtPsgDateofBirth" + "_" + o).className = "error";
        }

    }

    if (IsPastDate(id) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_110, "Issue Date Should be in the past for passenger").replace("{NO}", padZeros(o, 2)) + "<br/>";
        document.getElementById("txtPsgIssueDate" + "_" + o).className = "error";
    }
    if (IsPastDate(dateOfBirth) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_112, "Birth Date Should be in the past for passenger").replace("{NO}", padZeros(o, 2)) + "<br/>";
        document.getElementById('TxtPsgDateofBirth' + '_' + o).className = 'error';
    }

    if (DateValid(dateOfBirth) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_115, "Please supply a valid DateofBirth for passenger").replace("{NO}", padZeros(o, 2)) + "<br/>";
        document.getElementById("TxtPsgDateofBirth" + "_" + o).className = "error";
    }


    // Expiry date
    if (objExpiryDate != null) {
        if (GetControlValue(objExpiryDate) == "") {
        }
        else {

            if (DateValid(GetControlValue(objExpiryDate)) == false) {
                Status = false;
                e += DisplayMessage(objLanguage.Alert_Message_114, "Please supply a valid Expiry Date").replace("{NO}", padZeros(o, 2)) + "<br/>";
                document.getElementById("txtPsgExpiryDate" + "_" + o).className = "error";
            }
            else {
                if (IsPastDate(GetControlValue(objExpiryDate)) == true) {
                    Status = false;
                    e += DisplayMessage(objLanguage.Alert_Message_114, "Please supply a valid Expiry Date").replace("{NO}", padZeros(o, 2)) + "<br/>";
                    document.getElementById("txtPsgExpiryDate" + "_" + o).className = "error";
                }
            }
        }
    }


    if (e.length > 0) {
        ShowMessageBox(e, 0, '');
        return Status;
    }
    else {
        return Status;
    }
}

function ValidateNewPassenger(o) {
    var Status = true;
    var e = "";

    var fn = trim(GetControlValue(document.getElementById(o + "txtPsgFirstName")));
    var ln = trim(GetControlValue(document.getElementById(o + "txtPsgLastName")));
    var ctr = document.getElementById(o + "ddlCountry");
    var dn = trim(GetControlValue(document.getElementById(o + "txtPsgDocumentNumber")));
    var pi = trim(GetControlValue(document.getElementById(o + "txtPsgPlaceOfIssue")));
    var pb = trim(GetControlValue(document.getElementById(o + "txtPsgPlaceOfBirth")));
    var id = trim(GetControlValue(document.getElementById(o + "txtPsgIssueDate")));
    var objExpiryDate = document.getElementById(o + "txtPsgExpiryDate");
    var dateOfBirth = GetControlValue(document.getElementById(o + "TxtPsgDateofBirth"));
    var PassengerType = GetSelectedOption(document.getElementById("ddlPassengerType"));
    var todayDate = new Date();

    //First Name
    if (fn == 0 || IsChar(fn) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_90, "Please supply a valid First Name") + "</li>";
        document.getElementById(o + "txtPsgFirstName").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgFirstName").className = "";
    }

    //Surname
    if (ln == 0 || IsChar(ln) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_91, "Please supply a valid Surname.") + "</li>";
        document.getElementById(o + "txtPsgLastName").className = "error";
    }
    else {
        document.getElementById(o + "txtPsgLastName").className = "";
    }



    if (dateOfBirth == 0) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "TxtPsgDateofBirth").className = "error";
    }
    else if (dateOfBirth == GetDateMask()) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "TxtPsgDateofBirth").className = "error";
    }
    else {
        document.getElementById(o + "TxtPsgDateofBirth").className = "";
    }


    if (IsPastDate(dateOfBirth) == false) {
        Status = false;
        e += DisplayMessage(objLanguage.Alert_Message_109, "Birth Date Should be in the past") + "<br/>";
        document.getElementById(o + "TxtPsgDateofBirth").className = "error";
    }
    else {
        document.getElementById(o + "TxtPsgDateofBirth").className = "";
    }

    //Validate passenger type
    if (PassengerType.length == 0 || IsChar(PassengerType) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_148, "Passenger Type is required") + "</li>";
    }

    if (DateValid(dateOfBirth) == false) {
        Status = false;
        e += "<li>" + DisplayMessage(objLanguage.Alert_Message_106, "Please supply a valid DateofBirth") + "</li>";
        document.getElementById(o + "TxtPsgDateofBirth").className = "error";
    }
    else {
        if (validateChdInfBirthDate(todayDate.getFullYear() + "-" + (todayDate.getMonth() + 1) + "-" + todayDate.getDate(),
                                    ReformatDate(dateOfBirth),
                                    PassengerType) == false) {
            var strMsgCHDINF = "";
            if (PassengerType == "CHD") {
                //Alert for child
                strMsgCHDINF = objLanguage.Alert_Message_150;
            }
            else if (PassengerType == "INF") {
                //Alert for infant
                strMsgCHDINF = objLanguage.Alert_Message_151;
            }
            else {
                //Alert for Adult
                strMsgCHDINF = objLanguage.Alert_Message_149;
            }
            Status = false;
            e += "<li>" + strMsgCHDINF + "</li>";
            document.getElementById(o + "TxtPsgDateofBirth").className = "error";
        }
        else {
            document.getElementById(o + "TxtPsgDateofBirth").className = "";
        }
    }


    // Expiry date
    if (objExpiryDate != null) {
        if (GetControlValue(objExpiryDate) == "") {
        }
        else {
            if (DateValid(GetControlValue(objExpiryDate)) == false) {
                Status = false;
                e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid Expiry Date") + "</li>";
            }
            else {
                if (IsPastDate(GetControlValue(objExpiryDate)) == true) {
                    Status = false;
                    e += "<li>" + DisplayMessage(objLanguage.Alert_Message_105, "Please supply a valid Expiry Date") + "</li>";
                }
            }
        }
    }


    if (e.length > 0) {
        ShowMessageBox(e, 0, '');

        return Status;
    }
    else {
        return Status;
    }
}

function InitializeDateFormat(o) {
    InitializeDateMaskEdit(o + "txtDateofBirth");
    InitializeDateMaskEdit(o + "txtIssueDate");
    InitializeDateMaskEdit(o + "txtExpiryDate");

    document.getElementById(o + "txtDateofBirth").value = objLanguage.default_value_1;
    document.getElementById(o + "txtIssueDate").value = objLanguage.default_value_1;
    document.getElementById(o + "txtExpiryDate").value = objLanguage.default_value_1;
}
function InitializeDateFormatPassenger() {
    var iPaxcount = document.getElementsByName("hdPassengerId").length;
    for (var iCount = 1; iCount <= iPaxcount; iCount++) {
        InitializeDateMaskEdit("TxtPsgDateofBirth" + "_" + iCount);
        InitializeDateMaskEdit("txtPsgIssueDate" + "_" + iCount);
        InitializeDateMaskEdit("txtPsgExpiryDate" + "_" + iCount);
    }
}
function InitializeDateFormatPassengerAdd(o) {
    InitializeDateMaskEdit(o + "TxtPsgDateofBirth");
    InitializeDateMaskEdit(o + "txtPsgIssueDate");
    InitializeDateMaskEdit(o + "txtPsgExpiryDate");

}
function ControlBtt() {
    if (MultipleTab.IsCorrectTab()) {
        var objdv = document.getElementById("dvNewPassenger");
        if (objdv != null) {
            if (objdv.style.display == "none") {
                var objFirstName = document.getElementById("ctl00_txtPsgFirstName");
                objdv.style.display = "block";
                objFirstName.focus();

                //Watermark at date of birth
                InitializeWaterMark("ctl00_TxtPsgDateofBirth", objLanguage.default_value_1, "input");
                //Watermark at ExpiryDate
                InitializeWaterMark("ctl00_txtPsgExpiryDate", objLanguage.default_value_1, "input");

                var pos = findPos(objFirstName);
                window.scrollTo(0, parseInt(pos[1]) - 150);

                //Set passenger number.
                var objPaxId = document.getElementsByName("hdPassengerId");
                var spnPaxNo = document.getElementById("spnPaxNo");
                if (spnPaxNo != null) {
                    if (objPaxId != null) {
                        spnPaxNo.innerHTML = padZeros(objPaxId.length + 1, 2);
                    }
                    else {
                        spnPaxNo.innerHTML = "001";
                    }
                }
            }
            else {
                objdv.style.display = "none";
            }
        }
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}

function enableField(o) {
    if (MultipleTab.IsCorrectTab()) {

        if (document.getElementById("dvShowpassenger").style.display == "none") {
            document.getElementById(o + "txtReconfirmPassword").disabled = false;
            document.getElementById(o + "txtPassword").disabled = false;
        }
        else {
            document.getElementById(o + "ddlTitle").disabled = false;
            document.getElementById(o + "txtFirstName").disabled = false;
            document.getElementById(o + "txtLastName").disabled = false;
            document.getElementById(o + "txtReconfirmPassword").disabled = false;
            document.getElementById(o + "txtPassword").disabled = false;
            document.getElementById(o + "txtMobilePhone").disabled = false;
            document.getElementById(o + "txtHomePhone").disabled = false;
            document.getElementById(o + "txtBusinessPhone").disabled = false;
            document.getElementById(o + "txtEmail").disabled = false;
            document.getElementById(o + "txtMobileEmail").disabled = false;
            document.getElementById(o + "optLanguage").disabled = false;
            document.getElementById(o + "txtAddress1").disabled = false;
            document.getElementById(o + "txtAddressline2").disabled = false;
            document.getElementById(o + "txtZipCode").disabled = false;
            document.getElementById(o + "txtcity").disabled = false;
            document.getElementById(o + "txtState").disabled = false;
            document.getElementById(o + "ddlCountry").disabled = false;
        }
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
function ChangePassword(o) {

    //show password information
    ShowChangePassword(true);
    document.getElementById(o + "txtReconfirmPassword").disabled = false;
    document.getElementById(o + "txtPassword").disabled = false;
}
function disabledField(o) {

    document.getElementById(o + "ddlTitle").disabled = true;
    document.getElementById(o + "txtFirstName").disabled = true;
    document.getElementById(o + "txtLastName").disabled = true;
    document.getElementById(o + "txtReconfirmPassword").disabled = true;
    document.getElementById(o + "txtPassword").disabled = true;
    document.getElementById(o + "txtMobilePhone").disabled = true;
    document.getElementById(o + "txtHomePhone").disabled = true;
    document.getElementById(o + "txtBusinessPhone").disabled = true;
    document.getElementById(o + "txtEmail").disabled = true;
    document.getElementById(o + "txtMobileEmail").disabled = true;
    document.getElementById(o + "optLanguage").disabled = true;
    document.getElementById(o + "txtAddress1").disabled = true;
    document.getElementById(o + "txtAddressline2").disabled = true;
    document.getElementById(o + "txtZipCode").disabled = true;
    document.getElementById(o + "txtcity").disabled = true;
    document.getElementById(o + "txtState").disabled = true;
    document.getElementById(o + "ddlCountry").disabled = true;
    var iPaxcount = document.getElementsByName("hdPassengerId").length;

    for (var iCount = 1; iCount <= iPaxcount; iCount++) {
        document.getElementById("txtPsgFirstName" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgLastName" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgDocumentNumber" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgPlaceOfIssue" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgPlaceOfBirth" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgIssueDate" + "_" + iCount).disabled = true;
        document.getElementById("txtPsgExpiryDate" + "_" + iCount).disabled = true;
        document.getElementById("TxtPsgDateofBirth" + "_" + iCount).disabled = true;
        document.getElementById("ddlPsgTitle" + "_" + iCount).disabled = true;
        document.getElementById("optNationality" + "_" + iCount).disabled = true;
        document.getElementById("optIssueCountry" + "_" + iCount).disabled = true;
        document.getElementById("optDocumentType" + "_" + iCount).disabled = true;
        document.getElementById("ddlPassengerRole" + "_" + iCount).disabled = true;
        document.getElementById("ddlPassengerType" + "_" + iCount).disabled = true;

    }
}
function enableFieldPassenger(o) {
    if (MultipleTab.IsCorrectTab()) {

        if ($("#ulPassenger_" + o + " img").attr("src") == "App_Themes/Default/Images/expand.png") {
            $("#ulPassenger_" + o).find("a").click();
        }

        document.getElementById("txtPsgFirstName" + "_" + o).disabled = false;
        document.getElementById("txtPsgLastName" + "_" + o).disabled = false;

        document.getElementById("txtPsgDocumentNumber" + "_" + o).disabled = false;
        document.getElementById("txtPsgPlaceOfIssue" + "_" + o).disabled = false;
        document.getElementById("txtPsgPlaceOfBirth" + "_" + o).disabled = false;
        document.getElementById("txtPsgIssueDate" + "_" + o).disabled = false;
        document.getElementById("txtPsgExpiryDate" + "_" + o).disabled = false;
        document.getElementById("TxtPsgDateofBirth" + "_" + o).disabled = false;
        document.getElementById("ddlPsgTitle" + "_" + o).disabled = false;
        document.getElementById("optNationality" + "_" + o).disabled = false;
        document.getElementById("optIssueCountry" + "_" + o).disabled = false;
        document.getElementById("optDocumentType" + "_" + o).disabled = false;
        document.getElementById("ddlPassengerType" + "_" + o).disabled = false;
        if (o != 1) {
            document.getElementById("ddlPassengerRole" + "_" + o).disabled = false;
        }
    }
    else {
        MultipleTab.Redirect();
        return false;
    }

}
function disabledFieldPassenger(o) {

    document.getElementById("txtPsgFirstName" + "_" + o).disabled = true;
    document.getElementById("txtPsgLastName" + "_" + o).disabled = true;

    document.getElementById("txtPsgDocumentNumber" + "_" + o).disabled = true;
    document.getElementById("txtPsgPlaceOfIssue" + "_" + o).disabled = true;
    document.getElementById("txtPsgPlaceOfBirth" + "_" + o).disabled = true;
    document.getElementById("txtPsgIssueDate" + "_" + o).disabled = true;
    document.getElementById("txtPsgExpiryDate" + "_" + o).disabled = true;
    document.getElementById("TxtPsgDateofBirth" + "_" + o).disabled = true;
    document.getElementById("ddlPsgTitle" + "_" + o).disabled = true;
    document.getElementById("optNationality" + "_" + o).disabled = true;
    document.getElementById("optIssueCountry" + "_" + o).disabled = true;
    document.getElementById("optDocumentType" + "_" + o).disabled = true;
    document.getElementById("ddlPassengerRole" + "_" + o).disabled = true;
    document.getElementById("ddlPassengerType" + "_" + o).disabled = true;
}
function DateValidate(value) {
    var bResult = true;
    if (value == "") return bResult;
    value = ReformatDate(value);
    var RegExPattern = /^(?=\d)(?:(?!(?:(?:0?[5-9]|1[0-4])(?:\.|-|\/)10(?:\.|-|\/)(?:1582))|(?:(?:0?[3-9]|1[0-3])(?:\.|-|\/)0?9(?:\.|-|\/)(?:1752)))(31(?!(?:\.|-|\/)(?:0?[2469]|11))|30(?!(?:\.|-|\/)0?2)|(?:29(?:(?!(?:\.|-|\/)0?2(?:\.|-|\/))|(?=\D0?2\D(?:(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|2[0-8]|1\d|0?[1-9])([-.\/])(1[012]|(?:0?[1-9]))\2((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?:$|(?=\x20\d)\x20)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$/;
    if (!RegExPattern.test(value)) {
        bResult = false;
        return bResult;
    }
    else {
        bResult = true;
        return bResult;
    }
}

function IfAlphaOnly(s) {
    var i;
    var b = false;
    var c = "";
    if (s.length != 0) {
        for (i = 0; i < s.length; i++) {
            c = s.charCodeAt(i);
            if ((c > 47 && c < 58)) {
                return false;
            }
            else if (((c > 96 && c < 123) || (c > 64 && c < 91)) == false) {
                return false;
            }
        }
        b = true;
    }
    else {
        b = false;
    }

    return b;
}

function CheckCharacter() {
    //    if ((String.fromCharCode(event.keyCode) >= "a" &&
    //    String.fromCharCode(event.keyCode) <= "z") || (String.fromCharCode(event.keyCode) >= "A" &&
    //    String.fromCharCode(event.keyCode) <= "Z")) {
    //        return true;
    //    }
    //    else {
    //        if (String.fromCharCode(event.keyCode) == " ") {
    //            return true;
    //        }
    //        else if (String.fromCharCode(event.keyCode) == ".") {
    //            return true;
    //        }
    //        else if (String.fromCharCode(event.keyCode) == "-") {
    //            return true;
    //        }
    //        else if (String.fromCharCode(event.keyCode) == " ") {
    //            return true;
    //        }
    //        else if (String.fromCharCode(event.keyCode) == "'") {
    //            return true;
    //        }
    //        else if (String.fromCharCode(event.keyCode) == "_") {
    //            return true;
    //        }
    //        else {
    //            return false;
    //        }
    //    }
}

function InitializeRegistrationWaterMark(o) {

    //Passenger profile information.
    InitializeWaterMark(o + "ddlTitle", objLanguage.default_value_2, "select");
    InitializeWaterMark("ddlPassengerType", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "txtLastName", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtFirstName", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtDateofBirth", objLanguage.default_value_1, "input");
    //InitializeWaterMark(o + "optNationality", objLanguage.default_value_2, "select");

    //Document information.
    InitializeWaterMark(o + "optDocumentType", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "txtDocumentNumber", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "txtPlaceOfIssue", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtPlaceOfBirth", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtIssueDate", objLanguage.default_value_1, "input");
    InitializeWaterMark(o + "txtExpiryDate", objLanguage.default_value_1, "input");

    //Contact detail.

    InitializeWaterMark(o + "txtHomePhone", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtMobilePhone", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "txtBusinessPhone", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "txtEmail", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtMobileEmail", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "optLanguage", objLanguage.default_value_2, "select");

    InitializeWaterMark(o + "txtAddress1", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtAddressline2", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "txtZipCode", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtcity", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtState", objLanguage.default_value_3, "input");
    InitializeWaterMark(o + "ddlCountry", objLanguage.default_value_2, "select");

    InitializeWaterMark(o + "txtPassword", objLanguage.default_value_1, "password");
    InitializeWaterMark(o + "txtReconfirmPassword", objLanguage.default_value_1, "password");

    //Passenger profile
    var objIds = document.getElementsByName("hdPassengerId");
    for (var i = 1; i <= objIds.length; i++) {

        InitializeWaterMark("ddlPsgTitle_" + i, objLanguage.default_value_2, "select");
        InitializeWaterMark("ddlPassengerType_" + i, objLanguage.default_value_2, "select");
        InitializeWaterMark("ddlPassengerRole_" + i, objLanguage.default_value_2, "select");

        InitializeWaterMark("txtPsgFirstName_" + i, objLanguage.default_value_2, "input");
        InitializeWaterMark("txtPsgLastName_" + i, objLanguage.default_value_2, "input");

        // InitializeWaterMark("optNationality_" + i, objLanguage.default_value_2, "select");
        InitializeWaterMark("TxtPsgDateofBirth_" + i, objLanguage.default_value_1, "input");
        InitializeWaterMark("txtPsgDocumentNumber_" + i, objLanguage.default_value_3, "input");

        InitializeWaterMark("optDocumentType_" + i, objLanguage.default_value_2, "select");
        InitializeWaterMark("txtPsgPlaceOfBirth_" + i, objLanguage.default_value_2, "input");
        InitializeWaterMark("txtPsgPlaceOfIssue_" + i, objLanguage.default_value_2, "input");

        InitializeWaterMark("txtPsgExpiryDate_" + i, objLanguage.default_value_1, "input");
        InitializeWaterMark("txtPsgIssueDate_" + i, objLanguage.default_value_1, "input");

    }
}

function ShowChangePassword(bValue) {
    var objPasswordSec = document.getElementById("dvRegPassword");
    var objClientInfo = document.getElementById("ClientProfileInfo");
    if (bValue == true) {

        if (objClientInfo != null) {
            objClientInfo.style.display = "none";
        }
        if (objPasswordSec != null) {
            objPasswordSec.style.display = "block";
        }

    }
    else {
        if (objClientInfo != null) {
            objClientInfo.style.display = "block";
        }
        if (objPasswordSec != null) {
            objPasswordSec.style.display = "none";
        }
    }

}
function InitializePassengerAddWaterMark(o) {
    //Passenger profile
    InitializeWaterMark(o + "ddlPsgTitle", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "ddlPassengerType", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "ddlPassengerRole", objLanguage.default_value_2, "select");

    InitializeWaterMark(o + "txtPsgFirstName", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtPsgLastName", objLanguage.default_value_2, "input");

    //InitializeWaterMark(o + "optNationality", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "TxtPsgDateofBirth", objLanguage.default_value_1, "input");
    InitializeWaterMark(o + "txtPsgDocumentNumber", objLanguage.default_value_3, "input");

    InitializeWaterMark(o + "optDocumentType", objLanguage.default_value_2, "select");
    InitializeWaterMark(o + "txtPsgPlaceOfBirth", objLanguage.default_value_2, "input");
    InitializeWaterMark(o + "txtPsgPlaceOfIssue", objLanguage.default_value_2, "input");

    InitializeWaterMark(o + "txtPsgExpiryDate", objLanguage.default_value_1, "input");
    InitializeWaterMark(o + "txtPsgIssueDate", objLanguage.default_value_1, "input");
}

function LoadForgetPassword() {
    if (MultipleTab.IsCorrectTab()) {
        CloseDialog();
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.LoadForgetPassword(SuccessLoadForgetPassword, showError, showTimeOut);
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
/*function SuccessLoadForgetPassword(result)
{   
var objContainer = document.getElementById("dvContainer");
    
objContainer.innerHTML = result;

ShowProgressBar(false);
objContainer = null;
}*/
function SuccessLoadForgetPassword(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");


    //Insert passenger form content.
    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function ForgetPassword() {
    ShowProgressBar(true);
    var url = window.location;
    tikAeroB2C.WebService.B2cService.ForgetPassword(document.getElementById('txtUserID').value
        , url.href, SuccessForgetPassword, showError, showTimeOut);
}

function SuccessForgetPassword(result) {
    ShowProgressBar(false);
    CloseDialog();
    if (result.length > 0) {
        if (result == "100") {
            ShowMessageBox("Error,Client id isn't valid", 0, "");
        }
        else if (result == "101") {
            ShowMessageBox("Error,Cann't reset password.", 0, "");
        }
        else { ShowMessageBox("New password has been sent.", 1, 'loadHome'); }
    }
} function selectSeat(row, col, seatRow, seatCol, paxCount, className, blockChild, bEmergency, strFeeRcd, strJSon) {
    var objExitConfirm = document.getElementById("dvExitConfirm");

    var objSeat = document.getElementById("tbSeat_" + row + "_" + col);
    var objPaxSeat;
    var objdvPaxType;
    var objSeatRow;
    var objSeatCol;
    var objFeeRcd;
    var iNumberOfInfant = 0;
    var objJson = eval("(" + strJSon + ")");

    if (objSeat != null) {
        if (objSeat.className == className) {

            if (objExitConfirm != null && objExitConfirm.style.display == "block") {
                //Please Confirm Exit seat conformation.
                ShowMessageBox(objLanguage.Alert_Message_132, 0, '');
            }
            else {
                //Read Jason string to object...
                if (objJson != null) {
                    iNumberOfInfant = objJson.NumberOfInfant;
                }

                if (AllowedToAssignInfant(iNumberOfInfant, paxCount, seatRow) == false) {
                    ShowMessageBox(objLanguage.Alert_Message_156, 0, '');
                }
                else {

                    for (iCount = 1; iCount <= paxCount; iCount++) {
                        objPaxSeat = document.getElementById("dvSeatNumber" + iCount);
                        objSeatRow = document.getElementById("hdSeatRow" + iCount);
                        objSeatCol = document.getElementById("hdSeatCol" + iCount);
                        objdvPaxType = document.getElementById("dvPaxType" + iCount);
                        objFeeRcd = document.getElementById("hdFeeRcd" + iCount);

                        if (objPaxSeat != null) {
                            if (trim(objPaxSeat.innerHTML).length == 0 && trim(objdvPaxType.innerHTML) != "INF") {

                                if (bEmergency == 0) {
                                    if (trim(objdvPaxType.innerHTML) == "CHD" && blockChild > 0) {
                                        //This Seat is blocked for Children.
                                        ShowMessageBox(objLanguage.Alert_Message_133, 0, '');
                                        break;
                                    }
                                    else {
                                        if (haveInfant(trim(objdvPaxType.innerHTML), paxCount) == true && blockChild > 0) {
                                            //This Seat is blocked for Infant.
                                            ShowMessageBox(objLanguage.Alert_Message_134, 0, '');
                                            break;
                                        }
                                        else {

                                            //Check number of infant limit..


                                            objPaxSeat.innerHTML = seatRow + seatCol;
                                            objSeatRow.value = seatRow;
                                            objSeatCol.value = seatCol;
                                            objSeat.className = "selectedseat";
                                            objFeeRcd.value = strFeeRcd;

                                            if (objdvPaxType.innerHTML == "ADULT") {
                                                //Search infant To update seat infant with adult
                                                for (jCount = 1; jCount <= paxCount; jCount++) {
                                                    objPaxSeat = document.getElementById("dvSeatNumber" + jCount);
                                                    objdvPaxType = document.getElementById("dvPaxType" + jCount);
                                                    objSeatRow = document.getElementById("hdSeatRow" + jCount);
                                                    objSeatCol = document.getElementById("hdSeatCol" + jCount);
                                                    objFeeRcd = document.getElementById("hdFeeRcd" + jCount);

                                                    if (trim(objdvPaxType.innerHTML) == "INF" && trim(objPaxSeat.innerHTML).length == 0) {
                                                        objPaxSeat.innerHTML = seatRow + seatCol;
                                                        objSeatRow.value = seatRow;
                                                        objSeatCol.value = seatCol;
                                                        objFeeRcd.value = strFeeRcd;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (strFeeRcd != "") {
                                                PreCalculateSeat();
                                            }
                                            break;
                                        }
                                    }
                                }
                                else {
                                    if (trim(objdvPaxType.innerHTML) == "ADULT" & haveInfant(trim(objdvPaxType.innerHTML), paxCount) == false) {
                                        //Show Prompt to confirm to select seat.
                                        ShowSeatConfirm(row, col, seatRow, seatCol, paxCount, className, blockChild, bEmergency, strFeeRcd, strJSon);
                                    }
                                    else {
                                        ShowMessageBox(objLanguage.Alert_Message_137, 0, '');
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

            }
        }
        else {
            for (iCount = 1; iCount <= paxCount; iCount++) {
                objPaxSeat = document.getElementById("dvSeatNumber" + iCount);
                objSeatRow = document.getElementById("hdSeatRow" + iCount);
                objSeatCol = document.getElementById("hdSeatCol" + iCount);
                objFeeRcd = document.getElementById("hdFeeRcd" + iCount);

                if (objPaxSeat != null) {
                    if (objPaxSeat.innerHTML == (seatRow + seatCol)) {
                        objPaxSeat.innerHTML = "";
                        objSeatRow.value = "";
                        objSeatCol.value = "";
                        objSeat.className = className;
                        objFeeRcd.value = "";
                    }
                }
            }
            if (strFeeRcd != "") {
                PreCalculateSeat();
            }
        }
    }
    objSeat = null;
    objPaxSeat = null;
    objdvPaxType = null;
    objSeatRow = null;
    objSeatCol = null;

}
function FillSeat() {
    var objSeatPaxId = document.getElementsByName("hdSeatPaxId");
    var objSeatSegmentId = document.getElementsByName("hdSeatSegmentId");
    var objSeatRow = document.getElementsByName("hdSeatRow");
    var objSeatCol = document.getElementsByName("hdSeatCol");
    var objSeatFeeRcd = document.getElementsByName("hdFeeRcd");

    var paxXml = "";

    for (var iCount = 0; iCount < objSeatPaxId.length; iCount++) {
        paxXml = paxXml +
                 "<mapping>" +
                     "<passenger_id>" + objSeatPaxId[iCount].value + "</passenger_id>" +
                     "<segment_id>" + objSeatSegmentId[iCount].value + "</segment_id>" +
                     "<seat_row>" + objSeatRow[iCount].value + "</seat_row>" +
                     "<seat_col>" + objSeatCol[iCount].value + "</seat_col>" +
                     "<fee_rcd>" + objSeatFeeRcd[iCount].value + "</fee_rcd>" +
                 "</mapping>";
    }

    objSeatPaxId = null;
    objSeatSegmentId = null;
    objSeatRow = null;
    objSeatCol = null;
    objSeatFeeRcd = null;
    return paxXml;
}
function ValidateSeatINF() {
    var objSeatPaxId = document.getElementsByName("hdSeatPaxId");
    var objSeatRow = document.getElementsByName("hdSeatRow");
    var objSeatCol = document.getElementsByName("hdSeatCol");
    var objdvPaxType;
    var objPaxSeat;
    var SeatAdultlist = [];
    var SeatInfAdultMatch = false;
    var haveINF = false;

    for (var iCount = 0; iCount < objSeatPaxId.length; iCount++) {

        objPaxSeat = document.getElementById("dvSeatNumber" + (iCount + 1));
        objdvPaxType = document.getElementById("dvPaxType" + (iCount + 1));

        if (trim(objdvPaxType.innerHTML) == "INF") {
            haveINF = true;
            break;
        }
    }

    if (haveINF) {
        for (var iCount = 0; iCount < objSeatPaxId.length; iCount++) {

            objPaxSeat = document.getElementById("dvSeatNumber" + (iCount + 1));
            objdvPaxType = document.getElementById("dvPaxType" + (iCount + 1));

            if (trim(objdvPaxType.innerHTML) == "ADULT") {
                SeatAdultlist.push(objSeatRow[iCount].value + objSeatCol[iCount].value);
            }
        }

        for (var iCount = 0; iCount < objSeatPaxId.length; iCount++) {

            objPaxSeat = document.getElementById("dvSeatNumber" + (iCount + 1));
            objdvPaxType = document.getElementById("dvPaxType" + (iCount + 1));

            if (trim(objdvPaxType.innerHTML) == "INF") {
                for (var i = 0; i < SeatAdultlist.length; i++) {
                    if (objSeatRow[iCount].value + objSeatCol[iCount].value == SeatAdultlist[i]) {
                        SeatInfAdultMatch = true;
                        break;
                    }

                    if (SeatInfAdultMatch == false)
                        return false;
                }

            }

        }

        return SeatInfAdultMatch;
    }

    else {
        return true;
    }

}

function SaveSeatMap() {

    if (MultipleTab.IsCorrectTab()) {
        var paxXml = FillSeat();
        if (paxXml.length > 0) {
            if (ValidateSeatINF() == true) {
                CloseSeatMap();
                ShowProgressBar(true);
                //Call filled seat service with rendering the control
                tikAeroB2C.WebService.B2cService.FillSeat("<booking>" + paxXml + "</booking>", true, SuccessSaveSeatMap, showError, showTimeOut);
            }
            else {
                ShowMessageBox("Please check your Infant seat.", 0, '');
            }
        }

    }
    else {
        MultipleTab.Redirect();
    }
}

function SuccessSaveSeatMap(result) {
    //Reload Passenger Detail Page
    SuccessSavePassengerDetail(result);
    //Close Seat Map form
    //CloseSeatMap();
}
function haveInfant(paxType, paxCount) {
    var b = false;
    if (paxType == "ADULT") {
        var objdvPaxType;
        var objPaxSeat;
        for (iCount = 1; iCount <= paxCount; iCount++) {
            objdvPaxType = document.getElementById("dvPaxType" + iCount);
            objPaxSeat = document.getElementById("dvSeatNumber" + iCount);
            if (trim(objdvPaxType.innerHTML) == "INF" && objPaxSeat.innerHTML.length == 0) {
                b = true;
                break;
            }
        }
    }
    objdvPaxType = null;
    objPaxSeat = null;
    return b;
}

function CancelSelectSeat() {
    CloseSeatMap();
    ShowProgressBar(true);
    //Call filled seat service with rendering the control
    tikAeroB2C.WebService.B2cService.CancelSelectSeat(true, SuccessCancelSelectSeat, showError, showTimeOut);
}

function SuccessCancelSelectSeat(result) {
    var obj = document.getElementById("dvContainer");

    if (result == "{600}") {
        ShowMessageBox("Cancel select seat failed", 0, '');
    }
    else if (result == "{002}") {
        //Cancel failed because of session timeout.
        ShowMessageBox(objLanguage.Alert_Message_128, 0, 'loadHome');
    }
    else {
        if (result.length > 0) {
            obj.innerHTML = result;
            SetPassengerDetail();
            GetSessionQuoteSummary();
        }
    }
    ShowProgressBar(false);
    obj = null;
    ToolTipColor();
}

function CloseSeatMap() {
    var objHolder = document.getElementById("dvFormHolder");
    //Empty Form Holder
    objHolder.innerHTML = "";

    //Show Fading.
    objHolder.style.display = "none";
}
function showSeatMappingFlightSelect(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass) {
    var objTable = document.getElementById("tbSeatItinerary");

    ShowProgressBar(true);

    var obj = document.getElementById("dvProgressBarSeat");
    if (obj != null) {
        obj.style.display = "block";
    }


    for (var iCount = 0; iCount < objTable.rows.length; iCount++) {
        if (objTable.rows[iCount].id.toString().length != 0) {
            if (objTable.rows[iCount].id.toString() != strTrId) {
                //Change class name of tr element
                objTable.rows[iCount].className = "NotSelected";
            }
            else {
                //Change class name of tr element
                objTable.rows[iCount].className = "Selected";
            }
        }
    }
    objTable = null;

    //Get Set fee.
    GetSeatFee(strBoardingClass, strOriginRcd, strDestinationRcd);

    //Call web service.
    SaveSeatMapSelectFlight(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass);

}

function SaveSeatMapSelectFlight(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass) {
    var paxXml = FillSeat();
    if (paxXml.length > 0) {
        // ShowProgressBar(true);
        //Call FillSeat service without rendering the control.
        tikAeroB2C.WebService.B2cService.FillSeat("<booking>" + paxXml + "</booking>", false, SuccessSaveSeatMapSelectFlight(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass), showError, showTimeOut);
    }
}

var isGetMapLoaded = false;
var isGetPassengerSeatMap = false;
function SuccessSaveSeatMapSelectFlight(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass) {
    tikAeroB2C.WebService.B2cService.GetMap(strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass, SuccessLoadMap, showError, showTimeOut);
    tikAeroB2C.WebService.B2cService.GetPassengerSeatMap(strSeatFlightId, SuccessLoadMapPassenger, showError, showTimeOut);
    isGetMapLoaded = false;
    isGetPassengerSeatMap = false;
}

function SuccessLoadMap(result) {
    isGetMapLoaded = true;
    if (result == "{002}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        var objMap = document.getElementById(FindControlName("span", "dvSeatMap"));
        objMap.innerHTML = result;
        objMap = null;
        if (isGetPassengerSeatMap) ShowProgressBar(false);
    }
}
function SuccessLoadMapPassenger(result) {
    isGetPassengerSeatMap = true;
    if (result == "{002}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        var objMapPassenger = document.getElementById(FindControlName("div", "dvSeatPassenger"));
        objMapPassenger.innerHTML = result;
        objMapPassenger = null;

        var obj = document.getElementById("dvProgressBarSeat");
        if (obj != null) {
            obj.style.display = "none";
        }

        if (isGetMapLoaded) ShowProgressBar(false);

        //Baggage watermark
        var objCtrl = document.getElementsByTagName("select");
        for (var iCount = 0; iCount < objCtrl.length; iCount++) {
            if (objCtrl[iCount].id.indexOf("baggage") != -1) {
                InitializeWaterMark(objCtrl[iCount].id.toString(), objLanguage.default_value_7, "select");
            }
        }
    }
    ToolTipColor();
}

function ShowSeatConfirm(row, col, seatRow, seatCol, paxCount, className, blockChild, bEmergency, strFeeRcd, strJSon) {
    var objIni = document.getElementById("ctl00_dvSeatItinerary");
    var objPax = document.getElementById("ctl00_dvSeatPassenger");
    var objSaveButton = document.getElementById("dvSeatButton");
    var objExitConfirm = document.getElementById("dvExitConfirm");

    if (objIni != null) {
        objIni.style.display = "none";
    }
    if (objPax != null) {
        objPax.style.display = "none";
    }
    if (objSaveButton != null) {
        objSaveButton.style.display = "none";
    }
    if (objExitConfirm != null) {
        objExitConfirm.style.display = "block";
    }
    //Detect keyboard key
    $("#btmSeatExitConfirm").click(function (e) {
        CloseConfirm();
        selectSeat(row, col, seatRow, seatCol, paxCount, className, blockChild, 0, strFeeRcd, strJSon);
    });
}

function PreCalculateSeat() {
    ShowProgressBar(true);
    var paxXml = FillSeat();
    if (paxXml.length > 0) {
        // ShowProgressBar(true);
        //Call FillSeat service without rendering the control.
        tikAeroB2C.WebService.B2cService.PreCalculateSeat("<booking>" + paxXml + "</booking>", SuccessPreCalculateSeat, showError, showTimeOut);
    }
}

function SuccessPreCalculateSeat(result) {
    //Display quote information
    GetSessionQuoteSummary();
    ShowProgressBar(false);
}

function CloseConfirm() {
    var objIni = document.getElementById("ctl00_dvSeatItinerary");
    var objPax = document.getElementById("ctl00_dvSeatPassenger");
    var objSaveButton = document.getElementById("dvSeatButton");
    var objExitConfirm = document.getElementById("dvExitConfirm");

    if (objIni != null) {
        objIni.style.display = "block";
    }
    if (objPax != null) {
        objPax.style.display = "block";
    }
    if (objSaveButton != null) {
        objSaveButton.style.display = "block";
    }
    if (objExitConfirm != null) {
        objExitConfirm.style.display = "none";
    }
    $("#btmSeatExitConfirm").unbind();
}
function GetSeatFee(strClass, strOriginRcd, strDetination) {
    tikAeroB2C.WebService.B2cService.ReadSeatFee("SEAT", strClass, strOriginRcd, strDetination, SuccessGetSeatFee, showError, showTimeOut);
    tikAeroB2C.WebService.B2cService.ReadSeatFee("EXTRAS", strClass, strOriginRcd, strDetination, SuccessGetExtraSeatFee, showError, showTimeOut);
    tikAeroB2C.WebService.B2cService.ReadSeatFee("EXITS", strClass, strOriginRcd, strDetination, SuccessGetExitSeatFee, showError, showTimeOut);
    tikAeroB2C.WebService.B2cService.ReadSeatFee("PLEAS", strClass, strOriginRcd, strDetination, SuccessGetPremiumSeatFee, showError, showTimeOut);
}

function SuccessGetSeatFee(result) {
    var obj = document.getElementById("dvSeatFee");
    if (result.length > 0) {

        var objJSON = eval("(" + result + ")");
        if (objJSON != null) {
            if (obj != null) {
                obj.innerHTML = "+ " + AddCommas(parseFloat(objJSON.fee_amount_incl).toFixed(0));
            }
        }
    }
}
function SuccessGetExtraSeatFee(result) {
    var obj = document.getElementById("dvExtraSeatFee");
    if (result.length > 0) {

        var objJSON = eval("(" + result + ")");
        if (objJSON != null) {
            if (obj != null) {
                obj.innerHTML = "+ " + AddCommas(parseFloat(objJSON.fee_amount_incl).toFixed(0));
            }
        }
    }
}
function SuccessGetExitSeatFee(result) {
    var obj = document.getElementById("dvExitSeatFee");
    if (result.length > 0) {

        var objJSON = eval("(" + result + ")");
        if (objJSON != null) {
            if (obj != null) {
                obj.innerHTML = "+ " + AddCommas(parseFloat(objJSON.fee_amount_incl).toFixed(0));
            }
        }
    }
}
function SuccessGetPremiumSeatFee(result) {
    var obj = document.getElementById("dvPremiumSeatFee");
    if (result.length > 0) {

        var objJSON = eval("(" + result + ")");
        if (objJSON != null) {
            if (obj != null) {
                obj.innerHTML = "+ " + AddCommas(parseFloat(objJSON.fee_amount_incl).toFixed(0));
            }
        }
    }
}

function AllowedToAssignInfant(iNumberOfInfantLimit, paxCount, selectedRow) {

    //Count Total number of infant selected in the row.
    var objPaxSeat;
    var objdvPaxType;
    var objSeatRow;
    var iInfantInRow = 0;
    var iInfantselcted = 0;


    for (var iCount = 1; iCount <= paxCount; iCount++) {

        objPaxSeat = document.getElementById("dvSeatNumber" + iCount);
        objdvPaxType = document.getElementById("dvPaxType" + iCount);

        if (trim(objdvPaxType.innerHTML) == "INF" && trim(objPaxSeat.innerHTML).length == 0) {
            iInfantInRow = 1;
            break;
        }
    }

    //Find number of infant already selected
    for (var iCount = 1; iCount <= paxCount; iCount++) {

        objPaxSeat = document.getElementById("dvSeatNumber" + iCount);
        objdvPaxType = document.getElementById("dvPaxType" + iCount);
        objSeatRow = document.getElementById("hdSeatRow" + iCount);
        if (trim(objdvPaxType.innerHTML) == "INF" && trim(objPaxSeat.innerHTML).length > 0 && objSeatRow.value == selectedRow) {
            ++iInfantselcted;
        }
    }
    if (iInfantInRow > 0) {
        if ((iInfantselcted + iInfantInRow + iNumberOfInfantLimit) <= gTotalRowInfantLimit) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return true;
    }

}
function GetSeatMap(strSelectedFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass) {

    if (MultipleTab.IsCorrectTab()) {
        var strResult = FillPassengerDetail(); //Validate and fill data.
        if (strResult.length > 0) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.ShowSeatMap(strResult, strSelectedFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass, SuccessLoadSeatMap, showError, strBoardingClass + "|" + strOriginRcd + "|" + strDestinationRcd);
        }
    }
    else {
        MultipleTab.Redirect();
    }


}
function SuccessLoadSeatMap(result, param) {

    if (result == "{002}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        var objChk = document.getElementById("chkRemember");
        if (objChk.checked == true) {
            //Save Contact Detail to Cookies.
            ContactDetailToCookies();
        }
        else {
            //Delete Cookies.
            deleteCookie("coContact");
        }

        ShowProgressBar(false);
        if (result.length > 0) {

            var objContainer = document.getElementById("dvContainer");
            objContainer.innerHTML = result;
            //Display quote information
            GetSessionQuoteSummary();

            //Baggage watermark
            var objCtrl = document.getElementsByTagName("select");
            for (var iCount = 0; iCount < objCtrl.length; iCount++) {
                if (objCtrl[iCount].id.indexOf("baggage") != -1) {
                    InitializeWaterMark(objCtrl[iCount].id.toString(), objLanguage.default_value_7, "select");
                }
            }

            //Insert passenger form content.
            ClearErrorMsg();
            //Show Fading.
            objContainer.style.display = "block";
            objContainer = null;
        }

        objChk = null;
        if (param.length > 0) {
            var arr = param.split("|");
            GetSeatFee(arr[0], arr[1], arr[2]);
        }
    }
    ToolTipColor();
}
function FillSpecialService() {
    var objFeeId = document.getElementsByName("hdSsrFeeId");
    var objSsrCode = document.getElementsByName("hdSsrCode");
    var objSsrOnRequestFlag = document.getElementsByName("hdSsrOnRequestFlag");
    var objSsrSegment;
    var strXml;
    var objSsr;
    strXml = "<booking>";

    //Loop through fee type
    for (var i = 0; i < objFeeId.length; i++) {
        //Loop throught number of selection box(number of passenger + number of segment).
        objSsrSegment = document.getElementsByName("seSsrAmount_" + (i + 1));

        for (var j = 0; j < objSsrSegment.length; j++) {
            //Loop Through segment(if not married flight it will loop only one time per segment).
            objSsr = eval("([" + objSsrSegment[j].options[objSsrSegment[j].selectedIndex].value + "])");
            for (var k = 0; k < objSsr.length; k++) {
                strXml = strXml +
                        "<service>" +
                            "<passenger_id>" + objSsr[k].passenger_id + "</passenger_id>" +
                            "<booking_segment_id>" + objSsr[k].booking_segment_id + "</booking_segment_id>" +
                            "<origin_rcd>" + objSsr[k].origin_rcd + "</origin_rcd>" +
                            "<destination_rcd>" + objSsr[k].destination_rcd + "</destination_rcd>" +
                            "<fee_id>" + objFeeId[i].value + "</fee_id>" +
                            "<special_service_rcd>" + objSsrCode[i].value + "</special_service_rcd>" +
                            "<service_text>" + document.getElementById("spnSsrDisplayName_" + (i + 1)).innerHTML + "</service_text>" +
                            "<number_of_units>" + objSsrSegment[j].options[objSsrSegment[j].selectedIndex].text + "</number_of_units>" +
                            "<service_on_request_flag>" + objSsrOnRequestFlag[i].value + "</service_on_request_flag>" +
                        "</service>";
            }
            objSsr = null;
        }
        objSsrSegment = null;
    }
    strXml = strXml + "</booking>";
    CloseDialog();
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.FillSpecialService(strXml, true, SuccessFillSpecialService, showError, showTimeOut);

    objFeeId = null;
    objSsrCode = null;
    objSsrOnRequestFlag = null;
}
function SuccessFillSpecialService(result) {
    var objFareSummary = document.getElementById(FindControlName("div", "dvFareSummary"));
    if (objFareSummary != null) {
        objFareSummary.innerHTML = result;
        objFareSummary = null;

        //Initilalise Summary Collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
    ShowProgressBar(false);
}
function CalculatePaxSsr(iRow) {
    var objSelect = document.getElementsByName("seSsrAmount_" + iRow);
    var objSpn = null;
    var dclTotal = 0;

    for (var i = 0; i < objSelect.length; i++) {
        if (objSelect[i].style.display == "block" || objSelect[i].style.display == "") {
            objSpn = document.getElementById("spnSsrFeeAmount_" + iRow + "_" + objSelect[i].id.split("_")[2]);
            dclTotal = dclTotal + (parseInt(objSelect[i].options[objSelect[i].selectedIndex].text) * parseFloat("0" + objSpn.innerHTML));
        }
    }
}

function ShowSpecialService(strPaxId) {
    if (strPaxId.length > 0) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ShowSpecialService(strPaxId, SuccessShowSpecialService, showError, showTimeOut);
    }

}
function SuccessShowSpecialService(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        var objMessage = document.getElementById("dvFormHolder");
        var objContainer = document.getElementById("dvProgressBar");

        //Insert passenger form content.
        objMessage.innerHTML = result;

        //Calculate Service.
        CalculateSpecialService();

        //Show Fading.
        objContainer.style.display = "block";
        objMessage.style.display = "block";

        objMessage = null;
        objContainer = null;
    }
}
function CalculateSpecialService() {
    var objFeeId = document.getElementsByName("hdSsrFeeId");
    var objSsrSegment;

    //Loop through fee type
    for (var i = 0; i < objFeeId.length; i++) {
        //Loop throught number of selection box(number of passenger + number of segment).
        objSsrSegment = document.getElementsByName("seSsrAmount_" + (i + 1));
        //Calculate Total Ssr per passenger.
        CalculatePaxSsr((i + 1));
        objSsrSegment = null;
    }

    objFeeId = null;
}
function Tab() {
    this.tabId = "";
    this.status = "";
}

function checkSessionTab(isSigleTab, redirectURL) {
    var mainTab = getMainTab();
    var tab = getSessionTab();
    var tabs = getLocalTabs();

    if (tab == null && tabs == null)
        createTab();
    else if (tab == null && tabs != null && mainTab == null)
        createTab();
    else if (tab == null && tabs != null && mainTab != null)
        createSessionTab();
    else if (tab != null && tabs != null && mainTab != null)
        updateSessionTab(tab);
    else if (tab != null && tabs != null && mainTab == null)
        updateTab(tab);
    else
        updateTab(tab);

    mainTab = getMainTab();
    tab = getSessionTab();
    tabs = getLocalTabs();
    var isMain = isMainTab(mainTab, tab);
    if (isSigleTab == true && isMain == false) {
        //clearLocalTabs();
        //setSessionUnload();
        window.location = redirectURL;
    }

    //        if (mainTab != null) document.writeln("maintab==> id:" + mainTab.tabId + ", status:" + mainTab.status + "<br/>");
    //        if (tab != null) document.writeln("session tab==> id:" + tab.tabId + ", status:" + tab.status + "<br/>");
    //        document.writeln("isMain: " + isMain + "<br/>");
}

function createTab() {
    var tab = new Tab();
    tab.tabId = generateGuid();
    tab.status = "loaded";
    setSessionTab(tab);
    setLocalTabs(tab);
}

function updateTab(tab) {
    tab.status = "loaded";
    setSessionTab(tab);
    setLocalTabs(tab);
}

function createSessionTab() {
    var tab = new Tab();
    tab.tabId = generateGuid();
    tab.status = "loaded";
    setSessionTab(tab);
}

function updateSessionTab(tab) {
    tab.status = "loaded";
    setSessionTab(tab);
}

function getSessionTab() {
    return JSON.parse(sessionStorage.getItem("tabId"));
}

function setSessionTab(tab) {
    sessionStorage.setItem("tabId", JSON.stringify(tab));
}

function getLocalTabs() {
    var tabs = localStorage.getItem("tabs");
    if (tabs != null) { tabs = JSON.parse(tabs); }
    return tabs;
}

function getMainTab() {
    var tab = null;
    var tabs = getLocalTabs();
    if (tabs != null) {
        for (var i = 0; i < tabs.length; i++) {
            if (tabs[i].status == "loaded") {
                tab = tabs[i];
                break;
            }
        }
    }
    return tab;
}

function isMainTab(mainTab, sessionTab) {
    var result = false;
    if (mainTab != null && sessionTab != null)
        if (mainTab.tabId == sessionTab.tabId) result = true;
    return result;
}

function setLocalTabs(tab) {
    var tabs = getLocalTabs();
    if (tabs != null) {
        for (var i = 0; i < tabs.length; i++) {
            var t = tabs[i];
            if (tab.tabId == t.tabId) {
                tabs.splice(i, 1);
                i--;
            }
        }
        tabs.push(tab);
    }
    else {
        tabs = new Array(tab);
    }
    localStorage.setItem("tabs", JSON.stringify(tabs));
}

function clearLocalTabs() {
    localStorage.removeItem("tabs");
}

function generateGuid() {
    var result, i, j;
    result = '';
    for (j = 0; j < 32; j++) {
        if (j == 8 || j == 12 || j == 16 || j == 20)
            result = result + '-';
        i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
        result = result + i;
    }
    return result;
}

function setSessionUnload() {
    var tab = this.getSessionTab();
    if (tab == null) return;
    tab.status = "unloaded";
    this.setSessionTab(tab);
    this.setLocalTabs(tab);
}

function removeTabsUnload() {
    var tabs = getLocalTabs();
    if (tabs == null) return;
    for (var i = 0; i < tabs.length; i++) {
        var tab = tabs[i];
        if (tab.status == "unloaded") {
            tabs.splice(i, 1);
            i--;
        }
    }
    localStorage.setItem("tabs", JSON.stringify(tabs));
}
/*===============================================*/
// config for calendar type
// 1. Normal type
// 2. Show flight type
var calType = 2;
/*Call Web Service Zone*/
/*===== Parameter for call web service ==========*/
var current_cell = null;

function clearHeader() {
    var TabDate = document.getElementById("TabDate");
    if (TabDate != null) {
        for (var i = TabDate.rows.length - 1; i >= 0; i--) {
            TabDate.deleteRow(TabDate.rows[i]);
        }
    }
}


function CreateAjaxControl() {
    var xmlHttp = null;
    try {
        //alert(xmlHttp)
        //Firefox, Opera 8.0+, Safari  
        xmlHttp = new XMLHttpRequest();
        return xmlHttp;
    }
    catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            return xmlHttp;
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                return xmlHttp;
            }
            catch (e) {
                alert("Your browser does not support AJAX!");
                return null;
            }
        }
    }
}

var ControlName;
var CurrentRoute = 0;


function initialcalendar() {


    //Create Calendar 1
    CalendarCallWs("?df=1&i=1&type=datefrom", "divFrom", 1);
    //Create Calendar 2
    CalendarCallWs("?df=0&i=1&type=dateto&nm=1", "divTo", 0);
}

function lessthanfrom(m, y) {
    //yyyymmdd
    var hdd_ctr_none = "";
    var hdd_ctr_datefrom = document.getElementById("hdd_ctr_datefrom");
    var valdatefrom = hdd_ctr_datefrom.value;

    var mm = m.toString();
    if (m.toString().length <= 1) { mm = "0" + m.toString(); }
    //alert(parseInt(y.toString()+mm+"01"))
    //alert(valdatefrom);
    //alert(parseInt(valdatefrom)<= parseInt(y.toString()+mm+"01"))
    if (parseInt(valdatefrom) >= parseInt(y.toString() + mm + "01")) {
        return true;
    }
    else {
        return false;
    }
}

//yyyymmdd
function showschedule(ctrname, celltext, cellname) {

    //function showschedule('datefrom','20080423','ctr_datefrom_20080423')
    var hdd_defCell = document.getElementById("hdd_defCell");
    if (hdd_defCell != null) {
        var cc_Cell = document.getElementById(hdd_defCell.value);
        if (cc_Cell != null) {
            //alert(cc_Cell);
            if (hdd_defCell.value != cellname) {
                //alert(hdd_defCell.value)

                //New Cell Select

                //Old Cell
                cc_Cell.className = "calendar_DayStyle";
                cc_Cell.style.backgroundColor = "";
                //
                //Call Ws
                //alert(dvTable.innerHTML);
                //alert(Origin+","+Distination);
                //setInterval(animation,50);

            }
        }
        var selcell = document.getElementById(cellname);
        current_cell = cellname;
        hdd_defCell.value = cellname;
        selcell.className = "calendar_dateselect";
        //alert(cc_Cell);
        DateFrom = celltext;

        currentdate = celltext;

        DateTo = "";
        animation();
        var lang = "en-us"; //document.getElementById("hddlang");		 
        CallWs("?act=Table&ORG=" + Origin + "&DEST=" + Distination + "&DateFrom=" + DateFrom + "&DateTo=" + DateTo + "&lang=" + lang.value, "dvTable");

    }
    showBelow(celltext);

}
function showBelow(ccdate) {
    var dvCurrenDate = document.getElementById("dvCurrenDate");
    dvCurrenDate.innerHTML = "This below schedule for " + mydateformat(ccdate) + "";

}
function animation() {
    var dvTable = document.getElementById("dvTable");
    dvTable.innerHTML = "<img src='loading.gif' border=0 alt='' />";
}
function CalendarCallWs(parameter, controls, isinitial) {
    var xmlHttp = CreateAjaxControl();
    var ControlName = controls;
    var WS_URL = window.location.href.replace(window.location.href.split('/')[window.location.href.split('/').length - 1], 'Calendar/ddlCalendar.aspx');
    var lang = "en-us"; //document.getElementById("hddlang");
    if (lang.value != "") {
        if (parameter.indexOf('&lang') < 0) {
            parameter = "&lang=" + lang.value;
        }
    }

    WS_URL = WS_URL + parameter;
    //alert(WS_URL);
    var objControls = document.getElementById(ControlName);
    //if(objControls.innerHTML!=""){objControls.innerHTML="<img src='loading.gif' border=0 />";}

    if (xmlHttp != null) {
        xmlHttp.onreadystatechange = function (controls) {
            if (xmlHttp.readyState == 4) {
                objControls.innerHTML = "";
                if (xmlHttp.responseText == "") {
                    objControls.innerHTML = "<div class='schedule_not_found'>Schedule date not found </div>";
                }
                else {


                    var content = xmlHttp.responseText;

                    //content=content.replace('<!--IMG_Ctrl-->',"<img src='Images/bcp.gif' border='0' style='cursor:hand;' onclick=javascript:showCalendar(this);>")
                    //alert(content.indexOf('<!--IMG_Ctrl-->'));
                    //alert(content);
                    objControls.innerHTML = content;
                    if (isinitial == 1) {
                        //showinitial_schedule();
                    }
                }
            }
        };

        xmlHttp.open("GET", WS_URL, true);
        xmlHttp.send(null);
    }
}
function showinitial_schedule() {
    var hdd_defCell = document.getElementById("hdd_defCell");
    var defDay = hdd_defCell.value;
    var cDefDay = defDay.split("_");
    var DateFrom = cDefDay[2];
    currentdate = cDefDay[2];
    var DateTo = '';

    CallWs("?act=Table&ORG=" + Origin + "&DEST=" + Distination + "&DateFrom=" + DateFrom + "&DateTo=" + DateTo, "dvTable");
}


function SelectCurrentDay() {
    var d = new Date();
    var tmpDay = d.getDate();
    var currrentDay = padStrleft(tmpDay, '0', 2);

    if (document.getElementById('ddlMY_1').selectedIndex == 0 && $("#ddlDate_1").val() <= currrentDay) {
        $("#ddlDate_1").val(currrentDay);
    }
}

function hiddenCalendar(id, ctrl) {
    closeCalendar();
    var Cal = document.getElementById('Cal' + id);
    var ddlD = document.getElementById('ddlDate_2');
    // var oldDay2 = ddlD.options[ddlD.selectedIndex].value;
    if (Cal != null) Cal.innerHTML = "";

    if (id == "1") {
        //Change List Day//
        if (ctrl.id.indexOf("ddlDate_") < 0) {
            CreatedListDate('1');
            CreatedListDateByRef('2', '1');
        }
        SelectCurrentDay();
        DDLCopyvalue();
    }
    if (id == "2") {

        var ddlDate_2 = document.getElementById('ddlDate_2');
        var ddlMY_2 = document.getElementById('ddlMY_2');
        var ddlDate_1 = document.getElementById('ddlDate_1');
        var ddlMY_1 = document.getElementById('ddlMY_1');

        var D2 = ddlMY_2.options[ddlMY_2.selectedIndex].value + ddlDate_2.options[ddlDate_2.selectedIndex].value;
        var D1 = ddlMY_1.options[ddlMY_1.selectedIndex].value + ddlDate_1.options[ddlDate_1.selectedIndex].value;

        //alert(D1+'==>'+D2)
        if (parseInt(D1) > parseInt(D2)) {

            //alert("MMM");
            CreatedListDateByRef('2', '1');
            DDLCopyvalue();

        }
        else {
            CreatedListDate('2');
        }
    }

    /* var ddlM= document.getElementById('ddlMY_2');        
    var isCDate = isCheckDate(ddlM.options[ddlM.selectedIndex].value,oldDay2);
    if(isCDate)
    {
    var nIndex=FindByval(ddlD,oldDay2)     
    ddlD.options[nIndex].selected=true;
    }
    */
    /*Check Compare Flight Date And Return Date*/


}

function isCheckDate(ym, od) {
    var isMyDate = CheckIsDate(Number(ym.substring(0, 4)), Number(ym.substring(4, 6)), Number(od));
    return isMyDate;
}

function CreatedListDateByRef(id, refid) {

    var ddlM = document.getElementById('ddlMY_' + refid);
    var ddlD = document.getElementById('ddlDate_' + id);

    var ym = ddlM.options[ddlM.selectedIndex].value;
    var isSelectDate = false;
    if (ym != "") {
        var isMyDate = false;
        clearDDL(ddlD);
        for (var i = 1; i <= 31; i++) {
            isMyDate = CheckIsDate(Number(ym.substring(0, 4)), Number(ym.substring(4, 6)), i);
            if (isMyDate) {
                var sdate = padStrleft(i, '0', 2);

                createDDL(ddlD, sdate, sdate, i - 1);
            }
        }
    }
}

function CreatedListDate(id) {
    var ddlM = document.getElementById('ddlMY_' + id);
    var ddlD = document.getElementById('ddlDate_' + id);
    var ym = ddlM.options[ddlM.selectedIndex].value;

    var mySelectDate = ddlD.options[ddlD.selectedIndex].value;

    var isSelectDate = false;
    if (ym != "") {
        clearDDL(ddlD);
        var isMyDate = false;
        for (var i = 1; i <= 31; i++) {
            isMyDate = CheckIsDate(Number(ym.substring(0, 4)), Number(ym.substring(4, 6)), i);

            if (isMyDate) {
                var sdate = padStrleft(i, '0', 2);
                if (mySelectDate == sdate) {
                    isSelectDate = true;
                }
                else {
                    isSelectDate = false;
                }
                createDDL(ddlD, sdate, sdate, i - 1);
            }
        }

        var index = FindByval(ddlD, mySelectDate);
        ddlD.options[index].selected = true;
    }
}

function padStrleft(str, c, len) {
    if (str.toString().length >= parseInt(len)) return str.toString();

    var nl = (len - str.toString().length);
    var ntxt = '';
    for (var i = 0; i < nl; i++) { ntxt = ntxt + c; }
    return (ntxt + str.toString());
}


function clearDDL(ddl) {
    var len = ddl.options.length;
    for (var i = len - 1; i >= 0; i--) {
        ddl.remove(i);
    }
}

function createDDL(obj, text, value, index) {
    var opt = document.createElement('OPTION');
    opt.value = value;
    opt.text = text;

    obj.options.add(opt, index);

}



function CheckIsDate(y, m, d) {
    var nm = m - 1;
    try {
        var d = new Date(y, nm, d);

        if (parseInt(nm) != d.getMonth())
        { return false; }
        else
        { return true; }
    }
    catch (e) {
        alert('error');
        return false;
    }
}



var currObjx;
function showCalendar(obj, flightType) {


    var Cal = document.getElementById('Cal' + obj.id);
    var ddlDate = document.getElementById("ddlDate_" + obj.id);
    var ddlMonthYear = document.getElementById("ddlMY_" + obj.id);
    var dd = ddlDate.options[ddlDate.selectedIndex].value;
    var my = ddlMonthYear.options[ddlMonthYear.selectedIndex].value;

    var mm = (parseInt(my.substring(4, 6), 10));
    var mxmy = ddlMonthYear.options[ddlMonthYear.options.length - 1].value;
    var mnmy = ddlMonthYear.options[0].value;

    //  "?id="+obj.id
    //  +"&df=1&i=1&type=dateto&d="
    //  +dd
    //  +"&m="
    //  +mm.toString()
    //  +"&y="
    //  +my.substring(0,4)
    //  +"&mxmy="
    //  +mxmy
    //  +"&mnmy="+mnmy
    //alert(TikAeroWebB2E.WebService.BaseService);
    //alert(TikAeroWebB2E.WebService.BaseService);
    var uxOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var uxDest = document.getElementById(FindControlName("select", "optDestination"));
    var from = "";
    var to = "";

    if (Cal.innerHTML == "") {
        if (uxOrigin != null) {
            if (uxOrigin.selectedIndex == -1) {
                from = uxOrigin.options[0].value.split("|")[0];
            }
            else {
                from = uxOrigin.options[uxOrigin.selectedIndex].value.split("|")[0];
            }

            if (uxDest.selectedIndex == -1) {
                to = uxDest.options[0].value.split("|")[2];
            } else {
                to = uxDest.options[uxDest.selectedIndex].value.split("|")[2];
            }

            var lang = "en-us"; //document.getElementById("hddlang");
            currObjx = Cal.id;
            //CalendarCallWs("?id="+obj.id+"&df=1&i=1&type=dateto&d="+dd+"&m="+mm.toString()+"&y="+my.substring(0,4)+"&mxmy="+mxmy+"&mnmy="+mnmy,Cal.id,0);      
            //(string ID, string calendartype, string isonlyonemonth,string date,string month,string year,string mxMY, string mnMY)
            tikAeroB2C.WebService.WebUIRender.GetCalendar(obj.id, flightType, "1", dd, mm.toString(), my.substring(0, 4), mxmy, mnmy, lang, from, to, calType, GetCalendarresult);
            Cal.style.left = parseInt(findPosX(document.getElementById('ddlDate_' + obj.id)));
            var Cal1ID = "";
            switch (obj.id) {
                case "1":
                    Cal1ID = "2";
                    break;
                case "2":
                    Cal1ID = "1";
                    break;
                case "3":
                    Cal1ID = "4";
                    break;
                case "4":
                    Cal1ID = "3";
                    break;
            }
            var objAcal = $get('Cal' + Cal1ID);
            if (objAcal != null) {
                objAcal.innerHTML = '';
                hide_menu("fdCal" + Cal1ID);
            }
        }
    }
    else {
        Cal.innerHTML = "";
        hide_menu("fdCal" + obj.id);
    }

}
var tmpcal;
function GetCalendarresult(result) {
    /*if(currObjx!="")
    {
    var objControls =  document.getElementById(currObjx);     
        
    if(objControls.id.indexOf("calRen")<0)
    {
    if(objControls.id.indexOf("CalenderBase")<0)
    {
    objControls.innerHTML="<div id='d"+objControls.id+"'  style='display:none;z-index:9999;'  class='scrollpopup'>"+result+"</div>";//"<iframe  src='' class='frmcls'>"+content+"</iframe>";
    var dvobj =  document.getElementById("d"+objControls.id);
    display_menu(dvobj,dvobj.id);
    }
    }  
    //objControls.innerHTML=result;
    }*/
    if (currObjx != "") {
        var objControls = document.getElementById(currObjx);

        if (objControls.id.indexOf("calRen") < 0) {
            if (objControls.id.indexOf("CalenderBase") < 0) {
                tmpcal = result;
                RenderTagCalendar(objLanguage.NoFlight);
            }
        }
    }

}

// remove text from wem when don't want to show color on calendar
//for No Flight
function RenderTagCalendar(result) {
    var objControls = document.getElementById(currObjx);
    var strhtml = "<div id='d" + objControls.id + "'  style='display:none;z-index:9999;'  class='scrollpopup'>";
    strhtml += "<div class='close'><a href='javascript:closeCalendar()'>[x]</a></div>";
    strhtml += tmpcal;
    if (result != undefined) {
        strhtml += "<div class='CalendarRemark'><table class='Detail' border='0' cellSpacing='0' cellPadding='0' width='100%'>";
        strhtml += "<tr><td class='no_flight'></td><td >" + result + "</td></tr>";
    }
    tmpcal = strhtml;
    //for Operate Flight
    var op = 'Operate Flight';
    RenderTagCalendarOP(objLanguage.OperateFlight);
}
function RenderTagCalendarOP(result) {
    var objControls = document.getElementById(currObjx);
    var strhtml;
    strhtml = tmpcal;
    if (result != undefined) {
        strhtml += "<tr><td class='operate_flight'></td><td >" + result + "</td></tr>";
        strhtml += "<tr><td></td><td></td></tr></table>";
        strhtml += "</div></div>";
    }
    objControls.innerHTML = strhtml; //"<iframe  src='' class='frmcls'>"+content+"</iframe>";
    var dvobj = document.getElementById("d" + objControls.id);
    display_menu(dvobj, dvobj.id);
}

function closeCalendar() {
    var Cal = document.getElementById('Cal1');
    if (Cal.innerHTML == undefined || Cal.innerHTML == "") {
        Cal = document.getElementById('Cal2');
        Cal.innerHTML = "";
        hide_menu("fdCal2");
    } else {
        Cal.innerHTML = "";
        hide_menu("fdCal1");
    }

    var Cal2 = document.getElementById('Cal3');
    if (Cal2 != null) {
        Cal2.innerHTML = "";
        hide_menu("fdCal3");
    }

    var Cal3 = document.getElementById('Cal4');
    if (Cal3 != null) {
        Cal2.innerHTML = "";
        hide_menu("fdCal4");
    }

}
function findPos(obj) {
    var curleft = curtop = 0;
    if (obj.offsetParent) {
        curleft = obj.offsetLeft;
        curtop = obj.offsetTop;
        while (obj = obj.offsetParent) {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        }
    }
    return [curleft, curtop + 20];
}


function display_menu(parent, named) {



    var menu_element = document.getElementById(named);
    menu_element.style.display = "block";
    var placement = findPos(parent);
    //alert(menu_element)

    //        alert( menu_element.className);
    //        menu_element.style.left = placement[0] + "px";
    //        menu_element.style.top = placement[1] + "px";
    //        menu_element.style.height = menu_element.clientHeight + "px";
    //        menu_element.style.width = menu_element.clientWidth +"px";
    //        menu_element.style.left = menu_element.style.left;
    //        menu_element.style.top = menu_element.style.top;
    //        menu_element.style.display="block";


    //alert(menu_element.clientHeight);
    //menu_element.style.left = placement[0] + "px";
    //menu_element.style.top = placement[1] + "px";
    //alert(menu_element.style.top);
    //alert(named)

    document.getElementById("f" + named).style.height = "110px"; //menu_element.clientHeight +300+ "px";
    document.getElementById("f" + named).style.width = menu_element.clientWidth + "px";
    document.getElementById("f" + named).style.left = placement[0]; //menu_element.style.left;
    document.getElementById("f" + named).style.top = placement[1] - 20 + "px"; //menu_element.style.top;
    document.getElementById("f" + named).style.display = "block";


}
function hide_menu(named) {
    //    var menu_element = document.getElementById(named);
    //    menu_element.style.display = "none";

    document.getElementById(named).style.height = '0';
    document.getElementById(named).style.width = '0';
    document.getElementById(named).style.display = "none";
}

function monthActHome(m, y, cc, flightType) {

    //var dd = (parseInt(d)-1) ;
    /*var id = cc.replace("ctr_","");

    var   ddlDate = document.getElementById("ddlDate_" + id);
    var   ddlMonthYear =document.getElementById("ddlMY_" + id);

    var mxmy=ddlMonthYear.options[ddlMonthYear.options.length-1].value;
    var mnmy=ddlMonthYear.options[0].value;

    //CalendarCallWs("?id="+id+"&df=1&i=1&type=dateto&m="+m.toString()+"&y="+y+"&mxmy="+mxmy+"&mnmy="+mnmy,"Cal"+id,0);
 
    var lang="en-us";//document.getElementById("hddlang");
    currObjx="Cal"+id;
    //CalendarCallWs("?id="+obj.id+"&df=1&i=1&type=dateto&d="+dd+"&m="+mm.toString()+"&y="+my.substring(0,4)+"&mxmy="+mxmy+"&mnmy="+mnmy,Cal.id,0);      
    //(string ID, string calendartype, string isonlyonemonth,string date,string month,string year,string mxMY, string mnMY)
    tikAeroB2C.WebService.WebUIRender.GetCalendar(id,"dateto","1","",m.toString(),y,mxmy,mnmy,lang,GetCalendarresult)  
    //Cal.style.left = parseInt(findPosX(document.getElementById('ddlDate_'+obj.id)));*/

    //var dd = (parseInt(d)-1) ;
    var id = cc.replace("ctr_", "");

    var ddlDate = document.getElementById("ddlDate_" + id);
    var ddlMonthYear = document.getElementById("ddlMY_" + id);

    var mxmy = ddlMonthYear.options[ddlMonthYear.options.length - 1].value;
    var mnmy = ddlMonthYear.options[0].value;

    var uxOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var uxDest = document.getElementById(FindControlName("select", "optDestination"));
    var from = "";
    var to = "";

    if (uxOrigin.selectedIndex == -1) {
        from = uxOrigin.options[0].value.split("|")[0];
    } else {
        from = uxOrigin.options[uxOrigin.selectedIndex].value.split("|")[0];
    }

    if (uxDest.selectedIndex == -1) {
        to = uxDest.options[0].value.split("|")[2];
    } else {
        to = uxDest.options[uxDest.selectedIndex].value.split("|")[2];
    }

    var lang = "en-us"; //document.getElementById("hddlang");
    currObjx = "Cal" + id;

    tikAeroB2C.WebService.WebUIRender.GetCalendar(id, flightType, "1", "", m.toString(), y, mxmy, mnmy, lang, from, to, calType, GetCalendarresult);
    //Cal.style.left = parseInt(findPosX(document.getElementById('ddlDate_'+obj.id)));


}



function showschedule2(id, date, cellObj) {
    var ddlDate = document.getElementById("ddlDate_" + id);
    var ddlMonthYear = document.getElementById("ddlMY_" + id);

    if (ddlDate == null) return;

    ddlMonthYear.selectedIndex = FindByval(ddlMonthYear, date.substring(0, 6));
    hiddenCalendar(id, ddlMonthYear);
    ddlDate.selectedIndex = FindByval(ddlDate, date.substring(6, 8));

    var cal = document.getElementById('Cal' + id);
    var isCopyDef = false;
    /**Check Return date not over Flight date***/
    if (id == "2") {
        var ddlDate = document.getElementById("ddlDate_1");
        var ddlMonthYear = document.getElementById("ddlMY_1");
        var dd = ddlDate.options[ddlDate.selectedIndex].value;
        var mm = ddlMonthYear.options[ddlMonthYear.selectedIndex].value;
        var val = mm + dd;

        isCopyDef = (parseInt(date) < parseInt(val));

    }



    if (id == "1") {
        DDLCopyvalue();
        setSelectedDateSession(date, id);
    }
    if ((id == "2") && (isCopyDef == true)) {
        DDLCopyvalue();

    } else if ((id == "2") && (isCopyDef == false)) {
        setSelectedDateSession(date, id);
    }



    hide_menu("fdCal" + id);
    cal.innerHTML = "";

}
//--------------------------------------------------------------------
function setSelectedDateSession(dym, id) {
    tikAeroB2C.WebService.WebUIRender.setSelectedDate(dym, id);
    return true;
}



function DDLCopyvalue() {
    var ddlDate = document.getElementById("ddlDate_1");
    var ddlMonthYear = document.getElementById("ddlMY_1");
    var ddlDate2 = document.getElementById("ddlDate_2");
    var ddlMonthYear2 = document.getElementById("ddlMY_2");
    ddlDate2.selectedIndex = ddlDate.selectedIndex;
    ddlMonthYear2.selectedIndex = ddlMonthYear.selectedIndex;


}


function FindByval(ddl, val) {
    var index = 0;
    var found = false;

    for (var i = 0; i < ddl.options.length && (!found) ; i++) {
        if (ddl.options[i].value == val) {
            index = i;
            found = true;
        }
        // alert("[DLLOPT]:"+ddl.options[i].value)
    }
    return index;
}

function findPosX(obj) {
    var curleft = 0;
    if (obj.offsetParent)
        while (1) {
            curleft += obj.offsetLeft;
            if (!obj.offsetParent)
                break;
            obj = obj.offsetParent;
        }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
}
function getdatefrom() {
    var ddlMY_1 = document.getElementById('ddlMY_1');
    var ddlDate_1 = document.getElementById('ddlDate_1');
    return (ddlMY_1.options[ddlMY_1.selectedIndex].value + ddlDate_1.options[ddlDate_1.selectedIndex].value);
}
function getdateto() {
    var ddlMY_2 = document.getElementById('ddlMY_2');
    var ddlDate_2 = document.getElementById('ddlDate_2');
    return (ddlMY_2.options[ddlMY_2.selectedIndex].value + ddlDate_2.options[ddlDate_2.selectedIndex].value);
}
function getMonthfrom() {
    var ddlMY_1 = document.getElementById('ddlMY_1');
    return ddlMY_1.options[ddlMY_1.selectedIndex].value;
}
function getMonthto() {
    var ddlMY_2 = document.getElementById('ddlMY_2');
    return ddlMY_2.options[ddlMY_2.selectedIndex].value;
}
function ShowHideCalendar() {
    var objSearch = document.getElementById("optReturn");
    var objCar = document.getElementById("calRen2");
    var objReturnLabel = document.getElementById("lblReturnDate");

    if (objSearch.checked == true) {
        objCar.style.display = "block";
        objReturnLabel.style.display = "block";
    }
    else {
        objCar.style.display = "none";
        objReturnLabel.style.display = "none";
    }

    objReturnLabel = null;
    objCar = null;
    objSearch = null;
}
