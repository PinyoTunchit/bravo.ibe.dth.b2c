﻿function CheckClientLogon() {
    // mb=l
    if (existEachQueryString("cl") && queryStringValue("cl") == "l") {
        return true;
    }
    else {
        return false;
    }
}
function ClientLogon() {
   if (MultipleTab.IsCorrectTab()) { 
        var objClientId = document.getElementById('txtClientID');
        var objPassword = document.getElementById('txtPassword');

        if (GetControlValue(objClientId).length == 0 || GetControlValue(objClientId).length == 0) {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_52, "Login Failed"), 0, '');
        }
        else if (IsNumeric(GetControlValue(objClientId)) == true && GetControlValue(objClientId).length != 8) {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_143, "Invalid Client number"), 0, '');
        }
        else if (ValidEmail(GetControlValue(objClientId)) == false && IsNumeric(GetControlValue(objClientId)) == false) {
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_94, "Invalid Email"), 0, '');
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.ClientLogon(objClientId.value,
                                                 objPassword.value,
                                                 SuccessClientLogon,
                                                 showError,
                                                 showTimeOut);
        }
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
function SuccessClientLogon(result) {
    if (result.length > 0) {
        if (result == "{000}") {

            if (IsSSecurePage() == true) {
                //Success Login.
                //Load My Booking Information
                ShowProgressBar(true);
                //Load My Profile
                tikAeroB2C.WebService.B2cService.LoadRegistration_Edit(SuccessloadRegistration_Edit, showError, showTimeOut);

                //Load Client information
                tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);

                //Reload menu.
                LoadMenu();

                //****************************************************************
                //  Used in case of using login dialog to close dialog when login
                //  success.
                //****************************************************************
                // ShowProgressBar(false);
                var objMessage = document.getElementById("dvFormHolder");

                //Insert passenger form content.
                objMessage.innerHTML = "";
                objMessage.style.display = "none";
                objMessage = null;
             }
            else {
                LoadSecure(true);
            }
        }
        else {
            //Failed Login
            ShowMessageBox(DisplayMessage(objLanguage.Alert_Message_52, "Login Failed"), 0, '');
        }
    }

}
function LoadClientInformation() {
    var ClientProfileId = document.getElementById("ClientProfileId");
    if (ClientProfileId.value.length > 0) {
        // Load Client information
        tikAeroB2C.WebService.B2cService.LoadClientInformation(SuccessLoadClientInformation, showError, showTimeOut);
    }
}
 function SuccessLoadClientInformation(result) {
    if (result.length > 0) {
        ShowClientLogonMenu(true, result);
        ToolTipColor();
    }
}
function ClientLogonDialog() {
    CloseDialog();

    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.ClientLogon(document.getElementById('txtDialogClientID').value,
        document.getElementById('txtDialogPassword').value, SuccessClientLogon, showError, showTimeOut);

}
function ShowClientLogonMenu(bValue, strValue) {

    var obj = document.getElementById("dvClientInfo");
    var objFFP = document.getElementById("dvPromoCode");
    if (bValue == true) {
        obj.style.display = "block";
        if (strValue)
            obj.innerHTML = strValue;

        if (objFFP != null) {
            objFFP.style.display = "block";
        }
    }
    else {
        obj.style.display = "none";
        obj.innerHTML = "";
        if (objFFP != null) {
            objFFP.style.display = "none";
        }
    }
}
function ClientLogOff() {
    if (MultipleTab.IsCorrectTab()) {
        ShowProgressBar(true);
        //Set Insurance popup
        SetShowInsurance("0");
        tikAeroB2C.WebService.B2cService.ClientLogOff(SuccessClientLogOff, showError, showTimeOut);
    }
    else {
        MultipleTab.Redirect();
        return false;
    }
}
function SuccessClientLogOff(result) {
    if (result == true) {
        //Clear Cookies
        deleteCookie("coFFP");

        //Hide client menu.
        ShowClientLogonMenu(false, "");

        //Load B2C Menu
        LoadMenu();
        //Load Home page
        loadHome();
        //ShowProgressBar(false);
    }
}
function LoadDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadClientDialog(SuccessLoadClientDialog, showError, showTimeOut);
}

function SuccessLoadClientDialog(result) {

    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    //Insert passenger form content.
    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;

    InitializeWaterMark("txtClientID", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtPassword", objLanguage.default_value_2, "password");
}
function ReadFFPCookies() {
    var strCookies = getCookie("coFFP");
    if (strCookies != null) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ClientLoad(strCookies, SuccessClientLogon, showError, showTimeOut);
    }
}
function ShowClientLogon() {
    tikAeroB2C.WebService.B2cService.ShowClientLogon(SuccessShowClientLogon, showError, showTimeOut);
}
function SuccessShowClientLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}