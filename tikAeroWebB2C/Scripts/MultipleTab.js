﻿var MultipleTab = (function (window) {

    function GeterateRamdom() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    function guid() {
        return GeterateRamdom() + GeterateRamdom() + '-' + GeterateRamdom() + '-' + GeterateRamdom() + '-' + GeterateRamdom() + '-' + GeterateRamdom() + GeterateRamdom() + GeterateRamdom();
    }

    function AssignInitialTab(sessionID, bForceAssign) {
        var arrLocalValue = localStorage.getItem("TabID");
        var strSessionId = "";

        if (isValidJson(arrLocalValue)) {

            if (arrLocalValue != null && arrLocalValue.length > 0) {
                arrLocalValue = JSON.parse(arrLocalValue);
                if (arrLocalValue != null && arrLocalValue.length > 0) {
                    if (arrLocalValue[0].id != undefined) {
                        var arr = arrLocalValue[0].id.split("|");
                        strSessionId = arr[0];
                    }
                }
            }

            var generateGUID = guid();
            var arrLocal;
            var obj;

            if (strSessionId != sessionID || bForceAssign == true) {
                //Remove all array value when first load to clear Temp.
                localStorage.clear();
                //Create array of localstorage.
                arrLocal = new Array();
                obj = new Object();

                obj.id = sessionID + "|" + generateGUID;
                obj.sessionId = sessionID + "|" + generateGUID;
                obj.main = true;
                arrLocal.push(obj);

                obj = null;
                sessionStorage.setItem("TabID", sessionID + "|" + generateGUID);
            }
            else {
                arrLocal = JSON.parse(localStorage.getItem("TabID"));
                if (arrLocal != null && arrLocal.length > 0) {
                    //Find is main available.
                    var bFindMain = false;
                    for (var i = 0; i < arrLocal.length; i++) {
                        if (arrLocal[i].main == true) {
                            if (arrLocal[i].sessionId == sessionStorage.getItem("TabID") &&
                                arrLocal[i].id == sessionStorage.getItem("TabID")) {
                                break;
                            }
                        }
                        else {

                            //Clear session storage.
                            localStorage.clear();
                            arrLocal = [];

                            var globalId = sessionID + "|" + guid();
                            obj = new Object();

                            obj.id = globalId;
                            obj.sessionId = globalId;
                            obj.main = true;
                            arrLocal.push(obj);

                            sessionStorage.setItem("TabID", globalId);
                            obj = null;
                            break;
                        }
                    }
                }
            }
            localStorage.setItem("TabID", JSON.stringify(arrLocal));
            arrLocal = null;
        }
        else {
            ShowMessageBox("Invalid JSON Format", 0, '');
        }
    }

    function IsCorrectTab() {
        var arrLocal = JSON.parse(localStorage.getItem("TabID"));

        if (sessionStorage.getItem("TabID") != null) {
            for (var i = 0; i < arrLocal.length; i++) {
                if (arrLocal[i].sessionId == sessionStorage.getItem("TabID") &&
                    arrLocal[i].id == sessionStorage.getItem("TabID")) {
                    if (arrLocal[i].main == true) {
                        return true;
                    }
                }
                else {

                    return false;
                }
            }
        }

        //Assign New ID
        if (arrLocal != null && arrLocal.length > 0) {
            var globalId = arrLocal[0].id;
            AssignInitialTab(globalId.split("|")[0], true);
        }

        return true;
    }

    function langCode() {
        var tmpLangcode = "en";
        if (LanguageCode.split("-").length == 2)
            tmpLangcode = LanguageCode.split("-")[0];
        return tmpLangcode;
    }
    function Redirect() {
       window.location = "MultiTabAlert.aspx" ; 
    }

    function ClearID() {

        if (localStorage.getItem("TabID") != null) {
            //Get Array of local storage.
            var arrLocal = JSON.parse(localStorage.getItem("TabID"));
            for (var i = 0; i < arrLocal.length; i++) {
                if (arrLocal[i].sessionId == sessionStorage.getItem("TabID") &&
                    arrLocal[i].id == sessionStorage.getItem("TabID")) {

                    arrLocal[i].main = false;
                    //sessionStorage.removeItem("TabID")
                    localStorage.setItem("TabID", JSON.stringify(arrLocal));

                    break;
                }
            }

        }
    }
    return {
        AssignInitialTab: AssignInitialTab,
        IsCorrectTab: IsCorrectTab,
        ClearID: ClearID,
        Redirect: Redirect

    };
})(window);