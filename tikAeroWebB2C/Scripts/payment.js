﻿// JScript File
var ccErrorNo = 0;
var ccErrors = new Array();
var dclVcAmount = 0;

function paymentCreditCard() {
    ShowLoadBar(true);
    var objNameOnCard = document.getElementById("txtNameOnCard");
    var objCardType = document.getElementById(FindControlName("select", "optCardType"));
    var objIssueNumber = document.getElementById("txtIssueNumber");
    var objIssueMonth = document.getElementById("optIssueMonth");
    var objIssueYear = document.getElementById("optIssueYear");
    var objCardNumber = document.getElementById("txtCardNumber");
    var objExpiredMonth = document.getElementById("optExpiredMonth");
    var objExpiredYear = document.getElementById("optExpiredYear");
    var objCvv = document.getElementById("txtCvv");
    var objAddress1 = document.getElementById("txtAddress1");
    var objAddress2 = document.getElementById("txtAddress2");
    var objCity = document.getElementById("txtCity");
    var objCounty = document.getElementById("txtCounty");
    var objPostCode = document.getElementById("txtPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optCountry"));
    var objCondition = document.getElementById("chkPayCreditCard");

    var xmlStr = ValidateCreditCard(objNameOnCard,
                                    objCardType,
                                    objIssueNumber,
                                    objIssueMonth,
                                    objIssueYear,
                                    objCardNumber,
                                    objExpiredMonth,
                                    objExpiredYear,
                                    objCvv,
                                    objAddress1,
                                    objAddress2,
                                    objCity,
                                    objCounty,
                                    objPostCode,
                                    objCountry,
                                    objCondition);

    if (xmlStr.length > 0) {
        tikAeroB2C.WebService.B2cService.PaymentCreditCard(xmlStr, SuccessPaymentCreditCard, showErrorPayment, showTimeOutPayment);
        //tikAeroB2C.WebService.B2cService.Ogone3DPaymentRequest(xmlStr, SuccessPaymentCreditCard, showErrorPayment, showTimeOutPayment);
        //tikAeroB2C.WebService.B2cService.EqipayPaymentRequest(xmlStr, SuccessProcessEquipayPayment, showErrorPayment, showTimeOutPayment);
        //tikAeroB2C.WebService.B2cService.JccPaymentRequest(xmlStr, SuccessProcessEquipayPayment, showErrorPayment, showTimeOutPayment);                 
    }
}

function SuccessPaymentCreditCard(result) {

    var strError = "";

    if (result.length > 0) {
        if (result == "{002}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
         }
        else {
            var obj;
            var arrResult = result.split("{}");
            if (arrResult.length == 1) {
                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");

                ShowLoadBar(false);
            }
            else {
                if (arrResult[0] == "200") {
                    //Infant over limit
                    strError = objLanguage.Alert_Message_59;
                }
                else if (arrResult[0] == "3D") {
                    //Redirect to 3D secure    
                    $(document.body).append($.base64.decode(arrResult[1]));
                }
                else {
                    strError = arrResult[1];
                }
                ShowLoadBar(false);
                ShowMessageBox(strError, 0, '');
            }

            obj = null;

            //insurance summary display.
            ShowInsuranceSummary();
        }
    }
    else {
        ShowLoadBar(false);
        ShowMessageBox("Failed", 0, '');
    }
}

function activateCreditCardControl() {
    var objCardType = document.getElementById(FindControlName("select", "optCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objCreditCardFare = document.getElementById("dvAdditionalFee");
    var objFareTotal = document.getElementById("dvFareTotal");
    var objExpiredYear = document.getElementById("optExpiredYear");
    var optExpiredMonth = document.getElementById("optExpiredMonth");

    var objCvv = document.getElementById('dvCvv');
    var objExpiryDate = document.getElementById('dvExpiryDate');
    var objDvAddress = document.getElementById('dvAddress');
    var objDvIssueDate = document.getElementById('dvIssuDate');
    var objIssueNumber = document.getElementById('dvIssueNumber');
    var objCardNumber = document.getElementById("txtCardNumber");

    //Clear Error message.
    EmptyErrorMessage();

    ShowCreditCardField(objCardType,
                        objPaymentValue,
                        objCreditCardFare,
                        objFareTotal,
                        objExpiredYear,
                        optExpiredMonth,
                        objCvv,
                        objExpiryDate,
                        objDvAddress,
                        objDvIssueDate,
                        objIssueNumber,
                        objCardNumber);

    //Hide Credit Card Fare
    validateCC("", false);
}
function LoadMyName() {
    var objMyname = document.getElementById("chkMyname");
    if (objMyname.checked == true) {
        tikAeroB2C.WebService.B2cService.GetMyName(SuccessLoadName, showError, showTimeOut);
    }
    else {
        //If check bok not checked clear card holder information.
        SuccessLoadName("");
    }
    objMyname = null;
}

function SuccessLoadName(result) {
    var objNameOnCard = document.getElementById("txtNameOnCard");
    var objAddress1 = document.getElementById("txtAddress1");
    var objAddress2 = document.getElementById("txtAddress2");
    var objCity = document.getElementById("txtCity");
    var objCounty = document.getElementById("txtCounty");
    var objPostCode = document.getElementById("txtPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optCountry"));

    SetMyName(result,
              objNameOnCard,
              objAddress1,
              objAddress2,
              objCity,
              objCounty,
              objPostCode,
              objCountry);
}

function SetMyName(arg,
                   objNameOnCard,
                   objAddress1,
                   objAddress2,
                   objCity,
                   objCounty,
                   objPostCode,
                   objCountry) {
    
    if (arg.length > 0) {
        objNameOnCard.value = arg.split("{}")[0] + " " + arg.split("{}")[1];
        objAddress1.value = arg.split("{}")[2];
        objAddress2.value = arg.split("{}")[3];
        objCounty.value = arg.split("{}")[5];
        objCity.value = arg.split("{}")[6];
        objPostCode.value = arg.split("{}")[9];

        for (icount = 0; icount < objCountry.length; icount++) {
            if (objCountry.options[icount].value == arg.split("{}")[8]) {
                objCountry.selectedIndex = icount;
                break;
            }
        }

        if (GetControlValue(objNameOnCard).length > 0) {
            $("#" + objNameOnCard.id).removeClass("watermarkOn");
        }
        else {
            objNameOnCard.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objAddress1).length > 0) {
            $("#" + objAddress1.id).removeClass("watermarkOn");
        }
        else {
            objAddress1.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objAddress2).length > 0) {
            $("#" + objAddress2.id).removeClass("watermarkOn");
        }
        else {
            objAddress2.value = objLanguage.default_value_3;
        }

        if (GetControlValue(objCity).length > 0) {
            $("#" + objCity.id).removeClass("watermarkOn");
        }
        else {
            objCity.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objCounty).length > 0) {
            $("#" + objCounty.id).removeClass("watermarkOn");
        }
        else {
            objCounty.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objPostCode).length > 0) {
            $("#" + objPostCode.id).removeClass("watermarkOn");
        }
        else {
            objPostCode.value = objLanguage.default_value_2;
        }
    }
    else {
        objNameOnCard.value = objLanguage.default_value_2;
        objAddress1.value = objLanguage.default_value_2;
        objAddress2.value = objLanguage.default_value_3;
        objCity.value = objLanguage.default_value_2;
        objCounty.value = objLanguage.default_value_2;
        objPostCode.value = objLanguage.default_value_2;

        $("#" + objNameOnCard.id).addClass("watermarkOn");
        $("#" + objAddress1.id).addClass("watermarkOn");
        $("#" + objAddress2.id).addClass("watermarkOn");
        $("#" + objCity.id).addClass("watermarkOn");
        $("#" + objCounty.id).addClass("watermarkOn");
        $("#" + objPostCode.id).addClass("watermarkOn");
    }

    objNameOnCard = null;
    objAddress1 = null;
    objAddress2 = null;
    objCity = null;
    objCounty = null;
    objPostCode = null;
    objCountry = null;
}
function payment(arg, ctx) {
    //StartAsync(arg,ctx);
}

function checkCreditCard(cardnumber, cardname, bCheckLength) {
    // Establish card type
    var cardType = -1;

    // Ensure that the user has provided a credit card number
    if (cardnumber.length == 0) {
        ccErrorNo = 1;
        return false;
    }

    // Now remove any spaces from the credit card number
    cardnumber = cardnumber.replace(/\s/g, "");

    // Check that the number is numeric
    var cardNo = cardnumber;
    var cardexp = /^[0-9]{6,19}$/;
    if (!cardexp.exec(cardNo)) {
        ccErrorNo = 2;
        return false;
    }

    // The following are the card-specific checks we undertake.
    var PrefixValid = false;

    // We use these for holding the valid lengths and prefixes of a card type
    var prefix = new Array();
    var lengths = new Array();
    var strCardRange = new Array();
    var strCardNo;
    var strCardType = "";

    for (var i = 0; i < cards.length; i++) {
        // Load an array with the valid prefixes for this card
        prefix = cards[i].prefixes.split(",");
        // Now see if any of them match what we have in the card number

        for (j = 0; j < prefix.length; j++) {
            strCardRange = prefix[j].split("-");
            if (strCardRange.length == 1) {
                strCardNo = cardNo.substring(0, strCardRange[0].length);
                if (strCardNo == strCardRange[0]) {
                    strCardType = cards[i].name.toLowerCase();
                    cardType = i;
                    break;
                }
            }
            else {
                strCardNo = cardNo.substring(0, strCardRange[0].length);
                if (strCardNo >= strCardRange[0] && strCardNo <= strCardRange[1]) {
                    strCardType = strCardType = cards[i].name.toLowerCase();
                    cardType = i;
                    break;
                }
            }
        }
        if (strCardType.length > 0) {
            break;
        }
    }

    if (cardname.toLowerCase() == strCardType) {
        PrefixValid = true;
    }

    // If it isn't a valid prefix there's no point at looking at the length
    if (!PrefixValid) {
        ccErrorNo = 3;
        return false;
    }

    if (bCheckLength == true) {
        // If card type not found, report an error
        if (cardType == -1) {
            ccErrorNo = 0;
            return false;
        }

        // Now check the modulus 10 check digit - if required
        if (cards[cardType].checkdigit) {
            var checksum = 0;                                  // running checksum total
            var mychar = "";                                   // next char to process
            var j = 1;                                         // takes value of 1 or 2

            // Process each digit one by one starting at the right
            var calc;
            for (i = cardNo.length - 1; i >= 0; i--) {

                // Extract the next digit and multiply by 1 or 2 on alternative digits.
                calc = Number(cardNo.charAt(i)) * j;

                // If the result is in two digits add 1 to the checksum total
                if (calc > 9) {
                    checksum = checksum + 1;
                    calc = calc - 10;
                }

                // Add the units element to the checksum total
                checksum = checksum + calc;

                // Switch the value of j
                if (j == 1) {j = 2; } else {j = 1; };
            }
            // All done - if checksum is divisible by 10, it is a valid modulus 10.
            // If not, report an error.

            if (checksum % 10 != 0) {
                ccErrorNo = 3;
                return false;
            }
        }
        // See if the length is valid for this card
        var LengthValid = false;
        lengths = cards[cardType].length.split(",");
        for (j = 0; j < lengths.length; j++) {
            if (cardNo.length == lengths[j]) LengthValid = true;
        }

        // See if all is OK by seeing if the length was valid. We only check the 
        // length if all else was hunky dory.
        if (!LengthValid) {
            ccErrorNo = 4;
            return false;
        };
    }
    // The credit card is in the required format.
    return true;
}
function EmptyErrorMessage() {
    document.getElementById("spnError").innerHTML = "";
    document.getElementById("spnVoucherError").innerHTML = "";
}

function validateVoucher(voucherNumber, password) {
    var objVoucherError = document.getElementById("spnVoucherError");
    var errorMessage = "";

    if (voucherNumber.length == 0 || IsNumeric(voucherNumber) == false) {
        errorMessage = errorMessage + objLanguage.Alert_Message_34 + "<br/>"; //"- Please supply a valid Voucher Number.
    }
    if (password.length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_35 + "<br/>"; //"- Please supply a valid Password.
    }

    if (errorMessage.length == 0) {
        objVoucherError.innerHTML = "";
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ValidateVoucher(voucherNumber, password, successValidateVoucher, showError, showTimeOut);
    }
    else {
        ShowMessageBox(errorMessage, 0, '');
    }
    
}
function successValidateVoucher(result) {
    var objVoucher = document.getElementById("dvVoucherDetail");
    var objVoucherError = document.getElementById("spnVoucherError");
    var objDvPayVoucher = document.getElementById("dvPayVoucher");

    if (result.length == 0) {
        ShowMessageBox(objLanguage.Alert_Message_36, 0, ''); //"Voucher not found or wrong password!"
    }
    else if (result == "DUP") {
        ShowMessageBox(objLanguage.Alert_Message_75, 0, ''); //"This voucher already exist!";
    }
    else {
        objVoucher.innerHTML = result;
        objDvPayVoucher.style.display = "block";
        ShowProgressBar(false);
        //Calculate total amount voucher selected.
        CalculateTotalMultiplePayment();
    }

    objDvPayVoucher = null;
    objVoucherError = null;
    objVoucher = null;
}

function paymentVoucher() {

    var objVoucher = document.getElementsByName("nVoucher");
    var objVoucherError = document.getElementById("spnVoucherError");
    var strValue = "";
    var strVoucherId = "";
    var strVoucherNumber = "";
    var strFormOfPayment = "";
    var strPaymentSubType = "";

    if (objVoucher != null) {
        for (var iCount = 0; iCount < objVoucher.length; iCount++) {
            if (objVoucher[iCount].checked == true) {
                strValue = objVoucher[iCount].value;
                break;
            }
        }
        if (strValue.length > 0) {
            var objCondition = document.getElementById("chkPayVoucher");
            if (objCondition.checked == true) {
                ShowLoadBar(true);

                strVoucherId = strValue.split("|")[0];
                strVoucherNumber = strValue.split("|")[1];
                strFormOfPayment = strValue.split("|")[2];
                strPaymentSubType = strValue.split("|")[3];

                //Process payment voucher.
                tikAeroB2C.WebService.B2cService.PaymentVoucher(strVoucherId, strVoucherNumber, strFormOfPayment, strPaymentSubType, SuccessPaymentVoucher, showError, showTimeOut);
            }
            else {
                ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
            }
            objCondition = null;
        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_76, 0, ''); //"- Please select voucher.";
        }

    }
    objVoucherError = null;
    objVoucher = null;
    
}
function SuccessPaymentVoucher(result) {
    var obj;
    if (result.length > 0) {
        if (result == "{002}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            if (result.split("{}").length == 1) {
                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");
            }
            else {
                obj = document.getElementById("spnVoucherError");
                if (result.split("{}")[1] == "200") {
                    //Infant over limit
                    obj.innerHTML = objLanguage.Alert_Message_59;
                }
                else {
                    obj.innerHTML = result.split("{}")[1];
                }


            }
            ShowLoadBar(false);
            obj = null;
        }
    }
    else {
        ShowLoadBar(false);
    }
}

function showhidelayer(id) {
    var objCreditCardValue = document.getElementById("dvCreditCardValue");
    var objCreditCardFare = document.getElementById("dvAdditionalFee");
    var objFareTotal = document.getElementById("dvFareTotal");

    clearPaymentTabClickColor();
    EmptyErrorMessage();

    //Clear Credit fee informtion
    if (id != 'CreditCard') {
        objFareTotal.innerHTML = AddCommas((parseFloat(0 + RemoveCommas(objFareTotal.innerHTML)) - parseFloat(0 + RemoveCommas(objCreditCardValue.innerHTML))).toFixed(2));
        objCreditCardFare.style.display = "none";
        objCreditCardValue.innerHTML = "";
    }

    objCreditCardValue = null;
    objCreditCardFare = null;
    objFareTotal = null;

    if (id == 'CreditCard') {
        if (document.getElementById('dvcTCreditCard') != null) {

            document.getElementById('dvcTCreditCard').className = "CreditCard";


            document.getElementById('CreditCard').style.display = 'block';
            document.getElementById('Voucher').style.display = 'none';
            document.getElementById('BookNow').style.display = 'none';
            // Start Yai add External Payment
            document.getElementById('External').style.display = 'none';
            // End Yai add External Payment
            //Restore Credit card control when click credit card tab.
            document.getElementById('dvCCButton').style.display = 'block';
            document.getElementById('dvCCConfirm').style.display = 'block';
            document.getElementById('chkPayCreditCard').checked = false;
        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'Voucher') {
        if (document.getElementById('dvcTVoucher') != null) {

            document.getElementById('dvcTVoucher').className = "Voucher";


            document.getElementById('CreditCard').style.display = 'none';
            document.getElementById('Voucher').style.display = 'block';
            document.getElementById('BookNow').style.display = 'none';
            // Start Yai add External Payment
            document.getElementById('External').style.display = 'none';
            // End Yai add External Payment
        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'BookNow') {
        if (document.getElementById('dvcTLater') != null) {

            document.getElementById('dvcTLater').className = "BookNowPayLater";


            document.getElementById('CreditCard').style.display = 'none';
            document.getElementById('Voucher').style.display = 'none';
            document.getElementById('BookNow').style.display = 'block';
            // Start Yai add External Payment
            document.getElementById('External').style.display = 'none';
            // End Yai add External Payment
        }
        else {
            HideAllPayment();
        }
    }
    // Start Yai add External Payment
    else if (id == 'External') {
        if (document.getElementById('dvcTLater') != null) {

            document.getElementById('dvcTExternal').className = "External";


            document.getElementById('CreditCard').style.display = 'none';
            document.getElementById('Voucher').style.display = 'none';
            document.getElementById('BookNow').style.display = 'none';
            document.getElementById('External').style.display = 'block';
        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'ESewa')
    {
        if (document.getElementById('dvESewaTab') != null) {

            document.getElementById('dvESewaTab').className = "eSewa";


            document.getElementById('CreditCard').style.display = 'none';
            document.getElementById('Voucher').style.display = 'none';
            document.getElementById('BookNow').style.display = 'none';
            // Start Yai add External Payment
            document.getElementById('External').style.display = 'none';
            // End Yai add External Payment
            document.getElementById('ESewa').style.display = 'block';
        }
        else {
            HideAllPayment();
        }
    }
    // End Yai add External Payment
}
function clearPaymentTabClickColor() {

    var objTCreditCard = document.getElementById('dvcTCreditCard');
    var objTVoucher = document.getElementById('dvcTVoucher');
    var objTLater = document.getElementById('dvcTLater');
    var objTExternal = document.getElementById('dvcTExternal');

    if (objTCreditCard != null) {
        objTCreditCard.className = "CreditCardDisable";
    }

    if (objTVoucher != null) {
        objTVoucher.className = "VoucherDisable";
    }

    if (objTLater != null) {
        objTLater.className = "BookNowPayLaterDisable";
    }

    if (objTExternal != null) {
        objTExternal.className = "ExternalDisable";
    }

    objTCreditCard = null;
    objTVoucher = null;
    objTLater = null;
    objTExternal = null;
}

// Start Yai add External Payment
function paymentExternal() {
    var objRefCode = document.getElementById("txtReferenceCode");
    var objConfirmRefCode = document.getElementById("txtConfirmReferenceCode");
    var objSecurityCode = document.getElementById("txtSecurityCode");
    var bSuccess = true;
    var errorMessage = "<br/>";

    if (trim(objRefCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_77 + "<br/>"; //"- Please supply a valid User ID Klik BCA.
    }
    if (trim(objConfirmRefCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_78 + "<br/>"; //"- Please supply a valid Confirm User ID Klik BCA.<br/>";
    }
    if ((trim(objRefCode.value) != "") && (trim(objConfirmRefCode.value) != "") && (trim(objRefCode.value) != trim(objConfirmRefCode.value))) {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_79 + "<br/>"; //"- Confirm User ID Klik BCA mismatch.<br/>";
    }
    if (trim(objSecurityCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_80 + "<br/>"; //"- Please supply a valid Security Code.<br/>";
    }
    if (bSuccess) {
        var objCondition = document.getElementById("chkPayExternal");

        if (objCondition.checked == true) {
            ShowLoadBar(true);
            var strRefCode = trim(objRefCode.value);
            var strSecurityCode = trim(objSecurityCode.value);
            // Check Security first, first param is security code, second parameter is case sensitive checking
            tikAeroB2C.WebService.B2cService.CheckCaptchaSecurity(strSecurityCode, true, SuccessCheckCaptchaSecurity, showErrorPayment, showTimeOutPayment);
            // If it does not need to check Captcha use below instead of above
            //tikAeroB2C.WebService.B2cService.ExternalPayment(false, strRefCode, SuccessPaymentExternal, showErrorPayment, showTimeOutPayment);
        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
        }
        objCondition = null;
    }
    else {
        document.getElementById("spnExternalError").innerHTML = errorMessage;
    }
}

function SuccessCheckCaptchaSecurity(result) {
    var bCaptcha = result;
    if (bCaptcha) {
        var objRefCode = document.getElementById("txtReferenceCode");
        strRefCode = trim(objRefCode.value);
        tikAeroB2C.WebService.B2cService.ExternalPayment(false, strRefCode, SuccessPaymentExternal, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowLoadBar(false);
        ShowMessageBox(objLanguage.Alert_Message_81, 0, ''); //"Invalid Security Code !"
    }
}

function SuccessPaymentExternal(result) {
    
    if (result.length > 0) {
        if (result == "{002}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var obj;
            if (result.split("{}").length == 1) {
                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");
            }
            else {
                obj = document.getElementById("spnExternalError");
                if (result.split("{}")[1] == "200") {
                    //Infant over limit
                    obj.innerHTML = objLanguage.Alert_Message_59;
                }
                else {
                    obj.innerHTML = result.split("{}")[1];
                }
            }
            obj = null;
        }
    }
    ShowLoadBar(false);
}

function refreshCaptcha() {
    tikAeroB2C.WebService.B2cService.RefreshCaptcha(SuccessRefreshCaptcha, showErrorPayment, showTimeOutPayment);
}

function SuccessRefreshCaptcha(result) {
    var objImgSecurity = document.getElementById("imgSecurity");
    objImgSecurity.src = "captimage.aspx?r=" + result;
}
// End Yai add External Payment

function EmptyErrorMessage() {
    document.getElementById("spnError").innerHTML = "";
    document.getElementById("spnVoucherError").innerHTML = "";
}
function SaveBookedNowPayLater() {
    var objCondition = document.getElementById("chkPayPostPaid");
    if (objCondition.checked == true) {
        ShowLoadBar(true);

        var bookDate = new Date().format("yyyyMMddHHmmss");
        tikAeroB2C.WebService.B2cService.bookNowPaylater(false, bookDate, SuccessSaveBookedNowPayLater, showErrorPayment, showTimeOutPayment);

    }
    else {
        ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
    }
    objCondition = null;
}
function SuccessSaveBookedNowPayLater(result) {
    var obj;
    if (result.length > 0) {
        if (result == "{002}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else if (result == "overdue payment") {
            //alert('Overdue payment, Please select other method of payment');
            SetPaymentContent();
            InitializePaymentWaterMark();
            ShowMessageBox(objLanguage.Alert_Message_134, 0, '');
        }
        else if (result == "paylimit null") {
            //alert('Please contact our call center for payment timelimit information');
            SetPaymentContent();
            InitializePaymentWaterMark();
            ShowMessageBox(objLanguage.Alert_Message_135, 0, '');
        }
        else {
            if (result.split("{}").length == 1) {
                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                //Call google analytic.
                //SubmitAnalyticPageThankYou();

                //start wellnet
                if (result.split("<!--urlPopup :").length > 1) {
                    var urlPopup = result.split("<!--urlPopup :")[1].replace("-->", "");
                    //window.open(urlPopup, "mywindow");

                    var viewportwidth;
                    var viewportheight;
                    // the more standards compliant browsers (mozilla/netscape/opera/IE7) 
                    if (typeof window.innerWidth != 'undefined') {
                        viewportwidth = window.innerWidth,
                viewportheight = window.innerHeight;
                    }
                    // IE6 in standards compliant mode 
                    else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
                        viewportwidth = document.documentElement.clientWidth,
                viewportheight = document.documentElement.clientHeight;
                    }
                    else {
                        // older versions of IE
                        viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
                viewportheight = document.getElementsByTagName('body')[0].clientHeight;
                    }

                    if (window.showModalDialog) {
                        window.showModalDialog(urlPopup, "wellnet", "dialogWidth:" + viewportwidth + ";dialogHeight:" + viewportheight + ";center:yes");
                    } else {
                        window.open(urlPopup, 'wellnet', "height=" + viewportheight + ",width=" + viewportwidth + ",toolbar=no,directories=no,status=no, menubar=no,scrollbars=no,resizable=no ,modal=yes");
                    }
                }
                //end wellnet

                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");
            }
            else {
                if (result.split("{}")[1] == "200") {
                    //Infant over limit
                    ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
                }
                else {
                    ShowMessageBox(result.split("{}")[1], 0, '');
                }
            }
            obj = null;
            ShowLoadBar(false);

        }
     }
    else {
        ShowLoadBar(false);
    }
}
function ClearPaymentInput() {
    var objChkMyName = document.getElementById("chkMyname");
    var objNameOnCard = document.getElementById("txtNameOnCard");
    var objIssueNumber = document.getElementById("txtIssueNumber");
    var objIssueMonth = document.getElementById("optIssueMonth");
    var objIssueYear = document.getElementById("optIssueYear");
    var objCardNumber = document.getElementById("txtCardNumber");
    var objExpiredMonth = document.getElementById("optExpiredMonth");
    var objExpiredYear = document.getElementById("optExpiredYear");
    var objCvv = document.getElementById("txtCvv");
    var objAddress1 = document.getElementById("txtAddress1");
    var objAddress2 = document.getElementById("txtAddress2");
    var objCity = document.getElementById("txtCity");
    var objCounty = document.getElementById("txtCounty");
    var objPostCode = document.getElementById("txtPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optCountry"));


    objChkMyName.checked = false;
    objNameOnCard.value = "";
    objIssueNumber.value = "";
    objIssueMonth.selectedIndex = 0;
    objIssueYear.selectedIndex = 0;
    objCardNumber.value = "";
    objExpiredMonth.selectedIndex = 0;
    objExpiredYear.selectedIndex = 0;
    objCvv.value = "";
    objAddress1.value = "";
    objAddress2.value = "";
    objCity.value = "";
    objCounty.value = "";
    objPostCode.value = "";
    objCountry.selectedIndex = 0;

}
function validateCC(strCardNumber, bCalculate) {
    var objCardType = document.getElementById(FindControlName("select", "optCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objDvFareTotal = document.getElementById("dvFareTotal");
    var objTotalAdditinalFee = document.getElementById("divAdditionalFeeTotal");
    var objCreditCardValue = document.getElementById("dvCreditCardValue");
    var objCreditCardDetail = document.getElementById("liCreditCardFare");
    var objhdFareTotal = document.getElementById("hdSubTotal");

    var dclFeePercentage = parseFloat(objPaymentValue.split("{}")[13]).toFixed(2);
    var minimumFeeFlag = parseInt(objPaymentValue.split("{}")[14]);
    var Factor = parseInt(objPaymentValue.split("{}")[15]);
    var dclFeeAmount = parseFloat(objPaymentValue.split("{}")[16]).toFixed(2);
    var dclFeeAmountIncl = parseFloat(objPaymentValue.split("{}")[17]).toFixed(2);
    
    
    //Find Credit Card Fee
    var dblCreditCardFee = CalculateCreditCardFee(dclFeeAmount,
                                                  dclFeeAmountIncl,
                                                  parseFloat(0 + GetControlValue(objhdFareTotal)).toFixed(2),
                                                  dclFeePercentage,
                                                  Factor,
                                                  minimumFeeFlag);
     
    

    CalculateCCFee(strCardNumber,
                   bCalculate,
                   objCardType,
                   objPaymentValue,
                   objDvFareTotal,
                   objTotalAdditinalFee,
                   objCreditCardValue,
                   objCreditCardDetail,
                   dblCreditCardFee,
                   objhdFareTotal);

    ShowAdditionalFee();
}

function ShowAdditionalFee() {
    var objAdditionalTotal = document.getElementById("divAdditionalFeeTotal");
    var objDvAdditionalFee = document.getElementById("dvAdditionalFee");
    var dblAddtionTotal = 0;

    if (objAdditionalTotal != null) {
        dblAddtionTotal = parseFloat(0 + RemoveCommas(objAdditionalTotal.innerHTML));
    }

    if (objDvAdditionalFee != null) {
        if (dblAddtionTotal > 0) {

            objDvAdditionalFee.style.display = "block";
        }
        else {
            objDvAdditionalFee.style.display = "none";
        }
    }
}
function HideAllPaymentTab(bValue) {
    if (bValue == true) {
        document.getElementById("dvCCTab").style.display = "none";
        document.getElementById("dvVoucherTab").style.display = "none";

        //Form of payment section
        document.getElementById("CreditCard").style.display = "none";
        document.getElementById("Voucher").style.display = "none";
    }
    else {
        document.getElementById("dvCCTab").style.display = "block";
        document.getElementById("dvVoucherTab").style.display = "block";

        //Form of payment section
        document.getElementById("CreditCard").style.display = "block";
        document.getElementById("Voucher").style.display = "block";
    }
}
function SaveBookedRedeem() {
    var objCondition = document.getElementById("chkPayRedeem");
    if (objCondition.checked == true) {
        ShowLoadBar(true);
        var bookDate = new Date().format("yyyyMMddHHmmss");
        tikAeroB2C.WebService.B2cService.bookNowPaylater(true, bookDate, SuccessSaveBookedRedeem, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
    }
    objCondition = null;
}
function SuccessSaveBookedRedeem(result) {
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    obj = null;
    //If FFP Logon Reload point.
    var strClientProfileId = getCookie("coFFP");
    if (strClientProfileId != null) {
        LoadClientInformation(strClientProfileId);
    }
    ShowLoadBar(false);
}
//Start EquiPay payment
function SuccessProcessEquipayPayment(result) {
    var obj = document.getElementsByTagName("body");
    obj[0].innerHTML = result;
    document.payFormCcard.submit();
}

function paymentEft() {
    var objNameOnCard = document.getElementById("txtElvAccountHolder");
    var objAccNumber = document.getElementById("txtElvAccountNumber");
    var objCountry = document.getElementById(FindControlName("select", "optEftCountry"));
    var objBankCode = document.getElementById("txtElvBankCode");
    var objBankName = document.getElementById("txtElvBankName");


    var bSuccess = 1;
    var errorMessage = "<br/>";

    if (objNameOnCard.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_82 + "<br/>"; //"- Please supply a valid Name.<br/>";
        bSuccess = 0;
    }
    if (objBankCode.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_83 + "<br/>"; //"- Please supply a valid Bank code.<br/>";
        bSuccess = 0;
    }
    if (objBankName.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_84 + "<br/>"; //"- Please supply a valid Bank name.<br/>";
        bSuccess = 0;
    }
    if (objAccNumber.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_85 + "<br/>"; //"- Please supply a valid Account Number.<br/>";
        bSuccess = 0;
    }

    if (bSuccess == 1) {
        var objCondition = document.getElementById("chkElvCondition");

        if (objCondition.checked == true) {
            ShowLoadBar(true);
            var xmlStr = "<payment>";

            xmlStr += "<form_of_payment_subtype_rcd>EFT</form_of_payment_subtype_rcd>";
            xmlStr += "<form_of_payment_rcd>EFT</form_of_payment_rcd>";
            xmlStr += "<cvv_required_flag>0</cvv_required_flag>";
            xmlStr += "<display_cvv_flag>0</display_cvv_flag>";
            xmlStr += "<display_expiry_date_flag>0</display_expiry_date_flag>";
            xmlStr += "<expiry_date_required_flag>0</expiry_date_required_flag>";
            xmlStr += "<address_required_flag>0</address_required_flag>";
            xmlStr += "<display_address_flag>1</display_address_flag>";
            xmlStr += "<display_issue_date_flag>0</display_issue_date_flag>";
            xmlStr += "<display_issue_number_flag>0</display_issue_number_flag>";

            xmlStr += "<NameOnCard>" + objNameOnCard.value + "</NameOnCard>";
            xmlStr += "<CreditCardNumber>" + objAccNumber.value + "</CreditCardNumber>";
            xmlStr += "<CCDisplayName></CCDisplayName>";

            xmlStr += "<IssueMonth></IssueMonth>";
            xmlStr += "<IssueYear></IssueYear>";
            xmlStr += "<IssueNumber></IssueNumber>";

            xmlStr += "<ExpiryMonth></ExpiryMonth>";
            xmlStr += "<ExpiryYear></ExpiryYear>";

            xmlStr += "<Addr1></Addr1>";
            xmlStr += "<Addr2></Addr2>";
            xmlStr += "<Street></Street>";
            xmlStr += "<State></State>";
            xmlStr += "<City></City>";
            xmlStr += "<County></County>";
            xmlStr += "<ZipCode></ZipCode>";
            xmlStr += "<Country>" + objCountry.options[objCountry.selectedIndex].value + "</Country>";
            xmlStr += "<CVV></CVV>";
            xmlStr += "<fee_amount_incl>0</fee_amount_incl>";
            xmlStr += "<fee_amount>0</fee_amount>";
            xmlStr += "<bank_name>" + objBankName.value + "</bank_name>";
            xmlStr += "<bank_code>" + objBankCode.value + "</bank_code>";
            xmlStr += "<bank_iban></bank_iban>";
            xmlStr += "</payment>";

            tikAeroB2C.WebService.B2cService.PaymentCreditCard(xmlStr, SuccessPaymentCreditCard, showErrorPayment, showTimeOutPayment);
            // tikAeroB2C.WebService.B2cService.EqipayPaymentRequest(xmlStr, SuccessProcessEquipayPayment, showErrorPayment, showTimeOutPayment);
            //tikAeroB2C.WebService.B2cService.JccPaymentRequest(xmlStr, SuccessProcessEquipayPayment, showErrorPayment, showTimeOutPayment);                 
        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //passengerdetail.ascx
        }

        objCondition = null;
    }
    else
    { document.getElementById("spnError").innerHTML = errorMessage; }
}
function SuccessProcessTatraPayment(result) {
    var obj = document.getElementsByTagName("body");
    obj[0].innerHTML = result;
    document.payFormCcard.submit();
}
function paymentTatra() {
    var SType = document.getElementById("SType");
    var payType = SType.options[SType.selectedIndex].value;

    var bSuccess = 1;
    var errorMessage = "<br/>";


    if (bSuccess == 1) {
        var objCondition = document.getElementById("chkPayTatra");

        if (objCondition.checked == true) {
            ShowLoadBar(true);
            var xmlStr = "<payment>";
            if (payType == "CardPay") {
                xmlStr += "<form_of_payment_subtype_rcd>Tatra</form_of_payment_subtype_rcd>";
                xmlStr += "<form_of_payment_rcd>CC</form_of_payment_rcd>";
            }
            else {
                xmlStr += "<form_of_payment_subtype_rcd>EFT</form_of_payment_subtype_rcd>";
                xmlStr += "<form_of_payment_rcd>EFT</form_of_payment_rcd>";
            }
            xmlStr += "<cvv_required_flag>0</cvv_required_flag>";
            xmlStr += "<display_cvv_flag>0</display_cvv_flag>";
            xmlStr += "<display_expiry_date_flag>0</display_expiry_date_flag>";
            xmlStr += "<expiry_date_required_flag>0</expiry_date_required_flag>";
            xmlStr += "<address_required_flag>0</address_required_flag>";
            xmlStr += "<display_address_flag>1</display_address_flag>";
            xmlStr += "<display_issue_date_flag>0</display_issue_date_flag>";
            xmlStr += "<display_issue_number_flag>0</display_issue_number_flag>";
            xmlStr += "<NameOnCard>Tatra</NameOnCard>";
            xmlStr += "<CreditCardNumber>0000000000000000</CreditCardNumber>";
            xmlStr += "<CCDisplayName></CCDisplayName>";
            xmlStr += "<IssueMonth></IssueMonth>";
            xmlStr += "<IssueYear></IssueYear>";
            xmlStr += "<IssueNumber></IssueNumber>";
            xmlStr += "<ExpiryMonth></ExpiryMonth>";
            xmlStr += "<ExpiryYear></ExpiryYear>";
            xmlStr += "<Addr1></Addr1>";
            xmlStr += "<Addr2></Addr2>";
            xmlStr += "<Street></Street>";
            xmlStr += "<State></State>";
            xmlStr += "<City></City>";
            xmlStr += "<County></County>";
            xmlStr += "<ZipCode></ZipCode>";
            xmlStr += "<Country></Country>";
            xmlStr += "<CVV></CVV>";
            xmlStr += "<fee_amount_incl>0</fee_amount_incl>";
            xmlStr += "<fee_amount>0</fee_amount>";
            xmlStr += "<bank_name></bank_name>";
            xmlStr += "<bank_code></bank_code>";
            xmlStr += "<bank_iban></bank_iban>";
            xmlStr += "</payment>";


            tikAeroB2C.WebService.B2cService.TatraPaymentRequest(xmlStr, SuccessProcessTatraPayment, showErrorPayment, showTimeOutPayment);

        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //passengerdetail.ascx
        }

        objCondition = null;
    }
    else
    { document.getElementById("spnError").innerHTML = errorMessage; }
}
function SetPaymentContent() {

    //Show default first tab.
    showhidelayer('CreditCard');

    var objCC = document.getElementById("dvcTCreditCard");
    if (objCC != null && objCC.className == "CreditCard") {
        activateCreditCardControl();
    }

    //Check For Redeem payment
    //  if redeem with fee show form of payment
    //  if only redeem no payment show only redeem tab
    if (document.getElementById("dvPointTotal") != null &&
       parseFloat(0 + document.getElementById("dvFareTotal").innerHTML) > 0 &&
       parseInt(0 + document.getElementById("dvPointTotal").innerHTML) > 0) {
        HideAllPaymentTab(true);
        document.getElementById("dvRedeemTab").style.display = "none";
        document.getElementById("dvCCTab").style.display = "block";

        activateCreditCardControl();
        showhidelayer('CreditCard');
    }
    else if (document.getElementById("dvPointTotal") != null &&
             parseFloat(0 + document.getElementById("dvFareTotal").innerHTML) == 0 &&
             parseInt(0 + document.getElementById("dvPointTotal").innerHTML) > 0) {
        HideAllPaymentTab(true);
        document.getElementById("dvRedeemTab").style.display = "block";
        document.getElementById("dvReedeem").style.display = "block";
    }
    else {
        if (document.getElementById("dvRedeemTab") != null) {
            document.getElementById("dvRedeemTab").style.display = "none";
            GetAccuralQuote();
        }
    }
    
    //Hide Ffp information if not client.
    GetAccuralQuote();

    //Check EFT Tab();
    ShowPaymentFfpInfo();
    //Clear Passenger title to show only MR ans MRS
    var objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    if (objTitle != null) {
        for (var i = 0; i < objTitle.options.length; i++) {
            if (objTitle.options[i].value != "MR|M" & objTitle.options[i].value != "MRS|F") {
                $("#" + FindControlName("select", "stContactTitle") + " option[value='" + objTitle.options[i].value + "']").remove();
                i = i - 1;
            }
        }
    }
    
    //Initialize Total Additonal collapse
    $(function () {
        $("#ulAdditionalFee").jqcollapse({
            slide: true,
            speed: 400,
            easing: ''
        });
    });

    //Set Expiry date
    FilledCCYear("optExpiredYear");
    scroll(0, 0);
}
function SavePostfinance(paymentType) {
    var objCondition = "";
    if (paymentType == "PF") {
        objCondition = document.getElementById("chkPostfinance");
    } else {
        objCondition = document.getElementById("chkPostfinanceCard");
    }
    if (objCondition.checked == true) {
        ShowLoadBar(true);

        var exchange_fee_amount = "";

        if (document.getElementById("spRate") != null) {
            exchange_fee_amount = document.getElementById("spRate").innerHTML;
        }

        var xmlStr = "<payment>";

        var xmlStr = "<payment>";
        xmlStr += "<form_of_payment_subtype_rcd>EFT</form_of_payment_subtype_rcd>";
        xmlStr += "<form_of_payment_rcd>EFT</form_of_payment_rcd>";
        xmlStr += "<cvv_required_flag>0</cvv_required_flag>";
        xmlStr += "<display_cvv_flag>0</display_cvv_flag>";
        xmlStr += "<display_expiry_date_flag>0</display_expiry_date_flag>";
        xmlStr += "<expiry_date_required_flag>0</expiry_date_required_flag>";
        xmlStr += "<address_required_flag>0</address_required_flag>";
        xmlStr += "<display_address_flag>1</display_address_flag>";
        xmlStr += "<display_issue_date_flag>0</display_issue_date_flag>";
        xmlStr += "<display_issue_number_flag>0</display_issue_number_flag>";
        xmlStr += "<NameOnCard>Post Finance</NameOnCard>";
        xmlStr += "<CreditCardNumber>0000000000000000</CreditCardNumber>";
        xmlStr += "<CCDisplayName></CCDisplayName>";
        xmlStr += "<IssueMonth></IssueMonth>";
        xmlStr += "<IssueYear></IssueYear>";
        xmlStr += "<IssueNumber></IssueNumber>";
        xmlStr += "<ExpiryMonth></ExpiryMonth>";
        xmlStr += "<ExpiryYear></ExpiryYear>";
        xmlStr += "<Addr1></Addr1>";
        xmlStr += "<Addr2></Addr2>";
        xmlStr += "<Street></Street>";
        xmlStr += "<State></State>";
        xmlStr += "<City></City>";
        xmlStr += "<County></County>";
        xmlStr += "<ZipCode></ZipCode>";
        xmlStr += "<Country></Country>";
        xmlStr += "<CVV></CVV>";
        xmlStr += "<fee_amount_incl>0</fee_amount_incl>";
        xmlStr += "<fee_amount>0</fee_amount>";
        //xmlStr += "<exchange_fee_amount>" + exchange_fee_amount + "</exchange_fee_amount>";
        //xmlStr += "<exchange_currency></exchange_currency>";		
        xmlStr += "<exchange_fee_amount></exchange_fee_amount>";
        xmlStr += "<exchange_currency></exchange_currency>";
        xmlStr += "</payment>";

        setGoogleAdCookie();

        var qrs = "";
        qrs += "&web=" + queryStringValue("web");
        qrs += "&langculture=" + queryStringValue("langculture");

        tikAeroB2C.WebService.B2cService.OgoneECommercePaymentRequest(xmlStr, qrs, paymentType, SuccessOgoneECommercePayment, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
    }
    objCondition = null;
}

function PaymentMultipleForm() {

    if (VoucherSelect() == true) {
        var objCreditCard = document.getElementById("dvMultiFormCCPayment");
        if (EnoughVoucher() == true || (objCreditCard != null && (objCreditCard.style.display == "block" || objCreditCard.style.display == ""))) {
            var objVoucher = document.getElementsByName("nVoucher");
            var objVoucherError = document.getElementById("spnVoucherError");


            var strXml = "";
            var strCCXml = "";
            var arrVcValue;
            var bCCPass = false;

            //Check if credit card information is complete and successfully validate.
            if (objCreditCard != null && (objCreditCard.style.display == "block" || objCreditCard.style.display == "")) {

                var objNameOnCard = document.getElementById("txtMultiNameOnCard");
                var objCardType = document.getElementById(FindControlName("select", "optMultiCardType"));
                var objIssueNumber = document.getElementById("txtMultiIssueNumber");
                var objIssueMonth = document.getElementById("optMultiIssueMonth");
                var objIssueYear = document.getElementById("optMultiIssueYear");
                var objCardNumber = document.getElementById("txtMultiCardNumber");
                var objExpiredMonth = document.getElementById("optMultiExpiredMonth");
                var objExpiredYear = document.getElementById("optMultiExpiredYear");
                var objCvv = document.getElementById("txtMultiCvv");
                var objAddress1 = document.getElementById("txtMultiAddress1");
                var objAddress2 = document.getElementById("txtMultiAddress2");
                var objCity = document.getElementById("txtMultiCity");
                var objCounty = document.getElementById("txtMultiCounty");
                var objPostCode = document.getElementById("txtMultiPostCode");
                var objCountry = document.getElementById(FindControlName("select", "optMultiCountry"));
                var objCondition = document.getElementById("chkMultiPayCreditCard");

                strCCXml = ValidateCreditCard(objNameOnCard,
                                        objCardType,
                                        objIssueNumber,
                                        objIssueMonth,
                                        objIssueYear,
                                        objCardNumber,
                                        objExpiredMonth,
                                        objExpiredYear,
                                        objCvv,
                                        objAddress1,
                                        objAddress2,
                                        objCity,
                                        objCounty,
                                        objPostCode,
                                        objCountry,
                                        objCondition);

                if (strCCXml.length > 0) {
                    bCCPass = true;
                }
            }
            else {
                bCCPass = true;
            }

            if (objVoucher != null && bCCPass == true) {
                if (objVoucher.length > 0) {
                    var objCondition = document.getElementById("chkPayVoucher");
                    if (objCondition.checked == true) {
                        strXml = "<payments>";
                        for (var iCount = 0; iCount < objVoucher.length; iCount++) {
                            if (objVoucher[iCount].checked == true) {

                                if (objVoucher[iCount].value.length > 0) {

                                    arrVcValue = objVoucher[iCount].value.split("|");
                                    strXml = strXml + "<payment>" +
                                                  "<voucher_id>" + arrVcValue[0] + "</voucher_id>" +
                                                  "<voucher_number>" + arrVcValue[1] + "</voucher_number>" +
                                                  "<form_of_payment_rcd>" + arrVcValue[2] + "</form_of_payment_rcd>" +
                                                  "<form_of_payment_subtype_rcd>" + arrVcValue[3] + "</form_of_payment_subtype_rcd>" +
                                              "</payment>";
                                }

                            }
                        }

                        //Credit Card xml.
                        strXml = strXml + strCCXml;

                        strXml = strXml + "</payments>";

                        CloseDialog();
                        ShowLoadBar(true);
                        //Process payment voucher.
                        tikAeroB2C.WebService.B2cService.PaymentMultipleForm(strXml, SuccessPaymentMultipleForm, showError, showTimeOut);
                    }
                    else {
                        ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
                    }
                    objCondition = null;
                }
                else {
                    objVoucherError.innerHTML = objLanguage.Alert_Message_76; //"- Please select voucher.";
                }

            }

            objVoucherError = null;
            objVoucher = null;
        }
        else {
            //Voucher amount is not enough. Show credit card form.
            LoadFormCreditCard();
        }
    }
    else {
        ShowMessageBox(objLanguage.Alert_Message_76, 0, '');
     }
    
}
function SuccessPaymentMultipleForm(result) {
    var obj;

    if (result.length > 0) {
        if (result == "{002}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else if (result == "{703}") {
            ShowMessageBox(objLanguage.Alert_Message_37, 0, ''); //"Voucher amount not enough !"
            //Show Credit card section here.
        }
        else {
            if (result.split("{}").length == 1) {
                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");
            }
            else {
                obj = document.getElementById("spnVoucherError");
                if (result.split("{}")[1] == "200") {
                    //Infant over limit
                    obj.innerHTML = objLanguage.Alert_Message_59;
                }
                else {
                    obj.innerHTML = result.split("{}")[1];
                }


            }
            ShowLoadBar(false);
        }

        obj = null;
     }
    else {
        ShowLoadBar(false);
    }
}

function ValidateCreditCard(objNameOnCard,
                            objCardType,
                            objIssueNumber,
                            objIssueMonth,
                            objIssueYear,
                            objCardNumber,
                            objExpiredMonth,
                            objExpiredYear,
                            objCvv,
                            objAddress1,
                            objAddress2,
                            objCity,
                            objCounty,
                            objPostCode,
                            objCountry,
                            objCondition) {

    ccErrors[0] = objLanguage.Alert_Message_72; //"Unknown card type";
    ccErrors[1] = objLanguage.Alert_Message_73; //"No card number provided";
    ccErrors[2] = objLanguage.Alert_Message_39; //"Please supply a valid Credit Card Number.";
    ccErrors[3] = objLanguage.Alert_Message_74; //"Credit Card type mismatch.";
    ccErrors[4] = objLanguage.Alert_Message_74; //"Credit Card type mismatch.";

    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objPaymentText = objCardType.options[objCardType.selectedIndex].text;

    var expMonth = objExpiredMonth.options[objExpiredMonth.selectedIndex].value;
    var expYear = objExpiredYear.options[objExpiredYear.selectedIndex].value;

    // Insurance ID
    var hdInsuranceID = "";
    if (document.getElementById("hdProductID") != null)
        hdInsuranceID = document.getElementById("hdProductID").value;

    var bSuccess = 1;
    var errorMessage = "<br/>";
    var bCheckSum = false;
    var exchange_fee_amount = "";

    if (objPaymentValue.split("{}")[12] == "1")
    { bCheckSum = true; }

    if (expMonth.length == 1)
    { expMonth = '0' + expMonth; }

    if (GetControlValue(objNameOnCard).length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_38 + "<br/>"; //"- Please supply a valid Name On Card.<br/>";
        bSuccess = 0;
    }
    if (checkCreditCard(GetControlValue(objCardNumber), objPaymentValue.split("{}")[0], bCheckSum) == false) {
        errorMessage = errorMessage + "- " + ccErrors[ccErrorNo] + "<br/>";
        bSuccess = 0;
    }
    if (parseInt(expYear + expMonth) <= parseInt(today.getFullYear().toString() + leadingZero(today.getMonth()).toString())) {
        if (objPaymentValue.split("{}")[5] = 1) {
            errorMessage = errorMessage + objLanguage.Alert_Message_46 + "<br/>"; //"- Invalid Expired Date.<br/>";
            bSuccess = 0;
        }
    }

    if (objPaymentValue.split("{}")[2] == 1 && GetControlValue(objCvv).length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_45 + "<br/>"; //"- Invalid CCV code.<br/>";
        bSuccess = 0;
    }
    if (objPaymentValue.split("{}")[6] == 1) {
        if (GetControlValue(objAddress1).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_41 + "<br/>"; //"- Please supply a valid Address 1.<br/>";
            bSuccess = 0;
        }
        if (GetControlValue(objCity).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_42 + "<br/>"; //"- Please supply a valid Town/City.<br/>";
            bSuccess = 0;
        }
        if (GetControlValue(objPostCode).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_43 + "<br/>"; //"- Please supply a valid Postal Code.<br/>";
            bSuccess = 0;
        }
    }

    if (document.getElementById("spRate") != null) {
        exchange_fee_amount = document.getElementById("spRate").innerHTML;
    }

    if (bSuccess == 1) {
        

        if (objCondition.checked == true) {
            var xmlStr = "<payment>";

            xmlStr += "<form_of_payment_subtype_rcd>" + objPaymentValue.split("{}")[0] + "</form_of_payment_subtype_rcd>";
            xmlStr += "<form_of_payment_rcd>" + objPaymentValue.split("{}")[1] + "</form_of_payment_rcd>";
            xmlStr += "<cvv_required_flag>" + objPaymentValue.split("{}")[2] + "</cvv_required_flag>";
            xmlStr += "<display_cvv_flag>" + objPaymentValue.split("{}")[3] + "</display_cvv_flag>";
            xmlStr += "<display_expiry_date_flag>" + objPaymentValue.split("{}")[4] + "</display_expiry_date_flag>";
            xmlStr += "<expiry_date_required_flag>" + objPaymentValue.split("{}")[5] + "</expiry_date_required_flag>";
            xmlStr += "<address_required_flag>" + objPaymentValue.split("{}")[6] + "</address_required_flag>";
            xmlStr += "<display_address_flag>" + objPaymentValue.split("{}")[7] + "</display_address_flag>";
            xmlStr += "<display_issue_date_flag>" + objPaymentValue.split("{}")[8] + "</display_issue_date_flag>";
            xmlStr += "<display_issue_number_flag>" + objPaymentValue.split("{}")[9] + "</display_issue_number_flag>";

            xmlStr += "<NameOnCard>" + ConvertToValidXmlData(GetControlValue(objNameOnCard)) + "</NameOnCard>";
            xmlStr += "<CreditCardNumber>" + GetControlValue(objCardNumber) + "</CreditCardNumber>";
            xmlStr += "<CCDisplayName>" + ConvertToValidXmlData(objPaymentText) + "</CCDisplayName>";

            xmlStr += "<IssueMonth>" + objIssueMonth.options[objIssueMonth.selectedIndex].value + "</IssueMonth>";
            xmlStr += "<IssueYear>" + objIssueYear.options[objIssueYear.selectedIndex].value + "</IssueYear>";
            xmlStr += "<IssueNumber>" + GetControlValue(objIssueNumber) + "</IssueNumber>";

            xmlStr += "<ExpiryMonth>" + objExpiredMonth.options[objExpiredMonth.selectedIndex].value + "</ExpiryMonth>";
            xmlStr += "<ExpiryYear>" + objExpiredYear.options[objExpiredYear.selectedIndex].value + "</ExpiryYear>";

            xmlStr += "<Addr1>" + ConvertToValidXmlData(GetControlValue(objAddress1)) + "</Addr1>";
            xmlStr += "<Addr2></Addr2>";
            xmlStr += "<Street>" + ConvertToValidXmlData(GetControlValue(objAddress2)) + "</Street>";
            xmlStr += "<State>" + ConvertToValidXmlData(GetControlValue(objCounty)) + "</State>";
            xmlStr += "<City>" + ConvertToValidXmlData(GetControlValue(objCity)) + "</City>";
            xmlStr += "<County>" + ConvertToValidXmlData(GetControlValue(objCounty)) + "</County>";
            xmlStr += "<ZipCode>" + ConvertToValidXmlData(GetControlValue(objPostCode)) + "</ZipCode>";
            xmlStr += "<Country>" + objCountry.options[objCountry.selectedIndex].value + "</Country>";
            xmlStr += "<CVV>" + ConvertToValidXmlData(GetControlValue(objCvv)) + "</CVV>";
            xmlStr += "<fee_amount_incl>" + objPaymentValue.split("{}")[10] + "</fee_amount_incl>";
            xmlStr += "<fee_amount>" + objPaymentValue.split("{}")[11] + "</fee_amount>";
            xmlStr += "<exchange_fee_amount>" + exchange_fee_amount + "</exchange_fee_amount>";
            xmlStr += "<exchange_currency></exchange_currency>";
            xmlStr += "<InsuranceID>" + hdInsuranceID + "</InsuranceID>";

            xmlStr += "</payment>";

            return xmlStr; 
        }
        else {
            //Please accept Terms and Conditions.
            ShowMessageBox(objLanguage.Alert_Message_49, 0, '');
            return "";
        }

        objCondition = null;
    }
    else {
        ShowMessageBox(errorMessage, 0, '');
        return "";
    }
}


function CalculateTotalMultiplePayment() {
    var objMultiplePaymentMsg = document.getElementById("dvMultipleConfirm");
    if (EnoughVoucher() == false) {
        objMultiplePaymentMsg.style.display = "block";   
     }
    else {
        objMultiplePaymentMsg.style.display = "none";
    }
}
function EnoughVoucher() {
    var objChk = document.getElementsByName("nVoucher");
    var objHdTotalVC = document.getElementsByName("hdVcAmount");
    var objHdSubTotal = document.getElementById("hdSubTotal");
    
    var dblTotal = 0;
    if (objChk != null && objChk.length > 0) {
        for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].checked == true) {
                dblTotal = dblTotal + parseFloat(objHdTotalVC[i].value);
            }
        }
    }
    if (dblTotal < parseFloat(objHdSubTotal.value)) {
        return false;
    }
    else {
        return true;
    }
}
function LoadFormCreditCard() {
    var objChk = document.getElementsByName("nVoucher");
    var strXml = "";

    ShowProgressBar(true);
    strXml += "<vouchers>";
    for (var i = 0; i < objChk.length; i++) {
        if (objChk[i].checked == true) {
            var arr = objChk[i].value.split("|");
            if (arr.length == 4) {
                strXml += "<voucher>";

                strXml += "<voucher_id>" + arr[0] + "</voucher_id>";
                strXml += "<voucher_number>" + arr[1] + "</voucher_number>";

                strXml += "</voucher>";        
            }
        }
    }
    strXml += "</vouchers>";
    tikAeroB2C.WebService.B2cService.LoadFormCreditCard(strXml, SuccessLoadFormCreditCard, showError, showTimeOut);
}
function SuccessLoadFormCreditCard(result) {

    ShowProgressBar(false);
    if (result.length > 0) {
        var objHolder = document.getElementById("dvFormHolder");
        var objContainer = document.getElementById("dvProgressBar");

        //Insert passenger form content.
        objHolder.innerHTML = result;

        //Show Fading.
        objContainer.style.display = "block";
        objHolder.style.display = "block";

        ClearErrorMsg();
        objHolder = null;
        objContainer = null;

        //Detect keyboard key for Credit Card number.
        $("#txtMultiCardNumber").keyup(function (e) {
            var objCardNumber = document.getElementById("txtMultiCardNumber");
            var objCreditCardFee = document.getElementById("dvMultiCCFee");

            if (objCardNumber.value.length == 6) {
                if (e.keyCode == 8) {
                    ValidateMultiCC(objCardNumber.value, false);
                }
                else {
                    ValidateMultiCC(objCardNumber.value, true);

                }
            }
            else if (objCardNumber.value.length < 6 & e.keyCode == 8) {

                ValidateMultiCC(objCardNumber.value, false);
            }
        });

        //Make voucher information collapse
        $(function () {
            $("#ulMultipleVcSummary").jqcollapse({
                slide: true,
                speed: 300,
                easing: ''
            });
        });

        //Show Credit card filed that base on card type.
        ActivateMultiCCControl();

        //Initialize CC WaterMark.
        InitializeMultiCCWaterMark();

        //Set Expiry date
        FilledCCYear("optMultiExpiredYear");
    }
}
function ActivateMultiCCControl() {
    var objCardType = document.getElementById(FindControlName("select", "optMultiCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objCreditCardFare = document.getElementById("dvMultiAdditionalFee");
    var objFareTotal = document.getElementById("dvMultiFareTotal");
    var objExpiredYear = document.getElementById("optMultiExpiredYear");
    var optExpiredMonth = document.getElementById("optMultiExpiredMonth");

    var objCvv = document.getElementById('dvMultiCvv');
    var objExpiryDate = document.getElementById('dvMultiExpiryDate');
    var objDvAddress = document.getElementById('dvMultiAddress');
    var objDvIssueDate = document.getElementById('dvMultiIssuDate');
    var objIssueNumber = document.getElementById('dvMultiIssueNumber');
    var objCardNumber = document.getElementById("txtMultiCardNumber");

    //Clear Error message.    

    ShowCreditCardField(objCardType,
                        objPaymentValue,
                        objCreditCardFare,
                        objFareTotal,
                        objExpiredYear,
                        optExpiredMonth,
                        objCvv,
                        objExpiryDate,
                        objDvAddress,
                        objDvIssueDate,
                        objIssueNumber,
                        objCardNumber);
}
function ShowCreditCardField(objCardType,
                            objPaymentValue,
                            objCreditCardFare,
                            objFareTotal,
                            objExpiredYear,
                            optExpiredMonth,
                            objCvv,
                            objExpiryDate,
                            objDvAddress,
                            objDvIssueDate,
                            objIssueNumber,
                            objCardNumber) {

    if (objPaymentValue.split("{}")[3] == "1")
    { objCvv.style.display = 'block'; }
    else
    { objCvv.style.display = 'none'; }

    if (objPaymentValue.split("{}")[4] == "1")
    { objExpiryDate.style.display = 'block'; }
    else
    { objExpiryDate.style.display = 'none'; }

    if (objPaymentValue.split("{}")[7] == "1")
    { objDvAddress.style.display = 'block'; }
    else
    { objDvAddress.style.display = 'none'; }

    if (objPaymentValue.split("{}")[8] == "1")
    { objDvIssueDate.style.display = 'block'; }
    else
    { objDvIssueDate.style.display = 'none'; }

    if (objPaymentValue.split("{}")[9] == "1")
    { objIssueNumber.style.display = 'block'; }
    else
    { objIssueNumber.style.display = 'none'; }

    //Clear Card number value
    objCardNumber.value = objLanguage.default_value_2;

    //Set Expiry Year
    if (objExpiredYear.options.length == 0) {
        var y = today.getFullYear();
        var objOption;
        for (var i = 0; i < 20; i++) {
            objOption = new Option(y + i, y + i);
            objExpiredYear.options.add(objOption);
        }
    }
    optExpiredMonth.options[today.getMonth()].selected = true;
}
function InitializeMultiCCWaterMark() {

    //Credit Card
    InitializeWaterMark("txtMultiCardNumber", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiNameOnCard", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiCvv", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiAddress1", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiAddress2", objLanguage.default_value_3, "input");
    InitializeWaterMark("txtMultiCity", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiCounty", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiPostCode", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiIssueNumber", objLanguage.default_value_2, "input");
}
function CalculateCCFee(strCardNumber, 
                        bCalculate,
                        objCardType,
                        objPaymentValue,
                        objDvFareTotal,
                        objTotalAdditinalFee,
                        objCreditCardValue,
                        objCreditCardDetail,
                        dblCreditCardFee,
                        objhdFareTotal) {
    

    if (bCalculate == true) {
        if (checkCreditCard(strCardNumber, objPaymentValue.split("{}")[0], false) == true) {
            if (objTotalAdditinalFee != null) {
                objTotalAdditinalFee.innerHTML = AddCommas((parseFloat(0 + RemoveCommas(dblCreditCardFee))).toFixed(2));
            }

            objCreditCardValue.innerHTML = AddCommas(parseFloat(0 + dblCreditCardFee).toFixed(2));
            objDvFareTotal.innerHTML = AddCommas((parseFloat(0 + GetControlValue(objhdFareTotal)) + parseFloat(0 + RemoveCommas(dblCreditCardFee))).toFixed(2));

            if (objCreditCardDetail != null) {
                if (dblCreditCardFee > 0) {
                    objCreditCardDetail.style.display = "block";
                }
                else {
                    objCreditCardDetail.style.display = "block";
                }
            }
        }
        else {
            
            if (objCreditCardDetail != null) {
                objCreditCardValue.innerHTML = 0;
                objCreditCardDetail.style.display = "block";
            }

            if (parseFloat(0 + GetControlValue(objhdFareTotal)) != parseFloat(0 + RemoveCommas(GetControlInnerHtml(objDvFareTotal)))) {
                if (objTotalAdditinalFee != null) {
                    objTotalAdditinalFee.innerHTML = AddCommas((parseFloat(0 + RemoveCommas(objTotalAdditinalFee.innerHTML)) - parseFloat(0 + RemoveCommas(dblCreditCardFee))).toFixed(2));
                }
                objDvFareTotal.innerHTML = AddCommas(parseFloat(0 + GetControlValue(objhdFareTotal)).toFixed(2));
            }
        }
    }
    else {

        if (objCreditCardDetail != null) {
            objCreditCardValue.innerHTML = 0;
            objCreditCardDetail.style.display = "block";
        }

        if (parseFloat(0 + GetControlValue(objhdFareTotal)) != parseFloat(0 + RemoveCommas(GetControlInnerHtml(objDvFareTotal)))) {
            if (objTotalAdditinalFee != null) {
                objTotalAdditinalFee.innerHTML = AddCommas((parseFloat(0 + RemoveCommas(objTotalAdditinalFee.innerHTML)) - parseFloat(0 + RemoveCommas(dblCreditCardFee))).toFixed(2));
            }
            objDvFareTotal.innerHTML = AddCommas(parseFloat(0 + GetControlValue(objhdFareTotal)).toFixed(2));
        }
    }
}
function ValidateMultiCC(strCardNumber, bCalculate) {
    var objCardType = document.getElementById(FindControlName("select", "optMultiCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objDvFareTotal = document.getElementById("dvTotalAdditional");
    var objCreditCardValue = document.getElementById("dvMultiCreditCardValue");
    var dblCreditCardFee = parseFloat(objPaymentValue.split("{}")[10]).toFixed(2);
    var objhdFareTotal = document.getElementById("hdTotalAdditional");

    var dclFeePercentage = parseFloat(objPaymentValue.split("{}")[13]).toFixed(2);
    var minimumFeeFlag = parseInt(objPaymentValue.split("{}")[14]);
    var Factor = parseInt(objPaymentValue.split("{}")[15]);
    var dclFeeAmount = parseFloat(objPaymentValue.split("{}")[16]).toFixed(2);
    var dclFeeAmountIncl = parseFloat(objPaymentValue.split("{}")[17]).toFixed(2);


    //Find Credit Card Fee
    var dblCreditCardFee = CalculateCreditCardFee(dclFeeAmount,
                                                  dclFeeAmountIncl,
                                                  parseFloat(0 + GetControlValue(objhdFareTotal)).toFixed(2),
                                                  dclFeePercentage,
                                                  Factor,
                                                  minimumFeeFlag);

    CalculateCCFee(strCardNumber,
                    bCalculate,
                    objCardType,
                    objPaymentValue,
                    objDvFareTotal,
                    objCreditCardValue,
                    objCreditCardValue,
                    null,
                    dblCreditCardFee,
                    objhdFareTotal);
}

function LoadMultiMyName() {
    var objMyname = document.getElementById("chkMultiMyname");
    if (objMyname.checked == true) {
        tikAeroB2C.WebService.B2cService.GetMyName(SuccessLoadMultiMyName, showError, showTimeOut);
    }
    else {
        //If check bok not checked clear card holder information.
        SuccessLoadMultiMyName("");
    }
    objMyname = null;
}

function SuccessLoadMultiMyName(result) {
    var objNameOnCard = document.getElementById("txtMultiNameOnCard");
    var objAddress1 = document.getElementById("txtMultiAddress1");
    var objAddress2 = document.getElementById("txtMultiAddress2");
    var objCity = document.getElementById("txtMultiCity");
    var objCounty = document.getElementById("txtMultiCounty");
    var objPostCode = document.getElementById("txtMultiPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optMultiCountry"));

    SetMyName(result,
              objNameOnCard,
              objAddress1,
              objAddress2,
              objCity,
              objCounty,
              objPostCode,
              objCountry);
}
function VoucherSelect() {
    var objChk = document.getElementsByName("nVoucher");

    if (objChk != null && objChk.length > 0) {
        for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].checked == true) {
                return true;
            }
        }
    }
    return false;
}

function CalculateCreditCardFee(feeAmount,
                                feeAmountIncl,
                                saleAmount,
                                feePercentage,
                                Factor,
                                minimumFeeFlag) {

    var dclAmount;
    var dclAmountIncl;
    //Calculate FormOfPaymentFee
    if (feePercentage > 0) {

        var dFee = parseFloat((saleAmount / 100) * feePercentage).toFixed(2);
        if (feeAmountIncl == 0)
        { }
        else if (minimumFeeFlag == 0) {
            if (parseFloat(dFee) > parseFloat(feeAmountIncl)) {
                dFee = feeAmountIncl;
            }
        }
        else {
            if (parseFloat(dFee) < parseFloat(feeAmountIncl)) {
                dFee = feeAmountIncl;
            }
        }
        dclAmount = dFee;
        dclAmountIncl = dFee;
    }
    else {
        dclAmount = feeAmount * Factor;
        dclAmountIncl = feeAmountIncl * Factor;
    }

    return dclAmountIncl;
}

function HideAllPayment() {
    document.getElementById('CreditCard').style.display = 'none';
    document.getElementById('Voucher').style.display = 'none';
    document.getElementById('BookNow').style.display = 'none';
    document.getElementById('External').style.display = 'none';

    //Restore Credit card control when click credit card tab.
    document.getElementById('dvCCButton').style.display = 'none';
    document.getElementById('dvCCConfirm').style.display = 'none';
    document.getElementById('chkPayCreditCard').checked = false;
}

function FilledCCYear(strCtrl) {

    var todayDate = new Date();
    var iCurrentYear = todayDate.getFullYear();
    var objExpYear = document.getElementById(strCtrl);
    for (var i = 0; i < 14; i++) {
        var opt = document.createElement("option");

        opt.value = iCurrentYear;
        if (navigator.appName.indexOf('Microsoft') == 0) {
            opt.innerText = iCurrentYear;
        }
        else {
            opt.text = iCurrentYear;
        }
        objExpYear.appendChild(opt);

        //Increment Year.
        iCurrentYear = iCurrentYear + 1
    }
    SetComboValue(strCtrl, todayDate.getFullYear());
}

function paymentESewa() {
    var objCondition = document.getElementById("chkESewa");

    if (objCondition.checked == true) {
        ShowLoadBar();
        var xmlStr = "<payment>";
        xmlStr += "<form_of_payment_subtype_rcd>" + "ESEWA" + "</form_of_payment_subtype_rcd>"; //objPaymentValue.split("{}")[0]
        xmlStr += "<form_of_payment_rcd>" + "CC" + "</form_of_payment_rcd>"; //objPaymentValue.split("{}")[1]
        xmlStr += "<cvv_required_flag>" + "0" + "</cvv_required_flag>";
        xmlStr += "<display_cvv_flag>" + "0" + "</display_cvv_flag>";
        xmlStr += "<display_expiry_date_flag>" + "0" + "</display_expiry_date_flag>";
        xmlStr += "<expiry_date_required_flag>" + "0" + "</expiry_date_required_flag>";
        xmlStr += "<address_required_flag>" + "0" + "</address_required_flag>";
        xmlStr += "<display_address_flag>" + "0" + "</display_address_flag>";
        xmlStr += "<display_issue_date_flag>" + "0" + "</display_issue_date_flag>";
        xmlStr += "<display_issue_number_flag>" + "0" + "</display_issue_number_flag>";

        xmlStr += "<NameOnCard>" + "ESEWA Card" + "</NameOnCard>";
        xmlStr += "<CreditCardNumber>" + "00000000" + "</CreditCardNumber>";

        xmlStr += "<fee_amount_incl>" + "0" + "</fee_amount_incl>"; //objPaymentValue.split("{}")[10]
        xmlStr += "<fee_amount>" + "0" + "</fee_amount>"; //objPaymentValue.split("{}")[11]
        xmlStr += "</payment>";
        tikAeroB2C.WebService.B2cService.ESewaPaymentRequest(xmlStr, SuccessProcessTatraPayment, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowMessageBox("Please accept Terms and Conditions !", 0, '');
    }
}