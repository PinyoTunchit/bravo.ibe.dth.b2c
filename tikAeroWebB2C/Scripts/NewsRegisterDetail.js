function GetNewsRegisterDetail()
{
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetNewsRegisterDetail(SuccessGetNewsRegisterDetail, showError, showTimeOut);
}

function SuccessGetNewsRegisterDetail(result)
{
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    
    scroll(0,0);
    
    ShowPannel(false);
    ShowProgressBar(false);
    obj = null;
}

function GetNewsAdmin()
{
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetNewsAdmin(SuccessGetNewsAdmin, showError, showTimeOut);
}

function SuccessGetNewsAdmin(result)
{
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    
    scroll(0,0);
    
    ShowProgressBar(false);
    obj = null;
}

function SearchNewsRegister(page)
{
    var strResult = SearchNewsRegisterCondition();
    if(strResult.length > 0)
    {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SearchNewsRegister(strResult, page, SuccessSearchNewsRegister, showError, showTimeOut);
    }
}

function SuccessSearchNewsRegister(result)
{   
    var obj = document.getElementById(FindControlName("div", "dvSearchResult"));//document.getElementById("dvContainer"); 
        obj.innerHTML = result;

    scroll(0, 0);
    
    ShowProgressBar(false);
    obj = null;
}

function SearchNewsRegisterCondition()
{    
    var strCountry ="";
    var objCountry = document.getElementsByName("chkCountry");         
    for(var i=0;i<objCountry.length;i++)
    {
        if(objCountry[i].checked == true)
        {
            if(strCountry !="")
            {
              strCountry = strCountry + " or "; 
            } 
            strCountry = strCountry + "(Country_name like '%" + objCountry[i].value + "%')";    
        }
    }  
    
    var strDeparture ="";
    var objDeparture = document.getElementsByName("chkListDeparture");          
    for(var i=0;i<objDeparture.length;i++)
    {
        if(objDeparture[i].checked == true)
        {
            if(strDeparture !="")
            {
              strDeparture = strDeparture + " or "; 
            } 
            strDeparture = strDeparture + "(Departure_name like '%" + objDeparture[i].value + "%')"; 
        }
    }
    
    var strDestination ="";
    var objDestination = document.getElementsByName("chkListDestination");
    for(var i=0;i<objDestination.length;i++)
    {
        if(objDestination[i].checked == true)
        {
            if(strDestination !="")
            {
              strDestination = strDestination + " or "; 
            } 
            strDestination = strDestination + "(Destination_name like '%" + objDestination[i].value + "%')"; 
        }
    }
    
    var travelfor = "";
    var objTravelfor = document.getElementsByName("rdbTravelfor");
    for(var i=0;i<objTravelfor.length;i++)
    {
        if(objTravelfor[i].checked == true)
        {
            travelfor = " Travel_for = '" + objTravelfor[i].value + "' ";
            i = objTravelfor.length;    
        }
    }
    
    var havechildren = "";
    var objHaschild = document.getElementsByName("rdbHaschild");
    for(var i=0;i<objHaschild.length;i++)
    {
        if(objHaschild[i].checked == true)
        {
            havechildren = " Have_childern ='" + objHaschild[i].value  + "' ";
            i = objHaschild.length;    
        }
    }
    
    var strEmail = "";
    var objEmail = document.getElementById(FindControlName("input", "txtEmail"));
    if(objEmail.value.length != 0)
    {
        strEmail = " (Register_email like '%" + objEmail.value + "%') ";
    }
    
    var strEmailFlag = "";
    var objEmailFlag = document.getElementById(FindControlName("select", "ddlStatus"));
    if(objEmailFlag.value != "-1")
    {
        if(objEmailFlag.value == "1")
        {
            strEmailFlag = "(email_flag ='1')";
        }
        else
        {
            strEmailFlag = "(email_flag is null)";
        }
    }
    
    var strDateRenge = "";
    var objDateRegis = document.getElementById("chkDateRegis");    
    if(objDateRegis.checked == true)
    {
        strDateRenge = " Register_date between  #" + NewsRegisterGetDate('3') + "# and #" + NewsRegisterGetDate('4') + "# ";
    }
    
////////////////////////////////////////  end of 11111111111        
    var strCondition = "";
////////////////////////////////////////  start of 2222222222   
    if(strCountry !="") 
    {
      strCondition = strCondition + " and (" + strCountry + ")";  
    }  
    if(strDeparture !="") 
    {
      strCondition = strCondition + " and (" + strDeparture + ")";  
    }
    if(strDestination !="") 
    {
      strCondition = strCondition + " and (" + strDestination + ")";  
    }
    if(travelfor !="") 
    {
      strCondition = strCondition + " and (" + travelfor + ")";  
    }
    if(havechildren !="") 
    {
      strCondition = strCondition + " and (" + havechildren + ")"; 
    }
    if(strEmail !="") 
    {
      strCondition = strCondition + " and (" + strEmail + ")";  
    }
    if(strEmailFlag !="") 
    {
      strCondition = strCondition + " and (" + strEmailFlag + ")";  
    }
    if(strDateRenge !="")
    {
      strCondition = strCondition + " and (" + strDateRenge + ")";
    }
    
////////////////////////////////////////  end of 22222222222222 
    if(strCondition !="")
    {
        strCondition = " where 1=1 " + strCondition;
    }
////////////////////////////////////////  start of 33333333 
    
    objCountry = null;
    objDeparture = null;   
    objDestination = null;
    objTravelfor = null;
    objHaschild = null;
    objEmail = null;      

    return strCondition;
}

function NewsRegisterGetDate(o)
{ 
    var ddlMY=document.getElementById('ddlMY_'+o);
    var ddlDate=document.getElementById('ddlDate_'+o);
    return ddlMY.options[ddlMY.selectedIndex].value.substring(0,4) + '-' + ddlMY.options[ddlMY.selectedIndex].value.substring(4) + '-' +ddlDate.options[ddlDate.selectedIndex].value;
}

function validDepartureAll(flagchk){ 
	var items = document.getElementsByTagName("INPUT");
	for(var i=0;i<items.length;i++ ){
		if (items[i].id.indexOf('chkListDeparture_') == 0){
			items[i].checked = flagchk;
		}
	}
}

function validDestinationsAll(flagchk){ 
	var items = document.getElementsByTagName("INPUT");
	for(var i=0;i<items.length;i++ ){
		if (items[i].id.indexOf('chkListDestination_') == 0){
			items[i].checked = flagchk;
		}
	}
}

function validCountryAll(flagchk){ 
	var items = document.getElementsByTagName("INPUT");
	for(var i=0;i<items.length;i++ ){
		if (items[i].id.indexOf('chkCountry_') == 0){
			items[i].checked = flagchk;
		}
	}
}

function validSearchAll(flagchk){ 
	var items = document.getElementsByTagName("INPUT");
	for(var i=0;i<items.length;i++ ){
		if (items[i].id.indexOf('chkSearchAll_') == 0){
			items[i].checked = flagchk;
		}
	}
}
//* End Yua Section

function ShowNewsRegisterInfoError(strMessage)
{
    var objError = document.getElementById("dvPaxError");
    objError.innerHTML = strMessage;
    objError = null;
}

function SaveNewsRegisterDetail()
{   
    var strResult = FillNewsRegisterDetail(); //Validate and fill data.
    
    if(strResult.length > 0)
    {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.FillNewsRegisterData(strResult, SuccessSaveNewsRegisterDetail, showError, showTimeOut);
    }
}

function SuccessSaveNewsRegisterDetail(result)
{
    var objDivInput = document.getElementById("dvInput");
    var objDivResultComplete = document.getElementById("dvResultComplete"); 
    
    document.getElementById("submitXML").innerHTML = "";
    if(result=="complete")
    {
      objDivInput.style.display = "none";
      objDivResultComplete.style.display = "block";
    }
    else
    {
      document.getElementById("submitXML").innerHTML= result; 
    }
    
    scroll(0,0);

    ShowProgressBar(false);
    objDivInput = null;
    objDivResultComplete = null;    
}

function FillNewsRegisterDetail()
{
    var bPass = true;
    
    document.getElementById("spErrName").innerHTML = "*";
    document.getElementById("spErrEmail").innerHTML = "*";
    document.getElementById("spErrPostcode").innerHTML = "*";
    document.getElementById("spErrCountry").innerHTML = "*";
    document.getElementById("spErrDeparture").innerHTML = "";
    document.getElementById("spErrDestination").innerHTML = "";
    
    var objName = document.getElementById(FindControlName("input", "txtName")); 
    var objEmail = document.getElementById(FindControlName("input", "txtEmail"));
    var objPostcode = document.getElementById(FindControlName("input", "txtPostcode"));
    var objCountry = document.getElementById(FindControlName("select", "ddlCountry"));
    
    var objIPaddress = document.getElementById(FindControlName("input", "txtIPaddress"));
    var objBrowser = document.getElementById(FindControlName("input", "txtBrowser"));
    
    var strDeparture ="";
    var objDeparture = document.getElementsByName("chkListDeparture");  
        
    for(var i=0;i<objDeparture.length;i++)
    {
        if(objDeparture[i].checked == true)
        {
            if(strDeparture !="")
            {
              strDeparture = strDeparture + ","; 
            } 
            strDeparture = strDeparture + objDeparture[i].value;    
        }
    }
    
    var strDestination ="";
    var objDestination = document.getElementsByName("chkListDestination");
    for(var i=0;i<objDestination.length;i++)
    {
        if(objDestination[i].checked == true)
        {
            if(strDestination !="")
            {
              strDestination = strDestination + ","; 
            } 
            strDestination = strDestination + objDestination[i].value;    
        }
    }
    
    var travelfor = "";
    var objTravelfor = document.getElementsByName("rdbTravelfor");
    for(var i=0;i<objTravelfor.length;i++)
    {
        if(objTravelfor[i].checked == true)
        {
            travelfor = objTravelfor[i].value;
            i = objTravelfor.length;    
        }
    }
    
    var havechildren = "";
    var objHaschild = document.getElementsByName("rdbHaschild");
    for(var i=0;i<objHaschild.length;i++)
    {
        if(objHaschild[i].checked == true)
        {
            havechildren = objHaschild[i].value;
            i = objHaschild.length;    
        }
    }
    //objDestination[1].checked = true; //yua
            
    var strXml = "";
    var strError = "";   

    //Add Contact detail information to xml.
    
    if(objName.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrName").innerHTML = "* Name required. !!";
    }
    if(objEmail.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrEmail").innerHTML = "* Email required. !!";
    }
    else if(ValidEmail(objEmail.value) == false)
    {
        bPass = false;
        document.getElementById("spErrEmail").innerHTML = "* Supply valid Email address. !!";   
    }        
    if(objPostcode.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrPostcode").innerHTML = "* Post code required. !!";
    }
    if (objCountry.value == "-1")
    {
        bPass = false;
        document.getElementById("spErrCountry").innerHTML = "* Please select your country. !!";
    }				
    if(strDeparture == "")
    {
        bPass = false;
        document.getElementById("spErrDeparture").innerHTML = "* Select at least one departure point. !!";
    }    
    if(strDestination == "")
    {
        bPass = false;
        document.getElementById("spErrDestination").innerHTML = "* Select at least one destination for receiving news & offers. !!";
    }
    
    if(bPass == true)
    {
      strXml = "<register>" +
                    "<contact>" +
                        "<name>" + objName.value + "</name>" +
                        "<email>" + objEmail.value + "</email>" +
                        "<postcode>" + objPostcode.value + "</postcode>" +
                        "<country>" + objCountry.options[objCountry.selectedIndex].text + "</country>" +
                        "<departure>" + strDeparture + "</departure>" +
                        "<destination>" + strDestination + "</destination>" +  
                        "<travelfor>" + travelfor + "</travelfor>" +   
                        "<havechildren>" + havechildren + "</havechildren>" + 
                        "<ipaddress>" + objIPaddress.value + "</ipaddress>" + 
                        "<browser>" + objBrowser.value + "</browser>" +                                           
                    "</contact>" +
               "</register>";
    }
    else
    {
        //Failed criteria check go out of the loop.
        ShowNewsRegisterInfoError(strError);        
        strXml = "";
    }
        
    //document.getElementById("submitXML").innerHTML = strXml; 
    
    objName = null;
    objEmail = null;
    objPostcode = null;
    objCountry = null;  
    objIPaddress = null;  
    objBrowser = null;
    objDeparture = null;
    objDestination = null;
     
    return strXml;
}

function ShowUnsubscribe()
{
    var objDivInput = document.getElementById("dvInput");
    var objDivUnsubscribe = document.getElementById("dvUnsubscribe"); 

    objDivInput.style.display = "none";
    objDivUnsubscribe.style.display = "block";
    
    scroll(0,0);

    objDivInput = null;
    objDivUnsubscribe = null;
}


function SaveUnsubscribe()
{   
    var bPass = true;
    document.getElementById("spErrEmailUnsubscribe").innerHTML = "*";
    
    var objEmail = document.getElementById(FindControlName("input", "txtEmailUnsubscribe"));
    if(objEmail.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrEmailUnsubscribe").innerHTML = "* Email required. !!";
    }
    else if(ValidEmail(objEmail.value) == false)
    {
        bPass = false;
        document.getElementById("spErrEmailUnsubscribe").innerHTML = "* Supply valid Email address. !!";   
    }        
    
    if(bPass == true)
    {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SaveUnsubscribe(objEmail.value, SuccessSaveUnsubscribe, showError, showTimeOut);
    }
}

function SuccessSaveUnsubscribe(result)
{
    var objDivUnsubscribe = document.getElementById("dvUnsubscribe");
    var objDivResultComplete = document.getElementById("dvResultUnsubscribe"); 
    
    if(result=="complete")
    {
      objDivUnsubscribe.style.display = "none";
      objDivResultComplete.style.display = "block";
    }
    
    scroll(0,0);

    ShowProgressBar(false);
    objDivUnsubscribe = null;
    objDivResultComplete = null;    
}

function NewsAdminLogin()
{   
    var strResult = chkNewsAdminLogin(); //Validate and fill data.
    
    if(strResult.length > 0)
    {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.NewsAdminLogin(strResult, SuccessNewsAdminLogin, showError, showTimeOut);
    }
}

function SuccessNewsAdminLogin(result)
{
    var objDivContainer = document.getElementById("dvContainer");
    var objDivResult = document.getElementById("dvSubContainer"); 
    
    if(result=="complete")
    {
      objDivContainer.innerHTML = objDivResult.innerHTML;
    }
    else
    {
      ShowNewsRegisterInfoError(result);
    }
    
    scroll(0,0);

    ShowProgressBar(false);
    objDivContainer = null;
    objDivResult = null;    
}

function SubmitNewsAdminLogin(myfield,e)
{
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13)
    {
        NewsAdminLogin();
        return false;
    }
    else
        return true;
}
function chkNewsAdminLogin()
{
    var bPass = true;
    
    document.getElementById("spErrUserId").innerHTML = "*";
    document.getElementById("spErrPassword").innerHTML = "*";
    
    var objUserId = document.getElementById(FindControlName("input", "txtUserId")); 
    var objPassword = document.getElementById(FindControlName("input", "txtPassword"));    
    
    var strXml = "";
    var strError = "";  
    ShowNewsRegisterInfoError(strError); 

    //Add Contact detail information to xml.
    
    if(objUserId.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrUserId").innerHTML = "* User Id required!!";
    }
    if(objPassword.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrPassword").innerHTML = "* Password required !!";
    }
        
    if(bPass == true)
    {
      strXml = "<newsLogin>" +
                        "<UserId>" + objUserId.value + "</UserId>" +
                        "<Password>" + objPassword.value + "</Password>" +                                     
                 "</newsLogin>";
    }
    else
    {
        //Failed criteria check go out of the loop.
        ShowNewsRegisterInfoError(strError);        
        strXml = "";
    }
        
    //document.getElementById("submitXML").innerHTML = strXml; 
    
    objUserId = null;
    objPassword = null;
     
    return strXml;
}

var numOfselected=0;
function GetEditResgisterID()
{
    numOfselected = 0;
    var strRegister_id = "";
    var objRegister_id = document.getElementsByName("chkRegister_id");  
        
    for(var i=0;i<objRegister_id.length;i++)
    {
        if(objRegister_id[i].checked == true)
        {
            if(strRegister_id !="")
            {
              strRegister_id = strRegister_id + ","; 
            }
            numOfselected = numOfselected + 1; 
            strRegister_id = strRegister_id + objRegister_id[i].value;    
        }
    }
    
    return strRegister_id;
}

function DeleteNewsRegister()
{
    var strRegister_id = GetEditResgisterID();
    
    objRegister_id = null;
    if(strRegister_id.length > 0)
    {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.DeleteNewsRegister(strRegister_id, SuccessDeleteNewsRegister, showError, showTimeOut);
    }
}

function SuccessDeleteNewsRegister(result)
{   
    ShowProgressBar(false);
    SearchNewsRegister();
}

function SendMailNewsRegister()
{
    OpenCenterPopUp('..\MailForm.aspx','popup','750','350');
}

function OpenCenterPopUp(url,windowName,intPopUpWidth,intPopUpHeight)
{
	var winl = (screen.width - intPopUpWidth) / 2;
	var wint = (screen.height - intPopUpHeight) /2;
	window.open( 
			url,
			windowName,
			"'status=yes,scrollbars=yes,resizable=yes,top="+ wint + ",left=" + winl + ",width=" + intPopUpWidth + ",height=" + intPopUpHeight + "'");
}

function GetNewsMailForm()
{  
    var objDivContainer = document.getElementById("dvContainer");
    var objDivInputMail = document.getElementById("dvInputMail");
    var objDivTemp = document.getElementById("dvTemp");
    
    document.getElementById("txtMailFrom").value = document.getElementById("SenderEmailFrom").value;
    document.getElementById("txtMailTo").value = document.getElementById("SenderEmailTo").value;
    
    document.getElementById("txtMailBCC").value = GetEditResgisterID();    
    
    document.getElementById("txtMailAttachFile").value = "";
    document.getElementById("txtMailSubject").value = "";
    document.getElementById("txtMailMessage").value = "";
    
    var lblBcc = document.getElementById("lblBcc");
    if (numOfselected > 1)
    {
        lblBcc.innerHTML = numOfselected + " Receivers";
    }
    else
    {
        lblBcc.innerHTML = numOfselected + " Receiver";
    }
            
    objDivTemp.innerHTML = objDivContainer.innerHTML;
    objDivContainer.innerHTML = objDivInputMail.innerHTML;
    objDivInputMail.innerHTML = objDivTemp.innerHTML;
    objDivTemp.innerHTML = "";
        
    scroll(0,0);
    lblBcc = null;
    objDivTemp = null;
    objDivContainer = null;
    objDivInputMail = null;
}

function SuccessGetNewsMailForm(result)
{
    var obj = document.getElementById("dvSendmail");
    obj.innerHTML = result;
    
    scroll(0,0);
    
    ShowProgressBar(false);
    obj = null;
}

function CloseMailForm()
{
    var objDivResult = document.getElementById("dvContainer");
    var objDivSendmail = document.getElementById("dvInputMail");
    var objDivTemp = document.getElementById("dvTemp");
    
    objDivTemp.innerHTML = objDivResult.innerHTML;
    objDivResult.innerHTML = objDivSendmail.innerHTML;
    objDivSendmail.innerHTML = objDivTemp.innerHTML;
    objDivTemp.innerHTML = "";

    scroll(0,0);
    objDivResult = null;
    objDivTemp = null;
}

function SendMailComplete()
{
    var objDivContainer = document.getElementById("dvContainer");
    var objDivInputMail = document.getElementById("dvInputMail");
    var objDivResultMailComplete = document.getElementById("dvResultMailComplete");     

    objDivInputMail.innerHTML = objDivContainer.innerHTML;
    objDivContainer.innerHTML = objDivResultMailComplete.innerHTML;
    objDivContainer = null;
    objDivInputMail = null;
    objDivResultMailComplete = null;
}

function NewsRegisterSendMail()
{   
    var strResult = FillNewsRegisterMail(); //Validate and fill data.
        
    if(strResult.length > 0)
    {
        ShowProgressBar(true);
        document.getElementById("txtMailAttachFile").value = "";
        document.getElementById("txtMailSubject").value = "";
        document.getElementById("txtMailMessage").value = "";
        tikAeroB2C.WebService.B2cService.NewsRegisterSendMail(strResult, SuccessNewsRegisterSendMail, showError, showTimeOut);
    }
}

function SuccessNewsRegisterSendMail(result)
{
    ShowProgressBar(false);
    if(result=="complete")
    {
      SendMailComplete();
    }
    else
    {
      document.getElementById("spErrMailMessage").innerHTML = result; 
    }    
}

function FillNewsRegisterMail()
{
    var bPass = true;
    
    document.getElementById("spErrMailFrom").innerHTML = "";
    document.getElementById("spErrMailTo").innerHTML = "";
    document.getElementById("spErrMailSubject").innerHTML = "";
    document.getElementById("spErrMailMessage").innerHTML = "";
    document.getElementById("spErrMailBCC").innerHTML = "";    
    
    var objMailFrom = document.getElementById(FindControlName("input", "txtMailFrom")); 
    var objMailTo = document.getElementById(FindControlName("input", "txtMailTo"));
    var objMailSubject = document.getElementById(FindControlName("input", "txtMailSubject"));
    var objMailMessage = document.getElementById(FindControlName("textarea", "txtMailMessage"));
    
    var objMailBcc =  document.getElementById(FindControlName("input", "txtMailBCC")); 
    var objMailAddBcc = document.getElementById(FindControlName("textarea", "txtMailAddBcc"));    
                
    var strXml = "";
    var strError = "";   
    
    if(objMailFrom.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrMailFrom").innerHTML = "<br>Mail From required. !!";
    }
    else if(ValidEmail(objMailFrom.value) == false)
    {
        bPass = false;
        document.getElementById("spErrMailFrom").innerHTML = "<br>Mail From is not valid Email address. !!";   
    }       
    if(objMailTo.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrMailTo").innerHTML = "<br>Mail To required. !!";
    }
    else if(ValidEmail(objMailTo.value) == false)
    {
        bPass = false;
        document.getElementById("spErrMailTo").innerHTML = "<br>Mail To is not valid Email address. !!";   
    }    
    
    var strMailAddBCC = objMailAddBcc.value;
    while(strMailAddBCC.indexOf(",") > 0)  
    {
        strMailAddBCC = strMailAddBCC.replace(',',';');  
    }        
    var strArrMailBCC = strMailAddBCC.split(";");
    
    strMailAddBCC = "";
    for(var i=0;i<strArrMailBCC.length;i++)
    {
        if(trim(strArrMailBCC[i]) != "")
        {
            if(ValidEmail(strArrMailBCC[i]) == false)
            {
                bPass = false;
                document.getElementById("spErrMailBCC").innerHTML = "<br>" + strArrMailBCC[i] +" :Mail To is not valid Email address. !!";
                i = strArrMailBCC.length;   
            }    
            if(strMailAddBCC !="")
            {
              strMailAddBCC = strMailAddBCC + "; "; 
            } 
            strMailAddBCC = strMailAddBCC + trim(strArrMailBCC[i]);    
        }
    }            
       
    if(objMailSubject.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrMailSubject").innerHTML = "<br>Mail Subject required. !!";
    }
    if (objMailMessage.value.length == 0)
    {
        bPass = false;
        document.getElementById("spErrMailMessage").innerHTML = "<br>Mail Message required. !!";
    }				
        
    if(bPass == true)
    {
        strXml = "<SendMail>" +
                        "<MailFrom>" + objMailFrom.value + "</MailFrom>" +
                        "<MailTo>" + objMailTo.value + "</MailTo>" +
                        "<MailSubject>" + objMailSubject.value + "</MailSubject>" +
                        "<MailMessage>" + objMailMessage.value + "</MailMessage>" +
                        "<MailBcc>" + objMailBcc.value + "</MailBcc>" + 
                        "<MailAddBcc>" + strMailAddBCC + "</MailAddBcc>" +                                                                 
                  "</SendMail>";
    }
    else
    {
        ShowNewsRegisterInfoError(strError);        
        strXml = "";
    }     
        
    objMailFrom = null;
    objMailTo = null;
    objMailSubject = null;
    objMailMessage = null;  
    
    objMailBcc = null;
    objMailAddBcc = null;
    objMailAttachFile = null;
         
    return strXml;
}

function ShowDateRegis()
{    
    var objDateRegis = document.getElementById("chkDateRegis");
    var objDivDate = document.getElementById("dvDate");
    if(objDateRegis.checked == true)
    {
        objDivDate.style.display = "block";
    }
    else
    {
        objDivDate.style.display = "none";
    }
}

function SendRegisterMail()
{
    if(document.getElementById("txtMailAttachFile").value != "")
    {
        var strResult = FillNewsRegisterMail();    
        if(strResult.length > 0)
        {
            document.forms[0].submit();
        }
    }
    else
    {
        NewsRegisterSendMail();
    }
    document.getElementById("txtMailAttachFile").value = "";
    document.getElementById("txtMailSubject").value = "";
    document.getElementById("txtMailMessage").value = "";
}
