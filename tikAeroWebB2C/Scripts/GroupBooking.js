function LoadGroupBooking()
{
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadGroupBooking(SuccessLoadGroupBooking, showError, showTimeOut);
}

function SuccessLoadGroupBooking(result)
{   
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    
    scroll(0,0);
    
    ShowPannel(false);
    ShowProgressBar(false);
    obj = null;
}

function ValidateGroupBooking(o){
	var Status = true;
	var e 	= '';
	var fn 	= document.getElementById(o +'txtFirstName');
	var ln 	= document.getElementById(o+'txtLastName');
	var pm 	= document.getElementById(o+'txtContactMobile');
	var ph 	= document.getElementById(o+'txtContactPhoneNumber');
	var p = trim(pm.value) + trim(ph.value);
	var em 	= document.getElementById(o+'txtEmailAddress');
	var emReEnter = document.getElementById(o+'txtEmailAddressReEnter');
	var ad1 = document.getElementById(o+'txtAddress1');
	var cty = document.getElementById(o+'txtTownCity');
	var pc 	= document.getElementById(o+'txtPostcode');	

	//--->  First Name
	if (trim(fn.value)==0)
	{	
		Status =  false;
		e += "<li>Please supply a valid First Name</li>";
		fn.className = 'error';
	}
	else
	{
		fn.className = '';
	}	
	//--->  Surname
	if (trim(ln.value)==0)
	{
		Status =  false;
		e += "<li>Please supply a valid Surname</li>";
		ln.className = 'error';
	}
	else
	{
		ln.className = '';
	}
	//--->  Telephone Number
	if (p==0)
	{
		Status =  false;
		e += "<li>Please supply a valid Telephone Number</li>";
		pm.className = 'error';
		ph.className = 'error';
	}
	else
	{
		pm.className = '';
		ph.className = '';
	}
	//--->  Email
	if (trim(em.value)==0){
		Status =  false;
		e += "<li>Please supply a valid Email</li>";
		em.className = 'error';
	}
	else if (ValidEmail(em.value)!= true)
	{
		Status =  false;
		e += "<li>Please supply a valid Email format</li>";
		em.className = 'error';
    }
    else
    {
		em.className = '';
	}
    
    if (em.value != emReEnter.value)
    {
		Status =  false;
		e += "<li>Please supply a valid Confirm Email</li>";
		em.className = 'error';
	}
	//--->  Address 1
	if (trim(ad1.value) == 0)
	{
		Status = false;
		e += "<li>Please supply a valid Address 1</li>";
		ad1.className = 'error';
	}
	else
	{
		ad1.className = '';
	}
	//--->  Town/City
	if (trim(cty.value) == 0)
	{
		Status =  false;
		e += "<li>Please supply a valid Town/City</li>";
		cty.className = 'error';
	}
	else
	{
		cty.className = '';
	}
	//--->  Postal Code
	if (trim(pc.value)==0)
	{
		Status =  false;
		e += "<li>Please supply a valid Postcode</li>";
		pc.className = 'error';
	}
	else
	{
		pc.className = '';
	}
	
	var iA = new Number(document.getElementById('cmbAdult').options[document.getElementById('cmbAdult').selectedIndex].value);	
	var iC = new Number(document.getElementById('cmbChild').options[document.getElementById('cmbChild').selectedIndex].value);
	var groupNum = document.getElementById('cmbAdult');
	if ((iA+iC)<10)
	{
		Status =  false;
		e += "<li>Please supply a valid Group Size. Minimum 10 People for a group (Not Including Infant)</li>";
		groupNum.className = 'error';
	}
	
	if (e.length > 0) {
		Container = document.getElementById(o +'LabError');
		Container.innerHTML = 	'<ul>' + e + '</ul>';	
		return Status;
	}
	else
	{
		return Status;
	}
}

function SendMailGroupBooking(o)
{
    Status = true;
    Status = ValidateGroupBooking(o);

    if (Status==true)
    {  
        var cmbAdult = document.getElementById('cmbAdult').options[document.getElementById('cmbAdult').selectedIndex].text;
        var cmbChild = document.getElementById('cmbChild').options[document.getElementById('cmbChild').selectedIndex].text;
        var cmbInfant = document.getElementById('cmbInfant').options[document.getElementById('cmbInfant').selectedIndex].text;
        
        var Title = document.getElementById(o+'ddlTitle').options[document.getElementById(o+'ddlTitle').selectedIndex].value;

        Title =   Title.split("|")[0] ;

        var first_name = document.getElementById(o+'txtFirstName').value;
        var last_name = document.getElementById(o+'txtLastName').value;
        var phoneMobile = document.getElementById(o+'txtContactMobile').value;
        var phoneHome = document.getElementById(o+'txtContactPhoneNumber').value;
        var contactEmail = document.getElementById(o+'txtEmailAddress').value;
        var addressline1 = document.getElementById(o+'txtAddress1').value;  
        var addressline2 = document.getElementById(o+'txtAddress2').value;
        var TownCity = document.getElementById(o+'txtTownCity').value;   
        var txtContactCounty = document.getElementById(o+'txtContactCounty').value;     
        var postCode = document.getElementById(o+'txtPostcode').value;       
        var ddlCountry = document.getElementById(o+'ddlCountry').options[document.getElementById(o+'ddlCountry').selectedIndex].text;
        
        var txtOrigin = document.getElementById(o+'optOrigin').options[document.getElementById(o+'optOrigin').selectedIndex].text;
        var txtDestination = document.getElementById(o+'optDestination').options[document.getElementById(o+'optDestination').selectedIndex].text;       
        var cmbPreferedDepartureTime = document.getElementById('cmbPreferedDepartureTime').options[document.getElementById('cmbPreferedDepartureTime').selectedIndex].value;
        var cmbPreferedReturnDepartureTime = document.getElementById('cmbPreferedReturnDepartureTime').options[document.getElementById('cmbPreferedReturnDepartureTime').selectedIndex].value;
        var txtAdditionalInformation = document.getElementById(o+'txtAdditionalInformation').value;
        
        var ctlDepatureDate = GroupBookingGetDate('3');
        var ctlReturnDate = GroupBookingGetDate('4');

        tikAeroB2C.WebService.B2cService.SendMailGroupBooking( cmbAdult, cmbChild, cmbInfant, Title,  first_name,  last_name,  phoneMobile, phoneHome, contactEmail, addressline1,  addressline2, TownCity,  txtContactCounty, postCode,  ddlCountry, txtOrigin,  txtDestination,  ctlDepatureDate,  ctlReturnDate, cmbPreferedDepartureTime, cmbPreferedReturnDepartureTime,  txtAdditionalInformation, SuccessSendMailGroupBooking, showError, showTimeOut) ;
    }
}

function SuccessSendMailGroupBooking(result)
{   
    if (result=='1')
    {
       ShowMessageBox("Thank you for your enquiry,<br> we will get back to you shortly " , 1,'loadHome');            
    }
    else   
    {
       ShowMessageBox("Can't sendmail!!", 0, ''); 
    }
}

function GroupBookingGetDate(o)
{ 
    var ddlMY=document.getElementById('ddlMY_'+o);
    var ddlDate=document.getElementById('ddlDate_'+o);
    return ddlDate.options[ddlDate.selectedIndex].value+ '/'+ ddlMY.options[ddlMY.selectedIndex].value.substring(4) + '/' +ddlMY.options[ddlMY.selectedIndex].value.substring(0,4);
}

function CheckAdultInfant()
{
	var iA = new Number(document.getElementById('cmbAdult').options[document.getElementById('cmbAdult').selectedIndex].value);	
	var iI = new Number(document.getElementById('cmbInfant').options[document.getElementById('cmbInfant').selectedIndex].value);
	if (iA<iI)
	{
		alert('It is not possible to book more Infants than Adults!');
		document.getElementById('cmbInfant').selectedIndex = 0;
	}
}

function CheckAllPax(o1, o2) 
{
	var MaxAllPax = 5;
	var suffixAdult = "Adult";
	var suffixAdults = "Adults";
	var suffixChild = "Child";
	var suffixChildren = "Children";
	var suffix = "";
	
	var objO1 = document.getElementById(o1);
	var objO2 = document.getElementById(o2);
	
	var objO1SelectedIndex = objO1.selectedIndex;
	var objO2SelectedIndex = objO2.selectedIndex;
	
	var selectedBoxValue = parseInt(objO1.value);
	
	var difValue = MaxAllPax - selectedBoxValue;	
	
	// Clear Selected box
	objO1.options.length = 0;
	
	// Clear fullfill box
	objO2.options.length = 0;
	
	if (objO1.id.indexOf("Child") != -1) {
		var newOption = document.createElement('option');
		newOption.text = suffixChildren;
		newOption.value = 0;
		try {
			objO1.add(newOption, null); // standards compliant; doesn't work in IE
		}
		catch(ex) {
			objO1.add(newOption); // IE only
		}
	}
	
	if (objO2.id.indexOf("Child") != -1) {
		var newOption = document.createElement('option');
		newOption.text = suffixChildren;
		newOption.value = 0;
		try {
			objO2.add(newOption, null); // standards compliant; doesn't work in IE
		}
		catch(ex) {
			objO2.add(newOption); // IE only
		}
	}
	
	// Add O2 Option
	for (var i=1 ; i<=difValue ; i++) {
		suffix = "";
		var newOption = document.createElement('option');
		if (objO2.id.indexOf("Child") != -1) {
			if (i == 1) {
				suffix = suffixChild;
			}
			else {
				suffix = suffixChildren;
			}
		}
		else {
			if (i == 1) {
				suffix = suffixAdult;
			}
			else {
				suffix = suffixAdults;
			}
		}
		if (i == 0) {
			newOption.text = suffix;
		}
		else {
			newOption.text = i + " " + suffix;
		}
		newOption.value = i;
		try {
			objO2.add(newOption, null); // standards compliant; doesn't work in IE
		}
		catch(ex) {
			objO2.add(newOption); // IE only
		}
	}
	
	// Add O2 Option
	for (var j=1 ; j<=selectedBoxValue ; j++) {
		suffix = "";
		var newOption = document.createElement('option');
		if (objO1.id.indexOf("Child") != -1) {
			if (j == 1) {
				suffix = suffixChild;
			}
			else {
				suffix = suffixChildren;
			}
		}
		else {
			if (j == 1) {
				suffix = suffixAdult;
			}
			else {
				suffix = suffixAdults;
			}
		}
		if (j == 0) {
			newOption.text = suffix;
		}
		else {
			newOption.text = j + " " + suffix;
		}
		newOption.value = j;
		try {
			objO1.add(newOption, null); // standards compliant; doesn't work in IE
		}
		catch(ex) {
			objO1.add(newOption); // IE only
		}
	}
	
	objO2.selectedIndex = objO2SelectedIndex;
	objO1.selectedIndex = objO1SelectedIndex;
}