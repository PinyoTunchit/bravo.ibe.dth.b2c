﻿function CalculateBaggageFee(bReturn) {
    var objBagFee;
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPaxSelectPosition);
    var hdSegmentId;

    if (bReturn == false) {
        objBagFee = document.getElementById("BaggageDepart");
        hdSegmentId = document.getElementById("hdOutwardSegmentId");
    }
    else {
        objBagFee = document.getElementById("BaggageReturn");
        hdSegmentId = document.getElementById("hdReturnSegmentId");
    }

    if (objBagFee != null && hdSegmentId != null) {
        var iFeeUnit = GetSelectedOption(objBagFee);
        if (iFeeUnit.length > 0) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.CalculateBaggageFee(parseInt(iFeeUnit), GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), bReturn, true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.ClearBaggageFee(GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
    }
}

function CalculateBaggageSeat(baggageID, paxIndex) {
    var objBagFee = document.getElementById(baggageID);
    var hdPassengerId = document.getElementById("hdPassengerId" + paxIndex);
    var hdSegmentId = document.getElementById("hdSegmentId" + paxIndex);

    if (objBagFee != null && hdSegmentId != null) {
        var iFeeUnit = GetSelectedOption(objBagFee);
        if (iFeeUnit.length > 0) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.CalculateBaggageFee(parseInt(iFeeUnit), GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), true, true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.ClearBaggageFee(GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
    }
}

function SuccessCalculateBaggageFee(result) {
    if (result.length > 0) {

        DisplayQuoteSummary(result, "", "");

        //Initialize Summary collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
    ShowProgressBar(false);
}
function GetBaggageFeeOptions(iPosition) {
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPosition);
    //Get Baggage list.
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetBaggageFeeOptions(GetControlValue(hdPassengerId), SuccessGetBaggageFeeOptions, showError, showTimeOut);
}

function SuccessGetBaggageFeeOptions(result) {

    if (result.length > 0) {
        if (result == "{201}") {
            //Booking object is null.(Session time out).
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
         }
        else {
            var objJson = eval("(" + result + ")");
            var strFeeDisplayText = "";
            var strCurrencySign = "";

            //Fill baggage origin list.
            var objDept = document.getElementById("BaggageDepart");
            if (objDept != null) {
                objDept = ClearOptions(objDept);

                var optDefaultOut = document.createElement("option");
                optDefaultOut.className = "watermarkOn";
                optDefaultOut.value = "";
                if (objJson[0].BaggageOutAllowance != null && objJson[0].BaggageOutAllowance > 0) {
                    if (objJson[0].BaggageOutAllowance > 1) {
                        optDefaultOut.innerHTML = objJson[0].BaggageOutAllowance + " " + objLanguage.default_value_9 + " " + objLanguage.default_value_10;
                    }
                    else {
                        optDefaultOut.innerHTML = objJson[0].BaggageOutAllowance + " " + objLanguage.default_value_8 + " " + objLanguage.default_value_10;
                    }
                }
                else {
                    optDefaultOut.innerHTML = objLanguage.default_value_7;
                }
                objDept.appendChild(optDefaultOut);

                //Loop through list.
                for (var i = 0; i < objJson[0].BaggageOut.length; i++) {

                    var opt = document.createElement("option");

                    if (objJson[0].BaggageOut[i].currency_rcd == "JPY") {
                        strCurrencySign = "&#165;";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "HKD") {
                        strCurrencySign = "HK$";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "TWD") {
                        strCurrencySign = "NT$";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "CNY") {
                        strCurrencySign = "RMB";
                    }
                    else {
                        strCurrencySign = "&#8361;";
                    }

                    if (objJson[0].BaggageOut[i].number_of_units > 1) {
                        strFeeDisplayText = objJson[0].BaggageOut[i].number_of_units + objLanguage.default_value_9 + " " + strCurrencySign + " " + Math.round(objJson[0].BaggageOut[i].total_amount_incl);
                    }
                    else {
                        strFeeDisplayText = objJson[0].BaggageOut[i].number_of_units + objLanguage.default_value_8 + " " + strCurrencySign + " " + Math.round(objJson[0].BaggageOut[i].total_amount_incl);
                    }

                    if (objJson[0].BaggageOut[i].Selected == true) {
                        opt.selected = true;
                    }

                    opt.value = objJson[0].BaggageOut[i].number_of_units;
                    opt.innerHTML = strFeeDisplayText;
                    objDept.appendChild(opt);
                }

                //Add Event;
                $("#BaggageDepart").unbind();
                $("#BaggageDepart").change(function () {
                    CalculateBaggageFee(false);
                    SetPassengerValue();
                });
            }

            //Fill baggage Destination list.
            var objReturn = document.getElementById("BaggageReturn");
            if (objReturn != null) {
                objReturn = ClearOptions(objReturn);

                var optDefaultReturn = document.createElement("option");
                optDefaultReturn.className = "watermarkOn";
                optDefaultReturn.value = "";
                if (objJson[0].BaggageReturnAllowance != null && objJson[0].BaggageReturnAllowance > 0) {
                    if (objJson[0].BaggageReturnAllowance > 1) {
                        optDefaultReturn.innerHTML = objJson[0].BaggageReturnAllowance + " " + objLanguage.default_value_9 + " " + objLanguage.default_value_10;
                    }
                    else {
                        optDefaultReturn.innerHTML = objJson[0].BaggageReturnAllowance + " " + objLanguage.default_value_8 + " " + objLanguage.default_value_10;
                    }
                }
                else {
                    optDefaultReturn.innerHTML = objLanguage.default_value_7;
                }
                objReturn.appendChild(optDefaultReturn);

                //Loop through list.
                for (var i = 0; i < objJson[0].BaggageReturn.length; i++) {

                    var opt = document.createElement("option");

                    if (objJson[0].BaggageReturn[i].currency_rcd == "JPY") {
                        strCurrencySign = "&#165;";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "HKD") {
                        strCurrencySign = "HK$";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "TWD") {
                        strCurrencySign = "NT$";
                    }
                    else if (objJson[0].BaggageOut[i].currency_rcd == "CNY") {
                        strCurrencySign = "RMB";
                    }
                    else {
                        strCurrencySign = "&#8361;";
                    }

                    if (objJson[0].BaggageReturn[i].number_of_units > 1) {
                        strFeeDisplayText = objJson[0].BaggageReturn[i].number_of_units + objLanguage.default_value_9 + " " + strCurrencySign + " " + Math.round(objJson[0].BaggageReturn[i].total_amount_incl);
                    }
                    else {
                        strFeeDisplayText = objJson[0].BaggageReturn[i].number_of_units + objLanguage.default_value_8 + " " + strCurrencySign + " " + Math.round(objJson[0].BaggageReturn[i].total_amount_incl);
                    }

                    if (objJson[0].BaggageReturn[i].Selected == true) {
                        opt.selected = true;
                    }

                    opt.value = objJson[0].BaggageReturn[i].number_of_units;
                    opt.innerHTML = strFeeDisplayText;
                    objReturn.appendChild(opt);
                }

                //Add Event;
                $("#BaggageReturn").unbind();
                $("#BaggageReturn").change(function () {
                    CalculateBaggageFee(true);
                    SetPassengerValue();
                });
            }
        }
    }
    ShowProgressBar(false);
}