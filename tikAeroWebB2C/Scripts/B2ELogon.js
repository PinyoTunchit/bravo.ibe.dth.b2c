﻿function LoadB2ELogon() {
    tikAeroB2C.WebService.B2cService.LoadB2ELogon(SuccessLoadB2ELogon, showError, showTimeOut);
}

function SuccessLoadB2ELogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2ELogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2ELogonDialog(SuccessLoadB2ELogonDialog, showError, showTimeOut);
}

function SuccessLoadB2ELogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}

function LoadB2EAdminLogon() {
    tikAeroB2C.WebService.B2cService.LoadB2EAdminLogon(SuccessLoadB2EAdminLogon, showError, showTimeOut);
}

function SuccessLoadB2EAdminLogon(result) {
    var objLogon = document.getElementById("dvContainer");
    if (objLogon != null && result.length > 0) {
        objLogon.innerHTML = result;
    }
    objLogon = null;
}

function LoadB2EAdminLogonDialog() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.LoadB2EAdminLogonDialog(SuccessLoadB2EAdminLogonDialog, showError, showTimeOut);
}

function SuccessLoadB2EAdminLogonDialog(result) {
    ShowProgressBar(false);
    var objMessage = document.getElementById("dvFormHolder");
    var objContainer = document.getElementById("dvProgressBar");

    objMessage.innerHTML = result;
    objContainer.style.display = "block";
    objMessage.style.display = "block";
    objMessage = null;
}
function CorporateLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.CorporateLogon(document.getElementById('txtDialogCompanyCode').value,
        document.getElementById('txtDialogLogonID').value, document.getElementById('txtDialogPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.CorporateLogon(document.getElementById('txtCompanyCode').value,
        document.getElementById('txtLogonID').value, document.getElementById('txtPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateAdminLogonDialog() {
    CloseDialog();
    ShowProgressBar(true);

    tikAeroB2C.WebService.B2cService.CorporateAdminLogon(document.getElementById('txtDialogAgencyCode').value,
        document.getElementById('txtDialogAdminLogonID').value, document.getElementById('txtDialogPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function CorporateAdminLogon() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.CorporateAdminLogon(document.getElementById('txtAgencyCode').value,
        document.getElementById('txtAdminLogonID').value, document.getElementById('txtPassword').value, SuccessCorporateLogon, showError, showTimeOut);

}

function SuccessCorporateLogon(result) {
    var arrResult = result.split(",");

    if (arrResult.length == 2 && arrResult[0] == 'Error') {
        ShowProgressBar(false);
        if (arrResult[1] == "0") {
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
        else {
            ShowMessageBox(arrResult[1], 0, '');
        }
    }
    else {
        window.location.replace(result);
    }
}
function SubmitEnterCorporateLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            CorporateLogon();
        else
            CorporateLogonDialog();
        return false;
    }
    else
        return true;
}
function SubmitEnterCorporateAdminLogon(myfield, e, isDialog) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        if (isDialog == 'false')
            CorporateAdminLogon();
        else
            CorporateAdminLogonDialog();
        return false;
    }
    else
        return true;
}