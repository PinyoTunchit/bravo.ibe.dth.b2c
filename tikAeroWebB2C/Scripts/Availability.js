var tmpOutFlight_id = "";
var tmpOutFare_id = "";
var tmpRetFlight_id = "";
var tmpRetFare_id = "";
function getDestination() {
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    var strSelectedOrigin = "";

    //Add destination from selected origin
    strSelectedOrigin = objOrigin.options[objOrigin.selectedIndex].value;
    objDestination = ClearOptions(objDestination);  //Clear previous value

    for (var iCount = 0; iCount < arrDestination.length; iCount++) {
        if (arrDestination[iCount].split("|")[2] == strSelectedOrigin.split("|")[0]) {
            //Allowed only the destination that have the same
            //origin as selected origin.
            var opt = document.createElement("option");
            opt.value = arrDestination[iCount].split("|")[2] + "|" +
                        objOrigin.options[objOrigin.selectedIndex].text + "|" +
                        arrDestination[iCount].split("|")[0] + "|" +
                        arrDestination[iCount].split("|")[1] + "|" +
                        arrDestination[iCount].split("|")[3] + "|" +
                        arrDestination[iCount].split("|")[4];

            //Add display text to option
            if (navigator.appName.indexOf('Microsoft') == 0)
            { opt.innerText = arrDestination[iCount].split("|")[1]; }
            else
            { opt.text = arrDestination[iCount].split("|")[1]; }

            objDestination.appendChild(opt);
            opt = null;
        }
    }
    objOrigin = null;
    objDestination = null;
}
function ValidNumberOfPassenger(iAdult, iChild, iInfant, iOther) {
    if (iInfant > iAdult) {
        ShowMessageBox(objLanguage.Alert_Message_2, 0, '');
        return false;
    }
    else if ((iAdult + iChild + iInfant) > 12) {
        ShowMessageBox(objLanguage.Alert_Message_53.replace("{NO}", 12), 0, '');
        return false;
    }
    else if ((iAdult + iChild + iInfant + iOther) == 0) {
        ShowMessageBox(objLanguage.Alert_Message_6, 0, '');
        return false;
    }
    else {
        return true;
    }
}
function SearchAvailability(strSearchType, origin, destination) {
    var objOneWay = document.getElementById("optOneWay");
    var objAdult = document.getElementById("optAdult");
    var objChild = document.getElementById("optChild");
    var objInfant = document.getElementById("optInfant");
    var objBoardingClass = document.getElementById("optBoardingClass");
    var objPromoCode = document.getElementById("txtPromoCode");
    var objYP = document.getElementById("optYP");

    var isOneWay = objOneWay.checked.toString();
    var iAdult = parseInt(objAdult.options[objAdult.selectedIndex].value);
    var iChild = parseInt(objChild.options[objChild.selectedIndex].value);
    var iInfant = parseInt(objInfant.options[objInfant.selectedIndex].value);
    var strBoardingClass = objBoardingClass.options[objBoardingClass.selectedIndex].value;
    var dtDateFrom = getdatefrom("1");
    var dtDateTo = getdateto("2");
    var strPromoCode = "";
    var iOther = 0;
    var otherType = "";
    // Check Ip address for Default Currency 
    var obtCurrency = document.getElementById("AvailabilitySearch1_optCurrency");
    if (strSearchType.split(",").length == 3) {
        if (obtCurrency != null) {
            urrencyCode = obtCurrency.options[obtCurrency.selectedIndex].value;
        }
        strSearchType = 'FARE';
    }
    // End Check Ip address for Default Currency 
    if (objYP != null) {
        iOther = parseInt(objYP.options[objYP.selectedIndex].value);
        otherType = "YP";
    }

    if (objPromoCode != null) {
        strPromoCode = objPromoCode.value;
    }

    //Call Availability web service
    if (ValidNumberOfPassenger(iAdult, iChild, iInfant, iOther) == true) {
        ShowProgressBar(true);

        var strOri = "";
        var strDest = "";
        var currencyCode = "";
        if (origin.length == 0 & destination.length == 0) {
            var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
            var strOrigin = objOrigin.options[objOrigin.selectedIndex].value;

            var objDestination = document.getElementById(FindControlName("select", "optDestination"));
            var strDestination = objDestination.options[objDestination.selectedIndex].value;

            strOri = strDestination.split("|")[0];
            strDest = strDestination.split("|")[2];
            currencyCode = strOrigin.split("|")[1];

            objOrigin = null;
            objDestination = null;
        }
        else {
            strOri = origin;
            strDest = destination;
        }

        if (strOri.length == 0 || strDest.length == 0) {
            ShowMessageBox(objLanguage.Alert_Message_54, 0, '');
        }
        else {
            if (strSearchType.split(",").length == 2) {
                tikAeroB2C.WebService.B2cService.GetLowFareFinderBeforeAvailability(strOri,
                                                                                    strDest,
                                                                                    dtDateFrom,
                                                                                    dtDateTo,
                                                                                    isOneWay,
                                                                                    iAdult,
                                                                                    iChild,
                                                                                    iInfant,
                                                                                    strBoardingClass,
                                                                                    currencyCode,
                                                                                    strPromoCode,
                                                                                    strSearchType.split(",")[0],
                                                                                    iOther,
                                                                                    otherType,
                                                                                    g_strIpAddress,
                                                                                    SuccessGetLowFareFinder,
                                                                                    showError,
                                                                                    dtDateFrom + "|" + dtDateTo);
            }
            else {
                tikAeroB2C.WebService.B2cService.GetAvailability(strOri,
                                                                    strDest,
                                                                    dtDateFrom,
                                                                    dtDateTo,
                                                                    isOneWay,
                                                                    "0",
                                                                    iAdult,
                                                                    iChild,
                                                                    iInfant,
                                                                    strBoardingClass,
                                                                    currencyCode,
                                                                    strPromoCode,
                                                                    strSearchType,
                                                                    iOther,
                                                                    otherType,
                                                                    g_strIpAddress,
                                                                    SuccessSearchFlight,
                                                                    showError,
                                                                    showTimeOut);
            }
        }
    }

    objOrigin = null;
    objDestination = null;
    objOneWay = null;
    objAdult = null;
    objChild = null;
    objInfant = null;
    objBookingClass = null;
    objPromoCode = null;
}

function SuccessSearchFlight(result) {
    if (result.length > 0) {
        if (result == "{002}") {
            loadHome();
        }
        else if (result == "{003}") {
            //Invalid Required Parameter.
            ShowMessageBox(objLanguage.Alert_Message_55, 0, '');
        }
        else if (result == "{001}") {
            LoadSecure(false);
        }
        else if (result == "{102}") {
            //Low Fare finder input not found.
            ShowMessageBox(objLanguage.Alert_Message_3, 0, '');
        }
        else if (result == "{103}") {
            //Low Fare finder Exception error.
            ShowMessageBox(objLanguage.Alert_Message_3, 0, '');
        }
        else {
            if (result == "{004}") {
                //System Error. Please try again.
                ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
            }
            else {
                //Display Html
                var obj = document.getElementById("dvContainer");
                obj.innerHTML = result;
                obj = null;

                //Display Booking Summary.
                ShowAvailabilityBookingSummary();

                //Hide Select buttom when client is on hold
                var objClientOnHold = document.getElementById("dvClientOnHold");
                if (objClientOnHold.value == 1) {
                    document.getElementById("dvAvaiNext").style.display = "none";
                }
                else {
                    document.getElementById("dvAvaiNext").style.display = "block";
                }
                objClientOnHold = null;
                //Move scroll to top page.
                scroll(0, 0);

                //Set Calendar Date.
                var objFlightDateOutward = document.getElementById("Outward_hdSelectedDate");
                var objFlightDateReturn = document.getElementById("Return_hdSelectedDate");

                if (objFlightDateOutward != null) {
                    setFromDate(objFlightDateOutward.value);
                }
                if (objFlightDateReturn != null) {
                    setToDate(objFlightDateReturn.value);
                }

                ShowSearchPannel(false);
                chkOutward = null;
                chkReturn = null;
                ShowProgressBar(false);
            }
        }
    }
}

function getdatefrom(id) {
    var ddlMY_1 = document.getElementById("ddlMY_" + id);
    var ddlDate_1 = document.getElementById("ddlDate_" + id);
    var txt_1 = document.getElementById("txt_" + id);

    if (txt_1 == null)
        return (ddlMY_1.options[ddlMY_1.selectedIndex].value + ddlDate_1.options[ddlDate_1.selectedIndex].value);
    else {
    }
}
function getdateto(id) {
    var ddlMY_2 = document.getElementById("ddlMY_" + id);
    var ddlDate_2 = document.getElementById("ddlDate_" + id);
    var txt_2 = document.getElementById("txt_" + id);

    if (txt_2 == null) {
        if (ddlMY_2.selectedIndex >= 0) {

            return (ddlMY_2.options[ddlMY_2.selectedIndex].value + ddlDate_2.options[ddlDate_2.selectedIndex].value);
        }
        else {
            return "";
        }
    }
    else {
        if (txt_2.value.length > 0) {
        }
        else {
            return "";
        }
    }

}


function ShowHideCalendar(strOptionName, strCalHolderName, strCalLabelName) {
    
    var objSearch = document.getElementById(strOptionName);
    var objCar = document.getElementById(strCalHolderName);
    var objReturnLabel = document.getElementById(strCalLabelName);

    if (objSearch.checked == true) {
        CalendarToControls = "one2two";
        objCar.style.display = "block";
        objReturnLabel.style.display = "block";
    }
    else {
        CalendarToControls = "one2one";
        objCar.style.display = "none";
        objReturnLabel.style.display = "none";

    }
    objReturnLabel = null;
    objCar = null;
    objSearch = null;
}
function SearchSingleFlight(flightDate, strFlightType) {

    var objFlightDate;

    if (strFlightType == "Outward") {
        objFlightDate = document.getElementById("Return_hdSelectedDate");
    }
    else {
        objFlightDate = document.getElementById("Outward_hdSelectedDate");
    }

    if (objFlightDate != null &&
        ((strFlightType == "Return" && parseInt(flightDate) < parseInt(objFlightDate.value)))) {

    }
    else {
        if (strFlightType == "Outward") {

            setFromDate(flightDate);
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.SearchSingleFlight(flightDate, true, SuccessSearchFlight, showError, showTimeOut);
        }
        else {

            setToDate(flightDate);
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.SearchSingleFlight(flightDate, false, SuccessSearchFlight, showError, showTimeOut);
        }
        document.getElementById("dvAvaiNext").style.display = "none";
    }
    
}

function SelectFlight() {
    var chkOutward = document.getElementsByName("Outward");
    var OutwardParam = "";
    var OutwardDate = 0;
    var OutwardTime = 0;


    var chkReturn = document.getElementsByName("Return");
    var ReturnParam = "";
    var ReturnDate = 0;
    var ReturnTime = 0;

    var iCount = 0;

    var objSelectedDate = document.getElementsByName("hdSelectedDate");

    if (objSelectedDate.length == 2 & (chkReturn == null || chkReturn.length == 0)) {
        //Please select return flight.
        ShowMessageBox(objLanguage.Alert_Message_127, 0, '');
     }
    else {
        if (chkOutward != null) {
            iCount = chkOutward.length;
            if (iCount > 0) {
                for (var i = 0; i < iCount; i++) {
                    if (chkOutward[i].checked == true) {
                        OutwardParam = chkOutward[i].value;
                        break;
                    }
                }
                if (OutwardParam.length > 0) {
                    arrFlightInfo = OutwardParam.split("|");
                    for (var i = 0; i < arrFlightInfo.length; i++) {
                        arrFlightDetail = arrFlightInfo[i].split(":");
                        if (arrFlightDetail[0] == "departure_date") {
                            OutwardDate = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_departure_time") {
                            OutwardTime = parseInt(arrFlightDetail[1]);
                        }
                    }
                }
            }
        }

        iCount = 0;
        if (chkReturn != null) {
            iCount = chkReturn.length;
            if (iCount > 0) {
                for (var i = 0; i < iCount; i++) {
                    if (chkReturn[i].checked == true) {
                        ReturnParam = chkReturn[i].value;
                        break;
                    }
                }
                if (ReturnParam.length > 0) {
                    arrFlightInfo = ReturnParam.split("|");
                    for (var i = 0; i < arrFlightInfo.length; i++) {
                        arrFlightDetail = arrFlightInfo[i].split(":");
                        if (arrFlightDetail[0] == "departure_date") {
                            ReturnDate = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_departure_time") {
                            ReturnTime = parseInt(arrFlightDetail[1]);
                        }
                    }
                }
            }
        }

        if (OutwardParam.length == 0) {
            //Please Select your flight of origin.
            ShowMessageBox(objLanguage.Alert_Message_57, 0, '');
        }
        else if (objSelectedDate.length == 2 && ReturnParam.length == 0) {
            //Please select return flight.
            ShowMessageBox(objLanguage.Alert_Message_127, 0, '');
        }
        else if (ReturnParam.length != 0 && (OutwardDate > ReturnDate)) {
            //Please check your return Date! It must be equal to or higher than the Departure Date.
            ShowMessageBox(objLanguage.Alert_Message_5, 0, '');
        }
        else if (ReturnParam.length != 0 && (OutwardTime > ReturnTime) && (OutwardDate == ReturnDate)) {
            ////Please check your return Time! It must be equal to or higher than the Departure Time.
            ShowMessageBox(objLanguage.Alert_Message_58, 0, '');
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.AddFlight(OutwardParam, ReturnParam, g_strIpAddress, SuccessSelectFlight, showError, showTimeOut);
        }

        chkOutward = null;
        chkReturn = null;
    }
}

function SuccessSelectFlight(result) {
    var arrResult = result.split("{!}");

    if (arrResult.length == 1) {
        if (result == "{000}") {
            LoadSecure(true);
        }
        else if (result == "{200}") {
            //Please select return flight.
            ShowMessageBox(objLanguage.Alert_Message_127, 0, '');
        }
        else if (result == "{201}" || result == "{204}") {

            //Booking object is null.(Session time out).
            ShowMessageBox(objLanguage.Alert_Message_128, 0, '');
        }
    }
    else {
        if (arrResult[0] == "0") {
            //Success.
            var obj = document.getElementById("dvContainer");
            obj.innerHTML = arrResult[1];

            SetPassengerDetail();
            obj = null;

            //Display quote information
            GetSessionQuoteSummary();
            scroll(0, 0);

            ShowProgressBar(false);
        }
        else if (arrResult[0] == "101") {
            //US Message
            ShowMessageBox(objLanguage.Alert_Message_60.replace("{NO}", arrResult[1]));
        }
        else if (arrResult[0] == "200") {
            //Infant over limit
            ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
        } 
    }
}

// Start Search Flight Availability from Query string
function GenerateFromQueryString() {
    if (window.location.search.substring(1) != "") {
        // Check My Booking First
        if (checkMyBookingString()) {
            LoadCob(false, '');
        }
        else if (existEachQueryString("sn")) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.LoadManageBooking(queryStringValue("sn"), SuccessLoadCob, showError, showTimeOut);
        }
        else {
            searchFlightAvailFromQueryString();
        }
    }
}

function searchFlightAvailFromQueryString() {
    //ori=LGW&des=GCI&dep=2010-01-23&ret=2010-01-26&adt=1&chd=1&inf=1&pro=ww&bcls=Y&ft=L&currency=GBP

    if (!checkValidQueryStringForSearch()) {
        if (FoundOriginAndDestination() == true) {
            ShowAvailabilityPopup();
        }
    }
    else {   // If parameters are OK.
        // For check all set default value
        var origin = queryStringValue("ori");
        var destination = queryStringValue("des");

        if (origin != null && destination != null) {

            var bLff = queryStringValue("bLFF");
            var bPass = false;
            if (bLff == null) {
                bLff = false;
            }

            bPass = SetSearchValue(queryStringValue("ori"),
                                   queryStringValue("des"),
                                   queryStringValue("dep"),
                                   queryStringValue("ret"),
                                   queryStringValue("adt"),
                                   queryStringValue("chd"),
                                   queryStringValue("inf"),
                                   queryStringValue("bcls"),
                                   queryStringValue("pro"),
                                   bLff,
                                   queryStringValue("currency"),
                                   queryStringValue("ao"));

            if (bPass == true) {
                if (bLff == "false")
                { SearchAvailability('FARE', origin, destination); }
                else
                { SearchAvailability('FARE,LOWFARE', origin, destination); }
            }
        }
    }
}

function setSelectedOriginIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        var optValue = obj.options[i].value.split("|");
        if (optValue[0] == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}

function setSelectedDestinationIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        var optValue = obj.options[i].value.split("|");
        if (optValue[2] == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}

function setSelectedIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        if (obj.options[i].value == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}


function queryStringValue(strKey, strDefault) {
    if (existEachQueryString(strKey) == true) {
        if (strDefault == null)
        { strDefault = ""; }

        strKey = strKey.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

        var regex = new RegExp("[\\?&]" + strKey + "=([^&#]*)");
        var qs = regex.exec(window.location.href);

        if (qs == null)
        { return strDefault; }
        else
        { return qs[1]; } 
    }
    else {
        return null;
    }
}

function checkValidQueryStringForSearch() {
    var result = true;

    // Parameter "ori" is Origin Acronym. Required.
    result = FoundOriginAndDestination();

    // Parameter "dep" is Departure Date. Its format is yyyy-mm-dd. Required.
    if (!existEachQueryString("dep")) {
        result = false;
    }
    else {
        var dep = queryStringValue("dep");
        if (dep == "") {
            result = false;
        }
        else {
            if (!validateQueryStringDate(dep)) {
                result = false;
            }
        }
    }

    // Parameter "ret" is Return Date. Its format is yyyy-mm-dd. Required if round trip.
    if (existEachQueryString("ret")) {
        var ret = queryStringValue("ret");
        if (ret == "") {
            result = false;
        }
        else {
            if (!validateQueryStringDate(ret)) {
                result = false;
            }
        }
    }

    // Parameter "adt" is Adult Passenger amount. Required.
    if (!existEachQueryString("adt")) {
        result = false;
    }
    else {
        var adult = queryStringValue("adt");
        if (adult == "") {
            result = false;
        }
        else {
            if (!isNum(adult)) {
                result = false;
            }
        }
    }

    // Parameter "chd" is Child Passenger amount. Required.
    if (!existEachQueryString("chd")) {
        result = false;
    }
    else {
        var child = queryStringValue("chd");
        if (child == "") {
            result = false;
        }
        else {
            if (!isNum(child)) {
                result = false;
            }
        }
    }

    // Parameter "inf" is Infant Passenger amount. Required.
    if (!existEachQueryString("inf")) {
        result = false;
    }
    else {
        var infant = queryStringValue("inf");
        if (infant == "") {
            result = false;
        }
        else {
            if (!isNum(infant)) {
                result = false;
            }
        }
    }

    // Parameter "pro" is Promotion Code.
    if (existEachQueryString("pro")) {
        var procode = queryStringValue("pro");
        if (procode == "") {
            result = false;
        }
    }

    // Parameter "bcls" is Boarding Class.
    if (existEachQueryString("bcls")) {
        var bclass = queryStringValue("bcls");
        if (bclass == "") {
            result = false;
        }
    }

    // Parameter "ft" is Fare Type.
    if (existEachQueryString("ft")) {
        var faretype = queryStringValue("ft");
        if (faretype == "") {
            result = false;
        }
    }

    return result;
}
function FoundOriginAndDestination()
{
    var result = true;

    // Parameter "ori" is Origin Acronym. Required.
    if (!existEachQueryString("ori")) {
        result = false;
    }
    else {
        if (queryStringValue("ori") == "") {
            result = false;
        }
    }

    // Parameter "des" is Destination Acronym. Required.
    if (!existEachQueryString("des")) {
        result = false;
    }
    else {
        if (queryStringValue("des") == "") {
            result = false;
        }
    }

    return result;
}
function existEachQueryString(qryValue) {
    var hu = window.location.search.substring(1);
    var gy = hu.split("&");
    var found = false;
    for (i = 0; i < gy.length; i++) {
        ft = gy[i].split("=");
        if (ft[0] == qryValue) {
            found = true;
        }
    }
    return found;
}
function checkExistQueryString() {
    var hu = window.location.search.substring(1);
}
function validateQueryStringDate(dtValue) {
    var splitDate = dtValue.split("-");
    var refDate = new Date(splitDate[1] + "/" + splitDate[2] + "/" + splitDate[0]);
    if (splitDate[1] < 1 || splitDate[1] > 12 || refDate.getDate() != splitDate[2] || splitDate[0].length != 4 || (!/^20/.test(splitDate[0]))) {
        return false;
    }
    else {
        return true;
    }
}
function isNum(string) {
    var numericExpression = /^[0-9]+$/;
    if (string.match(numericExpression)) {
        return true;
    } else {
        return false;
    }
}
function generateYearMonth(strDate) {
    var arrDate = strDate.split("-");
    return arrDate[0] + arrDate[1];
}
function generateDate(strDate) {
    var arrDate = strDate.split("-");
    return arrDate[2];
}
function checkMyBookingString() {
    // mb=l
    if (existEachQueryString("mb") && queryStringValue("mb") == "l") {
        return true;
    }
    else {
        return false;
    }
}
function GetLowFareFinder() {
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    var arrAirport = objDestination.options[objDestination.selectedIndex].value.split("|");

    var strOrigin = arrAirport[0];
    var strDestination = arrAirport[2];
    var objOneWay = document.getElementById("optOneWay");

    var dtDateFrom = getdatefrom("1");
    var dtDateTo = 0;

    if (objOneWay.checked == false)
    { dtDateTo = getdateto("2"); }

    //Start Check Minimum Date that can select low fare finder.
    var d = Number(dtDateFrom.toString().substring(6, 8));
    var m = Number(dtDateFrom.toString().substring(4, 6));
    var y = Number(dtDateFrom.toString().substring(0, 4));

    var iDate = new Date(y, m - 1, d);

    var todayDate = new Date();
    var cDate = new Date(todayDate.getYear(), todayDate.getMonth(), todayDate.getDate());
    //End Check Minimum Date that can select low fare finder.

    if (dtDateTo != 0 && (dtDateFrom > dtDateTo)) {
    
        ShowMessageBox(objLanguage.Alert_Message_59, 0, ''); 
    }
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.GetLowFareFinder(strOrigin, strDestination, dtDateFrom, dtDateTo, SuccessGetLowFareFinder, showError, dtDateFrom + "|" + dtDateTo);
    }
    objDestination = null;
}

function GetNextLowFareFinder(searchDate, flightType) {
    ShowProgressBar(true);
    if (flightType == 'Return')
        tikAeroB2C.WebService.B2cService.GetLowFareFinder("", "", "0", searchDate, SuccessGetLowFareFinder, showError, showTimeOut);
    else if (flightType == 'OutWard')
        tikAeroB2C.WebService.B2cService.GetLowFareFinder("", "", searchDate, "0", SuccessGetLowFareFinder, showError, showTimeOut);
}

function SuccessGetLowFareFinder(result, strSelectDate) {
    if (result.length > 0) {
        var obj = document.getElementById("dvContainer");
        obj.innerHTML = result;
        var arrDate = strSelectDate.split("|");
        var objOutWare = document.getElementsByName("optLowfare_OutWard");
        var objReturn = document.getElementsByName("optLowfare_Return");

        var todayDate = new Date();
        var cDate = new Date(todayDate.getYear(), todayDate.getMonth(), todayDate.getDate());

        var strSelectDate;
        var d = Number(strSelectDate.substring(6, 8));
        var m = Number(strSelectDate.substring(4, 6));
        var y = Number(strSelectDate.substring(0, 4));

        var iDate;
        var objJSON;

        if (objOutWare != null && objOutWare.length > 0) {
            for (var i = 0; i < objOutWare.length; i++) {
                objJSON = eval("(" + objOutWare[i].value + ")");
                strSelectDate = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                var d = Number(strSelectDate.substring(6, 8));
                var m = Number(strSelectDate.substring(4, 6));
                var y = Number(strSelectDate.substring(0, 4));

                iDate = new Date(y, m - 1, d);
                //Remove date at are not match the 21 days
                if (dateDiff("d", cDate, iDate) <= 21) {
                    objOutWare[i].style.display = "none";
                }
                //Select at selected date
                if (strSelectDate == arrDate[0]) {
                    objOutWare[i].checked = true;
                }
                iDate = null;
            }
            //Highlight Selected date
            LffHighLight("Outward", true);
        }

        if (objReturn != null && objReturn.length > 0) {
            for (var i = 0; i < objReturn.length; i++) {
                objJSON = eval("(" + objReturn[i].value + ")");
                strSelectDate = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                var d = Number(strSelectDate.substring(6, 8));
                var m = Number(strSelectDate.substring(4, 6));
                var y = Number(strSelectDate.substring(0, 4));

                iDate = new Date(y, m - 1, d);
                //Remove date at are not match the 21 days
                if (dateDiff("d", cDate, iDate) <= 21) {
                    objReturn[i].style.display = "none";
                }
                //Select at selected date
                if (strSelectDate == arrDate[1]) {
                    objReturn[i].checked = true;
                }
            }
            //Highlight Selected date
            LffHighLight("Return", true);
        }
        ShowSearchPannel(false);
        objOutWare = null;
        objReturn = null;
    }
    ShowProgressBar(false);
}
function GetAvailabilityFromLowFareFinder() {
    var objOptLowFare;

    var strParam = "";
    var strDateFrom = 0;
    var strDateTo = 0;
    var dblOutwarFare = 0;
    var dblRetrunFare = 0;
    var objJSON;

    //-- Outward Flight
    objOptLowFare = document.getElementsByName("optLowfare_Outward");
    if (objOptLowFare != null) {
        for (var i = 0; i < objOptLowFare.length; i++) {
            if (objOptLowFare[i].checked == true) {
                objJSON = eval("(" + objOptLowFare[i].value + ")");

                strParam = objJSON.origin_rcd + "|" + objJSON.origin_name + "|" + objJSON.destination_rcd + "|1";
                strDateFrom = ReformatXmlToYYYYMMDD(objJSON.departure_date);

                var objOutwardNetTotal = document.getElementById("spnFareNetTotal_Outward");
                if (objOutwardNetTotal != null) {
                    dblOutwarFare = parseFloat(objOutwardNetTotal.innerHTML);
                    objOutwardNetTotal = null;
                }
            }
        }
    }
    objOptLowFare = null;

    //-- Return Flight
    objOptLowFare = document.getElementsByName("optLowfare_Return");
    if (objOptLowFare != null) {
        for (var i = 0; i < objOptLowFare.length; i++) {
            if (objOptLowFare[i].checked == true) {
                objJSON = eval("(" + objOptLowFare[i].value + ")");

                strDateTo = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                var objReturnNetTotal = document.getElementById("spnFareNetTotal_Return");
                if (objReturnNetTotal != null) {
                    dblRetrunFare = parseFloat(objReturnNetTotal.innerHTML);
                    objReturnNetTotal = null;
                }
            }
        }
    }
    objOptLowFare = null;

    if (strDateFrom != "") {
        if (strDateFrom > strDateTo && strDateTo != 0) {
            //Please check your return Date! It must be equal to or higher than the Departure Date
            ShowMessageBox(objLanguage.Alert_Message_5, 0, '');
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.GetAvailabilityFromLowFareFinder(strParam,
                                                                          strDateFrom,
                                                                          strDateTo,
                                                                          false,
                                                                          dblOutwarFare,
                                                                          dblRetrunFare,
                                                                          tmpOutFlight_id + ":" + tmpOutFare_id,
                                                                          tmpRetFlight_id + ":" + tmpRetFare_id,
                                                                          SuccessGetAvailabilityFromLowFareFinder,
                                                                          showError,
                                                                          dblOutwarFare + "|" + dblRetrunFare);
        }
    }
    else {
        //"Please select outward flight"
        ShowMessageBox(objLanguage.Alert_Message_57, 0, ''); 
    }
}
function SuccessGetAvailabilityFromLowFareFinder(result, strFareAmount) {
    if (result.length > 0) {
        var obj = document.getElementById("dvContainer");
        obj.innerHTML = result.split("{}")[1];

        var chkOutward = document.getElementsByName("Outward");
        var chkReturn = document.getElementsByName("Return");

        if (chkOutward != null && chkOutward.length > 0) {
            chkOutward[0].checked = true;
        }
        if (chkReturn != null && chkReturn.length > 0) {
            chkReturn[0].checked = true;
        }
    }
    ShowProgressBar(false);
}

function GetFlightSelectValue(o) {
    for (var i = 0; i < o.length; i++) {
        if (o[i].checked == true) {
            return o[i].value;
        }
    }
    return "";
}
function setFromDate(strDate) {
    SetComboValue("ddlMY_1", strDate.substring(0, 6));
    SetComboValue("ddlDate_1", strDate.substring(6, 8));
}
function setToDate(strDate) {
    SetComboValue("ddlMY_2", strDate.substring(0, 6));
    SetComboValue("ddlDate_2", strDate.substring(6, 8));
}

function LffHighLight(strName, bSelect) {
    var objOptLowFare = document.getElementsByName("optLowfare_" + strName);
    var strId = "";
    if (objOptLowFare != null) {
        for (var i = 0; i < objOptLowFare.length; i++) {
            if (objOptLowFare[i].checked == true) {
                strId = objOptLowFare[i].id.split("_")[2];
            }
        }
    }

    var objTd = document.getElementById("td_" + strName + "_" + strId);
    var ObjSpn = document.getElementById("spnLowfare_" + strName + "_" + strId);
    if (bSelect == true) {
        if (objTd != null)
        { objTd.className = "Select"; }

        if (ObjSpn != null)
        { ObjSpn.className = "Lowfarepriceselect"; }

    }
    else {
        if (objTd != null)
        { objTd.className = ""; }
        if (ObjSpn != null)
        { ObjSpn.className = "Lowfareprice"; }
    }

    objOptLowFare = null;
}

function SelectLlf(strName) {
    LffHighLight(strName.split("_")[0], false);
    document.getElementById("optLowfare_" + strName).checked = true;
    LffHighLight(strName.split("_")[0], true);
    return false;
}
function GetFlightTime(strId) {
    var objTime = document.getElementById("dvTime_" + strId);
    objTime.style.display = "block";
    objTime = null;
}

function ViewFareLowFareFinder(strObj)
{
    var arrFlightInfo;
    var arrFlightDetail;
    
    var strDepartDate = "";
    var objJSON;
    var obj = document.getElementById(strObj);
    var strFlightType = obj.name.split("_")[1];
    
    objJSON = eval("(" + obj.value + ")");
    strDepartDate = objJSON.departure_date;

    //Assign Value.
    var objAdultFare = document.getElementById("spn_" + strFlightType + "_Adultfare");
    var objChildFare = document.getElementById("spn_" + strFlightType + "_Childfare");
    var objInfantFare = document.getElementById("spn_" + strFlightType + "_Infantfare");
    var dblTotalAdultFare = 0;
    var dblTotalChildFare = 0;
    var dblTotalInfFare = 0;
    var intColumnNumber = obj.id.split("_")[3];
   
}


function ClearShowTime(strElement, event) 
{
    var current_mouse_target = null;
    var element = document.getElementById(strElement);
    
    if( event.toElement ) {				
	    current_mouse_target = event.toElement;
    } else if( event.relatedTarget ) {				
	    current_mouse_target = event.relatedTarget;
    }
    if( !is_child_of(element, current_mouse_target) && element != current_mouse_target ) 
    {
        //element.innerHTML = "";
        element.style.display = "none";
    }
    element = null;
}

function is_child_of(parent, child) {
    if (child != null) {
        while (child.parentNode) {
            if ((child = child.parentNode) == parent) {
                return true;
            }
        }
    }
    return false;
}
function GetQuoteSummary(objChk, strType) {
    var objOutwards = document.getElementsByName("Outward");
    var objReturns = document.getElementsByName("Return");
    var lenOutward = objOutwards.length;
    var lenReturn = objReturns.length;
    var objClassName = "";

    var sOutward = "";

    for (var i = 0; i < lenOutward; i++) {
        if (objOutwards[i] != null) {
            //alert("objOutwards :" + objOutwards[i].checked);
            sOutward = objOutwards[i].id;
            if (objOutwards[i].checked == true) {
                document.getElementById(sOutward.substring(3)).className = "SearchFlightSelection";
            }
            else {
                objClassName = "BodyCOL" + (5 + parseInt(sOutward.substring(sOutward.length - 1)));
                document.getElementById(sOutward.substring(3)).className = objClassName;
            }
        }
    }

    var sReturn = "";
    for (var i = 0; i < lenReturn; i++) {
        if (objReturns[i] != null) {
            //alert("objReturns :" + objReturns[i].checked);
            sReturn = objReturns[i].id;
            if (objReturns[i].checked == true) {
                document.getElementById(sReturn.substring(3)).className = "SearchFlightSelection";
            }
            else {
                objClassName = "BodyCOL" + (5 + parseInt(sReturn.substring(sReturn.length - 1)));
                document.getElementById(sReturn.substring(3)).className = objClassName;
            }
        }
    }
    var strXml = "";
    var strParam = "";
    var strAirlineRcd = "";
    var strFlightNumber = "";
    var strFlightId = "";
    var strOrigin = "";
    var strDest = "";
    var strFareId = "";
    var strFlightConnectId = "";
    var dtDepartture = "";
    var departTime = "";
    var arrivalTime = "";

    var strTransitAirlineRcd = "";
    var strTransitFlightNumber = "";
    var dtTransitDepartureDate = "";
    var transitDepartTime = "";
    var transitArrivalTime = "";
    var strTransitAirport;
    var strTransitFareId = "";
    var strBookingClassRcd = "";

    var arrFlightInfo;
    var arrFlightDetail;

    if (objChk.checked == true) {
        strParam = objChk.value;
        if (strParam.length > 0) {
            arrFlightInfo = strParam.split("|");
            for (var i = 0; i < arrFlightInfo.length; i++) {
                arrFlightDetail = arrFlightInfo[i].split(":");
                if (arrFlightDetail[0] == "flight_id") {
                    strFlightId = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "airline_rcd") {
                    strAirlineRcd = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "flight_number") {
                    strFlightNumber = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "origin_rcd") {
                    strOrigin = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "destination_rcd") {
                    strDest = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "fare_id") {
                    strFareId = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "transit_flight_id") {
                    strFlightConnectId = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "departure_date") {
                    dtDepartture = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "planned_departure_time") {
                    departTime = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "planned_arrival_time") {
                    arrivalTime = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "transit_airline_rcd") {
                    strTransitAirlineRcd = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "transit_flight_number") {
                    strTransitFlightNumber = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "transit_departure_date") {
                    dtTransitDepartureDate = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "transit_planned_departure_time") {
                    transitDepartTime = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "transit_planned_arrival_time") {
                    transitArrivalTime = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "transit_airport_rcd") {
                    strTransitAirport = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "transit_fare_id") {
                    strTransitFareId = arrFlightDetail[1];
                }
                if (arrFlightDetail[0] == "booking_class_rcd") {
                    strBookingClassRcd = arrFlightDetail[1];
                }
            }
            if (strOrigin.length > 0) {
                strXml = "<flights>" +
                            "<flight>" +
                                "<flight_id>" + strFlightId + "</flight_id>" +
                                "<airline_rcd>" + strAirlineRcd + "</airline_rcd>" +
                                "<flight_number>" + strFlightNumber + "</flight_number>" +
                                "<origin_rcd>" + strOrigin + "</origin_rcd>" +
                                "<destination_rcd>" + strDest + "</destination_rcd>" +
                                "<fare_id>" + strFareId + "</fare_id>" +
                                "<transit_airline_rcd>" + strTransitAirlineRcd + "</transit_airline_rcd>" +
                                "<transit_flight_number>" + strTransitFlightNumber + "</transit_flight_number>" +
                                "<transit_flight_id>" + strFlightConnectId + "</transit_flight_id>" +
                                "<departure_date>" + dtDepartture + "</departure_date>" +
                                "<departure_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtDepartture)) + "</departure_day>" +
                                "<planned_departure_time>" + departTime + "</planned_departure_time>" +
                                "<planned_arrival_time>" + arrivalTime + "</planned_arrival_time>" +
                                "<transit_departure_date>" + dtTransitDepartureDate + "</transit_departure_date>" +
                                "<transit_departure_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtTransitDepartureDate)) + "</transit_departure_day>" +
                                "<transit_planned_departure_time>" + transitDepartTime + "</transit_planned_departure_time>" +
                                "<transit_planned_arrival_time>" + transitArrivalTime + "</transit_planned_arrival_time>" +
                                "<transit_airport_rcd>" + strTransitAirport + "</transit_airport_rcd>" +
                                "<transit_fare_id>" + strTransitFareId + "</transit_fare_id>" +
                                "<booking_class_rcd>" + strBookingClassRcd + "</booking_class_rcd>" +
                            "</flight>" +
                        "</flights>";
                //Call Webservice
                tikAeroB2C.WebService.B2cService.GetQuoteSummary(strXml,
                                                                 strType,
                                                                 SuccessGetQuoteSummary,
                                                                 showError,
                                                                 strType);
            }
        }
    }
}
function SuccessGetQuoteSummary(result, strType) {

    if (result == "{100}" || result == "{101}" || result == "{004}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
    }
    else {

        if (strType == "Outward") {
            DisplayQuoteSummary("", result, "");
        }
        else {
            DisplayQuoteSummary("", "", result);
         }
        

        var objSubTotalFare = document.getElementById("dv_SubtotalFare");
        var objOutwardTotalIncl = document.getElementById("dv_Outward_TotalInclTax");
        var objReturnTotalIncl = document.getElementById("dv_Return_TotalInclTax");

        //Initialize Collape
        InitializeFareSummaryCollapse(strType);

        //Find Total of all Fare
        if (objSubTotalFare != null) {
            if (objReturnTotalIncl != null) {
                if (objReturnTotalIncl != null) {
                    if (objOutwardTotalIncl != null && objReturnTotalIncl != null) {
                        objSubTotalFare.innerHTML = AddCommas((parseFloat("0" + RemoveCommas(objOutwardTotalIncl.innerHTML)) + parseFloat("0" + RemoveCommas(objReturnTotalIncl.innerHTML))).toFixed(2));
                    }
                }
            }
            else {
                if (objOutwardTotalIncl != null) {
                    objSubTotalFare.innerHTML = objOutwardTotalIncl.innerHTML;
                }
            }
        }
    }
}
function SelectTime(strObj) {
    if (strObj.length > 0) {
        var obj = document.getElementById(strObj);
        if (obj != null && obj.parentNode.parentNode.parentNode != null) {
            var strType = obj.parentNode.parentNode.parentNode.id.split("_")[1];
            var objJSON = eval("(" + obj.value + ")");

            switch (strType) {
                case "Outward":
                    tmpOutFare_id = objJSON.fare_id.replace('{', '');
                    tmpOutFare_id = tmpOutFare_id.replace('}', '');
                    tmpOutFlight_id = objJSON.flight_id.replace('{', '');
                    tmpOutFlight_id = tmpOutFlight_id.replace('}', '');
                    break;
                case "Return":
                    tmpRetFare_id = objJSON.fare_id.replace('{', '');
                    tmpRetFare_id = tmpRetFare_id.replace('}', '');
                    tmpRetFlight_id = objJSON.flight_id.replace('{', '');
                    tmpRetFlight_id = tmpRetFlight_id.replace('}', '');
                    break;
            }

            var strXml = "<flights>" +
                         "<flight>" +
                            "<airline_rcd>" + objJSON.airline_rcd + "</airline_rcd>" +
                            "<flight_number>" + objJSON.flight_number + "</flight_number>" +
                            "<flight_id>" + objJSON.flight_id + "</flight_id>" +
                            "<origin_rcd>" + objJSON.origin_rcd + "</origin_rcd>" +
                            "<destination_rcd>" + objJSON.destination_rcd + "</destination_rcd>" +
                            "<booking_class_rcd>" + objJSON.booking_class_rcd + "</booking_class_rcd>" +
                            "<fare_id>" + objJSON.fare_id + "</fare_id>" +
                            "<transit_flight_id>" + objJSON.transit_flight_id + "</transit_flight_id>" +
                            "<departure_date>" + objJSON.departure_date + "</departure_date>" +
                            "<transit_departure_date>" + objJSON.transit_departure_date + "</transit_departure_date>" +
                            "<transit_airport_rcd>" + objJSON.transit_airport_rcd + "</transit_airport_rcd>" +
                            "<planned_departure_time>" + objJSON.planned_departure_time + "</planned_departure_time>" +
                            "<planned_arrival_time>" + objJSON.planned_arrival_time + "</planned_arrival_time>" +
                            "<transit_fare_id>" + objJSON.transit_fare_id + "</transit_fare_id>" +
                        "</flight>" +
                    "</flights>";
            //Call Webservice
            tikAeroB2C.WebService.B2cService.GetQuoteSummary(strXml,
                                                            strType,
                                                            SuccessGetQuoteSummary,
                                                            showError,
                                                            strType);
        }

    }

}

function SearchAvailabilityParameter() {
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var strOrigin = objOrigin.options[objOrigin.selectedIndex].value;
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    var objOneWay = document.getElementById("optOneWay");
    var objAdult = document.getElementById("optAdult");
    var objChild = document.getElementById("optChild");
    var objInfant = document.getElementById("optInfant");
    var objBoardingClass = document.getElementById("optBoardingClass");
    var objPromoCode = document.getElementById("txtPromoCode");
    var objYP = document.getElementById("optYP");


    var strDestination = objDestination.options[objDestination.selectedIndex].value;
    var isOneWay = objOneWay.checked;
    var iAdult = parseInt(objAdult.options[objAdult.selectedIndex].value);
    var iChild = parseInt(objChild.options[objChild.selectedIndex].value);
    var iInfant = parseInt(objInfant.options[objInfant.selectedIndex].value);
    var strBoardingClass = objBoardingClass.options[objBoardingClass.selectedIndex].value;
    var currencyCode = strOrigin.split("|")[1];
    var dtDateFrom = getdatefrom("1");
    var dtDateTo = getdateto("2");
    var strPromoCode = "";
    var iOther = 0;
    var otherType = "";

    // Check Ip address for Default Currency 
    var obtCurrency = document.getElementById("AvailabilitySearch1_optCurrency");

    if (obtCurrency != null) {
        currencyCode = obtCurrency.options[obtCurrency.selectedIndex].value;
    }
    // End Check Ip address for Default Currency 
    if (objYP != null) {
        iOther = parseInt(objYP.options[objYP.selectedIndex].value);
        otherType = "YP";
    }

    if (objPromoCode != null) {
        strPromoCode = objPromoCode.value;
    }

    //Call Availability web service
    if (iInfant > iAdult) {
        //Infant Can't be more than adult.
        ShowMessageBox(objLanguage.Alert_Message_2, 0, '');
    }
    else if ((iAdult + iChild + iInfant) > 12) {
        //Total passenger select can't be more than 12
        ShowMessageBox(objLanguage.objLanguage.Alert_Message_53.replace("{NO}", 12), 0, '');
    }
    else if ((iAdult + iChild + iInfant + iOther) == 0) {
        //Please select number of passenger.
        ShowMessageBox(objLanguage.Alert_Message_6, 0, '');
    }
    else {
        //Popup new window for search result

        var strParameter = "?ori=" + strOrigin.split("|")[0] +
                            "&des=" + strDestination.split("|")[2] +
                            "&dep=" + ReformatYYYYMMDDToDashDate(dtDateFrom) +
                            ((isOneWay == false) ? "&ret=" + ReformatYYYYMMDDToDashDate(dtDateTo) : "") +
                            "&adt=" + iAdult +
                            "&chd=" + iChild +
                            "&inf=" + iInfant +
                            "&currency=" + currencyCode +
                            "&pro=" + strPromoCode +
                            "&langculture=" + queryStringValue("langculture", "en-us");

        var strUrl = GetVirtualDirectory() + "default.aspx" + strParameter;
        window.open(strUrl, "", "scrollbars=1,status=1,toolbar=0,resizable=1", "");
    }

    objOrigin = null;
    objDestination = null;
    objOneWay = null;
    objAdult = null;
    objChild = null;
    objInfant = null;
    objBookingClass = null;
    objPromoCode = null;
}

function ShowAvailabilityBookingSummary() {
    var dtDateFrom = getdatefrom("1");
    var dtDateTo = getdateto("2");

    var chkOutward = document.getElementsByName("Outward");
    var chkReturn = document.getElementsByName("Return");

    //Get Summary for Outward flight.
    if (chkOutward != null && chkOutward.length > 0) {
        chkOutward[0].checked = true;
        for (var i = 0; i < chkOutward.length; i++) {

            if (GetArrayParamValue(chkOutward[i].value, "departure_date") == dtDateFrom) {

                chkOutward[i].checked = true;
                GetQuoteSummary(chkOutward[i], "Outward");
                break;
            }
        }

    }
    chkOutward = null;

    //Get Summary for Return flight.
    if (chkReturn != null && chkReturn.length > 0) {
        chkReturn[0].checked = true;
        for (var i = 0; i < chkReturn.length; i++) {
            if (GetArrayParamValue(chkReturn[i].value, "departure_date") == dtDateTo) {
                chkReturn[i].checked = true;
                GetQuoteSummary(chkReturn[i], "Return");
                break;
            }
        }
    }
    chkReturn = null;
}

function DisplayQuoteSummary(strFareSummaryHtml, strOutwardHtml, strReturnHtml) {

    var objDvFareSummary = document.getElementById("dvFareSummary");
    var objDvAvaiFareSummary = document.getElementById("dvAvaiQuote");

    var objOFareLine = document.getElementById("dvFareLine_Outward");
    var objRFareLine = document.getElementById("dvFareLine_Return");

    if (strFareSummaryHtml.length > 0) {
        if (objDvFareSummary != null) {
            objDvFareSummary.style.display = "block";
            objDvFareSummary.innerHTML = strFareSummaryHtml;
        }
    }
    else {
        if (objDvFareSummary != null) {
            objDvFareSummary.style.display = "none";
            objDvFareSummary.innerHTML = "";
        }
    }

    if (strOutwardHtml.length > 0 | strReturnHtml.length > 0) {
        if (objDvAvaiFareSummary != null) {
            objDvAvaiFareSummary.style.display = "block";

            if (strOutwardHtml.length > 0) {
                objOFareLine.innerHTML = strOutwardHtml;
            }

            if (strReturnHtml.length > 0) {
                objRFareLine.innerHTML = strReturnHtml;
            }
        }
    }
    else {
        if (objDvAvaiFareSummary != null) {
            objDvAvaiFareSummary.style.display = "none";

            objOFareLine.innerHTML = "";
            objRFareLine.innerHTML = "";
        }
    }
    
}

function LowfarefinderChangeDate(strDate, strFlightType) {
    var bPass = true;

    // Set Origin DropDownList            
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    if (objOrigin != null) {
        var origin = queryStringValue("ori");
        var canSet = setSelectedOriginIndex(objOrigin, origin);
        if (!canSet) {
            bPass = false;
        }
    }
    else {
        bPass = false;
    }

    // Set Destination DropDownLis before set value from query string.
    getDestination();

    // Set Destination DropDownList            
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    if (objDestination != null) {
        var destination = queryStringValue("des");
        var canSet = setSelectedDestinationIndex(objDestination, destination);
        if (!canSet) {
            bPass = false;
        }
    }
    else {
        bPass = false;
    }

    if (strFlightType == "Outward") {
        // Set Departure Date
        var ddlMY_1 = document.getElementById('ddlMY_1');
        var depDate = strDate.substring(0, 4) + '-' + strDate.substring(4, 6) + '-' + strDate.substring(6, 8);
        if (ddlMY_1 != null) {
            var canSet = setSelectedIndex(ddlMY_1, generateYearMonth(depDate));
            if (!canSet) {
                bPass = false;
                ShowMessageBox(objLanguage.Alert_Message_3, 0, '');
            }
            else {
                hiddenCalendar(1, ddlMY_1);
            }
        }
        else {
            bPass = false;
        }
        var ddlDate_1 = document.getElementById('ddlDate_1');
        if (ddlDate_1 != null) {
            var canSet = setSelectedIndex(ddlDate_1, generateDate(depDate));
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        if (existEachQueryString("dep") && existEachQueryString("ret")) {
            // Set Return Date
            var ddlMY_2 = document.getElementById('ddlMY_2');
            var retDate = strDate.substring(0, 4) + '-' + strDate.substring(4, 6) + '-' + strDate.substring(6, 8);
            if (ddlMY_2 != null) {
                var canSet = setSelectedIndex(ddlMY_2, generateYearMonth(retDate));
                if (!canSet) {
                    bPass = false;
                }
                else {
                    hiddenCalendar(2, ddlMY_2);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_2 = document.getElementById('ddlDate_2');
            if (ddlDate_2 != null) {
                var canSet = setSelectedIndex(ddlDate_2, generateDate(retDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }
    }

    if (strFlightType == "Return") {
        var currentTime = SetFormatDatYYYYMMDD(new Date());

        if (getdatefrom("1") > strDate) {
            // Set Departure Date
            var ddlMY_1 = document.getElementById('ddlMY_1');

            // Validate old time
            if (currentTime > (strDate - 1))
                strDate = currentTime;
            else
                strDate = String(strDate - 1);

            var depDate = strDate.substring(0, 4) + '-' + strDate.substring(4, 6) + '-' + (strDate.substring(6, 8));
            if (ddlMY_1 != null) {
                var canSet = setSelectedIndex(ddlMY_1, generateYearMonth(depDate));
                if (!canSet) {
                    bPass = false;
                    ShowMessageBox(objLanguage.Alert_Message_3, 0, '');
                }
                else {
                    hiddenCalendar(1, ddlMY_1);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_1 = document.getElementById('ddlDate_1');
            if (ddlDate_1 != null) {
                var canSet = setSelectedIndex(ddlDate_1, generateDate(depDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }

        // Set Return Date
        var ddlMY_2 = document.getElementById('ddlMY_2');

        // Validate old time
        if (currentTime > strDate)
            strDate = currentTime;

        var retDate = strDate.substring(0, 4) + '-' + strDate.substring(4, 6) + '-' + strDate.substring(6, 8);
        if (ddlMY_2 != null) {
            var canSet = setSelectedIndex(ddlMY_2, generateYearMonth(retDate));
            if (!canSet) {
                bPass = false;
            }
            else {
                hiddenCalendar(2, ddlMY_2);
            }
        }
        else {
            bPass = false;
        }
        var ddlDate_2 = document.getElementById('ddlDate_2');
        if (ddlDate_2 != null) {
            var canSet = setSelectedIndex(ddlDate_2, generateDate(retDate));
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }
    }

    if (bPass == true) {
        SearchAvailability('FARE,LOWFARE', origin, destination);
    }
}

function SetFormatDatYYYYMMDD(date) {

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if (day < 10 && month < 10)
        return year + "0" + month + "0" + day;
    else if (day < 10 && month >= 10)
        return year + "" + month + "0" + day;
    else if (day >= 10 && month >= 10)
        return year + "" + month + "" + day;
    else
        return year + "" + month + "" + day;
}
function ShowAvailabilityPopup() {

    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ShowAvailabilityPopup(SuccessShowAvailabilityPopup, showError, showTimeOut);
}

function SuccessShowAvailabilityPopup(result) {

    var objHolder = document.getElementById("dvFormHolder");
    var objFading = document.getElementById("dvProgressBar");
    if (objHolder != null & objFading != null) {

        objHolder.innerHTML = result;

        //Setup control..
        var objDeptDay = document.getElementById("ddlDate_3");
        var objDeptMonth = document.getElementById("ddlMY_3");
        var objDeptCar = document.getElementById("calendar3");

        var objArrDay = document.getElementById("ddlDate_4");
        var objArrMonth = document.getElementById("ddlMY_4");
        var objArrCar = document.getElementById("calendar4");

        var objPaxAdult = document.getElementById("sePopAdult");
        var objPaxChild = document.getElementById("sePopChild");
        var objPaxInfant = document.getElementById("sePopInfant");

        //Enable control
        var optReturn = document.getElementById("optReturnPopup");
        var optOneWay = document.getElementById("optOneWayPopup");
        var bDep = existEachQueryString("dep");
        var bRet = existEachQueryString("ret");

        if (bDep == true || bRet == true) {
            //Departure Date.
            DisableCalendar(objDeptDay, objDeptMonth, objDeptCar, true);
            //Return Date.
            DisableCalendar(objArrDay, objArrMonth, objArrCar, true);

            if (bDep == true && bRet == false) {
                optOneWay.checked = true;
                ShowHideCalendar("optReturnPopup", "calRenPopup2", "lblReturnDatePopup");
            }
            else if (bDep == true && bRet == true && trim(queryStringValue("ret")).length == 0) {
                optReturn.checked = true;
                ShowHideCalendar("optReturnPopup", "calRenPopup2", "lblReturnDatePopup");
                //Return Date.
                DisableCalendar(objArrDay, objArrMonth, objArrCar, false);
            }
            //Return option
            if (optReturn != null) {
                optReturn.disabled = true;
            }
            if (optOneWay != null) {
                optOneWay.disabled = true;
            }
        }
        else {
            //Departure Date.
            DisableCalendar(objDeptDay, objDeptMonth, objDeptCar, false);

            //Arrival control
            DisableCalendar(objArrDay, objArrMonth, objArrCar, false);

            //Return option
            if (optReturn != null) {
                optReturn.disabled = false;
            }
            if (optOneWay != null) {
                optOneWay.disabled = false;
            }
        }

        if (!existEachQueryString("adt")) {
            if (objPaxAdult != null) {
                objPaxAdult.disabled = false;
            }
            if (objPaxChild != null) {
                objPaxChild.disabled = false;
            }
            if (objPaxInfant != null) {
                objPaxInfant.disabled = false;
            }
        }
        else {
            if (objPaxAdult != null) {
                setSelectedIndex(objPaxAdult, queryStringValue("adt"));
            }
            if (objPaxChild != null) {
                setSelectedIndex(objPaxChild, queryStringValue("chd"));
            }
            if (objPaxInfant != null) {
                setSelectedIndex(objPaxInfant, queryStringValue("inf"));
            }
        }

        //Set Departure Date
        var depDate = queryStringValue("dep");
        if (depDate != null) {
            var ddlMY_3 = document.getElementById('ddlMY_3');
            var ddlDate_3 = document.getElementById('ddlDate_3');
            if (ddlMY_3 != null) {
                setSelectedIndex(ddlMY_3, generateYearMonth(depDate));
            }
            if (ddlDate_3 != null) {
                setSelectedIndex(ddlDate_3, generateDate(depDate)); 
            }
        }

        // Set Return Date
        var retDate = queryStringValue("ret");
        if (bRet == true && retDate.length == 0) {
            retDate = depDate;
        }
        if (retDate != null) {
            var ddlMY_4 = document.getElementById('ddlMY_4');
            var ddlDate_4 = document.getElementById('ddlDate_4');
            if (ddlMY_4 != null) {
                setSelectedIndex(ddlMY_4, generateYearMonth(retDate));
            }
            if (ddlDate_4 != null) {
                setSelectedIndex(ddlDate_4, generateDate(retDate));
            }
        }

        $('#calendar3').daterangepicker().click(function () {
            $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
        });
        $('#calendar4').daterangepicker().click(function () {
            $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
        });

        objFading.style.display = "block";
        objHolder.style.display = "block";
    }
    ShowProgressBar(false);
}

function SetSearchValue(origin, destination, depDate, retDate, adult, child, infant, bclass, strPromo, bLff, strCurrency, strAgencyCode) {
    var bPass = true;

    // Set Origin DropDownList            
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    if (objOrigin != null) {
        if (origin != null) {
            var canSet = setSelectedOriginIndex(objOrigin, origin);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }
    }
    else {
        bPass = false;
    }

    //Set Destination DropDownLis before set value from query string.
    getDestination();

    //Set Destination DropDownList            
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    if (objDestination != null) {
        if (destination != null) {
            var canSet = setSelectedDestinationIndex(objDestination, destination);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }
    }
    else {
        bPass = false;
    }

    // Set Radio One-Way or Return, and Calendar DropDownList
    if (depDate != null && retDate != null) {   // If there are both Departure Date and Return Date, then set return
        // Set Radio
        var objReturn = document.getElementById("optReturn");
        if (objReturn != null) {
            objReturn.checked = true;
        }

        // Set Departure Date
        var ddlMY_1 = document.getElementById('ddlMY_1');
        if (depDate != null) {
            if (ddlMY_1 != null) {
                var canSet = setSelectedIndex(ddlMY_1, generateYearMonth(depDate));
                if (!canSet) {
                    bPass = false;
                    ShowMessageBox(objLanguage.Alert_Message_3, 0, '');
                }
                else {
                    hiddenCalendar(1, ddlMY_1);
                }
            }
            else {
                bPass = false;
            }

            var ddlDate_1 = document.getElementById('ddlDate_1');
            if (ddlDate_1 != null) {
                var canSet = setSelectedIndex(ddlDate_1, generateDate(depDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }


        // Set Return Date
        var ddlMY_2 = document.getElementById('ddlMY_2');
        if (retDate != null) {
            if (ddlMY_2 != null) {
                var canSet = setSelectedIndex(ddlMY_2, generateYearMonth(retDate));
                if (!canSet) {
                    bPass = false;
                }
                else {
                    hiddenCalendar(2, ddlMY_2);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_2 = document.getElementById('ddlDate_2');
            if (ddlDate_2 != null) {
                var canSet = setSelectedIndex(ddlDate_2, generateDate(retDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }
    }
    else if (depDate != null && retDate == null) { // If there is only Departure Date, then set to One Way
        // Set Radio
        var objOneWay = document.getElementById("optOneWay");
        if (objOneWay != null) {
            objOneWay.checked = true;
        }

        // Set Departure Date
        var ddlMY_1 = document.getElementById('ddlMY_1');
        if (depDate != null) {
            if (depDate != null) {
                if (ddlMY_1 != null) {
                    var canSet = setSelectedIndex(ddlMY_1, generateYearMonth(depDate));
                    if (!canSet) {
                        bPass = false;
                        ShowMessageBox(objLanguage.Alert_Message_3, 0, '');
                    }
                    else {
                        hiddenCalendar(1, ddlMY_1);
                    }
                }
                else {
                    bPass = false;
                }

                var ddlDate_1 = document.getElementById('ddlDate_1');
                if (ddlDate_1 != null) {
                    var canSet = setSelectedIndex(ddlDate_1, generateDate(depDate));
                    if (!canSet) {
                        bPass = false;
                    }
                }
                else {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }
    }
    ShowHideCalendar("optReturn", "calRen2", "lblReturnDate");

    // Set Adult Pax to Adult DropDownList
    var objAdult = document.getElementById("optAdult");
    if (objAdult != null) {
        if (adult != null) {
            var canSet = setSelectedIndex(objAdult, adult);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }
    }
    else {
        bPass = false;
    }

    // Set Child Pax to Child DropDownList
    var objChild = document.getElementById("optChild");
    if (objChild != null) {
        if (child != null) {
            var canSet = setSelectedIndex(objChild, child);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

    }
    else {
        bPass = false;
    }

    // Set Infant Pax to Infant DropDownList
    var objInfant = document.getElementById("optInfant");
    if (objInfant != null) {
        if (infant != null) {
            var canSet = setSelectedIndex(objInfant, infant);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }
    }
    else {
        bPass = false;
    }

    // Set Boarding Class DropDownList
    var objBoardingClass = document.getElementById("optBoardingClass");
    if (objBoardingClass != null) {
        if (bclass != null) {
            var canSet = setSelectedIndex(objBoardingClass, bclass);
            if (!canSet) {
                bPass = false;
            }
        }
    }

    // Set Promotion Code
    var objPromo = document.getElementById("txtPromoCode");
    if (objPromo != null) {
        if (strPromo != null) {
            objPromo.value = strPromo;
        }
    }

    // Set Search Type (Availibility or lowfare finder)
    if (existEachQueryString("bLFF")) {
        if (bLff == null) {
            bLff = false;
        }
    }

    // Set Currency Class DropDownList
    var obtCurrency = document.getElementById(FindControlName("select", "optCurrency"));
    if (obtCurrency != null) {
        if (obtCurrency != null) {
            if (strCurrency != null) {
                var canSet = setSelectedIndex(obtCurrency, strCurrency);
                if (!canSet) {
                    bPass = false;
                }
            }
        }
    }

    //Write agency code cookies.
    if (existEachQueryString("ao")) {
        if (getCookie("logAgency") == null) {
            if (strAgencyCode != null) {
                var dtExpired = new Date();
                dtExpired.setDate(dtExpired.getDate() + iAgencyCodeExpDays);
                setCookie("logAgency", $.base64.encode(strAgencyCode), dtExpired, "", "", false);
            }
        }
    }

    return bPass;
}
function PopupGetAvai() {

    var objAdult = document.getElementById("sePopAdult");
    var objChild = document.getElementById("sePopChild");
    var objInfant = document.getElementById("sePopInfant");

    var origin = queryStringValue("ori");
    var destination = queryStringValue("des");
    
    var dtDateFrom = getdatefrom("3");
    var dtDateTo = getdateto("4");

    var iAdult = 0;
    var iChild = 0;
    var iInfant = 0;

    //Number of passenger.
    if (objAdult != null) {
        iAdult = parseInt(objAdult.options[objAdult.selectedIndex].value);
    }
    if (objChild != null) {
        iChild = parseInt(objChild.options[objChild.selectedIndex].value);
    }
    if (objInfant != null) {
        iInfant = parseInt(objInfant.options[objInfant.selectedIndex].value);
    }

    if (ValidNumberOfPassenger(iAdult, iChild, iInfant, 0) == true) {
        if (parseInt(dtDateFrom) > parseInt(dtDateTo)) {
            ShowMessageBox(objLanguage.Alert_Message_55, 0, '');
        }
        else {
            var optReturn = document.getElementById("optReturnPopup");
            var bLff = queryStringValue("bLFF");
            var bPass = false;
            var strReturnDate;
            if (bLff == null) {
                bLff = false;
            }

            if (optReturn != null && optReturn.checked == true) {
                strReturnDate = ReformatYYYYMMDDToDashDate(getdateto("4"));
            }
            bPass = SetSearchValue(origin,
                           destination,
                           ReformatYYYYMMDDToDashDate(getdatefrom("3")),
                           strReturnDate,
                           GetControlValue(objAdult),
                           GetControlValue(objChild),
                           GetControlValue(objInfant),
                           queryStringValue("bcls"),
                           queryStringValue("pro"),
                           bLff,
                           queryStringValue("currency"),
                           queryStringValue("ao"));

            //Call search flight.
            if (bPass == true) {
                if (bLff == "false")
                { SearchAvailability('FARE', origin, destination); }
                else
                { SearchAvailability('FARE,LOWFARE', origin, destination); }
                ClosePassengerList();
            }
        }
    }
}
function DisableCalendar(objDay, objMonth, objCar, bValue) {
    if (objDay != null) {
        objDay.disabled = bValue;
    }
    if (objMonth != null) {
        objMonth.disabled = bValue;
    }
    if (objCar != null) {
        objCar.disabled = bValue;
    }
}