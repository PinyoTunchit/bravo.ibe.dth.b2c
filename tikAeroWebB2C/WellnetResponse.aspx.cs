﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using tikAeroB2C.Classes;
using tikSystem.Web.Library;

namespace tikAeroB2C
{
    public partial class WellnetResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ExternalPaymentAddPayment();
            }
        }

        private void ExternalPaymentAddPayment()
        {
            NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("WellnetPaymentSetting");
            string strBookingId = string.Empty;
            string strFormOfPayment = Setting["FormOfPayment"];
            string strCurrencyCode = Setting["CurrencyCode"];
            decimal dAmount = 0;
            string strFormOfPaymentSubType = string.Empty;
            string strDocumentNumber = string.Empty;
            string strLanguage = Setting["DisplayLanguage"];
            DateTime dtPayment = DateTime.Now;
            string strReturnValue = "000";//success code
            string strLogToWrite = string.Empty;
            Library objLi = new Library();

            System.Globalization.CultureInfo culture;
            culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

            if (Request.Form.Count > 0)
            {
                if (Request.Form["rno"] != null && Request.Form["rno"].Trim() != "")
                {
                    strBookingId = Request.Form["rno"].Substring(0, 6);
                    strDocumentNumber = Request.Form["rno"].ToString();
                    strLogToWrite += "BookingId : " + strBookingId + Environment.NewLine;
                    strLogToWrite += "DocumentNumber : " + strDocumentNumber + Environment.NewLine;
                }
                else { strReturnValue = "103"; }

                if (Request.Form["ccd"] != null && Request.Form["ccd"].Trim() != "")
                {
                    strFormOfPaymentSubType = Request.Form["ccd"].ToString();
                    strLogToWrite += "FormOfPaymentSubType : " + strFormOfPaymentSubType + Environment.NewLine;
                }
                else { strReturnValue = "103"; }

                if (Request.Form["pri"] != null && Request.Form["pri"].Trim() != "" && decimal.TryParse(Request.Form["pri"].Trim(), out dAmount))
                {
                    dAmount = decimal.Parse(Request.Form["pri"].ToString());
                    strLogToWrite += "Amount : " + dAmount.ToString() + Environment.NewLine;
                }
                else { strReturnValue = "103"; }

                //if (Request.Form["tcd"] != null) 
                //{
                //    strFormOfPaymentSubType = Request.Form["tcd"].ToString();
                //}

                if (Request.Form["ndt"] != null && Request.Form["ndt"].Trim() != "" && DateTime.TryParseExact(Request.Form["ndt"].Trim(), "yyyyMMddHHmmss", culture, System.Globalization.DateTimeStyles.None, out dtPayment))
                {
                    dtPayment = DateTime.ParseExact(Request.Form["ndt"].ToString(), "yyyyMMddHHmmss", null);
                    strLogToWrite += "PaymentDate : " + dtPayment.ToString("yyyyMMddHHmmss") + Environment.NewLine;
                }
                else { strReturnValue = "103"; }

                if (Request.Form["ret"] != null && Request.Form["ret"].Trim() == "")
                {
                    strReturnValue = "103";   // check status code from client is not empty.
                }


                if (strReturnValue != "103")
                {

                    //Initialize webservice
                    Agents objAgent = new Agents();
                    ServiceClient objService = new ServiceClient();
                    objService.initializeWebService(B2CSetting.DefaultAgencyCode, ref objAgent);

                    //get booking id
                    DataSet ds = objService.GetBookings("", "", "", DateTime.MinValue, DateTime.MinValue, strBookingId, ""
                                                , "", "", "", "", "", ""
                                                , "", "", "", false, "", false, false, DateTime.MinValue, DateTime.MinValue);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        strBookingId = ds.Tables[0].Rows[0]["booking_id"].ToString();
                        Payments objPayments = new Payments();
                        string strResult = string.Empty;

                        Agent objAgents = CacheHelper.CacheAgency(B2CSetting.DefaultAgencyCode);

                        strResult = objPayments.ExternalPaymentAddPayment(strBookingId, 
                                                                        objAgents.agency_code,                       
                                                                        strFormOfPayment,                            
                                                                        strCurrencyCode,                             
                                                                        dAmount,                                     
                                                                        strFormOfPaymentSubType,                     
                                                                        objAgents.default_user_account_id.ToString(),
                                                                        strDocumentNumber,                           
                                                                        string.Empty,
                                                                        string.Empty,
                                                                        strLanguage,                                 
                                                                        dtPayment,                                   
                                                                        false,
                                                                        SecurityHelper.GenerateSessionlessToken());                                                                         

                        System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
                        xdoc.LoadXml(strResult);
                        System.Xml.XmlNodeList nl = xdoc.SelectNodes("Booking/Error");
                        if (nl != null && nl.Count > 0)
                        {
                            strReturnValue = nl[0].ChildNodes[0].InnerText;
                            strLogToWrite += "ErrorMessage : " + nl[0].ChildNodes[1].InnerText + Environment.NewLine;
                        }

                        strLogToWrite += "ReturnValue : " + strReturnValue;

                        string strLogLocation = string.Empty;
                        if (string.IsNullOrEmpty(Setting["LogFilePath"]) == false)
                        {
                            strLogLocation = Setting["LogFilePath"] + @"\" + "Wellnet_" + String.Format("{0:yyyyMMdd}", DateTime.Now) + "_" + Guid.NewGuid() + ".log"; ;
                        }
                        objLi.WriteLog(strLogLocation, strLogToWrite);
                    }
                    else
                    {
                        strReturnValue = "406";  // 406 - booking information not found
                    }
                }


                //writeToLogFile(strLogToWrite);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "text/plain";
                strReturnValue = (strReturnValue == "412" ? "206" : strReturnValue);
                Response.Write(strReturnValue);
                Response.End();
            }
            else
            {
                strReturnValue = "999"; //999 - General error
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "text/plain";
                Response.Write(strReturnValue);
                Response.End();
            }

        }


        //public static void writeToLogFile(string logMessage)
        //{
        //    NameValueCollection Setting = (NameValueCollection)ConfigurationManager.GetSection("WellnetPaymentSetting");
        //    string strLogMessage = string.Empty;
        //    string strLogFile = string.Empty;
        //    StreamWriter swLog;

        //    if (Setting != null && Setting["logFilePath"] != null)
        //    {
        //        strLogFile = HttpContext.Current.Server.MapPath(Setting["logFilePath"]) + "WellnetResponse_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
        //        strLogMessage = string.Format("{0} {1}", DateTime.Now.ToString("yyyyMMddHHmmss"), Environment.NewLine + logMessage);

        //        if (Directory.Exists(HttpContext.Current.Server.MapPath(Setting["logFilePath"])))
        //        {
        //            if (!File.Exists(strLogFile))
        //            {
        //                swLog = new StreamWriter(strLogFile);
        //            }
        //            else
        //            {
        //                swLog = File.AppendText(strLogFile);
        //            }

        //            swLog.WriteLine(strLogMessage);
        //            swLog.WriteLine();
        //            swLog.Flush();
        //            swLog.Close();
        //        }
        //    }

        //} 

    }
}