﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<!--BeginTitle--><title>Aurigny Air Services Arrivals Information</title><!--EndTitle-->


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript" src="../Scripts/forCMS.js"></script>
<link href="../stylesheets/cms_stylesheet.css"  rel="stylesheet" type="text/css">
<link href="../include/pop_style.css"  rel="stylesheet" type="text/css">


 <!--BeginKeywords--><META NAME="KEYWORDS" CONTENT="Aurigny
flights, Blue Islands 
cheap flights
timetable
destinations
airport"><!--EndKeywords-->
 <!--BeginDescription--><META NAME="DESCRIPTION" CONTENT="Aurigny Air Services flight information for all UK, European and Channel Island services."><!--EndDescription-->


</head>
<body leftmargin="10" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
     <%
          if(Session("login")= true) then
              lang = ""  
              lang = Request.QueryString("lang")
            
    
              if (lang = "") then 
                lang = "EN"  
                'Response.Redirect("CMSLogin.aspx") 
              end if 

              
              if (request.QueryString("Finish") <> "Yes")Then
				    response.Redirect("../htmlEditor.aspx?lang=" & lang & "&pagename=arrivalsDefault&pageback=arrivals") 
		      end if 
		  end if 
     %>

	<div class="Wrapper">
	<div class="WrapperBody">
	<!--#include file="header.html" -->
	<div class="WrapperPanelMid">
	<!--#include file="arrivalsDefault.html" --></div>
	<!--#include file="fSelection.html" -->	
	</div>
	<div class="WrapperPanelRight">
	
	<div class="clearboth"></div>
	<!--#include file="popup/leftmenu.html" --></div>
	
	</div>
	<!--#include file="footer.asp" -->
	
</body>
</html>
































