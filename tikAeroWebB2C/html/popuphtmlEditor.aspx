<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="popuphtmlEditor.aspx.vb" Inherits="TikAero.Web.CMS.popuphtmlEditor" ValidateRequest="false"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Html editor's page</title> 
		<!--<LINK href="HtmlEditor/StyleSheets/htmlEditor.css" type="text/css" rel="stylesheet">-->
		<!--<<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script type="text/javascript">_editor_url = "HtmlEditor/";</script>
		 <script src="htmleditor/scripts/htmlarea.js" type="text/javascript"></script>
		<LINK href="stylesheets/cms_stylesheet.css" type="text/css" rel="stylesheet">
		<script>
			function OpenCenterPopUpScroll(url,windowName,intPopUpWidth,intPopUpHeight)
				{
				var winl = (screen.width - intPopUpWidth) / 2;
				var wint = (screen.height - intPopUpHeight) /2;
				window.open( 
						url,
						windowName,
						"'status=yes,resizable=yes,scrollbars=no,top="+ wint + ",left=" + winl + ",width=" + intPopUpWidth + ",height=" + intPopUpHeight + "'");
				}
		</script>
	    <style>
            .content { TEXT-ALIGN: left; PADDING-BOTTOM: 5px; PADDING-LEFT: 13px; PADDING-RIGHT: 10px; FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; COLOR: #030303; FONT-SIZE: 11px; FONT-WEIGHT: normal; PADDING-TOP: 5px }
	        .content2 { FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; COLOR: #595f63; FONT-SIZE: 8pt; FONT-WEIGHT: normal }
	        INPUT { FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; COLOR: #595f63; FONT-SIZE: 8pt; FONT-WEIGHT: normal }
	        INPUT.error { BORDER-BOTTOM: red 1px solid; BORDER-LEFT: red 1px solid; BACKGROUND-COLOR: #ffa07a; COLOR: #000000; BORDER-TOP: red 1px solid; BORDER-RIGHT: red 1px solid }
            .test {FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; COLOR: #595f63; FONT-SIZE: 8pt; FONT-WEIGHT: normal} 
        </style>
	</HEAD>
	<body leftMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<textarea id="htmlEditor" name="htmlEditor" rows="35" cols="150" runat="server"></textarea>
			<script language="javascript" defer>
				HTMLArea.init();
			</script>
			<div class="content" align="left"><STRONG>* Page name (for save as only)&nbsp;:&nbsp;</STRONG>
				<asp:textbox id="txtName" runat="server" MaxLength="20" Width="208px"></asp:textbox><asp:requiredfieldvalidator id="reg1" runat="server" ErrorMessage="* Please enter page name " Display="Dynamic"
					ControlToValidate="txtName"></asp:requiredfieldvalidator><STRONG><BR>
					* For edit popup page and banner page please select page&nbsp;: &nbsp;</STRONG>
				<asp:dropdownlist id="ddlPopup" runat="server" CssClass="test"></asp:dropdownlist>&nbsp;
				<asp:label id="lblPopup" runat="server" ForeColor="Red"></asp:label><STRONG><BR>
				</STRONG>
				<br>
				<asp:button id="btnUpdateContent" runat="server" Width="122px" CausesValidation="False" Text="Update Content"></asp:button>&nbsp;
				<asp:button id="btnLoadPopup" runat="server" Width="122px" CausesValidation="False" Text="Load Popup page"></asp:button>&nbsp;<asp:button id="btnCancel" runat="server" Width="122px" CausesValidation="False" Text="Back"></asp:button>&nbsp;<asp:button id="btnLogout" runat="server" Width="122px" CausesValidation="False" Text="Logout"></asp:button>
				<asp:button id="Button1" runat="server" Width="122px" CausesValidation="False" Text="Restore"
					Visible="False"></asp:button>
			&nbsp;
			<asp:Literal id="Literal1" runat="server"></asp:Literal>
		    </div>
		</form>
	</body>
</HTML>