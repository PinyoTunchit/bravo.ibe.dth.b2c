<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="htmlEditor_popup_page.aspx.vb" Inherits="TikAero.Web.CMS.htmlEditor_popup" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>htmlEditor_popup</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style>.Content { PADDING-RIGHT: 10px; PADDING-LEFT: 13px; FONT-WEIGHT: normal; FONT-SIZE: 11px; PADDING-BOTTOM: 5px; COLOR: #030303; PADDING-TOP: 5px; FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; TEXT-ALIGN: left }
	INPUT { FONT-WEIGHT: normal; FONT-SIZE: 8pt; COLOR: #595f63; FONT-FAMILY: Arial,Tahoma, Arial, sans-serif, verdana }
	INPUT.error { BORDER-RIGHT: red 1px solid; BORDER-TOP: red 1px solid; BORDER-LEFT: red 1px solid; COLOR: #000000; BORDER-BOTTOM: red 1px solid; BACKGROUND-COLOR: #ffa07a }
		</style>
		<script>
			function closeMywin(objForm){
				window.close();
				 // onBlur="self.focus()"
			}
			
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="420" border="0">
				<tr>
					<td class="content" style="WIDTH: 120px" vAlign="top" width="120"><STRONG>KEYWORDS:&nbsp;
						</STRONG>
					</td>
					<td>&nbsp;
						<asp:textbox id="txtKeyWord" runat="server" Height="40px" Width="312px" TextMode="MultiLine"></asp:textbox></td>
				</tr>
				<TR>
					<TD class="content" style="WIDTH: 120px" vAlign="top" width="120"><STRONG>DESCRIPTION:</STRONG></TD>
					<TD>&nbsp;
						<asp:textbox id="txtDescription" runat="server" Height="40px" Width="312px" TextMode="MultiLine"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="content" style="WIDTH: 120px" vAlign="top" width="120"><STRONG>TITLE:</STRONG></TD>
					<TD>&nbsp;
						<asp:textbox id="txtTitle" runat="server" Height="40px" Width="312px" TextMode="MultiLine"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="content" style="WIDTH: 120px" width="120">&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<tr>
					<td class="content" style="WIDTH: 120px" width="120">&nbsp;</td>
					<td><asp:button id="btnSave" runat="server" Width="96px" Text="Update"></asp:button><input id="btnClose" style="WIDTH: 96px" onclick="closeMywin();" type="button" value="Close"
							name="btnClose">
					</td>
				</tr>
			</table>
			<asp:Label id="Label1" runat="server" Visible="False">Label</asp:Label>
		</form>
	</body>
</HTML>
