<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="htmlEditor.aspx.vb" Inherits="TikAero.Web.CMS.htmlEditor" ValidateRequest="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
   <title>Html editor's page</title> 
   <script type="text/javascript">_editor_url = "HtmlEditor/";</script>
   <script src="htmleditor/scripts/htmlarea.js" type="text/javascript"></script>
   <script>
			function OpenCenterPopUpScroll(url,windowName,intPopUpWidth,intPopUpHeight)
				{
				var winl = (screen.width - intPopUpWidth) / 2;
				var wint = (screen.height - intPopUpHeight) /2;
				window.open( 
						url,
						windowName,
						"'status=yes,resizable=yes,scrollbars=no,top="+ wint + ",left=" + winl + ",width=" + intPopUpWidth + ",height=" + intPopUpHeight + "'");
				}
   </script>
   
   <style>
        .content { TEXT-ALIGN: left; PADDING-BOTTOM: 5px; PADDING-LEFT: 13px; PADDING-RIGHT: 10px; FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; COLOR: #030303; FONT-SIZE: 11px; FONT-WEIGHT: normal; PADDING-TOP: 5px }
	    .content2 { FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; COLOR: #595f63; FONT-SIZE: 8pt; FONT-WEIGHT: normal }
	    INPUT { FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; COLOR: #595f63; FONT-SIZE: 8pt; FONT-WEIGHT: normal }
	    INPUT.error { BORDER-BOTTOM: red 1px solid; BORDER-LEFT: red 1px solid; BACKGROUND-COLOR: #ffa07a; COLOR: #000000; BORDER-TOP: red 1px solid; BORDER-RIGHT: red 1px solid }
        .test {FONT-FAMILY: Tahoma, Arial, sans-serif, verdana; COLOR: #595f63; FONT-SIZE: 8pt; FONT-WEIGHT: normal} 
   </style>
</head>
<body leftMargin="0" topMargin="0">
    <form id="form1" runat="server">
    	<textarea id="htmlEditor" name="htmlEditor" rows="35" cols="150" runat="server"></textarea>
    	<script language="javascript" defer>
				HTMLArea.init();
		</script>
		<div class="content" align="left">
		        <STRONG><BR>* Page name (for save as only)&nbsp;:&nbsp;</STRONG>
				<asp:textbox id="txtName" runat="server" Width="208px" MaxLength="20"></asp:textbox>
				<asp:requiredfieldvalidator id="reg1" runat="server" ErrorMessage="* Please enter page name " Display="Dynamic"
					ControlToValidate="txtName"></asp:requiredfieldvalidator>
					<STRONG><BR>* For edit popup page and banner page please select page&nbsp;: &nbsp;</STRONG>
				<asp:dropdownlist id="ddlPopup" runat="server" CssClass="test"></asp:dropdownlist>&nbsp;
				<asp:label id="lblPopup" runat="server" ForeColor="Red"></asp:label><BR><BR>
				<asp:button id="btnUpdateContent" runat="server" Width="122px" CausesValidation="False" Text="Update Content"></asp:button>&nbsp;&nbsp;
				<asp:button id="btnSaveAs" runat="server" Width="122px" Text="Save as"></asp:button>&nbsp;
				<asp:button id="btnLoadPopup" runat="server" Width="122px" CausesValidation="False" Text="Load Popup page"></asp:button>&nbsp;
				<asp:button id="btnCancel" runat="server" Width="122px" CausesValidation="False" Text="Back"></asp:button>&nbsp;
				<asp:button id="btnLogout" runat="server" Width="122px" CausesValidation="False" Text="Logout"></asp:button>
				<asp:button id="Button1" runat="server" Width="122px" CausesValidation="False" Text="Restore" Visible="False"></asp:button>&nbsp;
				<asp:button id="btnOpenMeta" runat="server" Text="Meta Tag &amp; Title" CausesValidation="False" Width="122px"></asp:button>
                <br />
				<asp:label id="lblContent" runat="server"></asp:label>
		        <asp:label id="lblTemplateName" runat="server"></asp:label>
		        <asp:Literal id="Literal1" runat="server"></asp:Literal>
		  </div>
    </form>
</body>
</html>
