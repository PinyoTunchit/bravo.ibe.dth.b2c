<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="upload_image.aspx.vb" Inherits="TikAero.Web.CMS.upload_image" %>

<HTML>
	<HEAD>
		<title>Insert Image</title>
		<script src="popup.js" type="text/javascript"></script>
		<script type="text/javascript">
		window.resizeTo(450, 280);

		function Init() {
		// alert('selection.text.length' + selection.text.length);	 // <--- add it because somm want the error 
		__dlg_init();
		
		// alert('selection.text.length' + selection.text.length);	 // <--- add it because somm want the error 
		
		var param = window.dialogArguments;
		if (param) {
		    // alert('selection.text.length' + selection.text.length);	 // <--- add it because somm want the error 
			// document.getElementById("f_url").value = param["f_url"];
			document.getElementById("f_alt").value = param["f_alt"];
			document.getElementById("f_border").value = param["f_border"];
			document.getElementById("f_align").value = param["f_align"];
			document.getElementById("f_vert").value = param["f_vert"];
			document.getElementById("f_horiz").value = param["f_horiz"];
			
			if (document.getElementById("f_url").value != "") 
			{
			   document.getElementById("f_url").value = document.getElementById("f_url").value;
			   param["f_url"] = document.getElementById("f_url").value;
			}
			else 
			{
			   document.getElementById("f_url").value = param["f_url"];
			}
			
			// window.ipreview.location.replace(param.f_url);
			window.ipreview.location.replace(document.getElementById("f_url").value);
		}
		//alert('f_url' + f_url)
		document.getElementById("f_url").focus();
		};

		function onOK() {
		var required = {
			"f_url": "You must enter the URL" 
		};
		for (var i in required) {
			var el = document.getElementById(i);
			// alert('the el' + el);
			if (!el.value) {
			alert(required[i]);
			el.focus();
			return false;
			}
		}
		// pass data back to the calling window
		var fields = ["f_url", "f_alt", "f_align", "f_border",
						"f_horiz", "f_vert"];
		var param = new Object();
		for (var i in fields) {
			var id = fields[i];
			var el = document.getElementById(id);
			param[id] = el.value;
		}
		__dlg_close(param);
		return false;
		};

		function onCancel() {
		__dlg_close(null);
		return false;
		};

		function onPreview() {
		// alert('on Preview');
		var f_url = document.getElementById("f_url");
		var url = f_url.value;
		if (!url) {
			alert("You have to enter an URL first");
			f_url.focus();
			return false;
		}
		
		//alert('selection.text.lengthselection.text.length);	 // <--- add it because somm want the error 

		window.ipreview.location.replace(url); 
		return false;
		};
		</script>
		<style type="text/css">HTML { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT: 11px Tahoma,Verdana,sans-serif; BACKGROUND: buttonface; COLOR: buttontext; PADDING-TOP: 0px }
	BODY { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT: 11px Tahoma,Verdana,sans-serif; BACKGROUND: buttonface; COLOR: buttontext; PADDING-TOP: 0px }
	BODY { PADDING-BOTTOM: 5px; PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 5px }
	TABLE { FONT: 11px Tahoma,Verdana,sans-serif }
	FORM P { MARGIN-TOP: 5px; MARGIN-BOTTOM: 5px }
	.fl { TEXT-ALIGN: right; PADDING-BOTTOM: 2px; PADDING-LEFT: 5px; WIDTH: 9em; PADDING-RIGHT: 5px; FLOAT: left; PADDING-TOP: 2px }
	.fr { TEXT-ALIGN: right; PADDING-BOTTOM: 2px; PADDING-LEFT: 5px; WIDTH: 6em; PADDING-RIGHT: 5px; FLOAT: left; PADDING-TOP: 2px }
	FIELDSET { PADDING-BOTTOM: 5px; PADDING-LEFT: 5px; PADDING-RIGHT: 10px; PADDING-TOP: 0px }
	SELECT { FONT: 11px Tahoma,Verdana,sans-serif }
	INPUT { FONT: 11px Tahoma,Verdana,sans-serif }
	BUTTON { FONT: 11px Tahoma,Verdana,sans-serif }
	BUTTON { WIDTH: 70px }
	.space { PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px }
	.title { BORDER-BOTTOM: black 1px solid; PADDING-BOTTOM: 3px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px; MARGIN-BOTTOM: 10px; BACKGROUND: #ddf; LETTER-SPACING: 2px; COLOR: #000; FONT-SIZE: 120%; FONT-WEIGHT: bold; PADDING-TOP: 3px }
	FORM { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px }
	</style>
	</HEAD>
	<body onload="Init()">
		<div class="title">Insert Image</div>
		<!--- new stuff --->
		<form id="Form1" method="post" runat="server">
			<table style="PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px"
				width="100%" border="0">
				<tbody>
					<TR>
						<TD width="77">Select images</TD>
						<TD><INPUT class="txtBoxList" id="txtFile" type="file" size="32" name="txtImage" runat="server">&nbsp;<button id="btnUpload" name="btnUpload" runat="server">Upload</button>&nbsp;<button title="Preview the image in a new window" style="DISPLAY: none" onclick="return onPreview();"
								name="preview">Preview</button></TD>
					</TR>
					<tr>
						<td width="77">Alternate text:</td>
						<td><input id="f_alt" title="For browsers that don't support images" size="36" name="alt"></td>
					</tr>
					<tr>
						<td colSpan="2">
							<table width="100%" border="0">
								<tr>
									<td width="50%">
										<fieldset>
											<legend>Layout</legend>
											<table width="100%" border="0">
												<tr>
													<td align="right" width="90">Alignment:</td>
													<td><select id="f_align" title="Positioning of this image" size="1" name="align">
															<option value="">Not set</option>
															<option value="left">Left</option>
															<option value="right">Right</option>
															<option value="texttop">Texttop</option>
															<option value="absmiddle">Absmiddle</option>
															<option value="baseline" selected>Baseline</option>
															<option value="absbottom">Absbottom</option>
															<option value="bottom">Bottom</option>
															<option value="middle">Middle</option>
															<option value="top">Top</option>
														</select>
													</td>
												</tr>
												<tr>
													<td align="right" width="90">Border thickness:</td>
													<td><ew:numericbox id="f_border" runat="server" ToolTip="Leave empty for no border" PositiveNumber="True"
															MaxLength="2" Columns="5" name="border"></ew:numericbox></td>
												</tr>
											</table>
										</fieldset>
									</td>
									<td width="50%">
										<fieldset>
											<legend>Spacing</legend>
											<table width="100%" border="0">
												<tr>
													<td align="right" width="70">Horizontal:</td>
													<td><ew:numericbox id="f_horiz" runat="server" ToolTip="Horizontal padding" PositiveNumber="True" MaxLength="2"
															Columns="5" name="horiz"></ew:numericbox></td>
												</tr>
												<tr>
													<td align="right" width="70">Vertical:</td>
													<td><ew:numericbox id="f_vert" runat="server" ToolTip="Vertical padding" PositiveNumber="True" MaxLength="2"
															Columns="5" name="vert"></ew:numericbox></td>
												</tr>
											</table>
										</fieldset>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="2">
							<table style="MARGIN-BOTTOM: 0.2em" width="100%">
								<tr>
									<td vAlign="bottom" width="100">Image Preview:<br>
										<iframe id="ipreview" style="BORDER-BOTTOM: gray 1px solid; BORDER-LEFT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-RIGHT: gray 1px solid"
											name="ipreview" src="" frameBorder="0" width="300" height="200"></iframe></td>
									<td vAlign="bottom"><button onclick="return onOK();" name="ok" id="BUTTON1">OK</button><br>
										<button onclick="return onCancel();" name="cancel">Cancel</button>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody></table>
			<INPUT id="f_url" title="Enter the image URL here" name="url" runat="server"><asp:literal id="Literal1" runat="server"></asp:literal>
		</form>
	</body>
</HTML>