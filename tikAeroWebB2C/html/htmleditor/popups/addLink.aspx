<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="addLink.aspx.vb" Inherits="TikAero.Web.CMS.addLink" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Insert/Modify Link</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="popup.js" type="text/javascript"></script>
		<script type="text/javascript">
    // 18082008 window.resizeTo(400, 200);
    window.resizeTo(650, 450);

I18N = window.opener.HTMLArea.I18N.dialogs;

function i18n(str) {
  return (I18N[str] || str);
};

function onTargetChanged() {
  var f = document.getElementById("f_other_target");
  if (this.value == "_other") {
    f.style.visibility = "visible";
    f.select();
    f.focus();
  } else {
		f.style.visibility = "hidden";
		var ObjDdl = document.getElementById("f_target")
		if (ObjDdl.selectedIndex == 2) {
			var td1 = document.getElementById("td1")
			var td2 = document.getElementById("td2") 
			var td3 = document.getElementById("td3")
			var td4 = document.getElementById("td4")
			
			td1.style.display = 'block'
			td2.style.display = 'block'
			td3.style.display = 'block'
			td4.style.display = 'block'
			// ��͹/�ʴ� ��ǹ�ͧ property popup
		}
		else {
			var td1 = document.getElementById("td1")
			var td2 = document.getElementById("td2")
			var td3 = document.getElementById("td3")
			var td4 = document.getElementById("td4")
			
			td1.style.display = 'none'
			td2.style.display = 'none'
			td3.style.display = 'none'
			td4.style.display = 'none'
		}
	}
};

function Init() {
  __dlg_translate(I18N);
  __dlg_init();
  var param = window.dialogArguments;
  var target_select = document.getElementById("f_target");
  
  // alert("sommmm" + target_select.value)
  
  var use_target = true;
  if (param) {
  
	// alert('somminit' + param["f_usetarget"]) == > alert true
	
    if ( typeof param["f_usetarget"] != "undefined" ) {
      use_target = param["f_usetarget"];
    }
    
    
    //18082008
    //alert('typeof param["f_href"]' + typeof param["f_href"]) // <== string
    //alert('param["ddlUrl"]' + param["ddlUrl"]) // <== undifine
        
    
    if ( typeof param["f_href"] != "undefined" ) {
	  // alert("f_href = undefined")
	  
	  /*
	  alert("Init param['f_href']" + param['f_href'])
	  alert("Init param['f_title']" + param['f_title'])
	  alert("Init param['f_target']" + param['f_target'])
	  */
	  
	  
      document.getElementById("f_href").value = param["f_href"];
      
      // alert("My target" + document.getElementById("f_href").value)
      
      // 18082008
      
	  var mystr = param["f_href"]
      if (mystr.search("javascript:CreateWnd")>= 0) {
			param["f_target"] = "popup"
			//document.getElementById("f_href").value =
			var para = mystr.split(",") 
			document.getElementById("txtwidth").value = para[1]
			document.getElementById("txtheight").value = para[2] // ź ; �͡ �óշ� resizeable ��ͧ����价�� resizeable ����
			//alert('mystr' + mystr)
			
			if (para[3].replace(");","") == 'true'){
				document.getElementById("chkresize").checked = true
			}
			else{
				document.getElementById("chkresize").checked = false
			}
      }
      
      document.getElementById("f_title").value = param["f_title"];
      comboSelectValue(target_select, param["f_target"]);
      if (target_select.value != param.f_target) {
        var opt = document.createElement("option");
        opt.value = param.f_target;
        opt.innerHTML = opt.value;
        target_select.appendChild(opt);
        opt.selected = true;
      }
    }
  }
  if (! use_target) {
    document.getElementById("f_target_label").style.visibility = "hidden";
    document.getElementById("f_target").style.visibility = "hidden";
    document.getElementById("f_target_other").style.visibility = "hidden";
  }
  var opt = document.createElement("option");
  // somm opt.value = "_other";
  // somm opt.innerHTML = i18n("Other");
  // somm target_select.appendChild(opt);
  target_select.onchange = onTargetChanged;
  document.getElementById("f_href").focus();
  document.getElementById("f_href").select();
  
  var ObjDdl = document.getElementById("f_target")
		if (ObjDdl.selectedIndex == 2) {
			var td1 = document.getElementById("td1")
			var td2 = document.getElementById("td2")
			var td3 = document.getElementById("td3")
			var td4 = document.getElementById("td4")
			
			td1.style.display = 'block'
			td2.style.display = 'block'
			td3.style.display = 'block'
			td4.style.display = 'block'
			// ��͹/�ʴ� ��ǹ�ͧ property popup
		}
		else {
			var td1 = document.getElementById("td1")
			var td2 = document.getElementById("td2")
			var td3 = document.getElementById("td3")
			var td4 = document.getElementById("td4")
			
			td1.style.display = 'none'
			td2.style.display = 'none'
			td3.style.display = 'none'
			td4.style.display = 'none'
		}
		
};

function onOK() {
  var required = {
    // f_href shouldn't be required or otherwise removing the link by entering an empty
    // url isn't possible anymore.
    // "f_href": i18n("You must enter the URL where this link points to")
  };
  for (var i in required) {
    var el = document.getElementById(i);
    if (!el.value) {
      alert(required[i]);
      el.focus();
      return false;
    }
  }
   
  
  // pass data back to the calling window
  
  
  var fields = ["f_href","f_title","f_target","ddlUrl"]; 
  var param = new Object();
  
  
  var myarget = document.getElementById("f_target").value; 
   
  
  for (var i in fields) {
    var id = fields[i];
    var el = document.getElementById(id);
    // var addUrl = "http://arisara/TikAero.Web.CMS/HTML/"
    var addUrl = '<%=urlHtmlFilePath%>';
    // alert(addUrl)
    var a = "";

	/*
    if (fields[i] == "f_target"){
		if (a != "") 
		 {
			a = el.value;
		 }
		param[id] = el.value;
    }
    */
      
    if (fields[i] == "f_href"){
		if (el.value != "")
		{
			id = "f_href"
			param[id] = el.value;
			// alert("href_param[id]" + param[id])
			
			// if el.value	
			/*
			var Tstr = el.value
			if (Tstr.search("javascript:CreateWnd")>= 0) {
				// <a href="javascript:CreateWnd('http://www.google.co.th/',200,100);"> -->
				var allpara = str.split(",") 
				var selectText = allpara[0].indexOf("'",0) allpara[0].indexOf("'",0)
			} 
			*/
			
			var allTag = el.value
			if (allTag.search("javascript:CreateWnd")>= 0) {
				var stpara = allTag.split(",") 
				var selectText = stpara[0]
				selectText = selectText.replace("javascript:CreateWnd('","")
				selectText = selectText.substring(0,selectText.length-1)
				// alert("sommselectText " + selectText)
				el.value = selectText
				param[id] = el.value;
			}
							
		
			if (myarget == 'popup') {
				var popuphight = document.getElementById("txtheight").value; 
				var popupwidth = document.getElementById("txtwidth").value; 
				//param[id] = '#sommstart#' + el.value + '#sommend#javascript:CreateWnd(' + el.value + ',' + popuphight + ',' + popupwidth + ');'
				//param[id] = "#sommstart#" + el.value + "#sommend#javascript:CreateWnd('" + el.value + "'," + popuphight + "," + popupwidth + ");"
				param[id] = "#sommstart#" + el.value + "#sommend#javascript:CreateWnd('" + el.value + "'," + popuphight + "," + popupwidth + "," + document.getElementById("chkresize").checked + ");"
				myarget = ''
			}

		}
		else
		{
			param[id] = "";	
		}
    }
    else if (fields[i] == "ddlUrl"){
		if (el.value != "")
		{
			id = "f_href"
			param[id] = addUrl + el.value;	
			   
			myall = addUrl + el.value;
			// alert('f_href' + myall);
			
			// alert("ddl_param[id]" + param[id])
			if (myarget == 'popup') {
				var popuphight = document.getElementById("txtheight").value; 
				var popupwidth = document.getElementById("txtwidth").value; 
				// param[id] = '#sommstart#' + el.value + '#sommend#javascript:CreateWnd(' + el.value + ',' + popuphight + ',' + popupwidth + ');'
				// param[id] = "#sommstart#" + el.value + "#sommend#javascript:CreateWnd('" + addUrl + el.value + "'," + popuphight + "," + popupwidth + ");"
				param[id] = "#sommstart#" + myall + "#sommend#javascript:CreateWnd('" + myall + "'," + popuphight + "," + popupwidth + "," + document.getElementById("chkresize").checked + ");"
				myarget = ''	
			}
		}
		else
		{
			param[id] = "";	
		}
    }

    else{
		param[id] = el.value;
		// alert("Else_param[id]" + param[id])
    }
  }
  
  // param['f_target'] = a
  // alert(a)
  //if (param.f_target == "_other")
  //param.f_target = document.getElementById("f_other_target").value;
  
  
  
  
   
   // 18082008 param['f_target'] = document.getElementById("f_target").value;  
     param["f_target"] = myarget      
   
   __dlg_close(param);
  return false;
  
};

function onCancel() {
  __dlg_close(null);
  return false;
};

		</script>
		<style type="text/css">HTML { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; BACKGROUND: buttonface; PADDING-BOTTOM: 0px; MARGIN: 0px; FONT: 11px Tahoma,Verdana,sans-serif; COLOR: buttontext; PADDING-TOP: 0px }
	BODY { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; BACKGROUND: buttonface; PADDING-BOTTOM: 0px; MARGIN: 0px; FONT: 11px Tahoma,Verdana,sans-serif; COLOR: buttontext; PADDING-TOP: 0px }
	BODY { PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px }
	TABLE { FONT: 11px Tahoma,Verdana,sans-serif }
	SELECT { FONT: 11px Tahoma,Verdana,sans-serif }
	INPUT { FONT: 11px Tahoma,Verdana,sans-serif }
	BUTTON { FONT: 11px Tahoma,Verdana,sans-serif }
	BUTTON { WIDTH: 70px }
	TABLE .label { WIDTH: 8em; TEXT-ALIGN: right }
	.title { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; FONT-WEIGHT: bold; FONT-SIZE: 120%; BACKGROUND: #ddf; MARGIN-BOTTOM: 10px; PADDING-BOTTOM: 3px; COLOR: #000; PADDING-TOP: 3px; BORDER-BOTTOM: black 1px solid; LETTER-SPACING: 2px }
	#buttons { PADDING-RIGHT: 2px; BORDER-TOP: #999 1px solid; MARGIN-TOP: 1em; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px; TEXT-ALIGN: right }
		</style>
		<script language="javascript">
		function selectOldUrl(){
			var ObjDdl = document.getElementById("ddlUrl")
			var ObjTxt = document.getElementById("f_href")
			if (ObjDdl.selectedIndex == 0) {
				ObjTxt.disabled = false
				ObjTxt.value = ""
			} 
			else{
				ObjTxt.value = ""
				ObjTxt.disabled = true
			}
		}
		</script>
	</HEAD>
	<body onload="Init();">
		<div class="title">Insert/Modify Link</div>
		<form id="Form1" method="post" runat="server">
			<table style="WIDTH: 100%" border="0">
				<TR>
					<TD class="label">Existing URL:</TD>
					<TD><asp:dropdownlist id="ddlUrl" runat="server" onChange="selectOldUrl();" Width="250px"></asp:dropdownlist></TD>
				</TR>
				<tr>
					<td class="label">New URL:</td>
					<td><asp:textbox id="f_href" runat="server" name="f_href" Width="250px"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label">Title (tooltip):</td>
					<td><input id="f_title" style="WIDTH: 250px; HEIGHT: 19px" type="text" size="36"></td>
				</tr>
				<tr>
					<td class="label"><span id="f_target_label">Target:</span>
					</td>
					<td>
						<select id="f_target" name="f_target" style="WIDTH: 250px">
							<option value="_blank" selected>New window(_blank)</option>
							<option value="_self">Same frame(_self)</option>
							<option value="popup">Popup</option>
						</select>
						<input id="f_other_target" style="VISIBILITY: hidden" type="text" size="10" name="f_other_target">&nbsp;
					</td>
				</tr>
				<TR>
					<TD class="label" id="td1">Size (Width):
					</TD>
					<TD id="td2"><asp:textbox id="txtwidth" runat="server" name="txtwidth" Width="80px"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						&nbsp;Size (Height):
						<asp:textbox id="txtheight" runat="server" name="txtheight" Width="80px"></asp:textbox>&nbsp;</TD>
				</TR>
				<TR>
					<TD class="label" id="td3">Resizable:</TD>
					<TD id="td4"><INPUT id="chkresize" type="checkbox" name="chkresize" style="WIDTH: 16px; HEIGHT: 20px"></TD>
				</TR>
				<TR>
					<TD class="label"></TD>
					<TD></TD>
				</TR>
			</table>
			<div id="buttons"><button onclick="return onOK();" name="ok" type="submit">OK</button><button onclick="return onCancel();" name="cancel" type="button">Cancel</button>
			</div>
		</form>
	</body>
</HTML>
