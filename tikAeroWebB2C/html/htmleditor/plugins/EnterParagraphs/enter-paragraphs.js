// Modification to htmlArea to insert Paragraphs instead of
// linebreaks, under Gecko engines, circa January 2004
// By Adam Wright, for The University of Western Australia
//
// Distributed under the same terms as HTMLArea itself.
// This notice MUST stay intact for use (see license.txt).

function EnterParagraphs(editor, params) {
	this.editor = editor;
	// activate only if we're talking to Gecko
	if (HTMLArea.is_gecko)
		this.onKeyPress = this.__onKeyPress;
};

EnterParagraphs._pluginInfo = {
	name          : "EnterParagraphs",
	version       : "1.0",
	developer     : "Adam Wright",
	developer_url : "http://blog.hipikat.org/",
	sponsor       : "The University of Western Australia",
	sponsor_url   : "http://www.uwa.edu.au/",
	license       : "htmlArea"
};

// An array of elements who, in html4, by default, have an inline display and can have children
// we use RegExp here since it should be a bit faster, also cleaner to check
EnterParagraphs.prototype._html4_inlines_re = /^(a|abbr|acronym|b|bdo|big|cite|code|dfn|em|font|i|kbd|label|q|s|samp|select|small|span|strike|strong|sub|sup|textarea|tt|u|var)$/i;

// Finds the first parent element of a given node whose display is probably not inline
EnterParagraphs.prototype.parentBlock = function(node) {
	while (node.parentNode && (node.nodeType != 1 || this._html4_inlines_re.test(node.tagName)))
		node = node.parentNode;
	return node;
};

// Internal function for recursively itterating over a all nodes in a fragment
// If a callback function returns a non-null value, that is returned and the crawl is therefore broken
EnterParagraphs.prototype.walkNodeChildren = function(me, callback) {
	if (me.firstChild) {
		var myChild = me.firstChild;
		var retVal;
		while (myChild) {
			if ((retVal = callback(this, myChild)) != null)
				return retVal;
			if ((retVal = this.walkNodeChildren(myChild, callback)) != null)
				return retVal;
			myChild = myChild.nextSibling;
		}
	}
};

// Callback function to be performed on each node in the hierarchy
// Sets flag to true if we find actual text or an element that's not usually displayed inline
EnterParagraphs.prototype._isFilling = function(self, node) {
	if (node.nodeType == 1 && !self._html4_inlines_re.test(node.nodeName))
		return true;
	else if (node.nodeType == 3 && node.nodeValue != '')
		return true;
	return null;
	//alert(node.nodeName);
};

// Inserts a node deeply on the left of a hierarchy of nodes
EnterParagraphs.prototype.insertDeepLeftText = function(target, toInsert) {
	var falling = target;
	while (falling.firstChild && falling.firstChild.nodeType == 1)
		falling = falling.firstChild;
	//var refNode = falling.firstChild ? falling.firstChild : null;
	//falling.insertBefore(toInsert, refNode);
	falling.innerHTML = toInsert;
};

// Kind of like a macros, for a frequent query...
EnterParagraphs.prototype.isElem = function(node, type) {
	return node.nodeName.toLowerCase() == type.toLowerCase();
};

