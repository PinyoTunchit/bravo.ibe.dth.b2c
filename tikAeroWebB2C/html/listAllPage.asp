﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript" src="scripts/forCMS.js"></script>
<link href="stylesheets/cms_stylesheet.css"  rel="stylesheet" type="text/css">

</head>
<body>	
    
     <%
           lang = ""  
           lang = Request.QueryString("lang")
                 
           if (lang = "") then 
                Response.Redirect("../CMSLogin.aspx") 
           end if 
     %>
     
     <% if (lang = "EN") then %> <link href="includedefault/pop_style.css"  rel="stylesheet" type="text/css"><!--#include file="EN/headerlist.html" --> <% end if %>
     <% if (lang = "FR") then %> <link href="includedefaultFR/pop_style.css"  rel="stylesheet" type="text/css"><!--#include file="FR/headerlist.html" --> <% end if %>
     <% if (lang = "JA") then %> <link href="includedefaultJA/pop_style.css"  rel="stylesheet" type="text/css"><!--#include file="JA/headerlist.html" --> <% end if %> 
       
  
<table width="0" cellspacing="0" cellpadding="0" id="wrapperHomeInside" class="Wrapper">
  	<tr>
    <td width="610" valign="top">
        <table style="margin-left:30px"> 
            <tr>
                <td class="Heading1">List all popup pages</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        <%
				    strDirectoryPath = server.mappath(Request.ServerVariables("PATH_INFO"))
					strDirectoryPath = Replace(strDirectoryPath, "\listAllPage.asp", "\" & lang & "\Popup\")
     				strUrlPath="http://arisara/newCMS/html/" & lang & "\Popup\"
    				
    			    Set objFileScripting = CreateObject("Scripting.FileSystemObject")
    				Set objFolder = objFileScripting.GetFolder(strDirectoryPath)
    				Set filecollection = objFolder.Files
    				
    				For Each filename In filecollection
     					Filename= right(Filename,len(Filename)-InStrRev(Filename, "\"))
     					FilenameandLang = Filename & "?lang=" & lang
     					if not (InStr(Filename,"banner.html") > 0) and not (InStr(Filename,"popuphtmlEditor.aspx") > 0) then
     					    Response.Write "<tr>"
    					    Response.Write "<td>"
    					    Response.Write "<A HREF=""" & strUrlPath & FilenameandLang & """ target='_blank'>" & filename & "</A>"
    					    Response.Write "</td></tr>" 
     					end if 
    				Next
			%>
			
			</table>
	</td>
      </tr>
</table><br />
</div>
<!--#include file="EN/footer.html" -->

</body>
</html>