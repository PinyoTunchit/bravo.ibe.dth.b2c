var hBar = new ItemStyle(55, 0, '', 0, 0, '10#dc2e64', '10#dc2e64', 'highText', 'highText', '', '', null, null, 'hand', 'default');
//ItemStyle() var names=['len','spacing','popInd','popPos','pad','outCol','overCol','outClass','overClass','outBorder','overBorder','outAlpha','overAlpha','normCursor','nullCursor']
// change background color above.


// var subM = new ItemStyle(22, 0, '', -15, 3, '#CCCCDD', '#6699CC', 'lowText', 'highText', 'itemBorder', 'itemBorder', null, null, 'hand', 'default');
var subM = new ItemStyle(22, 0, '', -15, 3, '#dc2e64', '#dc2e64', 'lowText', 'highText', 'itemBorder', 'itemBorder', null, null, 'hand', 'default');


var subBlank = new ItemStyle(22, 1, '', -15, 3, '#CCCCDD', '#6699CC', 'lowText', 'highText', 'itemBorderBlank', 'itemBorder', null, null, 'hand', 'default');

// var button = new ItemStyle(22, 0, '', -15, 2, '10#079BAC', '10#FCCA15', 'buttonText', 'buttonHover', 'itemBorder', '', null, null, 'hand', 'default');
var button = new ItemStyle(22, 0, '', -15, 2, '#15598e', '#1e7bc8', 'buttonText', 'buttonHover', 'itemBorder', 'itemBorder', null, null, 'hand', 'default');


var pMenu = new PopupMenu('pMenu');
with (pMenu)
{
	//root
	// startMenu('root', false, 'page.elmPos("test1").x + 238', 71, 39, hBar, '', false);
	// startMenu('root', false, 438, 54, 20, hBar, '', false);
	
	startMenu('root', false, 600, 125, 19, hBar, '', false);
	// addItem('<img src="html/images/bt_01.gif" border="0" onclick="javascript:LoadHomePage();">', '', '', hBar, 58);
	
	addItem('<img src="images/bt_01.gif" border="0">', '../default.aspx?lang=EN&langculture=en-US', '', hBar, 58);
	addItem('<img src="images/bt_02.gif" border="0">', 'menu1', 'sm:', hBar, 74);
	addItem('<img src="images/bt_03.gif" border="0">', 'menu2', 'sm:', hBar, 127);
	addItem('<img src="images/bt_04.gif" border="0">', 'menu3', 'sm:', hBar, 90);
	addItem('<img src="images/bt_05.gif" border="0">', 'news.asp', '', hBar, 94);
	addItem('<img src="images/bt_06.gif" border="0">', 'contactus.asp?lang=EN', '', hBar, 80);
		
	
	// About us
	startMenu('menu1', true, 0, 21, 160, subM, '', false);
	addItem('Company History', 'EN/history.asp?lang=EN', '', button, 20);
	addItem('Company Vision &amp; Mission', '../EN/vision.asp?lang=EN', '', button, 20);
	addItem('Company Profile', 'EN/profile.asp?lang=EN', '', button, 20);
	addItem('Management', 'EN/management.asp?lang=EN', '', button, 20);
	addItem('Photo Gallery', 'EN/gallery.asp?lang=EN', '', button, 20);
	addItem('Press Release', 'EN/pressrelease.asp?lang=EN', '', button, 20);
	addItem('Safety &amp; Security', '../EN/safety.asp?lang=EN', '', button, 20);
	addItem('Aircraft', 'EN/aircraft.asp?lang=EN', '', button, 20);
	addItem('Job Opportunities', 'EN/job.asp?lang=EN', '', button, 20);
	
	// Product and Service 
	startMenu('menu2', true, 0, 21, 145, subM, '', false);
	addItem('Check Flight Information', 'EN/checkflight.asp?lang=EN', '', button, 20);
	addItem('Airport Information', 'EN/airport.asp?lang=EN', '', button, 20);
	addItem('How to book', 'EN/howtobook.asp?lang=EN', '', button, 20);
	addItem('How to pay', 'EN/howtopay.asp?lang=EN', '', button, 20);
	addItem('General Information', 'EN/generalinfo.asp?lang=EN', '', button, 20);
	addItem('Promotion', 'EN/promotion.asp?lang=EN', '', button, 20);
	addItem('Inter Island Pass', 'EN/interIsland.asp?lang=EN', '', button, 20);
	addItem('Charter Flights', 'EN/charterflights.asp?lang=EN', '', button, 20);
	addItem('Lodging', 'EN/lodging.asp?lang=EN', '', button, 20);
	

	// Destination
	startMenu('menu3', true, 0, 21, 122, subM, '', false);
	addItem('Route Map', 'EN/routemap.asp?lang=EN', '', button, 20);
	addItem('Flight schedule', 'EN/flightschedule.asp?lang=EN', '', button, 20);

	// News & Info
	startMenu('menu4', true, 0, 21, 122, subM, '', false);
	
	// Contact Us
	startMenu('menu5', true, 0, 21, 122, subM, '', false);
	
}

addMenuBorder(pMenu, window.subBlank, null, '#666666', 1, '#CCCCDD', 2);
addDropShadow(pMenu, window.subM, [0,"#333333",6,6,-4,-4], [0,"#666666",4,4,0,0]);
addDropShadow(pMenu, window.subBlank, [0,"#333333",6,6,-4,-4], [0,"#666666",4,4,0,0]);

if (navigator.userAgent.indexOf('rv:0.')==-1 && !(isOp&&!document.documentElement))
{
	pMenu.showMenu = new Function('mN','menuAnim(this, mN, 10)');
	pMenu.hideMenu = new Function('mN','menuAnim(this, mN, -10)');
}

page.elmPos=function(e,p)
{
	var x=0,y=0,w=p?p:this.win;
	e=e?(e.substr?(isNS4?w.document.anchors[e]:getRef(e,w)):e):p;
	if(isNS4){if(e&&(e!=p)){x=e.x;y=e.y};if(p){x+=p.pageX;y+=p.pageY}}
	else if (e && e.focus && e.href && this.MS && /Mac/.test(navigator.platform))
	{
		e.onfocus = new Function('with(event){self.tmpX=clientX-offsetX;' + 'self.tmpY=clientY-offsetY}');
		e.focus();x=tmpX;y=tmpY;e.blur()
	}
	else 
		while(e){x+=e.offsetLeft;y+=e.offsetTop;e=e.offsetParent}
	return{x:x,y:y};
};

function menuAnim(menuObj, menuName, dir)
{
	var mD = menuObj.menu[menuName][0];
	if (!mD.timer) mD.timer = 0;
	if (!mD.counter) mD.counter = 0;
	with (mD)
	{
		clearTimeout(timer);
		if (!lyr || !lyr.ref) return;
		if (!visNow && dir>0) dir = 0-dir;
		if (dir>0) lyr.vis('visible');
		lyr.sty.zIndex = dir>0 ? mD.zIndex + 1 : 1001;

		lyr.clip(0, 0, menuW+2, (menuH+2)*Math.pow(Math.sin(Math.PI*counter/200),0.75) );
 
		counter += dir;
		if (counter>100) { counter = 100; lyr.sty.zIndex = mD.zIndex }
		else if (counter<0) { counter = 0; lyr.vis('hidden') }
		else timer = setTimeout('menuAnim('+menuObj.myName+',"'+menuName+'",'+dir+')', 40);
	}
};

function addMenuBorder(mObj, iS, alpha, bordCol, bordW, backCol, backW)
{
	for (var mN in mObj.menu)
	{
		var mR=mObj.menu[mN], dS='<div style="position:absolute; background:';
		if (mR[0].itemSty != iS) continue;
		for (var mI=1; mI<mR.length; mI++)
		{
			mR[mI].iX += bordW+backW;
			mR[mI].iY += bordW+backW;
		}
		mW = mR[0].menuW += 2*(bordW+backW);
		mH = mR[0].menuH += 2*(bordW+backW);
		
		if (isNS4) mR[0].extraHTML += '<layer bgcolor="' + bordCol + '" left="0" top="0" width="' + mW +
			'" height="' + mH + '" z-index="980"><layer bgcolor="' + backCol + '" left="' + bordW + '" top="' +
			bordW + '" width="' + (mW-2*bordW) + '" height="' + (mH-2*bordW) + '" z-index="990"></layer></layer>';
		else mR[0].extraHTML += dS + bordCol + '; left:0px; top:0px; width:' + mW + 'px; height:' + mH +
			'px; z-index:980; ' + (alpha!=null?'filter:alpha(opacity=' + alpha + '); -moz-opacity:' + (alpha/100):'') +
			'">' + dS + backCol + '; left:' + bordW + 'px; top:' + bordW + 'px; width:' + (mW-2*bordW) + 'px; height:' +
			(mH-2*bordW) + 'px; z-index:990"></div></div>';
	}
};

function addDropShadow(mObj, iS)
{
	for (var mN in mObj.menu)
	{
		var a=arguments, mD=mObj.menu[mN][0], addW=addH=0;
		if (mD.itemSty != iS) continue;
		for (var shad=2; shad<a.length; shad++)
		{
			var s = a[shad];
			var alpha = (s[0]!=null && navigator.userAgent.indexOf('AppleWebKit') == -1);
			if (isNS4) mD.extraHTML += '<layer bgcolor="' + s[1] + '" left="' + s[2] + '" top="' + s[3] + '" width="' +
				(mD.menuW+s[4]) + '" height="' + (mD.menuH+s[5]) + '" z-index="' + (arguments.length-shad) + '"></layer>';
			else mD.extraHTML += '<div style="position:absolute; background:' + s[1] + '; left:' + s[2] +
				'px; top:' + s[3] + 'px; width:' + (mD.menuW+s[4]) + 'px; height:' + (mD.menuH+s[5]) + 'px; z-index:' +
				(a.length-shad) + '; ' + (alpha?'filter:alpha(opacity=' + s[0] + '); -moz-opacity:' + s[0] + '%; opacity:' + (s[0]/100):'') +
				'"></div>';
			addW=Math.max(addW, s[2]+s[4]);
			addH=Math.max(addH, s[3]+s[5]);
		}
		mD.menuW+=addW; mD.menuH+=addH;
	}
};