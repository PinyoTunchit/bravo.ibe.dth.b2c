var AutoFocus = new String
var CcValidation  = new String
var CcPreselect  = new String
var CcTotalValue 
var CcCurrency
var CssClass  = new Object
var SelectedDate = new Object

var Status = new Boolean(true)
var SingleFlight =  new Boolean(false)
var goToRow 
//var DateValid = 'M/d/y'
//var DateValidText = 'mm/dd/yyyy'
var DateValid = 'd/M/y'
var DateValidText = 'dd/mm/yyyy'

//var pl = 3
var pl = 0
var pt = new Boolean(true)
var ptv = new Boolean(false)

var eMsg = new Array();
eMsg[0]	= 'Tidak memungkinkan untuk reservasi bayi lebih banyak dari orang dewasanya';
eMsg[1]	= 'Mohon cek tanggal kepulangan anda ! seharusnya pada hari yang sama atau setelah tanggal keberangkatan';
eMsg[2]	= 'Silahkan nama penumpang yang sesuai pada penumpang.';
eMsg[3]	= 'Please supply a valid date of birth ';
eMsg[4]	= 'Please supply a valid Passenger Name at line ';
eMsg[5]	= 'Silakan Masukan Tanggal lahir ';
eMsg[6]	= 'Please supply a valid Phone Number.';
eMsg[7]	= 'Masukkan alamat email.';
eMsg[8]	= 'Please supply a valid Address.';
eMsg[9]	= 'Mohon diterima syarat dan kondisi';
eMsg[10]= 'Please supply a valid Passport Number at line ';
eMsg[11]= 'Please supply a valid Issue Place at line ';
eMsg[12]= 'Silakan input tanggal kadaluarsa yang benar pada penumpang ';
eMsg[13]= 'Please supply a valid date Issue Date at line ';

//SetAutoFocus()

function dateDiff(p_Interval, p_Date1, p_Date2, p_firstdayofweek, p_firstweekofyear){
	if(!isDate(p_Date1)){return "invalid date: '" + p_Date1 + "'";}
	if(!isDate(p_Date2)){return "invalid date: '" + p_Date2 + "'";}
	var dt1 = new Date(p_Date1);
	var dt2 = new Date(p_Date2);

	// get ms between dates (UTC) and make into "difference" date
	var iDiffMS = dt2.valueOf() - dt1.valueOf();
	var dtDiff = new Date(iDiffMS);

	// calc various diffs
	var nYears  = dt2.getUTCFullYear() - dt1.getUTCFullYear();
	var nMonths = dt2.getUTCMonth() - dt1.getUTCMonth() + (nYears!=0 ? nYears*12 : 0);
	var nQuarters = parseInt(nMonths/3);	//<<-- different than VBScript, which watches rollover not completion
	
	var nMilliseconds = iDiffMS;
	var nSeconds = parseInt(iDiffMS/1000);
	var nMinutes = parseInt(nSeconds/60);
	var nHours = parseInt(nMinutes/60);
	var nDays  = parseInt(nHours/24);
	var nWeeks = parseInt(nDays/7);

	// return requested difference
	var iDiff = 0;
	switch(p_Interval.toLowerCase()){
		case "yyyy": return nYears;
		case "q": return nQuarters;
		case "m": return nMonths;
		case "y": 		// day of year
		case "d": return nDays;
		case "w": return nDays;
		case "ww":return nWeeks;		// week of year	// <-- inaccurate, WW should count calendar weeks (# of sundays) between
		case "h": return nHours;
		case "n": return nMinutes;
		case "s": return nSeconds;
		case "ms":return nMilliseconds;	// millisecond	// <-- extension for JS, NOT available in VBScript
		default: return "invalid interval: '" + p_Interval + "'";
	}
}

function dgItem_over(object){
	object.className = (object.className == "GridItems0_o" ? "GridItems0" : "GridItems0_o")
}

function dgItem_click(s){
	if (event.srcElement.tagName != 'A'){
		o = document.getElementById(s)
		o.checked = 'true'
	}
}

function ValidFlightSelection(o){
	Status = true;
	var d = GetSelectFlight('FlightAvailability0',2)
	if (LibDaysDiff(d,new Date()) < 0 ){
		alert('Untuk penerbangan yang akan berangkat kurang dari 4 jam lagi, harap menghubungi bagian reservasi di 0804 1 080808')
		Status =  false;
		return
	}
	if (document.getElementById('FlightAvailability1') != null) {
		var d = GetSelectFlight('FlightAvailability0',1)
		var r = GetSelectFlight('FlightAvailability1',0)
		if (r != undefined){
			if (LibDaysDiff(r,d) < 0){
				alert('Mohon cek tanggal kepulangan anda ! seharusnya pada hari yang sama atau setelah tanggal keberangkatan')
				Status =  false;
				return
			}
		}
	}	
}

function ShowCvvCode(o){
	if (document.getElementById(o + '_cbmFormOfPaymentSubtype') != null){
		var iA = document.getElementById(o + '_cbmFormOfPaymentSubtype').options[document.getElementById(o + '_cbmFormOfPaymentSubtype').selectedIndex].value.split('|')
		if (iA.length > 0 ){
			document.getElementById('CvvForm').style.visibility = (iA[2] == '1') ?  'visible' : 'hidden' ;
			document.getElementById('CvvLabel').style.visibility = (iA[2] == '1') ? 'visible' : 'hidden' ;
			document.getElementById('IssueDateForm').style.visibility = (iA[3] == '1') ?  'visible' : 'hidden' ;
			document.getElementById('IssueDateLabel').style.visibility = (iA[3] == '1') ? 'visible' : 'hidden' ;
			document.getElementById('IssueNumberForm').style.visibility = (iA[4] == '1') ?  'visible' : 'hidden' ;
			document.getElementById('IssueNumberLabel').style.visibility = (iA[4] == '1') ? 'visible' : 'hidden' ;
			document.getElementById('IssueRow').style.visibility = (iA[3] == '1' || iA[4] == '1') ? 'visible' : 'hidden' ;
		}
	}
}

function isInteger(val){
	if( isNaN( parseFloat(val))) {
		return false;
	} else {
		return parseFloat(val);
	}
}




/* Somm cookie at aspx */

function clearCookie(name)
{                  
     expires = new Date();
     expires = expires.toGMTString()
     document.cookie = name + '=null' + '; expires=' + expires; 
}

function getCookie(name) {
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);
  if (begin == -1) {
    begin = dc.indexOf(prefix);
    if (begin != 0) return null;
  } else
    begin += 2;
  var end = document.cookie.indexOf(";", begin);
  if (end == -1)
    end = dc.length;
  return unescape(dc.substring(begin + prefix.length, end));
}

function getSubCookie(name,parent) {
  try {
	var dc = parent;
	var prefix = name + "=";
	var begin = dc.indexOf("&" + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
		if (begin != 0) return null;
	} else
		begin += 1;
	var end = parent.indexOf("&", begin);
	if (end == -1)
		end = dc.length;
	return unescape(dc.substring(begin + prefix.length, end));
  }
  catch(e) {
	//window.location = 'InformationPage.asp?e=c'
  }
}

/*
if (getSubCookie('AddFlight',getCookie('tikAero')) == "Outprocess") {
	clearCookie('AddFlight');
	window.location = 'Default.aspx';
}
*/

var currentURL = document.URL
if ((getSubCookie('AddFlight',getCookie('tikAero')) == "Outprocess")&&(currentURL.indexOf('default.aspx') != -1))
{
	clearCookie('tikAero');
	window.location = 'Default.html'
}

/* End Somm Cookie */

function fillDestination(e,o){
	var i = 0
	var eD = document.getElementById(e)
	var oD = document.getElementById(o)
	var sOv = eD[eD.selectedIndex].value
	var sDv = oD[oD.selectedIndex].value
	oD.options.length = 0;
	for (ii=0;ii < Airports.length;ii++){
		var at = Airports[ii].split('|')
		if (at[0] == sOv){
			// oD.options[i] = new Option(at[2],at[1]);
			oD.options[i] = new Option(at[2],at[1] + "|" + at[3]);
			i++
		}
	}
	eD.focus()
}

function CheckCardType(CcNumber){
	var l = new Number(CcNumber.slice(0,6))
	var iA = document.getElementById(CcPreselect + '_cbmFormOfPaymentSubtype').options[document.getElementById(CcPreselect + '_cbmFormOfPaymentSubtype').selectedIndex].value.split('|')
	SelectedCardType = iA[0]
	if 	( 	(l >= 413733 && l <= 413737 ) || 
			(l >= 446200 && l <= 446299 ) || 
			(l >= 453978 && l <= 453979) || 
			(l == 454313 ) ||
			(l >= 454432 && l <= 446299 ) || 
			(l == 454742) ||
			(l >= 456725 && l <= 456745 ) || 
			(l >= 465830 && l <= 465879 ) ||
			(l >= 465901 && l <= 465950 ) ||
			(l >= 475110 && l <= 475159 ) ||
			(l >= 475710 && l <= 475759 ) ||
			(l >= 476220 && l <= 476269 ) ||
			(l >= 476340 && l <= 476389 ) ||
			(l >= 484409 && l <= 484410 ) ||
			(l >= 490960 && l <= 490979 ) ||
			(l >= 492181 && l <= 492182 ) ||
			(l == 498824)
		){
		CardType = "VISADRUK"
	}
	else if((l  == 424519) || (l >= 424962 && l <= 424963 ) || (l == 450875) || (l >= 484406 && l <= 484408 ) || (l >= 484411 && l <= 484455 ) || (l >= 491730 && l <= 491759 ) || (l == 491880))	{
		CardType = "VISAELUK"
	}
	else if((l >= 448400 && l <= 448699 ) || (l >= 471500 && l <= 471599 )){
		CardType = "VISAPU"
	}
	else if((l == 490303) ||	(l == 493698) || (l == 633311) || (l >= 675900 && l <= 675999)){
		CardType = "MAESTROUK"
	}
	else if(l >= 510000 && l <= 559999){
		CardType = "MC"
	}
	else if(l >= 557347 && l <= 557446){
		CardType = "MCDR"
	}	
	else if(l >= 676700 && l <= 676799){
		CardType = "SOLO"
	}
	else if((l >= 6180000 && l <= 180099) ||	(l >= 6213100 && l <= 213199)){
		CardType = "JCB"
	}
	else if(l >= 352800 && l <= 358999){
		if (SelectedCardType == "DC"){
			CardType = "DC"}
		else if (SelectedCardType == "AMEX"){
			CardType = "AMEX"}
		else{
			CardType = "JCB"
		}
	}
	else if(l >= 300000 && l <= 369999){
		if (SelectedCardType == "AMEX"){
			CardType =  "AMEX"}
		else{
			CardType = "DC"
		}
	}
	else if(l >= 340000 && l <= 379999){
		if (SelectedCardType == "AMEX"){
			CardType =  "AMEX"}
		else{
			CardType = "JCB"
		}
	}
	else if(l >= 300000 && l <= 399999){
		CardType =  "JCB"
	}
	else if((l >= 500000 && l <= 509999) || (l >= 560000 && l <= 589999) || (l >= 600000 && l <= 699999)){
		CardType = "MAESTRO"
	}
	else if(l >= 400000 && l <= 499999)	{
		CardType = "VISA"
	}
	else{
		CardType = ""
	}
	
	return CardType
}

function AdditionalPaymentTabChanged(o,P){

    // Change Image Buynow button at 'Book now pay later'

    /*var objBuyNowBtn = document.getElementById("ctrBooking_ctrPassengerInfosBase_btmSubmitPayment");

    if(objBuyNowBtn != null){

        if (o == "NoPayment"){

            objBuyNowBtn.src = "Images/0/B/bmpm.gif";

        }

        else{

            objBuyNowBtn.src = "Images/0/B/bbo.gif";

        }

    }*/

}


// Start for new check date function

var dtCh= "/";

var minYear = 1900;

var maxYear = 2100;

 

function isInteger(s){

    var i;

    for (i = 0; i < s.length; i++){   

    // Check that current character is number.

    var c = s.charAt(i);

        if (((c < "0") || (c > "9"))) return false;

    }

    // All characters are numbers.

    return true;

}

 

function stripCharsInBag(s, bag){

    var i;

    var returnString = "";

    // Search through string's characters one by one.

    // If character is not in bag, append to returnString.

    for (i = 0; i < s.length; i++){   

        var c = s.charAt(i);

        if (bag.indexOf(c) == -1) returnString += c;

    }

    return returnString;

}

 

function daysInFebruary (year){

    // February has 29 days in any year evenly divisible by four,

    // EXCEPT for centurial years which are not also divisible by 400.

    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );

}

 

function DaysArray(n) {

    for (var i = 1; i <= n; i++) {

        this[i] = 31;

        if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}

        if (i==2) {this[i] = 29}

    } 

    return this;

}

 

function isDateExist(dtStr){

      var daysInMonth = DaysArray(12);

      var pos1 = dtStr.indexOf(dtCh);

      var pos2 = dtStr.indexOf(dtCh, pos1 + 1);

      //var strMonth = dtStr.substring(0, pos1);

      var strDay = dtStr.substring(0, pos1);

      //var strDay = dtStr.substring(pos1 + 1, pos2);

      var strMonth = dtStr.substring(pos1 + 1, pos2);

      var strYear = dtStr.substring(pos2 + 1);

      strYr = strYear;

      if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1);

      if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1);

      for (var i = 1; i <= 3; i++) {

            if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1);

      }

      month = parseInt(strMonth);

      day = parseInt(strDay);

      year = parseInt(strYr);

      if (pos1 == -1 || pos2 == -1){

            //alert("The date format should be : mm/dd/yyyy");

            return false;

      }

      if (strMonth.length < 1 || month < 1 || month > 12){

            //alert("Please enter a valid month");

            return false;

      }

      if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]){

            //alert("Please enter a valid day");

            return false;

      }

      if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear){

            //alert("Please enter a valid 4 digit year between " + minYear + " and " + maxYear);

            return false;

      }

      if (dtStr.indexOf(dtCh, pos2 +1 ) != -1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){

            //alert("Please enter a valid date");

            return false;

      }

    return true;

}

// End for new check date function




