var popup = null;
var popupState = false

function CreateWnd (file, width, height, resize)
{
	var doCenter = false;

	if((popup == null) || popup.closed)
	{
		attribs = "";

		if(resize) size = "yes"; else size = "no";

		for(var item in window)
			{ if(item == "screen") { doCenter = true; break; } }

		if(doCenter)
		{
			if(screen.width <= width || screen.height <= height) size = "yes";

			WndTop  = (screen.height - height) / 2;
			WndLeft = (screen.width  - width)  / 2;
			attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," + 
			"status=no,toolbar=no,directories=no,menubar=no,location=no,top=" + WndTop + ",left=" + WndLeft;
		}
		else
		{
			if(navigator.appName=="Netscape" && navigator.javaEnabled())
			{
				var toolkit = java.awt.Toolkit.getDefaultToolkit();
				var screen_size = toolkit.getScreenSize();
				if(screen_size.width <= width || screen_size.height <= height) size = "yes";

				WndTop  = (screen_size.height - height) / 2;
				WndLeft = (screen_size.width  - width)  / 2;
				attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," + 
				"status=no,toolbar=no,directories=no,menubar=no,location=no,top=" + WndTop + ",left=" + WndLeft;
			}
			else
			{
				size = "yes";
				attribs = "width=" + width + ",height=" + height + ",resizable=" + size + ",scrollbars=" + size + "," + 
				"status=no,toolbar=no,directories=no,menubar=no,location=no";
			}
		}

		// create the window
		popup = open(file, "", attribs);
	}
	else
	{
		DestroyWnd();
		CreateWnd(file, width, height, resize);
	}
}



function DestroyWnd ()
{
	if(popup != null)
	{
		popup.close();
		popup = null;
	}
}

// Popup.js
// Author: William Su

var V=parseInt(navigator.appVersion);
var B=(navigator.userAgent+navigator.appName).toLowerCase();
var NS4=(B.indexOf("netscape")!=-1&&V==4);
var IE=(B.indexOf("microsoft")!=-1);
var parentInputBox = null;
var parentImage = null;
var PopupDivId = 'Popup';
var PopupIFrameId = 'PopupIFrame';
var ObjId 

//document.write("<iframe src='clear.html' id='PopupIFrame' frameBorder='0' scrolling='no'></iframe>");
document.write("<div id='Popup'></div>");

if(!NS4) window.onresize=function()
{
	if(parentImage)
	{
		HidePopup();
	}		
}

function iecompattest()
{
	return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

if(!NS4) document.onmouseup=function(e)
{
	if(parentImage)
	{
		var inputBoxPos = new Position(parentImage);
		var linkPos = new Position(parentImage);
		var PopupPos = new Position(PopupDivId);		
		
		var x = PopupPos.GetElementLeft();
		var y = PopupPos.GetElementBottom();

		var x = inputBoxPos.GetElementLeft();
		var y = inputBoxPos.GetElementBottom();

		var top=document.all && !window.opera? iecompattest().scrollTop : window.pageYOffset;
		var windowedge=document.all && !window.opera? iecompattest().scrollTop+iecompattest().clientHeight : window.pageYOffset+window.innerHeight;
		var windowwidth = document.all && !window.opera? iecompattest().scrollTop+iecompattest().clientWidth : window.pageXOffset+window.innerWidth;

		
		if(y + PopupPos.GetElementHeight() > windowedge)
		{
			var z = inputBoxPos.GetElementTop() - PopupPos.GetElementHeight();
			if(z >= top)
				y = z;
		}	
					
		var width = PopupPos.GetElementWidth();
		var height = PopupPos.GetElementHeight();

		var lx = linkPos.GetElementLeft();
		var ly = linkPos.GetElementTop();
		var lwidth = linkPos.GetElementWidth();
		var lheight = linkPos.GetElementHeight();

		var pageX, pageY,eventElement
		if (window.event)
		{
			e = window.event;
			pageX = e.clientX;
			pageY = e.clientY;
			eventElement = (window.event.srcElement.outerHTML.indexOf('ChangeCalenderMonth') == -1) ? null : window.event.srcElement
		}	
		else
		{
			pageX = e.pageX;
			pageY = e.pageY;
		}
		if(!(eventElement) && !((pageX >= x && pageY >= y && pageX < x+width && pageY < y+height) || (pageX >= lx && pageY >= ly && pageX < lx+lwidth && pageY < ly+lheight)))
		HidePopup();			
	}		
}

function Position(object)
{
	var m_elem = object;

	this.GetElementWidth = GetElementWidth;
	function GetElementWidth()
	{
		var elem;
		if(typeof(m_elem) == "object")
		{
			elem = m_elem;
		}
		else
		{
			elem = document.getElementById(m_elem);
		}
		return parseInt(elem.offsetWidth);
	}

	this.GetElementHeight = GetElementHeight;
	function GetElementHeight()
	{
		var elem;
		if(typeof(m_elem) == "object")
		{
			elem = m_elem;
		}
		else
		{
			elem = document.getElementById(m_elem);
		}
		return parseInt(elem.offsetHeight);
	}

	this.GetElementLeft = GetElementLeft;
	function GetElementLeft()
	{
		var x = 0;
		var elem;		
		if(typeof(m_elem) == "object")
			elem = m_elem;
		else
			elem = document.getElementById(m_elem);
			
		while (elem != null)
		{
			x += elem.offsetLeft;
			elem = elem.offsetParent;
		}
		return parseInt(x);
	}
	
	this.GetElementRight = GetElementRight;
	function GetElementRight()
	{
		return GetElementLeft(m_elem) + GetElementWidth(m_elem);
	}

	this.GetElementTop = GetElementTop;
	function GetElementTop()
	{
		var y = 0;
		var elem;
		if(typeof(m_elem) == "object")
		{
			elem = m_elem;
		}
		else
		{
			elem = document.getElementById(m_elem);
		}
		while (elem != null)
		{
			y+= elem.offsetTop;
			elem = elem.offsetParent;
		}
		return parseInt(y);
	}

	this.GetElementBottom = GetElementBottom;
	function GetElementBottom()
	{
		return GetElementTop(m_elem) + GetElementHeight(m_elem);
	}
}

function Popup()
{
	var Desc = 0;

	function SetElementProperty(elemId, property, value)
	{
		var elem = null;

		if(typeof(elemId) == "object")
			elem = elemId;
		else
			elem = document.getElementById(elemId);
			
		if((elem != null) && (elem.style != null))
		{
			elem = elem.style;
			elem[property] = value;
		}
	}

	function PopupInnerHTML(obj)
	{
		//var thisObj = document.getElementById(obj)
		//document.getElementById("Popup").appendChild(obj)
		//return 
	}

	this.Show = Show;
	function Show(inputBox, img,obj)
	{
		// If the Popup is visible, hide it.		
		if (parentImage == img)
		{
			return;
		}			
		else
		{
			parentImage = img;
		}			
		

		if(document.getElementById)
		{
			Popup = document.getElementById(PopupDivId);
			//Popup.innerHTML = PopupInnerHTML(obj);
			Popup.appendChild(obj)
			for (i = 0;i < Popup.childNodes.length;i++){
				SetElementProperty(Popup.childNodes[i], 'display', 'none')
			}
			// Fixed some Windows and IE problems
			if(B.indexOf("win")!=-1 && IE)
			SetElementProperty(PopupIFrameId, 'display', 'block');
			SetElementProperty(PopupDivId, 'display', 'block');
			SetElementProperty(ObjId, 'display', 'block');
			var inputBoxPos = new Position(parentImage);
			var PopupPos = new Position(PopupDivId);

			var x = inputBoxPos.GetElementLeft();
			var y = inputBoxPos.GetElementBottom();
			
			var top=document.all && !window.opera? iecompattest().scrollTop : window.pageYOffset;
			var windowedge=document.all && !window.opera? iecompattest().scrollTop+iecompattest().clientHeight : window.pageYOffset+window.innerHeight;
			var windowwidth = document.all && !window.opera? iecompattest().scrollTop+iecompattest().clientWidth : window.pageXOffset+window.innerWidth;

			if(y + PopupPos.GetElementHeight() > windowedge)
			{
				var z = inputBoxPos.GetElementTop() - PopupPos.GetElementHeight();
				if(z >= top)
					y = z;
			}	
			
			//if (x + PopupPos.GetElementWidth() > windowwidth){
			//	x = 5
			//}
			x = x - 150
			//alert(x + '|' + PopupPos.GetElementWidth() )
			SetElementProperty(PopupDivId, 'left', x + "px");
			SetElementProperty(PopupDivId, 'top', y + "px");
			SetElementProperty(PopupIFrameId, 'left', x + "px");
			SetElementProperty(PopupIFrameId, 'top', y + "px");
			SetElementProperty(PopupIFrameId, 'width', PopupPos.GetElementWidth() + "px");
			SetElementProperty(PopupIFrameId, 'height', PopupPos.GetElementHeight() + "px");
		}

	}

	this.Hide = Hide;
	function Hide()
	{
		if(parentImage)
		{
			SetElementProperty(PopupDivId, 'display', 'none');
			SetElementProperty(PopupIFrameId, 'display', 'none');
			parentImage = null;
		}
	}
}

var PopupWin = new Popup();

function ShowPopup(inputBox, imgDropDown,objId)
{
	ObjId = objId
	var obj = document.getElementById(objId)
	
	if(parentImage == null)
		PopupWin.Show(parentImage, imgDropDown,obj);
	else
		PopupWin.Hide();
}

function RefreshPopup()
{

}

function HidePopup()
{
	PopupWin.Hide();
}


function Sort(change)
{
	PopupWin.Sort(change);
}

