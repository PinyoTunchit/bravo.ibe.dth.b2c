using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Threading;
using System.Globalization;
namespace TikAeroWebB2E.Calendar
{

    public enum uxControlType
    {
        DropdownList=0,
        Calendar=1,
        HtmlImage=2

    }
    public partial class ddlCalendar : System.Web.UI.Page
    {
        public string LimitYear = "";
        public string StartMonth = "";
        public string IsBeginCurrentMont = "1";
        public string RenderType = "0";

        private System.Web.UI.WebControls.Calendar Tempcal;
        int day = 0;
        int month = 0;
        int year = 0;
        int isdefaultdate = 0;
        int isShowLastDate = 0;
        bool isonlyonemonth = true;
        bool isnextcurrentmonth = false;
        string calendartype = "none";
        string FirstDayOfWeek = "Monday";
        DateTime tDate;
        string id = "";
        string mxMY = "";
        string mnMY = "";


        private string Currentculture = "";
        protected override void InitializeCulture()
        {

            //This Control use when change langeuge
            //Fix its on master page with javascript

            string culture = Convert.ToString("" + Page.Request["langculture"]);
            if (culture != "")
            {
                CultureInfo ci = new CultureInfo(culture);
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;
                Currentculture = culture;
               
            }
            base.InitializeCulture();
        }

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        private void InitializeComponent()
        {
            Tempcal = new System.Web.UI.WebControls.Calendar();
            this.Tempcal.DayRender += new System.Web.UI.WebControls.DayRenderEventHandler(this.Tempcal_DayRender);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            RenderType = Convert.ToString("" + this.Page.Request["rt"]);
            id = Convert.ToString("" + this.Page.Request["id"]);
            if (RenderType != "0")
            {
                //AgentDetail = new UserDeatil();
                //string culture = "en-GB";
                //if (culture != "")
                //{
                //    CultureInfo ci = new CultureInfo(culture);
                //    Thread.CurrentThread.CurrentCulture = ci;
                //    Thread.CurrentThread.CurrentUICulture = ci;

                //}


                day = Convert.ToInt16("0" + this.Page.Request["d"]);
                month = Convert.ToInt16("0" + this.Page.Request["m"]);
                year = Convert.ToInt16("0" + this.Page.Request["y"]);

                //if (day == 0) day = DateTime.Now.Date.Date.Day;
                if (month == 0) month = DateTime.Now.Date.Month;
                if (year == 0) year = DateTime.Now.Date.Year;
                if (day == 0)
                {
                    //day = DateTime.Now.Date.Date.Day;
                    DateTime dt = new DateTime(DateTime.Now.Date.Year, month, 1);
                    day = dt.Date.Day;
                }

                if (Convert.ToString("" + Convert.ToString(this.Page.Request["type"])) != "") calendartype = Convert.ToString(this.Page.Request["type"]);
                if (Convert.ToString("" + this.Page.Request["i"]) != "") isonlyonemonth = Convert.ToBoolean(Convert.ToInt16(this.Page.Request["i"]));
                //if (Convert.ToString("" + this.Page.Request["df"]) != "") isdefaultdate = Convert.ToInt16("0" + this.Page.Request["df"]);
                if (Convert.ToString("" + this.Page.Request["nm"]) != "") isnextcurrentmonth = Convert.ToBoolean(Convert.ToInt16(this.Page.Request["nm"]));
                if (Convert.ToString("" + Convert.ToString(this.Page.Request["fdow"])) != "") FirstDayOfWeek = Convert.ToString(this.Page.Request["fdow"]);

                if (Convert.ToString("" + Convert.ToString(this.Page.Request["mxmy"])) != "") mxMY = Convert.ToString(this.Page.Request["mxmy"]);
                if (Convert.ToString("" + Convert.ToString(this.Page.Request["mnmy"])) != "") mnMY = Convert.ToString(this.Page.Request["mnmy"]);

                if (Convert.ToString("" + Convert.ToString(this.Page.Request["SLD"])) != "") isShowLastDate = Convert.ToInt16(this.Page.Request["SLD"]);
                //isShowLastDate



                try
                {
                    tDate = new DateTime(year, month, day);
                }
                catch 
                {
                    tDate = new DateTime(year, month, 1);
                }
                
                if (isnextcurrentmonth)
                {
                    tDate = tDate.AddMonths(1).Date;
                    month = tDate.Month;
                    year = tDate.Year;
                }

                //**Calendar set**//			
                try
                {
                    System.Web.UI.WebControls.FirstDayOfWeek tmp = (System.Web.UI.WebControls.FirstDayOfWeek)Enum.Parse(typeof(System.Web.UI.WebControls.FirstDayOfWeek), FirstDayOfWeek, true);
                    Tempcal.FirstDayOfWeek = tmp;
                }
                catch
                {
                    Tempcal.FirstDayOfWeek = System.Web.UI.WebControls.FirstDayOfWeek.Monday;
                }

                //Tempcal.SelectedDayStyle.BackColor = System.Drawing.Color.White;
                Tempcal.TodaysDate = tDate; //if(isdefaultdate==1){}                
               Tempcal.SelectedDate = tDate;
                
                Tempcal.ID = "ctr_" + id;

                //* Set Calendar Style*//
                //Tempcal.SelectedDayStyle.BackColor=System.Drawing.Color.Gold;  

                Tempcal.SelectorStyle.CssClass = "calendar_SelectorStyle";
                Tempcal.TitleStyle.CssClass = "calendar_TitleStyle";
                //Tempcal.TitleStyle.BorderWidth = 0;
               // Tempcal.TitleStyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None;
                //Tempcal.TitleStyle.BorderColor = System.Drawing.Color.White;
                //Tempcal.TitleFormat= System.Web.UI.WebControls.TitleFormat.

                Tempcal.TitleStyle.BackColor = System.Drawing.Color.White;

                Tempcal.SelectedDayStyle.CssClass = "calendar_dateselect";
                Tempcal.CssClass = "calendar_body";
                Tempcal.DayStyle.CssClass = "calendar_DayStyle";
                Tempcal.DayHeaderStyle.CssClass = "calendar_DayHeader";
                //Tempcal.BorderWidth = 0;


                StringBuilder sb = new StringBuilder();
                StringWriter tw = new StringWriter(sb);
                HtmlTextWriter hw = new HtmlTextWriter(tw);

                string sdDate = year.ToString() + month.ToString().PadLeft(2, '0') + tDate.Date.Day.ToString().PadLeft(2, '0');

                int PN = Convert.ToInt32(tDate.AddMonths(1).Year.ToString() + tDate.AddMonths(1).Month.ToString().PadLeft(2, '0'));
                int PP = Convert.ToInt32(tDate.AddMonths(-1).Year.ToString() + tDate.AddMonths(-1).Month.ToString().PadLeft(2, '0'));
  
               // mxMY,mnMY

                if (mnMY == "") mnMY = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0');
                if (mxMY == "") mxMY = "999999";

                if ((PN <= Convert.ToInt32(mxMY)) )
                {
                    Tempcal.NextMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(1).Month)) + "','" + tDate.AddMonths(1).Year.ToString() + "','" + Tempcal.ID + "');>" + Tempcal.NextMonthText + "</a>";
                }
                else
                {
                    Tempcal.NextMonthText = "";
                }

                if (PP >= Convert.ToInt32(mnMY))
                {
                    Tempcal.PrevMonthText = "<a href=javascript:monthActHome('" + Convert.ToString((tDate.AddMonths(-1).Month)) + "','" + tDate.AddMonths(-1).Year.ToString() + "','" + Tempcal.ID + "');>" + Tempcal.PrevMonthText + "</a>";
                }
                else
                {
                    Tempcal.PrevMonthText = "";
                }

                Tempcal.RenderControl(hw);
                string strCal = sb.ToString() + "<input type='hidden' id='hdd_" + Tempcal.ID + "' name='hdd_" + Tempcal.ID + "' value='" + sdDate + "'  />";
                if (this.calendartype.ToString() == "datefrom")
                {
                    strCal += "<input type='hidden' id='hdd_defCell' name='hdd_defCell' value='ctr_" + this.calendartype.ToString() + "_" + sdDate + "'  />";
                }

                strCal.Replace("\r", "");
                strCal.Replace("\n", "");
                strCal.Replace("\t", "");
                Response.Clear();
                Response.Write(strCal);

            }
            else
            {

                //Generate DDL,Calendar
                if (this.Page.Request["my"] != null) LimitYear = Convert.ToString("" + this.Page.Request["my"]);
                if (this.Page.Request["sm"] != null) StartMonth = Convert.ToString("" + this.Page.Request["sm"]);
                if (this.Page.Request["bc"] != null) IsBeginCurrentMont = Convert.ToString("" + this.Page.Request["bc"]);


                switch (IsBeginCurrentMont)
                {
                    case "0":
                        break;
                    case "1":

                        break;
                }
                DropDownList ddl = new DropDownList();
                DropDownList ddlDate = new DropDownList();
                DropDownList ddlMonthYear = new DropDownList();

                HtmlImage img = new HtmlImage();
                img.Src = "App_Themes/Default/Images/icon_calendar.gif";
                img.Style.Add("cursor", "hand");
                img.Border = 0;
                img.ID = id;
                img.Attributes.Add("onclick", "javascript:showCalendar(this)");
                ddlDate.ID = "ddlDate_" + id;
                ddlMonthYear.ID = "ddlMY_" + id;
                ddlDate.Attributes.Add("onchange", "javascript:hiddenCalendar('"+id+"', this)");
                ddlMonthYear.Attributes.Add("onchange", "javascript:hiddenCalendar('" + id + "', this)");
                GenDate(ddlDate);
                GenLimitYear(ddlMonthYear, 2, (int)((DateTime.Today.Month) - 1), DateTime.Today.Year);
                string Tmp = GetTempPlate();
                ddlDate.CssClass = "Day";
                ddlMonthYear.CssClass = "MonthYear";

                Tmp = Tmp.Replace("<!--DDL_Day-->", RenderControls(ddlDate, uxControlType.DropdownList));
                Tmp = Tmp.Replace("<!--DDL_Month_Year-->", RenderControls(ddlMonthYear, uxControlType.DropdownList));
                Tmp = Tmp.Replace("<!--IMG_Ctrl-->", RenderControls(img, uxControlType.HtmlImage));
                //"<select class='Day'></select> <select class='MonthYear'></select><img src='Images/icon_calendar.gif' />";
                Response.Clear();
                Response.Write(Tmp);
                 


            }
            
        }
        public string RenderControls(object obj,uxControlType control) 
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter hm = new HtmlTextWriter(sw);
            
            switch (control) 
            {
                case uxControlType.DropdownList:
                    DropDownList ddl = (DropDownList)obj;
                    ddl.RenderControl(hm); 
                    break;
                case uxControlType.Calendar:

                    System.Web.UI.WebControls.Calendar cal = (System.Web.UI.WebControls.Calendar)obj;
                    cal.RenderControl(hm); 
                    break;
                case uxControlType.HtmlImage:
                    HtmlImage img = (HtmlImage)obj;
                    img.RenderControl(hm); 
                    break;
            }
            return sb.ToString(); 
        }
        public string GetTempPlate() 
        {
            string ret = "";//"<table id='tabcal1' cellspan='0' rowspan='0'>";
                //ret+="<tr>";
                //ret+="<td align='left'>";
                //ret += "<!--DDL_Day-->";
                //ret+="</td>";
                //ret += "<td align='left'>";
                //ret+="<!--DDL_Month_Year-->";
                //ret+="</td>";
                //ret += "<td align='left'>";
                //ret += "<!--IMG_Ctrl-->";
                //ret+="</td>";
                //ret+="</tr>";
                //ret += "</table>";

            
            ret ="";
            ret += "<!--DDL_Day-->&nbsp;<!--DDL_Month_Year-->&nbsp;<!--IMG_Ctrl-->";
            //ret +="</div>";
            //ret += "";
                return ret;

        }
        public void GenLimitYear(DropDownList MyddlMonthList, int yearlimit, int beginmonth, int bgyear) 
        {
           
            DateTime month = Convert.ToDateTime("1/1/2000");

            for (int y = 0; y < yearlimit; y++) 
            {
                bgyear=bgyear+y;
                for (int i = beginmonth; i < 12; i++)
                {
                    DateTime NextMont = month.AddMonths(i);
                    ListItem list = new ListItem();
                    list.Text = NextMont.ToString("MMMM") + " " + bgyear.ToString()  ;
                    list.Value = bgyear + Convert.ToInt32(i + 1).ToString().PadLeft(2, '0');
                    MyddlMonthList.Items.Add(list);
                }
                beginmonth = 0;
            }
        }
        public void GenDate(DropDownList ddl) 
        {
            
            for (int i = 1; i <= 31; i++)
            {
                try
                {
                    DateTime NewDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, i);
                    ddl.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0')));
                }
                catch{

                }

            }

            if (ddl.Items.FindByValue(DateTime.Today.Day.ToString().PadLeft(2, '0')) != null) ddl.Items.FindByValue(DateTime.Today.Day.ToString().PadLeft(2, '0')).Selected = true;
          
        }
        private void Tempcal_DayRender(object sender, System.Web.UI.WebControls.DayRenderEventArgs e)
        {

            if (month == 0) { this.month = DateTime.Now.Month; }
            if (year == 0) { this.year = DateTime.Now.Year; }
            e.Cell.Text = e.Day.DayNumberText;

            int UsYear = e.Day.Date.Year;
            if (UsYear >= 2500) UsYear = UsYear - 543;
            //string Fdate =UsYear.ToString()+this.month.ToString().PadLeft(2,'0') +e.Day.Date.Day.ToString().PadLeft(2,'0') ;
            string Fdate = UsYear.ToString() + e.Day.Date.Month.ToString().PadLeft(2, '0') + e.Day.Date.Day.ToString().PadLeft(2, '0');


            e.Cell.ID = "ctr_" + this.calendartype.ToString() + "_" + Fdate;
            if (this.isonlyonemonth)
            {
                //if(this.month != e.Day.Date.Month) e.Cell.Text ="";				
                if (this.month != e.Day.Date.Month)
                {
                    e.Cell.CssClass = "Calendar_notinmonth";
                    e.Cell.Text = "";
                }
                else
                {
                    //if (isdefaultdate == 1)
                    //{
                        if (DateTime.Now.ToString("yyyyMMdd") == e.Day.Date.ToString("yyyyMMdd"))
                        {
                            e.Cell.CssClass = "calendar_dateselect";
                        }
                       
                    //}
                    string LinkDay="<a href=javascript:showschedule2('" + id + "','" + Fdate + "','" + e.Cell.ClientID.ToString() + "')>" + e.Cell.Text + "</a>"; 
                    if(isShowLastDate == 0)
                    {
                        if (DateTime.Now.Date > e.Day.Date)                        
                        {
                            LinkDay = e.Cell.Text;
                            e.Cell.CssClass = "Calendar_notinmonth";
                        }
                    }
                   
                    e.Cell.Text = LinkDay; 
                }
                //	if( e.Cell.Text!="")
                //	{				
                //	e.Cell.Text="<a href=javascript:showschedule('"+this.calendartype.ToString() +"','"+Fdate+"','"+e.Cell.ClientID.ToString()+"')>"+e.Cell.Text+"</a>";
                //	}
            }
            else
            {

                if (isdefaultdate == 1)
                {
                    //if (tDate == e.Day.Date)
                    //if (DateTime.Now.ToString("yyyyMMdd") == e.Day.Date.ToString("yyyyMMdd"))
                    //{
                        e.Cell.CssClass = "calendar_dateselect";
                    //}
                   
                }
                e.Cell.Text = "<a href=javascript:showschedule2('" + id + "','" + Fdate + "','" + e.Cell.ClientID.ToString() + "')>" + e.Cell.Text + "</a>";
            }
        }
    }
}
