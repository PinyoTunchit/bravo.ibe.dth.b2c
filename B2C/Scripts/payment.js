﻿// JScript File
var ccErrorNo = 0;
var ccErrors = new Array();

function paymentCreditCard() {

    var objChk = document.getElementById("chkPayCreditCard");

    if (objChk.checked == true) {
        ShowLoadBar(true, false);
        var objNameOnCard = document.getElementById("txtNameOnCard");
        var objCardType = document.getElementById(FindControlName("select", "optCardType"));
        var objIssueNumber = document.getElementById("txtIssueNumber");
        var objIssueMonth = document.getElementById("optIssueMonth");
        var objIssueYear = document.getElementById("optIssueYear");
        var objCardNumber = document.getElementById("txtCardNumber");
        var objExpiredMonth = document.getElementById("optExpiredMonth");
        var objExpiredYear = document.getElementById("optExpiredYear");
        var objCvv = document.getElementById("txtCvv");
        var objAddress1 = document.getElementById("txtAddress1");
        var objAddress2 = document.getElementById("txtAddress2");
        var objCity = document.getElementById("txtCity");
        var objCounty = document.getElementById("txtCounty");
        var objPostCode = document.getElementById("txtPostCode");
        var objCountry = document.getElementById(FindControlName("select", "optCountry"));
        var objCondition = document.getElementById("chkPayCreditCard");
        var currencyCode = $('#hdCurrencyCode').val();

        var xmlStr = ValidateCreditCard(objNameOnCard,
                                    objCardType,
                                    objIssueNumber,
                                    objIssueMonth,
                                    objIssueYear,
                                    objCardNumber,
                                    objExpiredMonth,
                                    objExpiredYear,
                                    objCvv,
                                    objAddress1,
                                    objAddress2,
                                    objCity,
                                    objCounty,
                                    objPostCode,
                                    objCountry,
                                    objCondition);

        if (xmlStr.length > 0) {
            if (currencyCode != null && currencyCode.length > 0 && currencyCode == "EUR")
                tikAeroB2C.WebService.B2cService.Ogone3D2015PaymentRequest(xmlStr, SuccessOgone3DPayment, showErrorPayment, showTimeOutPayment);
            else
                tikAeroB2C.WebService.B2cService.PaymentCreditCard(xmlStr, SuccessPaymentCreditCard, showErrorPayment, showTimeOutPayment);
        }
    }
    else {
        ShowMessageBox(objLanguage.Alert_Message_49, 0, '');
        scroll(0, 0);
    }
}

function SuccessPaymentCreditCard(result) {
    var strError = "";

    if (result.length > 0) {
        if (result == "{002}" || result == "{005}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var obj;
            var arrResult = result.split("{}");
            if (arrResult.length == 1) {

                ShowLoadBar(false, false);

                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;
                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");
                scroll(0, 0);

            }
            else {
                if (arrResult[0] == "200") {
                    //Infant over limit
                    strError = objLanguage.Alert_Message_59;
                }
                else if (arrResult[0] == "3D") {
                    //Redirect to 3D secure    
                    $(document.body).append($.base64.decode(arrResult[1]));
                }
                else {
                    strError = objLanguage.Alert_Message_47;
                }
                ShowLoadBar(false, false);
                ShowMessageBox(strError, 0, '');
            }
            obj = null;
        }
    }
    else {
        ShowLoadBar(false, false);
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
}
function SuccessOgone3DPayment(result) {
    var strError = "";
    if (result.length > 0) {
        if (result == "{002}" || result == "{005}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else if (result.indexOf(',') > -1 && result.indexOf(',')[0] == "Error" ) {
            var arr = result.split(',');
            ShowMessageBox(arr[1], 0, '');
        }
        else {
            var o = JSON.parse(result);
            if (o.code == '200') {
                //location.search = '?bookingID='+ o.msg+'&result=SUCCESS'
                $('<form method="POST">')
                    .attr('action', location.href)
                    .append($('<input />').attr('type', 'hidden').attr('name', 'bookingID').attr('value', o.msg))
                    .append($('<input />').attr('type', 'hidden').attr('name', 'result').attr('value', 'SUCCESS'))
                    .appendTo($('body'))
                    .submit();
            }
            else if (o.code == '300') {
                var msg = $.base64.decode(o.msg);
                $(document.body).append(msg);
            }
            else {
                strError = objLanguage.Alert_Message_47;
                ShowLoadBar(false, false);
                ShowMessageBox(strError, 0, '');
            }
        }
    }
    else {
        ShowLoadBar(false, false);
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
}

function activateCreditCardControl() {
    var objCardType = document.getElementById(FindControlName("select", "optCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objCreditCardFare = document.getElementById("dvAdditionalFee");
    var objFareTotal = document.getElementById("dvFareTotal");
    var objExpiredYear = document.getElementById("optExpiredYear");
    var optExpiredMonth = document.getElementById("optExpiredMonth");

    var objCvv = document.getElementById('dvCvv');
    var objExpiryDate = document.getElementById('dvExpiryDate');
    var objDvAddress = document.getElementById('dvAddress');
    var objDvIssueDate = document.getElementById('dvIssuDate');
    var objIssueNumber = document.getElementById('dvIssueNumber');
    var objCardNumber = document.getElementById("txtCardNumber");

    ToolTipColor();

    //Clear Error message.
    EmptyErrorMessage();

    ShowCreditCardField(objCardType,
                        objPaymentValue,
                        objCreditCardFare,
                        objFareTotal,
                        objExpiredYear,
                        optExpiredMonth,
                        objCvv,
                        objExpiryDate,
                        objDvAddress,
                        objDvIssueDate,
                        objIssueNumber,
                        objCardNumber);

    ToolTipColor();


}
function LoadMyName() {
    var objMyname = document.getElementById("chkMyname");
    if (objMyname.checked == true) {
        tikAeroB2C.WebService.B2cService.GetMyName(SuccessLoadName, showError, showTimeOut);
    }
    else {
        //If check bok not checked clear card holder information.
        SuccessLoadName("");
    }
    objMyname = null;
}

function SuccessLoadName(result) {
    var objNameOnCard = document.getElementById("txtNameOnCard");
    var objAddress1 = document.getElementById("txtAddress1");
    var objAddress2 = document.getElementById("txtAddress2");
    var objCity = document.getElementById("txtCity");
    var objCounty = document.getElementById("txtCounty");
    var objPostCode = document.getElementById("txtPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optCountry"));

    SetMyName(result,
              objNameOnCard,
              objAddress1,
              objAddress2,
              objCity,
              objCounty,
              objPostCode,
              objCountry);
}

function SetMyName(arg,
                   objNameOnCard,
                   objAddress1,
                   objAddress2,
                   objCity,
                   objCounty,
                   objPostCode,
                   objCountry) {
    if (arg.length > 0) {
        objNameOnCard.value = arg.split("{}")[0] + " " + arg.split("{}")[1];
        objAddress1.value = arg.split("{}")[2];
        objAddress2.value = arg.split("{}")[3];
        objCounty.value = arg.split("{}")[5];
        objCity.value = arg.split("{}")[6];
        objPostCode.value = arg.split("{}")[9];

        for (icount = 0; icount < objCountry.length; icount++) {
            if (objCountry.options[icount].value == arg.split("{}")[8]) {
                objCountry.selectedIndex = icount;
                break;
            }
        }

        if (GetControlValue(objNameOnCard).length > 0) {
            $("#" + objNameOnCard.id).removeClass("watermarkOn");
        }
        else {
            objNameOnCard.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objAddress1).length > 0) {
            $("#" + objAddress1.id).removeClass("watermarkOn");
        }
        else {
            objAddress1.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objAddress2).length > 0) {
            $("#" + objAddress2.id).removeClass("watermarkOn");
        }
        else {
            objAddress2.value = objLanguage.default_value_3;
        }

        if (GetControlValue(objCity).length > 0) {
            $("#" + objCity.id).removeClass("watermarkOn");
        }
        else {
            objCity.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objCounty).length > 0) {
            $("#" + objCounty.id).removeClass("watermarkOn");
        }
        else {
            objCounty.value = objLanguage.default_value_2;
        }

        if (GetControlValue(objPostCode).length > 0) {
            $("#" + objPostCode.id).removeClass("watermarkOn");
        }
        else {
            objPostCode.value = objLanguage.default_value_2;
        }
    }
    else {

        objNameOnCard.value = objLanguage.default_value_2;
        objAddress1.value = objLanguage.default_value_2;
        objAddress2.value = objLanguage.default_value_3;
        objCity.value = objLanguage.default_value_2;
        objCounty.value = objLanguage.default_value_2;
        objPostCode.value = objLanguage.default_value_2;

        $("#" + objNameOnCard.id).addClass("watermarkOn");
        $("#" + objAddress1.id).addClass("watermarkOn");
        $("#" + objAddress2.id).addClass("watermarkOn");
        $("#" + objCity.id).addClass("watermarkOn");
        $("#" + objCounty.id).addClass("watermarkOn");
        $("#" + objPostCode.id).addClass("watermarkOn");
    }

}
function payment(arg, ctx) {
    //StartAsync(arg,ctx);
}

function checkCreditCard(cardnumber, cardname, bCheckLength) {
    // Establish card type
    var cardType = -1;

    // Ensure that the user has provided a credit card number
    if (cardnumber.length == 0) {
        ccErrorNo = 1;
        return false;
    }

    // Now remove any spaces from the credit card number
    cardnumber = cardnumber.replace(/\s/g, "");

    // Check that the number is numeric
    var cardNo = cardnumber;
    var cardexp = /^[0-9]{6,19}$/;
    if (!cardexp.exec(cardNo)) {
        ccErrorNo = 2;
        return false;
    }

    // The following are the card-specific checks we undertake.
    var PrefixValid = false;

    // We use these for holding the valid lengths and prefixes of a card type
    var prefix = new Array();
    var lengths = new Array();
    var strCardRange = new Array();
    var strCardNo;
    var strCardType = "";

    for (var i = 0; i < cards.length; i++) {
        // Load an array with the valid prefixes for this card
        prefix = cards[i].prefixes.split(",");
        // Now see if any of them match what we have in the card number

        for (j = 0; j < prefix.length; j++) {
            strCardRange = prefix[j].split("-");
            if (strCardRange.length == 1) {
                strCardNo = cardNo.substring(0, strCardRange[0].length);
                if (strCardNo == strCardRange[0]) {
                    strCardType = cards[i].name.toLowerCase();
                    cardType = i;
                    break;
                }
            }
            else {
                strCardNo = cardNo.substring(0, strCardRange[0].length);
                if (strCardNo >= strCardRange[0] && strCardNo <= strCardRange[1]) {
                    strCardType = strCardType = cards[i].name.toLowerCase();
                    cardType = i;
                    break;
                }
            }
        }
        if (strCardType.length > 0) {
            break;
        }
    }

    if (cardname.toLowerCase() == strCardType) {
        PrefixValid = true;
    }

    // If it isn't a valid prefix there's no point at looking at the length
    if (!PrefixValid) {
        ccErrorNo = 3;
        return false;
    }

    if (bCheckLength == true) {
        // If card type not found, report an error
        if (cardType == -1) {
            ccErrorNo = 0;
            return false;
        }

        // Now check the modulus 10 check digit - if required
        if (cards[cardType].checkdigit) {
            var checksum = 0;                                  // running checksum total
            var mychar = "";                                   // next char to process
            var j = 1;                                         // takes value of 1 or 2

            // Process each digit one by one starting at the right
            var calc;
            for (i = cardNo.length - 1; i >= 0; i--) {

                // Extract the next digit and multiply by 1 or 2 on alternative digits.
                calc = Number(cardNo.charAt(i)) * j;

                // If the result is in two digits add 1 to the checksum total
                if (calc > 9) {
                    checksum = checksum + 1;
                    calc = calc - 10;
                }

                // Add the units element to the checksum total
                checksum = checksum + calc;

                // Switch the value of j
                if (j == 1)
                { j = 2; }
                else
                { j = 1; }
            }
            // All done - if checksum is divisible by 10, it is a valid modulus 10.
            // If not, report an error.

            if (checksum % 10 != 0) {
                ccErrorNo = 3;
                return false;
            }
        }
        // See if the length is valid for this card
        var LengthValid = false;
        lengths = cards[cardType].length.split(",");
        for (j = 0; j < lengths.length; j++) {
            if (cardNo.length == lengths[j]) LengthValid = true;
        }

        // See if all is OK by seeing if the length was valid. We only check the 
        // length if all else was hunky dory.
        if (!LengthValid) {
            ccErrorNo = 4;
            return false;
        };
    }
    // The credit card is in the required format.
    return true;
}
function EmptyErrorMessage() {
    document.getElementById("spnError").innerHTML = "";
    document.getElementById("spnVoucherError").innerHTML = "";
}

function validateVoucher(voucherNumber, password) {
    var objVoucherError = document.getElementById("spnVoucherError");
    var errorMessage = "";

    if (voucherNumber.length == 0 || IsNumeric(voucherNumber) == false) {
        errorMessage = errorMessage + objLanguage.Alert_Message_34 + "<div class='clear-all'></div>"; //"- Please supply a valid Voucher Number.
    }
    if (password.length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_35 + "<div class='clear-all'></div>"; //"- Please supply a valid Password.
    }

    if (errorMessage.length == 0) {
        objVoucherError.innerHTML = "";
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ValidateVoucher(voucherNumber, password, successValidateVoucher, showError, showTimeOut);
    }
    else {
        ShowMessageBox(errorMessage, 0, '');
    }
}
function successValidateVoucher(result) {
    var objVoucher = document.getElementById("dvVoucherDetail");
    var objVoucherError = document.getElementById("spnVoucherError");
    var objDvPayVoucher = document.getElementById("dvPayVoucher");

    if (result.length == 0) {
        ShowMessageBox(objLanguage.Alert_Message_36, 0, ''); //"Voucher not found or wrong password!"
    }
    else if (result == "DUP") {
        ShowMessageBox(objLanguage.Alert_Message_75, 0, ''); //"This voucher already exist!";
    }
    else {
        objVoucher.innerHTML = result;
        objDvPayVoucher.style.display = "block";
        ShowProgressBar(false);
    }

    objDvPayVoucher = null;
    objVoucherError = null;
    objVoucher = null;
}

function paymentVoucher() {

    var objChk = document.getElementById("chkPayVoucher");
    if (objChk.checked == true) {
        var objVoucher = document.getElementsByName("nVoucher");
        var objVoucherError = document.getElementById("spnVoucherError");
        var strValue = "";
        var strVoucherId = "";
        var strVoucherNumber = "";
        var strFormOfPayment = "";
        var strPaymentSubType = "";

        if (objVoucher != null) {
            for (var iCount = 0; iCount < objVoucher.length; iCount++) {
                if (objVoucher[iCount].checked == true) {
                    strValue = objVoucher[iCount].value;
                    break;
                }
            }
            if (strValue.length > 0) {
                // var objCondition = document.getElementById("chkPayVoucher");
                // if (objCondition.checked == true) {
                ShowLoadBar(true, false);

                strVoucherId = strValue.split("|")[0];
                strVoucherNumber = strValue.split("|")[1];
                strFormOfPayment = strValue.split("|")[2];
                strPaymentSubType = strValue.split("|")[3];

                //Process payment voucher.
                tikAeroB2C.WebService.B2cService.PaymentVoucher(strVoucherId, strVoucherNumber, strFormOfPayment, strPaymentSubType, SuccessPaymentVoucher, showError, showTimeOut);
                /*}
                else {
                ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
                }
                objCondition = null;
                }
                else {
                objVoucherError.innerHTML = objLanguage.Alert_Message_76; //"- Please select voucher.";
                */
            }

        }
        objVoucherError = null;
        objVoucher = null;
    }
    else {
        //Please accept Terms and Conditions
        ShowMessageBox(objLanguage.Alert_Message_49, 0, '');
    }
}
function SuccessPaymentVoucher(result) {
    var obj;
    if (result == "{002}") {
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        if (result.split("{}").length == 1) {

            ShowLoadBar(false, false);


            obj = document.getElementById("dvContainer");
            obj.innerHTML = result;
            //Hide right side information.
            DisplayQuoteSummary("", "", "");
            ShowClientLogonMenu(false, "");

            scroll(0, 0);
        }
        else {
            ShowLoadBar(false, false);
            obj = document.getElementById("spnVoucherError");
            if (result.split("{}")[1] == "200") {
                //Infant over limit
                obj.innerHTML = objLanguage.Alert_Message_59;
            }
            else {
                obj.innerHTML = result.split("{}")[1];
            }
        }
        obj = null;
    }
}

function showhidelayer(id) {

    var objAdditionalTotal = document.getElementById("divAdditionalFeeTotal");
    var objDvAdditionalFee = document.getElementById("dvAdditionalFee");
    clearPaymentTabClickColor();
    EmptyErrorMessage();

    document.getElementById('dvPaymentBack').style.display = 'none';
    if (id == 'CreditCard') {
        if (document.getElementById('dvcTCreditCard') != null) {

            document.getElementById('dvcTCreditCard').className = "CreditCard";

            document.getElementById('STM').style.display = 'none';
            document.getElementById('CreditCard').style.display = 'block';
            document.getElementById('Voucher').style.display = 'none';
            document.getElementById('BookNow').style.display = 'none';
            document.getElementById('ELV').style.display = 'none';
            RemoveWellNetFeeDisplay();
        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'Voucher') {
        if (document.getElementById('dvcTVoucher') != null) {

            document.getElementById('dvcTVoucher').className = "Voucher";

            document.getElementById('STM').style.display = 'none';
            document.getElementById('CreditCard').style.display = 'none';
            document.getElementById('Voucher').style.display = 'block';
            document.getElementById('ELV').style.display = 'none';

            document.getElementById('BookNow').style.display = 'none';


            if (objAdditionalTotal != null) {
                objDvAdditionalFee.style.display = "none";
                objDvAdditionalFee.style.display = "none";
            }
            RemoveWellNetFeeDisplay();
        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'BookNow') {
        if (document.getElementById('dvcTLater') != null) {

            document.getElementById('dvcTLater').className = "BookNowPayLater";


            document.getElementById('CreditCard').style.display = 'none';
            document.getElementById('Voucher').style.display = 'none';
            document.getElementById('ELV').style.display = 'none';
            document.getElementById('STM').style.display = 'none';
            document.getElementById('BookNow').style.display = 'block';

            if (objAdditionalTotal != null) {
                objDvAdditionalFee.style.display = "none";
                objDvAdditionalFee.style.display = "none";
            }

            //Display Convient store Fee(Wellnet).
            //GetWellNetFeeDisplay();
        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'eft') {
        if (document.getElementById('dvcTEFT') != null) {

            document.getElementById('dvcTEFT').className = "EFTpay";


            document.getElementById('CreditCard').style.display = 'none';
            document.getElementById('Voucher').style.display = 'none';
            document.getElementById('BookNow').style.display = 'none';
            document.getElementById('ELV').style.display = 'block';
            document.getElementById('STM').style.display = 'none';

            if (objAdditionalTotal != null) {
                objDvAdditionalFee.style.display = "none";
                objDvAdditionalFee.style.display = "none";
            }

            //Display Convient store Fee(Wellnet).
            GetWellNetFeeDisplay();
        }
        else {
            HideAllPayment();
        }
    }
    else if (id == 'STM') {
        if (document.getElementById('dvcTSatim') != null) {

            document.getElementById('dvcTSatim').className = "SatimPayment";


            document.getElementById('CreditCard').style.display = 'none';
            document.getElementById('Voucher').style.display = 'none';
            document.getElementById('BookNow').style.display = 'none';
            document.getElementById('ELV').style.display = 'none';
            document.getElementById('STM').style.display = 'block';

            if (objAdditionalTotal != null) {
                objDvAdditionalFee.style.display = "none";
                objDvAdditionalFee.style.display = "none";
            }

            if (objAdditionalTotal != null) {
                objDvAdditionalFee.style.display = "none";
                objDvAdditionalFee.style.display = "none";
            }
            RemoveWellNetFeeDisplay();
        }
        else {
            HideAllPayment();
        }
    }


    //Show addition fee information if there is.
    ShowAdditionalFee();
}
function clearPaymentTabClickColor() {

    var objTCreditCard = document.getElementById('dvcTCreditCard');


    var objTVoucher = document.getElementById('dvcTVoucher');


    var objTLater = document.getElementById('dvcTLater');

    var dvcTEFT = document.getElementById('dvcTEFT');
    var objTSatim = document.getElementById('dvcTSatim');

    // Start Yai add External Payment

    var objTExternal = document.getElementById('dvcTExternal');

    // End Yai add External Payment

    if (objTCreditCard != null) {
        objTCreditCard.className = "CreditCardDisable";
    }

    if (objTVoucher != null) {
        objTVoucher.className = "VoucherDisable";
    }

    if (objTLater != null) {
        objTLater.className = "BookNowPayLaterDisable";
    }
    if (dvcTEFT != null) {
        dvcTEFT.className = "EFTDisable";
    }
    if (objTSatim != null) {
        objTSatim.className = "SatimPaymentDisable";
    }


    // Start Yai add External Payment
    if (objTExternal != null) {
        objTExternal.className = "ExternalDisable";
    }
    // End Yai add External Payment

    objTCreditCard = null;

    objTVoucher = null;

    objTLater = null;
    objTSatim = null;
    // Start Yai add External Payment
    objTExternal = null;
    // End Yai add External Payment

}

// Start Yai add External Payment
function paymentExternal() {
    var objRefCode = document.getElementById("txtReferenceCode");
    var objConfirmRefCode = document.getElementById("txtConfirmReferenceCode");
    var objSecurityCode = document.getElementById("txtSecurityCode");
    var bSuccess = true;
    var errorMessage = "<div class='clear-all'></div>";

    if (trim(objRefCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_77 + "<div class='clear-all'></div>"; //"- Please supply a valid User ID Klik BCA.
    }
    if (trim(objConfirmRefCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_78 + "<div class='clear-all'></div>"; //"- Please supply a valid Confirm User ID Klik BCA.";
    }
    if ((trim(objRefCode.value) != "") && (trim(objConfirmRefCode.value) != "") && (trim(objRefCode.value) != trim(objConfirmRefCode.value))) {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_79 + "<div class='clear-all'></div>"; //"- Confirm User ID Klik BCA mismatch.";
    }
    if (trim(objSecurityCode.value) == "") {
        bSuccess = false;
        errorMessage = errorMessage + objLanguage.Alert_Message_80 + "<div class='clear-all'></div>"; //"- Please supply a valid Security Code.";
    }
    if (bSuccess) {
        var objCondition = document.getElementById("chkPayExternal");

        if (objCondition.checked == true) {
            ShowLoadBar(true, false);
            var strRefCode = trim(objRefCode.value);
            var strSecurityCode = trim(objSecurityCode.value);
            // Check Security first, first param is security code, second parameter is case sensitive checking
            tikAeroB2C.WebService.B2cService.CheckCaptchaSecurity(strSecurityCode, true, SuccessCheckCaptchaSecurity, showErrorPayment, showTimeOutPayment);
            // If it does not need to check Captcha use below instead of above
            //tikAeroB2C.WebService.B2cService.ExternalPayment(false, strRefCode, SuccessPaymentExternal, showErrorPayment, showTimeOutPayment);
        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
        }
        objCondition = null;
    }
    else {
        document.getElementById("spnExternalError").innerHTML = errorMessage;
    }
}

function SuccessCheckCaptchaSecurity(result) {
    var bCaptcha = result;
    if (bCaptcha) {
        var objRefCode = document.getElementById("txtReferenceCode");
        strRefCode = trim(objRefCode.value);
        tikAeroB2C.WebService.B2cService.ExternalPayment(false, strRefCode, SuccessPaymentExternal, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowLoadBar(false, false);
        ShowMessageBox(objLanguage.Alert_Message_81, 0, ''); //"Invalid Security Code !"
    }
}

function SuccessPaymentExternal(result) {
    var obj;
    if (result.split("{}").length == 1) {
        obj = document.getElementById("dvContainer");
        obj.innerHTML = result;

        //Hide right side information.
        DisplayQuoteSummary("", "", "");
        ShowClientLogonMenu(false, "");
        scroll(0, 0);
    }
    else {
        obj = document.getElementById("spnExternalError");
        if (result.split("{}")[1] == "200") {
            //Infant over limit
            obj.innerHTML = objLanguage.Alert_Message_59;
        }
        else {
            obj.innerHTML = result.split("{}")[1];
        }


    }
    obj = null;
    ShowLoadBar(false, false);
}

function refreshCaptcha() {
    tikAeroB2C.WebService.B2cService.RefreshCaptcha(SuccessRefreshCaptcha, showErrorPayment, showTimeOutPayment);
}

function SuccessRefreshCaptcha(result) {
    var objImgSecurity = document.getElementById("imgSecurity");
    objImgSecurity.src = "captimage.aspx?r=" + result;
}
// End Yai add External Payment

function EmptyErrorMessage() {
    $("#spnError").html("");
    $("spnVoucherError").html("");
}
function SaveBookedNowPayLater() {

    var objChk = document.getElementById("chkPayPostPaid");
    if (objChk.checked == true) {
        ShowLoadBar(true, false);
        var bookDate = new Date().format("yyyyMMddHHmmss");
        tikAeroB2C.WebService.B2cService.bookNowPaylater(false, bookDate, SuccessSaveBookedNowPayLater, showErrorPayment, showTimeOutPayment);
    }
    else {
        //Please accept Terms and Conditions
        ShowMessageBox(objLanguage.Alert_Message_49, 0, '');
    }

}
function SuccessSaveBookedNowPayLater(result) {
    var obj;
    if (result.length > 0) {
        if (result == "{002}" || result == "{005}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else if (result == "{706}") {
            ShowMessageBox(objLanguage.Alert_Message_153, 0, "");
        }
        else if (result == "overdue payment") {
            SetPaymentContent();
            InitializePaymentWaterMark();
            ShowMessageBox(objLanguage.Alert_Message_135, 0, '');
        }
        else if (result == "paylimit null") {
            SetPaymentContent();
            InitializePaymentWaterMark();
            ShowMessageBox(objLanguage.Alert_Message_136, 0, '');
        }
        else {
            if (result.split("{}").length == 1) {
                obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                //start wellnet
                if (result.split("<!--urlPopup :").length > 1) {
                    ShowLoadBar(false, false);
                    var urlPopup = result.split("<!--urlPopup :")[1].replace("-->", "");

                    ShowMessageBox(objLanguage.Alert_Message_140, 0, "wellnet|" + urlPopup);
                }
                else {
                    ShowLoadBar(false, false);
                }
                //end wellnet

                //Hide right side information.
                DisplayQuoteSummary("", "", "");
                ShowClientLogonMenu(false, "");

                scroll(0, 0);
            }
            else {
                if (result.split("{}")[1] == "200") {
                    //Infant over limit
                    ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
                }
                else {
                    ShowMessageBox(result.split("{}")[1], 0, '');
                }
            }
            obj = null;

        }
    }
    else {
        ShowLoadBar(false, false);
    }

}
function ClearPaymentInput() {
    var objChkMyName = document.getElementById("chkMyname");
    var objNameOnCard = document.getElementById("txtNameOnCard");
    var objIssueNumber = document.getElementById("txtIssueNumber");
    var objIssueMonth = document.getElementById("optIssueMonth");
    var objIssueYear = document.getElementById("optIssueYear");
    var objCardNumber = document.getElementById("txtCardNumber");
    var objExpiredMonth = document.getElementById("optExpiredMonth");
    var objExpiredYear = document.getElementById("optExpiredYear");
    var objCvv = document.getElementById("txtCvv");
    var objAddress1 = document.getElementById("txtAddress1");
    var objAddress2 = document.getElementById("txtAddress2");
    var objCity = document.getElementById("txtCity");
    var objCounty = document.getElementById("txtCounty");
    var objPostCode = document.getElementById("txtPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optCountry"));


    objChkMyName.checked = false;
    objNameOnCard.value = "";
    objIssueNumber.value = "";
    objIssueMonth.selectedIndex = 0;
    objIssueYear.selectedIndex = 0;
    objCardNumber.value = "";
    objExpiredMonth.selectedIndex = 0;
    objExpiredYear.selectedIndex = 0;
    objCvv.value = "";
    objAddress1.value = "";
    objAddress2.value = "";
    objCity.value = "";
    objCounty.value = "";
    objPostCode.value = "";
    objCountry.selectedIndex = 0;

}

function validateCC(strCardNumber, bCalculate) {
    var objCardType = document.getElementById(FindControlName("select", "optCardType"));
    if (objCardType != null) {
        var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
        var objDvFareTotal = document.getElementById("dvTotalFareSummary");
        var objTotalAdditinalFee = document.getElementById("divAdditionalFeeTotal");
        var objCreditCardValue = document.getElementById("dvCreditCardValue");
        var objCreditCardDetail = document.getElementById("liCreditCardFare");
        var objhdFareTotal = document.getElementById("hdSubTotal");

        var dclFeePercentage = Math.round(parseFloat(objPaymentValue.split("{}")[13]));
        var minimumFeeFlag = parseInt(objPaymentValue.split("{}")[14]);
        var Factor = parseInt(objPaymentValue.split("{}")[15]);
        var dclFeeAmount = Math.round(parseFloat(objPaymentValue.split("{}")[16]));
        var dclFeeAmountIncl = Math.round(parseFloat(objPaymentValue.split("{}")[17]));


        //Find Credit Card Fee
        var dblCreditCardFee = CalculateCreditCardFee(dclFeeAmount,
                                                  dclFeeAmountIncl,
                                                  Math.round(parseFloat(0 + GetControlValue(objhdFareTotal))),
                                                  dclFeePercentage,
                                                  Factor,
                                                  minimumFeeFlag);


        var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;

        var objhdFareTotal = document.getElementById("hdSubTotal");
        var dclFeePercentage = Math.round(parseFloat(objPaymentValue.split("{}")[13]));
        var minimumFeeFlag = parseInt(objPaymentValue.split("{}")[14]);
        var Factor = parseInt(objPaymentValue.split("{}")[15]);
        var dclFeeAmount = Math.round(parseFloat(objPaymentValue.split("{}")[16]));
        var dclFeeAmountIncl = Math.round(parseFloat(objPaymentValue.split("{}")[17]));

        CalculateCCFee(strCardNumber,
                   bCalculate,
                   objCardType,
                   objPaymentValue,
                   objDvFareTotal,
                   objTotalAdditinalFee,
                   objCreditCardValue,
                   objCreditCardDetail,
                   dblCreditCardFee,
                   objhdFareTotal);

        ShowAdditionalFee();
    }
}

function ShowAdditionalFee() {
    var objAdditionalTotal = document.getElementById("divAdditionalFeeTotal");
    var objDvAdditionalFee = document.getElementById("dvAdditionalFee");

    if (objAdditionalTotal != null) {
        var dblAddtionTotal = parseFloat(0 + RemoveCommas(objAdditionalTotal.innerHTML));

        if (dblAddtionTotal > 0) {
            objDvAdditionalFee.style.display = "block";
        }
        else {
            objDvAdditionalFee.style.display = "none";
        }
    }
}

function HideAllPaymentTab(bValue) {
    if (bValue == true) {
        document.getElementById("dvCCTab").style.display = "none";
        document.getElementById("dvVoucherTab").style.display = "none";

        //Form of payment section
        document.getElementById("CreditCard").style.display = "none";
        document.getElementById("Voucher").style.display = "none";
    }
    else {
        document.getElementById("dvCCTab").style.display = "block";
        document.getElementById("dvVoucherTab").style.display = "block";

        //Form of payment section
        document.getElementById("CreditCard").style.display = "block";
        document.getElementById("Voucher").style.display = "block";
    }
}
function SaveBookedRedeem() {
    var objCondition = document.getElementById("chkPayRedeem");
    if (objCondition.checked == true) {
        ShowLoadBar(true, false);
        var bookDate = new Date().format("yyyyMMddHHmmss");
        tikAeroB2C.WebService.B2cService.bookNowPaylater(true, bookDate, SuccessSaveBookedRedeem, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //"Please accept Terms and Conditions !"
    }
    objCondition = null;
}
function SuccessSaveBookedRedeem(result) {
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    obj = null;
    //If FFP Logon Reload point.
    var strClientProfileId = getCookie("coFFP");
    if (strClientProfileId != null) {
        LoadClientInformation(strClientProfileId);
    }
    ShowLoadBar(false, false);
}
//Start EquiPay payment
function SuccessProcessEquipayPayment(result) {
    var obj = document.getElementsByTagName("body");
    obj[0].innerHTML = result;
    document.payFormCcard.submit();
}

function paymentEft() {
    var objNameOnCard = document.getElementById("txtElvAccountHolder");
    var objAccNumber = document.getElementById("txtElvAccountNumber");
    var objCountry = document.getElementById(FindControlName("select", "optEftCountry"));
    var objBankCode = document.getElementById("txtElvBankCode");
    var objBankName = document.getElementById("txtElvBankName");


    var bSuccess = 1;
    var errorMessage = "<div class='clear-all'></div>";

    if (objNameOnCard.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_82 + "<div class='clear-all'></div>"; //"- Please supply a valid Name.";
        bSuccess = 0;
    }
    if (objBankCode.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_83 + "<div class='clear-all'></div>"; //"- Please supply a valid Bank code.";
        bSuccess = 0;
    }
    if (objBankName.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_84 + "<div class='clear-all'></div>"; //"- Please supply a valid Bank name.";
        bSuccess = 0;
    }
    if (objAccNumber.value.toString().length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_85 + "<div class='clear-all'></div>"; //"- Please supply a valid Account Number.";
        bSuccess = 0;
    }

    if (bSuccess == 1) {
        var objCondition = document.getElementById("chkElvCondition");

        if (objCondition.checked == true) {
            ShowLoadBar(true, false);
            var xmlStr = "<payment>";

            xmlStr += "<form_of_payment_subtype_rcd>EFT</form_of_payment_subtype_rcd>";
            xmlStr += "<form_of_payment_rcd>EFT</form_of_payment_rcd>";
            xmlStr += "<cvv_required_flag>0</cvv_required_flag>";
            xmlStr += "<display_cvv_flag>0</display_cvv_flag>";
            xmlStr += "<display_expiry_date_flag>0</display_expiry_date_flag>";
            xmlStr += "<expiry_date_required_flag>0</expiry_date_required_flag>";
            xmlStr += "<address_required_flag>0</address_required_flag>";
            xmlStr += "<display_address_flag>1</display_address_flag>";
            xmlStr += "<display_issue_date_flag>0</display_issue_date_flag>";
            xmlStr += "<display_issue_number_flag>0</display_issue_number_flag>";

            xmlStr += "<NameOnCard>" + objNameOnCard.value + "</NameOnCard>";
            xmlStr += "<CreditCardNumber>" + objAccNumber.value + "</CreditCardNumber>";
            xmlStr += "<CCDisplayName></CCDisplayName>";

            xmlStr += "<IssueMonth></IssueMonth>";
            xmlStr += "<IssueYear></IssueYear>";
            xmlStr += "<IssueNumber></IssueNumber>";

            xmlStr += "<ExpiryMonth></ExpiryMonth>";
            xmlStr += "<ExpiryYear></ExpiryYear>";

            xmlStr += "<Addr1></Addr1>";
            xmlStr += "<Addr2></Addr2>";
            xmlStr += "<Street></Street>";
            xmlStr += "<State></State>";
            xmlStr += "<City></City>";
            xmlStr += "<County></County>";
            xmlStr += "<ZipCode></ZipCode>";
            xmlStr += "<Country>" + objCountry.options[objCountry.selectedIndex].value + "</Country>";
            xmlStr += "<CVV></CVV>";
            xmlStr += "<fee_amount_incl>0</fee_amount_incl>";
            xmlStr += "<fee_amount>0</fee_amount>";
            xmlStr += "<bank_name>" + objBankName.value + "</bank_name>";
            xmlStr += "<bank_code>" + objBankCode.value + "</bank_code>";
            xmlStr += "<bank_iban></bank_iban>";
            xmlStr += "</payment>";

            tikAeroB2C.WebService.B2cService.PaymentCreditCard(xmlStr, SuccessPaymentCreditCard, showErrorPayment, showTimeOutPayment);
            // tikAeroB2C.WebService.B2cService.EqipayPaymentRequest(xmlStr, SuccessProcessEquipayPayment, showErrorPayment, showTimeOutPayment);
            //tikAeroB2C.WebService.B2cService.JccPaymentRequest(xmlStr, SuccessProcessEquipayPayment, showErrorPayment, showTimeOutPayment);                 
        }
        else {
            ShowMessageBox(objLanguage.Alert_Message_49, 0, ''); //passengerdetail.ascx
        }

        objCondition = null;
    }
    else { document.getElementById("spnError").innerHTML = errorMessage; }
}

function SetPaymentContent() {

    //Show default first tab.
    showhidelayer('BookNow');

    // set book now time limit content
    var BooknowTimeLimit = document.getElementById("hdBooknowTimeLimit").value;
    if (BooknowTimeLimit != "")
        $('#booknowTimelimit').text(BooknowTimeLimit + " hrs. only");
    else
        $('#booknowTimelimit').text("72 hrs. only");

    var objCC = document.getElementById("dvcTCreditCard");
    if (objCC != null && objCC.className == "CreditCard") {
        activateCreditCardControl();
    }

    //Check For Redeem payment
    //  if redeem with fee show form of payment
    //  if only redeem no payment show only redeem tab
    if (document.getElementById("dvPointTotal") != null &&
       parseFloat(0 + document.getElementById("dvFareTotal").innerHTML) > 0 &&
       parseInt(0 + document.getElementById("dvPointTotal").innerHTML) > 0) {
        HideAllPaymentTab(true);
        document.getElementById("dvRedeemTab").style.display = "none";
        document.getElementById("dvCCTab").style.display = "block";

        activateCreditCardControl();
        showhidelayer('CreditCard');
    }
    else if (document.getElementById("dvPointTotal") != null &&
             parseFloat(0 + document.getElementById("dvFareTotal").innerHTML) == 0 &&
             parseInt(0 + document.getElementById("dvPointTotal").innerHTML) > 0) {
        HideAllPaymentTab(true);
        document.getElementById("dvRedeemTab").style.display = "block";
        document.getElementById("dvReedeem").style.display = "block";
    }
    else {
        if (document.getElementById("dvRedeemTab") != null) {
            document.getElementById("dvRedeemTab").style.display = "none";
        }
    }


    //Check EFT Tab();
    ShowPaymentFfpInfo();
    //Clear Passenger title to show only MR ans MRS
    var objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    if (objTitle != null) {
        for (var i = 0; i < objTitle.options.length; i++) {
            if (objTitle.options[i].value != "MR|M" & objTitle.options[i].value != "MRS|F") {
                $("#" + FindControlName("select", "stContactTitle") + " option[value='" + objTitle.options[i].value + "']").remove();
                i = i - 1;
            }
        }
    }

    //Initialize Total Additonal collapse
    $(function () {
        $("#ulAdditionalFee").jqcollapse({
            slide: true,
            speed: 400,
            easing: ''
        });
    });

    scroll(0, 0);

    //Default Check Use contact address.
    var chkUsedContact = document.getElementById("chkMyname");
    if (chkUsedContact != null) {
        chkUsedContact.checked = true;
        LoadMyName();
    }

    //Set Expiry date
    FilledCCYear("optExpiredYear");
}
function LoadFormCreditCard() {
    var objChk = document.getElementsByName("nVoucher");
    var strXml = "";

    ShowProgressBar(true);
    strXml += "<vouchers>";
    for (var i = 0; i < objChk.length; i++) {
        if (objChk[i].checked == true) {
            var arr = objChk[i].value.split("|");
            if (arr.length == 4) {
                strXml += "<voucher>";

                strXml += "<voucher_id>" + arr[0] + "</voucher_id>";
                strXml += "<voucher_number>" + arr[1] + "</voucher_number>";

                strXml += "</voucher>";
            }
        }
    }
    strXml += "</vouchers>";
    tikAeroB2C.WebService.B2cService.LoadFormCreditCard(strXml, SuccessLoadFormCreditCard, showError, showTimeOut);
}
function SuccessLoadFormCreditCard(result) {

    ShowProgressBar(false);
    if (result.length > 0) {
        var objHolder = document.getElementById("dvFormHolder");
        var objContainer = document.getElementById("dvProgressBar");

        //Insert passenger form content.
        objHolder.innerHTML = result;

        //Show Fading.
        objContainer.style.display = "block";
        objHolder.style.display = "block";

        ClearErrorMsg();
        objHolder = null;
        objContainer = null;

        //Detect keyboard key for Credit Card number.
        $("#txtMultiCardNumber").keyup(function (e) {
            var objCardNumber = document.getElementById("txtMultiCardNumber");
            var objCreditCardFee = document.getElementById("dvMultiCCFee");

            if (objCardNumber.value.length == 6) {
                if (e.keyCode == 8) {
                    ValidateMultiCC(objCardNumber.value, false);
                }
                else {
                    ValidateMultiCC(objCardNumber.value, true);

                }
            }
            else if (objCardNumber.value.length < 6 & e.keyCode == 8) {

                ValidateMultiCC(objCardNumber.value, false);
            }
        });

        //Make voucher information collapse
        $(function () {
            $("#ulMultipleVcSummary").jqcollapse({
                slide: true,
                speed: 300,
                easing: ''
            });
        });

        //Show Credit card filed that base on card type.
        ActivateMultiCCControl();

        //Initialize CC WaterMark.
        InitializeMultiCCWaterMark();

        //Filter Credit card type by currency.
        FilterCardTypeList(true);

        //Set Expiry date
        FilledCCYear("optMultiExpiredYear");
    }
}
function InitializeMultiCCWaterMark() {

    //Credit Card
    InitializeWaterMark("txtMultiCardNumber", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiNameOnCard", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiCvv", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiAddress1", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiAddress2", objLanguage.default_value_3, "input");
    InitializeWaterMark("txtMultiCity", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiCounty", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiPostCode", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtMultiIssueNumber", objLanguage.default_value_2, "input");
}
function EnoughVoucher() {
    var objChk = document.getElementsByName("nVoucher");
    var objHdTotalVC = document.getElementsByName("hdVcAmount");
    var objHdSubTotal = document.getElementById("hdSubTotal");

    var dblTotal = 0;
    if (objChk != null && objChk.length > 0) {
        for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].checked == true) {
                dblTotal = dblTotal + parseFloat(objHdTotalVC[i].value);
            }
        }
    }
    if (dblTotal < parseFloat(objHdSubTotal.value)) {
        return false;
    }
    else {
        return true;
    }
}
function ValidateCreditCard(objNameOnCard,
                            objCardType,
                            objIssueNumber,
                            objIssueMonth,
                            objIssueYear,
                            objCardNumber,
                            objExpiredMonth,
                            objExpiredYear,
                            objCvv,
                            objAddress1,
                            objAddress2,
                            objCity,
                            objCounty,
                            objPostCode,
                            objCountry,
                            objCondition) {

    ccErrors[0] = objLanguage.Alert_Message_72; //"Unknown card type";
    ccErrors[1] = objLanguage.Alert_Message_73; //"No card number provided";
    ccErrors[2] = objLanguage.Alert_Message_39; //"Please supply a valid Credit Card Number.";
    ccErrors[3] = objLanguage.Alert_Message_74; //"Credit Card type mismatch.";
    ccErrors[4] = objLanguage.Alert_Message_74; //"Credit Card type mismatch.";

    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;

    var expMonth = objExpiredMonth.options[objExpiredMonth.selectedIndex].value;
    var expYear = objExpiredYear.options[objExpiredYear.selectedIndex].value;

    var bSuccess = 1;
    var errorMessage = "<div class='clear-all'></div>";
    var bCheckSum = false;
    var exchange_fee_amount = "";

    if (objPaymentValue.split("{}")[12] == "1")
    { bCheckSum = true; }

    if (expMonth.length == 1)
    { expMonth = '0' + expMonth; }

    if (GetSelectedOption(objCardType).length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_72 + "<div class='clear-all'></div>"; //"- Please supply a valid Card type.";
        bSuccess = 0;
    }

    if (GetControlValue(objNameOnCard).length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_38 + "<div class='clear-all'></div>"; //"- Please supply a valid Name On Card.";
        bSuccess = 0;
    }
    if (checkCreditCard(GetControlValue(objCardNumber), objPaymentValue.split("{}")[0], bCheckSum) == false) {
        errorMessage = errorMessage + "- " + ccErrors[ccErrorNo] + "<div class='clear-all'></div>";
        bSuccess = 0;
    }

    if ((expYear.length == 0 | expMonth.length == 0) & objPaymentValue.split("{}")[5] == 1) {
        errorMessage = errorMessage + objLanguage.Alert_Message_46 + "<div class='clear-all'></div>"; //"- Invalid Expired Date.";
        bSuccess = 0;
    }
    else {
        if (parseInt(expYear + expMonth) <= parseInt(today.getFullYear().toString() + leadingZero(today.getMonth()).toString())) {
            if (objPaymentValue.split("{}")[5] = 1) {
                errorMessage = errorMessage + objLanguage.Alert_Message_46 + "<div class='clear-all'></div>"; //"- Invalid Expired Date.";
                bSuccess = 0;
            }
        }
    }


    if (objPaymentValue.split("{}")[2] == 1 && GetControlValue(objCvv).length == 0) {
        errorMessage = errorMessage + objLanguage.Alert_Message_45 + "<div class='clear-all'></div>"; //"- Invalid CCV code.";
        bSuccess = 0;
    }
    if (objPaymentValue.split("{}")[6] == 1) {
        if (GetControlValue(objAddress1).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_41 + "<div class='clear-all'></div>"; //"- Please supply a valid Address 1.";
            bSuccess = 0;
        }
        if (GetControlValue(objCity).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_42 + "<div class='clear-all'></div>"; //"- Please supply a valid Town/City.";
            bSuccess = 0;
        }
        if (GetControlValue(objCounty).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_121 + "<div class='clear-all'></div>"; //"- Please supply a valid province.";
            bSuccess = 0;
        }
        if (GetControlValue(objPostCode).length == 0) {
            errorMessage = errorMessage + objLanguage.Alert_Message_43 + "<div class='clear-all'></div>"; //"- Please supply a valid Postal Code.";
            bSuccess = 0;
        }
    }

    if (document.getElementById("spRate") != null) {
        exchange_fee_amount = document.getElementById("spRate").innerHTML;
    }

    if (bSuccess == 1) {
        ShowLoadBar(true, false);

        var todayDate = new Date();
        var xmlStr = "<payment>";

        xmlStr += "<form_of_payment_subtype_rcd>" + objPaymentValue.split("{}")[0] + "</form_of_payment_subtype_rcd>";
        xmlStr += "<form_of_payment_rcd>" + objPaymentValue.split("{}")[1] + "</form_of_payment_rcd>";
        xmlStr += "<cvv_required_flag>" + objPaymentValue.split("{}")[2] + "</cvv_required_flag>";
        xmlStr += "<display_cvv_flag>" + objPaymentValue.split("{}")[3] + "</display_cvv_flag>";
        xmlStr += "<display_expiry_date_flag>" + objPaymentValue.split("{}")[4] + "</display_expiry_date_flag>";
        xmlStr += "<expiry_date_required_flag>" + objPaymentValue.split("{}")[5] + "</expiry_date_required_flag>";
        xmlStr += "<address_required_flag>" + objPaymentValue.split("{}")[6] + "</address_required_flag>";
        xmlStr += "<display_address_flag>" + objPaymentValue.split("{}")[7] + "</display_address_flag>";
        xmlStr += "<display_issue_date_flag>" + objPaymentValue.split("{}")[8] + "</display_issue_date_flag>";
        xmlStr += "<display_issue_number_flag>" + objPaymentValue.split("{}")[9] + "</display_issue_number_flag>";

        xmlStr += "<NameOnCard>" + ConvertToValidXmlData(GetControlValue(objNameOnCard)) + "</NameOnCard>";
        xmlStr += "<CreditCardNumber>" + GetControlValue(objCardNumber) + "</CreditCardNumber>";
        xmlStr += "<CCDisplayName>" + objCardType.options[objCardType.selectedIndex].Text + "</CCDisplayName>";

        xmlStr += "<IssueMonth>" + objIssueMonth.options[objIssueMonth.selectedIndex].value + "</IssueMonth>";
        xmlStr += "<IssueYear>" + objIssueYear.options[objIssueYear.selectedIndex].value + "</IssueYear>";
        xmlStr += "<IssueNumber>" + GetControlValue(objIssueNumber) + "</IssueNumber>";

        xmlStr += "<ExpiryMonth>" + objExpiredMonth.options[objExpiredMonth.selectedIndex].value + "</ExpiryMonth>";
        xmlStr += "<ExpiryYear>" + objExpiredYear.options[objExpiredYear.selectedIndex].value + "</ExpiryYear>";

        xmlStr += "<Addr1>" + ConvertToValidXmlData(GetControlValue(objAddress1)) + "</Addr1>";
        xmlStr += "<Addr2></Addr2>";
        xmlStr += "<Street>" + ConvertToValidXmlData(GetControlValue(objAddress2)) + "</Street>";
        xmlStr += "<State>" + ConvertToValidXmlData(GetControlValue(objCounty)) + "</State>";
        xmlStr += "<City>" + ConvertToValidXmlData(GetControlValue(objCity)) + "</City>";
        xmlStr += "<County>" + ConvertToValidXmlData(GetControlValue(objCounty)) + "</County>";
        xmlStr += "<ZipCode>" + ConvertToValidXmlData(GetControlValue(objPostCode)) + "</ZipCode>";
        xmlStr += "<Country>" + objCountry.options[objCountry.selectedIndex].value + "</Country>";
        xmlStr += "<CVV>" + ConvertToValidXmlData(GetControlValue(objCvv)) + "</CVV>";
        xmlStr += "<fee_amount_incl>" + objPaymentValue.split("{}")[10] + "</fee_amount_incl>";
        xmlStr += "<fee_amount>" + objPaymentValue.split("{}")[11] + "</fee_amount>";

        xmlStr += "<client_date_time>" + todayDate.getFullYear() + "-" + (todayDate.getMonth() + 1) + "-" + todayDate.getDate() + " " + todayDate.getHours() + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds() + "</client_date_time>";

        xmlStr += "</payment>";

        return xmlStr;
    }
    else {
        ShowMessageBox(errorMessage, 0, '');
        return "";
    }
    return "";
}

function CalculateCCFee(strCardNumber,
                        bCalculate,
                        objCardType,
                        objPaymentValue,
                        objDvFareTotal,
                        objTotalAdditinalFee,
                        objCreditCardValue,
                        objCreditCardDetail,
                        dblCreditCardFee,
                        objhdFareTotal) {

    //Display form of payment fee.
    var objMultiPercentageDisplay = document.getElementById("dvMultipleCCPercentage");
    var objCCCurrency = document.getElementById("dvFareCurrencyDisplay");

    if (objMultiPercentageDisplay == null) {
        var objPercentageDisplay = document.getElementById("dvCCPercentage");

        if (objPercentageDisplay != null) {
            objPercentageDisplay.innerHTML = GetControlInnerHtml(objCCCurrency) + " " + AddCommas(dblCreditCardFee.toFixed(2));
        }
    }

    if (objMultiPercentageDisplay != null) {
        objMultiPercentageDisplay.innerHTML = GetControlInnerHtml(objCCCurrency) + " " + AddCommas(dblCreditCardFee.toFixed(2));
    }

    if (bCalculate == true) {
        if (checkCreditCard(strCardNumber, objPaymentValue.split("{}")[0], false) == true) {
            if (objTotalAdditinalFee != null) {
                objTotalAdditinalFee.innerHTML = AddCommas((parseFloat(0 + dblCreditCardFee)).toFixed(2));
            }

            objCreditCardValue.innerHTML = AddCommas(Math.round(parseFloat(0 + dblCreditCardFee)));
            objDvFareTotal.innerHTML = AddCommas((parseFloat(0 + GetControlValue(objhdFareTotal)) + parseFloat(0 + dblCreditCardFee)).toFixed(2));

            if (objCreditCardDetail != null) {
                if (dblCreditCardFee > 0) {
                    objCreditCardDetail.style.display = "block";
                }
                else {
                    objCreditCardDetail.style.display = "block";
                }
            }
        }
        else {

            if (objCreditCardDetail != null) {
                objCreditCardValue.innerHTML = 0;
                objCreditCardDetail.style.display = "block";
            }

            if (parseFloat(0 + GetControlValue(objhdFareTotal)) != parseFloat(0 + RemoveCommas(GetControlInnerHtml(objDvFareTotal)))) {
                if (objTotalAdditinalFee != null) {
                    objTotalAdditinalFee.innerHTML = AddCommas((parseFloat(0 + RemoveCommas(objTotalAdditinalFee.innerHTML)) - parseFloat(0 + dblCreditCardFee)).toFixed(2));
                }
                objDvFareTotal.innerHTML = AddCommas(parseFloat(0 + GetControlValue(objhdFareTotal)).toFixed(2));
            }
        }
    }
    else {

        if (objCreditCardDetail != null) {
            objCreditCardValue.innerHTML = 0;
            objCreditCardDetail.style.display = "block";
        }

        if (parseFloat(0 + GetControlValue(objhdFareTotal)) != parseFloat(0 + RemoveCommas(GetControlInnerHtml(objDvFareTotal)))) {
            if (objTotalAdditinalFee != null) {
                objTotalAdditinalFee.innerHTML = AddCommas((parseFloat(0 + RemoveCommas(objTotalAdditinalFee.innerHTML)) - parseFloat(0 + dblCreditCardFee)).toFixed(2));
            }
            objDvFareTotal.innerHTML = AddCommas(parseFloat(0 + GetControlValue(objhdFareTotal)).toFixed(2));
        }
    }
}

function ShowCreditCardField(objCardType,
                            objPaymentValue,
                            objCreditCardFare,
                            objFareTotal,
                            objExpiredYear,
                            optExpiredMonth,
                            objCvv,
                            objExpiryDate,
                            objDvAddress,
                            objDvIssueDate,
                            objIssueNumber,
                            objCardNumber) {

    var ccSplit = objPaymentValue.split("{}");
    if (ccSplit[3] == "1")
    { objCvv.style.display = 'block'; }
    else
    { objCvv.style.display = 'none'; }

    if (objPaymentValue.split("{}")[4] == "1")
    { objExpiryDate.style.display = 'block'; }
    else
    { objExpiryDate.style.display = 'none'; }

    if (objPaymentValue.split("{}")[7] == "1")
    { objDvAddress.style.display = 'block'; }
    else
    { objDvAddress.style.display = 'none'; }

    if (objPaymentValue.split("{}")[8] == "1")
    { objDvIssueDate.style.display = 'block'; }
    else
    { objDvIssueDate.style.display = 'none'; }

    if (objPaymentValue.split("{}")[9] == "1")
    { objIssueNumber.style.display = 'block'; }
    else
    { objIssueNumber.style.display = 'none'; }

    //Clear Card number value
    objCardNumber.value = objLanguage.default_value_2;

    //Set Expiry Year
    if (objExpiredYear.options.length == 0) {
        var y = today.getFullYear();
        var objOption;
        for (var i = 0; i < 20; i++) {
            objOption = new Option(y + i, y + i);
            objExpiredYear.options.add(objOption);
        }
    }
}
function ActivateMultiCCControl() {
    var objCardType = document.getElementById(FindControlName("select", "optMultiCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objCreditCardFare = document.getElementById("dvMultiAdditionalFee");
    var objFareTotal = document.getElementById("dvMultiFareTotal");
    var objExpiredYear = document.getElementById("optMultiExpiredYear");
    var optExpiredMonth = document.getElementById("optMultiExpiredMonth");

    var objCvv = document.getElementById('dvMultiCvv');
    var objExpiryDate = document.getElementById('dvMultiExpiryDate');
    var objDvAddress = document.getElementById('dvMultiAddress');
    var objDvIssueDate = document.getElementById('dvMultiIssuDate');
    var objIssueNumber = document.getElementById('dvMultiIssueNumber');
    var objCardNumber = document.getElementById("txtMultiCardNumber");

    //Clear Error message.    

    ShowCreditCardField(objCardType,
                        objPaymentValue,
                        objCreditCardFare,
                        objFareTotal,
                        objExpiredYear,
                        optExpiredMonth,
                        objCvv,
                        objExpiryDate,
                        objDvAddress,
                        objDvIssueDate,
                        objIssueNumber,
                        objCardNumber);
    ValidateMultiCC("", false);
}
function ValidateMultiCC(strCardNumber, bCalculate) {
    var objCardType = document.getElementById(FindControlName("select", "optMultiCardType"));
    var objPaymentValue = objCardType.options[objCardType.selectedIndex].value;
    var objDvFareTotal = document.getElementById("dvTotalAdditional");
    var objCreditCardValue = document.getElementById("dvMultiCreditCardValue");
    var dblCreditCardFee = Math.round(parseFloat(objPaymentValue.split("{}")[10]));
    var objhdFareTotal = document.getElementById("hdTotalAdditional");

    var dclFeePercentage = Math.round(parseFloat(objPaymentValue.split("{}")[13]));
    var minimumFeeFlag = parseInt(objPaymentValue.split("{}")[14]);
    var Factor = parseInt(objPaymentValue.split("{}")[15]);
    var dclFeeAmount = Math.round(parseFloat(objPaymentValue.split("{}")[16]));
    var dclFeeAmountIncl = Math.round(parseFloat(objPaymentValue.split("{}")[17]));


    //Find Credit Card Fee
    var dblCreditCardFee = CalculateCreditCardFee(dclFeeAmount,
                                                  dclFeeAmountIncl,
                                                  Math.round(parseFloat(0 + GetControlValue(objhdFareTotal))),
                                                  dclFeePercentage,
                                                  Factor,
                                                  minimumFeeFlag);

    CalculateCCFee(strCardNumber,
                    bCalculate,
                    objCardType,
                    objPaymentValue,
                    objDvFareTotal,
                    objCreditCardValue,
                    objCreditCardValue,
                    null,
                    dblCreditCardFee,
                    objhdFareTotal);
}
function VoucherSelect() {
    var objChk = document.getElementsByName("nVoucher");

    if (objChk != null && objChk.length > 0) {
        for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].checked == true) {
                return true;
            }
        }
    }
    return false;
}
function LoadMultiMyName() {
    var objMyname = document.getElementById("chkMultiMyname");
    if (objMyname.checked == true) {
        tikAeroB2C.WebService.B2cService.GetMyName(SuccessLoadMultiMyName, showError, showTimeOut);
    }
    else {
        //If check bok not checked clear card holder information.
        SuccessLoadMultiMyName("");
    }
    objMyname = null;
}
function SuccessLoadMultiMyName(result) {
    var objNameOnCard = document.getElementById("txtMultiNameOnCard");
    var objAddress1 = document.getElementById("txtMultiAddress1");
    var objAddress2 = document.getElementById("txtMultiAddress2");
    var objCity = document.getElementById("txtMultiCity");
    var objCounty = document.getElementById("txtMultiCounty");
    var objPostCode = document.getElementById("txtMultiPostCode");
    var objCountry = document.getElementById(FindControlName("select", "optMultiCountry"));

    SetMyName(result,
              objNameOnCard,
              objAddress1,
              objAddress2,
              objCity,
              objCounty,
              objPostCode,
              objCountry);
}
function CalculateCreditCardFee(feeAmount,
                                feeAmountIncl,
                                saleAmount,
                                feePercentage,
                                Factor,
                                minimumFeeFlag) {

    var dclAmount;
    var dclAmountIncl;
    //Calculate FormOfPaymentFee
    if (feePercentage > 0) {

        var dFee = parseFloat((saleAmount / 100) * feePercentage).toFixed(2);
        if (feeAmountIncl == 0)
        { }
        else if (minimumFeeFlag == 0) {
            if (parseFloat(dFee) > parseFloat(feeAmountIncl)) {
                dFee = feeAmountIncl;
            }
        }
        else {
            if (parseFloat(dFee) < parseFloat(feeAmountIncl)) {
                dFee = feeAmountIncl;
            }
        }
        dclAmount = parseFloat(dFee);
        dclAmountIncl = parseFloat(dFee);
    }
    else {
        dclAmount = feeAmount * Factor;
        dclAmountIncl = feeAmountIncl * Factor;
    }

    return parseFloat(dclAmountIncl);
}
function HideAllPayment() {
    document.getElementById('CreditCard').style.display = 'none';
    document.getElementById('Voucher').style.display = 'none';
    document.getElementById('BookNow').style.display = 'none';
    document.getElementById('STM').style.display = 'none';
    document.getElementById('dvPaymentBack').style.display = 'block';
}
function GetWellNetFeeDisplay() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.CalculateFee(gWellNetFeeCode, true, SuccessGetWellNetFeeDisplay, showErrorPayment, showTimeOutPayment);
}
function SuccessGetWellNetFeeDisplay(result) {

    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {
            //Calculate payment fee failed due to session timeout.
            ShowMessageBox(objLanguage.Alert_Message_128, 0, 'loadHome');
        }
        else {
            SuccessGetSessionQuoteSummary(result);

            var hdFee = document.getElementById("inWellFee");
            var spnFee = document.getElementById("spnWellFee");

            if (hdFee != null && spnFee != null) {
                spnFee.innerHTML = Math.round(parseFloat(hdFee.value));
            }
            if (document.getElementById("ctl00_optCardType") != null) {
                var objCC = document.getElementById("dvcTCreditCard");
                if (objCC != null && objCC.className == "CreditCard") {
                    //Reset Credit card Fee display.
                    validateCC("", false);
                    //Filter Credit card type by currency.
                    FilterCardTypeList(false);

                }

            }
        }
    }
    ShowProgressBar(false);
}
function RemoveWellNetFeeDisplay() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ClearFeeFromCode(gWellNetFeeCode, true, SuccessGetWellNetFeeDisplay, showErrorPayment, showTimeOutPayment);
}

function ClearAdditionalFee() {

    var objDvFareTotal = document.getElementById("dvFareTotal");
    var objTotalAdditinalFee = document.getElementById("divAdditionalFeeTotal");
    var objhdFareTotal = document.getElementById("hdSubTotal");

    if (parseFloat(0 + GetControlValue(objhdFareTotal)) != parseFloat(0 + RemoveCommas(GetControlInnerHtml(objDvFareTotal)))) {
        if (objTotalAdditinalFee != null) {
            objTotalAdditinalFee.innerHTML = 0;
        }
        objDvFareTotal.innerHTML = AddCommas(Math.round(parseFloat(0 + GetControlValue(objhdFareTotal))).toString());
    }
}
function FilledCCYear(strCtrl) {

    var todayDate = new Date();
    var iCurrentYear = todayDate.getFullYear();
    var objExpYear = document.getElementById(strCtrl);
    for (var i = 0; i < 14; i++) {
        var opt = document.createElement("option");

        opt.value = iCurrentYear;
        if (navigator.appName.indexOf('Microsoft') == 0) {
            opt.innerText = iCurrentYear;
        }
        else {
            opt.text = iCurrentYear;
        }
        objExpYear.appendChild(opt);

        //Increment Year.
        iCurrentYear = iCurrentYear + 1;
    }
    SetComboValue(strCtrl, todayDate.getFullYear());
}

function FilterCardTypeList(bMultiple) {
    var se = null;
    var dvCardTypeLogoNonJPY = null;
    var dvCardTypeJPY = null;

    var strCurrency = GetControlValue(document.getElementById("hdCurrencyCode"));

    var strCardType = "";
    if (bMultiple == false) {
        se = document.getElementById(FindControlName("select", "optCardType"));
        dvCardTypeLogoNonJPY = document.getElementById("dvCardTypeLogoNonJPY");
        dvCardTypeJPY = document.getElementById("dvCardTypeLogoJPY");
    }
    else {
        se = document.getElementById(FindControlName("select", "optMultiCardType"));
        dvCardTypeLogoNonJPY = document.getElementById("dvMultiCardTypeLogoNonJPY");
        dvCardTypeJPY = document.getElementById("dvMultiCardTypeLogoJPY");
    }
    if (se != null) {
        //Show hide cc logo.
        if (strCurrency != "JPY") {
            dvCardTypeLogoNonJPY.style.display = "block";
            dvCardTypeJPY.style.display = "none";
        }
        else {
            dvCardTypeLogoNonJPY.style.display = "none";
            dvCardTypeJPY.style.display = "block";
        }

        for (var i = se.length - 1; i >= 0; i--) {
            strCardType = se.options[i].value.split("{}")[0];
            if ((strCardType != "VISA" && strCardType != "MC") && (strCurrency != "JPY")) {
                //Remove Items.
                se.remove(i);
            }
        }
    }
}
function AddCCKeyEvent() {
    $("#txtCardNumber").keyup(function (e) {
        var objCardNumber = document.getElementById("txtCardNumber");
        var objCreditCardValue = document.getElementById("dvCreditCardValue");

        objCardNumber.value = FilterNonNumeric(objCardNumber.value);

        if (objCardNumber.value.length == 6) {
            if (e.keyCode == 8) {

                validateCC(objCardNumber.value, false);
                objCreditCardValue.innerHTML = 0;

            }
            else {
                validateCC(objCardNumber.value, true);

            }
        }
        else if (objCardNumber.value.length < 6 & e.keyCode == 8) {

            validateCC(objCardNumber.value, false);
            objCreditCardValue.innerHTML = 0;
        }
    });
}

function SaveSatim() {
    var objCondition = document.getElementById("chkPaySatim");

    if (objCondition.checked == true) {
        ShowLoadBar(true, false);
        var xmlStr = "<payment>";
        xmlStr += "<form_of_payment_subtype_rcd>" + "CB" + "</form_of_payment_subtype_rcd>";
        xmlStr += "<form_of_payment_rcd>" + "CC" + "</form_of_payment_rcd>";
        xmlStr += "<cvv_required_flag>" + "0" + "</cvv_required_flag>";
        xmlStr += "<display_cvv_flag>" + "0" + "</display_cvv_flag>";
        xmlStr += "<display_expiry_date_flag>" + "0" + "</display_expiry_date_flag>";
        xmlStr += "<expiry_date_required_flag>" + "0" + "</expiry_date_required_flag>";
        xmlStr += "<address_required_flag>" + "0" + "</address_required_flag>";
        xmlStr += "<display_address_flag>" + "0" + "</display_address_flag>";
        xmlStr += "<display_issue_date_flag>" + "0" + "</display_issue_date_flag>";
        xmlStr += "<display_issue_number_flag>" + "0" + "</display_issue_number_flag>";

        xmlStr += "<NameOnCard>" + "</NameOnCard>";
        xmlStr += "<CreditCardNumber>" + "00000000" + "</CreditCardNumber>";

        xmlStr += "<fee_amount_incl>" + "0" + "</fee_amount_incl>";
        xmlStr += "<fee_amount>" + "0" + "</fee_amount>";
        xmlStr += "</payment>";
        tikAeroB2C.WebService.B2cService.SatimPaymentRequest(xmlStr, SuccessProcessCCPayment, showErrorPayment, showTimeOutPayment);
    }
    else {
        ShowMessageBox("Please accept Terms and Conditions !", 0, '');
    }
}

function SuccessProcessCCPayment(result) {
    if (result.split("{}").length > 1)
        ShowMessageBox(result.split("{}")[0] + " : " + result.split("{}")[1], 0, '');
    else
        document.location.href = result;
}