var iPaxSelectPosition = 1;

function ClosePassengerList() {
    var objDisable = document.getElementById("dvProgressBar");
    var objMessage = document.getElementById("dvFormHolder");

    objDisable.style.display = "none";
    objMessage.style.display = "none";
    objMessage.innerHTML = "";

    objDisable = null;
    objMessage = null;
}
function ShowFlightSummary() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ShowFlightSummary(SuccessShowFlightSummary, showError, showTimeOut);
}
function SuccessShowFlightSummary(result) {
    var obj = document.getElementById("dvContainer");
    obj.innerHTML = result;
    obj = null;
    scroll(0, 0);
    ShowProgressBar(false);
}

function ShowPassengerInfoError(strMessage) {
    var objError = document.getElementById("dvPaxError");
    objError.innerHTML = strMessage;
    objError = null;
}

function LockPassengerInput(bValue) {
    //Input Control
    var txtClientNo = document.getElementById("txtClientNoInput");
    var stTitle = document.getElementById("stTitleInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");
    var txtBirthDate = document.getElementById("txtBirthDateInput");
    var stNationality = document.getElementById("stNational");
    var stDocumentType = document.getElementById("stDocumentType");
    var txtDocumentNo = document.getElementById("txtDocumentNumber");
    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
    var txtIssueDate = document.getElementById("txtIssueDate");
    var txtExpiryDate = document.getElementById("txtExpiryDate");
    var stIssueCountry = document.getElementById("stIssueCountry");
    var chkCopy = document.getElementById("chkCopy");

    txtClientNo.disabled = bValue;
    stTitle.disabled = bValue;

    if (txtFirstName != null) {
        txtFirstName.disabled = bValue;
    }

    if (txtLastName != null) {
        txtLastName.disabled = bValue;
    }
}

function ClearAllData() {
    var iPaxcount = document.getElementsByName("hdPassengerId").length;

    var objTitle;
    var objFirstName;
    var objLastName;
    var objClientProfileId;
    var objPassengerProfileId;
    var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
    var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
    var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
    var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
    var objEmailConfirm = document.getElementById(FindControlName("input", "txtEmailConfirm"));
    var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
    var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
    var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
    var objCity = document.getElementById(FindControlName("input", "txtContactCity"));
    var objCountry = document.getElementById(FindControlName("select", "stContactCountry"));
    var objContactLanguage = document.getElementById(FindControlName("select", "stContactLanguage"));

    var objClientNo;
    var objNational;
    var objDocType;
    var objDocNo;
    var objIssuePlace;
    var objIssueDate;
    var objExpiryDate;
    var objBirthDate;
    var objWeight;
    var objBirthPlace;
    var objIssueCountry;

    //Clear passenger information.
    for (var iCount = 1; iCount <= iPaxcount; iCount++) {
        objClientNo = document.getElementById("txtClientNo_" + iCount);
        objTitle = document.getElementById("stTitle_" + iCount);
        objClientProfileId = document.getElementById("hdClientProfileId_" + iCount);
        objPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iCount);
        objFirstName = document.getElementById("txtName_" + iCount);
        objLastName = document.getElementById("txtLastname_" + iCount);
        objNational = document.getElementById("stNational_" + iCount);
        objDocType = document.getElementById("stDocumentType_" + iCount);
        objDocNo = document.getElementById("txtDocNumber_" + iCount);
        objIssuePlace = document.getElementById("txtIssuePlace_" + iCount);

        objIssueDate = document.getElementById("txtIssueDate_" + iCount);
        objExpiryDate = document.getElementById("txtExpiryDate_" + iCount);
        objBirthDate = document.getElementById("txtBirthDate_" + iCount);
        objWeight = document.getElementById("txtWeight_" + iCount);
        objBirthPlace = document.getElementById("txtBirthPlace_" + iCount);
        objIssueCountry = document.getElementById("stIssueCountry_" + iCount);

        if (objClientNo != null) {
            objClientNo.value = "";
        }

        if (objFirstName != null) {
            objFirstName.value = "";
        }

        if (objLastName != null) {
            objLastName.value = "";
        }

        if (objClientProfileId != null) {
            objClientProfileId.value = "";
        }

        if (objPassengerProfileId != null) {
            objPassengerProfileId.value = "";
        }
        if (objWeight != null) {
            objWeight.value = "";
        }
        if (objBirthPlace != null) {
            objBirthPlace.value = "";
        }

        if (objTitle != null) {
            objTitle.options[0].selected = true;
        }
        if (objNational != null) {
            objNational.options[0].selected = true;
        }
        if (objDocType != null) {
            objDocType.options[0].selected = true;
        }
        if (objDocNo != null) {
            objDocNo.value = "";
        }
        if (objIssuePlace != null) {
            objIssuePlace.value = "";
        }

        if (objIssueDate != null) {
            objIssueDate.value = objLanguage.default_value_1;
        }

        if (objExpiryDate != null) {
            objExpiryDate.value = objLanguage.default_value_1;
        }
        if (objBirthDate != null) {
            objBirthDate.value = objLanguage.default_value_1;
        }

        if (objIssueCountry != null) {
            objIssueCountry.options[0].selected = true;
        }

        objClientNo = null;
        objTitle = null;
        objFirstName = null;
        objLastName = null;
        objNational = null;
        objDocType = null;
        objDocNo = null;
        objIssuePlace = null;

        objIssueDate = null;
        objExpiryDate = null;
        objBirthDate = null;
        objWeight = null;
        objBirthPlace = null;
        objIssueCountry = null;
    }


    //Clear contact information.
    objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
    objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));

    if (objClientNo != null) {
        objClientNo.value = "";
    }

    if (objClientProfileId != null) {
        objClientProfileId.value = "";
    }

    if (objTitle != null) {
        objTitle.value = "";
    }

    if (objFirstName != null) {
        objFirstName.value = "";
    }

    if (objLastName != null) {
        objLastName.value = "";
    }

    if (objMobile != null) {
        objMobile.value = "";
    }

    if (objHome != null) {
        objHome.value = "";
    }

    if (objBusiness != null) {
        objBusiness.value = "";
    }

    if (objEmail != null) {
        objEmail.value = "";
    }

    if (objEmailConfirm != null) {
        objEmailConfirm.value = "";
    }

    if (objAddress1 != null) {
        objAddress1.value = "";
    }

    if (objAddress2 != null) {
        objAddress2.value = "";
    }
    if (objZip != null) {
        objZip.value = "";
    }
    if (objCity != null) {
        objCity.value = "";
    }
    if (objCountry != null) {
        objCountry.options[0].selected = true;
    }
    if (objContactLanguage != null) {
        objContactLanguage.options[0].selected = true;
    }


    //Clear error message.
    ShowPassengerInfoError("");
    ClearErrorMsg();

    objClientNo = null;
    objClientProfileId = null;
    objPassengerProfileId = null;
    objTitle = null;
    objFirstName = null;
    objLastName = null;
    objMobile = null;
    objHome = null;
    objBusiness = null;
    objEmail = null;
    objAddress1 = null;
    objAddress2 = null;
    objZip = null;
    objCity = null;
    objCountry = null;
    objContactLanguage = null;

    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.ClearPassengerData(SuccessClearData, showError, showTimeOut);
}

function ClearPassengerList() {

    //Input Control
    var txtClientNo = document.getElementById("txtClientNoInput");
    var stTitle = document.getElementById("stTitleInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");
    var txtBirthDate = document.getElementById("txtBirthDateInput");
    var txtBirthPlace = document.getElementById("txtPlaseOfBirth");
    var stNationality = document.getElementById("stNational");
    var stDocumentType = document.getElementById("stDocumentType");
    var txtDocumentNo = document.getElementById("txtDocumentNumber");
    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
    var txtIssueDate = document.getElementById("txtIssueDate");
    var txtExpiryDate = document.getElementById("txtExpiryDate");
    var txtWeight = document.getElementById("txtWeight");
    var stIssueCountry = document.getElementById("stIssueCountry");
    var chkCopy = document.getElementById("chkCopy");

    //Hidden passenger info.
    var objPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iPaxSelectPosition);
    var objClientProfileId = document.getElementById("hdClientProfileId_" + iPaxSelectPosition);
    var objVipFlag = document.getElementById("hd_Vip_flag_" + iPaxSelectPosition);
    var hdMemberLevel = document.getElementById("hdMemberLevelRcd_" + iPaxSelectPosition);

    //Show Water Mark
    GetPassengerValue(iPaxSelectPosition);

    $("#txtFirstNameInput").addClass("watermarkOn");
    $("#txtLastnameInput").addClass("watermarkOn");

    //Clear Input info.
    if (stTitle != null) {
        stTitle.selectedIndex = 0;
    }
    if (stNationality != null) {
        stNationality.options[0].selected = true;
    }
    if (stDocumentType != null) {
        stDocumentType.options[0].selected = true;
    }

    if (txtClientNo != null) {
        txtClientNo.value = "";
    }
    if (txtFirstName != null) {
        txtFirstName.value = objLanguage.default_value_2;
    }
    if (txtLastName != null) {
        txtLastName.value = objLanguage.default_value_2;
    }
    if (txtDocumentNo != null) {
        txtDocumentNo.value = "";
    }
    if (txtPlaceOfIssue != null) {
        txtPlaceOfIssue.value = "";
    }
    if (txtBirthPlace != null) {
        txtBirthPlace.value = "";
    }
    if (hdMemberLevel != null) {
        hdMemberLevel.value = "";
    }
    if (txtWeight != null) {
        txtWeight.value = 0;
    }

    if (txtBirthDate != null) {
        txtBirthDate.value = objLanguage.default_value_1;
    }
    if (txtIssueDate != null) {
        txtIssueDate.value = objLanguage.default_value_1;
    }
    if (txtExpiryDate != null) {
        txtExpiryDate.value = objLanguage.default_value_1;
    }

    if (stIssueCountry != null) {
        stIssueCountry.options[0].selected = true;
    }

    //If first passenger is clear checkbox have to unchecked.
    if (iPaxSelectPosition == 1) {
        
        if (chkCopy != null) {
            chkCopy.checked = false;
        }
    }
    
    //Set Value to hidden.
    SetPassengerValue();
    //Clear Hidden info.
    objPassengerProfileId.value = "00000000-0000-0000-0000-000000000000";
    objClientProfileId.value = "00000000-0000-0000-0000-000000000000";
    objVipFlag.value = 0;

    //Unlock input
    LockPassengerInput(false);
}
function SuccessClearData() {
    ShowProgressBar(false);
    loadHome();
}
function SavePassengerDetail() {
    var strResult = FillPassengerDetail(); //Validate and fill data.
    if (strResult.length > 0) {

        $("#dvErrorcontent").attr("style", "");

        ConfirmSavePassengerDetail(strResult);
    }
}

function ConfirmSavePassengerDetail(strResult) {
    if (strResult.length > 0) {
        ShowProgressBar(true);

        //Save cookies information.
        var objChk = document.getElementById("chkRemember");

        if (objChk.checked == true) {
            //Save Contact Detail to Cookies.
            ContactDetailToCookies();
        }
        else {
            //Delete Cookies.
            deleteCookie("coContact");
        }
        objChk = null;

        //Call webservice
        tikAeroB2C.WebService.B2cService.FillPassengerData(strResult, true, SuccessSavePassengerDetail, showError, showTimeOut);
    }
}

function SuccessSavePassengerDetail(result) {

    if (result.length > 0) {
        if (result != "{000}") {

            if (result == "{002}") {
                ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
            }
            else if (result == "{004}") {

                //System Error. Please try again.
                ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
            }
            else {

                var obj = document.getElementById("dvContainer");
                obj.innerHTML = result;

                SetPaymentContent();
 
                //Set payment water mark.
                InitializePaymentWaterMark();

                ShowProgressBar(false);
                obj = null;
                
                //Detect keyboard key for Credit Card number.
                AddCCKeyEvent();
            } 


            ToolTipColor();
        }
        else {
            LoadSecure(true);
        }
    }


}
function FillPassengerDetail() {
    //location.reload();
    var iPaxcount = document.getElementsByName("hdPassengerId").length;
    var bPass = true;

    var objTitle;
    var objPassengerId;
    var objFirstName;
    var objLastName;
    var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
    var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
    var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
    var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
    var objEmailConfirm = document.getElementById(FindControlName("input", "txtEmailConfirm"));
    var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
    var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
    var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
    var objCity = document.getElementById(FindControlName("input", "txtContactCity"));
    var objState = document.getElementById(FindControlName("input", "txtContactState"));
    var objCountry = document.getElementById(FindControlName("select", "stContactCountry"));
    var objContactLanguage = document.getElementById(FindControlName("select", "stContactLanguage"));
    var objMobileEmail = document.getElementById(FindControlName("input", "txtMobileEmail"));

    var objClientNo;
    var objPassengerProfileId;
    var objClientProfileId;
    var objVipFlag;
    var objNational;
    var objDocType;
    var objDocNo;
    var objIssuePlace;
    var objIssueDate;
    var objExpiryDate;
    var objBirthDate;
    var objWeight;
    var objBirthPlace;
    var objMemberLevel;
    var objIssueCountry;
    var objrequireDOBFlag;

    var strXml = "";
    var strPassenger = "";
    var strError = "";
    var strErrorPax = "";
    var strContactError = "";
    var strDupName = "";
    var strFirstName = "";
    var strLastName = "";

    var date_of_birth = "";
    var strMsgCHDINF = "";

    //Add passenger information to xml.
    for (var iCount = 1; iCount <= iPaxcount; iCount++) {
        objPassengerId = document.getElementById("hdPassengerId_" + iCount);
        objPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iCount);
        objClientProfileId = document.getElementById("hdClientProfileId_" + iCount);
        objVipFlag = document.getElementById("hd_Vip_flag_" + iCount);
        objClientNo = document.getElementById("hdClientNo_" + iCount);
        objTitle = document.getElementById("hdTitle_" + iCount);
        objFirstName = document.getElementById("hdName_" + iCount);
        objLastName = document.getElementById("hdLastname_" + iCount);
        objNational = document.getElementById("hdNational_" + iCount);
        objDocType = document.getElementById("hdDocumentType_" + iCount);
        objDocNo = document.getElementById("hdDocNumber_" + iCount);
        objIssuePlace = document.getElementById("hdIssuePlace_" + iCount);

        objIssueDate = document.getElementById("hdIssueDate_" + iCount);
        objExpiryDate = document.getElementById("hdExpiryDate_" + iCount);
        objBirthDate = document.getElementById("hdBirthDate_" + iCount);
        objWeight = document.getElementById("hdWeight_" + iCount);
        objBirthPlace = document.getElementById("hdBirthPlace_" + iCount);
        objPaxType = document.getElementById("hdPaxType_" + iCount);
        objMemberLevel = document.getElementById("hdMemberLevelRcd_" + iCount);
        objIssueCountry = document.getElementById("hdIssueCountry_" + iCount);
        objrequireDOBFlag = document.getElementById("requireDOBFlag");

        strErrorPax = "";

        //change class
        $('#dvErrorcontent').css({ "height": "250px", "overflow-y": "auto", "overflow-x": "hidden", "width": "425px" });

        //Title Name Validation
        if (GetControlValue(objTitle) == "|" || GetControlValue(objTitle).length == 0) {
           // bPass = false;
           // strError = strError + objLanguage.Alert_Message_125.replace("{NO}", "") + "<div class='clear-all'></div>";  //"- Please select title for passenger  "
            strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_125.replace("{NO}", "") + "</div></li>";  //"- Please select title for passenger  "
        }



        //First Name Validation
        if (objFirstName != null) {
            if (GetControlValue(objFirstName).length > 0) {
                if (ContainSpecialCharacter(GetControlValue(objFirstName)) == true) {
                    strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_26.replace("{NO}", "") + "</div></li>"; //"Passenger Firstname" + "" + " is not valid.";;
                }
                else if (ContainWhiteSpace(GetControlValue(objFirstName)) == true) {
                    strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_117.replace("{NO}", "") + "</div></li>"; //"- White is not allowed in firstname for passenger "
                }
            }
            else
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_66.replace("{NO}", "") + "</div></li>";  //"- Firstname is required for passenger "
        }

        //Last Name Validation
        if (objLastName != null) {
            if (GetControlValue(objLastName).length > 0) {
                if (ContainSpecialCharacter(GetControlValue(objLastName)) == true) {
                    strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_27.replace("{NO}", "") + "</div></li>"; //"Passenger Lastname" + "" + " is not valid.";;
                }
                else if (ContainWhiteSpace(GetControlValue(objLastName)) == true) {
                    strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_118.replace("{NO}", "") + "</div></li>"; //"- White is not allowed in Lastname for passenger "
                }
            }
            else
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_68.replace("{NO}", "") + "</div></li>"; //"- Lastname is required for passenger "
        }

        if (objBirthDate != null) {
            if (GetControlValue(objBirthDate).length > 0 && GetControlValue(objrequireDOBFlag) == "1") {
                if (DateValid(GetControlValue(objBirthDate)) == false && GetControlValue(objPaxType) != "ADULT") {
                    strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_14.replace("{NO}", "") + "</div></li>"; //"- Please supply valid date of birth."
                }
                else {
                    if (IsPastDate(GetControlValue(objBirthDate)) == false) {
                        strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_62.replace("{NO}", "") + "</div></li>"; //"- Birth Date Should be in the past for passenger "
                    }
                    if (GetControlValue(objBirthDate).length == 0 && GetControlValue(objPaxType) != "ADULT") {
                        strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_63.replace("{NO}", "") + "</div></li>"; //"- Birth Date is required for passenger "
                    }

                    if ((GetControlValue(objPaxType) == "ADULT" & ReformatDate(GetControlValue(objBirthDate)).length > 0) || GetControlValue(objPaxType) == "CHD" || GetControlValue(objPaxType) == "INF" || GetControlValue(objPaxType) == "YP") {
                        date_of_birth = (GetControlValue(objBirthDate) == objLanguage.default_value_1) ? "" : GetControlValue(objBirthDate);
                        if (date_of_birth.length > 0) {
                            var objDeptDate = document.getElementById("spnDD");
                            if (validateChdInfBirthDate(ReformatDate(GetControlValue(objDeptDate)), ReformatDate(date_of_birth), GetControlValue(objPaxType)) == false) {
                                //Select message by passenger type.
                                if (GetControlValue(objPaxType) == "CHD") {
                                    //Alert for child
                                    strMsgCHDINF = objLanguage.Alert_Message_22;
                                }
                                else if (GetControlValue(objPaxType) == "INF") {
                                    //Alert for infant
                                    strMsgCHDINF = objLanguage.Alert_Message_23;
                                }
                                else {
                                    //Alert for Adult
                                    strMsgCHDINF = objLanguage.Alert_Message_116;
                                }
                                strErrorPax += "<li><div class=\"charges_name\"> - " + strMsgCHDINF + "</div></li>"; //"- Birth Date is required for passenger "

                            }
                            objDeptDate = null;
                        }
                    }
                }
            }
            else {
                if (GetControlValue(objrequireDOBFlag) == "1")
                    strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_14.replace("{NO}", "") + "</div></li>"; //"- Date of birth is required "
            }

        }
        //DocType Validation
        if (document.getElementById("stDocumentType") != null) {
            if (GetControlValue(objDocType).length == 0) {
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_126.replace("{NO}", "") + "</div></li>"; //"Passenger Firstname" + "" + " is not valid.";;
            }
        }

        //Valudate Document number
        if (document.getElementById("txtDocumentNumber") != null) {
            if (GetControlValue(objDocNo).length == 0 || IsAlphaNumeric(GetControlValue(objDocNo)) == false) {
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_101.replace("{NO}", "") + "</div></li>"; //"Passenger Firstname" + "" + " is not valid.";;
            }
        }


        //Document Issue Date Validation
        if (document.getElementById("txtIssueDate") != null) {
            if (DateValid(GetControlValue(objIssueDate)) == false) {
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_18.replace("{NO}", "") + "</div></li>"; //"- Please supply valid issue date "
            }
            else {
                if (IsPastDate(GetControlValue(objIssueDate)) == false) {
                    strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_19.replace("{NO}", "") + "</div></li>"; //"- Issue Date Should be in the past for passenger "
                }
            }
        }

        //Document Expiry Date Validation
        if (document.getElementById("txtExpiryDate") != null) {
            if (DateValid(GetControlValue(objExpiryDate)) == false || GetControlValue(objExpiryDate).length == 0) {
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_17.replace("{NO}", "") + "</div></li>"; //"- Please supply valid expiry date "
            }
            else {
                var objLastDeptDate = document.getElementById("spnLDD");
                var strDeptDate = GetControlValue(objLastDeptDate);
                if (IsFutureDate(GetControlValue(objExpiryDate), strDeptDate) == false) {
                    strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_20.replace("{NO}", "") + "</div></li>"; //"- Document is expired for passenger "
                }
            }
        }

        //Document Issue palce Validation
        if (document.getElementById("txtPlaseOfIssue") != null) {
            if (GetControlValue(objIssuePlace).length == 0) {
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_16.replace("{NO}", "") + "</div></li>"; //"Passenger Firstname" + "" + " is not valid.";;
            }
        }

        //Document Validation
        if (document.getElementById("stIssueCountry") != null) {
            if (GetControlValue(objIssueCountry).length == 0) {
                bPass = false;
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_147.replace("{NO}", "") + "</div></li>";  //"- Issue country required"
            }
        }
        //Nationality Validation
        if (document.getElementById("stNational") != null) {
            if (GetControlValue(objNational).length == 0) {
                strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_127.replace("{NO}", "") + "</div></li>";  //"- Issue country required"
            }
        }

        strFirstName = ReplaceSpecialCharacter(GetControlValue(objFirstName)).toUpperCase();
        strLastName = ReplaceSpecialCharacter(GetControlValue(objLastName)).toUpperCase();

        strDupName = GetDuplicatePassenger(strLastName, strFirstName, iCount - 1);
        if (strDupName.length > 0) {
            strErrorPax += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_64.replace("{NO}", "") + "</div></li>"; //"- Passenger " + strDupName + "  already selected "
        }

        if (strErrorPax != "") {
            strError += "<div class=\"MessageGroup\">" +
                        "<ul id=\"ul_PaxInfo" + "" + "\">" +
                        "<li>" +
                        "<a style=\"cursor: pointer;\" >" +
                        "<div class=\"Parentmessage\">" + " Passenger " + padZeros(iCount, 3) + "</div>" +
                        "<div class=\"collapse\">" +
                        "</div>" +
                        "<div class=\"clear-all\"></div>" +
                        "</a>" +
                        "<ul class=\"Childmessage\">" +
                        strErrorPax +
                        "</ul></li></ul></div>";
        }




        if (strError == "") {
            //Pass criteria check.
            strPassenger = strPassenger +
                            "<passenger>" +
                                    "<passenger_id>" + GetControlValue(objPassengerId) + "</passenger_id>" +
                                    "<passenger_profile_id>" + GetControlValue(objPassengerProfileId) + "</passenger_profile_id>" +
                                    "<client_profile_id>" + GetControlValue(objClientProfileId) + "</client_profile_id>" +
                                    "<client_no>" + ((objClientNo.value.length == 8) ? objClientNo.value : "") + "</client_no>" +
                                    "<firstname>" + ConvertToValidXmlData(strFirstName).toUpperCase() + "</firstname>" +
                                    "<lastname>" + ConvertToValidXmlData(strLastName).toUpperCase() + "</lastname>" +
                                    "<title_rcd>" + GetControlValue(objTitle).toUpperCase() + "</title_rcd>" +
                                    "<nationality_rcd>" + GetControlValue(objNational) + "</nationality_rcd>" +
                                    "<document_type_rcd>" + ((GetControlValue(objDocNo).length > 0) ? "P" : "") + "</document_type_rcd>" +
                                    "<document_no>" + ConvertToValidXmlData(GetControlValue(objDocNo)) + "</document_no>" +
                                    "<issue_place>" + ConvertToValidXmlData(GetControlValue(objIssuePlace)) + "</issue_place>" +
                                    "<issue_date>" + ReformatDate(GetControlValue(objIssueDate)) + "</issue_date>" +
                                    "<expiry_date>" + ReformatDate(GetControlValue(objExpiryDate)) + "</expiry_date>" +
                                    "<date_of_birth>" + ReformatDate(GetControlValue(objBirthDate)) + "</date_of_birth>" +
                                    "<passenger_weight>" + ConvertToValidXmlData(GetControlValue(objWeight)) + "</passenger_weight>" +
                                    "<passport_birth_place>" + ConvertToValidXmlData(GetControlValue(objBirthPlace)) + "</passport_birth_place>" +
                                    "<vip_flag>" + GetControlValue(objVipFlag) + "</vip_flag>" +
                                    "<member_level_rcd>" + GetControlValue(objMemberLevel) + "</member_level_rcd>" +
                                    "<passport_issue_country_rcd>" + GetControlValue(objIssueCountry) + "</passport_issue_country_rcd>" +
                               "</passenger>";
        }

        objClientNo = null;
        objTitle = null;
        objFirstName = null;
        objLastName = null;
        objNational = null;
        objDocType = null;
        objDocNo = null;
        objIssuePlace = null;

        objIssueDate = null;
        objExpiryDate = null;
        objBirthDate = null;
        objWeight = null;
        objBirthPlace = null;
        objIssueCountry = null;
    }

    //Add Contact detail information to xml.
    objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
    objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));

    var txtEUVat = document.getElementById(FindControlName("input", "txtEUVat"));
    var txtCIN = document.getElementById(FindControlName("input", "txtCIN"));

    if (GetSelectedOption(objTitle).length == 0) {
        strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_122 + "</div></li>"; //"* Title Required!!";
        document.getElementById("spErrFirstname").innerHTML = "";
    }

    if (objFirstName != null) {
        if (GetControlValue(objFirstName).length == 0) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_66 + "</div></li>"; //"* Firstname required!!";
        }
        else {
            if (ContainSpecialCharacter(GetControlValue(objFirstName)) || IsChar(GetControlValue(objFirstName)) == false) {
                strContactError += "<li><div class=\"charges_name\"> - " + "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_65 + "</div></li>"; //"* Invalid input!!";
            }
            if (ContainWhiteSpace(GetControlValue(objFirstName)) == true) {
                strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_119 + "</div></li>"; //"* White is not allowed in firstname"
            }
        }
    }

    if (objLastName != null) {
        if (GetControlValue(objLastName).length == 0) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_68 + "</div></li>"; //"* Lastname required!!";
        }
        else {
            if (ContainSpecialCharacter(GetControlValue(objLastName)) == true || IsChar(GetControlValue(objLastName)) == false) {
                strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_67 + "</div></li>"; //"* Invalid input!!";
            }
            if (ContainWhiteSpace(GetControlValue(objLastName)) == true) {
                strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_120 + "</div></li>"; //"* White is not allowed in last"
            }
        }
    }


    if (objEmail != null) {
        if (GetControlValue(objEmail).length == 0) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_33 + "</div></li>"; //"* Email required !!";
        }
        else if (ValidEmail(GetControlValue(objEmail)) == false) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_24 + "</div></li>"; //"* Supply valid Email address !!";   
        }
    }

    if (objEmailConfirm != null) {
        if (GetControlValue(objEmailConfirm).length == 0) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_162 + "</div></li>"; //"* Email Confirm required !!";
        }
        else if (ValidEmail(GetControlValue(objEmailConfirm)) == false) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_163 + "</div></li>"; //"* Supply valid Email address !!";   
        }
        else if (GetControlValue(objEmailConfirm) != GetControlValue(objEmail)) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_164 + "</div></li>"; 
        }
    }

    if (GetControlValue(objMobileEmail).length > 0 && ValidEmail(GetControlValue(objMobileEmail)) == false) {
        strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_24 + "</div></li>"; //"* Supply valid Email address !!";   
    }

    if (objHome != null) {
//        if (GetControlValue(objHome).length == 0) {
//            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_69 + "</div></li>";
//        }
//        else 
        if (GetControlValue(objHome).length > 0 && ValidPhoneNumber(GetControlValue(objHome)) == false) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_32 + "</div></li>";
        }
    }

    if (objMobile != null) {
        if (GetControlValue(objMobile).length == 0) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_31 + "</div></li>";
        }
        else if (GetControlValue(objMobile).length > 0) {
            if (ValidPhoneNumber(GetControlValue(objMobile)) == false) {
                strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_31 + "</div></li>";
            }
        }
    }


    if (objBusiness != null) {
        if (GetControlValue(objBusiness).length > 0) {
            if (ValidPhoneNumber(GetControlValue(objBusiness)) == false) {
                strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_69 + "</div></li>";
            }
        }
    }

//    if (objAddress1 != null) {
//        if (GetControlValue(objAddress1).length == 0) {
//            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_41 + "</div></li>"; //"* Address 1 required!!";
//        }
//    }

//    if (objCity != null) {
//        if (GetControlValue(objCity).length == 0) {
//            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_42 + "</div></li>"; //"* City required!!";
//        }
//    }

//    if (objZip != null) {
//        if (GetControlValue(objZip).length == 0) {
//            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_43 + "</div></li>"; //"* Post code required!!";
//        }
//    }

    if (objContactLanguage != null) {
        if (GetSelectedOption(objContactLanguage).length == 0) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_124 + "</div></li>"; //"* Please select contact Language!!";
        }
    }

    if (objCountry != null) {
        if (GetSelectedOption(objCountry).length == 0) {
            strContactError += "<li><div class=\"charges_name\"> - " + objLanguage.Alert_Message_123 + "</div></li>"; //"* Please select contact country!!";
        }
    }

    if (strContactError != "") {
        strContactError = "<div class=\"MessageGroup\">" +
                        "<ul id=\"ul_ContactDetail\">" +
                        "<li>" +
                        "<a style=\"cursor: pointer;\" >" +
                        "<div class=\"Parentmessage\">" + " Contact Information</div>" +
                        "<div class=\"collapse\">" +
                        "</div>" +
                        "<div class=\"clear-all\"></div>" +
                        "</a>" +
                        "<ul class=\"Childmessage\">" +
                        strContactError +
                        "</ul></li></ul></div>";
    }
    if (strError == "" && strContactError == "") {
        //All validation pass goto next step.
        var todayDate = new Date();
        strXml = "<booking>" +
                    "<contact>" +
                        "<client_profile_id>" + GetControlValue(objClientProfileId) + "</client_profile_id>" +
                        "<client_number>" + GetControlValue(objClientNo) + "</client_number>" +
                        "<title>" + GetSelectedOption(objTitle) + "</title>" +
                        "<firstname>" + ConvertToValidXmlData(GetControlValue(objFirstName)) + "</firstname>" +
                        "<lastname>" + ConvertToValidXmlData(GetControlValue(objLastName)) + "</lastname>" +
                        "<mobile>" + ConvertToValidXmlData(GetControlValue(objMobile)) + "</mobile>" +
                        "<home>" + ConvertToValidXmlData(GetControlValue(objHome)) + "</home>" +
                        "<business>" + ConvertToValidXmlData(GetControlValue(objBusiness)) + "</business>" +
                        "<email>" + ConvertToValidXmlData(GetControlValue(objEmail)) + "</email>" +
                        "<addresss_1>" + ConvertToValidXmlData(GetControlValue(objAddress1)) + "</addresss_1>" +
                        "<addresss_2>" + ConvertToValidXmlData(GetControlValue(objAddress2)) + "</addresss_2>" +
                        "<street>" + ConvertToValidXmlData(GetControlValue(objAddress2)) + "</street>" +
                        "<zip>" + ConvertToValidXmlData(GetControlValue(objZip)) + "</zip>" +
                        "<city>" + ConvertToValidXmlData(GetControlValue(objCity)) + "</city>" +
                        "<state>" + ConvertToValidXmlData(GetControlValue(objState)) + "</state>" +
                        "<country>" + GetSelectedOption(objCountry) + "</country>" +
                        "<country_displayname>" + GetSelectedOptionText(objCountry) + "</country_displayname>" +
                        "<language>" + GetSelectedOption(objContactLanguage) + "</language>" +
                        "<newsletter_flag>0</newsletter_flag>" +
                        "<mobile_email>" + GetControlValue(objMobileEmail) + "</mobile_email>" +
                    "</contact>" +
                    strPassenger +
                 "</booking>";
    }
    else {
        //Failed criteria check go out of the loop.
        ShowMessageBox(strContactError + "<div class='clear-all'></div>" + strError, 0, '');
        strXml = "";
    }

    return strXml;
}
function CopyContactToPassenger() {
    var objChk = document.getElementById("chkCopy");
    var objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    var objCTitle = document.getElementById(FindControlName("select", "stContactTitle")); //Contact Title.
    var objCFirstName = document.getElementById(FindControlName("input", "txtContactFirstname")); //Contact firstname.
    var objCLastName = document.getElementById(FindControlName("input", "txtContactLastname")); //Contact lastname.

    //Input Informatiob
    var stTitle = document.getElementById("stTitleInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");

    var objTitle;
    //Passener hidden information.
    var objTitle;
    var objFirstName;
    var objLastName;

    var iPaxcount = document.getElementsByName("hdPassengerId").length;

    if (objChk.checked == true) {

        //Passener hidden information.
        objTitle = document.getElementById("hdTitle_1");
        objFirstName = document.getElementById("hdName_1");
        objLastName = document.getElementById("hdLastname_1");

        if (objClientProfileId.value.length == 0 || objClientProfileId.value == "00000000-0000-0000-0000-000000000000") {
            //Fill Hidden information.
            if (GetSelectedOption(objCTitle).length > 0) {
                objTitle.value = GetSelectedOption(objCTitle).toUpperCase();
            }
            if (GetControlValue(objCFirstName).length > 0) {
                objFirstName.value = GetControlValue(objCFirstName).toUpperCase();
            }
            if (GetControlValue(objCLastName).length > 0) {
                objLastName.value = GetControlValue(objCLastName).toUpperCase();
            }

            GetPassengerValue(1);

            if (document.getElementById("dvPaxTabName") != null) {
                if (GetControlValue(objFirstName).length > 0 & GetControlValue(objLastName).length > 0) {
                    document.getElementById("dvPaxTabName").innerHTML = GetSelectedOption(objCTitle).split("|")[0] + "  " + GetControlValue(objFirstName) + "  " + GetControlValue(objLastName);
                }
                else {
                    document.getElementById("dvPaxTabName").innerHTML = objLanguage.default_value_4;
                }
            }
        }
        else {
            //Get Client Information(Myself).
            GetClient(1, true);
        }
    }
    else {
        //Clear Passenger information.
        iPaxSelectPosition = 1;
        ClearPassengerList();
    }

}

function GetSeatMap(strSelectedFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass) {
    var strResult = FillPassengerDetail(); //Validate and fill data.
    if (strResult.length > 0) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ShowSeatMap(strResult, strSelectedFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass, SuccessLoadSeatMap, showError, strBoardingClass + "|" + strOriginRcd + "|" + strDestinationRcd);
    }

}
function SuccessLoadSeatMap(result, param) {

    if (result == "{002}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        var objChk = document.getElementById("chkRemember");
        if (objChk.checked == true) {
            //Save Contact Detail to Cookies.
            ContactDetailToCookies();
        }
        else {
            //Delete Cookies.
            deleteCookie("coContact");
        }

        ShowProgressBar(false);
        if (result.length > 0) {
            var objHolder = document.getElementById("dvFormHolder");
            var objContainer = document.getElementById("dvProgressBar");

            //Insert passenger form content.
            objHolder.innerHTML = result;
            ClearErrorMsg();
            //Show Fading.
            objContainer.style.display = "block";
            objHolder.style.display = "block";

            //check seatmap was set or not
            if (result.indexOf('selectSeat') < 0) {
                ShowMessageBox(objLanguage.Alert_Message_161, 0, '');
                objHolder.innerHTML = "";
                objHolder.style.display = "none";
            }

            objHolder = null;
            objContainer = null;
        }

        objChk = null;
    }
}

function ContactDetailToCookies() {
    var objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
    var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
    var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
    var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
    var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
    var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
    var objEmailConfirm = document.getElementById(FindControlName("input", "txtEmailConfirm"));
    var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
    var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
    var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
    var objCity = document.getElementById(FindControlName("input", "txtContactCity"));
    var objCountry = document.getElementById(FindControlName("select", "stContactCountry"));
    var objContactLanguage = document.getElementById(FindControlName("select", "stContactLanguage"));

    var objTaxId = document.getElementById(FindControlName("input", "txtTIN"));
    var objInvoiceReceiver = document.getElementById(FindControlName("input", "txtInvoiceReceiver"));
    var txtEUVat = document.getElementById(FindControlName("input", "txtEUVat"));
    var txtCIN = document.getElementById(FindControlName("input", "txtCIN"));
    var objOptionalEmail = document.getElementById(FindControlName("input", "txtContactEmail2"));
    var objMobileEmail = document.getElementById(FindControlName("input", "txtMobileEmail"));
    var objState = document.getElementById(FindControlName("input", "txtContactState"));

    var cookiesValue = GetSelectedOption(objTitle) + "{}" +
                       GetControlValue(objFirstName) + "{}" +
                       GetControlValue(objLastName) + "{}" +
                       GetControlValue(objMobile) + "{}" +
                       GetControlValue(objHome) + "{}" +
                       GetControlValue(objBusiness) + "{}" +
                       GetControlValue(objEmail) + "{}" +
                       GetControlValue(objAddress1) + "{}" +
                       GetControlValue(objAddress2) + "{}" +
                       GetControlValue(objZip) + "{}" +
                       GetControlValue(objCity) + "{}" +
                       GetSelectedOption(objCountry) + "{}" +
                       GetSelectedOption(objContactLanguage) + "{}" +
                       GetControlValue(objTaxId) + "{}" +
                       GetControlValue(objInvoiceReceiver) + "{}" +
                       GetControlValue(txtEUVat) + "{}" +
                       GetControlValue(txtCIN) + "{}" +
                       GetControlValue(objOptionalEmail) + "{}" +
                       GetControlValue(objMobileEmail) + "{}" +
                       GetControlValue(objState) + "{}" +
                       GetControlValue(objEmailConfirm);

    var dtExpired = new Date();

    //Add 5 days Expiry date to cookies.
    dtExpired.setDate(dtExpired.getDate() + iCookiesExpDays);
    setCookie("coContact", $.base64.encode(cookiesValue), dtExpired, "", "", false);

    dtExpired = null;
}

function CopyClientToContact(objClient) {
    var objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
    var objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
    var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
    var objContactHome = document.getElementById(FindControlName("input", "txtContactHome"));
    var objContactBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
    var objContactEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
    var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
    var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
    var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
    var objCity = document.getElementById(FindControlName("input", "txtContactCity"));

    //Copy first client information to contact detail
    objClientNo.value = objClient.client_number;
    objClientProfileId.value = objClient.client_profile_id;
    SetComboValue(FindControlName("select", "stContactTitle"), objClient.title_rcd + "|" + objClient.gender_type_rcd);
    objFirstName.value = objClient.firstname;
    objLastName.value = objClient.lastname;
    objMobile.value = objClient.phone_mobile;
    objContactHome.value = objClient.phone_home;
    objContactBusiness.value = objClient.phone_business;
    objContactEmail.value = objClient.contact_email;
    objAddress1.value = objClient.address_line1;
    objAddress2.value = objClient.address_line2;
    objZip.value = objClient.zip_code;
    objCity.value = objClient.city;
    SetComboValue(FindControlName("select", "stContactCountry"), objClient.nationality_rcd);

    objClientNo = null;
    objClientProfileId = null;
    objFirstName = null;
    objLastName = null;
    objMobile = null;
    objContactHome = null;
    objContactBusiness = null;
    objContactEmail = null;
    objAddress1 = null;
    objAddress2 = null;
    objZip = null;
    objCity = null;
}

//***************************************************
//  Select Passenger Function Section
//***************************************************
function GetClient(position, bGetMyself) {

    if (position == 0) {
        position = iPaxSelectPosition;
    }
    var objLastname = document.getElementById("hdLastname_" + position);
    var objClientProfileId = document.getElementById(FindControlName("input", "txtClientProfileId"));
    var objClientNo = document.getElementById("ctl00_txtClientNumber");

    ShowProgressBar(true);

    if (GetControlValue(objClientNo).length > 0 && bGetMyself == false) {
        tikAeroB2C.WebService.B2cService.GetClient(GetControlValue(objClientNo), "", true, SuccessGetClient, showError, showTimeOut);
    }
    else if (bGetMyself == true && (GetControlValue(objClientProfileId).length > 0 || GetControlValue(objClientProfileId) != "00000000-0000-0000-0000-000000000000")) {

        iPaxSelectPosition = position;
        var objClientNumber = document.getElementById(FindControlName("input", "txtClientNumber"));

        tikAeroB2C.WebService.B2cService.GetClient(GetControlValue(objClientNumber), GetControlValue(objLastname), false, SuccessGetClientMyself, showError, showTimeOut);
        objClientNumber = null;
    }
    else {
        //Field client number is empty.
        //Show error message.
        LockPassengerInput(false);
        ShowProgressBar(false);
        ClearPassengerList();
    }
    objClientNo = null;
    objLastname = null;
    objClientProfileId = null;
}

function SuccessGetClient(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
         }
        else {
            var objMessage = document.getElementById("dvFormHolder");
            var objContainer = document.getElementById("dvProgressBar");

            //Insert passenger form content.
            objMessage.innerHTML = result;

            var objCheckBox = document.getElementsByName("chkClient");
            if (objCheckBox.length > 1) {
                //Show Fading.
                objContainer.style.display = "block";
                objMessage.style.display = "block";
                //Get Total Amount of passenger
                document.getElementById("dvTotalPax").innerHTML = 1;
            }
            else {
                //Find only myself.
                objContainer.style.display = "none";
                objMessage.style.display = "none";
                //Default select passenger.
                if (objCheckBox.length > 0)
                { objCheckBox[0].checked = true; }
                GetSelectedClientPosition();
            }
            objCheckBox = null;

            objMessage = null;
        }
    }

}
function SuccessGetClientMyself(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        if (result == "{002}" || result == "{004}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
         }
        else {
            var objMessage = document.getElementById("dvFormHolder");
            var objContainer = document.getElementById("dvProgressBar");

            //Insert passenger form content.
            objMessage.innerHTML = result;

            var objCheckBox = document.getElementsByName("chkClient");
            if (objCheckBox.length > 0) {
                //Find only myself.
                objContainer.style.display = "none";
                objMessage.style.display = "none";
                //Default select passenger.
                if (objCheckBox.length > 0)
                { objCheckBox[0].checked = true; }
                GetSelectedClientPosition();
            }

            objCheckBox = null;
            objMessage = null;
        }
    }

}
function TotalPassengerAvailable() {
    var objPax = document.getElementsByName("txtLastname");
    var pCount = 0;

    for (var i = 0; i < objPax.length; i++) {
        if (objPax[i].value.length == 0) {
            pCount++;
        }
    }
    return pCount;
}
function GetSelectedClient() {
    var objCheckBox = document.getElementsByName("chkClient");
    var bFound = false;
    var strClientNumber;
    //iCount = Search Box Count.
    //jCount = Passenger Form Count.   
    for (var iCount = 0; iCount < objCheckBox.length; iCount++) {
        if (objCheckBox[iCount].checked == true) {
            var objTotalPax = document.getElementById("dvTotalPax");
            for (var jCount = 1; jCount <= document.getElementsByName("txtLastname").length; jCount++) {
                if ((document.getElementById("tdPaxType_" + jCount).innerHTML == document.getElementById("dvListPaxType_" + (iCount + 1)).innerHTML)) {
                    //Hidden Field Data
                    document.getElementById("hdClientProfileId_" + jCount).value = document.getElementById("hdListClientProfileId_" + (iCount + 1)).value;
                    document.getElementById("hdPassengerProfileId_" + jCount).value = document.getElementById("hdListPassengerProfileId_" + (iCount + 1)).value;
                    document.getElementById("hd_Vip_flag_" + jCount).value = document.getElementById("hdListVipFlag_" + (iCount + 1)).value;

                    //Non Hidden Field Data
                    strClientNumber = document.getElementById("hdListClientNo_" + (iCount + 1)).value;
                    document.getElementById("txtClientNo_" + jCount).value = strClientNumber;
                    SetComboValue("stTitle_" + jCount, document.getElementById("hdListGender_" + (iCount + 1)).value);
                    document.getElementById("txtLastname_" + jCount).value = document.getElementById("dvListLastName_" + (iCount + 1)).innerHTML;
                    document.getElementById("txtName_" + jCount).value = document.getElementById("dvListFirstName_" + (iCount + 1)).innerHTML;
                    SetComboValue("stNational_" + jCount, document.getElementById("hdListNationality_" + (iCount + 1)).value);
                    SetComboValue("stDocumentType_" + jCount, document.getElementById("hdListDocumentType_" + (iCount + 1)).value);
                    document.getElementById("txtDocNumber_" + jCount).value = document.getElementById("hdListDocumentNo_" + (iCount + 1)).value;
                    document.getElementById("txtIssuePlace_" + jCount).value = document.getElementById("hdListIssuePlace_" + (iCount + 1)).value;
                    document.getElementById("txtIssueDate_" + jCount).value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListIssueDate_" + (iCount + 1)).value));
                    document.getElementById("txtExpiryDate_" + jCount).value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListExpiryDate_" + (iCount + 1)).value));
                    document.getElementById("txtBirthDate_" + jCount).value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListDateOfBirth_" + (iCount + 1)).value));
                    document.getElementById("txtWeight_" + jCount).value = document.getElementById("hdListWeight_" + (iCount + 1)).value;
                    document.getElementById("hdMemberLevelRcd_" + jCount).value = document.getElementById("hdListMemberLevel_" + (iCount + 1)).value;
                    SetComboValue("stIssueCountry_" + jCount, document.getElementById("hdListIssueCountry_" + (iCount + 1)).value);

                    //Lock Control.
                    LockPassengerInput(true);
                    bFound = true;
                    break;
                }
            }

        }
    }

    ClosePassengerList();
    document.getElementById("dvFormHolder").innerHTML = "";
    var objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
    if (bFound == true && objClientNo.value != document.getElementById("txtClientNo_1").value) {
        //Get Myself to contact detail.
        GetClientProfile(strClientNumber);
    }
    objClientNo = null;

    objCheckBox = null;
}

function GetSelectedClientPosition() {
    if (iPaxSelectPosition > 0) {
        var objCheckBox = document.getElementsByName("chkClient");
        var bFound = false;
        var strClientNumber;
        //iCount = Search Box Count.

        for (var iCount = 0; iCount < objCheckBox.length; iCount++) {
            if (objCheckBox[iCount].checked == true) {
                var objTotalPax = document.getElementById("dvTotalPax");
                if ((GetControlValue(document.getElementById("hdPaxType_" + iPaxSelectPosition)) == GetControlInnerHtml(document.getElementById("dvListPaxType_" + (iCount + 1))))) {

                    //Input Control
                    var txtClientNo = document.getElementById("txtClientNoInput");
                    var txtFirstName = document.getElementById("txtFirstNameInput");
                    var txtLastName = document.getElementById("txtLastnameInput");
                    var txtBirthDate = document.getElementById("txtBirthDateInput");
                    var txtBirthPlace = document.getElementById("txtPlaseOfBirth");

                    var txtDocumentNo = document.getElementById("txtDocumentNumber");
                    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
                    var txtIssueDate = document.getElementById("txtIssueDate");
                    var txtExpiryDate = document.getElementById("txtExpiryDate");
                    var txtWeight = document.getElementById("txtWeight");

                    //Hidden passenger info.
                    var objPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iPaxSelectPosition);
                    var objClientProfileId = document.getElementById("hdClientProfileId_" + iPaxSelectPosition);
                    var objVipFlag = document.getElementById("hd_Vip_flag_" + iPaxSelectPosition);
                    var hdMemberLevel = document.getElementById("hdMemberLevelRcd_" + iPaxSelectPosition);

                    //Hidden Field Data
                    if (objClientProfileId != null) {
                        objClientProfileId.value = GetControlValue(document.getElementById("hdListClientProfileId_" + (iCount + 1)));
                    }
                    if (objPassengerProfileId != null) {
                        objPassengerProfileId.value = GetControlValue(document.getElementById("hdListPassengerProfileId_" + (iCount + 1)));
                    }
                    if (objVipFlag != null) {
                        objVipFlag.value = GetControlValue(document.getElementById("hdListVipFlag_" + (iCount + 1)));
                    }

                    //Non Hidden Field Data
                    if (txtClientNo != null) {
                        strClientNumber = GetControlValue(document.getElementById("hdListClientNo_" + (iCount + 1)));
                        txtClientNo.value = strClientNumber;
                    }
                    if (txtLastName != null) {
                        txtLastName.value = GetControlInnerHtml(document.getElementById("dvListLastName_" + (iCount + 1)));
                    }
                    if (txtFirstName != null) {
                        txtFirstName.value = GetControlInnerHtml(document.getElementById("dvListFirstName_" + (iCount + 1)));
                    }
                    if (txtDocumentNo != null) {
                        txtDocumentNo.value = GetControlValue(document.getElementById("hdListDocumentNo_" + (iCount + 1)));
                    }
                    if (txtPlaceOfIssue != null) {
                        txtPlaceOfIssue.value = GetControlValue(document.getElementById("hdListIssuePlace_" + (iCount + 1)));
                    }
                    if (txtIssueDate != null) {
                        txtIssueDate.value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListIssueDate_" + (iCount + 1)).value));
                    }
                    if (txtExpiryDate != null) {
                        txtExpiryDate.value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListExpiryDate_" + (iCount + 1)).value));
                    }
                    if (txtBirthDate != null) {
                        txtBirthDate.value = DateMaskFormat(ReformatXmlViewDate(document.getElementById("hdListDateOfBirth_" + (iCount + 1)).value));
                    }
                    if (txtBirthPlace != null) {
                        txtBirthPlace.value = GetControlValue(document.getElementById("hdListBirthPlace" + (iCount + 1)));
                    }
                    if (txtWeight != null) {
                        txtWeight.value = GetControlValue(document.getElementById("hdListWeight_" + (iCount + 1)));
                    }
                    if (hdMemberLevel != null) {
                        hdMemberLevel.value = GetControlValue(document.getElementById("hdListMemberLevel_" + (iCount + 1)));
                    }

                    SetComboSplitValue("stTitleInput", GetControlValue(document.getElementById("hdListGender_" + (iCount + 1))).split("|")[0], 0);
                    SetComboValue("stNational", GetControlValue(document.getElementById("hdListNationality_" + (iCount + 1))));
                    SetComboValue("stDocumentType", GetControlValue(document.getElementById("hdListDocumentType_" + (iCount + 1))));
                    SetComboValue("stIssueCountry", GetControlValue(document.getElementById("hdListIssueCountry_" + (iCount + 1))));

                    //Set Value to hidden field.
                    SetPassengerValue();
                    //Lock Control.
                    LockPassengerInput(true);

                    bFound = true;
                    break;
                }
            }
        }

        ClosePassengerList();

        var objClientNo = document.getElementById(FindControlName("input", "txtClientNumber"));
        if (bFound == true && objClientNo.value != document.getElementById("hdClientNo_1").value) {
            //Get Myself to contact detail.
            GetClientProfile(strClientNumber);
        }
        objClientNo = null;
        objCheckBox = null;
    }
}

function CheckPassengerSelect(strCtrl) {
    var objPaxSelecter = document.getElementById("dvPaxSelecter");
    var objTotalPax = document.getElementById("dvTotalPax");
    var objCheck = document.getElementById(strCtrl);

    //Add and subtract number of selected passenger.
    if (objCheck.checked == true) {
        if (objPaxSelecter.innerHTML >= objTotalPax.innerHTML) {
            //Excess the limit amount of passenger.
            objCheck.checked = false;
        }
        else {
            //Add number of selected passenger.
            objPaxSelecter.innerHTML = parseInt(objPaxSelecter.innerHTML) + 1;
        }
    }
    else {
        //sub number of selected passenger.
        objPaxSelecter.innerHTML = parseInt(objPaxSelecter.innerHTML) - 1;
    }


    objPaxSelecter = null;
    objTotalPax = null;
    objCheck = null;
}

function GetClientProfile(strClientNumber) {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetClientProfile(strClientNumber, SuccessGetClientProfile, showError, showTimeOut);
}

function SuccessGetClientProfile(result) {
    var objClient = eval("(" + result + ")");
    var clientProfileId = document.getElementById("");

    CopyClientToContact(objClient[0]);
    ShowProgressBar(false);
}
function IsTypeAvailable(strType) {
    var objLastname = document.getElementsByName("txtLastname");
    var bAvailable = false;

    for (var i = 0; i < objLastname.length; i++) {
        if (objLastname[i].value.length == 0 && strType == document.getElementById("tdPaxType_" + (i + 1)).innerHTML) {
            bAvailable = true;
            break;
        }
    }

    objLastname = null;
    objPaxType = null;

    return bAvailable;
}

function GetDuplicatePassenger(strLastname, strName, iPosition) {
    var objLastname = document.getElementsByName("txtLastname");
    var objName = document.getElementsByName("txtName");

    for (var i = 0; i < objLastname.length; i++) {
        if (objLastname[i].value.length > 0 && objName[i].value.length > 0) {
            if (strLastname == objLastname[i].value.toUpperCase() && strName == objName[i].value.toUpperCase() && iPosition != i) {
                return strLastname + "/" + strName;
            }
        }
    }
    return "";
}

function GetAccuralQuote() {
    tikAeroB2C.WebService.B2cService.AccuralQuote(SuccessAccuralQuote, showError, showTimeOut);
}
function SuccessAccuralQuote(result) {
    if (result.length > 0) {
        var objAccualSummary = document.getElementById("dvAccualSummary");
        objAccualSummary.innerHTML = result;
        objAccualSummary = null;
    }
}
function CalculateSpecialService() {
    var objFeeId = document.getElementsByName("hdFeeId");
    var objSsrSegment;

    //Loop through fee type
    for (var i = 0; i < objFeeId.length; i++) {
        //Loop throught number of selection box(number of passenger + number of segment).
        objSsrSegment = document.getElementsByName("seSsrAmount_" + (i + 1));
        //Calculate Total Ssr per passenger.
        CalculatePaxSsr((i + 1));
        objSsrSegment = null;
    }

    objFeeId = null;
}

function FillSpecialService() {
    var objFeeId = document.getElementsByName("hdFeeId");
    var objSsrCode = document.getElementsByName("hdSsrCode");
    var objSsrOnRequestFlag = document.getElementsByName("hdSsrOnRequestFlag");
    var objSsrSegment;
    var strXml;
    var objSsr;
    strXml = "<booking>";

    //Loop through fee type
    for (var i = 0; i < objFeeId.length; i++) {
        //Loop throught number of selection box(number of passenger + number of segment).
        objSsrSegment = document.getElementsByName("seSsrAmount_" + (i + 1));

        //Calculate Total Ssr per passenger.
        CalculateSpecialService();


        for (var j = 0; j < objSsrSegment.length; j++) {
            //Loop Through segment(if not married flight it will loop only one time per segment).
            objSsr = eval("([" + objSsrSegment[j].options[objSsrSegment[j].selectedIndex].value + "])");
            for (var k = 0; k < objSsr.length; k++) {
                strXml = strXml +
                        "<service>" +
                            "<passenger_id>" + objSsr[k].passenger_id + "</passenger_id>" +
                            "<booking_segment_id>" + objSsr[k].booking_segment_id + "</booking_segment_id>" +
                            "<origin_rcd>" + objSsr[k].origin_rcd + "</origin_rcd>" +
                            "<destination_rcd>" + objSsr[k].destination_rcd + "</destination_rcd>" +
                            "<fee_id>" + objFeeId[i].value + "</fee_id>" +
                            "<special_service_rcd>" + objSsrCode[i].value + "</special_service_rcd>" +
                            "<service_text>" + document.getElementById("tdDisplayName_" + (i + 1)).innerHTML + "</service_text>" +
                            "<number_of_units>" + objSsrSegment[j].options[objSsrSegment[j].selectedIndex].text + "</number_of_units>" +
                            "<service_on_request_flag>" + objSsrOnRequestFlag[i].value + "</service_on_request_flag>" +
                        "</service>";
            }
            objSsr = null;
        }
        objSsrSegment = null;
    }
    strXml = strXml + "</booking>";
    CloseDialog();
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.FillSpecialService(strXml, true, SuccessFillSpecialService, showError, showTimeOut);

    objFeeId = null;
    objSsrCode = null;
    objSsrOnRequestFlag = null;
}
function SuccessFillSpecialService(result) {
    var objFareSummary = document.getElementById(FindControlName("div", "dvFareSummary"));
    if (objFareSummary != null) {
        objFareSummary.innerHTML = result;
        objFareSummary = null;

        //Initilalise Summary Collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
    ShowProgressBar(false);
}
function CalculatePaxSsr(iRow) {
    var objSelect = document.getElementsByName("seSsrAmount_" + iRow);
    var objSsrTotal = document.getElementById("spnSsrFeeTotal_" + iRow);

    var objSpn = null;
    var dclTotal = 0;

    for (var i = 0; i < objSelect.length; i++) {
        if (objSelect[i].style.display == "block" || objSelect[i].style.display == "") {
            objSpn = document.getElementById("spnSsrFeeAmount_" + iRow + "_" + objSelect[i].id.split("_")[2]);
            dclTotal = dclTotal + (parseInt(objSelect[i].options[objSelect[i].selectedIndex].text) * parseFloat("0" + RemoveCommas(objSpn.innerHTML)));
        }
    }

    if (objSsrTotal)
        objSsrTotal.innerHTML = AddCommas(dclTotal.toFixed(2));

}

function ShowSpecialService(strPaxId) {
    if (strPaxId.length > 0) {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.ShowSpecialService(strPaxId, SuccessShowSpecialService, showError, showTimeOut);
    }

}
function SuccessShowSpecialService(result) {
    ShowProgressBar(false);
    if (result.length > 0) {
        var objMessage = document.getElementById("dvFormHolder");
        var objContainer = document.getElementById("dvProgressBar");

        //Insert passenger form content.
        objMessage.innerHTML = result;

        //Calculate Service.
        CalculateSpecialService();

        //Show Fading.
        objContainer.style.display = "block";
        objMessage.style.display = "block";

        objMessage = null;
        objContainer = null;
    }
}

function SetPassengerValue() {
    //Hidden Control.
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPaxSelectPosition);
    var hdPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iPaxSelectPosition);
    var hdClientProfileId = document.getElementById("hdClientProfileId_" + iPaxSelectPosition);
    var hdVipFlag = document.getElementById("hd_Vip_flag_" + iPaxSelectPosition);
    var hdClientNo = document.getElementById("hdClientNo_" + iPaxSelectPosition);
    var hdTitle = document.getElementById("hdTitle_" + iPaxSelectPosition);
    var hdFirstName = document.getElementById("hdName_" + iPaxSelectPosition);
    var hdLastName = document.getElementById("hdLastname_" + iPaxSelectPosition);
    var hdBirthDate = document.getElementById("hdBirthDate_" + iPaxSelectPosition);
    var hdBirthPlace = document.getElementById("hdBirthPlace_" + iPaxSelectPosition);
    var hdMemberLevel = document.getElementById("hdMemberLevelRcd_" + iPaxSelectPosition);
    var hdNationality = document.getElementById("hdNational_" + iPaxSelectPosition);
    var hdDocumentType = document.getElementById("hdDocumentType_" + iPaxSelectPosition);
    var hdDocumentNo = document.getElementById("hdDocNumber_" + iPaxSelectPosition);
    var hdPlaceOfIssue = document.getElementById("hdIssuePlace_" + iPaxSelectPosition);
    var hdIssueDate = document.getElementById("hdIssueDate_" + iPaxSelectPosition);
    var hdExpiryDate = document.getElementById("hdExpiryDate_" + iPaxSelectPosition);
    var hdWeight = document.getElementById("hdWeight_" + iPaxSelectPosition);
    var hdIssueCountry = document.getElementById("hdIssueCountry_" + iPaxSelectPosition);

    //Input Control
    var txtClientNo = document.getElementById("txtClientNoInput");
    var stTitle = document.getElementById("stTitleInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");
    var txtBirthDate = document.getElementById("txtBirthDateInput");
    var txtBirthPlace = document.getElementById("txtPlaseOfBirth");
    var stNationality = document.getElementById("stNational");
    var stDocumentType = document.getElementById("stDocumentType");
    var txtDocumentNo = document.getElementById("txtDocumentNumber");
    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
    var txtIssueDate = document.getElementById("txtIssueDate");
    var txtExpiryDate = document.getElementById("txtExpiryDate");
    var txtWeight = document.getElementById("txtWeight");
    var stIssueCountry = document.getElementById("stIssueCountry");

    if (hdClientNo != null) {
        hdClientNo.value = GetControlValue(txtClientNo);
    }
    if (hdTitle != null) {
        hdTitle.value = GetSelectedOption(stTitle);
    }
    if (hdFirstName != null) {
        if (GetControlValue(txtFirstName).length > 0) {
            txtFirstName.value = txtFirstName.value.toUpperCase();
        }
        hdFirstName.value = GetControlValue(txtFirstName);
    }
    if (hdLastName != null) {
        if (GetControlValue(txtLastName).length > 0) {
            txtLastName.value = txtLastName.value.toUpperCase();
        }
        hdLastName.value = GetControlValue(txtLastName);
    }
    if (hdBirthDate != null) {
        hdBirthDate.value = GetControlValue(txtBirthDate);
    }
    if (hdBirthPlace != null) {
        hdBirthPlace.value = GetControlValue(txtBirthPlace);
    }

    if (hdNationality != null) {
        hdNationality.value = GetSelectedOption(stNationality);
    }
    if (hdDocumentType != null) {
        hdDocumentType.value = GetSelectedOption(stDocumentType);
    }
    if (hdDocumentNo != null) {
        hdDocumentNo.value = GetControlValue(txtDocumentNo);
    }
    if (hdPlaceOfIssue != null) {
        hdPlaceOfIssue.value = GetControlValue(txtPlaceOfIssue);
    }
    if (hdIssueDate != null) {
        hdIssueDate.value = GetControlValue(txtIssueDate);
    }
    if (hdExpiryDate != null) {
        hdExpiryDate.value = GetControlValue(txtExpiryDate);
    }
    if (hdWeight != null) {
        hdWeight.value = GetControlValue(txtWeight);
    }
    if (hdIssueCountry != null) {
        hdIssueCountry.value = GetSelectedOption(stIssueCountry);
    }


    //Set Name To current Tab Header
    SetInputnameToTab(iPaxSelectPosition);
    
}

function SetInputnameToTab(iPosition) {
    var objPaxTabName = document.getElementById("dvPaxTabName");
    var objCurrentTitle = document.getElementById("hdTitle_" + iPosition);
    var objCurrentFirstName = document.getElementById("hdName_" + iPosition);
    var objCurrentLastName = document.getElementById("hdLastname_" + iPosition);
    if (objPaxTabName != null) {
        if (GetControlValue(objCurrentFirstName).length == 0 & GetControlValue(objCurrentLastName).length == 0) {
            objPaxTabName.innerHTML = objLanguage.default_value_4;
        }
        else {
            objPaxTabName.innerHTML = GetControlValue(objCurrentTitle).split("|")[0] +" "+ GetControlValue(objCurrentFirstName).toUpperCase() + " " + GetControlValue(objCurrentLastName).toUpperCase();
        }
    }
}
function GetPassengerValue(iPosition) {

    //Hidden Control.
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPosition);
    var hdPassengerProfileId = document.getElementById("hdPassengerProfileId_" + iPosition);
    var hdClientProfileId = document.getElementById("hdClientProfileId_" + iPosition);
    var hdVipFlag = document.getElementById("hd_Vip_flag_" + iPosition);
    var hdClientNo = document.getElementById("hdClientNo_" + iPosition);
    var hdTitle = document.getElementById("hdTitle_" + iPosition);
    var hdFirstName = document.getElementById("hdName_" + iPosition);
    var hdLastName = document.getElementById("hdLastname_" + iPosition);
    var hdBirthDate = document.getElementById("hdBirthDate_" + iPosition);
    var hdBirthPlace = document.getElementById("hdBirthPlace_" + iPosition);
    var hdMemberLevel = document.getElementById("hdMemberLevelRcd_" + iPosition);
    var hdNationality = document.getElementById("hdNational_" + iPosition);
    var hdDocumentType = document.getElementById("hdDocumentType_" + iPosition);
    var hdDocumentNo = document.getElementById("hdDocNumber_" + iPosition);
    var hdPlaceOfIssue = document.getElementById("hdIssuePlace_" + iPosition);
    var hdIssueDate = document.getElementById("hdIssueDate_" + iPosition);
    var hdExpiryDate = document.getElementById("hdExpiryDate_" + iPosition);
    var hdPaxType = document.getElementById("hdPaxType_" + iPosition);
    var hdWeight = document.getElementById("hdWeight_" + iPosition);
    var hdIssueCountry = document.getElementById("hdIssueCountry_" + iPosition);

    //Input Control
    var txtClientNo = document.getElementById("txtClientNoInput");
    var txtFirstName = document.getElementById("txtFirstNameInput");
    var txtLastName = document.getElementById("txtLastnameInput");
    var txtBirthDate = document.getElementById("txtBirthDateInput");
    var txtBirthPlace = document.getElementById("txtPlaseOfBirth");
    var txtDocumentNo = document.getElementById("txtDocumentNumber");
    var txtPlaceOfIssue = document.getElementById("txtPlaseOfIssue");
    var txtIssueDate = document.getElementById("txtIssueDate");
    var txtExpiryDate = document.getElementById("txtExpiryDate");
    var txtWeight = document.getElementById("txtWeight");

    var dvPaxType = document.getElementById("dvPaxType");

    if (hdPaxType.value != 'INF') {

        $('#divSSR').show();

        //Show Ssr Input
        var objFeeId = document.getElementsByName("hdFeeId");
        var objSsrSegment;
        for (var i = 0; i < objFeeId.length; i++) {
            objSsrSegment = document.getElementsByName("seSsrAmount_" + (i + 1));
            for (var j = 0; j < objSsrSegment.length; j++) {
                if (parseInt(objSsrSegment[j].id.split("_")[3]) == iPosition)
                { objSsrSegment[j].style.display = "block"; }
                else
                { objSsrSegment[j].style.display = "none"; }

            }
            objSsrSegment = null;
            //CalculatePaxSsr();
            CalculateSpecialService();
        }
        objFeeId = null;

    }
    else {
        $('#divSSR').hide();
    }

    //Clear Tab select
    var iCountTotalPax = document.getElementsByName("hdPassengerId").length;
    for (var i = 1; i <= iCountTotalPax; i++) {
        document.getElementById("ulPaxTab_" + i).className = "disable";
    }

    //Set Active Tab
    document.getElementById("ulPaxTab_" + iPosition).className = "active";


    //Set Selected row number
    iPaxSelectPosition = iPosition;

    if (GetControlValue(hdTitle) == "|") {
        SetComboValue("stTitleInput", "");
     }
    else {
        SetComboValue("stTitleInput", GetControlValue(hdTitle));
    }
    SetComboValue("stNational", GetControlValue(hdNationality));
    SetComboValue("stDocumentType", GetControlValue(hdDocumentType));
    SetComboValue("stIssueCountry", GetControlValue(hdIssueCountry));

    if (txtClientNo != null) {
        if (GetControlValue(hdClientNo).length == 0) {
            txtClientNo.value = objLanguage.default_value_2;
            $("#txtClientNoInput").addClass("watermarkOn");
        }
        else {
            txtClientNo.value = GetControlValue(hdClientNo);
            $("#txtClientNoInput").removeClass("watermarkOn");
        }

    }

    if (txtFirstName != null) {
        if (GetControlValue(hdFirstName).length == 0) {
            txtFirstName.value = objLanguage.default_value_2;
            $("#txtFirstNameInput").addClass("watermarkOn");
        }
        else {
            txtFirstName.value = GetControlValue(hdFirstName);
            $("#txtFirstNameInput").removeClass("watermarkOn");
        }

    }

    if (txtLastName != null) {
        if (GetControlValue(hdLastName).length == 0) {
            txtLastName.value = objLanguage.default_value_2;
            $("#txtLastnameInput").addClass("watermarkOn");
        }
        else {
            txtLastName.value = GetControlValue(hdLastName);
            $("#txtLastnameInput").removeClass("watermarkOn");
        }
    }

    if (txtBirthDate != null) {
        if (GetControlValue(hdBirthDate).length == 0 || GetControlValue(hdBirthDate) == GetDateMask() || GetControlValue(hdBirthDate) == objLanguage.default_value_1) {
            txtBirthDate.value = objLanguage.default_value_1;
            $("#txtBirthDateInput").addClass("watermarkOn");
        }
        else {
            txtBirthDate.value = GetControlValue(hdBirthDate);
            $("#txtBirthDateInput").removeClass("watermarkOn");
        }

    }

    if (txtBirthPlace != null) {
        if (GetControlValue(hdBirthPlace).length == 0) {
            txtBirthPlace.value = objLanguage.default_value_2;
            $("#txtPlaseOfBirth").addClass("watermarkOn");
        }
        else {
            txtBirthPlace.value = GetControlValue(hdBirthPlace);
            $("#txtPlaseOfBirth").removeClass("watermarkOn");
        }
    }

    if (txtDocumentNo != null) {
        if (GetControlValue(hdDocumentNo).length == 0) {
            txtDocumentNo.value = objLanguage.default_value_2;
            $("#txtDocumentNumber").addClass("watermarkOn");
        }
        else {
            txtDocumentNo.value = GetControlValue(hdDocumentNo);
            $("#txtDocumentNumber").removeClass("watermarkOn");
        }
    }

    if (txtPlaceOfIssue != null) {
        if (GetControlValue(hdPlaceOfIssue).length == 0) {
            txtPlaceOfIssue.value = objLanguage.default_value_2;
            $("#txtPlaseOfIssue").addClass("watermarkOn");
        }
        else {
            txtPlaceOfIssue.value = GetControlValue(hdPlaceOfIssue);
            $("#txtPlaseOfIssue").removeClass("watermarkOn");
        }

    }

    if (txtIssueDate != null) {
        if (GetControlValue(hdIssueDate).length == 0 || GetControlValue(hdIssueDate) == GetDateMask() || GetControlValue(hdIssueDate) == objLanguage.default_value_1) {
            txtIssueDate.value = objLanguage.default_value_1;
            $("#txtIssueDate").addClass("watermarkOn");
        }
        else {
            txtIssueDate.value = GetControlValue(hdIssueDate);
            $("#txtIssueDate").removeClass("watermarkOn");
        }
    }

    if (txtExpiryDate != null) {
        if (GetControlValue(hdExpiryDate).length == 0 || GetControlValue(hdExpiryDate) == GetDateMask() || GetControlValue(hdExpiryDate) == objLanguage.default_value_1) {
            txtExpiryDate.value = objLanguage.default_value_1;
            $("#txtExpiryDate").addClass("watermarkOn");
        }
        else {
            txtExpiryDate.value = GetControlValue(hdExpiryDate);
            $("#txtExpiryDate").removeClass("watermarkOn");
        }
    }

    if (txtWeight != null) {
        if (GetControlValue(hdWeight).length == 0) {
            txtWeight.value = objLanguage.default_value_3;
            $("#txtWeight").addClass("watermarkOn");
        }
        else {
            txtWeight.value = GetControlValue(hdWeight);
            $("#txtWeight").removeClass("watermarkOn");
        }
    }

    //Display PAX type.
    var objDvBaggage = document.getElementById("dvBaggage");

    if (GetControlValue(hdPaxType) == "ADULT") {
        dvPaxType.innerHTML = "ADULT";
        if (objDvBaggage != null) {
            objDvBaggage.style.display = "block";
        }
    }
    else if (GetControlValue(hdPaxType) == "CHD") {
        dvPaxType.innerHTML = "CHILD";
        if (objDvBaggage != null) {
            objDvBaggage.style.display = "block";
        }
    }
    else {
        dvPaxType.innerHTML = "INFANT";
        if (objDvBaggage != null) {
            objDvBaggage.style.display = "none";
        }
        
       
    }

    if (GetControlValue(hdClientNo).length > 0 && GetControlValue(hdClientNo) != "0") {
        LockPassengerInput(true);
    }
    else {
        LockPassengerInput(false);
    }

    //Set Name To current Tab Header
    var objPaxTabName = document.getElementById("dvPaxTabName");
    var objCurrentTitle = document.getElementById("hdTitle_" + iPosition);
    var objCurrentFirstName = document.getElementById("hdName_" + iPosition);
    var objCurrentLastName = document.getElementById("hdLastname_" + iPosition);
    if (objPaxTabName != null) {
        if (GetControlValue(objCurrentFirstName).length == 0 & GetControlValue(objCurrentLastName).length == 0) {
            objPaxTabName.innerHTML = objLanguage.default_value_4;
        }
        else {
            objPaxTabName.innerHTML = GetControlValue(objCurrentTitle).split("|")[0] + "  " + GetControlValue(objCurrentFirstName).toUpperCase() + " " + GetControlValue(objCurrentLastName).toUpperCase() ;
        }
    }

}
function ClearErrorMsg() {
    if (document.getElementById("spErrFirstname") != null) {
        document.getElementById("spErrFirstname").innerHTML = "";
    }
    if (document.getElementById("spErrLastname") != null) {
        document.getElementById("spErrLastname").innerHTML = "";
    }
    if (document.getElementById("spErrHome") != null) {
        document.getElementById("spErrHome").innerHTML = "";
    }
    if (document.getElementById("spErrEmail") != null) {
        document.getElementById("spErrEmail").innerHTML = "";
    }
    if (document.getElementById("spErrAddress1") != null) {
        document.getElementById("spErrAddress1").innerHTML = "";
    }
    if (document.getElementById("spErrCity") != null) {
        document.getElementById("spErrCity").innerHTML = "";
    }
    if (document.getElementById("spErrZip") != null) {
        document.getElementById("spErrZip").innerHTML = "";
    }
    if (document.getElementById("spMobileEmail") != null) {
        document.getElementById("spMobileEmail").innerHTML = "";
    }
}
function InitializePaymentWaterMark() {

    //Credit Card
    InitializeWaterMark("txtCardNumber", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtNameOnCard", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtCvv", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtAddress1", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtAddress2", objLanguage.default_value_3, "input");
    InitializeWaterMark("txtCity", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtCounty", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtPostCode", objLanguage.default_value_2, "input");

    if (document.getElementById("optExpiredMonth") != null) {
        document.getElementById("optExpiredMonth").selectedIndex = 0;
    }

    InitializeWaterMark("ctl00_optCardType", objLanguage.default_value_2, "select");
    InitializeWaterMark("optExpiredMonth", objLanguage.default_value_5, "select");
    InitializeWaterMark("optExpiredYear", objLanguage.default_value_6, "select");

    //Voucher
    InitializeWaterMark("txtVoucherNumber", objLanguage.default_value_2, "input");
    //InitializeWaterMark("txtVoucherPassword", objLanguage.default_value_2, "input");
}
function CalculateBaggageFee(bReturn) {
    var objBagFee;
    var hdPassengerId = document.getElementById("hdPassengerId_" + iPaxSelectPosition);
    var hdSegmentId;
    
    if (bReturn == false) {
        objBagFee = document.getElementById("BaggageDepart");
        hdSegmentId = document.getElementById("hdOutwardSegmentId");
    }
    else {
        objBagFee = document.getElementById("BaggageReturn");
        hdSegmentId = document.getElementById("hdReturnSegmentId");
    }

    if (objBagFee != null && hdSegmentId != null) {
        var iFeeUnit = GetSelectedOption(objBagFee);
        if (iFeeUnit.length > 0) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.CalculateBaggageFee(parseInt(iFeeUnit), GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), bReturn, true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.ClearBaggageFee(GetControlValue(hdSegmentId), GetControlValue(hdPassengerId), true, SuccessCalculateBaggageFee, showError, showTimeOut);
        }
    }
}

function SuccessCalculateBaggageFee(result) {
    if (result.length > 0) {

        DisplayQuoteSummary(result, "", "");

        //Initialize Summary collapse.
        InitializeFareSummaryCollapse("Outward");
        InitializeFareSummaryCollapse("Return");
    }
    ShowProgressBar(false);
}
function CookiesSameAsContact() {
    var objCookies = getCookie("coContact");
    

    if (objCookies != null) {

        var objTitle = document.getElementById(FindControlName("select", "stContactTitle"));
        var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
        var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
        var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
        var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
        var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
        var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
        var objEmailConfirm = document.getElementById(FindControlName("input", "txtEmailConfirm"));
        var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
        var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
        var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
        var objCity = document.getElementById(FindControlName("input", "txtContactCity"));
        var objCountry = document.getElementById(FindControlName("select", "stContactCountry"));

        var objTaxId = document.getElementById(FindControlName("input", "txtTIN"));
        var objInvoiceReceiver = document.getElementById(FindControlName("input", "txtInvoiceReceiver"));
        var txtEUVat = document.getElementById(FindControlName("input", "txtEUVat"));
        var txtCIN = document.getElementById(FindControlName("input", "txtCIN"));
        var objOptionalEmail = document.getElementById(FindControlName("input", "txtContactEmail2"));
        var objMobileEmail = document.getElementById(FindControlName("input", "txtMobileEmail"));
        var objState = document.getElementById(FindControlName("input", "txtContactState"));

        var strResult = $.base64.decode(objCookies);
        if (strResult.length > 0) {

            var strContactInfo = strResult.split("{}");
            if (strContactInfo.length == 21) {
                //Get passenger contact information.

                if (GetSelectedOption(objTitle) == strContactInfo[0] &
                    GetControlValue(objFirstName) == strContactInfo[1] &
                    GetControlValue(objLastName) == strContactInfo[2] &
                    GetControlValue(objMobile) == strContactInfo[3] &
                    GetControlValue(objHome) == strContactInfo[4] &
                    GetControlValue(objBusiness) == strContactInfo[5] &
                    GetControlValue(objEmail) == strContactInfo[6] &
                    GetControlValue(objAddress1) == strContactInfo[7] &
                    GetControlValue(objAddress2) == strContactInfo[8] &
                    GetControlValue(objZip) == strContactInfo[9] &
                    GetControlValue(objCity) == strContactInfo[10] &
                    GetSelectedOption(objCountry) == strContactInfo[11] &
                    (objTaxId == null || GetControlValue(objTaxId) == strContactInfo[13]) &
                    (objInvoiceReceiver == null || GetControlValue(objInvoiceReceiver) == strContactInfo[14]) &
                    (txtEUVat == null || GetControlValue(txtEUVat) == strContactInfo[15]) &
                    (txtCIN == null || GetControlValue(txtCIN) == strContactInfo[16]) &
                    (objOptionalEmail == null || GetControlValue(objOptionalEmail) == strContactInfo[17]) &
                    (objMobileEmail == null || GetControlValue(objMobileEmail) == strContactInfo[18]) &
                    (objState == null || GetControlValue(objState) == strContactInfo[19]) &
                    GetControlValue(objEmailConfirm) == strContactInfo[20]) {

                    return true;
                }
                else {
                    return false;
                 }
            }
            else {
                return false;
            }
        }
    }
    else {
        return false;
    }

    objCookies = null;
    objChk = null;
}


