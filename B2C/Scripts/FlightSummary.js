function BackToAvailability() {
    ShowProgressBar(true);

    //SuccessSearchFlight() is in the Availability.js
    tikAeroB2C.WebService.B2cService.AvailabilityGetSession(SuccessSearchFlight, showError, showTimeOut);
}

function GetPassengerDetail() {
    ShowProgressBar(true);
    tikAeroB2C.WebService.B2cService.GetPassengerDetail(SuccessGetPassengerDetail, showError, showTimeOut);
    
}

function SuccessGetPassengerDetail(result) {

    if (result.length > 0) {
        var obj = document.getElementById("dvContainer");
        if (result == "{002}") {
            //System Error. Please try again.
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else if (result != "{001}") {
            obj.innerHTML = result;
            SetPassengerDetail();

            RemoveWellNetFeeDisplay();
            //Display quote information
            GetSessionQuoteSummary();
            
            ShowProgressBar(false);
            obj = null;
            ToolTipColor();
        }
        else {
            LoadSecure(false);
        }
     }
    else {
        ShowProgressBar(false);
    }
    
}
function InitializePassengerMaskEdit() {
    InitializeDateMaskEdit("txtIssueDate");
    InitializeDateMaskEdit("txtExpiryDate");
    InitializeDateMaskEdit("txtBirthDateInput");
}

function LoadContactTitle() {
    var objSelect = document.getElementById("stTitle_1");
    var objContactTitle = document.getElementById("stContactTitle");


    //Populate contact detail title.
    for (var jCount = 0; jCount < objSelect.length; jCount++) {
        objContactTitle.options[objContactTitle.options.length] = new Option(objSelect.options[jCount].text, objSelect.options[jCount].value);
    }
    objSelect = null;
    objContactTitle = null;
}

function LoadContactCountry() {
    var objSelect = document.getElementById("stNational_1");
    var objContactCountry = document.getElementById("stContactCountry");

    //Populate contact national.
    for (var jCount = 0; jCount < objSelect.length; jCount++) {
        objContactCountry.options[objContactCountry.options.length] = new Option(objSelect.options[jCount].text, objSelect.options[jCount].value);
    }
    objSelect = null;
    objContactCountry = null;
}

function GetContactInformationFromCookies() {
    var objCookies = getCookie("coContact");
    var objChk = document.getElementById("chkRemember");

    if (objCookies != null) {

        var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
        var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
        var objMobile = document.getElementById(FindControlName("input", "txtContactMobile"));
        var objHome = document.getElementById(FindControlName("input", "txtContactHome"));
        var objBusiness = document.getElementById(FindControlName("input", "txtContactBusiness"));
        var objEmail = document.getElementById(FindControlName("input", "txtContactEmail"));
        var objEmailConfirm = document.getElementById(FindControlName("input", "txtEmailConfirm"));
        var objAddress1 = document.getElementById(FindControlName("input", "txtContactAddress1"));
        var objAddress2 = document.getElementById(FindControlName("input", "txtContactAddress2"));
        var objZip = document.getElementById(FindControlName("input", "txtContactZip"));
        var objCity = document.getElementById(FindControlName("input", "txtContactCity"));

        var objTaxId = document.getElementById(FindControlName("input", "txtTIN"));
        var objInvoiceReceiver = document.getElementById(FindControlName("input", "txtInvoiceReceiver"));
        var txtEUVat = document.getElementById(FindControlName("input", "txtEUVat"));
        var txtCIN = document.getElementById(FindControlName("input", "txtCIN"));
        var objOptionalEmail = document.getElementById(FindControlName("input", "txtContactEmail2"));
        var objMobileEmail = document.getElementById(FindControlName("input", "txtMobileEmail"));
        var objState = document.getElementById(FindControlName("input", "txtContactState"));

        var strResult = $.base64.decode(objCookies);
        if (strResult.length > 0 && objFirstName.value.length == 0) {

            var strContactInfo = strResult.split("{}");
            if (strContactInfo.length == 21) {
                objChk.checked = true;

                //Get passenger contact information.
                SetComboValue(FindControlName("select", "stContactTitle"), strContactInfo[0]);
                objFirstName.value = strContactInfo[1];
                objLastName.value = strContactInfo[2];
                objMobile.value = strContactInfo[3];
                objHome.value = strContactInfo[4];
                objBusiness.value = strContactInfo[5];
                objEmail.value = strContactInfo[6];
                objEmailConfirm.value = strContactInfo[6];
                objAddress1.value = strContactInfo[7];
                objAddress2.value = strContactInfo[8];
                objZip.value = strContactInfo[9];
                objCity.value = strContactInfo[10];
                SetComboValue(FindControlName("select", "stContactCountry"), strContactInfo[11]);

                if (objTaxId != null) {
                    objTaxId.value = strContactInfo[13];
                }
                if (objInvoiceReceiver != null) {
                    objInvoiceReceiver.value = strContactInfo[14];
                }
                if (txtEUVat != null) {
                    txtEUVat.value = strContactInfo[15];
                }
                if (txtCIN != null) {
                    txtCIN.value = strContactInfo[16];
                }
                if (objOptionalEmail != null) {
                    objOptionalEmail.value = strContactInfo[17];
                }
                if (objMobileEmail != null) {
                    objMobileEmail.value = strContactInfo[18];
                }
                objState.value = strContactInfo[19];
            }
        }
    }
    else {
        objChk.checked = false;
    }

    objCookies = null;
    objChk = null;
}
function SetPassengerDetail() {
    GetContactInformationFromCookies();
    scroll(0, 0);

    var objChk = document.getElementById("chkRemember");
    var objFirstName = document.getElementById(FindControlName("input", "txtContactFirstname"));
    var objLastName = document.getElementById(FindControlName("input", "txtContactLastname"));
    var objPaxFirstName;
    var objPaxLastName;

    var iPaxcount = document.getElementsByName("hdPassengerId").length;
    var objIAmPassenger = document.getElementById("chkCopy");

    objPaxFirstName = document.getElementById("hdName_1");
    objPaxLastName = document.getElementById("hdLastname_1");

    if (GetControlValue(objPaxFirstName).length > 0 && GetControlValue(objPaxLastName).length > 0 &&
        GetControlValue(objFirstName).toUpperCase() != GetControlValue(objPaxFirstName).toUpperCase() && GetControlValue(objLastName).toUpperCase() != GetControlValue(objPaxLastName).toUpperCase()) {

        objIAmPassenger.checked = false;

        //        for (var i = 1; i <= iPaxcount; i++) {
        //            objPaxFirstName = document.getElementById("hdName_" + i);
        //            objPaxLastName = document.getElementById("hdLastname_" + i);

        //            if (GetControlValue(objFirstName).toUpperCase() == GetControlValue(objPaxFirstName).toUpperCase() &&
        //                GetControlValue(objLastName).toUpperCase() == GetControlValue(objPaxLastName).toUpperCase()) {

        //                var objIAmPassenger = document.getElementById("chkCopy");
        //                objIAmPassenger.checked = true;
        //                objIAmPassenger = null;

        //                break;
        //            }
        //        }
    }
    else {
        objIAmPassenger.checked = true;
    }

    //Lock all control that are client.
    var objProfileId = document.getElementsByName("hdClientProfileId");
    for (var iCount = 1; iCount <= objProfileId.length; iCount++) {
        if (document.getElementById("txtClientNoInput").value.length > 0 &&
            document.getElementById("txtFirstNameInput").value.length > 0 &&
            document.getElementById("txtLastnameInput").value.length > 0) {
            //Lock Control.
            LockPassengerInput(true);
        }
    }

    //Set Current Tab position to 1
    GetPassengerValue(1);

    //Initial mask edit
    InitializePassengerMaskEdit();

    //Initialize Watermark.
    InitializeContactWaterMark();

    //JQuery for contact detail
    $("#" + FindControlName("select", "stContactTitle")).change(function () {

        var objChkFirstPax = document.getElementById("chkCopy");
        if (objChkFirstPax.checked == true) {
            var objPaxTitle = document.getElementById("stTitleInput");
            var objContactTitle = document.getElementById(this.id);

            GetPassengerValue(1);
            objPaxTitle.selectedIndex = objContactTitle.selectedIndex;
            SetPassengerValue();
            SetInputnameToTab(iPaxSelectPosition);
        }
    });

    $("#" + FindControlName("input", "txtContactLastname")).keyup(function () {

        var objChkFirstPax = document.getElementById("chkCopy");
        
        if (objChkFirstPax.checked == true) {
            var objPaxLastname = document.getElementById("txtLastnameInput");
            var objContactLastname = document.getElementById(this.id);

            GetPassengerValue(1);
            objPaxLastname.value = objContactLastname.value.toUpperCase();
            SetPassengerValue();
            SetInputnameToTab(iPaxSelectPosition);
        }
    });

    $("#" + FindControlName("input", "txtContactFirstname")).keyup(function () {

        var objChkFirstPax = document.getElementById("chkCopy");
        
        if (objChkFirstPax.checked == true) {
            var objPaxFirstname = document.getElementById("txtFirstNameInput");
            var objContactFirstname = document.getElementById(this.id);

            GetPassengerValue(1);
            objPaxFirstname.value = objContactFirstname.value.toUpperCase();
            SetPassengerValue();
            SetInputnameToTab(iPaxSelectPosition);
        }
    });
    //JQuery for passenger.
    $("#" + FindControlName("select", "stTitleInput")).change(function () {

        SelectUsedFirstPassenger();

    });

    $("#" + FindControlName("input", "txtLastnameInput")).keyup(function () {

        SelectUsedFirstPassenger();
    });

    $("#" + FindControlName("input", "txtFirstNameInput")).keyup(function () {

        SelectUsedFirstPassenger();
    });

    //Check Remember if cookes information is the same.
    if (CookiesSameAsContact() == true) {
        var objLastname = document.getElementById("txtLastnameInput");
        var objFirstname = document.getElementById("txtFirstNameInput");

        if (GetControlValue(objLastname).length == 0 & GetControlValue(objFirstname).length == 0) {
            objIAmPassenger.checked = true;
            CopyContactToPassenger();
        }
        objChk.checked = true;
    }
    else {
        objChk.checked = false;
     }

    objProfileId = null;

    objFirstName = null;
    objLastName = null;
    objPaxFirstName = null;
    objPaxLastName = null;
}
function InitializeContactWaterMark() {

    //Contact information
    InitializeWaterMark("ctl00_stContactTitle", objLanguage.default_value_7, "select");
    InitializeWaterMark("ctl00_txtContactFirstname", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactLastname", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactMobile", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactHome", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactBusiness", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactEmail", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtEmailConfirm", objLanguage.default_value_2, "input");
    InitializeWaterMark("ctl00_txtContactAddress1", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactAddress2", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactZip", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactCity", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtTIN", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtInvoiceReceiver", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtEUVat", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtCIN", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactEmail2", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtMobileEmail", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_txtContactState", objLanguage.default_value_3, "input");
    InitializeWaterMark("ctl00_stContactCountry", objLanguage.default_value_2, "select");

    //Passenger Information
    InitializeWaterMark("stTitleInput", objLanguage.default_value_7, "select");
    InitializeWaterMark("txtClientNoInput", objLanguage.default_value_3, "input");
    InitializeWaterMark("txtFirstNameInput", objLanguage.default_value_2, "input");
    InitializeWaterMark("txtLastnameInput", objLanguage.default_value_2, "input");

    if (document.getElementById("txtBirthDateInput") != null) {
        InitializeWaterMark("txtBirthDateInput", objLanguage.default_value_1, "input");
    }
    if (document.getElementById("txtPlaseOfBirth") != null) {
        InitializeWaterMark("txtPlaseOfBirth", objLanguage.default_value_2, "input");
    }
    if (document.getElementById("txtDocumentNumber") != null) {
        InitializeWaterMark("txtDocumentNumber", objLanguage.default_value_2, "input");
    }
    if (document.getElementById("txtPlaseOfIssue") != null) {
        InitializeWaterMark("txtPlaseOfIssue", objLanguage.default_value_2, "input");
    }
    if (document.getElementById("txtIssueDate") != null) {
        InitializeWaterMark("txtIssueDate", objLanguage.default_value_1, "input");
    }
    if (document.getElementById("txtExpiryDate") != null) {
        InitializeWaterMark("txtExpiryDate", objLanguage.default_value_1, "input");
    }

    if (document.getElementById("txtWeight") != null) {
        InitializeWaterMark("txtWeight", objLanguage.default_value_3, "input");
    }

    if (document.getElementById("stNational") != null) {
        InitializeWaterMark("stNational", objLanguage.default_value_2, "select");
    }

    if (document.getElementById("stDocumentType") != null) {
        InitializeWaterMark("stDocumentType", objLanguage.default_value_2, "select");
    }

    if (document.getElementById("stIssueCountry") != null) {
        InitializeWaterMark("stIssueCountry", objLanguage.default_value_2, "select");
    }
}

function SelectUsedFirstPassenger() {
    if (iPaxSelectPosition == 1) {
        var objChkFirstPax = document.getElementById("chkCopy");

        var objPaxTitle = document.getElementById("stTitleInput");
        var objContactTitle = document.getElementById(FindControlName("select", "stContactTitle"));

        var objPaxFirstname = document.getElementById("txtFirstNameInput");
        var objContactFirstname = document.getElementById(FindControlName("input", "txtContactFirstname"));

        var objPaxLastname = document.getElementById("txtLastnameInput");
        var objContactLastname = document.getElementById(FindControlName("input", "txtContactLastname"));

        if (objPaxTitle.selectedIndex != objContactTitle.selectedIndex ||
            GetControlValue(objPaxFirstname).toUpperCase() != GetControlValue(objContactFirstname).toUpperCase() ||
            GetControlValue(objPaxLastname).toUpperCase() != GetControlValue(objContactLastname).toUpperCase()) {

            objChkFirstPax.checked = false;
        }
        else {
            objChkFirstPax.checked = true;
        }
    }
}