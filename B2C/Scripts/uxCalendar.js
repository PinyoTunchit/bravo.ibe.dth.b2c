/*===============================================*/
// config for calendar type
// 1. Normal type
// 2. Show flight type
var calType = 2;
/*Call Web Service Zone*/
/*===== Parameter for call web service ==========*/
var current_cell = null;

function clearHeader() {
    var TabDate = document.getElementById("TabDate");
    if (TabDate != null) {
        for (var i = TabDate.rows.length - 1; i >= 0; i--) {
            TabDate.deleteRow(TabDate.rows[i]);
        }
    }
}


function CreateAjaxControl() {
    var xmlHttp = null;
    try {
        //alert(xmlHttp)
        //Firefox, Opera 8.0+, Safari  
        xmlHttp = new XMLHttpRequest();
        return xmlHttp;
    }
    catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            return xmlHttp;
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                return xmlHttp;
            }
            catch (e) {
                alert("Your browser does not support AJAX!");
                return null;
            }
        }
    }
}

var ControlName;
var CurrentRoute = 0;


function initialcalendar() {


    //Create Calendar 1
    CalendarCallWs("?df=1&i=1&type=datefrom", "divFrom", 1);
    //Create Calendar 2
    CalendarCallWs("?df=0&i=1&type=dateto&nm=1", "divTo", 0);
}

function lessthanfrom(m, y) {
    //yyyymmdd
    var hdd_ctr_none = "";
    var hdd_ctr_datefrom = document.getElementById("hdd_ctr_datefrom");
    var valdatefrom = hdd_ctr_datefrom.value;

    var mm = m.toString();
    if (m.toString().length <= 1) { mm = "0" + m.toString(); }
    //alert(parseInt(y.toString()+mm+"01"))
    //alert(valdatefrom);
    //alert(parseInt(valdatefrom)<= parseInt(y.toString()+mm+"01"))
    if (parseInt(valdatefrom) >= parseInt(y.toString() + mm + "01")) {
        return true;
    }
    else {
        return false;
    }
}

//yyyymmdd
function showschedule(ctrname, celltext, cellname) {

    //function showschedule('datefrom','20080423','ctr_datefrom_20080423')
    var hdd_defCell = document.getElementById("hdd_defCell");
    if (hdd_defCell != null) {
        var cc_Cell = document.getElementById(hdd_defCell.value);
        if (cc_Cell != null) {
            //alert(cc_Cell);
            if (hdd_defCell.value != cellname) {
                //alert(hdd_defCell.value)

                //New Cell Select

                //Old Cell
                cc_Cell.className = "calendar_DayStyle";
                cc_Cell.style.backgroundColor = "";
                //
                //Call Ws
                //alert(dvTable.innerHTML);
                //alert(Origin+","+Distination);
                //setInterval(animation,50);

            }
        }
        var selcell = document.getElementById(cellname);
        current_cell = cellname;
        hdd_defCell.value = cellname;
        selcell.className = "calendar_dateselect";
        //alert(cc_Cell);
        DateFrom = celltext;

        currentdate = celltext;

        DateTo = "";
        animation();
        var lang = "en-us"; //document.getElementById("hddlang");		 
        CallWs("?act=Table&ORG=" + Origin + "&DEST=" + Distination + "&DateFrom=" + DateFrom + "&DateTo=" + DateTo + "&lang=" + lang.value, "dvTable");

    }
    showBelow(celltext);

}
function showBelow(ccdate) {
    var dvCurrenDate = document.getElementById("dvCurrenDate");
    dvCurrenDate.innerHTML = "This below schedule for " + mydateformat(ccdate) + "";

}
function animation() {
    var dvTable = document.getElementById("dvTable");
    dvTable.innerHTML = "<img src='loading.gif' border=0 alt='' />";
}
function CalendarCallWs(parameter, controls, isinitial) {
    var xmlHttp = CreateAjaxControl();
    var ControlName = controls;
    var WS_URL = window.location.href.replace(window.location.href.split('/')[window.location.href.split('/').length - 1], 'Calendar/ddlCalendar.aspx');
    var lang = "en-us"; //document.getElementById("hddlang");
    if (lang.value != "") {
        if (parameter.indexOf('&lang') < 0) {
            parameter = "&lang=" + lang.value;
        }
    }

    WS_URL = WS_URL + parameter;
    //alert(WS_URL);
    var objControls = document.getElementById(ControlName);
    //if(objControls.innerHTML!=""){objControls.innerHTML="<img src='loading.gif' border=0 />";}

    if (xmlHttp != null) {
        xmlHttp.onreadystatechange = function (controls) {
            if (xmlHttp.readyState == 4) {
                objControls.innerHTML = "";
                if (xmlHttp.responseText == "") {
                    objControls.innerHTML = "<div class='schedule_not_found'>Schedule date not found </div>";
                }
                else {


                    var content = xmlHttp.responseText;

                    //content=content.replace('<!--IMG_Ctrl-->',"<img src='Images/bcp.gif' border='0' style='cursor:hand;' onclick=javascript:showCalendar(this);>")
                    //alert(content.indexOf('<!--IMG_Ctrl-->'));
                    //alert(content);
                    objControls.innerHTML = content;
                    if (isinitial == 1) {
                        //showinitial_schedule();
                    }
                }
            }
        };

        xmlHttp.open("GET", WS_URL, true);
        xmlHttp.send(null);
    }
}
function showinitial_schedule() {
    var hdd_defCell = document.getElementById("hdd_defCell");
    var defDay = hdd_defCell.value;
    var cDefDay = defDay.split("_");
    var DateFrom = cDefDay[2];
    currentdate = cDefDay[2];
    var DateTo = '';

    CallWs("?act=Table&ORG=" + Origin + "&DEST=" + Distination + "&DateFrom=" + DateFrom + "&DateTo=" + DateTo, "dvTable");
}


function SelectCurrentDay() {
    var d = new Date();
    var tmpDay = d.getDate();
    var currrentDay = padStrleft(tmpDay, '0', 2);

    if (document.getElementById('ddlMY_1').selectedIndex == 0 && $("#ddlDate_1").val() <= currrentDay) {
        $("#ddlDate_1").val(currrentDay);
    }
}

function hiddenCalendar(id, ctrl) {
    closeCalendar();
    var Cal = document.getElementById('Cal' + id);
    var ddlD = document.getElementById('ddlDate_2');
   // var oldDay2 = ddlD.options[ddlD.selectedIndex].value;
    if (Cal != null) Cal.innerHTML = "";

    if (id == "1") {
        //Change List Day//
        if (ctrl.id.indexOf("ddlDate_") < 0) {
            CreatedListDate('1');
            CreatedListDateByRef('2', '1');
        }
        SelectCurrentDay();
        DDLCopyvalue();
    }
    if (id == "2") {

        var ddlDate_2 = document.getElementById('ddlDate_2');
        var ddlMY_2 = document.getElementById('ddlMY_2');
        var ddlDate_1 = document.getElementById('ddlDate_1');
        var ddlMY_1 = document.getElementById('ddlMY_1');

        var D2 = ddlMY_2.options[ddlMY_2.selectedIndex].value + ddlDate_2.options[ddlDate_2.selectedIndex].value;
        var D1 = ddlMY_1.options[ddlMY_1.selectedIndex].value + ddlDate_1.options[ddlDate_1.selectedIndex].value;

        //alert(D1+'==>'+D2)
        if (parseInt(D1) > parseInt(D2)) {

            //alert("MMM");
            CreatedListDateByRef('2', '1');
            DDLCopyvalue();

        }
        else {
            CreatedListDate('2');
        }
    }

    /* var ddlM= document.getElementById('ddlMY_2');        
    var isCDate = isCheckDate(ddlM.options[ddlM.selectedIndex].value,oldDay2);
    if(isCDate)
    {
    var nIndex=FindByval(ddlD,oldDay2)     
    ddlD.options[nIndex].selected=true;
    }
    */
    /*Check Compare Flight Date And Return Date*/


}

function isCheckDate(ym, od) {
    var isMyDate = CheckIsDate(Number(ym.substring(0, 4)), Number(ym.substring(4, 6)), Number(od));
    return isMyDate;
}

function CreatedListDateByRef(id, refid) {

    var ddlM = document.getElementById('ddlMY_' + refid);
    var ddlD = document.getElementById('ddlDate_' + id);

    var ym = ddlM.options[ddlM.selectedIndex].value;
    var isSelectDate = false;
    if (ym != "") {
        var isMyDate = false;
        clearDDL(ddlD);
        for (var i = 1; i <= 31; i++) {
            isMyDate = CheckIsDate(Number(ym.substring(0, 4)), Number(ym.substring(4, 6)), i);
            if (isMyDate) {
                var sdate = padStrleft(i, '0', 2);

                createDDL(ddlD, sdate, sdate, i - 1);
            }
        }
    }
}

function CreatedListDate(id) {
    var ddlM = document.getElementById('ddlMY_' + id);
    var ddlD = document.getElementById('ddlDate_' + id);
    var ym = ddlM.options[ddlM.selectedIndex].value;

    var mySelectDate = ddlD.options[ddlD.selectedIndex].value;

    var isSelectDate = false;
    if (ym != "") {
        clearDDL(ddlD);
        var isMyDate = false;
        for (var i = 1; i <= 31; i++) {
            isMyDate = CheckIsDate(Number(ym.substring(0, 4)), Number(ym.substring(4, 6)), i);

            if (isMyDate) {
                var sdate = padStrleft(i, '0', 2);
                if (mySelectDate == sdate) {
                    isSelectDate = true;
                }
                else {
                    isSelectDate = false;
                }
                createDDL(ddlD, sdate, sdate, i - 1);
            }
        }

        var index = FindByval(ddlD, mySelectDate);
        ddlD.options[index].selected = true;
    }
}

function padStrleft(str, c, len) {
    if (str.toString().length >= parseInt(len)) return str.toString();

    var nl = (len - str.toString().length);
    var ntxt = '';
    for (var i = 0; i < nl; i++) { ntxt = ntxt + c; }
    return (ntxt + str.toString());
}


function clearDDL(ddl) {
    var len = ddl.options.length;
    for (var i = len - 1; i >= 0; i--) {
        ddl.remove(i);
    }
}

function createDDL(obj, text, value, index) {
    var opt = document.createElement('OPTION');
    opt.value = value;
    opt.text = text;

    obj.options.add(opt, index);

}



function CheckIsDate(y, m, d) {
    var nm = m - 1;
    try {
        var d = new Date(y, nm, d);

        if (parseInt(nm) != d.getMonth())
        { return false; }
        else
        { return true; }
    }
    catch (e) {
        alert('error');
        return false;
    }
}



var currObjx;
function showCalendar(obj, flightType) {


    var Cal = document.getElementById('Cal' + obj.id);
    var ddlDate = document.getElementById("ddlDate_" + obj.id);
    var ddlMonthYear = document.getElementById("ddlMY_" + obj.id);
    var dd = ddlDate.options[ddlDate.selectedIndex].value;
    var my = ddlMonthYear.options[ddlMonthYear.selectedIndex].value;

    var mm = (parseInt(my.substring(4, 6), 10));
    var mxmy = ddlMonthYear.options[ddlMonthYear.options.length - 1].value;
    var mnmy = ddlMonthYear.options[0].value;

    //  "?id="+obj.id
    //  +"&df=1&i=1&type=dateto&d="
    //  +dd
    //  +"&m="
    //  +mm.toString()
    //  +"&y="
    //  +my.substring(0,4)
    //  +"&mxmy="
    //  +mxmy
    //  +"&mnmy="+mnmy
    //alert(TikAeroWebB2E.WebService.BaseService);
    //alert(TikAeroWebB2E.WebService.BaseService);
    var uxOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var uxDest = document.getElementById(FindControlName("select", "optDestination"));
    var from = "";
    var to = "";

    if (Cal.innerHTML == "") {
        if (uxOrigin != null) {
            if (uxOrigin.selectedIndex == -1) {
                from = uxOrigin.options[0].value.split("|")[0];
            }
            else {
                from = uxOrigin.options[uxOrigin.selectedIndex].value.split("|")[0];
            }

            if (uxDest.selectedIndex == -1) {
                to = uxDest.options[0].value.split("|")[2];
            } else {
                to = uxDest.options[uxDest.selectedIndex].value.split("|")[2];
            }

            var lang = "en-us"; //document.getElementById("hddlang");
            currObjx = Cal.id;
            //CalendarCallWs("?id="+obj.id+"&df=1&i=1&type=dateto&d="+dd+"&m="+mm.toString()+"&y="+my.substring(0,4)+"&mxmy="+mxmy+"&mnmy="+mnmy,Cal.id,0);      
            //(string ID, string calendartype, string isonlyonemonth,string date,string month,string year,string mxMY, string mnMY)
            tikAeroB2C.WebService.WebUIRender.GetCalendar(obj.id, flightType, "1", dd, mm.toString(), my.substring(0, 4), mxmy, mnmy, lang, from, to, calType, GetCalendarresult);
            Cal.style.left = parseInt(findPosX(document.getElementById('ddlDate_' + obj.id)));
            var Cal1ID = "";
            switch (obj.id) {
                case "1":
                    Cal1ID = "2";
                    break;
                case "2":
                    Cal1ID = "1";
                    break;
                case "3":
                    Cal1ID = "4";
                    break;
                case "4":
                    Cal1ID = "3";
                    break;
            }
            var objAcal = $get('Cal' + Cal1ID);
            if (objAcal != null) {
                objAcal.innerHTML = '';
                hide_menu("fdCal" + Cal1ID);
            }
        }
    }
    else {
        Cal.innerHTML = "";
        hide_menu("fdCal" + obj.id);
    }

}
var tmpcal;
function GetCalendarresult(result) {
    /*if(currObjx!="")
    {
    var objControls =  document.getElementById(currObjx);     
        
    if(objControls.id.indexOf("calRen")<0)
    {
    if(objControls.id.indexOf("CalenderBase")<0)
    {
    objControls.innerHTML="<div id='d"+objControls.id+"'  style='display:none;z-index:9999;'  class='scrollpopup'>"+result+"</div>";//"<iframe  src='' class='frmcls'>"+content+"</iframe>";
    var dvobj =  document.getElementById("d"+objControls.id);
    display_menu(dvobj,dvobj.id);
    }
    }  
    //objControls.innerHTML=result;
    }*/
    if (currObjx != "") {
        var objControls = document.getElementById(currObjx);

        if (objControls.id.indexOf("calRen") < 0) {
            if (objControls.id.indexOf("CalenderBase") < 0) {
                tmpcal = result;
                RenderTagCalendar(objLanguage.NoFlight);
            }
        }
    }

}

// remove text from wem when don't want to show color on calendar
//for No Flight
function RenderTagCalendar(result) {
    var objControls = document.getElementById(currObjx);
    var strhtml = "<div id='d" + objControls.id + "'  style='display:none;z-index:9999;'  class='scrollpopup'>";
    strhtml += "<div class='close'><a href='javascript:closeCalendar()'>[x]</a></div>";
    strhtml += tmpcal;
    if (result != undefined) {
        strhtml += "<div class='CalendarRemark'><table class='Detail' border='0' cellSpacing='0' cellPadding='0' width='100%'>";
        strhtml += "<tr><td class='no_flight'></td><td >" + result + "</td></tr>";
    }
    tmpcal = strhtml;
    //for Operate Flight
    var op = 'Operate Flight';
    RenderTagCalendarOP(objLanguage.OperateFlight);
}
function RenderTagCalendarOP(result) {
    var objControls = document.getElementById(currObjx);
    var strhtml;
    strhtml = tmpcal;
    if (result != undefined) {
        strhtml += "<tr><td class='operate_flight'></td><td >" + result + "</td></tr>";
        strhtml += "<tr><td></td><td></td></tr></table>";
        strhtml += "</div></div>";
    }
    objControls.innerHTML = strhtml; //"<iframe  src='' class='frmcls'>"+content+"</iframe>";
    var dvobj = document.getElementById("d" + objControls.id);
    display_menu(dvobj, dvobj.id);
}

function closeCalendar() {
    var Cal = document.getElementById('Cal1');
    if (Cal.innerHTML == undefined || Cal.innerHTML == "") {
        Cal = document.getElementById('Cal2');
        Cal.innerHTML = "";
        hide_menu("fdCal2");
    } else {
        Cal.innerHTML = "";
        hide_menu("fdCal1");
    }

    var Cal2 = document.getElementById('Cal3');
    if (Cal2 != null) {
        Cal2.innerHTML = "";
        hide_menu("fdCal3");
    }

    var Cal3 = document.getElementById('Cal4');
    if (Cal3 != null) {
        Cal2.innerHTML = "";
        hide_menu("fdCal4");
    }

}
function findPos(obj) {
    var curleft = curtop = 0;
    if (obj.offsetParent) {
        curleft = obj.offsetLeft;
        curtop = obj.offsetTop;
        while (obj = obj.offsetParent) {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        }
    }
    return [curleft, curtop + 20];
}


function display_menu(parent, named) {



    var menu_element = document.getElementById(named);
    menu_element.style.display = "block";
    var placement = findPos(parent);
    //alert(menu_element)

    //        alert( menu_element.className);
    //        menu_element.style.left = placement[0] + "px";
    //        menu_element.style.top = placement[1] + "px";
    //        menu_element.style.height = menu_element.clientHeight + "px";
    //        menu_element.style.width = menu_element.clientWidth +"px";
    //        menu_element.style.left = menu_element.style.left;
    //        menu_element.style.top = menu_element.style.top;
    //        menu_element.style.display="block";


    //alert(menu_element.clientHeight);
    //menu_element.style.left = placement[0] + "px";
    //menu_element.style.top = placement[1] + "px";
    //alert(menu_element.style.top);
    //alert(named)

    document.getElementById("f" + named).style.height = "110px"; //menu_element.clientHeight +300+ "px";
    document.getElementById("f" + named).style.width = menu_element.clientWidth + "px";
    document.getElementById("f" + named).style.left = placement[0]; //menu_element.style.left;
    document.getElementById("f" + named).style.top = placement[1] - 20 + "px"; //menu_element.style.top;
    document.getElementById("f" + named).style.display = "block";


}
function hide_menu(named) {
    //    var menu_element = document.getElementById(named);
    //    menu_element.style.display = "none";

    document.getElementById(named).style.height = '0';
    document.getElementById(named).style.width = '0';
    document.getElementById(named).style.display = "none";
}

function monthActHome(m, y, cc, flightType) {

    //var dd = (parseInt(d)-1) ;
    /*var id = cc.replace("ctr_","");

    var   ddlDate = document.getElementById("ddlDate_" + id);
    var   ddlMonthYear =document.getElementById("ddlMY_" + id);

    var mxmy=ddlMonthYear.options[ddlMonthYear.options.length-1].value;
    var mnmy=ddlMonthYear.options[0].value;

    //CalendarCallWs("?id="+id+"&df=1&i=1&type=dateto&m="+m.toString()+"&y="+y+"&mxmy="+mxmy+"&mnmy="+mnmy,"Cal"+id,0);
 
    var lang="en-us";//document.getElementById("hddlang");
    currObjx="Cal"+id;
    //CalendarCallWs("?id="+obj.id+"&df=1&i=1&type=dateto&d="+dd+"&m="+mm.toString()+"&y="+my.substring(0,4)+"&mxmy="+mxmy+"&mnmy="+mnmy,Cal.id,0);      
    //(string ID, string calendartype, string isonlyonemonth,string date,string month,string year,string mxMY, string mnMY)
    tikAeroB2C.WebService.WebUIRender.GetCalendar(id,"dateto","1","",m.toString(),y,mxmy,mnmy,lang,GetCalendarresult)  
    //Cal.style.left = parseInt(findPosX(document.getElementById('ddlDate_'+obj.id)));*/

    //var dd = (parseInt(d)-1) ;
    var id = cc.replace("ctr_", "");

    var ddlDate = document.getElementById("ddlDate_" + id);
    var ddlMonthYear = document.getElementById("ddlMY_" + id);

    var mxmy = ddlMonthYear.options[ddlMonthYear.options.length - 1].value;
    var mnmy = ddlMonthYear.options[0].value;

    var uxOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var uxDest = document.getElementById(FindControlName("select", "optDestination"));
    var from = "";
    var to = "";

    if (uxOrigin.selectedIndex == -1) {
        from = uxOrigin.options[0].value.split("|")[0];
    } else {
        from = uxOrigin.options[uxOrigin.selectedIndex].value.split("|")[0];
    }

    if (uxDest.selectedIndex == -1) {
        to = uxDest.options[0].value.split("|")[2];
    } else {
        to = uxDest.options[uxDest.selectedIndex].value.split("|")[2];
    }

    var lang = "en-us"; //document.getElementById("hddlang");
    currObjx = "Cal" + id;

    tikAeroB2C.WebService.WebUIRender.GetCalendar(id, flightType, "1", "", m.toString(), y, mxmy, mnmy, lang, from, to, calType, GetCalendarresult);
    //Cal.style.left = parseInt(findPosX(document.getElementById('ddlDate_'+obj.id)));


}



function showschedule2(id, date, cellObj) {
    var ddlDate = document.getElementById("ddlDate_" + id);
    var ddlMonthYear = document.getElementById("ddlMY_" + id);

    if (ddlDate == null) return;

    ddlMonthYear.selectedIndex = FindByval(ddlMonthYear, date.substring(0, 6));
    hiddenCalendar(id, ddlMonthYear);
    ddlDate.selectedIndex = FindByval(ddlDate, date.substring(6, 8));

    var cal = document.getElementById('Cal' + id);
    var isCopyDef = false;
    /**Check Return date not over Flight date***/
    if (id == "2") {
        var ddlDate = document.getElementById("ddlDate_1");
        var ddlMonthYear = document.getElementById("ddlMY_1");
        var dd = ddlDate.options[ddlDate.selectedIndex].value;
        var mm = ddlMonthYear.options[ddlMonthYear.selectedIndex].value;
        var val = mm + dd;

        isCopyDef = (parseInt(date) < parseInt(val));

    }



    if (id == "1") {
        DDLCopyvalue();
        setSelectedDateSession(date, id);
    }
    if ((id == "2") && (isCopyDef == true)) {
        DDLCopyvalue();

    } else if ((id == "2") && (isCopyDef == false)) {
        setSelectedDateSession(date, id);
    }



    hide_menu("fdCal" + id);
    cal.innerHTML = "";

}
//--------------------------------------------------------------------
function setSelectedDateSession(dym, id) {
    tikAeroB2C.WebService.WebUIRender.setSelectedDate(dym, id);
    return true;
}



function DDLCopyvalue() {
    var ddlDate = document.getElementById("ddlDate_1");
    var ddlMonthYear = document.getElementById("ddlMY_1");
    var ddlDate2 = document.getElementById("ddlDate_2");
    var ddlMonthYear2 = document.getElementById("ddlMY_2");
    ddlDate2.selectedIndex = ddlDate.selectedIndex;
    ddlMonthYear2.selectedIndex = ddlMonthYear.selectedIndex;


}


function FindByval(ddl, val) {
    var index = 0;
    var found = false;

    for (var i = 0; i < ddl.options.length && (!found); i++) {
        if (ddl.options[i].value == val) {
            index = i;
            found = true;
        }
        // alert("[DLLOPT]:"+ddl.options[i].value)
    }
    return index;
}

function findPosX(obj) {
    var curleft = 0;
    if (obj.offsetParent)
        while (1) {
            curleft += obj.offsetLeft;
            if (!obj.offsetParent)
                break;
            obj = obj.offsetParent;
        }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
}