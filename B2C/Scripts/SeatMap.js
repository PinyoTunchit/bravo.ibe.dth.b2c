function selectSeat(row, col, seatRow, seatCol, paxCount, className, blockChild, bEmergency, strFeeRcd, strJSon) {
    

    var objSeat = document.getElementById("tbSeat_" + row + "_" + col);
    var objPaxSeat;
    var objdvPaxType;
    var objSeatRow;
    var objSeatCol;
    var objFeeRcd;
    var iNumberOfInfant = 0;
    var objJson = eval("(" + strJSon + ")");

    if (objSeat != null) {
        if (objSeat.className == className) {

            //Read Jason string to object...
            if (objJson != null) {
                iNumberOfInfant = objJson.NumberOfInfant;
            }

            for (iCount = 1; iCount <= paxCount; iCount++) {
                objPaxSeat = document.getElementById("dvSeatNumber" + iCount);
                objSeatRow = document.getElementById("hdSeatRow" + iCount);
                objSeatCol = document.getElementById("hdSeatCol" + iCount);
                objdvPaxType = document.getElementById("dvPaxType" + iCount);
                objFeeRcd = document.getElementById("hdFeeRcd" + iCount);

                if (objPaxSeat != null) {
                    if (trim(objPaxSeat.innerHTML).length == 0 && trim(objdvPaxType.innerHTML) != "INF") {

                        if (trim(objdvPaxType.innerHTML) == "CHD" && blockChild > 0) {
                            //This Seat is blocked for Children.
                            ShowMessageBox(objLanguage.Alert_Message_133, 0, '');
                            break;
                        }
                        else {
                            if (haveInfant(trim(objdvPaxType.innerHTML), paxCount) == true && blockChild > 0) {
                                //This Seat is blocked for Infant.
                                ShowMessageBox(objLanguage.Alert_Message_134, 0, '');
                                break;
                            }
                            else {

                                //Check number of infant limit..


                                objPaxSeat.innerHTML = seatRow + seatCol;
                                objSeatRow.value = seatRow;
                                objSeatCol.value = seatCol;

                                var tmpClass = className.replace(strFeeRcd, "").replace("EXTRAS", "");
                                var cssSeat = "selectedseat";
                                if (tmpClass.indexOf('R') != -1) {
                                    cssSeat = "selectedseatR";
                                }

                                objSeat.className = cssSeat;
                                objFeeRcd.value = strFeeRcd;

                                if (objdvPaxType.innerHTML == "ADULT") {
                                    //Search infant To update seat infant with adult
                                    for (jCount = 1; jCount <= paxCount; jCount++) {
                                        objPaxSeat = document.getElementById("dvSeatNumber" + jCount);
                                        objdvPaxType = document.getElementById("dvPaxType" + jCount);
                                        objSeatRow = document.getElementById("hdSeatRow" + jCount);
                                        objSeatCol = document.getElementById("hdSeatCol" + jCount);
                                        objFeeRcd = document.getElementById("hdFeeRcd" + jCount);

                                        if (trim(objdvPaxType.innerHTML) == "INF" && trim(objPaxSeat.innerHTML).length == 0) {
                                            objPaxSeat.innerHTML = seatRow + seatCol;
                                            objSeatRow.value = seatRow;
                                            objSeatCol.value = seatCol;
                                            objFeeRcd.value = strFeeRcd;
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        else {
            for (iCount = 1; iCount <= paxCount; iCount++) {
                objPaxSeat = document.getElementById("dvSeatNumber" + iCount);
                objSeatRow = document.getElementById("hdSeatRow" + iCount);
                objSeatCol = document.getElementById("hdSeatCol" + iCount);
                objFeeRcd = document.getElementById("hdFeeRcd" + iCount);

                if (objPaxSeat != null) {
                    if (objPaxSeat.innerHTML == (seatRow + seatCol)) {
                        objPaxSeat.innerHTML = "";
                        objSeatRow.value = "";
                        objSeatCol.value = "";
                        objSeat.className = className;
                        objFeeRcd.value = "";
                    }
                }
            }
        }
    }
    objSeat = null;
    objPaxSeat = null;
    objdvPaxType = null;
    objSeatRow = null;
    objSeatCol = null;
    
}
function FillSeat() {
    var objSeatPaxId = document.getElementsByName("hdSeatPaxId");
    var objSeatSegmentId = document.getElementsByName("hdSeatSegmentId");
    var objSeatRow = document.getElementsByName("hdSeatRow");
    var objSeatCol = document.getElementsByName("hdSeatCol");
    var objSeatFeeRcd = document.getElementsByName("hdFeeRcd");

    var paxXml = "";

    for (var iCount = 0; iCount < objSeatPaxId.length; iCount++) {
        paxXml = paxXml +
                 "<mapping>" +
                     "<passenger_id>" + objSeatPaxId[iCount].value + "</passenger_id>" +
                     "<segment_id>" + objSeatSegmentId[iCount].value + "</segment_id>" +
                     "<seat_row>" + objSeatRow[iCount].value + "</seat_row>" +
                     "<seat_col>" + objSeatCol[iCount].value + "</seat_col>" +
                     "<fee_rcd>" + objSeatFeeRcd[iCount].value + "</fee_rcd>" +
                 "</mapping>";
    }

    objSeatPaxId = null;
    objSeatSegmentId = null;
    objSeatRow = null;
    objSeatCol = null;
    objSeatFeeRcd = null;
    return paxXml;
}
function SaveSeatMap() {
    var paxXml = FillSeat();
    if (paxXml.length > 0) {
        CloseSeatMap();
        ShowProgressBar(true);
        //Call filled seat service with rendering the control
        tikAeroB2C.WebService.B2cService.FillSeat("<booking>" + paxXml + "</booking>", true, SuccessSaveSeatMap, showError, showTimeOut);
    }
}

function SuccessSaveSeatMap(result) {
    //Reload Passenger Detail Page
    SuccessGetPassengerDetail(result);
    //Close Seat Map form
    //CloseSeatMap();
}
function haveInfant(paxType, paxCount) {
    var b = false;
    if (paxType == "ADULT") {
        var objdvPaxType;
        var objPaxSeat;
        for (iCount = 1; iCount <= paxCount; iCount++) {
            objdvPaxType = document.getElementById("dvPaxType" + iCount);
            objPaxSeat = document.getElementById("dvSeatNumber" + iCount);
            if (trim(objdvPaxType.innerHTML) == "INF" && objPaxSeat.innerHTML.length == 0) {
                b = true;
                break;
            }
        }
    }
    objdvPaxType = null;
    objPaxSeat = null;
    return b;
}

function CancelSelectSeat() {
    CloseSeatMap();
    ShowProgressBar(true);
    //Call filled seat service with rendering the control
    tikAeroB2C.WebService.B2cService.CancelSelectSeat(true, SuccessCancelSelectSeat, showError, showTimeOut);
}

function SuccessCancelSelectSeat(result) {
    var obj = document.getElementById("dvContainer");

    if (result == "{600}") {
        ShowMessageBox("Cancel select seat failed", 0, '');
    }
    else if (result == "{002}") {
        //Cancel failed because of session timeout.
        ShowMessageBox(objLanguage.Alert_Message_128, 0, 'loadHome');
    }
    else {
        if (result.length > 0) {
            obj.innerHTML = result;
            SetPassengerDetail();
        }
    }
    ShowProgressBar(false);
    obj = null;
    ToolTipColor();
}

function CloseSeatMap() {
    var objHolder = document.getElementById("dvFormHolder");
    //Empty Form Holder
    objHolder.innerHTML = "";

    //Show Fading.
    objHolder.style.display = "none";
}
function showSeatMappingFlightSelect(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass) {
    var objTable = document.getElementById("tbSeatItinerary");

    for (var iCount = 0; iCount < objTable.rows.length; iCount++) {
        if (objTable.rows[iCount].id.toString().length != 0) {
            if (objTable.rows[iCount].id.toString() != strTrId) {
                //Change class name of tr element
                objTable.rows[iCount].className = "NotSelected";
            }
            else {
                //Change class name of tr element
                objTable.rows[iCount].className = "Selected";
            }
        }
    }
    objTable = null;

    //Call web service.
    SaveSeatMapSelectFlight(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass);
}

function SaveSeatMapSelectFlight(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass) {
    var paxXml = FillSeat();
    if (paxXml.length > 0) {
        ShowProgressBar(true);
        //Call FillSeat service without rendering the control.
        tikAeroB2C.WebService.B2cService.FillSeat("<booking>" + paxXml + "</booking>", false, SuccessSaveSeatMapSelectFlight(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass), showError, showTimeOut);
    }
}

function SuccessSaveSeatMapSelectFlight(strTrId, strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass) {
    tikAeroB2C.WebService.B2cService.GetMap(strSeatFlightId, strOriginRcd, strDestinationRcd, strBoardingClass, strBookingClass, SuccessLoadMap, showError, showTimeOut);
    tikAeroB2C.WebService.B2cService.GetPassengerSeatMap(strSeatFlightId, SuccessLoadMapPassenger, showError, showTimeOut);
}

function SuccessLoadMap(result) {
    if (result == "{002}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        var objMap = document.getElementById(FindControlName("span", "dvSeatMap"));
        objMap.innerHTML = result;
        objMap = null;
    }
    
}
function SuccessLoadMapPassenger(result) {

    if (result == "{002}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
    }
    else {
        var objMapPassenger = document.getElementById(FindControlName("div", "dvSeatPassenger"));
        objMapPassenger.innerHTML = result;
        objMapPassenger = null;

        ShowProgressBar(false);
    }
    
}
