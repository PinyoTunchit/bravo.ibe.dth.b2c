var tmpOutFlight_id = "";
var tmpOutFare_id = "";
var tmpRetFlight_id = "";
var tmpRetFare_id = "";
function getDestination() {
    var strCtrl = FindControlName("select", "optDestination");
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var objDestination = document.getElementById(strCtrl);
    var strSelectedOrigin = "";

    objDestination.className = "";
    //Add destination from selected origin
    strSelectedOrigin = objOrigin.options[objOrigin.selectedIndex].value;
    objDestination = ClearOptions(objDestination);  //Clear previous value

    //Add watermark option
    var optDefault = document.createElement("option");
    optDefault.value = "";
    optDefault.className = "watermarkOn";
    if (navigator.appName.indexOf('Microsoft') == 0)
    { optDefault.innerText = objLanguage.default_value_7; }
    else
    { optDefault.text = objLanguage.default_value_7; }
    objDestination.appendChild(optDefault);

    //Filled in route information.
    for (var iCount = 0; iCount < arrDestination.length; iCount++) {
        if (arrDestination[iCount].split("|")[2] == strSelectedOrigin.split("|")[0]) {
            //Allowed only the destination that have the same
            //origin as selected origin.
            var opt = document.createElement("option");
            opt.value = arrDestination[iCount].split("|")[2] + "|" +
                        objOrigin.options[objOrigin.selectedIndex].text + "|" +
                        arrDestination[iCount].split("|")[0] + "|" +
                        arrDestination[iCount].split("|")[1] + "|" +
            //arrDestination[iCount].split("|")[3];
                        arrDestination[iCount].split("|")[3] + "|" +
                        arrDestination[iCount].split("|")[4];


            //Add display text to option
            if (navigator.appName.indexOf('Microsoft') == 0) {
                opt.innerText = arrDestination[iCount].split("|")[1];
            }
            else
            { opt.text = arrDestination[iCount].split("|")[1]; }

            objDestination.appendChild(opt);
            opt = null;
        }
    }
    $("#" + strCtrl).unbind();
    InitializeWaterMark(strCtrl, objLanguage.default_value_7, "select");

    objOrigin = null;
    objDestination = null;
}

var strOriLowfare = "";
var strDestLowfare = "";
function CallLowfare() {
    SearchAvailability('', '', true);
}

function DisplayMonthView() {
    if (document.getElementById("rdSearchTypeDate") == null) {
        var objMonth_Outward = document.getElementById("Outward_dvShowmonthview");
        var objMonth_Return = document.getElementById("Return_dvShowmonthview");

        if (objMonth_Outward != null) {
            objMonth_Outward.style.display = "none";
        }
        if (objMonth_Return != null) {
            objMonth_Return.style.display = "none";
        }
    }
}

function SearchAvailability(origin, destination, Lowfare) {
    var objOneWay = document.getElementById("optOneWay");
    var objAdult = document.getElementById("optAdult");
    var objChild = document.getElementById("optChild");
    var objInfant = document.getElementById("optInfant");
    var objBoardingClass = document.getElementById("optBoardingClass");
    var objPromoCode = document.getElementById("txtPromoCode");
    var objYP = document.getElementById("optYP");
    var objSearchTypeDate = document.getElementById("rdSearchTypeDate");

    var isOneWay = objOneWay.checked.toString();
    var iAdult = parseInt(objAdult.options[objAdult.selectedIndex].value);
    var iChild = parseInt(objChild.options[objChild.selectedIndex].value);
    var iInfant = parseInt(objInfant.options[objInfant.selectedIndex].value);
    var strBoardingClass = objBoardingClass.options[objBoardingClass.selectedIndex].value;
    strBoardingClass = ""; // Select all class
    var dtDateFrom = "";
    var dtDateTo = "";
    var strPromoCode = "";
    var iOther = 0;
    var otherType = "";
    var currencyCode = "";
    var strSearchType = "FARE";

    if (existEachQueryString("currency")) {
        currencyCode = queryStringValue("currency");
    }

    // End Check Ip address for Default Currency 
    if (objYP != null) {
        iOther = parseInt(objYP.options[objYP.selectedIndex].value);
        otherType = "YP";
    }

    if (objPromoCode != null) {
        strPromoCode = objPromoCode.value;
    }

    //Call Availability web service
    if (iInfant > iAdult) {
        ShowMessageBox(objLanguage.Alert_Message_2, 0, '');
    }
    else if ((iAdult + iChild + iInfant) > 9) {
        ShowMessageBox(objLanguage.Alert_Message_53.replace("{NO}", 9), 0, '');
    }
    else if ((iAdult + iChild + iInfant + iOther) == 0) {
        ShowMessageBox(objLanguage.Alert_Message_6, 0, '');
    }
    else {
        ShowProgressBar(true);

        var strOri = "";
        var strDest = "";
        if (origin.length == 0 & destination.length == 0) {
            var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
            var strOrigin = GetSelectedOption(objOrigin);

            var objDestination = document.getElementById(FindControlName("select", "optDestination"));
            var strDestination = GetSelectedOption(objDestination);

            strOri = strDestination.split("|")[0];
            strDest = strDestination.split("|")[2];
            strOriLowfare = strDestination.split("|")[0];
            strDestLowfare = strDestination.split("|")[2];

            if (existEachQueryString("currency")) {
                currencyCode = queryStringValue("currency");
            }
            else {
                currencyCode = strDestination.split("|")[5];
            }

            objOrigin = null;
            objDestination = null;
        }
        else {
            strOri = origin;
            strDest = destination;
        }

        if (strOri.length == 0 || strDest.length == 0) {
            ShowMessageBox(objLanguage.Alert_Message_54, 0, '');
        }
        else {
            if (objSearchTypeDate != null && objSearchTypeDate.checked == false || Lowfare == true) {

                dtDateFrom = getMonthfrom();
                dtDateTo = getMonthto();
                var todayDate = new Date();

                tikAeroB2C.WebService.B2cService.GetLowFareFinderMonth(strOri,
                                                                       strDest,
                                                                       dtDateFrom,
                                                                       dtDateTo,
                                                                       isOneWay,
                                                                       iAdult,
                                                                       iChild,
                                                                       iInfant,
                                                                       strBoardingClass,
                                                                       currencyCode,
                                                                       strPromoCode,
                                                                       strSearchType,
                                                                       iOther,
                                                                       otherType,
                                                                       g_strIpAddress,
                                                                       ChangeToDateString(todayDate),
                                                                       SuccessGetLowFareFinder,
                                                                       showError,
                                                                       getdatefrom() + "|" + getdateto());
            }
            else {

                dtDateFrom = getdatefrom();
                dtDateTo = getdateto();
                tikAeroB2C.WebService.B2cService.GetAvailability(strOri,
                                                                 strDest,
                                                                 dtDateFrom,
                                                                 dtDateTo,
                                                                 isOneWay,
                                                                 "0",
                                                                 iAdult,
                                                                 iChild,
                                                                 iInfant,
                                                                 strBoardingClass,
                                                                 currencyCode,
                                                                 strPromoCode,
                                                                 strSearchType,
                                                                 iOther,
                                                                 otherType,
                                                                 g_strIpAddress,
                                                                 SuccessSearchFlight,
                                                                 showError,
                                                                 showTimeOut);
            }
        }
    }

    objOrigin = null;
    objDestination = null;
    objOneWay = null;
    objAdult = null;
    objChild = null;
    objInfant = null;
    objBookingClass = null;
    objPromoCode = null;

}

function SuccessSearchFlight(result) {

    if (result.length > 0) {

        if (result == "{002}") {
            loadHome();
        }
        else if (result == "{003}") {
            //Invalid Required Parameter.
            ShowMessageBox(objLanguage.Alert_Message_55, 0, '');
        }
        else if (result == "{001}") {
            LoadSecure(false);
        }
        else {
            if (result == "{004}") {
                //System Error. Please try again.
                ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
            }
            else {
                //Display Html
                var obj = document.getElementById("dvContainer");
                obj.innerHTML = result;
                obj = null;

                //Select at lowest fare.
                SelectLowestFare();

                //Clear Fare Summary
                DisplayQuoteSummary("", "", "");
                //Hide Select buttom when client is on hold
                var objClientOnHold = document.getElementById("dvClientOnHold");
                if (objClientOnHold.value == 1) {
                    document.getElementById("dvAvaiNext").style.display = "none";
                }
                else {
                    document.getElementById("dvAvaiNext").style.display = "block";
                }
                objClientOnHold = null;

                //Hide Previous 7 day if the current and selecte date are the same.
                var todayDate = new Date();
                var strCurrentDate = ChangeToDateString(todayDate);

                var objOutWardSelectDate = document.getElementById("Outward_hdSelectedDate");
                var objReturnSelectDate = document.getElementById("Return_hdSelectedDate");

                if (objOutWardSelectDate != null) {
                    if (strCurrentDate == GetControlValue(objOutWardSelectDate)) {
                        document.getElementById("Outward_DatePrevious").style.display = "none";
                    }
                    else {
                        document.getElementById("Outward_DatePrevious").style.display = "block";
                    }
                }

                if (objReturnSelectDate != null) {
                    if (strCurrentDate == GetControlValue(objReturnSelectDate)) {
                        document.getElementById("Return_DatePrevious").style.display = "none";
                    }
                    else {
                        document.getElementById("Return_DatePrevious").style.display = "block";
                    }
                }

                // Show Flight tool tip.
                ToolTipColor();

                //Move scroll to top page.
                scroll(0, 0);

                //Rename search button.
                var btmSearch = document.getElementById("spnSearchCap");
                if (btmSearch != null) {
                    btmSearch.innerHTML = objLanguage.Alert_Message_138;  //"Refine your search";
                }

                //Set Calendar Date.
                var objFlightDateOutward = document.getElementById("Outward_hdSelectedDate");
                var objFlightDateReturn = document.getElementById("Return_hdSelectedDate");

                if (objFlightDateOutward != null) {
                    setFromDate(objFlightDateOutward.value);
                }
                if (objFlightDateReturn != null) {
                    setToDate(objFlightDateReturn.value);
                }

                ShowSearchPannel(true);
                ShowProgressBar(false);
                DisplayMonthView();

            }
        }
    }
}

function setdatefrom(strdt) {
    var ddlDate_1 = document.getElementById('ddlDate_1');
    for (var i = 0; i < ddlDate_1.options.length; i++) {
        if (ddlDate_1.options[i].text === strdt) {
            ddlDate_1.selectedIndex = i;
            break;
        }
    }
}

function setdateto(strdt) {
    var ddlDate_2 = document.getElementById('ddlDate_2');
    for (var i = 0; i < ddlDate_2.options.length; i++) {
        if (ddlDate_2.options[i].text === strdt) {
            ddlDate_2.selectedIndex = i;
            break;
        }
    }
}
function getdatefrom() {
    var ddlMY_1 = document.getElementById('ddlMY_1');
    var ddlDate_1 = document.getElementById('ddlDate_1');
    return (ddlMY_1.options[ddlMY_1.selectedIndex].value + ddlDate_1.options[ddlDate_1.selectedIndex].value);
}
function getdateto() {
    var ddlMY_2 = document.getElementById('ddlMY_2');
    var ddlDate_2 = document.getElementById('ddlDate_2');
    return (ddlMY_2.options[ddlMY_2.selectedIndex].value + ddlDate_2.options[ddlDate_2.selectedIndex].value);
}
function getMonthfrom() {
    var ddlMY_1 = document.getElementById('ddlMY_1');
    return ddlMY_1.options[ddlMY_1.selectedIndex].value;
}
function getMonthto() {
    var ddlMY_2 = document.getElementById('ddlMY_2');
    return ddlMY_2.options[ddlMY_2.selectedIndex].value;
}
function ShowHideCalendar() {
    var objSearch = document.getElementById("optReturn");
    var objCar = document.getElementById("calRen2");
    var objReturnLabel = document.getElementById("lblReturnDate");

    if (objSearch.checked == true) {
        objCar.style.display = "block";
        objReturnLabel.style.display = "block";
    }
    else {
        objCar.style.display = "none";
        objReturnLabel.style.display = "none";
    }

    objReturnLabel = null;
    objCar = null;
    objSearch = null;
}
function SearchSingleFlight(flightDate, strFlightType) {

    var objFlightDate;

    if (strFlightType == "Outward") {
        objFlightDate = document.getElementById("Return_hdSelectedDate");
    }
    else {
        objFlightDate = document.getElementById("Outward_hdSelectedDate");
    }
    if (objFlightDate != null &&
        ((strFlightType == "Return" && parseInt(flightDate) < parseInt(objFlightDate.value)))) {

    }
    else {
        var todayDate = new Date();
        var cDate = ChangeToDateString(todayDate);
        var sDate = flightDate;

        if (sDate <= cDate) {
            flightDate = ChangeToDateString(todayDate);
        }

        if (strFlightType == "Outward") {

            setFromDate(flightDate);
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.SearchSingleFlight(flightDate, true, SuccessSearchFlight, showError, showTimeOut);
        }
        else {

            setToDate(flightDate);
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.SearchSingleFlight(flightDate, false, SuccessSearchFlight, showError, showTimeOut);
        }
        document.getElementById("dvAvaiNext").style.display = "none";
    }
}

function SelectFlight() {

    var objOut = document.getElementsByName("Outward");
    var objReturn = document.getElementsByName("Return");
    var bShowConfirm = true;
    var arrId;

    if (objOut != null) {
        for (var i = 0; i < objOut.length; i++) {
            if (objOut[i].checked == true) {
                arrId = objOut[i].id.split("_");
                if (arrId.length == 2) {
                    if (arrId[1] != 1) {
                        bShowConfirm = false;
                    }
                }
                else {
                    bShowConfirm = false;
                }
                break;
            }
        }
    }

    if (objReturn != null) {
        for (var i = 0; i < objReturn.length; i++) {
            if (objReturn[i].checked == true) {
                arrId = objReturn[i].id.split("_");
                if (arrId.length == 2) {
                    if (arrId[1] != 1) {
                        bShowConfirm = false;
                    }
                }
                else {
                    bShowConfirm = false;
                }
                break;
            }
        }
    }

    //Show fare up sell confirmation. Comment until customer ok to activate.
    //    if(bShowConfirm == true){
    //        ShowConfirm(objLanguage.Alert_Message_152, "", ConfirmSelectFlight);
    //    }
    //    else {
    ConfirmSelectFlight();
    //    }
}

function SuccessSelectFlight(result) {
    var arrResult = result.split("{!}");

    if (arrResult.length == 1) {
        if (result == "{000}") {

            LoadSecure(true);
        }
        else if (result == "{200}") {
            //Please select return flight.
            ShowMessageBox("Please select return flight.", 0, '');
        }
        else if (result == "{201}") {

            //Booking object is null.(Session time out).
            ShowMessageBox("Booking object is null.(Session time out).", 0, 'loadHome');
        }
    }
    else {
        if (arrResult[0] == "0") {
            //Success.
            var obj = document.getElementById("dvContainer");
            obj.innerHTML = arrResult[1];

            SetPassengerDetail();
            InitializeFareSummaryCollapse("Outward");
            InitializeFareSummaryCollapse("Return");

            obj = null;
            ShowSearchPannel(false);
            ShowProgressBar(false);

            ToolTipColor();

            //Display quote information
            GetSessionQuoteSummary();
            scroll(0, 0);

            //
            RemoveQueryString();
        }
        else if (arrResult[0] == "101") {
            //US Message
            ShowMessageBox(objLanguage.Alert_Message_60.replace("{NO}", arrResult[1]));
        }
        else if (arrResult[0] == "200") {
            //Infant over limit
            ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
        }
    }

}

// Start Search Flight Availability from Query string
function GenerateFromQueryString() {
    if (window.location.search.substring(1) != "") {
        // Check My Booking First
        if (checkMyBookingString()) {
            LoadCob(false, '');
        }
        else if (CheckClientLogon()) {
            LoadDialog();
        }
        else if (CheckRegisterLogon()) {
            LoadRegistration();
        }
        else if (existEachQueryString("sn")) {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.LoadManageBooking(queryStringValue("sn"), SuccessLoadCob, showError, showTimeOut);
        }
        else if (checkValidQueryStringForSearch() == true) {
            searchFlightAvailFromQueryString();
        }
        else {
            RefreshSetting();
        }
    }
    else {
        RefreshSetting();
    }
}

function searchFlightAvailFromQueryString() {
    //ori=LGW&des=GCI&dep=2010-01-23&ret=2010-01-26&adt=1&chd=1&inf=1&pro=ww&bcls=Y&ft=L&currency=GBP

    if (!checkValidQueryStringForSearch()) {
    }
    else {   // If parameters are OK.
        // For check all set default value
        var bPass = true;
        var bLff = false;

        // Set Origin DropDownList            
        var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
        if (objOrigin != null) {
            var origin = queryStringValue("ori");
            var canSet = setSelectedOriginIndex(objOrigin, origin);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Destination DropDownLis before set value from query string.
        getDestination();

        // Set Destination DropDownList            
        var objDestination = document.getElementById(FindControlName("select", "optDestination"));
        if (objDestination != null) {
            var destination = queryStringValue("des");
            var canSet = setSelectedDestinationIndex(objDestination, destination);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Radio One-Way or Return, and Calendar DropDownList
        if (existEachQueryString("dep") && existEachQueryString("ret")) {   // If there are both Departure Date and Return Date, then set return
            // Set Radio
            var objReturn = document.getElementById("optReturn");
            if (objReturn != null) {
                objReturn.checked = true;
            }

            // Set Departure Date
            var ddlMY_1 = document.getElementById('ddlMY_1');
            var depDate = queryStringValue("dep");
            if (ddlMY_1 != null) {
                var canSet = setSelectedIndex(ddlMY_1, generateYearMonth(depDate));
                if (!canSet) {
                    bPass = false;
                }
                else {
                    hiddenCalendar(1, ddlMY_1);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_1 = document.getElementById('ddlDate_1');
            if (ddlDate_1 != null) {
                var canSet = setSelectedIndex(ddlDate_1, generateDate(depDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }

            // Set Return Date
            var ddlMY_2 = document.getElementById('ddlMY_2');
            var retDate = queryStringValue("ret");
            if (ddlMY_2 != null) {
                var canSet = setSelectedIndex(ddlMY_2, generateYearMonth(retDate));
                if (!canSet) {
                    bPass = false;
                }
                else {
                    hiddenCalendar(2, ddlMY_2);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_2 = document.getElementById('ddlDate_2');
            if (ddlDate_2 != null) {
                var canSet = setSelectedIndex(ddlDate_2, generateDate(retDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }
        else if (existEachQueryString("dep") && !existEachQueryString("ret")) { // If there is only Departure Date, then set to One Way
            // Set Radio
            var objOneWay = document.getElementById("optOneWay");
            if (objOneWay != null) {
                objOneWay.checked = true;
            }

            // Set Departure Date
            var ddlMY_1 = document.getElementById('ddlMY_1');
            var depDate = queryStringValue("dep");
            if (ddlMY_1 != null) {
                var canSet = setSelectedIndex(ddlMY_1, generateYearMonth(depDate));
                if (!canSet) {
                    bPass = false;
                }
                else {
                    hiddenCalendar(1, ddlMY_1);
                }
            }
            else {
                bPass = false;
            }
            var ddlDate_1 = document.getElementById('ddlDate_1');
            if (ddlDate_1 != null) {
                var canSet = setSelectedIndex(ddlDate_1, generateDate(depDate));
                if (!canSet) {
                    bPass = false;
                }
            }
            else {
                bPass = false;
            }
        }
        ShowHideCalendar();

        // Set Adult Pax to Adult DropDownList
        var objAdult = document.getElementById("optAdult");
        if (objAdult != null) {
            var adult = queryStringValue("adt");
            var canSet = setSelectedIndex(objAdult, adult);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Child Pax to Child DropDownList
        var objChild = document.getElementById("optChild");
        if (objChild != null) {
            var child = queryStringValue("chd");
            var canSet = setSelectedIndex(objChild, child);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Infant Pax to Infant DropDownList
        var objInfant = document.getElementById("optInfant");
        if (objInfant != null) {
            var infant = queryStringValue("inf");
            var canSet = setSelectedIndex(objInfant, infant);
            if (!canSet) {
                bPass = false;
            }
        }
        else {
            bPass = false;
        }

        // Set Boarding Class DropDownList
        var objBoardingClass = document.getElementById("optBoardingClass");
        if (objBoardingClass != null && existEachQueryString("bcls")) {
            var bclass = queryStringValue("bcls");
            var canSet = setSelectedIndex(objBoardingClass, bclass);
            if (!canSet) {
                bPass = false;
            }
        }

        // Set Promotion Code
        var objPromo = document.getElementById("txtPromoCode");
        if (objPromo != null && existEachQueryString("pro")) {
            var strPromo = queryStringValue("pro");
            objPromo.value = strPromo;
        }

        // Set Search Type (Availibility or lowfare finder)
        if (existEachQueryString("bLFF")) {
            bLff = queryStringValue("bLFF");
        }

        // Set Currency Class DropDownList
        var obtCurrency = document.getElementById(FindControlName("select", "optCurrency"));
        if (obtCurrency != null) {
            if (obtCurrency != null && existEachQueryString("currency")) {
                var strCurrency = queryStringValue("currency").toUpperCase();
                var canSet = setSelectedIndex(obtCurrency, strCurrency);
                if (!canSet) {
                    bPass = false;
                }
            }
        }

        //Write agency code cookies.
        if (existEachQueryString("ao")) {

            //if (getCookie("logAgency") == null || getCookie("logAgency") != "") {
                var strAgencyCode = queryStringValue("ao");
                var dtExpired = new Date();
                dtExpired.setDate(dtExpired.getDate() + iAgencyCodeExpDays);
                setCookie("logAgency", $.base64.encode(strAgencyCode), dtExpired, "", "", false);
            //}
        }

        if (bPass == true) {
            if (bLff == "true") {
                var objSearchTypeCal = document.getElementById("rdSearchTypeCal");
                if (objSearchTypeCal != null) {
                    objSearchTypeCal.checked = true;
                }
            }
            SearchAvailability(origin, destination);
        }
    }
}

function setSelectedOriginIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        var optValue = obj.options[i].value.split("|");
        if (optValue[0] == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}

function setSelectedDestinationIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        var optValue = obj.options[i].value.split("|");
        if (optValue[2] == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}

function setSelectedIndex(obj, v) {
    var canSet = false;
    for (var i = 0; i < obj.options.length; i++) {
        if (obj.options[i].value == v) {
            obj.options[i].selected = true;
            canSet = true;
            break;
        }
    }
    return canSet;
}

/*
function setSelectedIndex(s, v) {
var canSet = false;
for ( var i = 0; i < s.options.length; i++ ) {
if ( s.options[i].value == v ) {
s.options[i].selected = true;
canSet = true;
break;
}
}
return canSet;
}
*/

function queryStringValue(strKey, strDefault) {
    if (strDefault == null)
    { strDefault = ""; }

    strKey = strKey.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

    var regex = new RegExp("[\\?&]" + strKey + "=([^&#]*)");
    var qs = regex.exec(window.location.href);

    if (qs == null)
    { return strDefault; }
    else
    { return qs[1]; }
}

function checkValidQueryStringForSearch() {
    var result = true;

    // Parameter "ori" is Origin Acronym. Required.
    if (!existEachQueryString("ori")) {
        result = false;
    }
    else {
        if (queryStringValue("ori") == "") {
            result = false;
        }
    }

    // Parameter "des" is Destination Acronym. Required.
    if (!existEachQueryString("des")) {
        result = false;
    }
    else {
        if (queryStringValue("des") == "") {
            result = false;
        }
    }

    // Parameter "dep" is Departure Date. Its format is yyyy-mm-dd. Required.
    if (!existEachQueryString("dep")) {
        result = false;
    }
    else {
        var dep = queryStringValue("dep");
        if (dep == "") {
            result = false;
        }
        else {
            if (!validateQueryStringDate(dep)) {
                result = false;
            }
        }
    }

    // Parameter "ret" is Return Date. Its format is yyyy-mm-dd. Required if round trip.
    if (existEachQueryString("ret")) {
        var ret = queryStringValue("ret");
        if (ret == "") {
            result = false;
        }
        else {
            if (!validateQueryStringDate(ret)) {
                result = false;
            }
        }
    }

    // Parameter "adt" is Adult Passenger amount. Required.
    if (!existEachQueryString("adt")) {
        result = false;
    }
    else {
        var adult = queryStringValue("adt");
        if (adult == "") {
            result = false;
        }
        else {
            if (!isNum(adult)) {
                result = false;
            }
        }
    }

    // Parameter "chd" is Child Passenger amount. Required.
    if (!existEachQueryString("chd")) {
        result = false;
    }
    else {
        var child = queryStringValue("chd");
        if (child == "") {
            result = false;
        }
        else {
            if (!isNum(child)) {
                result = false;
            }
        }
    }

    // Parameter "inf" is Infant Passenger amount. Required.
    if (!existEachQueryString("inf")) {
        result = false;
    }
    else {
        var infant = queryStringValue("inf");
        if (infant == "") {
            result = false;
        }
        else {
            if (!isNum(infant)) {
                result = false;
            }
        }
    }

    // Parameter "pro" is Promotion Code.
    if (existEachQueryString("pro")) {
        var procode = queryStringValue("pro");
        if (procode == "") {
            result = false;
        }
    }

    // Parameter "bcls" is Boarding Class.
    if (existEachQueryString("bcls")) {
        var bclass = queryStringValue("bcls");
        if (bclass == "") {
            result = false;
        }
    }

    // Parameter "ft" is Fare Type.
    if (existEachQueryString("ft")) {
        var faretype = queryStringValue("ft");
        if (faretype == "") {
            result = false;
        }
    }

    return result;
}

function existEachQueryString(qryValue) {
    var hu = window.location.search.substring(1);
    var gy = hu.split("&");
    var found = false;
    for (i = 0; i < gy.length; i++) {
        ft = gy[i].split("=");
        if (ft[0] == qryValue) {
            found = true;
        }
    }
    return found;
}
function checkExistQueryString() {
    var hu = window.location.search.substring(1);
}
function validateQueryStringDate(dtValue) {
    var splitDate = dtValue.split("-");
    var refDate = new Date(splitDate[1] + "/" + splitDate[2] + "/" + splitDate[0]);
    if (splitDate[1] < 1 || splitDate[1] > 12 || refDate.getDate() != splitDate[2] || splitDate[0].length != 4 || (!/^20/.test(splitDate[0]))) {
        return false;
    }
    else {
        return true;
    }
}
function isNum(string) {
    var numericExpression = /^[0-9]+$/;
    if (string.match(numericExpression)) {
        return true;
    } else {
        return false;
    }
}
function generateYearMonth(strDate) {
    var arrDate = strDate.split("-");
    return arrDate[0] + arrDate[1];
}
function generateDate(strDate) {
    var arrDate = strDate.split("-");
    return arrDate[2];
}
function checkMyBookingString() {
    // mb=l
    if (existEachQueryString("mb") && queryStringValue("mb") == "l") {
        return true;
    }
    else {
        return false;
    }
}
function CheckClientLogon() {
    // mb=l
    if (existEachQueryString("cl") && queryStringValue("cl") == "l") {
        return true;
    }
    else {
        return false;
    }
}
function CheckRegisterLogon() {
    // mb=l
    if (existEachQueryString("rg") && queryStringValue("rg") == "l") {
        return true;
    }
    else {
        return false;
    }
}
function GetLowFareFinder() {
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    var arrAirport = objDestination.options[objDestination.selectedIndex].value.split("|");

    var strOrigin = arrAirport[0];
    var strDestination = arrAirport[2];
    var objOneWay = document.getElementById("optOneWay");

    var dtDateFrom = getdatefrom();
    var dtDateTo = 0;

    if (objOneWay.checked == false)
    { dtDateTo = getdateto(); }

    //Start Check Minimum Date that can select low fare finder.
    var d = Number(dtDateFrom.toString().substring(6, 8));
    var m = Number(dtDateFrom.toString().substring(4, 6));
    var y = Number(dtDateFrom.toString().substring(0, 4));

    var iDate = new Date(y, m - 1, d);

    var todayDate = new Date();
    var cDate = new Date(todayDate.getYear(), todayDate.getMonth(), todayDate.getDate());
    //End Check Minimum Date that can select low fare finder.

    if (dtDateTo != 0 && (dtDateFrom > dtDateTo)) {

        ShowMessageBox(objLanguage.Alert_Message_59, 0, '');
    }
    else {
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.GetLowFareFinder(strOrigin, strDestination, dtDateFrom, dtDateTo, SuccessGetLowFareFinder, showError, dtDateFrom + "|" + dtDateTo);
    }
    objDestination = null;
}

function GetNextLowFareFinder(searchDate, flightType) {
    ShowProgressBar(true);
    if (flightType == 'Return')
        tikAeroB2C.WebService.B2cService.GetLowFareFinder("", "", "0", searchDate, SuccessGetLowFareFinder, showError, showTimeOut);
    else if (flightType == 'OutWard')
        tikAeroB2C.WebService.B2cService.GetLowFareFinder("", "", searchDate, "0", SuccessGetLowFareFinder, showError, showTimeOut);
}

function SuccessGetLowFareFinder(result, strSelectDate) {
    if (result.length > 0) {
        if (result == "{102}") {
            //Invalid Required Parameter.
            ShowMessageBox(objLanguage.Alert_Message_55, 0, '');
        }
        else if (result == "{103}") {
            ShowMessageBox(objLanguage.Alert_Message_56, 0, 'loadHome');
        }
        else {
            var obj = document.getElementById("dvContainer");
            obj.innerHTML = result;
            var arrDate = strSelectDate.split("|");
            var objOutWare = document.getElementsByName("optLowfare_Outward");
            var objReturn = document.getElementsByName("optLowfare_Return");

            var todayDate = new Date();
            var cDate = new Date(todayDate.getYear(), todayDate.getMonth(), todayDate.getDate());

            var strSelectDate;
            var d = Number(strSelectDate.substring(6, 8));
            var m = Number(strSelectDate.substring(4, 6));
            var y = Number(strSelectDate.substring(0, 4));

            var iDate;
            var objJSON;

            DisplayQuoteSummary("", "", "");
            if (objOutWare != null && objOutWare.length > 0) {
                for (var i = 0; i < objOutWare.length; i++) {
                    objJSON = eval("(" + objOutWare[i].value + ")");
                    strSelectDate = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                    var d = Number(strSelectDate.substring(6, 8));
                    var m = Number(strSelectDate.substring(4, 6));
                    var y = Number(strSelectDate.substring(0, 4));

                    iDate = new Date(y, m - 1, d);
                    //Remove date at are not match the 21 days
                    if (dateDiff("d", cDate, iDate) <= 21) {
                        objOutWare[i].style.display = "none";
                    }
                    //Select at selected date
                    if (strSelectDate == arrDate[0]) {
                        objOutWare[i].checked = true;
                    }
                    iDate = null;
                }
                //Highlight Selected date
                LffHighLight("Outward", true);
                GetSelectLowFareSummary("Outward");



            }

            if (objReturn != null && objReturn.length > 0) {
                for (var i = 0; i < objReturn.length; i++) {
                    objJSON = eval("(" + objReturn[i].value + ")");
                    strSelectDate = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                    var d = Number(strSelectDate.substring(6, 8));
                    var m = Number(strSelectDate.substring(4, 6));
                    var y = Number(strSelectDate.substring(0, 4));

                    iDate = new Date(y, m - 1, d);
                    //Remove date at are not match the 21 days
                    if (dateDiff("d", cDate, iDate) <= 21) {
                        objReturn[i].style.display = "none";
                    }
                    //Select at selected date
                    if (strSelectDate == arrDate[1]) {
                        objReturn[i].checked = true;
                    }
                }
                //Highlight Selected date
                LffHighLight("Return", true);
                GetSelectLowFareSummary("Return");


            }

            $('#spOriLowfare_Outward').html(strOriLowfare);
            $('#spDestLowfare_Outward').html(strDestLowfare);

            $('#spOriLowfare_Return').html(strDestLowfare);
            $('#spDestLowfare_Return').html(strOriLowfare);


            ShowSearchPannel(true);
            objOutWare = null;
            objReturn = null;
            ShowProgressBar(false);
        }
    }
}
function GetAvailabilityFromLowFareFinder() {
    var objOptLowFare;

    var strParam = "";
    var strDateFrom = 0;
    var strDateTo = 0;
    var dblOutwarFare = 0;
    var dblRetrunFare = 0;
    var objJSON;

    //-- Outward Flight
    objOptLowFare = document.getElementsByName("optLowfare_Outward");
    if (objOptLowFare != null) {
        for (var i = 0; i < objOptLowFare.length; i++) {
            if (objOptLowFare[i].checked == true) {
                objJSON = eval("(" + objOptLowFare[i].value + ")");

                strParam = objJSON.origin_rcd + "|" + objJSON.origin_name + "|" + objJSON.destination_rcd + "|1";
                strDateFrom = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                dblOutwarFare = objJSON.total_adult_fare;
                var objOutwardNetTotal = document.getElementById("spnFareNetTotal_Outward");
                if (objOutwardNetTotal != null) {
                    dblOutwarFare = parseFloat(objOutwardNetTotal.innerHTML);
                    objOutwardNetTotal = null;
                }
            }
        }
    }
    objOptLowFare = null;

    //-- Return Flight
    objOptLowFare = document.getElementsByName("optLowfare_Return");
    if (objOptLowFare != null) {
        for (var i = 0; i < objOptLowFare.length; i++) {
            if (objOptLowFare[i].checked == true) {
                objJSON = eval("(" + objOptLowFare[i].value + ")");

                strDateTo = ReformatXmlToYYYYMMDD(objJSON.departure_date);
                dblRetrunFare = objJSON.total_adult_fare;
                var objReturnNetTotal = document.getElementById("spnFareNetTotal_Return");
                if (objReturnNetTotal != null) {
                    dblRetrunFare = parseFloat(objReturnNetTotal.innerHTML);
                    objReturnNetTotal = null;
                }
            }
        }
    }
    objOptLowFare = null;

    if (strDateFrom != "") {
        if (strDateFrom > strDateTo && strDateTo != 0) {
            //Please check your return Date! It must be equal to or higher than the Departure Date
            ShowMessageBox(objLanguage.Alert_Message_5, 0, '');
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.GetAvailabilityFromLowFareFinder(strParam,
                                                                          strDateFrom,
                                                                          strDateTo,
                                                                          false,
                                                                          dblOutwarFare,
                                                                          dblRetrunFare,
                                                                          tmpOutFlight_id + ":" + tmpOutFare_id,
                                                                          tmpRetFlight_id + ":" + tmpRetFare_id,
                                                                          SuccessGetAvailabilityFromLowFareFinder,
                                                                          showError,
                                                                          dblOutwarFare + "|" + dblRetrunFare);
        }
    }
    else {
        //"Please select outward flight"
        ShowMessageBox(objLanguage.Alert_Message_57, 0, '');
    }
}
function SuccessGetAvailabilityFromLowFareFinder(result, strFareAmount) {
    if (result.length > 0) {

        if (result == "{201}") {
            ShowMessageBox("Booking object is null.(Session time out).", 0, 'loadHome');
        }
        else if (result == "{102}") {
            ShowMessageBox("Low fare finder input not found, Please check your input.", 0, '');
        }
        else {
            var obj = document.getElementById("dvContainer");
            obj.innerHTML = result.split("{}")[1];

            var chkOutward = document.getElementsByName("Outward");
            var chkReturn = document.getElementsByName("Return");
            var arrLowestAmount = strFareAmount.split("|");

            if (chkOutward != null && chkOutward.length > 0) {
                chkOutward[0].checked = true;
            }
            if (chkReturn != null && chkReturn.length > 0) {
                chkReturn[0].checked = true;
            }

            //preselect at the lowest far.
            var arrLowestAmount = strFareAmount.split("|");
            var dclOutboundFare = parseFloat(arrLowestAmount[0]);
            var dclReturnFare = parseFloat(arrLowestAmount[1]);
            var objOut = document.getElementsByName("Outward");
            var objReturn = document.getElementsByName("Return");
            var dclTotalFareAmount = 0;
            var arrParam;
            //loop to preselect the lowest fare.
            if (objOut != null) {
                for (var i = 0; i < objOut.length; i++) {
                    arrParam = objOut[i].value.split("|");
                    if (arrParam.length > 0) {
                        dclTotalFareAmount = parseFloat(arrParam[20].split(":")[1]) + parseFloat(arrParam[21].split(":")[1]);
                        if (dclTotalFareAmount == dclOutboundFare) {
                            objOut[i].checked = true;
                            break;
                        }
                    }
                }
            }

            //Clear Param value.
            arrParam = null;
            dclTotalFareAmount = 0;
            if (objReturn != null) {
                for (var i = 0; i < objReturn.length; i++) {
                    arrParam = objReturn[i].value.split("|");
                    if (arrParam.length > 0) {
                        dclTotalFareAmount = parseFloat(arrParam[20].split(":")[1]) + parseFloat(arrParam[21].split(":")[1]);
                        if (dclTotalFareAmount == dclReturnFare) {
                            objReturn[i].checked = true;
                            break;
                        }
                    }
                }
            }
            // Show Flight tool tip.
            ToolTipColor();

            //Move scroll to top page.
            scroll(0, 0);
        }
    }
    ShowProgressBar(false);
}

function GetFlightSelectValue(o) {
    for (var i = 0; i < o.length; i++) {
        if (o[i].checked == true) {
            return o[i].value;
        }
    }
    return "";
}

function TotalDayOfmonth(month, year) {
    return Math.round(((new Date(year, month)) - (new Date(year, month - 1))) / 86400000);
}

function genValuesDDL(obj, strDate) {
    ddl = document.getElementById(obj);
    clearDDL(ddl);
    for (var i = 1; i <= TotalDayOfmonth(strDate.substring(0, 2), strDate.substring(2, 6)); i++) {
        var opt = document.createElement("option");
        if (i < 10) {
            i = "0" + i;
        }
        opt.text = i;
        opt.value = i;
        ddl.options.add(opt);
    }
}

function setFromDate(strDate) {

    genValuesDDL("ddlDate_1", strDate);
    SetComboValue("ddlMY_1", strDate.substring(0, 6));
    SetComboValue("ddlDate_1", strDate.substring(6, 8));
}
function setToDate(strDate) {

    genValuesDDL("ddlDate_2", strDate);
    SetComboValue("ddlMY_2", strDate.substring(0, 6));
    SetComboValue("ddlDate_2", strDate.substring(6, 8));
}

function LffHighLight(strName, bSelect) {
    var objOptLowFare = document.getElementsByName("optLowfare_" + strName);
    var strId = "";
    if (objOptLowFare != null) {
        for (var i = 0; i < objOptLowFare.length; i++) {
            if (objOptLowFare[i].checked == true) {
                strId = objOptLowFare[i].id.split("_")[2];
            }
        }
    }

    var objTd = document.getElementById("td_" + strName + "_" + strId);
    var ObjSpn = document.getElementById("spnLowfare_" + strName + "_" + strId);
    if (bSelect == true) {
        if (objTd != null)
        { objTd.className = "Select"; }

        if (ObjSpn != null)
        { ObjSpn.className = "Lowfarepriceselect"; }

    }
    else {
        if (objTd != null)
        { objTd.className = ""; }
        if (ObjSpn != null)
        { ObjSpn.className = "Lowfareprice"; }
    }

    objOptLowFare = null;
}

function SelectLlf(strName) {
    LffHighLight(strName.split("_")[0], false);
    document.getElementById("optLowfare_" + strName).checked = true;
    LffHighLight(strName.split("_")[0], true);
    return false;
}
function GetFlightTime(strId) {
    var objTime = document.getElementById("dvTime_" + strId);
    objTime.style.display = "block";
    objTime = null;
}

function ClearShowTime(strElement, event) {
    var current_mouse_target = null;
    var element = document.getElementById(strElement);

    if (event.toElement) {
        current_mouse_target = event.toElement;
    } else if (event.relatedTarget) {
        current_mouse_target = event.relatedTarget;
    }
    if (!is_child_of(element, current_mouse_target) && element != current_mouse_target) {
        element.innerHTML = "";
    }
    element = null;
}

function is_child_of(parent, child) {
    if (child != null) {
        while (child.parentNode) {
            if ((child = child.parentNode) == parent) {
                return true;
            }
        }
    }
    return false;
}
var aircraft_type_rcd_Outward = "";
var transit_aircraft_type_rcd_Outward = "";
var aircraft_type_rcd_Return = "";
var transit_aircraft_type_rcd_Return = "";

function GetQuoteSummary(objChk, strType) {
    var objOutwards = document.getElementsByName("Outward");
    var objReturns = document.getElementsByName("Return");
    var lenOutward = objOutwards.length;
    var lenReturn = objReturns.length;
    var objClassName = "";

    var sOutward = "";
    for (var i = 0; i < lenOutward; i++) {
        if (objOutwards[i] != null) {
            //alert("objOutwards :" + objOutwards[i].checked);
            sOutward = objOutwards[i].id;
            if (objOutwards[i].checked == true) {
                document.getElementById(sOutward.substring(3)).className = "SearchFlightSelection";
            }
            else {
                objClassName = "BodyCOL" + (5 + parseInt(sOutward.substring(sOutward.length - 1)));
                document.getElementById(sOutward.substring(3)).className = objClassName;
            }
        }
    }

    var sReturn = "";
    for (var i = 0; i < lenReturn; i++) {
        if (objReturns[i] != null) {
            //alert("objReturns :" + objReturns[i].checked);
            sReturn = objReturns[i].id;
            if (objReturns[i].checked == true) {
                document.getElementById(sReturn.substring(3)).className = "SearchFlightSelection";
            }
            else {
                objClassName = "BodyCOL" + (5 + parseInt(sReturn.substring(sReturn.length - 1)));
                document.getElementById(sReturn.substring(3)).className = objClassName;
            }
        }
    }
    var strXml = "";
    var strParam = "";
    var strAirlineRcd = "";
    var strFlightNumber = "";
    var strFlightId = "";
    var strOrigin = "";
    var strDest = "";
    var strFareId = "";
    var strFlightConnectId = "";
    var dtDepartture = "";
    var dtArrival = "";
    var departTime = "";
    var arrivalTime = "";

    var strTransitAirlineRcd = "";
    var strTransitFlightNumber = "";
    var dtTransitDepartureDate = "";
    var dtTransitArrivalDate = "";
    var transitDepartTime = "";
    var transitArrivalTime = "";
    var strTransitAirport;
    var strTransitFareId = "";
    var strBookingClassRcd = "";

    var arrFlightInfo;
    var arrFlightDetail;

    if (objChk.checked == true) {
        strParam = objChk.value;
        if (strParam.length > 0) {
            arrFlightInfo = strParam.split("|");
            for (var i = 0; i < arrFlightInfo.length; i++) {
                arrFlightDetail = arrFlightInfo[i].split(":");
                if (arrFlightDetail[0] == "flight_id") {
                    strFlightId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "airline_rcd") {
                    strAirlineRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "flight_number") {
                    strFlightNumber = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "origin_rcd") {
                    strOrigin = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "destination_rcd") {
                    strDest = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "fare_id") {
                    strFareId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_flight_id") {
                    strFlightConnectId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "departure_date") {
                    dtDepartture = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "planned_departure_time") {
                    departTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "planned_arrival_time") {
                    arrivalTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_airline_rcd") {
                    strTransitAirlineRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_flight_number") {
                    strTransitFlightNumber = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_departure_date") {
                    dtTransitDepartureDate = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_planned_departure_time") {
                    transitDepartTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_planned_arrival_time") {
                    transitArrivalTime = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_airport_rcd") {
                    strTransitAirport = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_fare_id") {
                    strTransitFareId = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "booking_class_rcd") {
                    strBookingClassRcd = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "arrival_date") {
                    dtArrival = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_arrival_date") {
                    dtTransitArrivalDate = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "aircraft_type_rcd") {
                    if (strType == "Outward")
                        aircraft_type_rcd_Outward = arrFlightDetail[1];
                    else
                        aircraft_type_rcd_Return = arrFlightDetail[1];
                }
                else if (arrFlightDetail[0] == "transit_aircraft_type_rcd") {
                    if (strType == "Outward")
                        transit_aircraft_type_rcd_Outward = arrFlightDetail[1];
                    else
                        transit_aircraft_type_rcd_Return = arrFlightDetail[1];
                }
            }
            if (strOrigin.length > 0) {
                strXml = "<flights>" +
                            "<flight>" +
                                "<flight_id>" + strFlightId + "</flight_id>" +
                                "<airline_rcd>" + strAirlineRcd + "</airline_rcd>" +
                                "<flight_number>" + strFlightNumber + "</flight_number>" +
                                "<origin_rcd>" + strOrigin + "</origin_rcd>" +
                                "<destination_rcd>" + strDest + "</destination_rcd>" +
                                "<fare_id>" + strFareId + "</fare_id>" +
                                "<transit_airline_rcd>" + strTransitAirlineRcd + "</transit_airline_rcd>" +
                                "<transit_flight_number>" + strTransitFlightNumber + "</transit_flight_number>" +
                                "<transit_flight_id>" + strFlightConnectId + "</transit_flight_id>" +
                                "<departure_date>" + dtDepartture + "</departure_date>" +
                                "<arrival_date>" + dtArrival + "</arrival_date>" +
                                "<arrival_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtArrival)) + "</arrival_day>" +
                                "<departure_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtDepartture)) + "</departure_day>" +
                                "<planned_departure_time>" + departTime + "</planned_departure_time>" +
                                "<planned_arrival_time>" + arrivalTime + "</planned_arrival_time>" +
                                "<transit_departure_date>" + dtTransitDepartureDate + "</transit_departure_date>" +
                                "<transit_departure_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtTransitDepartureDate)) + "</transit_departure_day>" +
                                "<transit_arrival_date>" + dtTransitArrivalDate + "</transit_arrival_date>" +
                                "<transit_arrival_day>" + GetDayOfWeek(ReformatYYYYMMDDToDashDate(dtTransitArrivalDate)) + "</transit_arrival_day>" +
                                "<transit_planned_departure_time>" + transitDepartTime + "</transit_planned_departure_time>" +
                                "<transit_planned_arrival_time>" + transitArrivalTime + "</transit_planned_arrival_time>" +
                                "<transit_airport_rcd>" + strTransitAirport + "</transit_airport_rcd>" +
                                "<transit_fare_id>" + strTransitFareId + "</transit_fare_id>" +
                                "<booking_class_rcd>" + strBookingClassRcd + "</booking_class_rcd>" +
                            "</flight>" +
                        "</flights>";
                //Call Webservice
                tikAeroB2C.WebService.B2cService.GetQuoteSummary(strXml,
                                                                 strType,
                                                                 SuccessGetQuoteSummary,
                                                                 showError,
                                                                 strType);
            }
        }
    }

}
function SuccessGetQuoteSummary(result, strType) {

    if (result == "{100}" || result == "{101}" || result == "{004}") {
        //System Error. Please try again.
        ShowMessageBox(objLanguage.Alert_Message_56, 0, '');
    }
    else {

        if (strType == "Outward") {
            DisplayQuoteSummary("", result, "");
            $('#spAircraft_Outward').text(aircraft_type_rcd_Outward);
            $('#spAircraft_transit_Outward').text(transit_aircraft_type_rcd_Outward);
        }
        else {
            DisplayQuoteSummary("", "", result);
            $('#spAircraft_Return').text(aircraft_type_rcd_Return);
            $('#spAircraft_transit_Return').text(transit_aircraft_type_rcd_Return);
        }
        //Initialize Collape
        InitializeFareSummaryCollapse(strType);
    }

}
function SelectTime(strObj) {
    if (strObj.length > 0) {
        var obj = document.getElementById(strObj);
        if (obj != null && obj.parentNode.parentNode.parentNode != null) {
            var strType = obj.parentNode.id.split("_")[1];
            var objJSON = eval("(" + obj.value + ")");
            GetLowFareFinderSummary(strType, objJSON);
        }

    }

}
function SearchAvailabilityParameter() {
    var objOrigin = document.getElementById(FindControlName("select", "optOrigin"));
    var strOrigin = objOrigin.options[objOrigin.selectedIndex].value;
    var objDestination = document.getElementById(FindControlName("select", "optDestination"));
    var objOneWay = document.getElementById("optOneWay");
    var objAdult = document.getElementById("optAdult");
    var objChild = document.getElementById("optChild");
    var objInfant = document.getElementById("optInfant");
    var objBoardingClass = document.getElementById("optBoardingClass");
    var objPromoCode = document.getElementById("txtPromoCode");
    var objYP = document.getElementById("optYP");
    var objLang = document.getElementById('hdLang');

    var strDestination = objDestination.options[objDestination.selectedIndex].value;
    var isOneWay = objOneWay.checked;
    var iAdult = parseInt(objAdult.options[objAdult.selectedIndex].value);
    var iChild = parseInt(objChild.options[objChild.selectedIndex].value);
    var iInfant = parseInt(objInfant.options[objInfant.selectedIndex].value);
    var strBoardingClass = objBoardingClass.options[objBoardingClass.selectedIndex].value;
    var currencyCode = strOrigin.split("|")[1];
    var dtDateFrom = getdatefrom();
    var dtDateTo = getdateto();
    var strPromoCode = "";
    var iOther = 0;
    var otherType = "";
    var bLFF = false;
    var language = objLang.value == "" ? "en-us" : objLang.value;

    // Check Ip address for Default Currency 
    var obtCurrency = document.getElementById("AvailabilitySearch1_optCurrency");
    var objSearchTypeCal = document.getElementById("rdSearchTypeCal");

    if (obtCurrency != null) {
        currencyCode = obtCurrency.options[obtCurrency.selectedIndex].value;
    }

    if (objSearchTypeCal != null) {
        if (objSearchTypeCal.checked == true) {
            bLFF = true;
        }
    }
    
    // End Check Ip address for Default Currency 
    if (objYP != null) {
        iOther = parseInt(objYP.options[objYP.selectedIndex].value);
        otherType = "YP";
    }

    if (objPromoCode != null) {
        strPromoCode = objPromoCode.value;
    }

    //Call Availability web service
    if (iInfant > iAdult) {
        //Infant Can't be more than adult.
        ShowMessageBox(objLanguage.Alert_Message_2, 0, '');
    }
    else if ((iAdult + iChild + iInfant) > 12) {
        //Total passenger select can't be more than 12
        ShowMessageBox(objLanguage.objLanguage.Alert_Message_53.replace("{NO}", 12), 0, '');
    }
    else if ((iAdult + iChild + iInfant + iOther) == 0) {
        //Please select number of passenger.
        ShowMessageBox(objLanguage.Alert_Message_6, 0, '');
    }
    else if (strOrigin.length == 0 || strDestination.length == 0) {
        ShowMessageBox(objLanguage.Alert_Message_54, 0, '');
    }
    else {
        //Popup new window for search result

        var strParameter = "?ori=" + strOrigin.split("|")[0] +
                            "&des=" + strDestination.split("|")[2] +
                            "&dep=" + ReformatYYYYMMDDToDashDate(dtDateFrom) +
                            ((isOneWay == false) ? "&ret=" + ReformatYYYYMMDDToDashDate(dtDateTo) : "") +
                            "&adt=" + iAdult +
                            "&chd=" + iChild +
                            "&inf=" + iInfant +
                            "&currency=" + currencyCode +
                            "&bLFF=" + bLFF +
                            "&langculture=" + queryStringValue("langculture", language);

        var strUrl = GetVirtualDirectory() + "default.aspx" + strParameter;
        window.open(strUrl, "SearchAvail", "height=" + screen.height + ",width= " + screen.width + " location=yes,scrollbars=1,toolbar=yes,resizable=yes,left=0,top=10,titlebar=yes,status=yes");
    }

    objOrigin = null;
    objDestination = null;
    objOneWay = null;
    objAdult = null;
    objChild = null;
    objInfant = null;
    objBookingClass = null;
    objPromoCode = null;
}

function DisplayQuoteSummary(strFareSummaryHtml, strOutwardHtml, strReturnHtml) {

    var objDvFareSummary = document.getElementById("dvFareSummary");
    var objDvAvaiFareSummary = document.getElementById("dvAvaiQuote");
    var objOFareLine = document.getElementById("dvFareLine_Outward");
    var objRFareLine = document.getElementById("dvFareLine_Return");
    var objSummary = document.getElementById("dvYourSelection");

    if (strFareSummaryHtml.length == 0 & strOutwardHtml.length == 0 & strReturnHtml.length == 0) {

        objSummary.style.display = "none";
        if (objOFareLine != null) {
            objOFareLine.innerHTML = "";
        }
        if (objRFareLine != null) {
            objRFareLine.innerHTML = "";
        }
        if (objDvFareSummary != null) {
            objDvFareSummary.innerHTML = "";
        }
    }
    else {

        objSummary.style.display = "block";

        if (strFareSummaryHtml.length > 0) {
            if (objDvFareSummary != null) {
                objDvFareSummary.style.display = "block";
                objDvFareSummary.innerHTML = strFareSummaryHtml;
            }
        }
        else {
            if (objDvFareSummary != null) {
                objDvFareSummary.style.display = "none";
                objDvFareSummary.innerHTML = "";
            }
        }

        if (objOFareLine != null && objRFareLine != null) {
            if (strOutwardHtml.length > 0 | strReturnHtml.length > 0) {
                if (objDvAvaiFareSummary != null) {
                    objDvAvaiFareSummary.style.display = "block";

                    if (strOutwardHtml.length > 0) {
                        objOFareLine.innerHTML = strOutwardHtml;
                    }

                    if (strReturnHtml.length > 0) {
                        objRFareLine.innerHTML = strReturnHtml;
                    }
                }
            }
            else {
                if (objDvAvaiFareSummary != null) {
                    objDvAvaiFareSummary.style.display = "none";

                    objOFareLine.innerHTML = "";
                    objRFareLine.innerHTML = "";
                }
            }
        }
        //Calculate totoal summary.
        CalculateTotalFare();
    }
}
function SelectLowestFare() {

    var objOut = document.getElementsByName("Outward");
    var objReturn = document.getElementsByName("Return");
    var dclFareAmount = 0;
    var arrFareValue;
    var arrFare;
    var arrTax;
    var j = 0;

    //Search For outbound.
    if (objOut != null) {
        for (var i = 0; i < objOut.length; i++) {

            arrFareValue = objOut[i].value.split("|");
            arrFare = arrFareValue[21].split(":");
            arrTax = arrFareValue[20].split(":");

            //Get Total Fare plus Tax
            if (dclFareAmount == 0) {
                j = i;
                dclFareAmount = parseFloat(arrFare[1]) + parseFloat(arrTax[1]);
            }
            else if (dclFareAmount > (parseFloat(arrFare[1]) + parseFloat(arrTax[1]))) {
                j = i;
                dclFareAmount = parseFloat(arrFare[1]) + parseFloat(arrTax[1]);
            }
        }

        if (objOut.length > 0) {
            objOut[j].checked = true;
            GetQuoteSummary(objOut[j], "Outward");
        }
    }

    //Search For return.
    dclFareAmount = 0;
    j = 0;
    if (objReturn != null) {
        for (var i = 0; i < objReturn.length; i++) {

            arrFareValue = objReturn[i].value.split("|");
            arrFare = arrFareValue[21].split(":");
            arrTax = arrFareValue[20].split(":");

            //Get Total Fare plus Tax
            if (dclFareAmount == 0) {
                j = i;
                dclFareAmount = parseFloat(arrFare[1]) + parseFloat(arrTax[1]);
            }
            else if (dclFareAmount > (parseFloat(arrFare[1]) + parseFloat(arrTax[1]))) {
                j = i;
                dclFareAmount = parseFloat(arrFare[1]) + parseFloat(arrTax[1]);
            }
        }

        if (objReturn.length > 0) {
            objReturn[j].checked = true;
            GetQuoteSummary(objReturn[j], "Return");
        }
    }
}
function CalculateTotalFare() {
    var objhdTotal = document.getElementsByName("hdSubTotal");
    var objDvTotal = document.getElementById("dvTotalFareSummary");
    var objDdSign = document.getElementById("hdCurrencySign");

    var dclTotal = 0;
    if (objhdTotal != null) {
        for (var i = 0; i < objhdTotal.length; i++) {
            dclTotal = dclTotal + parseFloat("0" + GetControlValue(objhdTotal[i]));
        }
    }
    if (objDvTotal != null) // && objDdSign != null
    {
        objDvTotal.innerHTML = GetControlValue(objDdSign) + " " + AddCommas(dclTotal.toFixed(2));
    }
}

function ConfirmSelectFlight() {
    var chkOutward = document.getElementsByName("Outward");
    var OutwardParam = "";
    var OutwardDate = 0;
    var OutwardTime = 0;
    var OutwardArrDate = 0;
    var OutwardArrivalTime = 0;


    var chkReturn = document.getElementsByName("Return");
    var ReturnParam = "";
    var ReturnDate = 0;
    var ReturnTime = 0;
    var ReturnArrivalTime = 0;

    var iCount = 0;

    var objSelectedDate = document.getElementsByName("hdSelectedDate");

    CloseMessageBox();
    if (objSelectedDate.length == 2 & (chkReturn == null || chkReturn.length == 0)) {
        //Please select return flight.
        ShowMessageBox("Please select return flight.", 0, '');
    }
    else {
        if (chkOutward != null) {
            iCount = chkOutward.length;
            if (iCount > 0) {
                for (var i = 0; i < iCount; i++) {
                    if (chkOutward[i].checked == true) {
                        OutwardParam = chkOutward[i].value;
                        break;
                    }
                }
                if (OutwardParam.length > 0) {
                    arrFlightInfo = OutwardParam.split("|");
                    for (var i = 0; i < arrFlightInfo.length; i++) {
                        arrFlightDetail = arrFlightInfo[i].split(":");
                        if (arrFlightDetail[0] == "departure_date") {
                            OutwardDate = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "arrival_date") {
                            OutwardArrDate = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_departure_time") {
                            OutwardTime = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_arrival_time") {
                            OutwardArrivalTime = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "transit_planned_arrival_time") {
                            if (arrFlightDetail[1].length > 0) {
                                OutwardArrivalTime = parseInt(arrFlightDetail[1]);
                            }
                        }
                        if (arrFlightDetail[0] == "transit_arrival_date") {
                            if (arrFlightDetail[1].length > 0) {
                                OutwardArrDate = parseInt(arrFlightDetail[1]);
                            }
                        }
                    }
                }
            }
        }

        iCount = 0;
        if (chkReturn != null) {
            iCount = chkReturn.length;
            if (iCount > 0) {
                for (var i = 0; i < iCount; i++) {
                    if (chkReturn[i].checked == true) {
                        ReturnParam = chkReturn[i].value;
                        break;
                    }
                }
                if (ReturnParam.length > 0) {
                    arrFlightInfo = ReturnParam.split("|");
                    for (var i = 0; i < arrFlightInfo.length; i++) {
                        arrFlightDetail = arrFlightInfo[i].split(":");
                        if (arrFlightDetail[0] == "departure_date") {
                            ReturnDate = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_departure_time") {
                            ReturnTime = parseInt(arrFlightDetail[1]);
                        }
                        if (arrFlightDetail[0] == "planned_arrival_time") {
                            ReturnArrivalTime = parseInt(arrFlightDetail[1]);
                        }
                    }
                }
            }
        }

        if (OutwardParam.length == 0) {
            //Please Select your flight of origin.
            ShowMessageBox(objLanguage.Alert_Message_57, 0, '');
        }
        else if (objSelectedDate.length == 2 && ReturnParam.length == 0) {
            //Please select return flight.
            ShowMessageBox("Please select return flight.", 0, '');
        }
        else if (ReturnParam.length != 0 && (OutwardArrDate > ReturnDate)) {
            //  else if (ReturnParam.length != 0 && (OutwardDate > ReturnDate)) {
            //Please check your return Date! It must be equal to or higher than the Departure Date.
            ShowMessageBox(objLanguage.Alert_Message_5, 0, '');
        }
        else if (ReturnParam.length != 0 && (OutwardTime > ReturnTime) && (OutwardDate == ReturnDate)) {
            ////Please check your return Time! It must be equal to or higher than the Departure Time.
            ShowMessageBox(objLanguage.Alert_Message_58, 0, '');
        }
        else if (ReturnParam.length != 0 && (OutwardArrivalTime >= ReturnTime) && (OutwardDate == ReturnDate)) {
            ////Please check your return Time! It must be equal to or higher than the Departure Time.
            ShowMessageBox(objLanguage.Alert_Message_58, 0, '');
        }
        else {
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.AddFlight(OutwardParam, ReturnParam, g_strIpAddress, SuccessSelectFlight, showError, showTimeOut);
        }

        chkOutward = null;
        chkReturn = null;
    }
}
function SearchLowFareSingleMonth(flightDate, strFlightType) {

    if (strFlightType == "Outward") {

        setFromDate(flightDate);
        ShowProgressBar(true);
        tikAeroB2C.WebService.B2cService.SearchLowFareSingleMonth(flightDate, true, SuccessGetLowFareFinder, showError, getdatefrom() + "|" + getdateto());
    }
    else {

        if (flightDate >= getMonthfrom()) {
            setToDate(flightDate);
            ShowProgressBar(true);
            tikAeroB2C.WebService.B2cService.SearchLowFareSingleMonth(flightDate, false, SuccessGetLowFareFinder, showError, getdatefrom() + "|" + getdateto());

        }
        else {
            ShowMessageBox('Return date connot be less than departure date', 0, 'Return date connot be less than departure date');
        }
    }
}

function GetLowFareFinderSummary(strFligtType, objFlightParam) {
    var strdt = objFlightParam.departure_date.substring(6, 8);
    switch (strFligtType) {
        case "Outward":
            tmpOutFare_id = objFlightParam.fare_id.replace('{', '');
            tmpOutFare_id = tmpOutFare_id.replace('}', '');
            tmpOutFlight_id = objFlightParam.flight_id.replace('{', '');
            tmpOutFlight_id = tmpOutFlight_id.replace('}', '');

            setdatefrom(strdt);
            break;
        case "Return":
            tmpRetFare_id = objFlightParam.fare_id.replace('{', '');
            tmpRetFare_id = tmpRetFare_id.replace('}', '');
            tmpRetFlight_id = objFlightParam.flight_id.replace('{', '');
            tmpRetFlight_id = tmpRetFlight_id.replace('}', '');

            setdateto(strdt);
            break;
    }

    var strXml = "<flights>" +
                         "<flight>" +
                            "<airline_rcd>" + objFlightParam.airline_rcd + "</airline_rcd>" +
                            "<flight_number>" + objFlightParam.flight_number + "</flight_number>" +
                            "<flight_id>" + objFlightParam.flight_id + "</flight_id>" +
                            "<origin_rcd>" + objFlightParam.origin_rcd + "</origin_rcd>" +
                            "<destination_rcd>" + objFlightParam.destination_rcd + "</destination_rcd>" +
                            "<booking_class_rcd>" + objFlightParam.booking_class_rcd + "</booking_class_rcd>" +
                            "<fare_id>" + objFlightParam.fare_id + "</fare_id>" +
                            "<transit_flight_id>" + objFlightParam.transit_flight_id + "</transit_flight_id>" +
                            "<departure_date>" + objFlightParam.departure_date + "</departure_date>" +
                            "<arrival_date>" + objFlightParam.arrival_date + "</arrival_date>" +
                            "<transit_departure_date>" + objFlightParam.transit_departure_date + "</transit_departure_date>" +
                            "<transit_airport_rcd>" + objFlightParam.transit_airport_rcd + "</transit_airport_rcd>" +
                            "<planned_departure_time>" + objFlightParam.planned_departure_time + "</planned_departure_time>" +
                            "<planned_arrival_time>" + objFlightParam.planned_arrival_time + "</planned_arrival_time>" +
                            "<transit_fare_id>" + objFlightParam.transit_fare_id + "</transit_fare_id>" +
                             "<transit_airline_rcd>" + objFlightParam.transit_airline_rcd + "</transit_airline_rcd>" +
                            "<transit_flight_number>" + objFlightParam.transit_flight_number + "</transit_flight_number>" +
                            "<transit_arrival_date>" + objFlightParam.transit_arrival_date + "</transit_arrival_date>" +
                            "<transit_planned_departure_time>" + objFlightParam.transit_planned_departure_time + "</transit_planned_departure_time>" +
                            "<transit_planned_arrival_time>" + objFlightParam.transit_planned_arrival_time + "</transit_planned_arrival_time>" +
                        "</flight>" +
                    "</flights>";
    //Call Webservice
    tikAeroB2C.WebService.B2cService.GetQuoteSummary(strXml,
                                                     strFligtType,
                                                     SuccessGetQuoteSummary,
                                                     showError,
                                                     strFligtType);
}
function GetSelectLowFareSummary(strFlightType) {
    var objOpt = document.getElementsByName("optLowfare_" + strFlightType);
    var objHd = document.getElementsByName("hdTime_" + strFlightType);
    var objJSON = null;

    if (objOpt != null && objOpt.length > 0) {
        for (var i = 0; i < objOpt.length; i++) {
            if (objOpt[i].checked == true) {
                objJSON = eval("(" + objHd[i].value + ")");
                //Call display summary.
                GetLowFareFinderSummary(strFlightType, objJSON);
                break;
            }
        }
    }
}

function CallCOB() {
    var strUrl = GetVirtualDirectory();
    var strParameterValue = getRequestParameter("langculture");
    var strLanng = "";
    if (strParameterValue.length > 0) {
        strLanng = "?langculture=" + getRequestParameter("langculture");
    }
    if (strUrl.indexOf('flywithstyle') == -1) {
        strUrl = GetVirtualDirectory() + "cob/default.aspx" + strLanng;
    }
    else {
        strUrl = "http://www.flywithstyle.com/dth/cob/default.aspx" + strLanng;
    }

    window.open(strUrl, "cob", "height=" + screen.height + ",width= " + screen.width + " location=yes,scrollbars=1,toolbar=yes,resizable=yes,left=0,top=10,titlebar=yes,status=yes");
}
function OpenWebCheckIn() {
    var strUrl = GetVirtualDirectory();

    if (strUrl.indexOf('flywithstyle') == -1) {
        strUrl = GetVirtualDirectory() + "webcheckin/default.aspx";
    }
    else {
        strUrl = "http://www.flywithstyle.com/dth/webcheckin/default.aspx";
    }

    window.open(strUrl, "webcheckin", "height=" + screen.height + ",width= " + screen.width + " location=yes,scrollbars=1,toolbar=yes,resizable=yes,left=0,top=10,titlebar=yes,status=yes");
}
