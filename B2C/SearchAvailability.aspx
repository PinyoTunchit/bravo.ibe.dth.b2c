﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="Default" Inherits="tikAeroB2C.SearchAvailability" %>

<%@ Register TagPrefix="AvailabilitySearch" TagName="AvailabilitySearch" Src="~/UserControls/SearchAvailabilityDefault.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link href="App_Themes/Default/Style/main.css?V60" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/style.css?V60" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/jQuery/jquery-1.6.1.min.js"></script>   
    <script type="text/javascript" src="Scripts/jQuery/jquery.maskedinput-1.2.2.min.js"></script>   
    <script type="text/javascript" src="Scripts/jQuery/jquery.base64.min.js"></script>
    <script type="text/javascript" src="Scripts/jQuery/jquery.collapse.js"></script>

<%--    <script type="text/javascript" src="Scripts/Main.js?V60"></script>
    <script type="text/javascript" src="Scripts/uxCalendar.js?V60"></script>
    <script type="text/javascript" src="Scripts/Availability.js?V62"></script>--%>
    <script type="text/javascript" src="Scripts/Release/DTH_SCRIPT_3.2.0.2.js"></script>

    <title>Tassili Airlines طيران الطاسيلي</title>
    <script language="javascript" type="text/javascript">
        function wireUpEvents() {
            window.onbeforeunload = function () {
                var browser;
                if ($.browser.mozilla == true) {
                    if (window.screenY <= 90) {
                        CloseSession();
                        alert("The website was closing");
                    }
                }
                else {

                    if (window.screenTop <= 90) {
                        CloseSession();
                    }

                }

            }

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
      
      <!--Fading-->
      <input type="hidden" id="hdLang" name="hdLang" value="<%=tikAeroB2C.B2CSession.LanCode %>" />
      <div id="dvProgressBar" ></div>

      <!--progress bar-->
      <div id="dvFormHolder" ></div>
    
    <!--Start Error Box-->
    	  <!--Start Error Box-->
		<div id="dvMessageBox"  style="display:none">
			<div class="xouter">
				<div class="xcontainer">
					<div class="xinner LoadingBoxError">
							<div class="errortop"></div>
							<div class="errorcontent">
							<div class="ErrorBoxHeader">
								<img src="App_Themes/Default/Images/mainlogo.png" alt="Tassili" />
							</div>
							<div class="ErrorContent">
							<div id="dvMessageIcon" class="ErrorBoxMid">
								<img src="App_Themes/Default/Images/alert.png" alt=""/>
							</div>
							<div id="dvErrorMessage" class="ErrorBoxBot RedFontB">
								Error Text
							</div>
							</div>
							<div class="clear-all"></div>
                            <div class="AjaxButton" id="dvConfirmOK" style="display:none">
								<a href="#" onclick="return false;" class="defaultbutton">
									<span>
										<%=tikAeroB2C.Classes.Language.Value("Default_Value_12", "Confirm", _stdLanguage)%>
									</span>
								</a>
								<div class="clear-all"></div>
							</div>

                            <div class="AjaxButton">
								<a href="#" onclick="return false;" class="defaultbutton">
									<span id="dvMessageButtonText" onclick="CloseMessageBox();">
										<%=tikAeroB2C.Classes.Language.Value("Default_Value_11", "OK", _stdLanguage)%>
									</span>
								</a>
								<div class="clear-all"></div>
							</div>

							<div class="clear-all"></div>
							</div>
							<div class="errorbottom"></div>
					</div>
				</div>
			</div>
		</div>
      <!--End Error Box-->
    <!--End Error Box-->
    <div style="display:none">
    <div id="dvLoad"  style="display:none">
        <div class="xouter">
          <div class="xcontainer">
            <div class="xinner LoadingBox">
              <div class="loadingtext">
                <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_27", "Now processing your request...", _stdLanguage)%>
              </div>
              <div class="clear-all" ></div>

              <div class="LoadingBoxMid">
                <img src="App_Themes/Default/Images/loading.gif" alt=""/>
              </div>
              <div class="LoadingBoxFoot"></div>

              <div class="clear-all"></div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="dvLoadBar"  style="display: none;">
        <div class="ProgressBarBoxYellow">
            <div class="ProgressBarBoxYellow1">
            </div>
        </div>
        <div class="ProgressBarHeaderTxt">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_51", "Thank you for booking at flygermania.de", _stdLanguage)%>
        </div>
        <div class="ProgressBarAnimate">
            <img src="App_Themes/Default/Images/progressbar.gif" width="278" height="21" alt="" />
        </div>
        <div class="ProgressBarBodyTxt">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_52", "Please wait while we process your booking or payment process.<br />This will take approximately 1 minute  and <br />during this time you should not use any of the buttons on your browser.", _stdLanguage)%>
        </div>
        <div class="ProgressBarBoxYellow">
            <div class="ProgressBarBoxYellow3">
            </div>
        </div>
    </div>
    <asp:ScriptManager ID="smService" runat="server" ScriptMode="Release" EnablePartialRendering="false">
        <Services>
          <asp:ServiceReference Path="WebService/B2cService.asmx" />
          <asp:ServiceReference Path="WebService/WebUIRender.asmx" />
        </Services>
    </asp:ScriptManager>
    <div id="dvAvailabilitySearch">
        <div class="FlightSelectionBoxDefault">
            <AvailabilitySearch:AvailabilitySearch runat="server" ID="AvailabilitySearch1" />
			
        </div>
    </div>
    </form>
    <!--START script for SearchAvailability calendar Only--> 
    <script language="javascript" type="text/javascript">

        var ResultType = "<%= ResultType %>";
        var CalendarToControls = "<%= CalendarToControls %>";
        var DateFormat = "<%= DateFormat %>";

        $(function () {
            $('#calendar').daterangepicker().click(function () {
                $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
            });

            $('#calendar2').daterangepicker().click(function () {
                $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
            });

        });

        InitializeWaterMark("AvailabilitySearch1_optOrigin", objLanguage.default_value_7, "select");
        InitializeWaterMark("AvailabilitySearch1_optDestination", objLanguage.default_value_7, "select");
    </script>
    <script type="text/javascript" src="Scripts/JQuery/jquery-ui-1.7.1.custom.min.js?V60"></script>
    <script type="text/javascript" src="Scripts/JQuery/daterangepicker.jQuery.js?V60"></script>
     <!--END script for SearchAvailability calendar Only-->
</body>
</html>
