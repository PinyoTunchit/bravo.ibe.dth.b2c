<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no" encoding="utf-8" omit-xml-declaration="yes"/>
	<xsl:template match="/">
    <input type="hidden" id="hdBooknowTimeLimit" value="{ArrayOfAgent/Setting/FormOfPayment/ATM/timelimit_hours}" />
    <xsl:for-each select="ArrayOfAgent/Agent">

			
			<!--<xsl:if test="b2b_post_paid_flag = 1 and number(../Setting/balance) &lt;= 300000  and ../Setting/FormOfPayment/ATM/timelimit_flag = 0 and ../Setting/origin_country = 'JP'">-->
      <xsl:if test="b2b_post_paid_flag = 1 ">
      <div class="PaymentTab" id="dvBookNowTab">
					<div id="dvcTLater" class="BookNowPayLaterDisable" onclick="javascript:showhidelayer('BookNow');">
						<div id="crTabNoPayment">
							<xsl:value-of select="tikLanguage:get('Booking_Step_5_33','Book Now Pay Later')" />
						</div>
					</div>
				</div>
			</xsl:if>


      <xsl:if test="issue_ticket_flag = 1">

        <!--Order Default Voucher-->
        <xsl:if test="agency_payment_type_rcd = 'VOUCHER'">
          <xsl:if test="b2b_voucher_payment_flag = 1 and (../Setting/FormOfPayment/VOUCHER/timelimit_flag = 0 or ../Setting/FormOfPayment/VOUCHER/timelimit_flag = 2)">
            <div class="PaymentTab" id="dvVoucherTab">
              <div id="dvcTVoucher" class="Voucher" onclick="javascript:showhidelayer('Voucher')">
                <div id ="crTabVoucher">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_26','Voucher')" />
                </div>
              </div>
            </div>
          </xsl:if>

          <xsl:if test="(b2b_credit_card_payment_flag = 1 and (../Setting/FormOfPayment/CC/timelimit_flag = 0 or ../Setting/FormOfPayment/CC/timelimit_flag = 2)) and /ArrayOfAgent/Setting/currency_rcd != 'DZD'">
            <div class="PaymentTab" id="dvCCTab">
              <div id="dvcTCreditCard" class="CreditCardDisable" onclick="javascript:showhidelayer('CreditCard');activateCreditCardControl();">
                <div id="crTabCard">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_2','Credit Card')" />
                </div>
              </div>
            </div>
          </xsl:if>
        </xsl:if>

        <!--Order Default Credit Card-->
        <xsl:if test="agency_payment_type_rcd = 'CC'">

          <xsl:if test="b2b_voucher_payment_flag = 1 and (../Setting/FormOfPayment/VOUCHER/timelimit_flag = 0 or ../Setting/FormOfPayment/VOUCHER/timelimit_flag = 2)">
            <div class="PaymentTab" id="dvVoucherTab">
              <div id="dvcTVoucher" class="VoucherDisable" onclick="javascript:showhidelayer('Voucher')">
                <div id ="crTabVoucher">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_26','Voucher')" />
                </div>
              </div>
            </div>
          </xsl:if>


          <xsl:if test="(b2b_credit_card_payment_flag = 1 and (../Setting/FormOfPayment/CC/timelimit_flag = 0 or ../Setting/FormOfPayment/CC/timelimit_flag = 2)) and /ArrayOfAgent/Setting/currency_rcd != 'DZD' ">
            <div class="PaymentTab first" id="dvCCTab">
              <div id="dvcTCreditCard" class="CreditCard" onclick="javascript:showhidelayer('CreditCard');activateCreditCardControl();">
                <div id="crTabCard">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_2','Credit Card')" />
                </div>
              </div>
            </div>
          </xsl:if>

        </xsl:if>

        <!--Order Default Voucher-->
        <xsl:if test="agency_payment_type_rcd = 'VOUCHER' and (../Setting/FormOfPayment/VOUCHER/timelimit_flag = 0 or ../Setting/FormOfPayment/VOUCHER/timelimit_flag = 2)">
          <xsl:if test="b2b_voucher_payment_flag = 1 and (../Setting/FormOfPayment/VOUCHER/timelimit_flag = 0 or ../Setting/FormOfPayment/VOUCHER/timelimit_flag = 2)">
            <div class="PaymentTab" id="dvVoucherTab">
              <div id="dvcTVoucher" class="Voucher" onclick="javascript:showhidelayer('Voucher')">
                <div id ="crTabVoucher">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_26','Voucher')" />
                </div>
              </div>
            </div>
          </xsl:if>

          <xsl:if test="(b2b_credit_card_payment_flag = 1 and (../Setting/FormOfPayment/CC/timelimit_flag = 0 or ../Setting/FormOfPayment/CC/timelimit_flag = 2)) and /ArrayOfAgent/Setting/currency_rcd != 'DZD' ">
            <div class="PaymentTab" id="dvCCTab">
              <div id="dvcTCreditCard" class="CreditCardDisable" onclick="javascript:showhidelayer('CreditCard');activateCreditCardControl();">
                <div id="crTabCard">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_2','Credit Card')" />
                </div>
              </div>
            </div>
          </xsl:if>
        </xsl:if>

        <xsl:if test="agency_payment_type_rcd = ''">
          <xsl:if test="b2b_voucher_payment_flag = 1 and (../Setting/FormOfPayment/VOUCHER/timelimit_flag = 0 or ../Setting/FormOfPayment/VOUCHER/timelimit_flag = 2)">
            <div class="PaymentTab" id="dvVoucherTab">
              <div id="dvcTVoucher" class="Voucher" onclick="javascript:showhidelayer('Voucher')">
                <div id ="crTabVoucher">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_26','Voucher')" />
                </div>
              </div>
            </div>
          </xsl:if>

          <xsl:if test="(b2b_credit_card_payment_flag = 1 and (../Setting/FormOfPayment/CC/timelimit_flag = 0 or ../Setting/FormOfPayment/CC/timelimit_flag = 2)) and /ArrayOfAgent/Setting/currency_rcd != 'DZD' ">
            <div class="PaymentTab" id="dvCCTab">
              <div id="dvcTCreditCard" class="CreditCardDisable" onclick="javascript:showhidelayer('CreditCard');activateCreditCardControl();">
                <div id="crTabCard">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_5_2','Credit Card')" />
                </div>
              </div>
            </div>
          </xsl:if>
        </xsl:if>
        
      </xsl:if>


      <!--EFT-->
      <xsl:if test="b2b_eft_flag = 1 and (../Setting/FormOfPayment/EFT/timelimit_flag = 0 or ../Setting/FormOfPayment/EFT/timelimit_flag = 2)">
        <div class="PaymentTab" id="dvEFTTab">
          <div id="dvcTEFT" class="ELFDisable" onclick="javascript:showhidelayer('eft');">
            <div id="crTabNoPayment">
              <xsl:value-of select="tikLanguage:get('Booking_Step_5_xx','EFT')" />
            </div>
          </div>
        </div>
      </xsl:if>

      <!--Satim Card-->
      <xsl:if test="/ArrayOfAgent/Setting/currency_rcd = 'DZD' ">
        <div class="PaymentTab" id="dvSatimTab">
          <div id="dvcTSatim" class="SatimPaymentDisable" onclick="javascript:showhidelayer('STM');">
            <div id="crTabNoPayment">
              <xsl:value-of select="tikLanguage:get('Booking_Step_5_199','Satim')" />
            </div>
          </div>
        </div>
      </xsl:if>
      
    </xsl:for-each>
	</xsl:template>

</xsl:stylesheet>

