<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>
  <xsl:key name="availability_date_group" match="FlightGroup/flight" use="departure_date"/>

  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <!--<xsl:value-of select="substring($Date,1,4)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,9,2)"/>-->
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>

    </xsl:if>
  </xsl:template>

  <xsl:template name="DayMonthFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeDurationFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:variable name="calHour" select="$Time div 60" />
    <xsl:variable name="calMinute" select="$Time mod 60" />

    <xsl:value-of select="floor($calHour)" />&#160;
    <xsl:value-of select="tikLanguage:get('Booking_Step_2_28','Hour')" />&#160;
    <xsl:value-of select="format-number($calMinute, '0')" />&#160;
    <xsl:value-of select="tikLanguage:get('Booking_Step_2_29','Minute')" />&#160;
  </xsl:template>

  <xsl:template match="/">
    <xsl:variable name="flight_type" select="FlightGroup/flight_type" />
    <xsl:variable name="fare_type_rcd" select="FlightGroup/fare_type_rcd" />
    <xsl:variable name="selected_date" select="substring(FlightGroup/departure_date, 1, 10)" />
    <xsl:variable name="fare" select="FlightGroup/fare" />
    <xsl:variable name="currency_rcd" select="FlightGroup/currency_rcd" />
    <xsl:variable name="number_of_adult" select="FlightGroup/number_of_adult" />
    <xsl:variable name="number_of_child" select="FlightGroup/number_of_child" />
    <xsl:variable name="number_of_infant" select="FlightGroup/number_of_infant" />

    <div class="FlightAvailability">
      <div class="WrapperTBLYourFlight">
        <div class="BoxtopleftCorner"></div>
        <div class="Boxtopmiddle">
          <xsl:choose>
            <xsl:when test="$flight_type = 'Outward'">
              <xsl:value-of select="tikLanguage:get('Booking_Step_2_2','Departure flight')" />&#160;
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="tikLanguage:get('Booking_Step_2_23','Return flight')" />&#160;
            </xsl:otherwise>
          </xsl:choose>

          <span class="TextFlightName">
            <xsl:value-of select="concat(FlightGroup/origin_name, ' (', FlightGroup/origin_rcd, ')')" />
          </span>
          &#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_10','To')" />&#160;
          <span class="TextFlightName">
            <xsl:value-of select="concat(FlightGroup/destination_name, ' (', FlightGroup/destination_rcd, ')')" />
          </span>
        </div>

        <div id="{$flight_type}_dvShowmonthview" class="showmonthview">
          <a href="javascript:CallLowfare()">
            <div class="monthviewlink" title="{tikLanguage:get('Booking_Step_2_57','Month View')}">
              <xsl:value-of select="tikLanguage:get('Booking_Step_2_57','Month View')" />
            </div>
          </a>
        </div>

        <div class="BoxtoprightCorner"></div>
      </div>

      <div class="LoadingDisable">
        <div id="{$flight_type}_loading" class="loadingImage">
          <img src="App_Themes/Default/Images/loading.gif" alt="" />
        </div>
      </div>

      <xsl:variable name="tab_departure_date" select="concat(substring(FlightGroup/departure_date,1,4), substring(FlightGroup/departure_date,6,2), substring(FlightGroup/departure_date,9,2))" />
      <div id="graygradient">
        <div class="AvailabilitySearch">
          <div class="WrapperTBLSelectNewFlight">
            <xsl:variable name="DatePrevious" select="JCode:getDateAdd($tab_departure_date, -3)" />
            <div class="WrapperFlightDate">
              <div id="{$flight_type}_DatePrevious" class="dateback">
                <a href="javascript:SearchSingleFlight('{$DatePrevious}', '{$flight_type}');">
                  <img src="App_Themes/Default/Images/dateback.png" alt="{tikLanguage:get('Booking_Step_2_46','Previous 3 days')}" title="{tikLanguage:get('Booking_Step_2_46','Previous 3 days')}" />
                </a>
              </div>

              <div class="showdategroup">
                <xsl:variable name="DateM1" select="JCode:getDateAdd($tab_departure_date, -1)" />
                <div class="showdate">
                  <div class="dateleft"></div>
                  <div class="datecontent"></div>
                  <div class="dateright"></div>
                  <div class="clear-all"></div>
                  <div class="flighttime">
                    <xsl:variable name="lowest_fare_tab">
                      <xsl:choose>
                        <xsl:when test="count(FlightGroup/flight[concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2)) = $DateM1]/fare/group) > 0">
                          <xsl:for-each select="FlightGroup/flight[concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2)) = $DateM1]/fare/group[total_adult_fare != 0][full_flight_flag = 0][class_open_flag = 1][close_web_sales = 0]">
                            <xsl:sort select="adult_fare" data-type="number"/>
                            <xsl:if test="position() = 1">
                              <xsl:value-of select="total_adult_fare"/>
                            </xsl:if>
                          </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>N/A</xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>

                    <xsl:attribute name="onClick">
                      SearchSingleFlight('<xsl:value-of select="$DateM1"/>', '<xsl:value-of select="$flight_type"/>');
                    </xsl:attribute>

                    <xsl:value-of select="JCode:getFormatSelectdate($DateM1,tikLanguage:GetAbbrivateDay(0), 
					                                                                  tikLanguage:GetAbbrivateDay(1),
					                                                                  tikLanguage:GetAbbrivateDay(2),
					                                                                  tikLanguage:GetAbbrivateDay(3),
					                                                                  tikLanguage:GetAbbrivateDay(4),
					                                                                  tikLanguage:GetAbbrivateDay(5), 
					                                                                  tikLanguage:GetAbbrivateDay(6))"/>

                  </div>
                </div>

                <xsl:variable name="Date1" select="JCode:getDateAdd($tab_departure_date, 0)" />
                <input type="hidden" id="{$flight_type}_hdSelectedDate" value="{$Date1}" name="hdSelectedDate" />
                <div class="showdateselect">
                  <div class="dateouterbox">
                    <div class="airfareleftbox"></div>
                    <div class="airfaremiddle dateselectcontent"></div>
                    <div class="airfarerightbox"></div>
                    <div class="clear-all"></div>
                    <div class="flighttimeSelect">
                      <xsl:variable name="lowest_fare_tab">
                        <xsl:choose>
                          <xsl:when test="count(FlightGroup/flight[concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2)) = $Date1]/fare/group) > 0">
                            <xsl:for-each select="FlightGroup/flight[concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2)) = $Date1]/fare/group[total_adult_fare != 0][full_flight_flag = 0][class_open_flag = 1][close_web_sales = 0]">
                              <xsl:sort select="adult_fare" data-type="number"/>
                              <xsl:if test="position() = 1">
                                <xsl:value-of select="total_adult_fare"/>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:when>
                          <xsl:otherwise>N/A</xsl:otherwise>
                        </xsl:choose>

                      </xsl:variable>

                      <xsl:value-of select="JCode:getFormatSelectdate($Date1,tikLanguage:GetAbbrivateDay(0), 
						                                                                 tikLanguage:GetAbbrivateDay(1),
						                                                                 tikLanguage:GetAbbrivateDay(2),
						                                                                 tikLanguage:GetAbbrivateDay(3),
						                                                                 tikLanguage:GetAbbrivateDay(4),
						                                                                 tikLanguage:GetAbbrivateDay(5), 
						                                                                 tikLanguage:GetAbbrivateDay(6))"/>

                      <div class="price">
                        <xsl:choose>
                          <xsl:when test="$lowest_fare_tab = 'N/A'">
                            N/A
                          </xsl:when>
                          <xsl:when test="string-length($lowest_fare_tab) > 0">
                            <!--<xsl:value-of select="format-number($lowest_fare_tab,'#,##0.00')"/>~-->
                          </xsl:when>

                          <xsl:otherwise>
                            <xsl:value-of select="tikLanguage:get('Booking_Step_2_12','Full')" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </div>
                    </div>
                    <div class="clear-all"></div>
                    <div class="airfareleftbottombox"></div>
                    <div class="airfaremiddlebottom dateselectcontent"></div>
                    <div class="airfarerightbottombox"></div>
                  </div>
                </div>

                <xsl:variable name="DateA1" select="JCode:getDateAdd($tab_departure_date, 1)" />
                <div class="showdate">
                  <div class="dateleft"></div>
                  <div class="datecontent"></div>
                  <div class="dateright"></div>
                  <div class="clear-all"></div>
                  <div class="flighttime">

                    <xsl:variable name="lowest_fare_tab">
                      <xsl:choose>
                        <xsl:when test="count(FlightGroup/flight[concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2)) = $DateA1]/fare/group) > 0">
                          <xsl:for-each select="FlightGroup/flight[concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2)) = $DateA1]/fare/group[total_adult_fare != 0][full_flight_flag = 0][class_open_flag = 1][close_web_sales = 0]">
                            <xsl:sort select="adult_fare" data-type="number"/>
                            <xsl:if test="position() = 1">
                              <xsl:value-of select="total_adult_fare"/>
                            </xsl:if>
                          </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>N/A</xsl:otherwise>
                      </xsl:choose>

                    </xsl:variable>

                    <xsl:attribute name="onClick">
                      SearchSingleFlight('<xsl:value-of select="$DateA1"/>', '<xsl:value-of select="$flight_type"/>');
                    </xsl:attribute>

                    <xsl:value-of select="JCode:getFormatSelectdate($DateA1,tikLanguage:GetAbbrivateDay(0), 
				                                                                  tikLanguage:GetAbbrivateDay(1),
				                                                                  tikLanguage:GetAbbrivateDay(2),
				                                                                  tikLanguage:GetAbbrivateDay(3),
				                                                                  tikLanguage:GetAbbrivateDay(4),
				                                                                  tikLanguage:GetAbbrivateDay(5), 
				                                                                  tikLanguage:GetAbbrivateDay(6))"/>

                  </div>
                </div>
              </div>

              <div class="datenext">
                <xsl:variable name="DateNext" select="JCode:getDateAdd($tab_departure_date, 3)" />
                <a href="javascript:SearchSingleFlight('{$DateNext}', '{$flight_type}');">
                  <img src="App_Themes/Default/Images/datenext.png" alt="{tikLanguage:get('Booking_Step_2_45','Next 3 days')}" title="{tikLanguage:get('Booking_Step_2_45','Next 3 days')}" />
                </a>
              </div>

              <div class="clear-all"></div>
            </div>
          </div>
          <div class="clear-all"></div>

          <div class="FirstCOL"></div>
          <div class="FirstContent"></div>
          <div class="LastCOL"></div>

          <div class="clear-all"></div>

          <table class="TBLYourFlight">
            <tr class="TableHead Header">
              <td class="HeadCOL1 Last">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_3','Flight')" />
              </td>
              <td class="HeadCOL4 Last">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_5','Departure')" /> / <xsl:value-of select="tikLanguage:get('Booking_Step_2_6','Arrival')" />
              </td>

              <td class="HeadCOL6 Last">

                <div class="demoB">

                  <xsl:value-of select="tikLanguage:get('Booking_Step_2_16','Column 1')" />

                </div>
              </td>
              <td class="HeadCOL7 Last">
                <div class="demoC">


                  <xsl:value-of select="tikLanguage:get('Booking_Step_2_17','Column 2')" />

                </div>

              </td>
              <td class="HeadCOL8 Last">
                <div class="demoC">


                  <xsl:value-of select="tikLanguage:get('Booking_Step_2_18','Column 3')" />

                </div>

              </td>
            </tr>
            <xsl:if test="count(FlightGroup/flight) > 0">
              <xsl:for-each select="FlightGroup/flight[substring(departure_date, 1, 10) = $selected_date]">
                <xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
                <xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
                <xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
                <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>

                <xsl:variable name = "flight_position" select="position()" />
                <!--Display fare group information-->
                <xsl:variable name="flight_id" select="flight_id" />
                <xsl:variable name="airline_rcd" select="airline_rcd" />
                <xsl:variable name="flight_number" select="flight_number" />
                <xsl:variable name="transit_flight_id" select="transit_flight_id" />
                <xsl:variable name="transit_airline_rcd" select="transit_airline_rcd" />
                <xsl:variable name="transit_flight_number" select="transit_flight_number" />
                <xsl:variable name="departure_date" select="concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2))" />
                <xsl:variable name="planned_departure_time" select="planned_departure_time" />
                <xsl:variable name="planned_arrival_time" select="planned_arrival_time" />
                <xsl:variable name="transit_airport_rcd" select="transit_airport_rcd" />
                <xsl:variable name="transit_departure_date" select="concat(substring(transit_departure_date,1,4), substring(transit_departure_date,6,2), substring(transit_departure_date,9,2))" />
                <xsl:variable name="transit_planned_departure_time" select="transit_planned_departure_time" />
                <xsl:variable name="transit_planned_arrival_time" select="transit_planned_arrival_time" />
                <xsl:variable name="arrival_date" select="concat(substring(arrival_date,1,4), substring(arrival_date,6,2), substring(arrival_date,9,2))" />
                <xsl:variable name="transit_arrival_date" select="concat(substring(transit_arrival_date,1,4), substring(transit_arrival_date,6,2), substring(transit_arrival_date,9,2))" />
                <xsl:variable name="origin_rcd" select="origin_rcd" />
                <xsl:variable name="destination_rcd" select="destination_rcd" />
                <xsl:variable name="transit_points" select="transit_points" />
                <xsl:variable name="transit_points_name" select="transit_points_name" />
                <xsl:variable name="aircraft_type_rcd" select="aircraft_type_rcd" />
                <xsl:variable name="transit_aircraft_type_rcd" select="transit_aircraft_type_rcd" />

                <tr class="FlightInformation{position() mod 2}">
                  <td class="BodyCOL1" title="{flight_comment}">
                    <div class="FlightdurationDetail Oldflight">

                      <div class="Flightdurationtooltip">

                        <div class="fg-tooltip ui-widget ui-widget-tooltip ui-corner-all" style="display: none;">

                          <ul class="tooltipcontent">
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_3','Flight')" />&#160;
                              <xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
                            </li>
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_5','Departure')" />&#160;:&#160;
                              <xsl:call-template name="TimeFormat">
                                <xsl:with-param name="Time" select="planned_departure_time"></xsl:with-param>
                              </xsl:call-template>
                            </li>
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_6','Arrival')" />&#160;:&#160;
                              <xsl:call-template name="TimeFormat">
                                <xsl:with-param name="Time" select="$planned_arrival_time"></xsl:with-param>
                              </xsl:call-template>
                            </li>
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_26','Flight Duration')" />&#160;:&#160;
                              <xsl:call-template name="TimeDurationFormat">
                                <xsl:with-param name="Time" select="flight_duration"></xsl:with-param>
                              </xsl:call-template>
                            </li>
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_30','Air Craft Type')" />&#160;:&#160;
                              <xsl:value-of select="aircraft_type_rcd" />
                            </li>

                          </ul>

                          <div class="fg-tooltip-pointer-down ui-widget-tooltip">
                            <div class="fg-tooltip-pointer-down-inner"></div>
                          </div>
                        </div>

                        <p class="demoText">
                          <a href="#" class="tooltip" onclick="return false;">
                            <xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
                          </a>
                        </p>

                      </div>
                    </div>

                    <xsl:if test="count(transit_flight_id) > 0 and string-length(transit_flight_id) > 0">

                      <div class="Transitflighttooltip">

                        <div class="fg-tooltip ui-widget ui-widget-tooltip ui-corner-all" style="display: none;">

                          <ul class="tooltipcontent">
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_3','Flight')" />&#160;
                              <xsl:value-of select="transit_airline_rcd" />&#160;<xsl:value-of select="transit_flight_number" />
                            </li>
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_5','Departure')" />&#160;:&#160;
                              <xsl:call-template name="TimeFormat">
                                <xsl:with-param name="Time" select="transit_planned_departure_time"></xsl:with-param>
                              </xsl:call-template>
                            </li>
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_6','Arrival')" />&#160;:&#160;
                              <xsl:call-template name="TimeFormat">
                                <xsl:with-param name="Time" select="transit_planned_arrival_time"></xsl:with-param>
                              </xsl:call-template>
                            </li>
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_26','Flight Duration')" />&#160;:&#160;
                              <xsl:call-template name="TimeDurationFormat">
                                <xsl:with-param name="Time" select="transit_flight_duration"></xsl:with-param>
                              </xsl:call-template>
                            </li>
                            <li>
                              <xsl:value-of select="tikLanguage:get('Booking_Step_2_30','Air Craft Type')" />&#160;:&#160;
                              <xsl:value-of select="transit_aircraft_type_rcd" />
                            </li>

                          </ul>

                          <div class="fg-tooltip-pointer-down ui-widget-tooltip">
                            <div class="fg-tooltip-pointer-down-inner"></div>
                          </div>
                        </div>

                        <p class="demoText">
                          <a href="#" class="tooltip" onclick="return false;">
                            <xsl:value-of select="transit_airline_rcd"/>&#160;<xsl:value-of select="transit_flight_number "/>
                          </a>
                        </p>
                      </div>
                    </xsl:if>
                  </td>

                  <td class="BodyCOL4">

                    <div class="FlightDuration">
                      <!-- Dept Flight -->
                      <div class="FlightdurationDetail Deptflight">
                        <xsl:value-of select="tikLanguage:get('Booking_Step_2_24','Dept')" />&#160;
                        <xsl:call-template name="DayMonthFormat">
                          <xsl:with-param name="Date" select="departure_date"></xsl:with-param>
                        </xsl:call-template>&#160;
                        <xsl:call-template name="TimeFormat">
                          <xsl:with-param name="Time" select="planned_departure_time"></xsl:with-param>
                        </xsl:call-template>&#160;
                        <xsl:value-of select="origin_name"/>
                      </div>

                      <div class="clear-all"></div>

                      <!--Show Transit Flight-->
                      <xsl:if test="count(transit_flight_id) > 0 and string-length(transit_flight_id) > 0">
                        <div class="FlightdurationDetail ArrivOldflight">
                          <xsl:value-of select="tikLanguage:get('Booking_Step_2_25','Arriv')" />&#160;

                          <xsl:call-template name="DayMonthFormat">
                            <xsl:with-param name="Date" select="departure_date"></xsl:with-param>
                          </xsl:call-template>&#160;

                          <xsl:call-template name="TimeFormat">
                            <xsl:with-param name="Time" select="$planned_arrival_time"></xsl:with-param>
                          </xsl:call-template>&#160;
                          <xsl:value-of select="transit_name"/>&#160;
                        </div>

                        <div class="clear-all"></div>

                        <div class="FlightdurationDetail DeptNewflight">
                          <xsl:value-of select="tikLanguage:get('Booking_Step_2_24','Dept')" />&#160;
                          <xsl:call-template name="DayMonthFormat">
                            <xsl:with-param name="Date" select="departure_date"></xsl:with-param>
                          </xsl:call-template>&#160;

                          <xsl:call-template name="TimeFormat">
                            <xsl:with-param name="Time" select="transit_planned_departure_time"></xsl:with-param>
                          </xsl:call-template>&#160;
                          <xsl:value-of select="transit_name"/>&#160;
                        </div>
                        <div class="clear-all"></div>
                      </xsl:if>

                      <!--Show Married Flight-->
                      <xsl:if test="count(transit_points) > 0 and string-length(transit_points) > 0">
                        <div class="FlightdurationDetail VIAflight">
                          <xsl:value-of select="tikLanguage:get('Booking_Step_2_15','VIA')" />&#160;
                          <xsl:value-of select="transit_points_name"/>&#160;
                        </div>
                        <div class="clear-all"></div>
                      </xsl:if>


                      <!-- Arriv Flight -->
                      <div class="FlightdurationDetail Arrivflight">
                        <xsl:value-of select="tikLanguage:get('Booking_Step_2_25','Arriv')" />&#160;
                        <xsl:choose>
                          <xsl:when test="count(transit_flight_id) > 0 and string-length(transit_flight_id) > 0">
                            <xsl:call-template name="DayMonthFormat">
                              <xsl:with-param name="Date" select="transit_arrival_date"></xsl:with-param>
                            </xsl:call-template>&#160;
                            <xsl:call-template name="TimeFormat">
                              <xsl:with-param name="Time" select="transit_planned_arrival_time"></xsl:with-param>
                            </xsl:call-template>&#160;
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:call-template name="DayMonthFormat">
                              <xsl:with-param name="Date" select="arrival_date"></xsl:with-param>
                            </xsl:call-template>&#160;
                            <xsl:call-template name="TimeFormat">
                              <xsl:with-param name="Time" select="$planned_arrival_time"></xsl:with-param>
                            </xsl:call-template>&#160;
                          </xsl:otherwise>
                        </xsl:choose>

                        <xsl:value-of select="destination_name"/>
                      </div>
                    </div>
                  </td>

                  <xsl:for-each select="../flight[flight_id = $flight_id][transit_flight_id = $transit_flight_id]/fare/group">
                    <xsl:if test="$fare_type_rcd = 'POINT'">
                      <td class="BodyCOL4">
                        <xsl:value-of select="redemption_points + transit_redemption_points"/>
                      </td>
                    </xsl:if>

                    <xsl:choose>
                      <xsl:when test="full_flight_flag = 1">
                        <td class="notshowflight">
                          <xsl:value-of select="tikLanguage:get('Booking_Step_2_12','Full')" />
                        </td>
                      </xsl:when>

                      <xsl:when test="class_open_flag = 0 and waitlist_open_flag = 0">
                        <td class="notshowflight">
                          <xsl:value-of select="tikLanguage:get('Booking_Step_2_12','Full')" />
                        </td>
                      </xsl:when>

                      <xsl:when test="class_open_flag = 0">
                        <td class="notshowflight">
                          <xsl:value-of select="tikLanguage:get('Booking_Step_2_13','Closed')" />
                        </td>
                      </xsl:when>

                      <xsl:when test="close_web_sales = 1">
                        <td class="notshowflight">
                          <xsl:value-of select="tikLanguage:get('Booking_Step_2_14','Call')" />
                        </td>
                      </xsl:when>

                      <xsl:otherwise>
                        <xsl:if test="string-length(flight_id) > 0">
                          <td class="BodyCOL{5 + position()}" id="{concat($flight_type, $flight_position, '_', position())}">
                            <xsl:variable name="total_tax" select="format-number(total_adult_fare - adult_fare, '###0.00')" />
                            <xsl:variable name="flight_param" select="concat('flight_id:',flight_id,'|',
														                                                  'fare_id:',fare_id,'|',
														                                                  'boarding_class_rcd:',boarding_class_rcd,'|',
														                                                  'booking_class_rcd:',booking_class_rcd,'|',
		                        						                                      'airline_rcd:',$airline_rcd,'|',
		                        						                                      'flight_number:',$flight_number,'|',
														                                                  'origin_rcd:',$origin_rcd,'|',
														                                                  'destination_rcd:',$destination_rcd,'|',
														                                                  'departure_date:',$departure_date,'|',
														                                                  'planned_departure_time:',$planned_departure_time,'|',
														                                                  'planned_arrival_time:',$planned_arrival_time,'|',
		                       							                                      'transit_airline_rcd:',$transit_airline_rcd,'|',
		                        						                                      'transit_flight_number:',$transit_flight_number,'|',
														                                                  'transit_airport_rcd:',$transit_airport_rcd,'|',
														                                                  'transit_boarding_class_rcd:',transit_boarding_class_rcd,'|',
														                                                  'transit_booking_class_rcd:',transit_booking_class_rcd,'|',
														                                                  'transit_flight_id:',$transit_flight_id,'|',
														                                                  'transit_fare_id:',transit_fare_id,'|',
		                        						                                      'transit_planned_departure_time:',$transit_planned_departure_time,'|',
														                                                  'transit_planned_arrival_time:',$transit_planned_arrival_time,'|',
														                                                  'total_tax:',$total_tax,'|',
														                                                  'adult_fare:',adult_fare,'|',
														                                                  'child_fare:',child_fare,'|',
														                                                  'infant_fare:',infant_fare,'|',
														                                                  'transit_departure_date:',$transit_departure_date, '|',
                                                                              'arrival_date:',$arrival_date, '|',
                                                                              'transit_arrival_date:',$transit_arrival_date, '|',
                                                                              'transit_points:',$transit_points ,'|' ,
                                                                              'aircraft_type_rcd:', $aircraft_type_rcd ,'|' ,
                                                                              'number_of_adult:',$number_of_adult, '|',
                                                                              'number_of_child:',$number_of_child, '|',
                                                                              'number_of_infant:',$number_of_infant, '|',
                                                                              'currency_rcd:',$currency_rcd, '|',
                                                                              'transit_aircraft_type_rcd:', $transit_aircraft_type_rcd
                                                                               )"/>
                            <input id="opt{concat($flight_type, $flight_position, '_', position())}" name="{$flight_type}" type="radio" value="{$flight_param}" onclick="GetQuoteSummary(this, '{$flight_type}', '{concat($flight_type, $flight_position, '_', position())}')"/>
                            <label for="opt{concat($flight_type, $flight_position, '_', position())}">
                              <xsl:if test="$fare = total_adult_fare">
                                <xsl:attribute name="class">Lowest</xsl:attribute>
                              </xsl:if>
                              <xsl:value-of select="format-number(total_adult_fare,'#,##0.00')"/>
                            </label>
                          </td>
                        </xsl:if>
                      </xsl:otherwise>
                    </xsl:choose>

                  </xsl:for-each>
                </tr>
              </xsl:for-each>
            </xsl:if>

            <xsl:if test="count(FlightGroup/flight) = 0">
              <tr>
                <td colspan="10">
                  <div class="noflight">
                    <xsl:value-of select="tikLanguage:get('Booking_Step_2_21','No flight found')" />.
                  </div>
                </td>
              </tr>
            </xsl:if>
          </table>
          <!--end Fare7Days-->

        </div>

        <div class="BottominnerBox SearchFlightBottomBox">
          <div class="innerBoxdownleftCorner"></div>
          <div class="innerBoxdownmiddleCorner"></div>
          <div class="innerBoxdownrightCorner"></div>
        </div>
        <div class="clear-all"></div>
      </div>
      <div class="clear-all"></div>
      <div class="BottomBox">
        <div class="BoxdownleftCorner"></div>
        <div class="BoxdownmiddleCorner"></div>
        <div class="BoxdownrightCorner"></div>
      </div>
      <div class="clear-all"></div>
    </div>
  </xsl:template>
  <msxsl:script implements-prefix="JCode" language="JavaScript">
    function getFormatSelectdate(strDate, strSun, strMon, strTue, strWed, strThu, strFri, strSat)
    {
    var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
    var weekday = new Array(strSun,strMon,strTue,strWed,strThu,strFri,strSat);

    return strDate.substring(6,8) + '/' + strDate.substring(4,6) + ' ' + '(' + weekday[dtDate.getDay()] + ')';
    }
    function GetDayOfWeek(strDate, strSun, strMon, strTue, strWed, strThu, strFri, strSat)
    {
    var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
    var weekday = new Array(strSun,strMon,strTue,strWed,strThu,strFri,strSat);

    return weekday[dtDate.getDay()];
    }
    function getDateAdd(strDate, addValue)
    {
    var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
    var strMonth;
    var strday;

    dtDate.setDate(dtDate.getDate()+(addValue))

    if ((dtDate.getMonth() + 1).toString().length == 1)
    {
    strMonth = '0' + (dtDate.getMonth() + 1).toString()
    }
    else
    {
    strMonth = (dtDate.getMonth() + 1).toString()
    }

    if (dtDate.getDate().toString().length == 1)
    {
    strday = '0' + dtDate.getDate().toString()
    }
    else
    {
    strday = dtDate.getDate().toString()
    }

    return (dtDate.getYear().toString() + '' + strMonth + '' + strday);
    }
  </msxsl:script>
</xsl:stylesheet>
