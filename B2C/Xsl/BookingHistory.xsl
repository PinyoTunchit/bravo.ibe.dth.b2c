<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							                xmlns:tikLanguage="tik:Language">
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>

    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,1,4)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,9,2)"/>
    </xsl:if>
  </xsl:template>
	<xsl:template name="paging" match="/">
		<xsl:variable name="page">
			<xsl:value-of select="count(/object/paging)"/>
		</xsl:variable>
		<table border="0" cellspacing="0" cellpadding="0" class="TBLLiveBooking">
      <xsl:choose>
        <xsl:when test="number($page) > 0">
          <tr>
            <td class="Paging">
              <div align="right">
                <xsl:choose>
                  <xsl:when test="count(/object/paging/pageindex) > 0">
                    <xsl:for-each select="/object/paging/pageindex">
                      <xsl:if test="/object/paging/selected!=position()">
                        <a>
                          <xsl:attribute name="href">
                            javascript:GetHistoryBooking('<xsl:value-of select="position()"/>');
                          </xsl:attribute>
                          <xsl:value-of select="position()"/>
                        </a>&#160;
                      </xsl:if>
                      <xsl:if test="/object/paging/selected=position()">
                        <xsl:value-of select="position()"/>&#160;
                      </xsl:if>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:if test="count(object/ClientBookings/ClientBooking) >= 3">
                      <a>
                        <xsl:attribute name="href">
                          javascript:GetHistoryBooking('<xsl:value-of select="0"/>');
                        </xsl:attribute>
                        <xsl:value-of select="tikLanguage:get('FFP_My_Booking_24','Show All')" />
                      </a>&#160;
                    </xsl:if>
                  </xsl:otherwise>
                </xsl:choose>
              </div>
            </td>
          </tr>
        </xsl:when>
        <xsl:otherwise>
          <tr>
            <td class="Paging">
              <div align="right">
                <xsl:if test="count(ClientBookings/ClientBooking) >= 3">
                  <a>
                    <xsl:attribute name="href">
                      javascript:GetHistoryBooking('<xsl:value-of select="1"/>');
                    </xsl:attribute>
                    <xsl:value-of select="tikLanguage:get('FFP_My_Booking_25','Hide')" />
                  </a>&#160;
                </xsl:if>
              </div>
            </td>
          </tr>
        </xsl:otherwise>
      </xsl:choose>
		</table>
	</xsl:template>
	
	<xsl:template match="/">
		<table border="0" cellspacing="0" cellpadding="0" class="TBLLiveBooking">
      <tr class="Header">
        <td class="HeadCOL1"></td>
        <td class="HeadCOL2">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_9','Bookings')" />
        </td>
        <td class="HeadCOL2">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_26','Booking Date')" />
        </td>
        <td class="HeadCOL3">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_11','First Segment')" />
        </td>
        <td class="HeadCOL4">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_27','Departure Date')" />
        </td>
        <td class="HeadCOL5 Last">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_28','Itinerary Detail')" />
        </td>
      </tr>
			<xsl:if test="count(//ClientBooking)!=0">
				<xsl:for-each select="//ClientBooking">
          <xsl:sort select="record_locator" data-type="text" order="descending"/>
					<tr class="Body">
						<td class="BodyCOL1">
							<xsl:value-of select="format-number(position(), '000')"/>
						</td>
						<td class="BodyCOL2">
							<xsl:value-of select="record_locator"/>
						</td>
            <td class="BodyCOL2">
              <xsl:call-template name="DateFormat">
                <xsl:with-param name="Date" select="create_date_time"></xsl:with-param>
              </xsl:call-template>
            </td>
            <td class="BodyCOL3">
              <xsl:value-of select="origin_rcd"/>-<xsl:value-of select="destination_rcd"/>
            </td>
            <td class="BodyCOL4">
              <xsl:call-template name="DateFormat">
                <xsl:with-param name="Date" select="departure_date"></xsl:with-param>
              </xsl:call-template>
            </td>
						<td class="BodyCOL5 Last">
							<div class="ArrowGray">
								<xsl:attribute name="onclick">
									LoadBookingDetail('<xsl:value-of select="booking_id"/>','<xsl:value-of select="tikLanguage:get('FFP_My_Booking_12','No booking found. This transaction might be added manually. Please contact airline for more detail.')" />')
								</xsl:attribute>
								
								<a class="viewbutton" title="View">
									<span><xsl:value-of select="tikLanguage:get('FFP_My_Booking_23','Show')" /></span>
								</a>
							</div>
						</td>
					</tr>
				</xsl:for-each>
        <xsl:call-template name="paging"></xsl:call-template>
			</xsl:if>
			<xsl:if test="count(//ClientBooking)=0">
				<tr>
					<td colspan="6" class="BodyCOL5 Last notfound">
			            <xsl:value-of select="tikLanguage:get('FFP_My_Booking_13','Booking not found')" />
					</td>
				</tr>
			</xsl:if>
		</table>
	</xsl:template>
</xsl:stylesheet>