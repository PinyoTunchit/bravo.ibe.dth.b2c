<?xml version="1.0" encoding="utf-8"?>
<!-- 
Project: Tassili Air
File name: fr_email.xsl
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:CharCode="http://www.tikaero.com/Printing">

	<xsl:import href="string.xsl"/>
	<xsl:import href="date-time.xsl"/>
	<xsl:import href="functions.xsl"/>

	<xsl:include href="fr_email_details.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:variable name="Debug">0</xsl:variable>

	<xsl:variable name="OTable">&lt;table style="width: 730px; margin: 0px 0px 0px 36px; height: 779px;"&gt;</xsl:variable>
	<xsl:variable name="CTable">&lt;/table&gt;</xsl:variable>

	<xsl:template match="/">
		<html>
			<!-- Header with StyleSheet-->
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<meta content="en-us" http-equiv="Content-Language"/>
				<meta http-equiv="Pragma" content="no-cache"/>
				<meta http-equiv="Expires" content="-1"/>
				<title>Tassili Air Itinerary</title>
				<xsl:call-template name="styleColor"></xsl:call-template>
				<style type="text/css">
					<!--/* Remove margins from the 'html' and 'body' tags, and ensure the page takes up full screen height */-->html, body 
	{
	height: 100%;
	margin: 0;
	padding: 0;
	}
	#page-background 
	{
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	}
	#content
	{
	position: relative;
	z-index: 1;
	padding: 10px;
	}</style>
			</head>
			<!--Body-->
			<body>
				<table>
					<xsl:if test="$Debug = 1">(Debug)</xsl:if>
					<xsl:if test="$Debug = 1">

						<span style="font-family:trebuchet ms; font-size:120%">Debug Version</span>
						<hr/>
					</xsl:if>
				</table>
				<!--<table cellspacing="1"  style="width: 759px;background-color: yellow; ">
	<tr>
		<td>******************************************************************
		FOR CHECK PAGE WIDTH
		        ******************************************************************
		</td>
	</tr>
</table>-->
				<!--<p>Main Form</p>-->
				<xsl:copy-of select="$ReportHeader"/>

				<!--Header-->
				<xsl:apply-templates select="Booking/Header"></xsl:apply-templates>
				<!--End Header-->

				<!--Passengers-->
				<xsl:apply-templates select="Booking/Passengers"></xsl:apply-templates>
				<!--End Passengers-->

				<!--FlightSegment-->
				<xsl:apply-templates select="Booking/Itinerary"></xsl:apply-templates>
				<!--End FlightSegment-->

				<!--Auxiliaries-->
				<xsl:apply-templates select="Booking/Remarks"></xsl:apply-templates>
				<!--End Auxiliaries-->

				<!--Tickets-->
				<xsl:apply-templates select="Booking/Tickets"></xsl:apply-templates>
				<!--End Tickets-->

				<!--Special Services-->
				<xsl:apply-templates select="Booking/SpecialServices"></xsl:apply-templates>
				<!--End Special Services-->

				<!--Quotes-->
				<xsl:apply-templates select="Booking/TicketQuotes"></xsl:apply-templates>
				<!--End Quotes-->

				<!--Payments-->
				<xsl:apply-templates select="Booking/Payments"></xsl:apply-templates>
				<!--End Payments-->

				<!--Filler -->
				<xsl:choose>
					<!-- case of only one page-->
					<xsl:when test="CharCode:getCounter() &lt;= $RowsPerPage">
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="$RowsPerPage - (CharCode:getCounter())"/>
						</xsl:call-template>
						<xsl:copy-of select="$ReportFooter"/>
					</xsl:when>
					<!-- case of more than one page-->
					<xsl:otherwise>
						<xsl:call-template name="Filler">
							<xsl:with-param name="fillercount" select="$RowsPerPage - (CharCode:getCounter() mod $RowsPerPage )"/>
						</xsl:call-template>
						<xsl:copy-of select="$ReportFooter"/>
					</xsl:otherwise>
				</xsl:choose>
				<!--End Filler -->

				<xsl:apply-templates select="Booking/Header/BookingHeader"></xsl:apply-templates>

				<!--</div>-->
			</body>
			<!--End Body-->
		</html>
	</xsl:template>
	<!--End Template match-->
	<!-- variable ReportHeader-->
	<xsl:variable name="ReportHeader">
		<table border="0" class="tableReportHeader" cellspacing="0">
			<tr>
				<td align="right">
					<img src="http://www.tikaero.com/xslimages/dth/DTH_Logo1.jpg" width="160"/>
				</td>
				<!--<td align="right">
					<img  src="http://www.tikaero.com/xslimages/dth/DTH_Logo2.jpg"  width="150"/>
					
				</td>-->
			</tr>
		</table>
		<br/>
	</xsl:variable>
	<!-- End variable ReportHeader-->
	<!-- variable ReportFooter-->
	<xsl:variable name="ReportFooter">
		<table class="tableReportFooter">
			<tr>
				<td>
					<!--<table>
						<tr>
							<td>
								<img alt="Smile" src="http://www.tikaero.com/XSLImages/srk/srk_f1.jpg" width="744"/>
							</td>
						</tr>
					</table>-->
				</td>
			</tr>
		</table>
	</xsl:variable>
	<!-- End variable ReportFooter-->
	<!-- Template Filler  (Insert blank line)-->
	<xsl:template name="Filler">
		<xsl:param name="fillercount" select="1"/>
		<xsl:if test="$fillercount &gt; 0">
			<!--<table cellspacing="0" class="grid-top">-->
			<table cellspacing="0" class="grid-main" style="border-top: none; border-bottom: .75pt solid white; border-left: none; table-layout:fixed; width: 673px; margin: 0px 0px 0px 36px;">
				<tr>
					<!--<td class="grid-even">-->
					<td align="right" class="grid-top" style="border-right: .0pt;">
						<!--<xsl:value-of select="position()"/>-->&#xA0;</td>
				</tr>
			</table>
			<xsl:call-template name="Filler">
				<xsl:with-param name="fillercount" select="$fillercount - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- End Template Filler-->

	<!-- variable Header-->
	<xsl:variable name="ReportHeader1">
		<xsl:apply-templates select="Booking/Header"></xsl:apply-templates>
	</xsl:variable>
	<!-- End variable Header-->
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="yes" url="..\case\A04YOO.xml" htmlbaseurl="" outputurl="" processortype="msxmldotnet" useresolver="no" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->