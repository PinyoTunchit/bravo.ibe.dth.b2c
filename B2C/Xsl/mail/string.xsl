<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
												 xmlns:doc="http://www.tikaero.com/xsl/documentation/1.0" 
												 xmlns:str="http://www.tikaero.com/string" 
												 extension-element-prefixes="doc str">
	<doc:reference xmlns="">
		<referenceinfo>
			<releaseinfo role="meta">$Id: string.xsl,v 1.13 2004/10/08 06:37:25 balls Exp $</releaseinfo>
			<author>
				<surname>Ball</surname>
				<firstname>Steve</firstname>
			</author>
			<copyright>
				<year>2002</year>
				<year>2001</year>
				<holder>Steve Ball</holder>
			</copyright>
		</referenceinfo>

		<title>String Processing</title>

		<partintro>
			<section>
				<title>Introduction</title>

				<para>This module provides templates for manipulating strings.</para>
			</section>
		</partintro>
	</doc:reference>

	<!-- Common string constants and datasets as XSL variables -->

	<!-- str:lower and str:upper contain pairs of lower and upper case
       characters. Below insanely long strings should contain the
       official lower/uppercase pairs, making this stylesheet working
       for every language on earth. Hopefully. -->
	<!-- These values are not enough, however. There are some
       exceptions, dealt with below. -->
	<xsl:variable name="xsltsl-str-lower"
	              select="'abcdefghijklmnopqrstuvwxyzµàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿāăąćĉċčďđēĕėęěĝğġģĥħĩīĭįıĳĵķĺļľŀłńņňŋōŏőœŕŗřśŝşšţťŧũūŭůűųŵŷźżžſƃƅƈƌƒƕƙơƣƥƨƭưƴƶƹƽƿǅǆǈǉǋǌǎǐǒǔǖǘǚǜǝǟǡǣǥǧǩǫǭǯǲǳǵǹǻǽǿȁȃȅȇȉȋȍȏȑȓȕȗșțȝȟȣȥȧȩȫȭȯȱȳɓɔɖɗəɛɠɣɨɩɯɲɵʀʃʈʊʋʒͅάέήίαβγδεζηθικλμνξοπρςστυφχψωϊϋόύώϐϑϕϖϛϝϟϡϣϥϧϩϫϭϯϰϱϲϵабвгдежзийклмнопрстуфхцчшщъыьэюяѐёђѓєѕіїјљњћќѝўџѡѣѥѧѩѫѭѯѱѳѵѷѹѻѽѿҁҍҏґғҕҗҙқҝҟҡңҥҧҩҫҭүұҳҵҷҹһҽҿӂӄӈӌӑӓӕӗәӛӝӟӡӣӥӧөӫӭӯӱӳӵӹաբգդեզէըթժիլխծկհձղճմյնշոչպջռսվտրցւփքօֆḁḃḅḇḉḋḍḏḑḓḕḗḙḛḝḟḡḣḥḧḩḫḭḯḱḳḵḷḹḻḽḿṁṃṅṇṉṋṍṏṑṓṕṗṙṛṝṟṡṣṥṧṩṫṭṯṱṳṵṷṹṻṽṿẁẃẅẇẉẋẍẏẑẓẕẛạảấầẩẫậắằẳẵặẹẻẽếềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹἀἁἂἃἄἅἆἇἐἑἒἓἔἕἠἡἢἣἤἥἦἧἰἱἲἳἴἵἶἷὀὁὂὃὄὅὑὓὕὗὠὡὢὣὤὥὦὧὰάὲέὴήὶίὸόὺύὼώᾀᾁᾂᾃᾄᾅᾆᾇᾐᾑᾒᾓᾔᾕᾖᾗᾠᾡᾢᾣᾤᾥᾦᾧᾰᾱᾳιῃῐῑῠῡῥῳⅰⅱⅲⅳⅴⅵⅶⅷⅸⅹⅺⅻⅼⅽⅾⅿⓐⓑⓒⓓⓔⓕⓖⓗⓘⓙⓚⓛⓜⓝⓞⓟⓠⓡⓢⓣⓤⓥⓦⓧⓨⓩａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ𐐨𐐩𐐪𐐫𐐬𐐭𐐮𐐯𐐰𐐱𐐲𐐳𐐴𐐵𐐶𐐷𐐸𐐹𐐺𐐻𐐼𐐽𐐾𐐿𐑀𐑁𐑂𐑃𐑄𐑅𐑆𐑇𐑈𐑉𐑊𐑋𐑌𐑍'"/>
	<xsl:variable name="xsltsl-str-upper"
	              select="'ABCDEFGHIJKLMNOPQRSTUVWXYZΜÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŸĀĂĄĆĈĊČĎĐĒĔĖĘĚĜĞĠĢĤĦĨĪĬĮIĲĴĶĹĻĽĿŁŃŅŇŊŌŎŐŒŔŖŘŚŜŞŠŢŤŦŨŪŬŮŰŲŴŶŹŻŽSƂƄƇƋƑǶƘƠƢƤƧƬƯƳƵƸƼǷǄǄǇǇǊǊǍǏǑǓǕǗǙǛƎǞǠǢǤǦǨǪǬǮǱǱǴǸǺǼǾȀȂȄȆȈȊȌȎȐȒȔȖȘȚȜȞȢȤȦȨȪȬȮȰȲƁƆƉƊƏƐƓƔƗƖƜƝƟƦƩƮƱƲƷΙΆΈΉΊΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΣΤΥΦΧΨΩΪΫΌΎΏΒΘΦΠϚϜϞϠϢϤϦϨϪϬϮΚΡΣΕАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏѠѢѤѦѨѪѬѮѰѲѴѶѸѺѼѾҀҌҎҐҒҔҖҘҚҜҞҠҢҤҦҨҪҬҮҰҲҴҶҸҺҼҾӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӸԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠḢḤḦḨḪḬḮḰḲḴḶḸḺḼḾṀṂṄṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦṨṪṬṮṰṲṴṶṸṺṼṾẀẂẄẆẈẊẌẎẐẒẔṠẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴỶỸἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾺΆῈΈῊΉῚΊῸΌῪΎῺΏᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾼΙῌῘῙῨῩῬῼⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩⅪⅫⅬⅭⅮⅯⒶⒷⒸⒹⒺⒻⒼⒽⒾⒿⓀⓁⓂⓃⓄⓅⓆⓇⓈⓉⓊⓋⓌⓍⓎⓏＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ𐐀𐐁𐐂𐐃𐐄𐐅𐐆𐐇𐐈𐐉𐐊𐐋𐐌𐐍𐐎𐐏𐐐𐐑𐐒𐐓𐐔𐐕𐐖𐐗𐐘𐐙𐐚𐐛𐐜𐐝𐐞𐐟𐐠𐐡𐐢𐐣𐐤𐐥'"/>
	<xsl:variable name="xsltsl-str-digits" select="'0123456789'"/>
	<!-- space (#x20) characters, carriage returns, line feeds, or tabs. -->
	<xsl:variable name="xsltsl-str-ws" select="' &#x9;&#xD;&#xA;'"/>

	<xsl:template name="AddrFormat">
		<xsl:param name="value"/>
		<xsl:choose>
			<xsl:when test="string-length($value) != 0">
				<xsl:value-of select="$value"/>&#xA0;</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="str:de-title">
		<xsl:param name="text"/>
		<xsl:choose>
			<xsl:when test="($text='MR') or ($text='MSTR')">   
					<xsl:text>Herr</xsl:text>
			</xsl:when>
			<xsl:when test="($text='MRS') or ($text='MS') or ($text='MISS')">   
					<xsl:text>Frau</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="FloatFormat">
		<xsl:param name="Input"/>
		<xsl:variable name="temppoint">
			<xsl:value-of select="substring-after($Input,'.')"/>
		</xsl:variable>
		<xsl:if test="string-length( substring-before($Input,'.'))=0">
			<xsl:value-of select="$Input"/>.00</xsl:if>
		<xsl:if test=" string-length(substring-before($Input,'.'))&gt; 0">
			<xsl:choose>
				<xsl:when test="string-length($temppoint) = 0">
					<xsl:value-of select="substring-before($Input,'.')"/>.00</xsl:when>
				<xsl:when test="string-length($temppoint) = 1">
					<xsl:value-of select="substring-before($Input,'.')"/>.<xsl:value-of select="$temppoint"/>0</xsl:when>
				<xsl:when test="string-length($temppoint) = 2">
					<xsl:value-of select="substring-before($Input,'.')"/>.<xsl:value-of select="$temppoint"/></xsl:when>
				<xsl:when test="string-length($temppoint) &gt; 2">
					<xsl:value-of select="substring-before($Input,'.')"/>.<xsl:value-of select="substring($temppoint,1,2)"/></xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>


	<doc:template name="str:to-upper" xmlns="">
		<refpurpose>Make string uppercase</refpurpose>
		<refdescription>
			<para>Converts all lowercase letters to uppercase.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string to be converted</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string with all uppercase letters.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:to-upper">
		<xsl:param name="text"/>
		<!-- Below exception is extracted from unicode's SpecialCasing.txt
         file. It's the german lowercase "eszett" (the thing looking
         like a greek beta) that's to become "SS" in uppercase (note:
         that are *two* characters, that's why it doesn't fit in the
         list of upper/lowercase characters). There are more
         characters in that file (103, excluding the locale-specific
         ones), but they seemed to be much less used to me and they
         add up to a hellish long stylesheet.... - Reinout -->
		<xsl:param name="modified-text">
			<xsl:call-template name="str:subst">
				<xsl:with-param name="text">
					<xsl:value-of select="$text"/>
				</xsl:with-param>
				<xsl:with-param name="replace">
					<xsl:text>&#xDF;</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="with">
					<xsl:text>&#x53;</xsl:text>
					<xsl:text>&#x53;</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:param>
		<xsl:value-of select="translate($modified-text, $xsltsl-str-lower, $xsltsl-str-upper)"/>
	</xsl:template>
	<doc:template name="str:to-lower" xmlns="">
		<refpurpose>Make string lowercase</refpurpose>
		<refdescription>
			<para>Converts all uppercase letters to lowercase.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string to be converted</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string with all lowercase letters.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:to-lower">
		<xsl:param name="text"/>
		<xsl:value-of select="translate($text, $xsltsl-str-upper, $xsltsl-str-lower)"/>
	</xsl:template>
	<doc:template name="str:capitalise" xmlns="">
		<refpurpose>Capitalise string</refpurpose>
		<refdescription>
			<para>Converts first character of string to an uppercase letter.  All remaining characters are converted to lowercase.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string to be capitalised</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>all</term>
					<listitem>
						<para>Boolean controlling whether all words in the string are capitalised.</para>
						<para>Default is true.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string with first character uppcase and all remaining characters lowercase.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:capitalise">
		<xsl:param name="text"/>
		<xsl:param name="all" select="true()"/>
		<xsl:choose>
			<xsl:when test="$all and (contains($text, ' ') or contains($text, ' ') or contains($text, '&#xA;'))">
				<xsl:variable name="firstword">
					<xsl:call-template name="str:substring-before-first">
						<xsl:with-param name="text" select="$text"/>
						<xsl:with-param name="chars" select="$xsltsl-str-ws"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:call-template name="str:capitalise">
					<xsl:with-param name="text">
						<xsl:value-of select="$firstword"/>
					</xsl:with-param>
					<xsl:with-param name="all" select="false()"/>
				</xsl:call-template>
				<xsl:value-of select="substring($text, string-length($firstword) + 1, 1)"/>
				<xsl:call-template name="str:capitalise">
					<xsl:with-param name="text">
						<xsl:value-of select="substring($text, string-length($firstword) + 2)"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="str:to-upper">
					<xsl:with-param name="text" select="substring($text, 1, 1)"/>
				</xsl:call-template>
				<xsl:call-template name="str:to-lower">
					<xsl:with-param name="text" select="substring($text, 2)"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:to-camelcase" xmlns="">
		<refpurpose>Convert a string to one camelcase word</refpurpose>
		<refdescription>
			<para>Converts a string to one lowerCamelCase or UpperCamelCase
      word, depending on the setting of the "upper"
      parameter. UpperCamelCase is also called MixedCase while
      lowerCamelCase is also called just camelCase. The template
      removes any spaces, tabs and slashes, but doesn't deal with
      other punctuation. It's purpose is to convert strings like
      "hollow timber flush door" to a term suitable as identifier or
      XML tag like "HollowTimberFlushDoor".</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string to be capitalised</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>upper</term>
					<listitem>
						<para>Boolean controlling whether the string becomes an
            UpperCamelCase word or a lowerCamelCase word.</para>
						<para>Default is true.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string with first character uppcase and all remaining characters lowercase.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:to-camelcase">
		<xsl:param name="text"/>
		<xsl:param name="upper" select="true()"/>
		<!-- First change all 'strange' characters to spaces -->
		<xsl:param name="string-with-only-spaces">
			<xsl:value-of select="translate($text,concat($xsltsl-str-ws,'/'),'     ')"/>
		</xsl:param>
		<!-- Then process them -->
		<xsl:param name="before-space-removal">
			<xsl:variable name="firstword">
				<xsl:call-template name="str:substring-before-first">
					<xsl:with-param name="text" select="$string-with-only-spaces"/>
					<xsl:with-param name="chars" select="$xsltsl-str-ws"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="$upper">
					<xsl:call-template name="str:to-upper">
						<xsl:with-param name="text" select="substring($firstword, 1, 1)"/>
					</xsl:call-template>
					<xsl:call-template name="str:to-lower">
						<xsl:with-param name="text" select="substring($firstword, 2)"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="str:to-lower">
						<xsl:with-param name="text" select="$firstword"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="str:capitalise">
				<xsl:with-param name="text">
					<xsl:value-of select="substring($string-with-only-spaces, string-length($firstword) + 2)"/>
				</xsl:with-param>
				<xsl:with-param name="all" select="true()"/>
			</xsl:call-template>
		</xsl:param>
		<xsl:value-of select="translate($before-space-removal,' ','')"/>
	</xsl:template>
	<doc:template name="str:substring-before-first" xmlns="">
		<refpurpose>String extraction</refpurpose>
		<refdescription>
			<para>Extracts the portion of string 'text' which occurs before any of the characters in string 'chars'.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string from which to extract a substring.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>chars</term>
					<listitem>
						<para>The string containing characters to find.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:substring-before-first">
		<xsl:param name="text"/>
		<xsl:param name="chars"/>
		<xsl:choose>
			<xsl:when test="string-length($text) = 0"/>
			<xsl:when test="string-length($chars) = 0">
				<xsl:value-of select="$text"/>
			</xsl:when>
			<xsl:when test="contains($text, substring($chars, 1, 1))">
				<xsl:variable name="this" select="substring-before($text, substring($chars, 1, 1))"/>
				<xsl:variable name="rest">
					<xsl:call-template name="str:substring-before-first">
						<xsl:with-param name="text" select="$text"/>
						<xsl:with-param name="chars" select="substring($chars, 2)"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="string-length($this) &lt; string-length($rest)">
						<xsl:value-of select="$this"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$rest"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="str:substring-before-first">
					<xsl:with-param name="text" select="$text"/>
					<xsl:with-param name="chars" select="substring($chars, 2)"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:substring-after-last" xmlns="">
		<refpurpose>String extraction</refpurpose>
		<refdescription>
			<para>Extracts the portion of string 'text' which occurs after the last of the character in string 'chars'.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string from which to extract a substring.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>chars</term>
					<listitem>
						<para>The string containing characters to find.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:substring-after-last">
		<xsl:param name="text"/>
		<xsl:param name="chars"/>
		<xsl:choose>
			<xsl:when test="contains($text, $chars)">
				<xsl:variable name="last" select="substring-after($text, $chars)"/>
				<xsl:choose>
					<xsl:when test="contains($last, $chars)">
						<xsl:call-template name="str:substring-after-last">
							<xsl:with-param name="text" select="$last"/>
							<xsl:with-param name="chars" select="$chars"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$last"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:substring-before-last" xmlns="">
		<refpurpose>String extraction</refpurpose>
		<refdescription>
			<para>Extracts the portion of string 'text' which occurs before the first character of the last occurance of string 'chars'.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string from which to extract a substring.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>chars</term>
					<listitem>
						<para>The string containing characters to find.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:substring-before-last">
		<xsl:param name="text"/>
		<xsl:param name="chars"/>
		<xsl:choose>
			<xsl:when test="string-length($text) = 0"/>
			<xsl:when test="string-length($chars) = 0">
				<xsl:value-of select="$text"/>
			</xsl:when>
			<xsl:when test="contains($text, $chars)">
				<xsl:call-template name="str:substring-before-last-aux">
					<xsl:with-param name="text" select="$text"/>
					<xsl:with-param name="chars" select="$chars"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="str:substring-before-last-aux">
		<xsl:param name="text"/>
		<xsl:param name="chars"/>
		<xsl:choose>
			<xsl:when test="string-length($text) = 0"/>
			<xsl:when test="contains($text, $chars)">
				<xsl:variable name="after">
					<xsl:call-template name="str:substring-before-last-aux">
						<xsl:with-param name="text" select="substring-after($text, $chars)"/>
						<xsl:with-param name="chars" select="$chars"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="substring-before($text, $chars)"/>
				<xsl:if test="string-length($after) &gt; 0">
					<xsl:value-of select="$chars"/>
					<xsl:copy-of select="$after"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:subst" xmlns="">
		<refpurpose>String substitution</refpurpose>
		<refdescription>
			<para>Substitute 'replace' for 'with' in string 'text'.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string upon which to perform substitution.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>replace</term>
					<listitem>
						<para>The string to substitute.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>with</term>
					<listitem>
						<para>The string to be substituted.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>disable-output-escaping</term>
					<listitem>
						<para>A value of <literal>yes</literal> indicates that the result should have output escaping disabled.  Any other value allows normal escaping of text values.  The default is to enable output escaping.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:subst">
		<xsl:param name="text"/>
		<xsl:param name="replace"/>
		<xsl:param name="with"/>
		<xsl:param name="disable-output-escaping">no</xsl:param>
		<xsl:choose>
			<xsl:when test="string-length($replace) = 0 and $disable-output-escaping = 'yes'">
				<xsl:value-of select="$text" disable-output-escaping="yes"/>
			</xsl:when>
			<xsl:when test="string-length($replace) = 0">
				<xsl:value-of select="$text"/>
			</xsl:when>
			<xsl:when test="contains($text, $replace)">
				<xsl:variable name="before" select="substring-before($text, $replace)"/>
				<xsl:variable name="after" select="substring-after($text, $replace)"/>
				<xsl:choose>
					<xsl:when test='$disable-output-escaping = "yes"'>
						<xsl:value-of select="$before" disable-output-escaping="yes"/>
						<xsl:value-of select="$with" disable-output-escaping="yes"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$before"/>
						<xsl:value-of select="$with"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:call-template name="str:subst">
					<xsl:with-param name="text" select="$after"/>
					<xsl:with-param name="replace" select="$replace"/>
					<xsl:with-param name="with" select="$with"/>
					<xsl:with-param name="disable-output-escaping" select="$disable-output-escaping"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test='$disable-output-escaping = "yes"'>
				<xsl:value-of select="$text" disable-output-escaping="yes"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:count-substring" xmlns="">
		<refpurpose>Count Substrings</refpurpose>
		<refdescription>
			<para>Counts the number of times a substring occurs in a string.  This can also counts the number of times a character occurs in a string, since a character is simply a string of length 1.</para>
		</refdescription>
		<example>
			<title>Counting Lines</title>
			<programlisting><![CDATA[
<xsl:call-template name="str:count-substring">
  <xsl:with-param name="text" select="$mytext"/>
  <xsl:with-param name="chars" select="'&#x0a;'"/>
</xsl:call-template>
]]></programlisting>
		</example>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The source string.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>chars</term>
					<listitem>
						<para>The substring to count.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns a non-negative integer value.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:count-substring">
		<xsl:param name="text"/>
		<xsl:param name="chars"/>
		<xsl:choose>
			<xsl:when test="string-length($text) = 0 or string-length($chars) = 0">
				<xsl:text>0</xsl:text>
			</xsl:when>
			<xsl:when test="contains($text, $chars)">
				<xsl:variable name="remaining">
					<xsl:call-template name="str:count-substring">
						<xsl:with-param name="text" select="substring-after($text, $chars)"/>
						<xsl:with-param name="chars" select="$chars"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$remaining + 1"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:substring-after-at" xmlns="">
		<refpurpose>String extraction</refpurpose>
		<refdescription>
			<para>Extracts the portion of a 'char' delimited 'text' string "array" at a given 'position'.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string from which to extract a substring.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>chars</term>
					<listitem>
						<para>delimiters</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>position</term>
					<listitem>
						<para>position of the elements</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>all</term>
					<listitem>
						<para>If true all of the remaining string is returned, otherwise only the element at the given position is returned.  Default: false().</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:substring-after-at">
		<xsl:param name="text"/>
		<xsl:param name="chars"/>
		<xsl:param name="position"/>
		<xsl:param name="all" select="false()"/>
		<xsl:choose>
			<xsl:when test="number($position) = 0 and $all">
				<xsl:value-of select="$text"/>
			</xsl:when>
			<xsl:when test="number($position) = 0 and not($chars)">
				<xsl:value-of select="$text"/>
			</xsl:when>
			<xsl:when test="number($position) = 0 and not(contains($text, $chars))">
				<xsl:value-of select="$text"/>
			</xsl:when>
			<xsl:when test="not(contains($text, $chars))">
			</xsl:when>
			<xsl:when test="number($position) = 0">
				<xsl:value-of select="substring-before($text, $chars)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="str:substring-after-at">
					<xsl:with-param name="text" select="substring-after($text, $chars)"/>
					<xsl:with-param name="chars" select="$chars"/>
					<xsl:with-param name="all" select="$all"/>
					<xsl:with-param name="position" select="$position - 1"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:substring-before-at" xmlns="">
		<refpurpose>String extraction</refpurpose>
		<refdescription>
			<para>Extracts the portion of a 'char' delimited 'text' string "array" at a given 'position'</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string from which to extract a substring.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>chars</term>
					<listitem>
						<para>delimiters</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>position</term>
					<listitem>
						<para>position of the elements</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:substring-before-at">
		<xsl:param name="text"/>
		<xsl:param name="chars"/>
		<xsl:param name="position"/>
		<xsl:choose>
			<xsl:when test="$position &lt;= 0"/>
			<xsl:when test="not(contains($text, $chars))"/>
			<xsl:otherwise>
				<xsl:value-of select="substring-before($text, $chars)"/>
				<xsl:value-of select="$chars"/>
				<xsl:call-template name="str:substring-before-at">
					<xsl:with-param name="text" select="substring-after($text, $chars)"/>
					<xsl:with-param name="position" select="$position - 1"/>
					<xsl:with-param name="chars" select="$chars"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:insert-at" xmlns="">
		<refpurpose>String insertion</refpurpose>
		<refdescription>
			<para>Insert 'chars' into "text' at any given "position'</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string upon which to perform insertion</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>position</term>
					<listitem>
						<para>the position where insertion will be performed</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>with</term>
					<listitem>
						<para>The string to be inserted</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:insert-at">
		<xsl:param name="text"/>
		<xsl:param name="position"/>
		<xsl:param name="chars"/>
		<xsl:variable name="firstpart" select="substring($text, 0, $position)"/>
		<xsl:variable name="secondpart" select="substring($text, $position, string-length($text))"/>
		<xsl:value-of select="concat($firstpart, $chars, $secondpart)"/>
	</xsl:template>
	<doc:template name="str:backward" xmlns="">
		<refpurpose>String reversal</refpurpose>
		<refdescription>
			<para>Reverse the content of a given string</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string to be reversed</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns string.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:backward">
		<xsl:param name="text"/>
		<xsl:variable name="mirror">
			<xsl:call-template name="str:build-mirror">
				<xsl:with-param name="text" select="$text"/>
				<xsl:with-param name="position" select="string-length($text)"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="substring($mirror, string-length($text) + 1, string-length($text))"/>
	</xsl:template>
	<xsl:template name="str:build-mirror">
		<xsl:param name="text"/>
		<xsl:param name="position"/>
		<xsl:choose>
			<xsl:when test="$position &gt; 0">
				<xsl:call-template name="str:build-mirror">
					<xsl:with-param name="text" select="concat($text, substring($text, $position, 1))"/>
					<xsl:with-param name="position" select="$position - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:justify" xmlns="">
		<refpurpose>Format a string</refpurpose>
		<refdescription>
			<para>Inserts newlines and spaces into a string to format it as a block of text.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>String to be formatted.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>max</term>
					<listitem>
						<para>Maximum line length.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>indent</term>
					<listitem>
						<para>Number of spaces to insert at the beginning of each line.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>justify</term>
					<listitem>
						<para>Justify left, right or both.  Not currently implemented (fixed at "left").</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Formatted block of text.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:justify">
		<xsl:param name="text"/>
		<xsl:param name="max" select='"80"'/>
		<xsl:param name="indent" select='"0"'/>
		<xsl:param name="justify" select='"left"'/>
		<xsl:choose>
			<xsl:when test="string-length($text) = 0 or $max &lt;= 0"/>
			<xsl:when test='string-length($text) &gt; $max and contains($text, " ") and string-length(substring-before($text, " ")) &gt; $max'>
				<xsl:call-template name="str:generate-string">
					<xsl:with-param name="text" select='" "'/>
					<xsl:with-param name="count" select="$indent"/>
				</xsl:call-template>
				<xsl:value-of select='substring-before($text, " ")'/>
				<xsl:text>
</xsl:text>
				<xsl:call-template name="str:justify">
					<xsl:with-param name="text" select='substring-after($text, " ")'/>
					<xsl:with-param name="max" select="$max"/>
					<xsl:with-param name="indent" select="$indent"/>
					<xsl:with-param name="justify" select="$justify"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test='string-length($text) &gt; $max and contains($text, " ")'>
				<xsl:variable name="first">
					<xsl:call-template name="str:substring-before-last">
						<xsl:with-param name="text" select="substring($text, 1, $max)"/>
						<xsl:with-param name="chars" select='" "'/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:call-template name="str:generate-string">
					<xsl:with-param name="text" select='" "'/>
					<xsl:with-param name="count" select="$indent"/>
				</xsl:call-template>
				<xsl:value-of select="$first"/>
				<xsl:text>
</xsl:text>
				<xsl:call-template name="str:justify">
					<xsl:with-param name="text" select="substring($text, string-length($first) + 2)"/>
					<xsl:with-param name="max" select="$max"/>
					<xsl:with-param name="indent" select="$indent"/>
					<xsl:with-param name="justify" select="$justify"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="str:generate-string">
					<xsl:with-param name="text" select='" "'/>
					<xsl:with-param name="count" select="$indent"/>
				</xsl:call-template>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:character-first" xmlns="">
		<refpurpose>Find first occurring character in a string</refpurpose>
		<refdescription>
			<para>Finds which of the given characters occurs first in a string.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The source string.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>chars</term>
					<listitem>
						<para>The characters to search for.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
	</doc:template>
	<xsl:template name="str:character-first">
		<xsl:param name="text"/>
		<xsl:param name="chars"/>
		<xsl:choose>
			<xsl:when test="string-length($text) = 0 or string-length($chars) = 0"/>
			<xsl:when test="contains($text, substring($chars, 1, 1))">
				<xsl:variable name="next-character">
					<xsl:call-template name="str:character-first">
						<xsl:with-param name="text" select="$text"/>
						<xsl:with-param name="chars" select="substring($chars, 2)"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="string-length($next-character)">
						<xsl:variable name="first-character-position" select="string-length(substring-before($text, substring($chars, 1, 1)))"/>
						<xsl:variable name="next-character-position" select="string-length(substring-before($text, $next-character))"/>
						<xsl:choose>
							<xsl:when test="$first-character-position &lt; $next-character-position">
								<xsl:value-of select="substring($chars, 1, 1)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$next-character"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring($chars, 1, 1)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="str:character-first">
					<xsl:with-param name="text" select="$text"/>
					<xsl:with-param name="chars" select="substring($chars, 2)"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:string-match" xmlns="">
		<refpurpose>Match A String To A Pattern</refpurpose>
		<refdescription>
			<para>Performs globbing-style pattern matching on a string.</para>
		</refdescription>
		<example>
			<title>Match Pattern</title>
			<programlisting><![CDATA[
<xsl:call-template name="str:string-match">
  <xsl:with-param name="text" select="$mytext"/>
  <xsl:with-param name="pattern" select="'abc*def?g'"/>
</xsl:call-template>
]]></programlisting>
		</example>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The source string.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>pattern</term>
					<listitem>
						<para>The pattern to match against.  Certain characters have special meaning:</para>
						<variablelist>
							<varlistentry>
								<term>*</term>
								<listitem>
									<para>Matches zero or more characters.</para>
								</listitem>
							</varlistentry>
							<varlistentry>
								<term>?</term>
								<listitem>
									<para>Matches a single character.</para>
								</listitem>
							</varlistentry>
							<varlistentry>
								<term>\</term>
								<listitem>
									<para>Character escape.  The next character is taken as a literal character.</para>
								</listitem>
							</varlistentry>
						</variablelist>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
		<refreturn>
			<para>Returns "1" if the string matches the pattern, "0" otherwise.</para>
		</refreturn>
	</doc:template>
	<xsl:template name="str:string-match">
		<xsl:param name="text"/>
		<xsl:param name="pattern"/>
		<xsl:choose>
			<xsl:when test="$pattern = '*'">
				<!-- Special case: always matches -->
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="string-length($text) = 0 and string-length($pattern) = 0">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="string-length($text) = 0 or string-length($pattern) = 0">
				<xsl:text>0</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="before-special">
					<xsl:call-template name="str:substring-before-first">
						<xsl:with-param name="text" select="$pattern"/>
						<xsl:with-param name="chars" select='"*?\"'/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="special">
					<xsl:call-template name="str:character-first">
						<xsl:with-param name="text" select="$pattern"/>
						<xsl:with-param name="chars" select='"*?\"'/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="new-text" select="substring($text, string-length($before-special) + 1)"/>
				<xsl:variable name="new-pattern" select="substring($pattern, string-length($before-special) + 1)"/>
				<xsl:choose>
					<xsl:when test="not(starts-with($text, $before-special))">
						<!-- Verbatim characters don't match -->
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:when test="$special = '*' and string-length($new-pattern) = 1">
						<xsl:text>1</xsl:text>
					</xsl:when>
					<xsl:when test="$special = '*'">
						<xsl:call-template name="str:match-postfix">
							<xsl:with-param name="text" select="$new-text"/>
							<xsl:with-param name="pattern" select="substring($new-pattern, 2)"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$special = '?'">
						<xsl:call-template name="str:string-match">
							<xsl:with-param name="text" select="substring($new-text, 2)"/>
							<xsl:with-param name="pattern" select="substring($new-pattern, 2)"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$special = '\' and substring($new-text, 1, 1) = substring($new-pattern, 2, 1)">
						<xsl:call-template name="str:string-match">
							<xsl:with-param name="text" select="substring($new-text, 2)"/>
							<xsl:with-param name="pattern" select="substring($new-pattern, 3)"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$special = '\' and substring($new-text, 1, 1) != substring($new-pattern, 2, 1)">
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<!-- There were no special characters in the pattern -->
						<xsl:choose>
							<xsl:when test="$text = $pattern">
								<xsl:text>1</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>0</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="str:match-postfix">
		<xsl:param name="text"/>
		<xsl:param name="pattern"/>
		<xsl:variable name="result">
			<xsl:call-template name="str:string-match">
				<xsl:with-param name="text" select="$text"/>
				<xsl:with-param name="pattern" select="$pattern"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test='$result = "1"'>
				<xsl:value-of select="$result"/>
			</xsl:when>
			<xsl:when test="string-length($text) = 0">
				<xsl:text>0</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="str:match-postfix">
					<xsl:with-param name="text" select="substring($text, 2)"/>
					<xsl:with-param name="pattern" select="$pattern"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<doc:template name="str:generate-string" xmlns="">
		<refpurpose>Create A Repeating Sequence of Characters</refpurpose>
		<refdescription>
			<para>Repeats a string a given number of times.</para>
		</refdescription>
		<refparameter>
			<variablelist>
				<varlistentry>
					<term>text</term>
					<listitem>
						<para>The string to repeat.</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>count</term>
					<listitem>
						<para>The number of times to repeat the string.</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</refparameter>
	</doc:template>
	<xsl:template name="str:generate-string">
		<xsl:param name="text"/>
		<xsl:param name="count"/>
		<xsl:choose>
			<xsl:when test="string-length($text) = 0 or $count &lt;= 0"/>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
				<xsl:call-template name="str:generate-string">
					<xsl:with-param name="text" select="$text"/>
					<xsl:with-param name="count" select="$count - 1"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios/>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition>
			<template name="str:to-upper"></template>
			<template name="str:justify"></template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->