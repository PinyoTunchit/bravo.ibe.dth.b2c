<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.tikaero.com/xsl/documentation/1.0" xmlns:dt="http://www.tikaero.com/date-time" xmlns:str="http://www.tikaero.com/string"
                xmlns:CharCode="http://www.tikaero.com/Printing">

	<xsl:variable name="RowsPerPage">31</xsl:variable>
	<xsl:template name="styleColor">
		<!--Do not  use "Import" or "Include" for StyleSheet (CSS) because it error on EXE, but Web work fine. -->
		<STYLE TYPE="text/css">/*DO NOT DELETE COMMENT BELOW*/
	  /*Modify Date: 18MAR2011*/
	  /**********************************************/
	  body 
	  {
	  color: white;
	  font-family: 				"sans-serif";
	  font-size: 					8.5pt;
	  }
	  
      table
      {
      border-spacing: 		0px;
      empty-cells: 				show;
      margin: 						0px;
      padding: 					0px;
      }
      
      td								
      {
      vertical-align: 			top;
      }

      th								
      {
      border: 						.09mm;
      border-left: 				0px;
      border-right: 				0px;
      color: 						#19171a;
      text-align: 					center;
      }

      .blueline					
      {
      border: 						.09mm;
      border-bottom: 			solid #8bb110 1px;
      }

      .documentheader	
      { 
	  font-family: 				"sans-serif";
	  font-size: 					8.5pt;
	  font-weight: 				bold;	
	  height: 						6mm;
	  vertical-align: 			middle;
	  color: 						#19171a; 
      }

      .documenttotal		
      {
      border-bottom: 			solid #8bb110 1px;
      color: 						#8bb110;
      font-weight: 				bold;
      }

      .imgfooter 				
      {
      border-left-color: 		#FFFFFF;
      border-left-style: 		solid;
      border-top-style: 		solid;
      border-width: 			0 0 0 15px;
      left: 							0px;
      }

      .imglogo					
      {
      border-color: 				White;
      border-style: 				none;
      vertical-align: 			top;
      }

      .pagebreak 
      { 
      page-break-after: 		always; 
      }

      .tabledetails				
      {
      border-left: 				solid #FFFFFF 0px;
      border-right: 				solid #ffffff 0px;
      color: 						#8bb110;
      width: 						730px;
	  height: 						6mm;
      }

      .tablereportfooter		
      {
      border-bottom: 			solid #ffffff 0px;
      border-left: 				solid #ffffff 0px;
      border-right: 				solid #ffffff 0px;
	  margin: 						0px;
      }

      .tablereportheader	
      {
      border-left: 				solid #ffffff 0px;
      border-right: 				solid #ffffff 0px;
      border-top: 				solid #ffffff 0px;
      width: 						730px;
	  margin: 					0px 0px 0px 45px;
      }

      .tdheader					
      {
      border-left: 				0px;
      border-right: 				0px;
      border-style: 				none;
      color: 						#19171a;
      font-weight: 				bold;
      text-align: 					left;
      }

      .tdmargin					
      { 
      width: 						20px; 
      }

      .tdorderheader			
      {
      border: 						solid 1px #8bb110;
      border-left: 				0px;
      border-right: 				0px;
      }

      .tdtotalmargin			
      { width: 						450px; 
      }

      .grid-main
      {
	  font-family: 				"sans-serif";
	  font-size: 					8.5pt;
	  height: 						4.25mm;
	  color: 						#19171a;
	  vertical-align: 			middle;
      border-left: 				.75pt solid;
      border-right-style: 		none;
      border-top: 				.75pt solid;
      border-bottom: 			.75pt solid;
      background-color: 	white;
      border-top-color: 		#8bb110;
      border-bottom-color: #8bb110;
      border-left-color: 		#8bb110;
      }

      .grid-top
      {
	  font-family: 				"sans-serif";
	  font-size: 					8.5pt;
 	  color: 						#19171a;
      height: 						6mm;
	  vertical-align: 			middle;
	  border-left-style: 		none;
      border-right: 				.75pt solid;
      border-top-style: 		none;
      border-bottom-style: none;
      background-color: 	white;
      border-right-color: 	#8bb110;
      }

      .grid-even
      {
	  font-family: 				"sans-serif";
	  font-size: 					8.5pt;
	  color: 						#19171a;
		 line-height:normal;
	  vertical-align: 			middle;
      border-left-style: 		none;
      border-right: 				.75pt solid;
      border-top: 				.75pt solid;
      border-bottom-style: none;
      background-color: 	white;
      border-top-color: 		#8bb110;
      border-right-color: 	#8bb110;
      }

	 p.msonormal	
	 { 
	 color: 							#19171a; 
	 font-family: 				"sans-serif";
	 font-size: 					9pt; 
	 margin-bottom: 			.0001pt; 
	 margin-left: 				0in; 
	 margin-right: 				0in; 
	 margin-top: 				0in; 
	 mso-style-parent: 		""; 
	 text-align: 					justify; 
	 }
	 
	 h1 
	 {
	 font-family: 				sans-serif; 
	 font-size: 					14pt; 
	 color: 							#19171a; 
	 margin: 						0px; 
	line-height:					6mm;
	  width: 						730px; 
	  margin: 						0px 0px 0px 45px;


	 }
	 
	 h2 
	 {
	 font-family: 				sans-serif; 
	 font-size: 					8.5pt; 
	 line-height:					5mm;
	 }
	 
	 h3 
	 {
	 font-family: 				sans-serif; 
	 font-size: 					8.5pt;
	 }
	 
	 h4 
	 {
	 font-family: 				sans-serif; 
	 font-size: 					8.5pt;
	 }

	h1+p 
	{
  	margin-top: 					0;
	height: 							6mm;
	}</STYLE>
	</xsl:template>

	<!--HeaderSection-->
	<xsl:template match="Booking/Header">
		<!-- variable Header-->
		<xsl:variable name="BarPDF417URL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcodePDF417.ashx?valueToEncode=</xsl:variable>
		<xsl:variable name="BarURL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
		<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>
		<xsl:variable name="payment_reference" select="//Booking/Payments/Payment/payment_reference"/>

		<table border="0" cellspacing="0" style=" border-style: none; table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
			<tr>
				<td>
					<table border="0" class="tabledetails" cellspacing="0" style="width: 500px;">
						<tr>
							<td class="documentheader" style="width: 150px">Your Booking:&#xA0;</td>
							<td class="documentheader" style="font-weight: normal">
								<xsl:value-of select="/Booking/Header/BookingHeader/record_locator"/>&#xA0;</td>
						</tr>
						<tr>
							<td class="documentheader">Agency Code:</td>
							<td class="documentheader" style="font-weight: normal">
								<xsl:value-of select="/Booking/Header/BookingHeader/agency_code"/>&#xA0;/&#xA0;<xsl:value-of select="/Booking/Header/BookingHeader/agency_name"/></td>
						</tr>
						<tr>
							<td class="documentheader">Booking Date:</td>
							<td class="documentheader" style="font-weight: normal">
								<xsl:call-template name="dt:formatdate">
									<xsl:with-param name="date" select="/Booking/Header/BookingHeader/create_date_time"/>
								</xsl:call-template>&#xA0;</td>
						</tr>
						<tr>
							<td style="height: 8px"></td>
							<td style="height: 8px"></td>
						</tr>
					</table>
				</td>
				<td align="right" width="230">
					<img hspace="0" src="{concat($BarPDF417URL,$pass_id,'0')}" width="200"/>
				</td>
			</tr>
		</table>
		<!--		</div>-->
	</xsl:template>
	<!--End HeaderSection-->

	<!--PassengerSection-->
	<xsl:variable name="PassengerRowsHeader">
		<table cellspacing="0" class="grid-main" style="table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
			<tr>
				<td class="grid-top" style="width: 25px; color: white; background-color: #8bb110;">&#xA0;</td>
				<td class="grid-top" align="left" style="width: 150px; color: white; background-color: #8bb110;">
					<h2>Lastname</h2>
				</td>
				<td class="grid-top" align="left" style="width: 150px; color: white; background-color: #8bb110;">
					<h2>Firstname</h2>
				</td>
				<td class="grid-top" align="left" style="width: 100px; color: white; background-color: #8bb110;">
					<h2>Title</h2>
				</td>
				<td class="grid-top" align="left" style="width: 95px; color: white; background-color: #8bb110;">
					<h2>Type</h2>
				</td>
				<td class="grid-top" align="left" style="width: 105px; color: white; background-color: #8bb110;">
					<h2>ID TYPE</h2>
				</td>
				<td class="grid-top" align="left" style="width: 105px; color: white; background-color: #8bb110;">
					<h2>ID Number</h2>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Passengers">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<h1>PASSENGERS</h1>
				</td>
			</tr>
		</table>
		<xsl:copy-of select="$PassengerRowsHeader"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>

		<xsl:for-each select="/Booking/Passengers/Passenger">
			<xsl:value-of select="CharCode:increment()"/>
			<table cellspacing="0" class="grid-main" style=" border-top: .0pt; border-left: none; table-layout:fixed; width: 673px; margin: 0px 0px 0px 45px;">
				<tr>
					<td align="right" class="grid-top" style="width: 25px; border-right: .0pt;">
						<xsl:choose>
							<xsl:when test="position()&gt;9">
								<xsl:value-of select="concat('0','',string(position()))"/>&#xA0;</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="concat('00','',string(position()))"/>&#xA0;</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="left" class="grid-top" style="width: 150px; border-right: .0pt;">
						<xsl:value-of select="lastname"/>
						<xsl:text>&#xA0;</xsl:text>
					</td>
					<td align="left" class="grid-top" style="width: 150px; border-right: .0pt;">
						<xsl:value-of select="firstname"/>
						<xsl:text>&#xA0;</xsl:text>
					</td>
					<td align="left" class="grid-top" style="width: 100px; border-right: .0pt;">
						<xsl:value-of select="title"/>
						<xsl:text>&#xA0;</xsl:text>
					</td>
					<td align="left" class="grid-top" style="width: 95px; border-right: .0pt;">
						<xsl:value-of select="passenger_type"/>
						<xsl:text>&#xA0;</xsl:text>
					</td>
					<td align="left" class="grid-top" style="width: 105px; border-right: .0pt;">
						<xsl:choose>
							<xsl:when test="document_type_rcd ='P'">
								<text>Passport</text>
							</xsl:when>
							<xsl:when test="document_type_rcd ='V'">
								<text>Visa</text>
							</xsl:when>
							<xsl:when test="document_type_rcd ='I'">
								<text>ID Card</text>
							</xsl:when>
							<xsl:when test="document_type_rcd ='B'">
								<text>Birth Certificate</text>
							</xsl:when>
							<xsl:when test="document_type_rcd ='R'">
								<text>Resident Card</text>
							</xsl:when>
						<!--<xsl:value-of select="document_type_rcd"/>-->
						<!--<xsl:text>&#xA0;</xsl:text>-->
						</xsl:choose>&#xA0;
					</td>
					<td align="left" class="grid-top" style="width: 105px; border-right: .0pt;">
						<xsl:value-of select="passport_number"/>
						<xsl:text>&#xA0;</xsl:text>
					</td>
				</tr>
			</table>
			<xsl:if test="(CharCode:getCounter() mod  $RowsPerPage) = 0 ">
				<xsl:copy-of select="$ReportFooter"/>
				<div style="page-break-after:always">
					<br/>
				</div>
				<xsl:copy-of select="$ReportHeader"/>
				<xsl:copy-of select="$PassengerRowsHeader"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--End PassengerSection-->

	<!--FlightsSection-->
	<xsl:variable name="FlightRowsHeader">
		<table cellspacing="0" class="grid-main" style="table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
			<tr>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 25px">&#xA0;</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 50px" align="left">
					<h2>Flight</h2>
				</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 190px" align="left">
					<h2>From</h2>
				</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 190px" align="left">
					<h2>To</h2>
				</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 80px" align="center">
					<h2>Date</h2>
				</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 60px" align="center">
					<h2>Dep</h2>
				</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 60px" align="center">
					<h2>Arr</h2>
				</td>
				<!--<th>Class</th>-->
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 75px" align="center">
					<h2>Status</h2>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Itinerary">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<h1>FLIGHTS</h1>
				</td>
			</tr>
		</table>
		<xsl:copy-of select="$FlightRowsHeader"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:for-each select="/Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
			<xsl:value-of select="CharCode:increment()"/>
			<table cellspacing="0" class="grid-main" style=" border-top: .0pt; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
				<tr>
					<td class="grid-top" style="width: 25px; border-right: .0pt;">
						<xsl:value-of select="concat('00','',string(position()))"/>
					</td>
					<td class="grid-top" style="width: 50px; border-right: .0pt;">
						<SPAN>
							<xsl:value-of select="airline_rcd"/>
							<xsl:value-of select="flight_number"/>
						</SPAN>
					</td>
					<td align="left" class="grid-top" style="width: 190px; border-right: .0pt;">
						<xsl:value-of select="origin_name"/>&#xA0;(<xsl:value-of select="origin_rcd"/>)</td>
					<td align="left" class="grid-top" style="width: 190px; border-right: .0pt;">
						<xsl:value-of select="destination_name"/>&#xA0;(<xsl:value-of select="destination_rcd"/>)</td>
					<td align="center" class="grid-top" style="width: 80px; border-right: .0pt;">
						<xsl:if test="string-length(departure_date) &gt; '0'">
							<xsl:call-template name="dt:formatdate">
								<xsl:with-param name="date" select="departure_date"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="string-length(departure_date) = '0'">
							<xsl:text>OPEN</xsl:text>
						</xsl:if>
					</td>
					<td align="center" class="grid-top" style="width: 60px; border-right: .0pt;">
						<xsl:if test="string(departure_time) != '0'">
							<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
					</td>
					<td align="center" class="grid-top" style="width: 60px; border-right: .0pt;">
						<xsl:if test="string(planned_arrival_time) != '0'">
							<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/></xsl:if>
					</td>
					<td align="center" class="grid-top" style="width: 70px; border-right: .0pt;">
						<xsl:choose>
							<xsl:when test="flight_id != ''">
								<xsl:value-of select="status_name"/>
							</xsl:when>
							<xsl:when test="not(string-length(departure_date) &gt; '0')">
								<xsl:text>&#xA0;</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Info</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
			</table>
			<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
				<xsl:copy-of select="$ReportFooter"/>
				<div style="page-break-after:always">
					<br/>
				</div>
				<xsl:copy-of select="$ReportHeader"/>
				<xsl:copy-of select="$FlightRowsHeader"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--End FlightsSection-->

	<!--Auxiliaries-->
	<xsl:variable name="RemarkRowsHeader">
		<table cellspacing="0" class="grid-main" style="table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
			<tr>
				<td class="grid-top" style="width: 230px; color: white; background-color: #8bb110;" align="left">Code</td>
				<td class="grid-top" style="width: 500px; color: white; background-color: #8bb110;" align="left">Description</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Remarks">
		<xsl:if test="count(/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX'])&gt;0">
			<table cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<h1>AUXILIARIES</h1>
					</td>
				</tr>
			</table>
			<!--<h1>AUXILIARIES</h1>-->
			<xsl:copy-of select="$RemarkRowsHeader"/>
			<xsl:value-of select="CharCode:increment()"/>
			<xsl:value-of select="CharCode:increment()"/>
			<xsl:for-each select="/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
				<xsl:value-of select="CharCode:increment()"/>
				<table cellspacing="0" class="grid-main" style=" border-top: .0pt; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
					<tr>
						<td class="grid-top" style="width: 230px; border-right: .0pt;" align="left">
							<xsl:value-of select="display_name"/>
							<xsl:text>&#xA0;</xsl:text>
						</td>
						<td class="grid-top" style="width: 500px; border-right: .0pt;" align="left">
							<xsl:value-of select="remark_text"/>
							<xsl:text>&#xA0;</xsl:text>
						</td>
					</tr>
				</table>
				<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
					<xsl:copy-of select="$ReportFooter"/>
					<div style="page-break-after:always">
						<br/>
					</div>
					<xsl:copy-of select="$ReportHeader"/>
					<xsl:copy-of select="$RemarkRowsHeader"/>
				</xsl:if>
			</xsl:for-each>
			<!--Filler -->
			<xsl:choose>
				<!-- case of only one page-->
				<xsl:when test="CharCode:getCounter() &lt;= $RowsPerPage">
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
			<!--End Auxiliaries-->
		</xsl:if>
	</xsl:template>
	<!--End Auxiliaries-->

	<!--Special Services-->
	<xsl:variable name="ServicesRowsHeader">
		<table cellspacing="0" class="grid-main" style="table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
			<tr>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 25px">&#xA0;</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 50px" align="left">Flight</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 200px" align="left">Passenger Name</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 100px" align="left">Status</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 200px" align="left">Special Service</td>
				<td class="grid-top" style="color: white; background-color: #8bb110; width: 155px" align="left">Text</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/SpecialServices">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<h1>SPECIAL SERVICES</h1>
				</td>
			</tr>
		</table>
		<!--<h1>SPECIAL SERVICES</h1>-->
		<xsl:copy-of select="$ServicesRowsHeader"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')]">
			<xsl:sort select="departure_date" order="descending"/>
			<!--<xsl:if test="position()=1">-->
			<table cellspacing="0" class="grid-main" style=" border-top: .0pt; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
				<xsl:value-of select="CharCode:increment()"/>

				<tr>
					<td class="grid-top" style="width: 25px; border-right: .0pt;">
						<xsl:value-of select="concat('00','',string(position()))"/>&#xA0;</td>
					<td class="grid-top" style="width: 50px; border-right: .0pt;">
						<xsl:value-of select="airline_rcd"/>&#xA0;<xsl:value-of select="flight_number"/></td>
					<td class="grid-top" style="width: 200px; border-right: .0pt;">
						<xsl:value-of select="lastname"/>/&#xA0;<xsl:value-of select="firstname"/></td>
					<td class="grid-top" align="left" style="width: 100px; border-right: .0pt;">
						<xsl:value-of select="special_service_status"/>
					</td>
					<td class="grid-top" style="width: 200px; border-right: .0pt;">
						<xsl:value-of select="special_service_rcd"/>
						<xsl:text>&#xA0;</xsl:text>
						<xsl:value-of select="display_name"/>
					</td>
					<td class="grid-top" style="width: 155px; border-right: .0pt;">
						<xsl:value-of select="service_text"/>&#xA0;</td>
				</tr>
			</table>
			<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
				<xsl:copy-of select="$ReportFooter"/>
				<div style="page-break-after:always">
					<br/>
				</div>
				<xsl:copy-of select="$ReportHeader"/>
				<xsl:copy-of select="$ServicesRowsHeader"/>
			</xsl:if>
			<!--</xsl:if>-->
		</xsl:for-each>
		<!--Filler -->
		<xsl:choose>
			<!-- case of only one page-->
			<xsl:when test="CharCode:getCounter() &lt;= $RowsPerPage">
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
		<!--End Filler -->
	</xsl:template>
	<!--End Special Services-->

	<!--TicketSection-->
	<xsl:variable name="TicketRowsHeader">
		<table cellspacing="0" class="grid-main" style="table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
			<tr>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110; width: 50px;">
					<h2>Flight</h2>
				</td>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110; width: 150px;">
					<h2>Passenger Name</h2>
				</td>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110; width: 100px;">
					<h2>Ticket</h2>
				</td>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110; width: 100px;">
					<h2>Class</h2>
				</td>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110; width: 100px;">
					<h2>Baggage</h2>
				</td>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110; width: 230px;">
					<h2>Endorsement &amp; Restrictions</h2>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Tickets">
		<!--Ticket-->
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<h1>TICKETS</h1>
				</td>
			</tr>
		</table>
		<!--<h1>TICKETS</h1>-->
		<xsl:copy-of select="$TicketRowsHeader"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>

		<xsl:for-each select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
			<xsl:sort select="seat_number" order="descending"/>
			<xsl:sort select="ticket_number" order="descending"/>
			<xsl:sort select="restriction_text" order="descending"/>
			<xsl:sort select="endorsement_text" order="descending"/>
			<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
			<table cellspacing="0" class="grid-main" style=" border-top: .0pt; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
				<xsl:if test="(string-length(seat_number) &gt;0) or (string-length(ticket_number) &gt;0) or (string-length(restriction_text) &gt;0) or (string-length(endorsement_text) &gt;0)">
					<xsl:value-of select="CharCode:increment()"/>
					<tr>
						<td style="border-right: .0pt; width: 50px" align="left" valign="top" class="grid-top">
							<xsl:value-of select="airline_rcd"/>
							<xsl:value-of select="flight_number"/>&#xA0;</td>
						<td align="left" valign="top" class="grid-top" style="width: 150px; border-right: .0pt;">
							<xsl:choose>
								<xsl:when test="not(string-length(lastname) &gt; '0')">
									<xsl:text>&#xA0;</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="lastname"/>&#xA0;/&#xA0;<xsl:value-of select="firstname"/></xsl:otherwise>
							</xsl:choose>
						</td>
						<td align="left" valign="top" class="grid-top" style="border-right: .0pt; width: 100px;">
							<xsl:value-of select="ticket_number"/>&#xA0;</td>
						<td align="left" valign="top" class="grid-top" style="border-right: .0pt; width: 100px;">
							<xsl:value-of select="/Booking/Itinerary/FlightSegment[booking_segment_id = $booking_segment_id][segment_status_rcd != 'XX']/class_name"/>&#xA0;</td>
						<td align="left" valign="top" class="grid-top" style="border-right: .0pt; width: 100px;">
							<xsl:value-of select="format-number(baggage_weight,'#,##0')"/>&#xA0;Kgs.</td>
						<td align="left" valign="top" class="grid-top" style="border-right: .0pt; width: 230px;">
							<xsl:choose>
								<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) &gt;0)">
									<xsl:value-of select="endorsement_text"/>&#xA0;/&#xA0;
									<xsl:value-of select="restriction_text"/>
								</xsl:when>
								<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) =0)">
									<xsl:value-of select="restriction_text"/>
								</xsl:when>
								<xsl:when test="(string-length(restriction_text) =0) and (string-length(endorsement_text) &gt;0)">
									<xsl:value-of select="endorsement_text"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="restriction_text"/>
								</xsl:otherwise>
							</xsl:choose>&#xA0;</td>
					</tr>
				</xsl:if>
			</table>
			<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
				<!--40 rows per page-->
				<xsl:copy-of select="$ReportFooter"/>
				<div style="page-break-after:always">
					<br/>
				</div>
				<xsl:copy-of select="$ReportHeader"/>
				<xsl:copy-of select="$TicketRowsHeader"/>
			</xsl:if>
		</xsl:for-each>
		<!--Filler -->
		<xsl:choose>
			<!-- case of only one page-->
			<xsl:when test="CharCode:getCounter() &lt;= $RowsPerPage">
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--End TicketSection-->

	<!--QuoteSection-->
	<xsl:variable name="QuoteRowsHeader">
		<table cellspacing="0" class="grid-main" style="table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
			<tr>
				<td class="grid-top" align="left" style="width: 100px; color: white; background-color: #8bb110;">
					<h2>Passenger</h2>
				</td>
				<td class="grid-top" align="center" style="width: 50px; color: white; background-color: #8bb110;">
					<h2>Units</h2>
				</td>
				<td class="grid-top" align="left" style="width: 280px; color: white; background-color: #8bb110;">
					<h2>Charge</h2>
				</td>
				<td class="grid-top" align="right" style="width: 100px; color: white; background-color: #8bb110;">
					<h2>Amount</h2>
				</td>
				<td class="grid-top" align="right" style="width: 100px; color: white; background-color: #8bb110;">
					<h2>VAT</h2>
				</td>
				<td class="grid-top" align="right" style="width: 100px; color: white; background-color: #8bb110;">
					<h2>Total&#xA0;</h2>
				</td>
			</tr>
		</table>
	</xsl:variable>


	<xsl:key name="passenger_type_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<xsl:template match="Booking/TicketQuotes">
		<xsl:if test="position()=1">
			<table cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<h1>QUOTES</h1>
					</td>
				</tr>
			</table>
			<!--<h1>QUOTES</h1>-->
			<xsl:copy-of select="$QuoteRowsHeader"/>
			<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
				<xsl:copy-of select="$ReportFooter"/>
				<div style="page-break-after:always">
					<br/>
				</div>
				<xsl:copy-of select="$ReportHeader"/>
				<xsl:copy-of select="$QuoteRowsHeader"/>
			</xsl:if>
			<xsl:value-of select="CharCode:increment()"/>
			<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
				<xsl:copy-of select="$ReportFooter"/>
				<div style="page-break-after:always">
					<br/>
				</div>
				<xsl:copy-of select="$ReportHeader"/>
				<xsl:copy-of select="$QuoteRowsHeader"/>
			</xsl:if>
			<!--<xsl:value-of select="CharCode:increment()"/>-->
			<xsl:for-each select="//Passengers/Passenger[count(. | key('passenger_type_group', passenger_type_rcd)[1]) = 1]">
				<xsl:variable name="passenger_type" select="passenger_type_rcd"/>

				<xsl:for-each select="//TicketQuotes/Total[passenger_type_rcd=$passenger_type]">
					<table cellspacing="0" class="grid-main" style=" border-top: .0pt; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
						<xsl:value-of select="CharCode:increment()"/>
						<xsl:variable name="TotalCharge">
							<xsl:if test="charge_type != 'REFUND'">
								<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type]/charge_amount)"/>
							</xsl:if>
							<xsl:if test="charge_type = 'REFUND'">
								<xsl:value-of select="sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type]/charge_amount)"/>
							</xsl:if>
						</xsl:variable>
						<xsl:if test="position()=1">
							<xsl:if test="position()!=last()">
								<tr>
									<td class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;">
										<xsl:value-of select="passenger_type_rcd"/>
									</td>
									<td class="grid-top" align="center" style=" border-top: .0pt; width: 50px; border-right: .0pt;">
										<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type]/passenger_count)"/>
									</td>
									<td class="grid-top" align="left" style=" border-top: .0pt; width: 280px; border-right: .0pt;">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;" align="right">
										<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
									</td>
									<td class="grid-top" align="right" style=" border-top: .0pt; width: 100px; border-right: .0pt;">
										<xsl:if test="number(tax_amount) != 0">
											<xsl:if test="number(tax_amount) &gt;= 0">
												<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
											</xsl:if>
										</xsl:if>&#xA0;</td>
									<td class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;" align="right">
										<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type]/tax_amount),'#,##0.00')"/>
										<xsl:text>&#x20;</xsl:text>
										<xsl:value-of select="currency_rcd"/>
									</td>
								</tr>
							</xsl:if>
							<xsl:if test="position()=last()">
								<tr>
									<td class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;">
										<xsl:value-of select="passenger_type_rcd"/>
									</td>
									<td align="center" class="grid-top" style=" border-top: .0pt; width: 50px; border-right: .0pt;">
										<xsl:value-of select="passenger_count"/>
									</td>
									<td align="left" class="grid-top" style=" border-top: .0pt; width: 280px; border-right: .0pt;">
										<xsl:value-of select="charge_name"/>
									</td>
									<td align="right" class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;">
										<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
									</td>
									<td align="right" class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;">
										<xsl:if test="number(tax_amount) != 0">
											<xsl:if test="number(tax_amount) &gt;= 0">
												<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
											</xsl:if>
										</xsl:if>&#xA0;</td>
									<td align="right" class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;">
										<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type]/tax_amount),'#,##0.00')"/>
										<xsl:text>&#x20;</xsl:text>
										<xsl:value-of select="currency_rcd"/>
									</td>
								</tr>
							</xsl:if>
						</xsl:if>
						<xsl:if test="position()!=1">
							<xsl:if test="position()=last()">
								<xsl:if test="charge_type != 'REFUND'">
									<tr>
										<td class="grid-top" style=" border-top: .0pt;width: 100px;  border-right: .0pt;">&#xA0;</td>
										<td class="grid-top" style=" border-top: .0pt;width: 50px;  border-right: .0pt;">&#xA0;</td>
										<td class="grid-top" style=" border-top: .0pt; width: 280px; border-right: .0pt;">
											<xsl:value-of select="charge_name"/>
										</td>
										<td class="grid-top" align="right" style=" border-top: .0pt; width: 100px; border-right: .0pt;">
											<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
										</td>
										<td class="grid-top" align="right" style=" border-top: .0pt; width: 100px; border-right: .0pt;">
											<xsl:if test="number(tax_amount) != 0">
												<xsl:if test="number(tax_amount) &gt;= 0">
													<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
												</xsl:if>
											</xsl:if>&#xA0;</td>
										<td class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;">&#xA0;</td>
									</tr>
								</xsl:if>
								<xsl:if test="charge_type = 'REFUND'">
									<tr>
										<td>&#xA0;</td>
										<td align="center" style=" border-top: .0pt; width: 100px; border-right: .0pt;">&#xA0;</td>
										<td style=" border-top: .0pt; width: 50px; border-right: .0pt;">
											<xsl:value-of select="charge_name"/>
										</td>
										<td align="right" style=" border-top: .0pt; width: 280px; border-right: .0pt;">&#xA0;</td>
										<td align="right" style=" border-top: .0pt; width: 100px; border-right: .0pt;">&#xA0;</td>
										<td align="right" style=" border-top: .0pt width: 100px; border-right: .0pt;">-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type]/tax_amount),'#,##0.00')"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency_rcd"/>
										</td>
									</tr>
								</xsl:if>
							</xsl:if>
							<xsl:if test="position()!=last()">
								<xsl:if test="charge_type != 'REFUND'">
									<tr>
										<td class="grid-top" style=" border-bottom: .0pt; width: 100px; border-right: .0pt; ">&#xA0;</td>
										<td class="grid-top" style=" border-bottom: .0pt; width: 50px; border-right: .0pt;">&#xA0;</td>
										<td class="grid-top" style=" border-bottom: .0pt; width: 280px; border-right: .0pt;">
											<xsl:value-of select="charge_name"/>
										</td>
										<td class="grid-top" style=" border-bottom: .0pt; width: 100px; border-right: .0pt;" align="right">
											<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
										</td>
										<td class="grid-top" style="border-bottom: .0pt; width: 100px; border-right: .0pt;" align="right">
											<xsl:if test="number(tax_amount) != 0">
												<xsl:if test="number(tax_amount) &gt;= 0">
													<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
												</xsl:if>
											</xsl:if>&#xA0;</td>
										<td class="grid-top" style=" border-top: .0pt; width: 100px; border-right: .0pt;">&#xA0;</td>
									</tr>
								</xsl:if>
								<xsl:if test="charge_type = 'REFUND'">
									<tr>
										<td style=" border-top: .0pt; width: 100px; border-right: .0pt;">&#xA0;</td>
										<td align="center" style=" border-top: .0pt; width: 50px; border-right: .0pt;">&#xA0;</td>
										<td>
											<xsl:value-of select="charge_name"/>
										</td>
										<td align="right" style=" border-top: .0pt; width: 280px; border-right: .0pt;">&#xA0;</td>
										<td align="right" style=" border-top: .0pt; width: 100px; border-right: .0pt;">&#xA0;</td>
										<td align="right" style=" border-top: .0pt; width: 100px; border-right: .0pt;">-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type = $passenger_type]/tax_amount),'#,##0.00')"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency_rcd"/>
										</td>
									</tr>
								</xsl:if>
							</xsl:if>
						</xsl:if>
					</table>
					<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
						<xsl:copy-of select="$ReportFooter"/>
						<div style="page-break-after:always">
							<br/>
						</div>
						<xsl:copy-of select="$ReportHeader"/>
						<xsl:copy-of select="$QuoteRowsHeader"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>

			<xsl:for-each select="//Fees/Fee[void_date_time = '']">
				<table cellspacing="0" class="grid-main" style=" border-top: .0pt; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
					<xsl:if test="position()=1">
						<xsl:value-of select="CharCode:increment()"/>
						<tr>
							<td class="grid-top" style="width: 100px; border-right: .0pt;">&#xA0;</td>
							<td class="grid-top" style="width: 50px; border-right: .0pt;">&#xA0;</td>
							<td class="grid-top" style="width: 280px; border-right: .0pt;">
								<xsl:value-of select="display_name"/>
							</td>
							<td class="grid-top" style="border-right: .0pt; width: 100px; " align="right">
								<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
							</td>
							<td class="grid-top" style=" width: 100px; border-right: .0pt;" align="right">
								<xsl:choose>
									<xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">&#xA0;</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>&#xA0;</xsl:otherwise>
								</xsl:choose>
							</td>
							<td class="grid-top" style="border-right: .0pt; width: 100px; " align="right">
								<xsl:value-of select="format-number(sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/>
								<xsl:text>&#xA0;</xsl:text>
								<xsl:value-of select="currency_rcd"/>
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="position()!=1">
						<xsl:value-of select="CharCode:increment()"/>
						<tr>
							<td class="grid-top" style="width: 100px; border-right: .0pt;">&#xA0;</td>
							<td class="grid-top" style="width: 50px; border-right: .0pt;">&#xA0;</td>
							<td class="grid-top" style="width: 280px; border-right: .0pt;">
								<xsl:value-of select="display_name"/>
							</td>
							<td class="grid-top" align="right" style="border-right: .0pt; width: 100px; ">
								<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
							</td>
							<td class="grid-top" align="right" style=" width: 100px; border-right: .0pt;">
								<xsl:choose>
									<xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">&#xA0;</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td class="grid-top" style="border-right: .0pt; width: 100px; ">&#xA0;</td>
						</tr>
					</xsl:if>
				</table>
				<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
					<!--40 rows per page-->
					<xsl:copy-of select="$ReportFooter"/>
					<div style="page-break-after:always">
						<br/>
					</div>
					<xsl:copy-of select="$ReportHeader"/>
					<xsl:copy-of select="$QuoteRowsHeader"/>
				</xsl:if>
			</xsl:for-each>

			<table cellspacing="0" class="grid-main" style="border-top: none; border-left: none; table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
				<xsl:value-of select="CharCode:increment()"/>
				<tr>
					<td class="grid-top" style="width: 100px; border-right: .0pt;">&#xA0;</td>
					<td class="grid-top" style="width: 50px; border-right: .0pt;">&#xA0;</td>
					<td class="grid-top" style="width: 280px; border-right: .0pt;">&#xA0;</td>
					<td class="grid-top" style="border-right: .0pt; width: 100px; ">&#xA0;</td>
					<td class="grid-top" style=" width: 100px; border-right: .0pt;" align="right">
						<span>Total</span>
					</td>
					<td class="grid-top" style="border-right: .0pt; width: 100px; " align="right">
						<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/>
						<xsl:text>&#xA0;</xsl:text>
						<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/>
					</td>
				</tr>
			</table>
			<!--<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
				<xsl:copy-of select="$ReportFooter"/>
				<div style="page-break-after:always">
					<br/>
				</div>
				<xsl:copy-of select="$ReportHeader"/>
				<xsl:copy-of select="$QuoteRowsHeader"/>
			</xsl:if>-->
		</xsl:if>
	</xsl:template>
	<!--End QuoteSection-->

	<!--PaymentSection-->
	<xsl:variable name="PaymentRowsHeader">
		<table cellspacing="0" class="grid-main" style="table-layout:fixed;  width: 730px; margin: 0px 0px 0px 45px;">
			<tr>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110;border-right-style: none; width: 135px">
					<h2>Description</h2>
				</td>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110;border-right-style: none; width: 110px">
					<h2>ID Transaction</h2>
				</td><td class="grid-top" align="left" style="color: white; background-color: #8bb110;border-right-style: none; width: 145px">
					<h2>Track ID/Authorization code</h2>
				</td>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110;border-right-style: none; width: 125px">
					<h2>Reference Number</h2>
				</td>
				<td class="grid-top" align="left" style="color: white; background-color: #8bb110;border-right-style: none; width: 80px">
					<h2>Status, Date</h2>
				</td>
				<td class="grid-top" align="right" style="color: white; background-color: #8bb110;border-right-style: none; width: 80px">
					<h2>Credit</h2>
				</td>
				<td class="grid-top" align="right" style="color: white; background-color: #8bb110;border-right-style: none; width: 55px">
					<h2>Debit&#xA0;</h2>
				</td>
			</tr>
		</table>
	</xsl:variable>
	<xsl:template match="Booking/Payments">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<h1>PAYMENTS</h1>
				</td>
			</tr>
		</table>
		<!--<h1>PAYMENTS</h1>-->
		<xsl:copy-of select="$PaymentRowsHeader"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
		<xsl:variable name="Ticket_total"
		              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>

		<xsl:choose>
			<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">
				<table cellspacing="0" class="grid-main" style="border-top: none; border-bottom: .75pt solid #8bb110; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
					<!--<table cellspacing="0" class="grid-main" style="border-bottom: .0pt;border-top: none;border-left: none; border-right: none; table-layout:fixed; width: 673px; margin: 0px 0px 0px 45px;">-->
					<xsl:value-of select="CharCode:increment()"/>
					<tr>
						<td align="left" class="grid-top" style="border-right-style: none; width: 135px">Ticket Cost &amp; Fee</td>
						<td align="left" class="grid-top" style="border-right-style: none; width: 110px">&#xA0;</td>
						
						<td align="left" class="grid-top" style="border-right-style: none; width: 145px">&#xA0;</td>
						<td align="left" class="grid-top" style="border-right-style: none; width: 125px">&#xA0;</td>
						<xsl:if test="starts-with(string($Ticket_total), '-')">
							<td align="left" class="grid-top" style="border-right-style: none; width: 80px">&#xA0;</td>
							<td align="right" class="grid-top" style="border-right-style: none; width: 80px">
								<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
						</xsl:if>
							<td align="left" class="grid-top" style="border-right-style: none; width: 100px">&#xA0;</td>
						<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
							<td align="right" class="grid-top" style="border-right-style: none; width: 75px">
								<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;
								<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>

							<td align="left" class="grid-top" style="border-right-style: none; width: 40px">&#xA0;</td>
						</xsl:if>
					</tr>
				</table>
				<xsl:if test="(CharCode:getCounter() mod $RowsPerPage ) = 0 ">
					<!--40 rows per page-->
					<xsl:copy-of select="$ReportFooter"/>
					<div style="page-break-after:always">
						<br/>
					</div>
					<xsl:copy-of select="$ReportHeader"/>
					<xsl:copy-of select="$PaymentRowsHeader"/>
				</xsl:if>

				<xsl:for-each select="/Booking/Payments/Payment[void_date_time='']">

					<table cellspacing="0" class="grid-main" style="border-top: none; border-bottom: .75pt solid #8bb110; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
						<xsl:value-of select="CharCode:increment()"/>
						<tr>
							<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 135px">
								<xsl:choose>
									<xsl:when test="form_of_payment_subtype_rcd ='EFT'">
										<text>PostFinance</text>
									</xsl:when>
									<xsl:when test="form_of_payment_rcd ='EFT'">
										<text>PostFinance</text>
									</xsl:when>

									<xsl:when test="form_of_payment_subtype !='' and form_of_payment_subtype !='EFT' ">
										<xsl:value-of select="form_of_payment_subtype"/>
									</xsl:when>

									<xsl:when test="form_of_payment !='' and form_of_payment !='EFT'">
										<xsl:value-of select="form_of_payment"/>
									</xsl:when>


									<xsl:otherwise>
										<text></text>
									</xsl:otherwise>
								</xsl:choose>&#xA0;</td>

	<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 110px">
								<xsl:value-of select="payment_reference"/>&#xA0;</td>
									<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 145px">
								<!--<xsl:value-of select="payment_reference"/>--><xsl:value-of select="transaction_reference"/><!--<xsl:value-of select='substring(transaction_reference,1,10)'/> -->/ <xsl:value-of select="approval_code"/></td>

							<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 125px;">
								<!--<xsl:value-of select="document_number"/>-->&#xA0;</td>
							<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">&#xA0;
								<xsl:choose>
									<xsl:when test="void_date_time != ''">
										<xsl:text>XX</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:if test="payment_date_time != ''">
											<xsl:call-template name="dt:formatdatetime">
												<xsl:with-param name="date" select="payment_date_time"/>
											</xsl:call-template>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 10px">
								<xsl:choose>
									<xsl:when test="not(payment_amount &gt;= 0)">
										<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
									<xsl:otherwise>
									</xsl:otherwise>
								</xsl:choose>&#xA0;</td>
							<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 105px">
								<xsl:choose>
									<xsl:when test="payment_amount &gt; 0">
										<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;	<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
									<xsl:otherwise>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</table>
					<xsl:if test="(CharCode:getCounter() mod $RowsPerPage ) = 0 ">
						<!--40 rows per page-->
						<xsl:copy-of select="$ReportFooter"/>
						<div style="page-break-after:always">
							<br/>
						</div>
						<xsl:copy-of select="$ReportHeader"/>
						<xsl:copy-of select="$PaymentRowsHeader"/>
					</xsl:if>
				</xsl:for-each>

				<table cellspacing="0" class="grid-main" style="border-top: none; border-bottom: .75pt solid #8bb110; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
					<xsl:value-of select="CharCode:increment()"/>
					<tr>
						<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 140px ">OUTSTANDING BALANCE</td>
						<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 110px">&#xA0;</td>
						<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 145px">&#xA0;</td>
						<xsl:variable name="SubTotal" select="$Ticket_total - $Payment_total"/>
						<xsl:if test="number($SubTotal) = 0">
							<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 125px">&#xA0;</td>
							<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 90px">&#xA0;</td>
							<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 56px">&#xA0;</td>
							<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 64px">0.00&#xA0;&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
						</xsl:if>
						<xsl:if test="number($SubTotal) != 0">
							<xsl:if test="number($SubTotal) &gt;= 0">
								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
								<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
							</xsl:if>
							<xsl:if test="number($SubTotal) &lt; 0">
								<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">

									<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
							</xsl:if>
						</xsl:if>
					</tr>
				</table>
				<xsl:if test="(CharCode:getCounter() mod $RowsPerPage ) = 0 ">
					<!--40 rows per page-->
					<xsl:copy-of select="$ReportFooter"/>
					<div style="page-break-after:always">
						<br/>
					</div>
					<xsl:copy-of select="$ReportHeader"/>
					<xsl:copy-of select="$PaymentRowsHeader"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<table cellspacing="0" class="grid-main" style="border-top: none; border-bottom: .75pt solid #8bb110; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
					<xsl:value-of select="CharCode:increment()"/>
					<tr>
						<td align="left" class="grid-top" style="border-top:none; border-right-style: none; width: 280px">Ticket Cost &amp; Fee</td>
						<td align="left" class="grid-top" style="border-top:none; border-right-style: none; width: 150px">&#xA0;</td>
						<td align="left" class="grid-top" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
						<xsl:if test="starts-with(string($Ticket_total), '-')">
							<td align="left" class="grid-top" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
							<td align="right" class="grid-top" style="border-top:none; border-right-style: none; width: 100px">
								<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
						</xsl:if>
						<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
							<td align="right" class="grid-top" style="border-top:none; border-right-style: none; width: 100px">
								<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
							<td align="left" class="grid-top" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
						</xsl:if>
					</tr>
				</table>
				<xsl:if test="(CharCode:getCounter() mod $RowsPerPage ) = 0 ">
					<!--40 rows per page-->
					<xsl:copy-of select="$ReportFooter"/>
					<div style="page-break-after:always">
						<br/>
					</div>
					<xsl:copy-of select="$ReportHeader"/>
					<xsl:copy-of select="$PaymentRowsHeader"/>
				</xsl:if>

				<xsl:for-each select="/Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)]">
					<xsl:if test="form_of_payment_subtype !=''">
						<table cellspacing="0" class="grid-main" style="border-top: none; border-bottom: .75pt solid #8bb110; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
							<xsl:value-of select="CharCode:increment()"/>
							<tr>
								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 280px">
									<xsl:choose>
										<xsl:when test="form_of_payment_subtype_rcd ='EFT'">
											<text>PostFinance</text>
										</xsl:when>
										<xsl:when test="form_of_payment_rcd ='EFT'">
											<text>PostFinance</text>
										</xsl:when>

										<xsl:when test="form_of_payment_subtype !='' and form_of_payment_subtype !='EFT' ">
											<xsl:value-of select="form_of_payment_subtype"/>
										</xsl:when>

										<xsl:when test="form_of_payment !='' and form_of_payment !='EFT'">
											<xsl:value-of select="form_of_payment"/>
										</xsl:when>


										<xsl:otherwise>
											<text></text>
										</xsl:otherwise>
									</xsl:choose>&#xA0;</td>

								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 150px">
									<xsl:value-of select="document_number"/>&#xA0;</td>
								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:choose>
										<xsl:when test="void_date_time != ''">
											<xsl:text>XX</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="payment_date_time != ''">
												<xsl:call-template name="dt:formatdate">
													<xsl:with-param name="date" select="payment_date_time"/>
												</xsl:call-template>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</td>
								<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:choose>
										<xsl:when test="not(payment_amount &gt;= 0)">
											<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</td>
								<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:choose>
										<xsl:when test="payment_amount &gt; 0">
											<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
						</table>
						<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
							<!--40 rows per page-->
							<xsl:copy-of select="$ReportFooter"/>
							<div style="page-break-after:always">
								<br/>
							</div>
							<xsl:copy-of select="$ReportHeader"/>
							<xsl:copy-of select="$PaymentRowsHeader"/>
						</xsl:if>
					</xsl:if>

					<xsl:if test="form_of_payment_subtype =''">
						<table cellspacing="0" class="grid-main" style="border-top: none; border-bottom: .75pt solid #8bb110; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
							<xsl:value-of select="CharCode:increment()"/>
							<tr>
								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 280px">
									<xsl:choose>
										<xsl:when test="form_of_payment_subtype_rcd ='EFT'">
											<text>PostFinance</text>
										</xsl:when>
										<xsl:when test="form_of_payment_rcd ='EFT'">
											<text>PostFinance</text>
										</xsl:when>

										<xsl:when test="form_of_payment_subtype !='' and form_of_payment_subtype !='EFT' ">
											<xsl:value-of select="form_of_payment_subtype"/>
										</xsl:when>

										<xsl:when test="form_of_payment !='' and form_of_payment !='EFT'">
											<xsl:value-of select="form_of_payment"/>
										</xsl:when>


										<xsl:otherwise>
											<text></text>
										</xsl:otherwise>
									</xsl:choose>&#xA0;</td>

								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 150px">
									<xsl:value-of select="document_number"/>&#xA0;</td>
								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:if test="payment_date_time != ''">
										<xsl:call-template name="dt:formatdate">
											<xsl:with-param name="date" select="payment_date_time"/>
										</xsl:call-template>
									</xsl:if>
								</td>
								<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:choose>
										<xsl:when test="not(payment_amount &gt;= 0)">
											<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</td>
								<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:choose>
										<xsl:when test="payment_amount &gt; 0">
											<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#xA0;	<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
						</table>
						<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
							<!--40 rows per page-->
							<xsl:copy-of select="$ReportFooter"/>
							<div style="page-break-after:always">
								<br/>
							</div>
							<xsl:copy-of select="$ReportHeader"/>
							<xsl:copy-of select="$PaymentRowsHeader"/>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
				<table cellspacing="0" class="grid-main" style="border-top: none; border-bottom: .75pt solid #8bb110; border-left: none; table-layout:fixed; width: 730px; margin: 0px 0px 0px 45px;">
					<xsl:value-of select="CharCode:increment()"/>
					<tr>
						<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 280px">OUTSTANDING BALANCE</td>
						<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 150px">&#xA0;</td>
						<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
						<xsl:variable name="SubTotal" select="number($Ticket_total) - number($Payment_total)"/>
						<xsl:if test="number($SubTotal) = 0">
							<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
							<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">0.00&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
						</xsl:if>
						<xsl:if test="number($SubTotal) != 0">
							<xsl:if test="number($SubTotal) &gt;= 0">
								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
								<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
							</xsl:if>
							<xsl:if test="number($SubTotal) &lt; 0">
								<td align="right" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">
									<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#xA0;<xsl:value-of select="/Booking/Tickets/Ticket/currency_rcd"/></td>
								<td align="left" class="grid-even" style="border-top:none; border-right-style: none; width: 100px">&#xA0;</td>
							</xsl:if>
						</xsl:if>
					</tr>
				</table>
				<xsl:if test="(CharCode:getCounter() mod $RowsPerPage) = 0 ">
					<xsl:copy-of select="$ReportFooter"/>
					<div style="page-break-after:always">
						<br/>
					</div>
					<xsl:copy-of select="$ReportHeader"/>
					<xsl:copy-of select="$PaymentRowsHeader"/>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>

		<!--End Payments-->
	</xsl:template>
	<!--End PaymentSection-->

	<!--en_terms-->
	<xsl:template match="Booking/Header/BookingHeader">
		<xsl:value-of select="CharCode:increment()"/>
		<xsl:value-of select="CharCode:increment()"/>
		<div style="page-break-after:always">
			<br/>
		</div>
		<xsl:copy-of select="$ReportHeader"/>
		<!--<xsl:if test="$Debug = 0">
					<xsl:copy-of select="$OTable"/>

			</xsl:if>-->

		<table border="0" style="width: 730px; margin: 0px 0px 0px 45px; ">
			<tr>
				<td class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal">

					<p class="MsoNormal" align="left" style="text-align:left;font-weight: bold; font-size: 11pt;">Terms and Conditions</p>
					<br/>
					<p class="MsoNormal">Thank you for choosing Tassili Airlines. Please print the booking confirmation. You will receive your boarding card at check-in when you present your booking code and ID. Wish you a pleasant flight and welcome you on board.</p>
					<br/>
					<p class="MsoNormal"><strong>Check-In Time Limit 1 Hour Before Departure.</strong></p>
					<br/>
					<p class="MsoNormal">Terms and Conditions details, please refer to <a target="_blank" href="http://www.tassiliairlines.dz">http://www.tassiliairlines.dz</a></p>
					<br/>
					<!--				<p class="MsoNormal" align="left" style="text-align:left;font-weight: bold; font-size: 8.5pt;">Note</p>
					<p class="MsoNormal">The Transport Contract between you and SkyWork Airlines is subject to the applicable law, our conditions of carriage and the fare terms and conditions. You can consult these at any time on our website <a target="_blank" href="http://www.flyskywork.com">www.flyskywork.com</a>. Please note that if you do not use all sections as booked and in the sequence provided, we will not accept your flight ticket and it will lose its validity (article 3.3. of the conditions of carriage).</p>
					<br/>
					<p class="MsoNormal">The liability of SkyWork Airlines or of another airline is subject to the Montreal/Warsaw Convention and EU Council Regulation 2027/97. In the event of death or bodily injury, SkyWork Airlines waives the liability limits of the Warsaw Convention and up to 113,100 Special Drawing Rights (SDRs) per passenger on the objection that it took all necessary measures to avoid the damage. In the event of death or bodily injury, SkyWork Airlines shall pay an advance in accordance with EU law.</p>
					<br/>
					<p class="MsoNormal" align="left" style="text-align:left;font-weight: bold; font-size: 8.5pt;">
						<b>Check-in</b>
					</p>
					<p class="MsoNormal">Check-in begins 120 minutes before the scheduled departure time. Please ensure that you have your boarding card 40 minutes (Bern: 30 minutes) before the scheduled departure time, as check-in closes then. Passengers who do not turn up on time forfeit their right to board the flight and remain obliged to pay the air fare.</p>
					<br/>
					<p class="MsoNormal">Our flights are ticketless flights. At check-in, you will need the booking code and an official photo ID (passport, official identity card). We also recommend that you present the booking confirmation. For children and minors, it is also necessary to present an official ID or for them to be included on the ID of the parent/legal guardian.</p>
					<br/>
					<p class="MsoNormal">For international flights, the passenger must be in possession of a valid passport or official ID card as well as any other travel documents required for travel into/out of the destination country (visa, etc.). This also applies for children and minors. In the absence of these documents, we are obliged to refuse carriage. The passenger is responsible for bringing along all necessary documents for travel into/out of the respective country.</p>
					<br/>
					<p class="MsoNormal" align="left" style="text-align:left;font-weight: bold; font-size: 8.5pt;">
						<b>Baggage</b>
					</p>
					<p class="MsoNormal">Our passengers can check in one (1) piece of luggage with an allowance of 15 kg. In addition, one (1) piece of hand luggage (dimension: 55cm x 40cm x 20cm) with a maximum weight of 8 kg as well as a notebook computer may be taken on board. The restrictions must be strictly adhered to.</p>
					<br/>
					<p class="MsoNormal">As a rule, a single piece of luggage may not exceed 30 kg. Since November 6, 2006, only limited amounts of liquids may be carried in hand luggage aboard flights that originate in EU countries (and also Switzerland). Please read the information we have provided at: <a target="_blank" href="http://flyskywork.com/en/flight-information/my-flight/baggage">http://flyskywork.com/en/flight-information/my-flight/baggage</a>.</p>
					<br/>
					<p class="MsoNormal">SkyExecutive: For passengers who have chosen our SkyExecutive fare, a check-in baggage allowance of 20 kg applies. In addition two (2) pieces of hand luggage weighing not more than 8 kg each (dimension: 55cm x 40cm x 20cm) as well as a notebook computer may be taken on board.</p>
					<br/>
					<p class="MsoNormal">Children: Children under two (2) years of age with a booking at the infant fare do not have any check-in baggage or hand luggage allowance in accordance with the aforementioned conditions.</p>
					<br/>
					<p class="MsoNormal">Please see our fare terms and conditions.</p>
					<br/>
					<p class="MsoNormal" align="left" style="text-align:left;font-weight: bold; font-size: 8.5pt;">
						<b>Flight and airport</b>
					</p>
					<p class="MsoNormal">The booking confirmation contains the flight times that are valid at that time. However, we request that you check these flight times again 24 hours before departure on our website at <a target="_blank" href="http://www.flyskywork.com">www.flyskywork.com</a>.</p>
					<br/>
					<p class="MsoNormal">You can also consult information on airports and terminals at any time on our website (<a target="_blank" href="http://flyskywork.com/en/flight-information/airportinfos">http://flyskywork.com/en/flight-information/airportinfos</a> ).</p>
					<br/>
					<p class="MsoNormal" align="left" style="text-align:left;font-weight: bold; font-size: 8.5pt;">
						<b>Customer service</b>
					</p>
					<p class="MsoNormal">If you have questions regarding our airline or your flights, please send an e-mail to <a href="mailto:customersupport@flyskywork.com">customersupport@flyskywork.com</a>. We will then contact you as quickly as possible.</p>
					<br/>
					<p class="MsoNormal">In addition, you can also contact us daily by phone from 8:00 to 20:00 (local Bern time). The country-specific number to call is provided in the following overview. Please note that charges apply for these calls (mobile rates may vary).</p>
					<br/>-->
				</td>
			</tr>
			<xsl:copy-of select="$ReportFooter"/>
			<!--<div style="page-break-after:always">
				<br/>
			</div>-->
			<!--<xsl:copy-of select="$ReportHeader"/-->>
		</table>

		<!--<table border="0" style="width: 730px; margin: 0px 0px 0px 45px; height: 100px;">
			<tr>
				<td class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal">
					 
					<p class="MsoNormal">The employees of the SkyWork Group – comprising SkyWork Airlines, SkyWork Travel and Aaretal Reisen – wish you a pleasant flight and welcome you on board.</p>
					<p class="MsoNormal"><a target="_blank" href="http://www.flyskywork.com/45-0-Flughafeninfos.html">http://www.flyskywork.com/45-0-Flughafeninfos.html</a></p>
				</td>
			</tr>
		</table>-->
		<!--			<xsl:if test="$Debug = 0">
					<xsl:copy-of select="$CTable"/>

			</xsl:if>-->

		<!--40 rows per page-->
		
		<xsl:copy-of select="$ReportFooter"/>
	</xsl:template>
	<!--End en_terms-->
	<xsl:template name="CheckPageBreak">
		<xsl:param name="RowCount"/>
		<xsl:if test="($RowCount mod 2) = 0 ">
			<table class="tabledetails" cellspacing="0" style="table-layout:fixed;">
				<tr>
					<td>&#xA0;<xsl:value-of select="$RowCount"/></td>
				</tr>
			</table>
			<DIV style="page-break-after:always"></DIV>
			<xsl:call-template name="Filler">
				<xsl:with-param name="fillercount" select="29"/>
			</xsl:call-template>
			<xsl:copy-of select="$PassengerRowsHeader"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="yes" url="..\case\A04YOO.xml" htmlbaseurl="" outputurl="" processortype="msxmldotnet" useresolver="no" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->