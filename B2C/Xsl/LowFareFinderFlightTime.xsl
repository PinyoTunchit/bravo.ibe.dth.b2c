<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							                xmlns:tikLanguage="tik:Language" 
                              exclude-result-prefixes="JCode xsl msxsl tikLanguage">

  <xsl:output method="html" indent="no" omit-xml-declaration="no"/>
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
    
  </xsl:template>

  <xsl:template match="/">
    <ul class="Lowfarepriceselect">
    <xsl:for-each select="Booking/AvailabilityFlight">
      <xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
      <xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
      <xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
      <xsl:sort select="planned_departure_time" order="ascending" data-type="number"/>

      <xsl:variable name = "flight_position" select="position()" />
      <!--Display fare group information-->
      <xsl:variable name="flight_id" select="flight_id" />
      <xsl:variable name="transit_flight_id" select="transit_flight_id" />
      <xsl:variable name="transit_departure_date" select="concat(substring(transit_departure_date,1,4), substring(transit_departure_date,6,2), substring(transit_departure_date,9,2))" />
      <xsl:variable name="transit_airport_rcd" select="transit_airport_rcd" />
      <xsl:variable name="transit_fare_id" select="transit_fare_id" />
      <xsl:variable name="departure_date" select="concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2))" />
      <xsl:variable name="planned_departure_time" select="planned_departure_time" />
      <xsl:variable name="planned_arrival_time" select="planned_arrival_time" />

      <li>
        <span>
          <xsl:call-template name="TimeFormat">
            <xsl:with-param name="Time" select="planned_departure_time"></xsl:with-param>
          </xsl:call-template>
          -
          <xsl:call-template name="TimeFormat">
            <xsl:with-param name="Time" select="$planned_arrival_time"></xsl:with-param>
          </xsl:call-template>
        </span>
        <xsl:choose>
          <xsl:when test="full_flight_flag = 1">
            <b>
              -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_21','Full')" />
            </b>
          </xsl:when>
          <xsl:when test="class_open_flag = 0 and waitlist_open_flag = 0">
            <b>
              -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_21','Full')" />
            </b>
          </xsl:when>
          <xsl:when test="class_open_flag = 0">
            <b>
              -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_22','Closed')" />
            </b>
          </xsl:when>
          <xsl:when test="close_web_sales = 1">
            <b>
              -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_23','Call')" />
            </b>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable name="flight_param_fri">
              {&#034;flight_id&#034;:&#034;<xsl:value-of select="$flight_id" />&#034;,
              &#034;origin_rcd&#034;:&#034;<xsl:value-of select="origin_rcd" />&#034;,
              &#034;destination_rcd&#034;:&#034;<xsl:value-of select="destination_rcd" />&#034;,
              &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="$transit_flight_id" />&#034;,
              &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
              &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="$transit_airport_rcd" />&#034;,
              &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="$transit_fare_id" />&#034;,
              &#034;fare_id&#034;:&#034;<xsl:value-of select="fare_id" />&#034;}
            </xsl:variable>

            <input type="hidden" id="spnTime_{position()}" value="{$flight_param_fri}"/>
            <b onclick="SelectTime('spnTime_{position()}');">
              -&#160;<xsl:value-of select="format-number(total_adult_fare,'#,##0.00')"/>
            </b>
          </xsl:otherwise>
        </xsl:choose>
      </li>
    </xsl:for-each>
  </ul>
</xsl:template>
  <msxsl:script implements-prefix="JCode" language="JavaScript">
    <![CDATA[
     
     function getFormatSelectdate(strDate)
     {
        var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
        var weekday = new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");

        return weekday[dtDate.getDay()] + ', ' + strDate.substring(6,8) + '/' + strDate.substring(4,6);
     }
     
     function getDateAdd(strDate, addValue)
     {
        var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
        var strMonth;
        var strday;
        
        dtDate.setDate(dtDate.getDate()+(addValue))

        if ((dtDate.getMonth() + 1).toString().length == 1)
        {
          strMonth = '0' + (dtDate.getMonth() + 1).toString()
        }
        else
        {
          strMonth = (dtDate.getMonth() + 1).toString()
        }

        if (dtDate.getDate().toString().length == 1)
        {
          strday = '0' + dtDate.getDate().toString()
        }
        else
        {
          strday = dtDate.getDate().toString()
        }

        return (dtDate.getYear().toString() + '' + strMonth + '' + strday);         
     }
		]]>
  </msxsl:script>
</xsl:stylesheet>