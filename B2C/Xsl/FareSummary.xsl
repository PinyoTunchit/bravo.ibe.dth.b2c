<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>

  <xsl:key name="route_group" match="Booking/FlightSegment" use="concat(od_origin_rcd, '|', od_destination_rcd)"/>
  <xsl:key name="tax_rcd_group" match="Booking/Tax" use="tax_rcd"/>
  <xsl:key name="fee_rcd_group" match="Booking/Fee" use="fee_rcd"/>

  <xsl:template name="DateFormat">
    <xsl:param name="Date"></xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>

  <xsl:template match="/">
    <xsl:variable name="Ticket_total" select="(sum(Booking/Quote[charge_type != 'REFUND']/charge_amount) - sum(Booking/Quote[charge_type = 'REFUND']/charge_amount)) + (sum(Booking/Quote[charge_type != 'REFUND']/tax_amount) - sum(Booking/Quote[charge_type = 'REFUND']/tax_amount)) + sum(Booking/Fee[void_by = '00000000-0000-0000-0000-000000000000']/fee_amount_incl)"/>
    <xsl:variable name="Ticket_total_no_vat" select="(sum(Booking/Quote[charge_type != 'REFUND']/charge_amount) - sum(Booking/Quote[charge_type = 'REFUND']/charge_amount)) +  sum(Booking/Fee[void_by = '00000000-0000-0000-0000-000000000000']/fee_amount)"/>
    <xsl:variable name="currency_rcd" select="Booking/Quote[position()=1]/currency_rcd" />
    <xsl:variable name="FlightCount" select="count(Booking/FlightSegment[segment_status_rcd != 'XX'])" />
    <xsl:variable name="Origin" select="Booking/FlightSegment[segment_status_rcd != 'XX'][position() = 1]/origin_name" />
    <xsl:variable name="Destination" select="Booking/FlightSegment[segment_status_rcd != 'XX'][position() = $FlightCount]/destination_name" />
    <xsl:variable name="Total_Point" select="sum(Booking/Quote/redemption_points)" />
    <xsl:variable name="currency_sign">
    </xsl:variable>
    <input type="hidden" id="inWellFee" value="{Booking/Fee[fee_rcd = 'CONV']/fee_amount_incl}" />
    <input type="hidden" id="hdCurrencyCode" value="{$currency_rcd}" />
    <input type="hidden" id="hdCurrencySign" value="{$currency_sign}" />
    <xsl:for-each select="Booking/FlightSegment[count(. | key('route_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">
      <xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
      <xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
      <xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
      <xsl:sort select="departure_time" order="ascending" data-type="number"/>

      <xsl:variable name="od_origin_rcd" select="od_origin_rcd" />
      <xsl:variable name="od_destination_rcd" select="od_destination_rcd" />
      <div class="YourFlightSelected">
        <xsl:choose>
          <xsl:when test="position() = 1">
            <div class="OutboundFlight">
              <div class="Topic FlightType">
                <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_3','Outbound Flight')" />
                <div class="clear-all"></div>
                <div class="ArrowIconout">
                  <img src="App_Themes/Default/Images/outbound.png" class="displayimage" id="imgFromImage" alt=""/>
                </div>

                <div class="clear-all"></div>

              </div>
            </div>
          </xsl:when>

          <xsl:otherwise>
            <div class="InboundFlight">
              <div class="Topic FlightType">
                <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_4','Inbound Flight')" />
                <div class="clear-all"></div>
                <div class="ArrowIconin">
                  <img src="App_Themes/Default/Images/inbound.png" class="displayimage" id="imgFromImage" alt="" />
                </div>

                <div class="clear-all"></div>

              </div>
            </div>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:for-each select="../FlightSegment[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]">
          <xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
          <xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
          <xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
          <xsl:sort select="departure_time" order="ascending" data-type="number"/>

          <div class="YourFlight">
            <ul>
              <li>
                <xsl:value-of select="airline_rcd" />&#160;
                <xsl:value-of select="flight_number" />
              </li>
              <li>
                <xsl:value-of select="aircraft_type_rcd" />
              </li>
            </ul>

            <div class="clear-all"></div>

            <div class="YourFlightTime">
              <div class="YourFlightTimeDept">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_24','Dept')" />&#160;
				
				<span>
					<xsl:call-template name="DateFormat">
						<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
					</xsl:call-template>
				</span>&#160;
				
                <span>
                  <xsl:call-template name="TimeFormat">
                    <xsl:with-param name="Time" select="departure_time"></xsl:with-param>
                  </xsl:call-template>
                </span>
              </div>

              <div class="YourFlightTimeArri">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_25','Arriv')" />&#160;
				
				<span>
					<xsl:call-template name="DateFormat">
						<xsl:with-param name="Date" select="arrival_date"></xsl:with-param>
					</xsl:call-template>
				</span>&#160;
				
                <span>
                  <xsl:call-template name="TimeFormat">
                    <xsl:with-param name="Time" select="arrival_time"></xsl:with-param>
                  </xsl:call-template>
                </span>
              </div>

            </div>

            <div class="clear-all"></div>

            <div class="flightnameleft"></div>
            <div class="flightname">
              <xsl:value-of select="origin_name" />
            </div>
            <div class="flightnameright"></div>

            <div class="clear-all"></div>

            <div class="flightto">
              <img src="App_Themes/Default/Images/to.png" alt="" title=""  />
            </div>

            <div class="clear-all"></div>

            <div class="flightnameleft"></div>
            <div class="flightname">
              <xsl:value-of select="destination_name" />
            </div>
            <div class="flightnameright"></div>

            <div class="clear-all"></div>

          </div>
        </xsl:for-each>

        <div class="FareRule">
          <xsl:value-of select="../Mapping[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd][position() = 1]/restriction_text" />
        </div>

        <div class="clear-all"></div>


        <div class="airfareleftbox"></div>
        <div class="airfaremiddle"></div>
        <div class="airfarerightbox"></div>

        <div class="airfarecontent">
          <div class="airfareheader">
            <div class="airfaretext">
              <div class="lefttitle">
              <xsl:value-of select="tikLanguage:get('Booking_Step_2_22','Airfare')" />
              </div>
              <div class="rightcurrency">
              (<xsl:value-of select="$currency_rcd"/>)
              </div>
            </div>
          </div>

          <div class="clear-all"></div>

          <xsl:variable name="TotalFare" select="sum(../Mapping[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/fare_amount)" />
          <xsl:variable name="number_of_adult" select="count(../Passenger[passenger_type_rcd = 'ADULT'])" />
          <xsl:variable name="number_of_chd" select="count(../Passenger[passenger_type_rcd = 'CHD'])" />
          <xsl:variable name="number_of_inf" select="count(../Passenger[passenger_type_rcd = 'INF'])" />

          <div class="charges">
            <ul>
              <xsl:attribute name="id">
                <xsl:choose>
                  <xsl:when test="position() = 1">ul_Outward_FareInfo</xsl:when>
                  <xsl:otherwise>ul_Return_FareInfo</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <li>
                <div class="Taxes_Fees">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_2_11','Fare')" />
                </div>
                <div class="collapse">
                  <img src="App_Themes/Default/Images/expand.png" alt="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" title="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" />
                </div>

                <div class="charges_price_total">
                  <xsl:value-of select="$currency_sign"/>
                  <xsl:value-of select="format-number($TotalFare,'#,##0.00')"/>
                </div>

                <div class="clear-all"></div>

                <ul>
                  <xsl:if test="$number_of_adult > 0">
                    <li>
                      <xsl:variable name="fare_amount_adult" select="sum(../Mapping[passenger_type_rcd = 'ADULT'][od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/fare_amount)" />

                      <div class="charges_name">
                        <div class="amount">
                          - <xsl:value-of select="$number_of_adult"/>
                        </div>
                        <div class="yourselecttype">
                          <xsl:choose>
                            <xsl:when test="$number_of_adult > 1">
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_6','Adults')" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_16','Adult')" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                      </div>

                      <div class="charges_price">
                        <xsl:value-of select="$currency_sign"/>
                        <xsl:value-of select="format-number($fare_amount_adult,'#,##0.00')"/>
                      </div>

                      <div class="clear-all"></div>
                    </li>
                  </xsl:if>

                  <xsl:if test="$number_of_chd > 0">
                    <li>
                      <xsl:variable name="fare_amount_chd" select="sum(../Mapping[passenger_type_rcd = 'CHD'][od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/fare_amount)" />

                      <div class="charges_name">
                        <div class="amount">
                          - <xsl:value-of select="$number_of_chd"/>
                        </div>
                        <div class="yourselecttype">
                          <xsl:choose>
                            <xsl:when test="$number_of_chd > 1">
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_7','Children')" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_17','Child')" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                      </div>

                      <div class="charges_price">
                        <xsl:value-of select="$currency_sign"/>
                        <xsl:value-of select="format-number($fare_amount_chd,'#,##0.00')"/>
                      </div>

                      <div class="clear-all"></div>
                    </li>
                  </xsl:if>

                  <xsl:if test="$number_of_inf > 0">
                    <li>
                      <xsl:variable name="fare_amount_inf" select="sum(../Mapping[passenger_type_rcd = 'INF'][od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/fare_amount)" />
                      <div class="charges_name">
                        <div class="amount">
                          - <xsl:value-of select="$number_of_inf"/>
                        </div>
                        <div class="yourselecttype">
                          <xsl:choose>
                            <xsl:when test="$number_of_inf > 1">
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_8','Infants')" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_18','Infant')" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                      </div>

                      <div class="charges_price">
                        <xsl:value-of select="$currency_sign"/>
                        <xsl:value-of select="format-number($fare_amount_inf,'#,##0.00')"/>
                      </div>

                      <div class="clear-all"></div>
                    </li>
                  </xsl:if>
                </ul>
              </li>
            </ul>
          </div>

          <xsl:variable name="Group_booking_segment_id" select="../FlightSegment[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/booking_segment_id" />
          <xsl:if test="sum(../Tax[booking_segment_id = $Group_booking_segment_id]/tax_amount) > 0">
            <div class="charges">
              <ul>
                <xsl:attribute name="id">
                  <xsl:choose>
                    <xsl:when test="position() = 1">ul_Outward_TaxInfo</xsl:when>
                    <xsl:otherwise>ul_Return_TaxInfo</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <li>
                  <div class="Taxes_Fees">
                    <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_9','Taxes and Airport charges')" />
                  </div>
                  <div class="collapse">
                    <img src="App_Themes/Default/Images/expand.png" alt="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" title="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" />
                  </div>

                  <div class="charges_price_total">
                    <xsl:value-of select="$currency_sign"/>
                    <xsl:value-of select="format-number(sum(../Tax[booking_segment_id = $Group_booking_segment_id]/sales_amount),'#,##0.00')"/>
                  </div>

                  <div class="clear-all"></div>

                  <ul>
                    <xsl:for-each select="../Tax[count(. | key('tax_rcd_group', tax_rcd)[1]) = 1]">
                      <xsl:variable name="tax_rcd" select="tax_rcd" />
                      <xsl:variable name="total_tax_amount" select="sum(../Tax[booking_segment_id = $Group_booking_segment_id][tax_rcd = $tax_rcd]/sales_amount)" />
                      <xsl:if test="$total_tax_amount > 0">
                        <li>
                          <div class="charges_name">
                            - <xsl:value-of select="display_name" />
                          </div>

                          <div class="charges_price">
                            <xsl:value-of select="$currency_sign"/>
                            <xsl:value-of select="format-number($total_tax_amount, '#,##0.00')"/>
                          </div>
                        </li>
                      </xsl:if>
                    </xsl:for-each>
                  </ul>
                </li>
              </ul>
            </div>

            <div class="clear-all"></div>
          </xsl:if>

          <xsl:if test="count(../Fee[booking_segment_id = $Group_booking_segment_id]) > 0">
            <div class="charges">
              <ul>
                <xsl:attribute name="id">
                  <xsl:choose>
                    <xsl:when test="position() = 1">ul_Outward_FeeInfo</xsl:when>
                    <xsl:otherwise>ul_Return_FeeInfo</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>

                <li>
                  <div class="Taxes_Fees">
                    <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_15','Fees')" />
                  </div>
                  <div class="collapse">
                    <img src="App_Themes/Default/Images/expand.png" alt="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" title="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" />
                  </div>

                  <div class="charges_price_total">
                    <xsl:value-of select="$currency_sign"/>
                    <xsl:value-of select="format-number(sum(../Fee[booking_segment_id = $Group_booking_segment_id]/fee_amount),'#,##0.00')"/>
                  </div>

                  <div class="clear-all"></div>

                  <ul>
                    <xsl:for-each select="../Fee[count(. | key('fee_rcd_group', fee_rcd)[1]) = 1]">
                      <xsl:variable name="fee_rcd" select="fee_rcd" />
                      <xsl:variable name="total_fee_amount" select="sum(../Fee[booking_segment_id = $Group_booking_segment_id][fee_rcd = $fee_rcd]/fee_amount)" />
                      <!--<xsl:if test="$total_fee_amount > 0">-->
                        <li>
                          <div class="charges_name">
                            - <xsl:value-of select="display_name" />
                          </div>

                          <div class="charges_price">
                            <xsl:value-of select="$currency_sign"/>
                            <xsl:value-of select="format-number($total_fee_amount, '#,##0.00')"/>
                          </div>
                        </li>
                      <!--</xsl:if>-->
                    </xsl:for-each>
                  </ul>
                </li>
              </ul>
            </div>

            <div class="clear-all"></div>

          </xsl:if>

          <xsl:variable name="TotalFareIncl" select="sum(../Mapping[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/fare_amount_incl)" />

          <xsl:variable name="TotalTax" select="sum(../Mapping[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/tax_amount)" />
          <xsl:variable name="TotalTaxIncl" select="sum(../Mapping[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/tax_amount_incl)" />

          <xsl:variable name="TotalYQ" select="sum(../Mapping[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/yq_amount)" />
          <xsl:variable name="TotalYQIncl" select="sum(../Mapping[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/yq_amount_incl)" />

          <xsl:variable name="TotalFee" select="sum(../Fee[booking_segment_id = ../FlightSegment[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/booking_segment_id]/fee_amount)" />
          <xsl:variable name="TotalFeeIncl" select="sum(../Fee[booking_segment_id = ../FlightSegment[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/booking_segment_id]/fee_amount_incl)" />

          <xsl:variable name="total_vat" select="number($TotalFareIncl - $TotalFare) + number($TotalTaxIncl - $TotalTax)  + number($TotalYQIncl - $TotalYQ) + number($TotalFeeIncl - $TotalFee)"/>
          <xsl:if test="$total_vat > 0">
            <div class="charges">
              <div class="Taxes_Fees">
                <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_13','VAT')" />
              </div>

              <div class="charges_price_total">
                <xsl:value-of select="$currency_sign"/>
                <xsl:value-of select="format-number($total_vat,'#,##0.00')"/>
              </div>

              <div class="clear-all"></div>
            </div>
          </xsl:if>

          <div class="totalfare fontB">
            <div class="totalflight">
              <xsl:choose>
                <xsl:when test="position() = 1">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_10','Total fare outbound flight')" />
                </xsl:when>

                <xsl:otherwise>
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_11','Total fare inbound flight')" />
                </xsl:otherwise>
              </xsl:choose>
            </div>

            <div class="charges_price_total">
              <xsl:value-of select="$currency_sign"/>
              <xsl:value-of select="format-number(sum(../Mapping[od_origin_rcd = $od_origin_rcd][od_destination_rcd = $od_destination_rcd]/net_total) + number($TotalFeeIncl),'#,##0.00')"/>
            </div>

            <div class="clear-all"></div>

          </div>
        </div>
        <div class="clear-all"></div>

        <div class="airfareleftbottombox"></div>
        <div class="airfaremiddlebottom"></div>
        <div class="airfarerightbottombox"></div>

        <div class="clear-all"></div>
      </div>
      <!--  end yourflight selected -->

      <xsl:if test="position() != last()">
        <div class="clear-all"></div>
      </xsl:if>
    </xsl:for-each>

    <div class="clear-all"></div>

    <div id="dvAdditionalFee" class="YourFlightSelected">
      <div class="airfareleftbox"></div>
      <div class="airfaremiddle"></div>
      <div class="airfarerightbox"></div>
      <div class="airfarecontent">
        <div class="charges noborder">
          <ul id="ulAdditionalFee">
            <li>
              <div class="Taxes_Fees">
                <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_14','Additional Fee')" />
              </div>
              <div class="collapse">
                <img src="App_Themes/Default/Images/expand.png" alt="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" title="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" />
              </div>

              <div class="charges_price_total">
                <div id="dvFareCurrencyDisplay" class="currencydisplay">
                  <xsl:value-of select="$currency_sign"/>
                </div>
                <div id="divAdditionalFeeTotal">
                  <xsl:value-of select="format-number(sum(Booking/Fee[booking_segment_id = '00000000-0000-0000-0000-000000000000' or count(booking_segment_id) = 0]/fee_amount),'#,##0.00')"/>
                </div>
              </div>


              <div class="clear-all"></div>

              <ul>
                <xsl:for-each select="Booking/Fee[booking_segment_id = '00000000-0000-0000-0000-000000000000' or count(booking_segment_id) = 0][count(. | key('fee_rcd_group', fee_rcd)[1]) = 1]">
                  <xsl:variable name="fee_rcd" select="fee_rcd" />
                  <li>
                    <div class="charges_name">
                      - <xsl:value-of select="display_name" />
                    </div>

                    <div class="charges_price">
                      <xsl:value-of select="$currency_sign"/>
                      <xsl:value-of select="format-number(sum(../Fee[booking_segment_id = '00000000-0000-0000-0000-000000000000' or count(booking_segment_id) = 0][fee_rcd = $fee_rcd]/fee_amount),'#,##0.00')"/>
                    </div>
                  </li>
                </xsl:for-each>

                <li id="liCreditCardFare" style="display:none;">
                  <div class="charges_name">
                    - <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_12','Credit Card')" />
                  </div>

                  <div class="currencydisplay">
                    <xsl:value-of select="$currency_sign"/>
                  </div>

                  <div id="dvCreditCardValue" class="charges_price">
                    0
                  </div>
                </li>
              </ul>
            </li>
          </ul>

        </div>

      </div>
      <div class="airfareleftbottombox"></div>
      <div class="airfaremiddlebottom"></div>
      <div class="airfarerightbottombox"></div>

    </div>

    <div class="clear-all"></div>
    <xsl:variable name="SubTotal" select="sum(Booking/Mapping/net_total) + sum(Booking/Fee/fee_amount_incl)" />
    <input type="hidden" id="hdSubTotal" name="hdSubTotal" value="{format-number($SubTotal,'0.00')}" />
  </xsl:template>
</xsl:stylesheet>