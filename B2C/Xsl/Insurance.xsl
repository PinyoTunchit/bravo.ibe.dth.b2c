﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
        xmlns:tikLanguage="tik:Language"
				xmlns:JCode="http://www.tiksystems.com/B2C"
        xmlns='http://www.mondial-assistance.com/ecommerce/schema'>
    <xsl:template match='/'>
    <table>
      <xsl:for-each select="//productsAvailable">
        <tr>
          <td>
            <a href="{productURL}" target="_blank">
              <xsl:value-of select="label" />
            </a>
          </td>
          <td>
            <xsl:value-of select="premiumProduct" />
          </td>
          <td>
            <div class="ButtonAlignRight">
              <a href="" title="{tikLanguage:get('Confirm','Confirm')}" onclick="SelectInsuarance('{productVariant/@id}','{premiumProduct}','{label}');return false;">
                <div class="BTN-Left"></div>
                <div class="BTN-Middle">
                  <xsl:value-of select="tikLanguage:get('Confirm', 'Confirm')" />
                </div>
                <div class="BTN-Right"></div>
              </a>
            </div>
          </td>
        </tr>
      </xsl:for-each>
      <tr>
        <td>
          No
        </td>
        <td></td>
        <td>
          <div class="ButtonAlignRight">
            <a href="" title="{tikLanguage:get('Confirm','Confirm')}" onclick="CloseInsurance(true);return false">
              <div class="BTN-Left"></div>
              <div class="BTN-Middle">
                <xsl:value-of select="tikLanguage:get('Confirm', 'Confirm')" />
              </div>
              <div class="BTN-Right"></div>
            </a>
          </div>
        </td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>
