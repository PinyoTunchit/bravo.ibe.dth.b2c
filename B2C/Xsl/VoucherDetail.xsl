<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:tikLanguage="tik:Language">
  <xsl:output method="html" indent="no" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:template match="/">
  
	<div class="PaymentDetail">
		<div class="whiteboxtopleft"></div>
		<div class="whiteboxtopcontent"></div>
		<div class="whiteboxtopright"></div>
	</div>
    <xsl:variable name="currency_rcd" select="Booking/ArrayOfVoucher/Voucher[position()=1]/currency_rcd" />
  	<div class="nopadding-margin">
    <table id="tbVoucherHeader" border="0" class="TBLVoucher">
		<tr class="Header">
			<td class="HeadCOL1"></td>
			<td class="HeadCOL2">
				<xsl:value-of select="tikLanguage:get('Booking_Step_5_27','Voucher Number')" />
			</td>
			<td class="HeadCOL3">
				<xsl:value-of select="tikLanguage:get('Booking_Step_5_31','Voucher Amount')" />
				(<xsl:value-of select="$currency_rcd"/>)
			</td>
			<td class="HeadCOL4">
				<xsl:value-of select="tikLanguage:get('Booking_Step_5_32','Voucher Status')" />
			</td>
		</tr>
		
      <xsl:for-each select="Booking/ArrayOfVoucher/Voucher">
        <tr>
          <td class="BodyCOL1">
            <xsl:variable name="payment_total">
              <xsl:choose>
                <xsl:when test="count(payment_total) = 0">
                  <xsl:value-of select="0"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="payment_total"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:choose>
              <xsl:when test="(number(voucher_value) - number($payment_total)) > number(../../Header/balance)">
                <xsl:if test="voucher_status_rcd = 'OPEN'">
                  <input type="hidden" name="hdVcAmount" value="{format-number(number(voucher_value) - number(payment_total),'###0.00')}" />
                  <span class="TextRed">
                    <xsl:choose>
                      <xsl:when test="position() = 1">
                        <input name="nVoucher" type="radio" checked="checked" value="{concat(voucher_id,'|',voucher_number,'|',form_of_payment_rcd,'|',form_of_payment_subtype_rcd)}" />
                      </xsl:when>
                      <xsl:otherwise>
                        <input name="nVoucher" type="radio" value="{concat(voucher_id,'|',voucher_number,'|',form_of_payment_rcd,'|',form_of_payment_subtype_rcd)}" />
                      </xsl:otherwise>
                    </xsl:choose>
                  </span>
                </xsl:if>
                <xsl:if test="voucher_status_rcd = 'EXP'">
                  <span class="TextRed">
                    <xsl:value-of select="tikLanguage:get('Alert_Message_151','please check voucher condition')" />
                  </span>  
                </xsl:if>
                <xsl:if test="voucher_status_rcd = 'NP'">
                  <span class="TextRed">
                    <xsl:value-of select="tikLanguage:get('Alert_Message_151','please check voucher condition')" />
                  </span>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <span class="TextRed">
                  <xsl:value-of select="tikLanguage:get('Alert_Message_37','Voucher amount not enough')" />
                </span>
              </xsl:otherwise>
            </xsl:choose>
          </td>
          <td class="BodyCOL2">
            <xsl:value-of select="voucher_number"/>
          </td>
          <td class="BodyCOL3">
            <xsl:choose>
              <xsl:when test="count(payment_total) = 0">
                <xsl:value-of select="format-number(number(voucher_value),'#,##0.00')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(number(voucher_value) - number(payment_total),'#,##0.00')"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>
          <td class="BodyCOL4">
            <xsl:value-of select="voucher_status_rcd"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
	</div>
	
	<div class="clear-all"></div>
	
	<div class="BottominnerBox">
		<div class="whiteboxbottomleft"></div>
		<div class="whiteboxbottomcontent"></div>
		<div class="whiteboxbottomright"></div>
	</div>
  </xsl:template>
</xsl:stylesheet>