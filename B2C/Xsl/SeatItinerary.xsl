<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:tikLanguage="tik:Language">
  <xsl:output method="html" indent="no" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>
  
  <xsl:variable name="transit_points" select="Booking/FlightSegment/transit_points"></xsl:variable>

  <xsl:template match="/">
	    <div class="bookinginform">
			<!-- <div class="BoxtopleftCorner"></div>
			<div class="Boxseatmiddle">Flight Selection</div>
			<div class="BoxtoprightCorner"></div> 
			
			<div class="clear-all"></div>-->
			
			<div class="whiteboxtopleft"></div>
			<div class="innerseatboxmiddle"></div>
			<div class="whiteboxtopright"></div>
			
			<div class="clear-all"></div>
			
			<table id="tbSeatItinerary" class="TBLSeat innerwhitebox">
				<tr class="Header">
					<td class="HeadCOL1">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_2','Flight')" />
					</td>
					
					<td class="HeadCOL2">
						<xsl:value-of select="tikLanguage:get('Flight_Selection_3','From')" />
					</td>
					
					<td class="HeadCOL3">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_4','To')" />
					</td>
          <xsl:if test="string-length($transit_points) > 0">
            <td class="HeadCOL3">
              Via
            </td>
          </xsl:if>

          <td class="HeadCOL4">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_5','Date')" />
					</td>
					
					<td class="HeadCOL5">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_6','Dep')" />
					</td>
					
					<td class="HeadCOL5">
						<xsl:value-of select="tikLanguage:get('Seat_Reservation_19','Arr')" />
					</td>
					
					 <td class="HeadCOL6"></td>
				</tr>
				<xsl:for-each select="Booking/FlightSegment[segment_status_rcd = 'HK' or segment_status_rcd = 'NN']">
				
					<xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
					<xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
					<xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>
					<xsl:variable name="ClassName">
						<xsl:choose>
							<xsl:when test="../Setting/selected_flight_id = flight_id">
								<xsl:value-of select="'Selected'"/>
							</xsl:when>
							
							<xsl:otherwise>
								<xsl:value-of select="'NotSelected'"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
				
					<tr id="trSeatFlight_{position()}" class="{$ClassName}">
						<td class="BodyCOL1">
							<xsl:value-of select="airline_rcd" />&#160;<xsl:value-of select="flight_number" />
						</td>
						
						<td class="BodyCOL2">
							<xsl:variable name="origin_rcd" select="origin_rcd"/>
							<xsl:choose>
								<xsl:when test="count(origin_name) = 0">
									<xsl:value-of select="../FlightSegment[origin_rcd = $origin_rcd][position() = 1]/origin_name" />
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:value-of select="origin_name" />
								</xsl:otherwise>
							</xsl:choose>
						</td>
						
						<td class="BodyCOL3">
							<xsl:variable name="destination_rcd" select="destination_rcd"/>
							<xsl:choose>
								<xsl:when test="count(origin_name) = 0">
									<xsl:value-of select="../FlightSegment[destination_rcd = $destination_rcd][position() = 1]/destination_name" />
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:value-of select="destination_name" />
								</xsl:otherwise>
							</xsl:choose>
						</td>

            <xsl:choose>
              <xsl:when test="string-length(transit_points) > 0">
                <td class="BodyCOL3">
                  <xsl:value-of select="transit_points_name"  />
                </td>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="string-length($transit_points) > 0">
                  <td class="BodyCOL3">
                    &#160;
                  </td>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>

            <td class="BodyCOL4">
							<xsl:call-template name="DateFormat">
								<xsl:with-param name="Date" select="departure_date"></xsl:with-param>
							</xsl:call-template>
						</td>
						
						<td class="BodyCOL5">
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="departure_time"></xsl:with-param>
							</xsl:call-template>
						</td>
						
						<td class="BodyCOL5">
							<xsl:call-template name="TimeFormat">
								<xsl:with-param name="Time" select="arrival_time"></xsl:with-param>
							</xsl:call-template>
						</td>
						
						<td class="BodyCOL6">
							<xsl:choose>
								<xsl:when test="temp_seatmap_flag = 0 and seatmap_flag = 0">
									<!--button-->
									<div class="buttonCornerLeftGreyBlue"></div>
									<div class="buttonContentGreyBlue">
										<xsl:value-of select="tikLanguage:get('Seat_Reservation_20','Selected')" />
									</div>
									<div class="buttonCornerRightGreyBlue"></div>
									<!-- end button-->
								</xsl:when>
								
								<xsl:otherwise>
									<!--button-->
									<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
									<xsl:choose>
										<xsl:when test="count(../Mapping[passenger_status_rcd = 'OK'][booking_segment_id = $booking_segment_id][seat_number != '']) = count(../Mapping[passenger_status_rcd = 'OK'][booking_segment_id = $booking_segment_id]) and (segment_status_rcd != 'NN')">
											<div class="buttonCornerLeftGreyBlue"></div>
											<div class="buttonContentGreyBlue">
												<xsl:value-of select="tikLanguage:get('Seat_Reservation_20','Selected')" />
											</div>
											<div class="buttonCornerRightGreyBlue"></div>
										</xsl:when>
										
										<xsl:otherwise>
											<div class="seatselectionbutton">
												<a href="javascript:showSeatMappingFlightSelect('trSeatFlight_{position()}', '{flight_id}', '{origin_rcd}', '{destination_rcd}', '{boarding_class_rcd}', '{booking_class_rcd}');" class="defaultbutton">
													<span>
														<xsl:value-of select="tikLanguage:get('Seat_Reservation_20','Selected')" />
													</span>
												</a>
											</div>
										</xsl:otherwise>
										
									</xsl:choose>
									<!-- end button-->
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:for-each>
			</table>
			
			<div class="clear-all"></div>
			
			<div class="BottominnerBox">
				<div class="whiteboxbottomleft"></div>
				<div class="innerseatboxmiddlebottom"></div>
				<div class="whiteboxbottomright"></div>
			</div>
			
			<div class="clear-all"></div>
	    </div>
	</xsl:template>
</xsl:stylesheet>