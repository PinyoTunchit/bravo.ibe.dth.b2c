<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">
	<xsl:template name="DateFormat">
		<xsl:param name="DateTime" />
		<!-- new date format 2006-01-14T08:55:22 -->
		<xsl:value-of select="substring($DateTime,9,2)" />/<xsl:value-of select="substring($DateTime,6,2)" />/<xsl:value-of select="substring($DateTime,0,5)" />
	</xsl:template>
	
	<xsl:template  match="Client">
		<div class="BoxGrayContent">
			<div class="BoxGrayContentHeader">
        <xsl:value-of select="tikLanguage:get('FFP_My_Booking_32','My Mileage Details')" />
      </div>
			<div class="clearboth"></div>
			<div class="CurrentStatement">
        <xsl:value-of select="tikLanguage:get('FFP_My_Booking_33','My current account statement')" />
      </div>
			<div class="clearboth"></div>
			<div class="MilleageUserInfo">
				<div class="MilleageName">
					<xsl:value-of select="title_rcd"/>&#160;<xsl:value-of select="firstname"/>&#160;<xsl:value-of select="lastname"/>
				</div>
				<div class="clearboth"></div>
				<div class="MilleageContactAdd">
					<xsl:value-of select="address_line1"/>&#160;<xsl:value-of select="address_line2"/><br/>
					<xsl:value-of select="street"/>&#160;<xsl:value-of select="district"/>&#160;<xsl:value-of select="state"/><br/>
					<xsl:value-of select="city"/>&#160;<xsl:value-of select="province"/>&#160;<xsl:value-of select="zip_code"/><br/>
					<xsl:value-of select="po_box"/>&#160;<xsl:value-of select="country_rcd"/>
				</div>
			</div>

			<div class="clearboth"></div>
			<div class="MilleageSubHeader">
        <xsl:value-of select="tikLanguage:get('FFP_My_Booking_34','Customer Number')" />
        <span>
					<xsl:value-of select="client_number"/>
				</span>
			</div>
			<div class="clearboth"></div>
			<div class="MilleageBox">
				<div class="MilleageBoxLeft">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_35','Your Current Status')" />
        </div>
				<div class="MilleageBoxRight">
					<xsl:value-of select="member_level_display_name"/></div>
			</div>
			<div class="MilleageBox">
				<div class="MilleageBoxLeft">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_36','Status Validity Until')" />
        </div>
				<div class="MilleageBoxRight">
					<xsl:call-template name="DateFormat">
						<xsl:with-param name="DateTime" select="member_since_date"></xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="clearboth"></div>
			<div class="MilleageSubHeader">
        <xsl:value-of select="tikLanguage:get('FFP_My_Booking_37','Current Account Balance')" />
        <span></span>
			</div>
			<div class="clearboth"></div>
			<div class="MilleageBox">
				<div class="MilleageBoxLeft">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_17','Award Points')" /> 
        </div>
				<div class="MilleageBoxRight">
					<xsl:value-of select="format-number(ffp_total,'#,##0.00')"/>
				</div>
			</div>
			<div class="MilleageBox">
				<div class="MilleageBoxLeft">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_18','Status Points')" />
        </div>
				<div class="MilleageBoxRight">
					<xsl:value-of select="format-number(ffp_total,'#,##0.00')"/>
				</div>
			</div>
			<div class="MilleageBox">
				<div class="MilleageBoxLeft">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_38','Your Current Account Balance')" />
        </div>
				<div class="MilleageBoxRight">
					<xsl:value-of select="format-number(ffp_balance,'#,##0.00')"/>
				</div>
			</div>
			<div class="MilleageBox">
				<div class="MilleageBoxLeft">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_39','Points to Maintain Tier')" />
        </div>
				<div class="MilleageBoxRight">
					<xsl:value-of select="format-number(keep_point,'#,##0.00')"/>
				</div>
			</div>
			<div class="clearboth"></div>
		</div>
	</xsl:template>
</xsl:stylesheet>