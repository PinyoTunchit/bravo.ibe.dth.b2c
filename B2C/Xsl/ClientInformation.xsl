<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">
	<xsl:template match="Client">
		<!-- Start Frequent Traveller by hnin -->
		<input type="hidden" id="hdClientId" name="hdClientId" value="{client_profile_id}"/>
		<input type="hidden" id="hdClientNumber" value="{client_number}"/>
		
		<div class="TBLFrequentFlyerUser"><xsl:value-of select="firstname"/>&#160;<xsl:value-of select="lastname"/></div>
		<div class="clear-all"></div>
		<!-- **************************** -->
		<!-- <div class="TBLFrequentFlyerBox">
			<div class="BoxHeader">
				<div class="TicketText">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_15','FREQUENT TRAVELLER')" />
        </div>
			</div>
			
			<div class="TBLFrequentFlyerID">
				<xsl:value-of select="client_number"/>
			</div>
			<div class="TBLFrequentFlyerPoints">
				<ul>
					<li>
						<div class="PointsLeft">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_16','Member Tier')" />
            </div>
						<div class="PointsRight">
							<xsl:value-of select="member_level_display_name"/>
						</div>
					</li>
					<li>
						<div class="PointsLeft">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_17','Award Points')" />
            </div>
						<div class="PointsRight">
							<xsl:value-of select="format-number(ffp_total,'#,##0.00')"/>
						</div>
					</li>
					<li>
						<div class="PointsLeft">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_18','Status Points')" />
            </div>
						<div class="PointsRight">
							<span id="dvCurrentPoint">
								<xsl:value-of select="format-number(ffp_balance,'#,##0.00')"/>
							</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="clear-all"></div>
			<div class="line"></div>
			<div class="clear-all"></div>
			<div class="TBLFrequentFlyerDetails">
				<ul>
					<li>
						<a href="Javascript:LoadMilleageDetail();">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_19','Milleage account in detail')" />
            </a>
					</li>
					<li>
						<a href="Javascript:LoadRegistration_Edit();">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_20','My Profile')" />
            </a>
					</li>
					<li>
						
					</li>
					<li>
						
					</li>
					<li>
						<a href="Javascript:ClientLogOff();">
              <xsl:value-of select="tikLanguage:get('FFP_My_Booking_23','Log off')" />
            </a>
					</li>
				</ul>
			</div>
			<div class="clear-all"></div>
			<div class="line"></div>
			<div class="clear-all"></div>
			<div class="BlueBoxFooter"></div>
		</div> -->
		
		<!-- ************************************************** -->
		<div class="WrapperPanelRight">
			<div class="col-two">
				<div class="topbox">
					<div class="homerightnavtopleft"></div>
					<div class="homerightnavtop"></div>
					<div class="homerightnavtopright"></div>
				</div>
				
				<div class="gradient Rightnav">
					<div class="rightnavmenulist">
						<div class="airfareleftbox"></div>
						<div class="airfaremiddle"></div>
						<div class="airfarerightbox"></div>
						
						<div class="airfarecontent">
							<a href="Javascript:LoadMyBooking();" title="{tikLanguage:get('FFP_My_Booking_22','My Booking')}">
								<div class="menuicon">
									<img src="App_Themes/Default/Images/changebooking.png" alt="{tikLanguage:get('FFP_My_Booking_22','My Booking')}" title="{tikLanguage:get('FFP_My_Booking_22','My Booking')}" />
								</div>
								
								<div class="menuname">
									<xsl:value-of select="tikLanguage:get('FFP_My_Booking_22','My Booking')" />
								</div>
							</a>
						</div>
						
						<div class="clear-all"></div>
						
						<div class="airfareleftbottombox"></div>
						<div class="airfaremiddlebottom"></div>
						<div class="airfarerightbottombox"></div>
						
						<div class="clear-all"></div>

					</div>
					
					<div class="clear-all"></div>
					
					<div class="rightnavmenulist">
						<div class="airfareleftbox"></div>
						<div class="airfaremiddle"></div>
						<div class="airfarerightbox"></div>
						
						<div class="airfarecontent">
							<a href="Javascript:LoadRegistration_Edit();" title="{tikLanguage:get('FFP_My_Booking_20','My Profile')}">
								<div class="menuicon">
									<img src="App_Themes/Default/Images/myprofile.png" alt="{tikLanguage:get('FFP_My_Booking_20','My Profile')}" title="{tikLanguage:get('FFP_My_Booking_20','My Profile')}" />
								</div>
								
								<div class="menuname">
									<xsl:value-of select="tikLanguage:get('FFP_My_Booking_20','My Profile')" />
								</div>
							</a>
						</div>
						
						<div class="clear-all"></div>
						
						<div class="airfareleftbottombox"></div>
						<div class="airfaremiddlebottom"></div>
						<div class="airfarerightbottombox"></div>
						
						<div class="clear-all"></div>

					</div>
					
					<div class="clear-all"></div>

					<div class="rightnavmenulist">
						<div class="airfareleftbox"></div>
						<div class="airfaremiddle"></div>
						<div class="airfarerightbox"></div>
						
						<div class="airfarecontent">
						
							<a href="Javascript:LoadRegistration_EditPassword();" title="{tikLanguage:get('FFP_My_Booking_21','Change Password')}">
								<div class="menuicon">
									<img src="App_Themes/Default/Images/changepassword.png" alt="Change Password" title="{tikLanguage:get('FFP_My_Booking_21','Change Password')}" />
								</div>
								
								<div class="menuname">
									<xsl:value-of select="tikLanguage:get('FFP_My_Booking_21','Change Password')" />
								</div>
							</a>
						
						</div>
						
						<div class="clear-all"></div>
						
						<div class="airfareleftbottombox"></div>
						<div class="airfaremiddlebottom"></div>
						<div class="airfarerightbottombox"></div>
						
						<div class="clear-all"></div>

					</div>

				</div>
				
				<div class="clear-all"></div>
				
				<div class="bottombox">
					<div class="homerightnavbottomleft"></div>
					<div class="homerightnavbottom"></div>
					<div class="homerightnavbottomright"></div>
				</div>
			</div>
		</div>
		

		<div class="clear-all"></div>
		<!-- End Frequent Traveller by hnin -->
	</xsl:template>
</xsl:stylesheet>