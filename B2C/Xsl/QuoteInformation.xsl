<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:tikLanguage="tik:Language">

	<xsl:output method="html" indent="no"/>

	<xsl:key name="passenger_type_rcd_group" match="Booking/Passenger" use="passenger_type_rcd"/>
	<xsl:key name="fee_id_group" match="Booking/Fee[void_by = '00000000-0000-0000-0000-000000000000']" use="fee_id"/>

	<xsl:template name="DateFormat">
		<xsl:param name="Date">
		</xsl:param>
		<xsl:if test="string-length($Date)!=0">
			<xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="TimeFormat">
		<xsl:param name ="Time"></xsl:param>

		<xsl:if test="string-length($Time) =  3 ">
			0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 4 ">
			<xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
		</xsl:if>

		<xsl:if test="string-length($Time) = 2 ">
			00:<xsl:value-of select="$Time"  />
		</xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
	</xsl:template>

	<xsl:template match="/">
		<xsl:variable name="Ticket_total" select="(sum(Booking/Quote[charge_type != 'REFUND']/charge_amount) - sum(Booking/Quote[charge_type = 'REFUND']/charge_amount)) + (sum(Booking/Quote[charge_type != 'REFUND']/tax_amount) - sum(Booking/Quote[charge_type = 'REFUND']/tax_amount)) + sum(Booking/Fee[void_by = '00000000-0000-0000-0000-000000000000']/fee_amount_incl)"/>
		<xsl:variable name="currency_rcd" select="Booking/Quote[position()=1]/currency_rcd" />
		<table class="TBLYourItinerary2">
			<tr>
				<td class="HeadCOL1">
					<xsl:value-of select="tikLanguage:get('Booking_Step_3_10','Passenger')" />
				</td>
				<td class="HeadCOL2">
					<xsl:value-of select="tikLanguage:get('Booking_Step_3_11','Charges')" />
				</td>
				<td class="HeadCOL3">
					<xsl:value-of select="tikLanguage:get('Booking_Step_3_12','Amount')" />
					&#160;(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL4">
					<xsl:value-of select="tikLanguage:get('Booking_Step_3_13','Vat')" />
					&#160;(<xsl:value-of select="$currency_rcd"/>)
				</td>
				<td class="HeadCOL5">
					<xsl:value-of select="tikLanguage:get('Booking_Step_3_14','Total Price')" />
					&#160;(<xsl:value-of select="$currency_rcd"/>)
				</td>
			</tr>
			<!--Go through quote information-->
			<xsl:for-each select="Booking/Passenger[count(. | key('passenger_type_rcd_group', passenger_type_rcd)[1]) = 1]">
				<xsl:variable name="passenger_type_rcd" select="passenger_type_rcd"/>
				<xsl:for-each select="../Quote[passenger_type_rcd=$passenger_type_rcd]">
					<xsl:variable name="TotalCharge">
						<xsl:if test="charge_type != 'REFUND'">
							<xsl:value-of select="sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
						</xsl:if>
						<xsl:if test="charge_type = 'REFUND'">
							<xsl:value-of select="sum(../Quote[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
						</xsl:if>
					</xsl:variable>
					<xsl:if test="position()=1">
						<xsl:if test="redemption_points = 0">
							<tr>
								<td class="BodyCOL1">
									<xsl:if test="position()!=last()">
										<xsl:value-of select="sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)"/>
									</xsl:if>
									<xsl:if test="position()=last()">
										<xsl:value-of select="passenger_count"/>
									</xsl:if>
									&#160;<xsl:value-of select="passenger_type_rcd"/>
								</td>
								<td class="BodyCOL2">
									<xsl:value-of select="charge_name"/>
								</td>
								<td class="BodyCOL3">
									<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
								</td>
								<td class="BodyCOL4">
									<xsl:if test="tax_amount > 0">
										<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
									</xsl:if>
								</td>
								<td class="BodyCOL5">
									<xsl:value-of select="format-number($TotalCharge + sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="redemption_points != 0">
							<tr>
								<td class="BodyCOL1">
									<xsl:if test="position()!=last()">
										<xsl:value-of select="sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)"/>
									</xsl:if>
									<xsl:if test="position()=last()">
										<xsl:value-of select="passenger_count"/>
									</xsl:if>
									&#160;<xsl:value-of select="passenger_type_rcd"/>
								</td>
								<td class="BodyCOL2">
									Spend points
								</td>
								<td class="BodyCOL3">
									<xsl:value-of select="sum(../Quote[charge_type = 'FARE'][passenger_type_rcd = $passenger_type_rcd]/redemption_points)"/>&#xA0;
								</td>
								<td class="BodyCOL4">
									&#xA0;
								</td>
								<td class="BodyCOL5">
									&#xA0;
								</td>
							</tr>
							<tr>
								<td class="BodyCOL1">
									&#160;
								</td>
								<td class="BodyCOL2">
									<xsl:value-of select="charge_name"/>
								</td>
								<td class="BodyCOL3">
									<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
								</td>
								<td class="BodyCOL4">
									<xsl:if test="tax_amount > 0">
										<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
									</xsl:if>
								</td>
								<td class="BodyCOL5">
									<xsl:value-of select="format-number($TotalCharge + sum(../Quote[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
								</td>
							</tr>
						</xsl:if>	
					</xsl:if>
					<xsl:if test="position()!=1">
						<xsl:if test="position()=last()">
							<xsl:if test="charge_type != 'REFUND'">
								<tr>
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#160;</td>
								</tr>
							</xsl:if>
							<xsl:if test="charge_type = 'REFUND'">
								<tr>
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										-<xsl:value-of select="format-number($TotalCharge + sum(../Quote[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#160;</td>
								</tr>
							</xsl:if>
						</xsl:if>
						<xsl:if test="position()!=last()">
							<xsl:if test="charge_type != 'REFUND'">
								<tr>
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">
										<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>&#xA0;
									</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>
									</td>
									<td class="BodyCOL5">&#160;</td>
								</tr>
							</xsl:if>
							<xsl:if test="charge_type = 'REFUND'">
								<tr>
									<td class="BodyCOL1">&#xA0;</td>
									<td class="BodyCOL2">
										<xsl:value-of select="charge_name"/>
									</td>
									<td class="BodyCOL3">&#160;</td>
									<td class="BodyCOL4">
										<xsl:if test="tax_amount > 0">
											<xsl:value-of select="format-number(tax_amount,'#,##0.00')"/>&#xA0;
										</xsl:if>

									</td>
									<td class="BodyCOL5">
										-<xsl:value-of select="format-number($TotalCharge + sum(../Quote[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#xA0;
									</td>
								</tr>
							</xsl:if>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>

			<!--Go through fee information-->
			<xsl:for-each select="Booking/Fee[void_by = '00000000-0000-0000-0000-000000000000'][count(. | key('fee_id_group', fee_id)[1]) = 1]">
				<xsl:sort select="fee_id"/>
				<xsl:variable name="fee_id" select="fee_id"/>
				<xsl:for-each select="../Fee[void_by = '00000000-0000-0000-0000-000000000000'][fee_id = $fee_id]">
					<xsl:if test="position() = 1">
						<tr>
							<td class="BodyCOL1">&#xA0;</td>
							<td class="BodyCOL2">
								<xsl:value-of select="display_name"/>
							</td>
							<td class="BodyCOL3">
								<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>&#xA0;
							</td>
							<td class="BodyCOL4">
								<xsl:variable name="vat" select="fee_amount_incl - fee_amount" />
								<xsl:if test="$vat > 0">

									<xsl:value-of select="format-number($vat,'#,##0.00')"/>
								</xsl:if>
								&#xA0;
							</td>
							<td class="BodyCOL5">
								<xsl:value-of select="format-number(sum(../Fee[void_by = '00000000-0000-0000-0000-000000000000'][fee_id = $fee_id]/fee_amount_incl),'#,##0.00')"/>&#xA0;
							</td>
						</tr>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>
			<!--Summary-->
			<tr>
				<td class="BodyCOL1">&#xA0;</td>
				<td class="BodyCOL2">&#xA0;</td>
				<td class="BodyCOL3">&#xA0;</td>
				<td class="BodyCOL4">&#160;</td>
				<td class="BodyCOL5Total">
					<xsl:value-of select="tikLanguage:get('Booking_Step_3_14','Total')" />
					&#160;&#160;<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#xA0;&#xA0;
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>