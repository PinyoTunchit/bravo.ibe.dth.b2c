<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>
  <xsl:key name="passenger_id_group" match="Booking/Mapping" use="passenger_id"/>
  <xsl:key name="booking_segment_id_group" match="Booking/Mapping" use="booking_segment_id"/>
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>
  <xsl:key name="fee_rcd_group" match="Booking/ServiceFees/*" use="fee_rcd"/>
  <xsl:key name="passenger_id_group" match="Booking/Mapping" use="passenger_id"/>
  <xsl:key name="route_group" match="Booking/FlightSegment" use="concat(od_origin_rcd, '|', od_destination_rcd)"/>
  <xsl:variable name="currency_rcd" select="Booking/Mapping/currency_rcd" />
  <xsl:variable name="b2b_allow_seat_assignment_flag" select="Booking/Setting/b2b_allow_seat_assignment_flag" />

  <xsl:template match="/">
    <xsl:variable name="selected_passenger_id" select="Booking/Setting/selected_passenger_id"/>

    <xsl:variable name="begin_dept_date">
      <xsl:call-template name="DateFormat">
        <xsl:with-param name="Date" select="Booking/FlightSegment[position() = 1]/departure_date"></xsl:with-param>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="last_dept_date">
      <xsl:call-template name="DateFormat">
        <xsl:with-param name="Date" select="Booking/FlightSegment[position() = last()]/departure_date"></xsl:with-param>
      </xsl:call-template>
    </xsl:variable>

    <xsl:if test="$b2b_allow_seat_assignment_flag=1">
      <div class="ButtonAlignLeft">
        <a href="javascript:GetSeatMap('{Booking/FlightSegment[position() = 1]/flight_id}', '{Booking/FlightSegment[position() = 1]/origin_rcd}', '{Booking/FlightSegment[position() = 1]/destination_rcd}', '{Booking/FlightSegment[position() = 1]/boarding_class_rcd}', '{Booking/FlightSegment[position() = 1]/booking_class_rcd}');" title="{tikLanguage:get('Booking_Step_4_53','Select Seat')}" class="defaultbutton">
          <span>
            <xsl:value-of select="tikLanguage:get('Booking_Step_4_53','Select Seat')" />
          </span>
        </a>
      </div>
    </xsl:if>

    <div class="clear-all"></div>
    <input id="spnDD" type="hidden" value="{$begin_dept_date}" />
    <input id="spnLDD" type="hidden" value="{$last_dept_date}" />

    <!--Seat information -->
    <div class="DisplaySeatSelection">
      <xsl:if test="count(Booking/Mapping[count(lastname) > 0 or string-length(lastname) > 0]) > 0">
        <div class="whiteboxtopleft"></div>
        <div class="whiteboxtopcontent paxinnerwhitebox"></div>
        <div class="whiteboxtopright"></div>

        <table class="SeatInformation">
          <tr class="Header">
            <td class="HeadCOL1">
              <xsl:value-of select="tikLanguage:get('Booking_Step_3_40','traveller')" />
            </td>

            <xsl:variable name="origin_destination" select="concat(Booking/FlightSegment[position() = 1]/od_origin_rcd, Booking/FlightSegment[position() = 1]/od_destination_rcd)" />

            <xsl:for-each select="Booking/Mapping[count(. | key('booking_segment_id_group', booking_segment_id)[1]) = 1]">
              <xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
              <xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
              <xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>

              <td class="HeadCOL{position() + 1}">
                <xsl:choose>
                  <xsl:when test="$origin_destination = concat(od_origin_rcd, od_destination_rcd)">
                    <xsl:value-of select="tikLanguage:get('Booking_Step_3_16','Outbound')" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="tikLanguage:get('Booking_Step_3_17','Inbound')" />
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </xsl:for-each>

          </tr>
          <xsl:for-each select="Booking/Mapping[count(. | key('passenger_id_group', passenger_id)[1]) = 1]">
            <xsl:if test="count(lastname) > 0 and string-length(lastname) > 0">
              <xsl:variable name="passenger_id" select="passenger_id" />
              <tr class="Body">
                <td class="BodyCOL1">
                  <xsl:value-of select="lastname"/>&#160;
                  <xsl:value-of select="firstname"/>&#160;
                  <xsl:value-of select="title_rcd"/>
                </td>
                <xsl:for-each select="../Mapping[passenger_id = $passenger_id]">
                  <xsl:sort select="substring(departure_date,1,4)" order="ascending" data-type="number"/>
                  <xsl:sort select="substring(departure_date,6,2)" order="ascending" data-type="number"/>
                  <xsl:sort select="substring(departure_date,9,2)" order="ascending" data-type="number"/>

                  <td class="BodyCOL{position() + 1}">
                    <xsl:choose>
                      <xsl:when test="count(seat_number) > 0 and string-length(seat_number) > 0">
                        <xsl:value-of select="seat_number"/>
                      </xsl:when>
                      <xsl:otherwise>
                        &#160;
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </xsl:for-each>
              </tr>
            </xsl:if>
          </xsl:for-each>

        </table>
        <div class="BottominnerBox">
          <div class="whiteboxbottomleft"></div>
          <div class="whiteboxbottomcontent paxinnerwhitebox"></div>
          <div class="whiteboxbottomright"></div>
        </div>
      </xsl:if>
    </div>

    <div class="clear-all"></div>

    <ul class="divSSR" id="divSSR" style="display:block">

      <div class="space30"></div>

      <div id="dvSsrWrapper">
        <xsl:if test="count(Booking/ServiceFees/*) > 0">

          <div class="BoxtopRed">
            <xsl:value-of select="tikLanguage:get('Booking_Step_4_71','Special services')" />
          </div>
          <div class="TopinnerBox">
            <div class="whiteboxtopleft"></div>
            <div class="whiteboxtopcontent paxinnerwhitebox"></div>
            <div class="whiteboxtopright"></div>
          </div>
          <div class="paxinnerwhiteboxcontent">
            <table class="PaxServiceDetail" border="0" cellspacing="0" cellpadding="0" >
              <tr>
                <td class="HeadCOL1">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_4_72','Service name')" />
                </td>
                <xsl:for-each select="Booking/FlightSegment[count(. | key('route_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">
                  <td class="HeadCOL3">
                    <xsl:value-of select="od_origin_rcd" />-<xsl:value-of select="od_destination_rcd" />
                  </td>
                  <td class="HeadCOL2">
                    <xsl:value-of select="tikLanguage:get('Booking_Step_4_84','Service cost')" />
                    (<xsl:value-of select="$currency_rcd"/>)
                  </td>
                </xsl:for-each>
                <td class="HeadCOL5">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_4_92','Total cost')" />
                  (<xsl:value-of select="$currency_rcd"/>)
                </td>
              </tr>
              <xsl:for-each select="Booking/ServiceFees/*[count(. | key('fee_rcd_group', fee_rcd)[1]) = 1]">
                <xsl:variable name="row_position" select="position()" />
                <xsl:variable name="fee_rcd" select="fee_rcd" />
                <xsl:variable name="currency_rcd" select="currency_rcd" />

                <xsl:if test="string-length(service_on_request_flag) != 0">
                  <tr>
                    <input type="hidden" name="hdFeeId" value="{fee_id}" />
                    <input type="hidden" name="hdSsrCode" value="{$fee_rcd}" />
                    <input type="hidden" name="hdSsrOnRequestFlag" value="{service_on_request_flag}" />

                    <td id="tdDisplayName_{position()}" class="BodyCOL1">
                      <xsl:value-of select="display_name"/>
                    </td>
                    <xsl:for-each select="../../FlightSegment[count(. | key('route_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">

                      <xsl:variable name="origin_rcd" select="od_origin_rcd" />
                      <xsl:variable name="destination_rcd" select="od_destination_rcd" />
                      <xsl:variable name="booking_segment_id" select="booking_segment_id" />
                      <xsl:variable name="col_position" select="position()" />
                      <xsl:choose>
                        <xsl:when test="count(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]) > 0">
                          <td class="BodyCOL3">
                            <xsl:for-each select="../Mapping[count(. | key('passenger_id_group', passenger_id)[1]) = 1]">

                              <xsl:variable name="passenger_id" select="passenger_id" />
                              <xsl:variable name="SsrParam">
                                <xsl:for-each select="../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]">
                                  <xsl:variable name="param_origin_rcd" select="origin_rcd" />
                                  <xsl:variable name="param_destination_rcd" select="destination_rcd" />

                                  {&apos;booking_segment_id&apos; : &apos;<xsl:value-of select="../../FlightSegment[origin_rcd = $param_origin_rcd][destination_rcd = $param_destination_rcd]/booking_segment_id"/>&apos;,
                                  &apos;origin_rcd&apos; : &apos;<xsl:value-of select="$param_origin_rcd"/>&apos;,
                                  &apos;destination_rcd&apos; : &apos;<xsl:value-of select="$param_destination_rcd"/>&apos;,
                                  &apos;passenger_id&apos; : &apos;<xsl:value-of select="$passenger_id"/>&apos;}
                                  <xsl:if test="position() != last()">
                                    &#44;
                                  </xsl:if>
                                </xsl:for-each>
                              </xsl:variable>

                              <xsl:variable name="fee_amount" select="(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]/fee_amount_incl)" />
                              <xsl:if test="count($fee_amount) > 0">
                                <select id="seSsrAmount_{concat($row_position, '_', $col_position, '_', position())}"  name="seSsrAmount_{$row_position}" onchange="FillSpecialService();">
                                  <xsl:if test="position() > 1">
                                    <xsl:attribute name="style">display:none;</xsl:attribute>
                                  </xsl:if>
                                  <option value="{$SsrParam}">
                                    <xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 0">
                                      <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    0
                                  </option>
                                  <option value="{$SsrParam}">
                                    <xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 1">
                                      <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    1
                                  </option>
                                  <option value="{$SsrParam}">
                                    <xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 2">
                                      <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    2
                                  </option>

                                </select>
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                          <td class="BodyCOL2">

                            <span id="spnSsrFeeAmount_{$row_position}_{$col_position}">
                              <xsl:if test="count(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]/fee_amount_incl) &gt; 0">
                                <xsl:value-of select="format-number(sum(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]/fee_amount_incl),'#,##0.00')"/>
                                <!--&#160;<xsl:value-of select="//currency_rcd"/>-->
                              </xsl:if>
                            </span>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td class="BodyCOL3">
                            &#160;
                          </td>
                          <td class="BodyCOL2">
                            &#160;
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>

                    </xsl:for-each>
                    <td class="BodyCOL5">
                      <xsl:choose>
                        <xsl:when test="fee_amount_incl">
                          <span name="spTotal" id="spnSsrFeeTotal_{$row_position}">
                            <xsl:value-of select="format-number(0,'#,##0.00')"/>
                          </span>
                        </xsl:when>
                        <xsl:otherwise>
                          <span style="display:none" name="spTotal" id="spnSsrFeeTotal_{$row_position}">
                            <xsl:value-of select="format-number(0,'#,##0.00')"/>
                          </span>
                        </xsl:otherwise>
                      </xsl:choose>
                      <!--&#160;<xsl:value-of select="//currency_rcd"/>-->
                    </td>
                  </tr>
                </xsl:if>

              </xsl:for-each>
            </table>
          </div>
          <div class="clear-all"></div>
          <div class="BottominnerBox">
            <div class="whiteboxbottomleft"></div>
            <div class="whiteboxbottomcontent paxinnerwhitebox"></div>
            <div class="whiteboxbottomright"></div>
          </div>
        </xsl:if>
      </div>
      <li class="star">&#160;</li>

      <div class="clearboth"></div>

    </ul>

  </xsl:template>
</xsl:stylesheet>