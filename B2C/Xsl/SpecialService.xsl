<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>
  
  <xsl:key name="fee_rcd_group" match="Booking/ServiceFees/*" use="fee_rcd"/>
  <xsl:key name="passenger_id_group" match="Booking/Mapping" use="passenger_id"/>
  <xsl:key name="route_group" match="Booking/FlightSegment" use="concat(od_origin_rcd, '|', od_destination_rcd)"/>
  
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:if test="$Date != '0001-01-01T00:00:00'">
        <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>
  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>

	<xsl:template match="/">
		
		<xsl:if test="count(Booking/ServiceFees/*) > 0">
			<div class="xouter">
				<div class="xcontainer">
					<div class="xinner TBLSpecialService">
	
						<div class="boxheader">Special Services
							<div class="Close">
								<a href="javascript:CloseDialog();"><img src="App_Themes/Default/Images/close.png" alt="Close" title="Close" /></a>
							</div>
						</div>
			
						<table>
							<tr class="TableHeader">
								<td class="HeadCOL1">Service Name</td>
								<td class="Empty"></td>
								<xsl:for-each select="Booking/FlightSegment[count(. | key('route_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">
									<xsl:variable name="od_origin_rcd" select="od_origin_rcd"/>
									<xsl:variable name="od_destination_rcd" select="od_destination_rcd"/>
									<td class="HeadCOL2">
										<xsl:value-of select="../FlightSegment[origin_rcd = $od_origin_rcd][position() = 1]/origin_name" />&#160;
										to&#160;
										<xsl:value-of select="../FlightSegment[destination_rcd = $od_destination_rcd][position() = 1]/destination_name" />
										<!-- <br/>
										<span class="left">Quantity</span><span class="right">Service Cost</span> -->
									</td>
									<td class="Empty"></td>
								</xsl:for-each>
								<!-- <td class="HeadCOL3">Total Cost</td> -->
							</tr>
							
							<xsl:for-each select="Booking/ServiceFees/*[count(. | key('fee_rcd_group', fee_rcd)[1]) = 1]">
								<xsl:variable name="row_position" select="position()" />
								<xsl:variable name="fee_rcd" select="fee_rcd" />
								<xsl:variable name="currency_rcd" select="currency_rcd" />
								<tr>
									<input type="hidden" name="hdSsrFeeId" value="{fee_id}" />
									<input type="hidden" name="hdSsrCode" value="{$fee_rcd}" />
									<input type="hidden" name="hdSsrOnRequestFlag" value="{service_on_request_flag}" />
								
									<td class="BodyCOL1">
										<span id="spnSsrDisplayName_{position()}">
											<xsl:value-of select="display_name"/>
										</span>
										<img src="App_Themes/Default/Images/info.jpg" alt="" />
									</td>
									<td class="Empty"></td>
									<xsl:for-each select="../../FlightSegment[count(. | key('route_group', concat(od_origin_rcd, '|', od_destination_rcd))[1]) = 1]">
									
										<xsl:variable name="origin_rcd" select="od_origin_rcd" />
										<xsl:variable name="destination_rcd" select="od_destination_rcd" />
										<xsl:variable name="booking_segment_id" select="booking_segment_id" />
										<xsl:variable name="col_position" select="position()" />
										<xsl:choose>
											<xsl:when test="count(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]) > 0">
												<td class="BodyCOL2">
													<span class="left">
														<xsl:for-each select="../Mapping[count(. | key('passenger_id_group', passenger_id)[1]) = 1]">
														
															<xsl:variable name="passenger_id" select="passenger_id" />
															<xsl:variable name="SsrParam">
																<xsl:for-each select="../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]">
																<xsl:variable name="param_origin_rcd" select="origin_rcd" />
																<xsl:variable name="param_destination_rcd" select="destination_rcd" />
																
																{&apos;booking_segment_id&apos; : &apos;<xsl:value-of select="../../FlightSegment[origin_rcd = $param_origin_rcd][destination_rcd = $param_destination_rcd]/booking_segment_id"/>&apos;,
																&apos;origin_rcd&apos; : &apos;<xsl:value-of select="$param_origin_rcd"/>&apos;,
																&apos;destination_rcd&apos; : &apos;<xsl:value-of select="$param_destination_rcd"/>&apos;,
																&apos;passenger_id&apos; : &apos;<xsl:value-of select="$passenger_id"/>&apos;}
																<xsl:if test="position() != last()">
																&#44;
																</xsl:if>
																</xsl:for-each>
															</xsl:variable>
														
															<select id="seSsrAmount_{concat($row_position, '_', $col_position, '_', position())}"  name="seSsrAmount_{$row_position}" onchange="CalculateSpecialService();">
																<xsl:if test="position() > 1">
																	<xsl:attribute name="style">display:none;</xsl:attribute>
																</xsl:if>
																<option value="{$SsrParam}">
																	<xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 0">
																		<xsl:attribute name="selected">selected</xsl:attribute>
																	</xsl:if>
																	0
																</option>
																<option value="{$SsrParam}">
																	<xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 1">
																		<xsl:attribute name="selected">selected</xsl:attribute>
																	</xsl:if>
																	1
																</option>
																<option value="{$SsrParam}">
																	<xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 2">
																	<xsl:attribute name="selected">selected</xsl:attribute>
																	</xsl:if>
																	2
																</option>
																<option value="{$SsrParam}">
																	<xsl:if test="../Service[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][special_service_rcd = $fee_rcd][passenger_id = $passenger_id]/number_of_units = 3">
																	<xsl:attribute name="selected">selected</xsl:attribute>
																	</xsl:if>
																	3
																</option>
															</select>
														</xsl:for-each>
													</span>
													<span class="right">
														<span id="spnSsrFeeAmount_{$row_position}_{$col_position}">
															<xsl:value-of select="format-number(sum(../ServiceFees/*[origin_rcd = $origin_rcd or destination_rcd = $destination_rcd][fee_rcd = $fee_rcd]/fee_amount_incl),'#,##0.00')"/>&#160;
														</span>
														<xsl:value-of select="$currency_rcd"/>
													</span>
												</td>
												
											</xsl:when>
											<xsl:otherwise>
												<td class="BodyCOL2">
													N/A
												</td>
											</xsl:otherwise>
											
										</xsl:choose>
									<td class="Empty"></td>
									</xsl:for-each>
									<!-- <td class="BodyCOL3">
										<span id="spnSsrFeeTotal_{$row_position}">
											0.00
										</span>
										<xsl:value-of select="$currency_rcd"/> 
									</td>-->
								</tr>
							</xsl:for-each>
							<tr>
								<td class="SpecialServiceLine"></td>
								<td></td>
								<td class="SpecialServiceLine"></td>
								<td></td>
								<td class="SpecialServiceLine"></td>
								<!-- <td></td>
								<td></td> -->
							</tr>
						</table>
			
						<div class="BTN-Search">
						<div class="ButtonTable" onclick="FillSpecialService();">
							<div class="BTN-Left"></div>
							<div class="BTN-Middle">
								<xsl:value-of select="tikLanguage:get('Booking_Step_4_53_','OK')" />
							</div>
							<div class="BTN-Right"></div>
						</div>
						</div>
						<div class="clear-all"></div>

					</div>
				</div>
			</div>
		</xsl:if>
		
	</xsl:template>
</xsl:stylesheet>