<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">

	<xsl:template match="/">
		<!-- Start Login by hnin -->
		<div class="TBLloginBox">
			<div class="BoxHeader">
				<div class="TicketText">LOGIN</div>
			</div>
			<ul>
				<li class="Text">
					Client ID/ Email<br />
					<input id="txtDialogClientID" type="text" onkeypress="return SubmitEnterUser(this,event,'true')" />
				</li>
				<li class="Text">
					Password<br />
					<input id="txtDialogPassword" type="password" onkeypress="return SubmitEnterUser(this,event,'true')" />
				</li>
			</ul>
			<div class="ButtonAlignMiddleUserInfo">
				<div class="buttonCornerLeft"></div>
				<div class="buttonContent" onclick="ClientLogonDialog();">LOGIN</div>
				<div class="buttonCornerRight"></div>
			</div>
			<div class="clearboth"></div>
			<div class="line"></div>
			<div class="clearboth"></div>
			<div class="forgetID" onclick="LoadForgetPassword();">Forget Password</div>
			<div class="clearboth"></div>
			<div class="forgetID" onclick="CloseDialog();">Close</div>
			<div class="clearboth"></div>
			<div class="BlueBoxFooter"></div>
		</div>
		<div class="clearboth"></div>
		<!-- End Login by hnin -->
	</xsl:template>

</xsl:stylesheet>