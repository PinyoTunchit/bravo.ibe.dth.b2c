<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							                xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,9,2)"/>/<xsl:value-of select="substring($Date,6,2)"/>/<xsl:value-of select="substring($Date,1,4)"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="GetDay">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="number(substring($Date,9,2))"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>
  <xsl:template name="month_name">
    <xsl:param name="Date" />
    <xsl:variable name="month" select="number(substring($Date,5,2))"/>
    <xsl:variable name="year" select="number(substring($Date,0,5))"/>
    <div class="LowYear">
      <xsl:value-of select="$year"/>
    </div>
    <div class="LowMonth">
      <xsl:choose>
        <xsl:when test="$month=1">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(0)" />
        </xsl:when>
        <xsl:when test="$month=2">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(1)" />
        </xsl:when>
        <xsl:when test="$month=3">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(2)" />
        </xsl:when>
        <xsl:when test="$month=4">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(3)" />
        </xsl:when>
        <xsl:when test="$month=5">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(4)" />
        </xsl:when>
        <xsl:when test="$month=6">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(5)" />
        </xsl:when>
        <xsl:when test="$month=7">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(6)" />
        </xsl:when>
        <xsl:when test="$month=8">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(7)" />
        </xsl:when>
        <xsl:when test="$month=9">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(8)" />
        </xsl:when>
        <xsl:when test="$month=10">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(9)" />
        </xsl:when>
        <xsl:when test="$month=11">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(10)" />
        </xsl:when>
        <xsl:when test="$month=12">
          <xsl:value-of select="tikLanguage:GetAbbrivateMonth(11)" />
        </xsl:when>
        <xsl:otherwise>INVALID MONTH</xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>

  <!-- Get Low Fare Finder Time-->
  <xsl:template match="flight">
    <!--Display fare group information-->
    <xsl:variable name="flight_id" select="flight_id" />
    <xsl:variable name="flight_number" select="flight_number" />
    <xsl:variable name="transit_flight_id" select="transit_flight_id" />
    <xsl:variable name="transit_departure_date" select="concat(substring(transit_departure_date,1,4), substring(transit_departure_date,6,2), substring(transit_departure_date,9,2))" />
    <xsl:variable name="transit_airport_rcd" select="transit_airport_rcd" />
    <xsl:variable name="transit_fare_id" select="transit_fare_id" />
    <xsl:variable name="departure_date" select="concat(substring(departure_date,1,4), substring(departure_date,6,2), substring(departure_date,9,2))" />
    <xsl:variable name="planned_departure_time" select="planned_departure_time" />
    <xsl:variable name="planned_arrival_time" select="planned_arrival_time" />
    <xsl:variable name="arrival_date" select="concat(substring(arrival_date,1,4), substring(arrival_date,6,2), substring(arrival_date,9,2))" />
    <xsl:variable name="airline_rcd" select="airline_rcd" />
    <xsl:variable name="transit_airline_rcd" select="transit_airline_rcd" />
    <xsl:variable name="transit_flight_number" select="transit_flight_number" />
    <xsl:variable name="transit_planned_departure_time" select="transit_planned_departure_time" />
    <xsl:variable name="transit_planned_arrival_time" select="transit_planned_arrival_time" />
    <xsl:variable name="transit_arrival_date" select="concat(substring(transit_arrival_date,1,4), substring(transit_arrival_date,6,2), substring(transit_arrival_date,9,2))" />
    <xsl:variable name="origin_rcd" select="origin_rcd" />
    <xsl:variable name="destination_rcd" select="destination_rcd" />
    <xsl:variable name="transit_points" select="transit_points" />
    <xsl:variable name="transit_points_name" select="transit_points_name" />
    <li>
      <span>
        <xsl:call-template name="TimeFormat">
          <xsl:with-param name="Time" select="planned_departure_time"></xsl:with-param>
        </xsl:call-template>
        -
        <xsl:call-template name="TimeFormat">
          <xsl:with-param name="Time" select="$planned_arrival_time"></xsl:with-param>
        </xsl:call-template>
      </span>
      <xsl:choose>
        <xsl:when test="full_flight_flag = 1">
          <b>
            -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_21','Full')" />
          </b>
        </xsl:when>
        <xsl:when test="class_open_flag = 0 and waitlist_open_flag = 0">
          <b>
            -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_21','Full')" />
          </b>
        </xsl:when>
        <xsl:when test="class_open_flag = 0">
          <b>
            -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_22','Closed')" />
          </b>
        </xsl:when>
        <xsl:when test="close_web_sales = 1">
          <b>
            -&#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_23','Call')" />
          </b>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="flight_param_fri">
            {&#034;flight_id&#034;:&#034;<xsl:value-of select="$flight_id" />&#034;,
            &#034;flight_number&#034;:&#034;<xsl:value-of select="$flight_number" />&#034;,
            &#034;origin_rcd&#034;:&#034;<xsl:value-of select="origin_rcd" />&#034;,
            &#034;airline_rcd&#034;:&#034;<xsl:value-of select="airline_rcd" />&#034;,
            &#034;destination_rcd&#034;:&#034;<xsl:value-of select="destination_rcd" />&#034;,
            &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="booking_class_rcd" />&#034;,
            &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="$transit_flight_id" />&#034;,
            &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
            &#034;arrival_date&#034;:&#034;<xsl:value-of select="$arrival_date" />&#034;,           &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="$planned_departure_time" />&#034;,
            &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="$planned_arrival_time" />&#034;,
            &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
            &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="$transit_airport_rcd" />&#034;,
            &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="$transit_fare_id" />&#034;,
            &#034;fare_id&#034;:&#034;<xsl:value-of select="fare_id" />&#034;,
            &#034;transit_airline_rcd&#034;:&#034;<xsl:value-of select="$transit_airline_rcd" />&#034;,
            &#034;transit_flight_number&#034;:&#034;<xsl:value-of select="$transit_flight_number" />&#034;,
            &#034;transit_boarding_class_rcd&#034;:&#034;<xsl:value-of select="transit_boarding_class_rcd" />&#034;,
            &#034;transit_booking_class_rcd&#034;:&#034;<xsl:value-of select="transit_booking_class_rcd" />&#034;,
            &#034;transit_planned_departure_time&#034;:&#034;<xsl:value-of select="$transit_planned_departure_time" />&#034;,
            &#034;transit_planned_arrival_time&#034;:&#034;<xsl:value-of select="$transit_planned_arrival_time" />&#034;,
            &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
            &#034;transit_arrival_date&#034;:&#034;<xsl:value-of select="$transit_arrival_date" />&#034;,
            &#034;transit_points&#034;:&#034;<xsl:value-of select="$transit_points" />&#034; ,
            &#034;transit_points_name&#034;:&#034;<xsl:value-of select="$transit_points_name" />&#034; }
          </xsl:variable>

          <input type="hidden" id="spnTime_{$flight_id}" value="{$flight_param_fri}"/>
          <b onclick="SelectTime('spnTime_{$flight_id}');">
            -&#160;<xsl:value-of select="format-number(total_adult_fare,'#,##0.00')"/>
          </b>
        </xsl:otherwise>
      </xsl:choose>
    </li>
  </xsl:template>

  <xsl:template match="/">
    <xsl:variable name="origin_name" select="flights/setting/OriginName" />
    <xsl:variable name="destination_name" select="flights/setting/DestinationName" />
    <xsl:variable name="flight_type" select="flights/setting/flight_type" />
    <xsl:variable name="LowFareFinderRange" select="flights/setting/LowFareFinderRange" />
    <xsl:variable name="flight_departure_date" select="flights/week[position()=1]/flight[position() = 1]/departure_date" />
    <xsl:variable name="deptDate" select="concat(substring($flight_departure_date,1,4), 
                                                  substring($flight_departure_date,6,2), 
                                                  substring($flight_departure_date,9,2))" />
    <xsl:variable name="lowest_fare_amount" select="flights/*/flight/total_adult_fare[not(. &gt; ../../../../flights/*/flight/total_adult_fare)][1]" />
    <xsl:if test="count(flights/week) > 0">
      <div class="FareCalender">
        <div class="CalenderBase">

          <div class="WrapperTBLYourFlight">
            <div class="DisplayFlightDetail">
              <span class="MonthViewTitle">
                <xsl:if test="$flight_type ='Outward'">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_2_2','Your Flights Options')" />
                </xsl:if>
                <xsl:if test="$flight_type ='Return'">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_2_23','Your Flights Options')" />
                </xsl:if>
              </span>&#160;
              <xsl:variable name="route_rcd" select="flights/week[flight[total_adult_fare != 'N/A']][position() = 1]/flight[total_adult_fare != 'N/A'][position() = 1]" />
              <span class="TextFlightName">
                <xsl:value-of select="flights/setting/OriginName" /> (<span id="spOriLowfare_{$flight_type}" />)
              </span>
              
              &#160;<xsl:value-of select="tikLanguage:get('Booking_Step_2_10','To')" />&#160;
              <span class="TextFlightName">
                <xsl:value-of select="flights/setting/DestinationName" /> (<span  id="spDestLowfare_{$flight_type}" />)
              </span>
            </div>
          </div>
		  <div class="clear-all"></div>
		  <div class="LowfareMonthCalendar">
      <xsl:variable name="month_date_previous" select="JCode:getDateAddMonth($deptDate, -1)" />
        <xsl:if test="$month_date_previous != '000000'">
          <div class="LowfareMonth" onclick="SearchLowFareSingleMonth('{$month_date_previous}', '{$flight_type}');">
            <xsl:call-template name="month_name">
              <xsl:with-param name="Date" select="$month_date_previous"></xsl:with-param>
            </xsl:call-template>
          </div>    
        </xsl:if>
			<div class="LowfareMonthSelect">
        <xsl:call-template name="month_name">
          <xsl:with-param name="Date" select="JCode:getDateAddMonth($deptDate, 0)"></xsl:with-param>
        </xsl:call-template>
			</div>
        
      <xsl:variable name="month_date_next_1" select="JCode:getDateAddMonth($deptDate, 1)" />
			<div class="LowfareMonth" onclick="SearchLowFareSingleMonth('{$month_date_next_1}', '{$flight_type}');">
        <xsl:call-template name="month_name">
          <xsl:with-param name="Date" select="$month_date_next_1"></xsl:with-param>
        </xsl:call-template>
			</div>
      
        <xsl:variable name="month_date_next_2" select="JCode:getDateAddMonth($deptDate, 2)" />
			<div class="LowfareMonth" onclick="SearchLowFareSingleMonth('{$month_date_next_2}', '{$flight_type}');">
        <xsl:call-template name="month_name">
          <xsl:with-param name="Date" select="$month_date_next_2"></xsl:with-param>
        </xsl:call-template>
			</div>
		  
		  </div>
		  
		  <div class="clear-all"></div>
		  
          <div class="CalenderControl"></div>

          <div class="LowFareBox">
            <table class="FareCalenderTable" cellpadding="0" cellspacing="0">
              <tr class="Lowfaredatepanel">
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:GetAbbrivateDay(0)" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:GetAbbrivateDay(1)" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:GetAbbrivateDay(2)" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:GetAbbrivateDay(3)" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:GetAbbrivateDay(4)" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:GetAbbrivateDay(5)" />
                </td>
                <td class="Lowfaredate">
                  <xsl:value-of select="tikLanguage:GetAbbrivateDay(6)" />
                </td>
              </tr>
              <xsl:for-each select="flights/week">
                <tr>
                  <td id="td_{../setting/flight_type}_SUN{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Sunday']) > 0">
                        <xsl:variable name="total_fare_amount" select="flight[day_of_week = 'Sunday'][position() = 1]/total_adult_fare"/>
                        <xsl:choose>
                          <xsl:when test="$total_fare_amount != 'N/A'">
                            <xsl:variable name="transit_departure_date" select="concat(substring(flight[day_of_week = 'Sunday'][position() = 1]/transit_departure_date,1,4), 
                                                                            substring(flight[day_of_week = 'Sunday'][position() = 1]/transit_departure_date,6,2), 
                                                                            substring(flight[day_of_week = 'Sunday'][position() = 1]/transit_departure_date,9,2))" />

                            <xsl:variable name="departure_date" select="concat(substring(flight[day_of_week = 'Sunday'][position() = 1]/departure_date,1,4), 
                                                                    substring(flight[day_of_week = 'Sunday'][position() = 1]/departure_date,6,2), 
                                                                    substring(flight[day_of_week = 'Sunday'][position() = 1]/departure_date,9,2))" />
                            
                            <xsl:variable name="arrival_date" select="concat(substring(flight[day_of_week = 'Sunday'][position() = 1]/arrival_date,1,4), 
                                                                    substring(flight[day_of_week = 'Sunday'][position() = 1]/arrival_date,6,2), 
                                                                    substring(flight[day_of_week = 'Sunday'][position() = 1]/arrival_date,9,2))" />


                            <xsl:variable name="transit_planned_departure_time" select="flight[day_of_week = 'Sunday'][position() = 1]/transit_planned_departure_time" />
                            <xsl:variable name="transit_planned_arrival_time" select="flight[day_of_week = 'Sunday'][position() = 1]/transit_planned_arrival_time" />
                            <xsl:variable name="transit_arrival_date" select="concat(substring(flight[day_of_week = 'Sunday'][position() = 1]/transit_arrival_date,1,4), 
                                                                              substring(flight[day_of_week = 'Sunday'][position() = 1]/transit_arrival_date,6,2), 
                                                                              substring(flight[day_of_week = 'Sunday'][position() = 1]/transit_arrival_date,9,2))" />
                            <xsl:variable name="transit_airline_rcd"  select="flight[day_of_week = 'Sunday'][position() = 1]/transit_airline_rcd" />
                            <xsl:variable name="transit_flight_number"  select="flight[day_of_week = 'Sunday'][position() = 1]/transit_flight_number" />
                            <xsl:variable name="transit_points"  select="flight[day_of_week = 'Sunday'][position() = 1]/transit_points" />
                            <xsl:variable name="transit_points_name"  select="flight[day_of_week = 'Sunday'][position() = 1]/transit_points_name" />
                            <xsl:variable name="flight_param">
                              {&#034;flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/flight_id" />&#034;,
                              &#034;flight_number&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/flight_number" />&#034;,
                              &#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/origin_rcd" />&#034;,
                              &#034;airline_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/airline_rcd" />&#034;,
                              &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/destination_rcd" />&#034;,
                              &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/booking_class_rcd" />&#034;,
                              &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/transit_flight_id" />&#034;,
                              &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
                              &#034;arrival_date&#034;:&#034;<xsl:value-of select="$arrival_date" />&#034;,
                              &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/planned_departure_time" />&#034;,
                              &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/transit_airport_rcd" />&#034;,
                              &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/transit_fare_id" />&#034;,
                              &#034;fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/fare_id" />&#034;,
                              &#034;transit_airline_rcd&#034;:&#034;<xsl:value-of select="$transit_airline_rcd" />&#034;,
                              &#034;transit_flight_number&#034;:&#034;<xsl:value-of select="$transit_flight_number" />&#034;,
                              &#034;transit_boarding_class_rcd&#034;:&#034;<xsl:value-of select="transit_boarding_class_rcd" />&#034;,
                              &#034;transit_booking_class_rcd&#034;:&#034;<xsl:value-of select="transit_booking_class_rcd" />&#034;,
                              &#034;transit_planned_departure_time&#034;:&#034;<xsl:value-of select="$transit_planned_departure_time" />&#034;,
                              &#034;transit_planned_arrival_time&#034;:&#034;<xsl:value-of select="$transit_planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_arrival_date&#034;:&#034;<xsl:value-of select="$transit_arrival_date" />&#034;,
                              &#034;transit_points&#034;:&#034;<xsl:value-of select="$transit_points" />&#034; ,
                              &#034;transit_points_name&#034;:&#034;<xsl:value-of select="$transit_points_name" />&#034;}
                            </xsl:variable>                            
                            <input type="hidden" id="spnTime_{flight[day_of_week = 'Sunday'][position() = 1]/flight_id}" value="{$flight_param}" name="hdTime_{../setting/flight_type}"/>
                            <a class="Low" onclick="SelectLlf('{../setting/flight_type}_SUN{position()}');SelectTime('spnTime_{flight[day_of_week = 'Sunday'][position() = 1]/flight_id}');">
                              <xsl:variable name="flight_param_sun">
                                {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/origin_rcd" />&#034;,
                                &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/origin_name" />&#034;,
                                &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/destination_rcd" />&#034;,
                                &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/departure_date" />&#034;,
                                &#034;total_adult_fare&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/total_adult_fare" />&#034;}
                              </xsl:variable>

                              <div class="ShowLowfaredate">
                                <input id="optLowfare_{../setting/flight_type}_SUN{position()}"  name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_sun)}" style="display:none;" />

                                <label for="optLowfare_{../setting/flight_type}_SUN{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Sunday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>

                              <div id="spnLowfare_{../setting/flight_type}_SUN{position()}">

                                <xsl:choose>
                                  <xsl:when test="$lowest_fare_amount = $total_fare_amount">
                                    <xsl:attribute name="class">LowestFare</xsl:attribute>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:attribute name="class">Lowfareprice</xsl:attribute>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <div class="sup">
                                  <xsl:value-of select="flight[day_of_week = 'Sunday'][position() = 1]/currency_rcd"/>
                                </div>
                                <xsl:value-of select="format-number($total_fare_amount,'#,##0.00')"/>
                              </div>
                            </a>
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="Low" onclick="return false;">
                              <div class="ShowLowfaredate">
                                <label for="optLowfare_{../setting/flight_type}_SUN{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Sunday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>
                              <div id="spnLowfare_{../setting/flight_type}_SUN{position()}" class="Lowfareprice">
                                N/A
                              </div>
                            </a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_MON{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Monday']) > 0">
                        <xsl:variable name="total_fare_amount" select="flight[day_of_week = 'Monday'][position() = 1]/total_adult_fare"/>
                        <xsl:choose>
                          <xsl:when test="$total_fare_amount != 'N/A'">
                            <xsl:variable name="transit_departure_date" select="concat(substring(flight[day_of_week = 'Monday'][position() = 1]/transit_departure_date,1,4), 
                                                                            substring(flight[day_of_week = 'Monday'][position() = 1]/transit_departure_date,6,2), 
                                                                            substring(flight[day_of_week = 'Monday'][position() = 1]/transit_departure_date,9,2))" />

                            <xsl:variable name="departure_date" select="concat(substring(flight[day_of_week = 'Monday'][position() = 1]/departure_date,1,4), 
                                                                    substring(flight[day_of_week = 'Monday'][position() = 1]/departure_date,6,2), 
                                                                    substring(flight[day_of_week = 'Monday'][position() = 1]/departure_date,9,2))" />

                            <xsl:variable name="arrival_date" select="concat(substring(flight[day_of_week = 'Monday'][position() = 1]/arrival_date,1,4), 
                                                                    substring(flight[day_of_week = 'Monday'][position() = 1]/arrival_date,6,2), 
                                                                    substring(flight[day_of_week = 'Monday'][position() = 1]/arrival_date,9,2))" />


                            <xsl:variable name="transit_planned_departure_time" select="flight[day_of_week = 'Monday'][position() = 1]/transit_planned_departure_time" />
                            <xsl:variable name="transit_planned_arrival_time" select="flight[day_of_week = 'Monday'][position() = 1]/transit_planned_arrival_time" />
                            <xsl:variable name="transit_arrival_date" select="concat(substring(flight[day_of_week = 'Monday'][position() = 1]/transit_arrival_date,1,4), 
                                                                              substring(flight[day_of_week = 'Monday'][position() = 1]/transit_arrival_date,6,2), 
                                                                              substring(flight[day_of_week = 'Monday'][position() = 1]/transit_arrival_date,9,2))" />
                            <xsl:variable name="transit_airline_rcd"  select="flight[day_of_week = 'Monday'][position() = 1]/transit_airline_rcd" />
                            <xsl:variable name="transit_flight_number"  select="flight[day_of_week = 'Monday'][position() = 1]/transit_flight_number" />
                            <xsl:variable name="transit_points"  select="flight[day_of_week = 'Monday'][position() = 1]/transit_points" />
                            <xsl:variable name="transit_points_name"  select="flight[day_of_week = 'Monday'][position() = 1]/transit_points_name" />

                            <xsl:variable name="flight_param">
                              {&#034;flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/flight_id" />&#034;,
                              &#034;flight_number&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/flight_number" />&#034;,
                              &#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/origin_rcd" />&#034;,
                              &#034;airline_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/airline_rcd" />&#034;,
                              &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/destination_rcd" />&#034;,
                              &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/booking_class_rcd" />&#034;,
                              &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/transit_flight_id" />&#034;,
                              &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
                              &#034;arrival_date&#034;:&#034;<xsl:value-of select="$arrival_date" />&#034;,                             &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/planned_departure_time" />&#034;,
                              &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/transit_airport_rcd" />&#034;,
                              &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/transit_fare_id" />&#034;,
                              &#034;fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/fare_id" />&#034;,
                              &#034;transit_airline_rcd&#034;:&#034;<xsl:value-of select="$transit_airline_rcd" />&#034;,
                              &#034;transit_flight_number&#034;:&#034;<xsl:value-of select="$transit_flight_number" />&#034;,
                              &#034;transit_boarding_class_rcd&#034;:&#034;<xsl:value-of select="transit_boarding_class_rcd" />&#034;,
                              &#034;transit_booking_class_rcd&#034;:&#034;<xsl:value-of select="transit_booking_class_rcd" />&#034;,
                              &#034;transit_planned_departure_time&#034;:&#034;<xsl:value-of select="$transit_planned_departure_time" />&#034;,
                              &#034;transit_planned_arrival_time&#034;:&#034;<xsl:value-of select="$transit_planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_arrival_date&#034;:&#034;<xsl:value-of select="$transit_arrival_date" />&#034;,
                              &#034;transit_points&#034;:&#034;<xsl:value-of select="$transit_points" />&#034; ,
                              &#034;transit_points_name&#034;:&#034;<xsl:value-of select="$transit_points_name" />&#034;}
                            </xsl:variable>                           
                            <input type="hidden" id="spnTime_{flight[day_of_week = 'Monday'][position() = 1]/flight_id}" value="{$flight_param}"  name="hdTime_{../setting/flight_type}"/>
                            <a class="Low" onclick="SelectLlf('{../setting/flight_type}_MON{position()}');SelectTime('spnTime_{flight[day_of_week = 'Monday'][position() = 1]/flight_id}');">
                              <xsl:variable name="flight_param_mon">
                                {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/origin_rcd" />&#034;,
                                &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/origin_name" />&#034;,
                                &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/destination_rcd" />&#034;,
                                &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/departure_date" />&#034;,
                                &#034;total_adult_fare&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/total_adult_fare" />&#034;}
                              </xsl:variable>

                              <div class="ShowLowfaredate">
                                <input id="optLowfare_{../setting/flight_type}_MON{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_mon)}" style="display:none;" />

                                <label for="optLowfare_{../setting/flight_type}_MON{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Monday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>

                              <div id="spnLowfare_{../setting/flight_type}_MON{position()}">
                                <xsl:choose>
                                  <xsl:when test="$lowest_fare_amount = $total_fare_amount">
                                    <xsl:attribute name="class">LowestFare</xsl:attribute>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:attribute name="class">Lowfareprice</xsl:attribute>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <div class="sup">
                                  <xsl:value-of select="flight[day_of_week = 'Monday'][position() = 1]/currency_rcd"/>
                                </div>
                                <xsl:value-of select="format-number($total_fare_amount, '#,##0.00')"/>
                              </div>
                            </a>    
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="Low" onclick="return false;">
                              <div class="ShowLowfaredate">
                                <label for="optLowfare_{../setting/flight_type}_MON{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Monday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>
                              <div id="spnLowfare_{../setting/flight_type}_MON{position()}" class="Lowfareprice">
                                N/A
                              </div>
                            </a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_TUE{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Tuesday']) > 0">
                        <xsl:variable name="total_fare_amount" select="flight[day_of_week = 'Tuesday'][position() = 1]/total_adult_fare"/>
                        <xsl:choose>
                          <xsl:when test="$total_fare_amount != 'N/A'">
                            <xsl:variable name="transit_departure_date" select="concat(substring(flight[day_of_week = 'Tuesday'][position() = 1]/transit_departure_date,1,4), 
                                                                            substring(flight[day_of_week = 'Tuesday'][position() = 1]/transit_departure_date,6,2), 
                                                                            substring(flight[day_of_week = 'Tuesday'][position() = 1]/transit_departure_date,9,2))" />

                            <xsl:variable name="departure_date" select="concat(substring(flight[day_of_week = 'Tuesday'][position() = 1]/departure_date,1,4), 
                                                                    substring(flight[day_of_week = 'Tuesday'][position() = 1]/departure_date,6,2), 
                                                                    substring(flight[day_of_week = 'Tuesday'][position() = 1]/departure_date,9,2))" />
                            <xsl:variable name="arrival_date" select="concat(substring(flight[day_of_week = 'Tuesday'][position() = 1]/arrival_date,1,4), 
                                                                    substring(flight[day_of_week = 'Tuesday'][position() = 1]/arrival_date,6,2), 
                                                                    substring(flight[day_of_week = 'Tuesday'][position() = 1]/arrival_date,9,2))" />


                            <xsl:variable name="transit_planned_departure_time" select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_planned_departure_time" />
                            <xsl:variable name="transit_planned_arrival_time" select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_planned_arrival_time" />
                            <xsl:variable name="transit_arrival_date" select="concat(substring(flight[day_of_week = 'Tuesday'][position() = 1]/transit_arrival_date,1,4), 
                                                                              substring(flight[day_of_week = 'Tuesday'][position() = 1]/transit_arrival_date,6,2), 
                                                                              substring(flight[day_of_week = 'Tuesday'][position() = 1]/transit_arrival_date,9,2))" />
                            <xsl:variable name="transit_airline_rcd"  select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_airline_rcd" />
                            <xsl:variable name="transit_flight_number"  select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_flight_number" />
                            <xsl:variable name="transit_points"  select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_points" />
                            <xsl:variable name="transit_points_name"  select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_points_name" />

                            <xsl:variable name="flight_param">
                              {&#034;flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/flight_id" />&#034;,
                              &#034;flight_number&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/flight_number" />&#034;,
                              &#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/origin_rcd" />&#034;,
                              &#034;airline_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/airline_rcd" />&#034;,
                              &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/destination_rcd" />&#034;,
                              &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/booking_class_rcd" />&#034;,
                              &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_flight_id" />&#034;,
                              &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
                              &#034;arrival_date&#034;:&#034;<xsl:value-of select="$arrival_date" />&#034;,                              &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/planned_departure_time" />&#034;,
                              &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_airport_rcd" />&#034;,
                              &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/transit_fare_id" />&#034;,
                              &#034;fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/fare_id" />&#034;,
                              &#034;transit_airline_rcd&#034;:&#034;<xsl:value-of select="$transit_airline_rcd" />&#034;,
                              &#034;transit_flight_number&#034;:&#034;<xsl:value-of select="$transit_flight_number" />&#034;,
                              &#034;transit_boarding_class_rcd&#034;:&#034;<xsl:value-of select="transit_boarding_class_rcd" />&#034;,
                              &#034;transit_booking_class_rcd&#034;:&#034;<xsl:value-of select="transit_booking_class_rcd" />&#034;,
                              &#034;transit_planned_departure_time&#034;:&#034;<xsl:value-of select="$transit_planned_departure_time" />&#034;,
                              &#034;transit_planned_arrival_time&#034;:&#034;<xsl:value-of select="$transit_planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_arrival_date&#034;:&#034;<xsl:value-of select="$transit_arrival_date" />&#034;,
                              &#034;transit_points&#034;:&#034;<xsl:value-of select="$transit_points" />&#034; ,
                              &#034;transit_points_name&#034;:&#034;<xsl:value-of select="$transit_points_name" />&#034;}
                            </xsl:variable>                            
                            <input type="hidden" id="spnTime_{flight[day_of_week = 'Tuesday'][position() = 1]/flight_id}" value="{$flight_param}" name="hdTime_{../setting/flight_type}"/>

                            <a class="Low" onclick="SelectLlf('{../setting/flight_type}_TUE{position()}');SelectTime('spnTime_{flight[day_of_week = 'Tuesday'][position() = 1]/flight_id}');">
                              <xsl:variable name="flight_param_tue">
                                {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/origin_rcd" />&#034;,
                                &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/origin_name" />&#034;,
                                &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/destination_rcd" />&#034;,
                                &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/departure_date" />&#034;,
                                &#034;total_adult_fare&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/total_adult_fare" />&#034;}
                              </xsl:variable>

                              <div class="ShowLowfaredate">
                                <input id="optLowfare_{../setting/flight_type}_TUE{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_tue)}" style="display:none;"/>

                                <label for="optLowfare_{../setting/flight_type}_TUE{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Tuesday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>

                              <div id="spnLowfare_{../setting/flight_type}_TUE{position()}">
                                <xsl:choose>
                                  <xsl:when test="$lowest_fare_amount = $total_fare_amount">
                                    <xsl:attribute name="class">LowestFare</xsl:attribute>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:attribute name="class">Lowfareprice</xsl:attribute>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <div class="sup">
                                  <xsl:value-of select="flight[day_of_week = 'Tuesday'][position() = 1]/currency_rcd"/>
                                </div>
                                <xsl:value-of select="format-number($total_fare_amount, '#,##0.00')"/>
                              </div>
                            </a>
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="Low" onclick="return false;">
                              <div class="ShowLowfaredate">
                                <label for="optLowfare_{../setting/flight_type}_TUE{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Tuesday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>
                              <div id="spnLowfare_{../setting/flight_type}_TUE{position()}" class="Lowfareprice">
                                N/A
                              </div>
                            </a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_WED{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Wednesday']) > 0">
                        <xsl:variable name="total_fare_amount" select="flight[day_of_week = 'Wednesday'][position() = 1]/total_adult_fare"/>
                        <xsl:choose>
                          <xsl:when test="$total_fare_amount != 'N/A'">
                            <xsl:variable name="transit_departure_date" select="concat(substring(flight[day_of_week = 'Wednesday'][position() = 1]/transit_departure_date,1,4), 
                                                                            substring(flight[day_of_week = 'Wednesday'][position() = 1]/transit_departure_date,6,2), 
                                                                            substring(flight[day_of_week = 'Wednesday'][position() = 1]/transit_departure_date,9,2))" />

                            <xsl:variable name="departure_date" select="concat(substring(flight[day_of_week = 'Wednesday'][position() = 1]/departure_date,1,4), 
                                                                    substring(flight[day_of_week = 'Wednesday'][position() = 1]/departure_date,6,2), 
                                                                    substring(flight[day_of_week = 'Wednesday'][position() = 1]/departure_date,9,2))" />
                            <xsl:variable name="arrival_date" select="concat(substring(flight[day_of_week = 'Wednesday'][position() = 1]/arrival_date,1,4), 
                                                                    substring(flight[day_of_week = 'Wednesday'][position() = 1]/arrival_date,6,2), 
                                                                    substring(flight[day_of_week = 'Wednesday'][position() = 1]/arrival_date,9,2))" />
                            <xsl:variable name="transit_planned_departure_time" select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_planned_departure_time" />
                            <xsl:variable name="transit_planned_arrival_time" select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_planned_arrival_time" />
                            <xsl:variable name="transit_arrival_date" select="concat(substring(flight[day_of_week = 'Wednesday'][position() = 1]/transit_arrival_date,1,4), 
                                                                              substring(flight[day_of_week = 'Wednesday'][position() = 1]/transit_arrival_date,6,2), 
                                                                              substring(flight[day_of_week = 'Wednesday'][position() = 1]/transit_arrival_date,9,2))" />
                            <xsl:variable name="transit_airline_rcd"  select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_airline_rcd" />
                            <xsl:variable name="transit_flight_number"  select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_flight_number" />
                            <xsl:variable name="transit_points"  select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_points" />
                            <xsl:variable name="transit_points_name"  select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_points_name" />


                            <xsl:variable name="flight_param">
                              {&#034;flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/flight_id" />&#034;,
                              &#034;flight_number&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/flight_number" />&#034;,
                              &#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/origin_rcd" />&#034;,
                              &#034;airline_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/airline_rcd" />&#034;,
                              &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/destination_rcd" />&#034;,
                              &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/booking_class_rcd" />&#034;,
                              &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_flight_id" />&#034;,
                              &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
                              &#034;arrival_date&#034;:&#034;<xsl:value-of select="$arrival_date" />&#034;,
                            &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/planned_departure_time" />&#034;,
                              &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_airport_rcd" />&#034;,
                              &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/transit_fare_id" />&#034;,
                              &#034;fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/fare_id" />&#034;,
                              &#034;transit_airline_rcd&#034;:&#034;<xsl:value-of select="$transit_airline_rcd" />&#034;,
                              &#034;transit_flight_number&#034;:&#034;<xsl:value-of select="$transit_flight_number" />&#034;,
                              &#034;transit_boarding_class_rcd&#034;:&#034;<xsl:value-of select="transit_boarding_class_rcd" />&#034;,
                              &#034;transit_booking_class_rcd&#034;:&#034;<xsl:value-of select="transit_booking_class_rcd" />&#034;,
                              &#034;transit_planned_departure_time&#034;:&#034;<xsl:value-of select="$transit_planned_departure_time" />&#034;,
                              &#034;transit_planned_arrival_time&#034;:&#034;<xsl:value-of select="$transit_planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_arrival_date&#034;:&#034;<xsl:value-of select="$transit_arrival_date" />&#034;,
                              &#034;transit_points&#034;:&#034;<xsl:value-of select="$transit_points" />&#034; ,
                              &#034;transit_points_name&#034;:&#034;<xsl:value-of select="$transit_points_name" />&#034;}
                            </xsl:variable>                            
                            <input type="hidden" id="spnTime_{flight[day_of_week = 'Wednesday'][position() = 1]/flight_id}" value="{$flight_param}" name="hdTime_{../setting/flight_type}"/>
                            <a class="Low" onclick="SelectLlf('{../setting/flight_type}_WED{position()}');SelectTime('spnTime_{flight[day_of_week = 'Wednesday'][position() = 1]/flight_id}');">

                              <xsl:variable name="flight_param_wed">
                                {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/origin_rcd" />&#034;,
                                &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/origin_name" />&#034;,
                                &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/destination_rcd" />&#034;,
                                &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/departure_date" />&#034;,
                                &#034;total_adult_fare&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/total_adult_fare" />&#034;}
                              </xsl:variable>

                              <div class="ShowLowfaredate">
                                <input id="optLowfare_{../setting/flight_type}_WED{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_wed)}" style="display:none;" />

                                <label for="optLowfare_{../setting/flight_type}_WED{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Wednesday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>

                              <div id="spnLowfare_{../setting/flight_type}_WED{position()}">
                                <xsl:choose>
                                  <xsl:when test="$lowest_fare_amount = $total_fare_amount">
                                    <xsl:attribute name="class">LowestFare</xsl:attribute>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:attribute name="class">Lowfareprice</xsl:attribute>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <div class="sup">
                                  <xsl:value-of select="flight[day_of_week = 'Wednesday'][position() = 1]/currency_rcd"/>
                                </div>
                                <xsl:value-of select="format-number($total_fare_amount, '#,##0.00')"/>
                              </div>
                            </a>
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="Low" onclick="return false;">
                              <div class="ShowLowfaredate">
                                <label for="optLowfare_{../setting/flight_type}_WED{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Wednesday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>
                              <div id="spnLowfare_{../setting/flight_type}_WED{position()}" class="Lowfareprice">
                                N/A
                              </div>
                            </a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_THU{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Thursday']) > 0">
                        <xsl:variable name="total_fare_amount" select="flight[day_of_week = 'Thursday'][position() = 1]/total_adult_fare"/>
                        <xsl:choose>
                          <xsl:when test="$total_fare_amount != 'N/A'">
                            <xsl:variable name="transit_departure_date" select="concat(substring(flight[day_of_week = 'Thursday'][position() = 1]/transit_departure_date,1,4), 
                                                                            substring(flight[day_of_week = 'Thursday'][position() = 1]/transit_departure_date,6,2), 
                                                                            substring(flight[day_of_week = 'Thursday'][position() = 1]/transit_departure_date,9,2))" />

                            <xsl:variable name="departure_date" select="concat(substring(flight[day_of_week = 'Thursday'][position() = 1]/departure_date,1,4), 
                                                                    substring(flight[day_of_week = 'Thursday'][position() = 1]/departure_date,6,2), 
                                                                    substring(flight[day_of_week = 'Thursday'][position() = 1]/departure_date,9,2))" />
                            <xsl:variable name="arrival_date" select="concat(substring(flight[day_of_week = 'Thursday'][position() = 1]/arrival_date,1,4), 
                                                                    substring(flight[day_of_week = 'Thursday'][position() = 1]/arrival_date,6,2), 
                                                                    substring(flight[day_of_week = 'Thursday'][position() = 1]/arrival_date,9,2))" />


                            <xsl:variable name="transit_planned_departure_time" select="flight[day_of_week = 'Thursday'][position() = 1]/transit_planned_departure_time" />
                            <xsl:variable name="transit_planned_arrival_time" select="flight[day_of_week = 'Thursday'][position() = 1]/transit_planned_arrival_time" />
                            <xsl:variable name="transit_arrival_date" select="concat(substring(flight[day_of_week = 'Thursday'][position() = 1]/transit_arrival_date,1,4), 
                                                                              substring(flight[day_of_week = 'Thursday'][position() = 1]/transit_arrival_date,6,2), 
                                                                              substring(flight[day_of_week = 'Thursday'][position() = 1]/transit_arrival_date,9,2))" />
                            <xsl:variable name="transit_airline_rcd"  select="flight[day_of_week = 'Thursday'][position() = 1]/transit_airline_rcd" />
                            <xsl:variable name="transit_flight_number"  select="flight[day_of_week = 'Thursday'][position() = 1]/transit_flight_number" />
                            <xsl:variable name="transit_points"  select="flight[day_of_week = 'Thursday'][position() = 1]/transit_points" />
                            <xsl:variable name="transit_points_name"  select="flight[day_of_week = 'Thursday'][position() = 1]/transit_points_name" />

                            <xsl:variable name="flight_param">
                              {&#034;flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/flight_id" />&#034;,
                              &#034;flight_number&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/flight_number" />&#034;,
                              &#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/origin_rcd" />&#034;,
                              &#034;airline_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/airline_rcd" />&#034;,
                              &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/destination_rcd" />&#034;,
                              &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/booking_class_rcd" />&#034;,
                              &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/transit_flight_id" />&#034;,
                              &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
                              &#034;arrival_date&#034;:&#034;<xsl:value-of select="$arrival_date" />&#034;,
                            &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/planned_departure_time" />&#034;,
                              &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/transit_airport_rcd" />&#034;,
                              &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/transit_fare_id" />&#034;,
                              &#034;fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/fare_id" />&#034;,
                              &#034;transit_airline_rcd&#034;:&#034;<xsl:value-of select="$transit_airline_rcd" />&#034;,
                              &#034;transit_flight_number&#034;:&#034;<xsl:value-of select="$transit_flight_number" />&#034;,
                              &#034;transit_boarding_class_rcd&#034;:&#034;<xsl:value-of select="transit_boarding_class_rcd" />&#034;,
                              &#034;transit_booking_class_rcd&#034;:&#034;<xsl:value-of select="transit_booking_class_rcd" />&#034;,
                              &#034;transit_planned_departure_time&#034;:&#034;<xsl:value-of select="$transit_planned_departure_time" />&#034;,
                              &#034;transit_planned_arrival_time&#034;:&#034;<xsl:value-of select="$transit_planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_arrival_date&#034;:&#034;<xsl:value-of select="$transit_arrival_date" />&#034;,
                              &#034;transit_points&#034;:&#034;<xsl:value-of select="$transit_points" />&#034; ,
                              &#034;transit_points_name&#034;:&#034;<xsl:value-of select="$transit_points_name" />&#034;}
                            </xsl:variable>                            
                            <input type="hidden" id="spnTime_{flight[day_of_week = 'Thursday'][position() = 1]/flight_id}" value="{$flight_param}" name="hdTime_{../setting/flight_type}"/>
                            <a class="Low" onclick="SelectLlf('{../setting/flight_type}_THU{position()}');SelectTime('spnTime_{flight[day_of_week = 'Thursday'][position() = 1]/flight_id}');">

                              <xsl:variable name="flight_param_thu">
                                {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/origin_rcd" />&#034;,
                                &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/origin_name" />&#034;,
                                &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/destination_rcd" />&#034;,
                                &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/departure_date" />&#034;,
                                &#034;total_adult_fare&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/total_adult_fare" />&#034;}
                              </xsl:variable>

                              <div class="ShowLowfaredate">
                                <input id="optLowfare_{../setting/flight_type}_THU{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_thu)}" style="display:none;"/>

                                <label for="optLowfare_{../setting/flight_type}_THU{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Thursday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>

                              <div id="spnLowfare_{../setting/flight_type}_THU{position()}">
                                <xsl:choose>
                                  <xsl:when test="$lowest_fare_amount = $total_fare_amount">
                                    <xsl:attribute name="class">LowestFare</xsl:attribute>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:attribute name="class">Lowfareprice</xsl:attribute>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <div class="sup">
                                  <xsl:value-of select="flight[day_of_week = 'Thursday'][position() = 1]/currency_rcd"/>
                                </div>
                                <xsl:value-of select="format-number($total_fare_amount, '#,##0.00')"/>
                              </div>
                            </a>
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="Low" onclick="return false;">
                              <div class="ShowLowfaredate">
                                <label for="optLowfare_{../setting/flight_type}_THU{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Thursday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>
                              <div id="spnLowfare_{../setting/flight_type}_THU{position()}" class="Lowfareprice">
                                N/A
                              </div>
                            </a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_FRI{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Friday']) > 0">
                        <xsl:variable name="total_fare_amount" select="flight[day_of_week = 'Friday'][position() = 1]/total_adult_fare"/>
                        <xsl:choose>
                          <xsl:when test="$total_fare_amount != 'N/A'">
                            <xsl:variable name="transit_departure_date" select="concat(substring(flight[day_of_week = 'Friday'][position() = 1]/transit_departure_date,1,4), 
                                                                            substring(flight[day_of_week = 'Friday'][position() = 1]/transit_departure_date,6,2), 
                                                                            substring(flight[day_of_week = 'Friday'][position() = 1]/transit_departure_date,9,2))" />

                            <xsl:variable name="departure_date" select="concat(substring(flight[day_of_week = 'Friday'][position() = 1]/departure_date,1,4), 
                                                                    substring(flight[day_of_week = 'Friday'][position() = 1]/departure_date,6,2), 
                                                                    substring(flight[day_of_week = 'Friday'][position() = 1]/departure_date,9,2))" />
                            <xsl:variable name="arrival_date" select="concat(substring(flight[day_of_week = 'Friday'][position() = 1]/arrival_date,1,4), 
                                                                    substring(flight[day_of_week = 'Friday'][position() = 1]/arrival_date,6,2), 
                                                                    substring(flight[day_of_week = 'Friday'][position() = 1]/arrival_date,9,2))" />

                            <xsl:variable name="transit_planned_departure_time" select="flight[day_of_week = 'Friday'][position() = 1]/transit_planned_departure_time" />
                            <xsl:variable name="transit_planned_arrival_time" select="flight[day_of_week = 'Friday'][position() = 1]/transit_planned_arrival_time" />
                            <xsl:variable name="transit_arrival_date" select="concat(substring(flight[day_of_week = 'Friday'][position() = 1]/transit_arrival_date,1,4), 
                                                                              substring(flight[day_of_week = 'Friday'][position() = 1]/transit_arrival_date,6,2), 
                                                                              substring(flight[day_of_week = 'Friday'][position() = 1]/transit_arrival_date,9,2))" />
                            <xsl:variable name="transit_airline_rcd"  select="flight[day_of_week = 'Friday'][position() = 1]/transit_airline_rcd" />
                            <xsl:variable name="transit_flight_number"  select="flight[day_of_week = 'Friday'][position() = 1]/transit_flight_number" />
                            <xsl:variable name="transit_points"  select="flight[day_of_week = 'Friday'][position() = 1]/transit_points" />
                            <xsl:variable name="transit_points_name"  select="flight[day_of_week = 'Friday'][position() = 1]/transit_points_name" />


                            <xsl:variable name="flight_param">
                              {&#034;flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/flight_id" />&#034;,
                              &#034;flight_number&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/flight_number" />&#034;,
                              &#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/origin_rcd" />&#034;,
                              &#034;airline_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/airline_rcd" />&#034;,
                              &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/destination_rcd" />&#034;,
                              &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/booking_class_rcd" />&#034;,
                              &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/transit_flight_id" />&#034;,
                              &#034;arrival_date&#034;:&#034;<xsl:value-of select="$arrival_date" />&#034;,
                            &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
                              &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/planned_departure_time" />&#034;,
                              &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/transit_airport_rcd" />&#034;,
                              &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/transit_fare_id" />&#034;,
                              &#034;fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/fare_id" />&#034;,
                              &#034;transit_airline_rcd&#034;:&#034;<xsl:value-of select="$transit_airline_rcd" />&#034;,
                              &#034;transit_flight_number&#034;:&#034;<xsl:value-of select="$transit_flight_number" />&#034;,
                              &#034;transit_boarding_class_rcd&#034;:&#034;<xsl:value-of select="transit_boarding_class_rcd" />&#034;,
                              &#034;transit_booking_class_rcd&#034;:&#034;<xsl:value-of select="transit_booking_class_rcd" />&#034;,
                              &#034;transit_planned_departure_time&#034;:&#034;<xsl:value-of select="$transit_planned_departure_time" />&#034;,
                              &#034;transit_planned_arrival_time&#034;:&#034;<xsl:value-of select="$transit_planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_arrival_date&#034;:&#034;<xsl:value-of select="$transit_arrival_date" />&#034;,
                              &#034;transit_points&#034;:&#034;<xsl:value-of select="$transit_points" />&#034; ,
                              &#034;transit_points_name&#034;:&#034;<xsl:value-of select="$transit_points_name" />&#034;}
                            </xsl:variable>                            
                            <input type="hidden" id="spnTime_{flight[day_of_week = 'Friday'][position() = 1]/flight_id}" value="{$flight_param}" name="hdTime_{../setting/flight_type}"/>
                            <a class="Low" onclick="SelectLlf('{../setting/flight_type}_FRI{position()}');SelectTime('spnTime_{flight[day_of_week = 'Friday'][position() = 1]/flight_id}');">

                              <xsl:variable name="flight_param_fri">
                                {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/origin_rcd" />&#034;,
                                &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/origin_name" />&#034;,
                                &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/destination_rcd" />&#034;,
                                &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/departure_date" />&#034;,
                                &#034;total_adult_fare&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/total_adult_fare" />&#034;}
                              </xsl:variable>

                              <div class="ShowLowfaredate">
                                <input id="optLowfare_{../setting/flight_type}_FRI{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_fri)}" style="display:none;"/>

                                <label for="optLowfare_{../setting/flight_type}_FRI{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Friday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>

                              <div id="spnLowfare_{../setting/flight_type}_FRI{position()}">
                                <xsl:choose>
                                  <xsl:when test="$lowest_fare_amount = $total_fare_amount">
                                    <xsl:attribute name="class">LowestFare</xsl:attribute>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:attribute name="class">Lowfareprice</xsl:attribute>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <div class="sup">
                                  <xsl:value-of select="flight[day_of_week = 'Friday'][position() = 1]/currency_rcd"/>
                                </div>
                                <xsl:value-of select="format-number($total_fare_amount, '#,##0.00')"/>
                              </div>
                            </a>
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="Low" onclick="return false;">
                              <div class="ShowLowfaredate">
                                <label for="optLowfare_{../setting/flight_type}_FRI{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Friday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>
                              <div id="spnLowfare_{../setting/flight_type}_FRI{position()}" class="Lowfareprice">
                                N/A
                              </div>
                            </a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>

                  <td id="td_{../setting/flight_type}_SAT{position()}">
                    <xsl:choose>
                      <xsl:when test="count(flight[day_of_week = 'Saturday']) > 0">
                        <xsl:variable name="total_fare_amount" select="flight[day_of_week = 'Saturday'][position() = 1]/total_adult_fare"/>
                        <xsl:choose>
                          <xsl:when test="$total_fare_amount != 'N/A'">
                            <xsl:variable name="transit_departure_date" select="concat(substring(flight[day_of_week = 'Saturday'][position() = 1]/transit_departure_date,1,4), 
                                                                            substring(flight[day_of_week = 'Saturday'][position() = 1]/transit_departure_date,6,2), 
                                                                            substring(flight[day_of_week = 'Saturday'][position() = 1]/transit_departure_date,9,2))" />

                            <xsl:variable name="departure_date" select="concat(substring(flight[day_of_week = 'Saturday'][position() = 1]/departure_date,1,4), 
                                                                    substring(flight[day_of_week = 'Saturday'][position() = 1]/departure_date,6,2), 
                                                                    substring(flight[day_of_week = 'Saturday'][position() = 1]/departure_date,9,2))" />
                            <xsl:variable name="arrival_date" select="concat(substring(flight[day_of_week = 'Saturday'][position() = 1]/arrival_date,1,4), 
                                                                    substring(flight[day_of_week = 'Saturday'][position() = 1]/arrival_date,6,2), 
                                                                    substring(flight[day_of_week = 'Saturday'][position() = 1]/arrival_date,9,2))" />


                            <xsl:variable name="transit_planned_departure_time" select="flight[day_of_week = 'Saturday'][position() = 1]/transit_planned_departure_time" />
                            <xsl:variable name="transit_planned_arrival_time" select="flight[day_of_week = 'Saturday'][position() = 1]/transit_planned_arrival_time" />
                            <xsl:variable name="transit_arrival_date" select="concat(substring(flight[day_of_week = 'Saturday'][position() = 1]/transit_arrival_date,1,4), 
                                                                              substring(flight[day_of_week = 'Saturday'][position() = 1]/transit_arrival_date,6,2), 
                                                                              substring(flight[day_of_week = 'Saturday'][position() = 1]/transit_arrival_date,9,2))" />
                            <xsl:variable name="transit_airline_rcd"  select="flight[day_of_week = 'Saturday'][position() = 1]/transit_airline_rcd" />
                            <xsl:variable name="transit_flight_number"  select="flight[day_of_week = 'Saturday'][position() = 1]/transit_flight_number" />
                            <xsl:variable name="transit_points"  select="flight[day_of_week = 'Saturday'][position() = 1]/transit_points" />
                            <xsl:variable name="transit_points_name"  select="flight[day_of_week = 'Saturday'][position() = 1]/transit_points_name" />


                            <xsl:variable name="flight_param">
                              {&#034;flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/flight_id" />&#034;,
                              &#034;flight_number&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/flight_number" />&#034;,
                              &#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/origin_rcd" />&#034;,
                              &#034;airline_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/airline_rcd" />&#034;,
                              &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/destination_rcd" />&#034;,
                              &#034;booking_class_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/booking_class_rcd" />&#034;,
                              &#034;transit_flight_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/transit_flight_id" />&#034;,
                              &#034;departure_date&#034;:&#034;<xsl:value-of select="$departure_date" />&#034;,
                              &#034;arrival_date&#034;:&#034;<xsl:value-of select="$arrival_date" />&#034;,
                            &#034;planned_departure_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/planned_departure_time" />&#034;,
                              &#034;planned_arrival_time&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_airport_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/transit_airport_rcd" />&#034;,
                              &#034;transit_fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/transit_fare_id" />&#034;,
                              &#034;fare_id&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/fare_id" />&#034;,
                              &#034;transit_airline_rcd&#034;:&#034;<xsl:value-of select="$transit_airline_rcd" />&#034;,
                              &#034;transit_flight_number&#034;:&#034;<xsl:value-of select="$transit_flight_number" />&#034;,
                              &#034;transit_boarding_class_rcd&#034;:&#034;<xsl:value-of select="transit_boarding_class_rcd" />&#034;,
                              &#034;transit_booking_class_rcd&#034;:&#034;<xsl:value-of select="transit_booking_class_rcd" />&#034;,
                              &#034;transit_planned_departure_time&#034;:&#034;<xsl:value-of select="$transit_planned_departure_time" />&#034;,
                              &#034;transit_planned_arrival_time&#034;:&#034;<xsl:value-of select="$transit_planned_arrival_time" />&#034;,
                              &#034;transit_departure_date&#034;:&#034;<xsl:value-of select="$transit_departure_date" />&#034;,
                              &#034;transit_arrival_date&#034;:&#034;<xsl:value-of select="$transit_arrival_date" />&#034;,
                              &#034;transit_points&#034;:&#034;<xsl:value-of select="$transit_points" />&#034; ,
                              &#034;transit_points_name&#034;:&#034;<xsl:value-of select="$transit_points_name" />&#034;}
                            </xsl:variable>                            
                            <input type="hidden" id="spnTime_{flight[day_of_week = 'Saturday'][position() = 1]/flight_id}" value="{$flight_param}" name="hdTime_{../setting/flight_type}"/>
                            <a class="Low" onclick="SelectLlf('{../setting/flight_type}_SAT{position()}');SelectTime('spnTime_{flight[day_of_week = 'Saturday'][position() = 1]/flight_id}');">

                              <xsl:variable name="flight_param_sat">
                                {&#034;origin_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/origin_rcd" />&#034;,
                                &#034;origin_name&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/origin_name" />&#034;,
                                &#034;destination_rcd&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/destination_rcd" />&#034;,
                                &#034;departure_date&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/departure_date" />&#034;,
                                &#034;total_adult_fare&#034;:&#034;<xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/total_adult_fare" />&#034;}
                              </xsl:variable>

                              <div class="ShowLowfaredate">
                                <input id="optLowfare_{../setting/flight_type}_SAT{position()}" name="optLowfare_{../setting/flight_type}" type="radio" value="{normalize-space($flight_param_sat)}" style="display:none;" />

                                <label for="optLowfare_{../setting/flight_type}_SAT{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Saturday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>

                              <div id="spnLowfare_{../setting/flight_type}_SAT{position()}">
                                <xsl:choose>
                                  <xsl:when test="$lowest_fare_amount = $total_fare_amount">
                                    <xsl:attribute name="class">LowestFare</xsl:attribute>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:attribute name="class">Lowfareprice</xsl:attribute>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <div class="sup">
                                  <xsl:value-of select="flight[day_of_week = 'Saturday'][position() = 1]/currency_rcd"/>
                                </div>
                                <xsl:value-of select="format-number($total_fare_amount, '#,##0.00')"/>
                              </div>
                            </a>
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="Low" onclick="return false;">
                              <div class="ShowLowfaredate">
                                <label for="optLowfare_{../setting/flight_type}_SAT{position()}">
                                  <xsl:call-template name="GetDay">
                                    <xsl:with-param name="Date" select="flight[day_of_week = 'Saturday'][position() = 1]/departure_date"></xsl:with-param>
                                  </xsl:call-template>
                                </label>
                              </div>
                              <div id="spnLowfare_{../setting/flight_type}_SAT{position()}" class="Lowfareprice">
                                N/A
                              </div>
                            </a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>

                      <xsl:otherwise>
                        <div class="DateCss">&#160;</div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </div>
          <div class="clearboth"></div>
        </div>
        <div class="clearboth"></div>
      </div>
    </xsl:if>
  </xsl:template>
  <msxsl:script implements-prefix="JCode" language="JavaScript">
    function getDateAddMonth(strDate, addValue)
    {
    var dtDate = new Date(strDate.substring(0,4) + '/' + strDate.substring(4,6) + '/' + strDate.substring(6,8));
    var todayDate = new Date();

    var newselmonth = (dtDate.getMonth())+addValue;
    dtDate.setMonth(newselmonth);

    var departmonth = dtDate.getYear() + (dtDate.getMonth()+ ((dtDate.getYear()-todayDate.getYear())*12));
    var todaymonth = todayDate.getYear() + todayDate.getMonth();

    if( departmonth >= todaymonth)
    {
    var strMonth;
    var strday;

    if ((dtDate.getMonth() + 1).toString().length == 1)
    {
    strMonth = '0' + (dtDate.getMonth() + 1).toString()
    }
    else
    {
    strMonth = (dtDate.getMonth() + 1).toString()
    }

    if (dtDate.getDate().toString().length == 1)
    {
    strday = '0' + dtDate.getDate().toString()
    }
    else
    {
    strday = dtDate.getDate().toString()
    }

    return (dtDate.getYear().toString() + strMonth);
    }
    else
    {
    return '000000';
    }
    }

  </msxsl:script>
</xsl:stylesheet>