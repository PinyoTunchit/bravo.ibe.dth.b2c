<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">
  <xsl:template name="paging" match="object/paging">
    <xsl:variable name="page">
      <xsl:value-of select="/object/paging/pageindex"/>
    </xsl:variable>
    <table border="0" cellspacing="0" cellpadding="0" class="TBLLiveBooking">
      <xsl:if test="string-length($page)>0">
        <tr>
          <td class="Paging">
            <div align="right">
              <xsl:for-each select="/object/paging/pageindex">
                <xsl:if test="/object/paging/selected!=position()">
                  <a>
                    <xsl:attribute name="href">
                      javascript:GetPagingFFP('<xsl:value-of select="position()"/>');
                    </xsl:attribute>
                    <xsl:value-of select="position()"/>
                  </a>&#160;
                </xsl:if>
                <xsl:if test="/object/paging/selected=position()">
                  <xsl:value-of select="position()"/>&#160;
                </xsl:if>
              </xsl:for-each>
            </div>
          </td>
        </tr>
      </xsl:if>
    </table>
  </xsl:template>
  
	<xsl:template name="DateFormat">
		<xsl:param name="DateTime" />
		<!-- new date format 2006-01-14T08:55:22 -->
		<xsl:value-of select="substring($DateTime,9,2)" />/<xsl:value-of select="substring($DateTime,6,2)" />/<xsl:value-of select="substring($DateTime,0,5)" />
	</xsl:template>
	
	
	<xsl:template  match="Transactions">
		<table border="0" cellspacing="0" cellpadding="0" class="TBLMilleage">
			<tr>
				<td class="HeadCOL1">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_25','Type')" />
        </td>
				<td class="HeadCOL2">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_26','Passenger')" />
        </td>
				<td class="HeadCOL3">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_27','Description')" />
        </td>
				<td class="HeadCOL4">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_28','Expiry Date')" />
        </td>
				<td class="HeadCOL5">
          <xsl:value-of select="tikLanguage:get('FFP_My_Booking_29','Points')" />
        </td>
				<td class="HeadCOL6">&#160;</td>
			</tr>
			<xsl:if test="count(Transaction)!=0">
				<xsl:for-each select="Transaction">
					<tr>
						<td class="BodyCOL1">
							<xsl:value-of select="segment_type_rcd"/>
						</td>
						<td class="BodyCOL2">
							<xsl:if test="(string-length(booking_id) != 0) and (string-length(passenger_id) != 0) and (string-length(booking_segment_id) != 0)">
								<a>
									<xsl:attribute name="href">
										javascript:OpenTicketDetail('<xsl:value-of select ="booking_id"/>','<xsl:value-of select ="passenger_id"/>','<xsl:value-of select ="booking_segment_id"/>');
									</xsl:attribute>
									<xsl:value-of select="passenger_name"/>
								</a>
							</xsl:if>
							<xsl:if test="(string-length(booking_id) = 0) and (string-length(passenger_id) = 0) and (string-length(booking_segment_id) = 0) ">
								<xsl:value-of select="passenger_name"/>
							</xsl:if>
						</td>
						<td class="BodyCOL3">
							<xsl:value-of select="description"/>
						</td>
						<td class="BodyCOL4">
							<xsl:if test="string-length(expiry_date) != 0">
								<xsl:call-template name="DateFormat">
									<xsl:with-param name="DateTime" select="expiry_date"></xsl:with-param>
								</xsl:call-template>
							</xsl:if>
							<xsl:if test="string-length(expiry_date) = 0">
                <xsl:value-of select="tikLanguage:get('FFP_My_Booking_30','No Expiry')" />
							</xsl:if>
						</td>
						<td class="BodyCOL5">
							<xsl:value-of select="format-number(ffp_total,'#,##0.00')"/>
						</td>
						<td class="BodyCOL6">
							<div class="ArrowGray">
								<xsl:attribute name="onclick">
									LoadBookingDetail('<xsl:value-of select="booking_id"/>','<xsl:value-of select="tikLanguage:get('FFP_My_Booking_12','No booking found. This transaction might be added manually. Please contact airline for more detail.')" />')
								</xsl:attribute>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(Transaction)=0">
				<tr>
					<td colspan="6" class="BodyCOL5" style="text-align:center;color:red;">
            <xsl:value-of select="tikLanguage:get('FFP_My_Booking_31','Transactions not found.')" />
          </td>
				</tr>
			</xsl:if>
		</table>
	</xsl:template>
</xsl:stylesheet>