<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:JCode="http://www.tiksystems.com/B2C"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
							  xmlns:tikLanguage="tik:Language">

  <xsl:output method="html" indent="no"/>
  <xsl:key name="flight_id_group" match="Booking/Mapping" use="flight_id"/>
  <xsl:key name="tax_rcd_group" match="Booking/Tax" use="tax_rcd"/>
  <xsl:template name="DateFormat">
    <xsl:param name="Date">
    </xsl:param>
    <xsl:if test="string-length($Date)!=0">
      <xsl:value-of select="substring($Date,7,2)"/>/<xsl:value-of select="substring($Date,5,2)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="TimeFormat">
    <xsl:param name ="Time"></xsl:param>

    <xsl:if test="string-length($Time) =  3 ">
      0<xsl:value-of select="substring($Time,1,1)"  />:<xsl:value-of select="substring($Time,2,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 4 ">
      <xsl:value-of select="substring($Time,1,2)"  />:<xsl:value-of select="substring($Time,3,2)"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 2 ">
      00:<xsl:value-of select="$Time"  />
    </xsl:if>

    <xsl:if test="string-length($Time) = 1 ">
      00:0<xsl:value-of select="$Time"  />
    </xsl:if>
  </xsl:template>
  <xsl:template name="DayOfWeek">
    <xsl:param name ="Day"></xsl:param>
    <xsl:choose>
      <xsl:when test="number($Day) = 0">
        <xsl:value-of select="tikLanguage:GetAbbrivateDay(0)"/>
      </xsl:when>
      <xsl:when test="number($Day) = 1">
        <xsl:value-of select="tikLanguage:GetAbbrivateDay(1)"/>
      </xsl:when>
      <xsl:when test="number($Day) = 2">
        <xsl:value-of select="tikLanguage:GetAbbrivateDay(2)"/>
      </xsl:when>
      <xsl:when test="number($Day) = 3">
        <xsl:value-of select="tikLanguage:GetAbbrivateDay(3)"/>
      </xsl:when>
      <xsl:when test="number($Day) = 4">
        <xsl:value-of select="tikLanguage:GetAbbrivateDay(4)"/>
      </xsl:when>
      <xsl:when test="number($Day) = 5">
        <xsl:value-of select="tikLanguage:GetAbbrivateDay(5)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="tikLanguage:GetAbbrivateDay(6)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/">
    <xsl:variable name="flight_type" select="Booking/setting/flight_type" />
    <xsl:variable name="num_adult" select="Booking/setting/adult" />
    <xsl:variable name="num_chd" select="Booking/setting/child" />
    <xsl:variable name="num_inf" select="Booking/setting/infant" />
    <xsl:variable name="currency_rcd" select="Booking/Mapping[position() = 1]/currency_rcd" />

    <xsl:variable name="currency_sign">
    </xsl:variable>
    <xsl:if test="Booking/setting/flight_type = 'Outward'">
      <input type="hidden" id="hdCurrencySign" value="{$currency_sign}" />
    </xsl:if>
    <div class="YourFlightSelected">
      <div class="OutboundFlight">
        <div class="FlightType">
          <xsl:choose>
            <xsl:when test="Booking/setting/flight_type = 'Outward'">
              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_3','Outbound flight')" />
              <div class="clear-all"></div>
              <div class="ArrowIconout">
                <img src="App_Themes/Default/Images/outbound.png" class="displayimage" id="imgFromImage" alt="" />
              </div>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_4','Inbound flight')" />
              <div class="clear-all"></div>
              <div class="ArrowIconin">
                <img src="App_Themes/Default/Images/inbound.png" class="displayimage" id="imgFromImage" alt="" />
              </div>
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </div>

      <div class="YourFlight">
        <ul>
          <li>
            <xsl:value-of select="Booking/setting/airline_rcd"/>&#160;<xsl:value-of select="Booking/setting/flight_number"/>
          </li>
          <li id="spAircraft_{Booking/setting/flight_type}">
          </li>        
        </ul>

        <div class="clear-all"></div>

        <div class="YourFlightTime">
          <div class="YourFlightTimeDept">
            <xsl:value-of select="tikLanguage:get('Booking_Step_2_24','Dept')" />&#160;

            <span>
              <xsl:call-template name="DateFormat">
                <xsl:with-param name="Date" select="Booking/setting/departure_date"></xsl:with-param>
              </xsl:call-template>
            </span>&#160;

            <span>
              <xsl:call-template name="TimeFormat">
                <xsl:with-param name="Time" select="Booking/setting/planned_departure_time"></xsl:with-param>
              </xsl:call-template>
            </span>
          </div>

          <div class="YourFlightTimeArri">
            <xsl:value-of select="tikLanguage:get('Booking_Step_2_25','Arriv')" />&#160;

            <span>
              <xsl:call-template name="DateFormat">
                <xsl:with-param name="Date" select="Booking/setting/arrival_date"></xsl:with-param>
              </xsl:call-template>
            </span>&#160;

            <span>
              <xsl:call-template name="TimeFormat">
                <xsl:with-param name="Time" select="Booking/setting/planned_arrival_time"></xsl:with-param>
              </xsl:call-template>
            </span>
          </div>

        </div>

        <div class="clear-all"></div>

        <xsl:choose>
          <xsl:when test="string-length(Booking/setting/transit_flight_id) = 0">
            <div class="flightnameleft"></div>
            <div class="flightname">
              <xsl:value-of select="Booking/setting/origin_name"/>
            </div>
            <div class="flightnameright"></div>

            <div class="clear-all"></div>

            <div class="flightto">
              <img src="App_Themes/Default/Images/to.png" alt="" title="" />
            </div>

            <div class="clear-all"></div>

            <div class="flightnameleft"></div>
            <div class="flightname">
              <xsl:value-of select="Booking/setting/destination_name"/>
            </div>
            <div class="flightnameright"></div>
          </xsl:when>

          <xsl:otherwise>
            <div class="flightnameleft"></div>
            <div class="flightname">
              <xsl:value-of select="Booking/setting/origin_name"/>
            </div>
            <div class="flightnameright"></div>

            <div class="clear-all"></div>

            <div class="flightto">
              <img src="App_Themes/Default/Images/to.png" alt="" title="" />
            </div>

            <div class="clear-all"></div>

            <div class="flightnameleft"></div>
            <div class="flightname">
              <xsl:value-of select="Booking/setting/transit_airport_name"/>
            </div>
            <div class="flightnameright"></div>
          </xsl:otherwise>
        </xsl:choose>

        <div class="clear-all"></div>

      </div>

      <xsl:if test ="string-length(Booking/setting/transit_flight_id) > 0">
        <div class="YourFlight">
          <!--<ul>
            <li>
              <xsl:call-template name="DayOfWeek">
                <xsl:with-param name="Day" select="Booking/setting/transit_departure_day"></xsl:with-param>
              </xsl:call-template>
              &#160;
              <xsl:call-template name="DateFormat">
                <xsl:with-param name="Date" select="Booking/setting/transit_departure_date"></xsl:with-param>
              </xsl:call-template>
              &#160;
              &#160;
              <xsl:call-template name="DayOfWeek">
                <xsl:with-param name="Day" select="Booking/setting/transit_arrival_day"></xsl:with-param>
              </xsl:call-template>
              &#160;
              <xsl:call-template name="DateFormat">
                <xsl:with-param name="Date" select="Booking/setting/transit_arrival_date"></xsl:with-param>
              </xsl:call-template>
            </li>

            <li>
              <xsl:value-of select="Booking/setting/transit_airline_rcd"/>&#160;<xsl:value-of select="Booking/setting/transit_flight_number"/>
            </li>
          </ul>-->

          <div class="YourFlight">
            <ul>
              <li>
                <xsl:value-of select="Booking/setting/transit_airline_rcd"/>&#160;<xsl:value-of select="Booking/setting/transit_flight_number"/>
              </li>
              <li id="spAircraft_transit_{Booking/setting/flight_type}">
              </li>
            </ul>

            <div class="clear-all"></div>

            <div class="YourFlightTime">
              <div class="YourFlightTimeDept">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_24','Dept')" />&#160;

                <span>
                  <xsl:call-template name="DateFormat">
                    <xsl:with-param name="Date" select="Booking/setting/transit_departure_date"></xsl:with-param>
                  </xsl:call-template>
                </span>&#160;

                <span>
                  <xsl:call-template name="TimeFormat">
                    <xsl:with-param name="Time" select="Booking/setting/transit_planned_departure_time"></xsl:with-param>
                  </xsl:call-template>
                </span>
              </div>

              <div class="YourFlightTimeArri">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_25','Arriv')" />&#160;

                <span>
                  <xsl:call-template name="DateFormat">
                    <xsl:with-param name="Date" select="Booking/setting/transit_arrival_date"></xsl:with-param>
                  </xsl:call-template>
                </span>&#160;

                <span>
                  <xsl:call-template name="TimeFormat">
                    <xsl:with-param name="Time" select="Booking/setting/transit_planned_arrival_time"></xsl:with-param>
                  </xsl:call-template>
                </span>
              </div>

            </div>

            <div class="clear-all"></div>

          </div>

          <div class="flightnameleft"></div>
          <div class="flightname">
            <xsl:value-of select="Booking/setting/transit_airport_name"/>
          </div>
          <div class="flightnameright"></div>

          <div class="clear-all"></div>

          <div class="flightto">
            <img src="App_Themes/Default/Images/to.png" alt="" title="" />
          </div>

          <div class="clear-all"></div>

          <div class="flightnameleft"></div>
          <div class="flightname">
            <xsl:value-of select="Booking/setting/destination_name"/>
          </div>
          <div class="flightnameright"></div>

          <div class="clear-all"></div>

        </div>
      </xsl:if>

      <div class="FareRule">
        <xsl:value-of select="Booking/Mapping[position() = 1]/restriction_text" />
      </div>
      <div class="clear-all"></div>

      <div class="airfare">
        <div class="airfareleftbox"></div>
        <div class="airfaremiddle"></div>
        <div class="airfarerightbox"></div>



        <div class="airfarecontent">

          <div class="airfareheader">
            <div class="airfaretext">
              <div class="lefttitle">
                <xsl:value-of select="tikLanguage:get('Booking_Step_2_22','Airfare')" />
              </div>
              <div class="rightcurrency">
                (<xsl:value-of select="$currency_rcd"/>)
              </div>
            </div>
          </div>
          <div class="clear-all"></div>
          <xsl:variable name="TotalFare" select="sum(Booking/Mapping/fare_amount)" />

          <div class="charges">
            <ul id="ul_{$flight_type}_FareInfo">
              <li>
                <div class="Taxes_Fees">
                  <xsl:value-of select="tikLanguage:get('Booking_Step_2_11','Fare')" />
                </div>
                <div class="collapse">
                  <img src="App_Themes/Default/Images/expand.png" alt="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" title="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" />
                </div>

                <div class="charges_price_total">

                  <xsl:value-of select="$currency_sign"/>
                  <xsl:value-of select="format-number($TotalFare,'#,##0.00')"/>
                </div>
                <div class="clear-all"></div>
                <ul>
                  <xsl:if test="number(Booking/setting/adult) > 0">
                    <li>
                      <div class="charges_name">
                        <div class="amount">
                          - <xsl:value-of select="Booking/setting/adult"/>
                        </div>
                        <div class="yourselecttype">
                          <xsl:choose>
                            <xsl:when test="number(Booking/setting/adult) > 1">
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_6','Adults')" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_16','Adult')" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                      </div>

                      <div class="charges_price">
                        <xsl:value-of select="$currency_sign"/>
                        <xsl:value-of select="format-number(sum(Booking/Mapping[passenger_type_rcd = 'ADULT']/fare_amount),'#,##0.00')"/>
                      </div>

                    </li>

                  </xsl:if>

                  <xsl:if test="number(Booking/setting/child) > 0">
                    <li>
                      <div class="charges_name">
                        <div class="amount">
                          - <xsl:value-of select="Booking/setting/child"/>
                        </div>
                        <div class="yourselecttype">
                          <xsl:choose>
                            <xsl:when test="number(Booking/setting/child) > 1">
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_7','Children')" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_17','Child')" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                      </div>

                      <div class="charges_price">
                        <xsl:value-of select="$currency_sign"/>
                        <xsl:value-of select="format-number(sum(Booking/Mapping[passenger_type_rcd = 'CHD']/fare_amount),'#,##0.00')"/>
                      </div>

                    </li>
                  </xsl:if>

                  <xsl:if test="number(Booking/setting/infant) > 0">
                    <li>
                      <div class="charges_name">
                        <div class="amount">
                          - <xsl:value-of select="Booking/setting/infant"/>
                        </div>
                        <div class="yourselecttype">
                          <xsl:choose>
                            <xsl:when test="number(Booking/setting/infant) > 1">
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_8','Infants')" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_18','Infant')" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                      </div>

                      <div class="charges_price">
                        <xsl:value-of select="$currency_sign"/>
                        <xsl:value-of select="format-number(sum(Booking/Mapping[passenger_type_rcd = 'INF']/fare_amount),'#,##0.00')"/>
                      </div>

                    </li>
                  </xsl:if>
                </ul>
              </li>
            </ul>
          </div>
          <div class="clear-all"></div>

          <xsl:if test="sum(Booking/Tax/tax_amount) > 0">
            <div class="charges">
              <ul id="ul_{$flight_type}_TaxInfo">
                <li>
                  <div class="Taxes_Fees">
                    <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_9','Taxes and Airport charges')" />
                  </div>
                  <div class="collapse">
                    <img src="App_Themes/Default/Images/expand.png" alt="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" title="{tikLanguage:get('Booking_Step_2_47','click to check detail')}" />
                  </div>

                  <div class="charges_price_total">
                    <xsl:value-of select="$currency_sign"/>
                    <xsl:value-of select="format-number(sum(Booking/Tax/sales_amount),'#,##0.00')"/>
                  </div>
                  <div class="clear-all"></div>
                  <ul>
                    <xsl:for-each select="Booking/Tax[count(. | key('tax_rcd_group', tax_rcd)[1]) = 1]">
                      <xsl:variable name="tax_rcd" select="tax_rcd" />
                      <li>
                        <div class="charges_name">
                          - <xsl:value-of select="display_name" />
                        </div>

                        <div class="charges_price">
                          <xsl:value-of select="$currency_sign"/>
                          <xsl:value-of select="format-number(sum(../Tax[tax_rcd = $tax_rcd]/sales_amount),'#,##0.00')"/>
                        </div>
                      </li>
                    </xsl:for-each>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="clear-all"></div>
          </xsl:if>
          <xsl:variable name="TotalFareIncl" select="sum(Booking/Mapping/fare_amount_incl)" />

          <xsl:variable name="TotalTax" select="sum(Booking/Mapping/tax_amount)" />
          <xsl:variable name="TotalTaxIncl" select="sum(Booking/Mapping/tax_amount_incl)" />

          <xsl:variable name="TotalYQ" select="sum(Booking/Mapping/yq_amount)" />
          <xsl:variable name="TotalYQIncl" select="sum(Booking/Mapping/yq_amount_incl)" />

          <xsl:variable name="total_vat" select="number($TotalFareIncl - $TotalFare) + number($TotalTaxIncl - $TotalTax)  + number($TotalYQIncl - $TotalYQ)"/>
          <xsl:if test="$total_vat > 0">
            <div class="charges">
              <div class="Taxes_Fees">
                <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_13','VAT')" />
              </div>

              <div class="charges_price_total">
                <xsl:value-of select="$currency_sign"/>
                <xsl:value-of select="format-number($total_vat,'#,##0.00')"/>
              </div>
              <div class="clear-all"></div>
            </div>
          </xsl:if>
          <div class="clear-all"></div>

          <div class="totalfare fontB">
            <xsl:choose>
              <xsl:when test="Booking/setting/flight_type = 'Outward'">
                <div class="totalflight">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_10','Total fare outbound flight')" />
                </div>
              </xsl:when>
              <xsl:otherwise>
                <div class="totalflight">
                  <xsl:value-of select="tikLanguage:get('Flight_Selection_Summary_11','Total fare inbound flight')" />
                </div>
              </xsl:otherwise>
            </xsl:choose>
            
            <xsl:variable name="SubTotal" select="sum(Booking/Mapping/net_total)" />
            <div id="dv_{$flight_type}_TotalInclTax" class="charges_price_total">
              <xsl:value-of select="$currency_sign"/>
              <xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>
            </div>
            <input type="hidden" name="hdSubTotal" value="{format-number($SubTotal,'0.00')}" />
          </div>
        </div>

        <div class="clear-all"></div>

        <div class="airfareleftbottombox"></div>
        <div class="airfaremiddlebottom"></div>
        <div class="airfarerightbottombox"></div>

      </div>
      <div class="clear-all"></div>
    </div>
  </xsl:template>
</xsl:stylesheet>


