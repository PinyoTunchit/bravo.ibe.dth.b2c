<%@ Page Language="C#" AutoEventWireup="true" Inherits="tikAeroB2C._Default" Async="true" EnableViewState="false" %>
<%@ Register TagPrefix="AvailabilitySearch" TagName="AvailabilitySearch" src="~/UserControls/SearchAvailability.ascx" %>
<%@ Register Src="UserControls/ClientLogon.ascx" TagName="ClientLogon" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="KEYWORDS" content="airline, booking flight, online flight booking" />
    <meta name="DESCRIPTION" content="Tassili Airlines" />

    <link href="App_Themes/Default/Images/favicon.ico" type="image/x-icon" rel="shortcut icon" />
    <link href="App_Themes/Default/Style/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/fg-tooltip.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/htmlie7.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/jquery-ui-1.7.1.custom.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/main.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/popup.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Default/Style/style.css" rel="stylesheet" type="text/css" />
	
    <link href="App_Themes/Default/Style/ui.daterangepicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/jQuery/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="Scripts/jQuery/jquery.maskedinput-1.2.2.min.js"></script>
    <script type="text/javascript" src="Scripts/jQuery/jquery.base64.min.js"></script>
    <script type="text/javascript" src="Scripts/jQuery/jquery.collapse.js"></script>

      
    <script type="text/javascript" src="Scripts/GlobalSetup.js"></script>
    <script type="text/javascript" src="Scripts/Main.js"></script>
    <script type="text/javascript" src="Scripts/uxCalendar.js"></script>
    <script type="text/javascript" src="Scripts/Availability.js?v01"></script>
    <script type="text/javascript" src="Scripts/FlightSummary.js"></script>
    <script type="text/javascript" src="Scripts/PassengerDetail.js"></script>
    <script type="text/javascript" src="Scripts/SeatMap.js"></script>
    <script type="text/javascript" src="Scripts/payment.js"></script>
    <script type="text/javascript" src="Scripts/Registration.js"></script>

<%--    <script type="text/javascript" src="Scripts/Release/DTH_SCRIPT_3.2.0.5.js"></script>--%>
      
<%--      <script src="Scripts/Main.js"></script>     
      <script src="Scripts/Availability.js"></script>
      <script src="Scripts/PassengerDetail.js"></script>
      <script src="Scripts/payment.js"></script>--%>
<title>Tassili</title>
   <%-- <script type="text/javascript" language="javascript">
        function wireUpEvents() {
            window.onbeforeunload = function () {
                if ($.browser.msie == true) {
                    if ((window.event.clientX < 0) || (window.event.clientY < 0)) {
                        CloseSession();
                    }
                }
                else {
                    CloseSession();
                }
            }
        }

    </script>--%>
</head>
  <!--<body onbeforeunload="CloseSession();">-->
  <body>
    <form name="frmMain" id="frmMain" runat="server">
      <asp:ScriptManager ID="smService" runat="server" ScriptMode="Release" EnablePartialRendering="false">
        <Services>
          <asp:ServiceReference Path="WebService/B2cService.asmx" />
        </Services>
      </asp:ScriptManager>

      <!--Fading-->
      <input type="hidden" id="hdLang" name="hdLang" value="<%=tikAeroB2C.B2CSession.LanCode %>" />
      <div id="dvProgressBar" class="DisableWindow"></div>

      <!--progress bar-->
      <div id="dvFormHolder" class="IndicatorPax"></div>
	  
	  <!--Start Error Box-->
		<div id="dvMessageBox" class="ErrorBox">
			<div class="xouter">
				<div class="xcontainer">
					<div class="xinner LoadingBoxError">
							<div class="errortop"></div>
							<div  class="errorcontent">
							<div class="ErrorBoxHeader">
								<img src="App_Themes/Default/Images/mainlogo.png" alt="Tassili" />
							</div>
							<div id="dvErrorcontent" class="ErrorContent">
						    	<div id="dvMessageIcon" class="ErrorBoxMid">
								<img src="App_Themes/Default/Images/alert.png" alt=""/>
							</div>
							    <div id="dvErrorMessage" class="ErrorBoxBot RedFontB">
								Error Text
							</div>
							</div>
							<div class="clear-all"></div>
                            <div class="AjaxButton" id="dvConfirmOK" style="display:none">
								<a href="#" onclick="return false;" class="defaultbutton">
									<span>
										<%=tikAeroB2C.Classes.Language.Value("Default_Value_12", "Confirm", _stdLanguage)%>
									</span>
								</a>
								<div class="clear-all"></div>
							</div>

                            <div class="AjaxButton">
								<a href="#" onclick="return false;" class="defaultbutton">
									<span id="dvMessageButtonText" onclick="CloseMessageBox();">
										<%=tikAeroB2C.Classes.Language.Value("Default_Value_11", "OK", _stdLanguage)%>
									</span>
								</a>
								<div class="clear-all"></div>
							</div>

							<div class="clear-all"></div>
							</div>
							<div class="errorbottom"></div>
					</div>
				</div>
			</div>
		</div>
      <!--End Error Box-->


      <div id="dvLoad" class="ProgressBox">
        <div class="xouter">
          <div class="xcontainer">
            <div class="xinner LoadingBox">
              <div class="loadingtext">
                <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_27", "Now processing your request...", _stdLanguage)%>
              </div>
              <div class="clear-all" ></div>

              <div class="LoadingBoxMid">
                <img src="App_Themes/Default/Images/progressbar.gif" alt=""/>
              </div>
              <div class="LoadingBoxFoot"></div>

              <div class="clear-all"></div>

            </div>
          </div>
        </div>
      </div>

      <div class="clear-all"></div>

		<div id="dvLoadBar" class="ProgressBarBox">
			<div class="xouter">
				<div class="xcontainer">
					<div class="xinner LoadingBox">
                        <div id="dvPaymentLoadingMessage" class="loadingtext">
                            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_52", "Please wait while we process your booking or payment process", _stdLanguage)%>
                        </div>
						<div class="clear-all" ></div>
						  
						<div class="LoadingBoxMid">
							<img src="App_Themes/Default/Images/progressbar.gif" alt=""/>
						</div>
					
						<div class="LoadingBoxFoot"></div>
					</div>
				</div>
			</div>
		</div>

      <div class="Wrapper">


		<div class="Header">
			<div class="mainlogo">
				<a href="http://www.tassiliairlines.dz/">
					<img src="App_Themes/Default/Images/mainlogo.png" alt="Tassili" title="Tassili" />
				</a>
			</div>
		
            <div style="display:none">
			<div id="dvHeaderMenu" class="HeaderMenu">
				<div class="topmenuleft"></div>
				<div class="topmenucontent">
				<!--Menu Section-->
					<div id="dvMenu" class="menu" runat="server"></div>
				</div>
				<div class="topmenuright"></div>
			</div>
            </div>

			<div class="clear-all"></div>
		</div>

        <div class="clear-all"></div>

        <div class="content-default">

          <!--Start Main Contend-->
          <div id="dvContainer" runat="server"></div>
          <!--End Main Contend-->
		  
		  <!--Start FFP Login Menu-->
          <div id="dvClientInfo" style="display:none;"></div>
          <!--End FFP Login Menu-->
           <!-- Start Booking Summary -->
            <div id="dvYourSelection" class="YourSelection" style="display:none;">
		        <div class="rigntnavleft"></div>
                <div class="rightnavcontent">
	                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_Summary_1", "Your Selection", _stdLanguage)%>
			
                </div>
		        <div class="rightnavright"></div>
		
		        <div class="clear-all"></div>
		        <div class="gradient">
        
                <!--Display fare summary for left menu-->
                <div id="dvFareSummary" style="display:none;"></div>
                <div id="dvAvaiQuote"  style="display:none;">
                    <div id="dvFareLine_Outward"></div>
                    <div id="dvFareLine_Return"></div>
                </div>
        		
		        <div class="clear-all"></div>
		
		        </div>
		        <div class="bottombox">
			        <div class="homerightnavbottomleft"></div>
			        <div class="homerightnavbottom"></div>
			        <div class="homerightnavbottomright"></div>
		        </div>
				
				<div class="clear-all"></div>
			<!--  Start Total All Fare -->
			<div class="TotalAllFare">
				<div class="topbox">
					<div class="homerightnavtopleft"></div>
					<div class="homerightnavtop"></div>
					<div class="homerightnavtopright"></div>
				</div>
	
				<div class="gradient">
					<div class="airfare">
						<div class="airfareleftbox"></div>
						<div class="airfaremiddle"></div>
						<div class="airfarerightbox"></div>
						
						<div class="airfarecontent">
							<div class="totalfare fontB">
								<div class="totalflight">
									<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_Summary_2", "Total Price", _stdLanguage)%>
								</div>
								
								<div id="dvTotalFareSummary" class="charges_price_total">
									
								</div>
							</div>
						
						</div>
						<div class="clear-all"></div>
						<div class="airfareleftbottombox"></div>
						<div class="airfaremiddlebottom"></div>
						<div class="airfarerightbottombox"></div>
					
					</div>

				</div>
				<div class="clearboth"></div>
				<div class="bottombox">
					<div class="homerightnavbottomleft"></div>
					<div class="homerightnavbottom"></div>
					<div class="homerightnavbottomright"></div>
				</div>
			</div>
			
			<!--  End Total All Fare -->
            </div>
			
			
			
            <!-- End Booking Summary -->

          <!--Start Avai Section (Find at SearchAvailability.ascx)-->
          <div id="dvAvailabilitySearch" class="SearchBox">
            <AvailabilitySearch:AvailabilitySearch runat="server" ID="AvailabilitySearch1" />
            <div class="clear-all"></div>
          </div>
          <!--End Avai Section-->


          <div class="clear-all">
            <!--control bottom "WrapperBody"-->
          </div>
        </div>
        <!--End WrapperBody-->
		
		<div class="clear-all"></div>

        <div class="Footer">
			<div class="footerbg">
				<div class="copyright">
					<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_17", "Copyright &copy; 2011 Tassili Airlines Limited.", _stdLanguage)%>
				</div>
				
<%--				<div class="mercator">
					<div class="poweredby">Powered by</div>
					<div class="mercatorlogo">
						<img src="App_Themes/Default/Images/bravo.png" alt="bravo" />
					</div>
				</div>
--%>
		  	</div>
			
			<div class="clear-all"></div>
          
        </div>


        <div class="clear-all">
          <!--control bottom "Wrapper"-->
        </div>
      </div>
      <!--End Wrapper-->
        <!--START script for SearchAvailability calendar Only--> 
        <script language="javascript" type="text/javascript">

            var ResultType = "<%= ResultType %>";
            var CalendarToControls = "<%= CalendarToControls %>";
            var DateFormat = "<%= DateFormat %>";

            $(function () {
                $('#calendar').daterangepicker().click(function () {
                    $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
                });
                $('#calendar2').daterangepicker().click(function () {
                    $('.ui-daterangepicker:visible .ui-daterangepicker-dateRange').click();
                });
            });

        </script>
        <script type="text/javascript" src="Scripts/JQuery/jquery-ui-1.7.1.custom.min.js"></script>
        <script type="text/javascript" src="Scripts/JQuery/daterangepicker.jQuery.js"></script>
         <!--END script for SearchAvailability calendar Only-->

    </form>
    <script type="text/javascript">
        //**********************************************************
        //wireUpEvents();
        //Call function when every DOM is load
        $(document).ready(FormLoadOperation)
    </script>
    
  </body>
</html>
