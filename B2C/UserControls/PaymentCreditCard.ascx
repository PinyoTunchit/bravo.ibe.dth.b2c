<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentCreditCard.ascx.cs" Inherits="tikAeroB2C.UserControls.PaymentCreditCard" %>
<div class="xouter">
	<div class="xcontainer">
		<div class="xinner multiplepayment">
			<div class="seatboxtopleft"></div>
			<div class="multiboxtop"></div>
			<div class="seatboxtopright"></div>
			<div class="clear-all"></div>
				<div class="multipaymentgradient">
					<div id="dvMultiFormCCPayment">
						<div class="MultiplePaymentbyCreditCard">
						<div class="MethodBox">
							<div class="BoxtopleftCorner"></div>
							<div class="Boxtopmiddle">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_58", "Please select your method of payment:", _stdLanguage)%>
							</div>
							<div class="BoxtoprightCorner"></div>
						
							<div class="clear-all"></div>
						
							<div class="PaymentMethod">
								<div class="Paymentwhitetop">
									<div class="whiteboxtopleft"></div>
									<div class="whiteboxtopcontent"></div>
									<div class="whiteboxtopright"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="clear-all"></div>
						
						</div>
					
						<div class="clear-all"></div>
						
						
					
						<div id="graygradient">
							<div class="PassengerDetails paymentinnercontent">

				                <div class="displaycard">
                                    <img src="App_Themes/Default/Images/creditcard.png" alt="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_196", "We accept JCB and Diners Club", _stdLanguage)%>" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_196", "We accept JCB and Diners Club", _stdLanguage)%>">
                                </div>
							
								<div>
									<div class="ContactDetail">
										<label for="ctl00_optMultiCardType">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_7", "Credit Card Type", _stdLanguage)%>
										</label>
									
										<div class="InformationInput">
											<select id="optMultiCardType" onchange="ActivateMultiCCControl();" runat="server">
												<option value="" class="watermarkOn"></option>
											</select>
											<div class="mandatory"></div>
										</div>
									
										<div class="multicreditcardcharge">
										  <span><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_119", "A payment fee of", _stdLanguage)%> <span id="dvMultipleCCPercentage"></span> <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_120", "applies", _stdLanguage)%></span>
										</div>
									
										<div class="clear-all"></div>
									
									</div>
								
									<div class="ContactDetail">
										<label for="txtMultiCardNumber">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_10", "Credit Card Number", _stdLanguage)%>
										</label>
									
										<div class="InformationInput">
											<input id="txtMultiCardNumber" type="text" />
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>	  
								
									<div class="ContactDetail">
										<label for="txtMultiNameOnCard">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_6", "Name On Card", _stdLanguage)%>
										</label>
										
										<div class="InformationInput">
											<input id ="txtMultiNameOnCard" type="text" onkeypress="EmptyErrorMessage();"/>
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail" id="dvMultiIssueNumber">
										<label for="txtMultiIssueNumber">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_8", "Issue Number", _stdLanguage)%>
										</label>
										
										<div class="InformationInput">
											<input id="txtMultiIssueNumber" type="text" />
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail" id="dvMultiIssuDate">
										<label for="optMultiIssueMonth">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_9", "Issue Date(if present)", _stdLanguage)%>
										</label>
									
										<div class="InformationInput">
											<select id="optMultiIssueMonth" class="CardExpiryDate">
												<option value="" class="watermarkOn"></option>
												<option selected="selected" value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
											</select>
										
											<select id="optMultiIssueYear" class="CardExpiryDate">
												<option value="" class="watermarkOn"></option>
												<option selected="selected" value="2008">2008</option>
												<option value="2009">2009</option>
												<option value="2010">2010</option>
												<option value="2011">2011</option>
												<option value="2012">2012</option>
												<option value="2013">2013</option>
												<option value="2014">2014</option>
												<option value="2015">2015</option>
												<option value="2016">2016</option>
												<option value="2017">2017</option>
												<option value="2018">2018</option>
												<option value="2019">2019</option>
												<option value="2020">2020</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
											</select>
										
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
		
									<div class="ContactDetail" id="dvMultiExpiryDate">
										<label for="optMultiExpiredMonth">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_11", "Expiry Date", _stdLanguage)%>
										</label>
									
										<div class="InformationInput">
											<select id="optMultiExpiredMonth" class="CardExpiryDate" onchange="EmptyErrorMessage();">
												<option value="" class="watermarkOn"></option>
												<option selected="selected" value="01">1</option>
												<option value="02">2</option>
												<option value="03">3</option>
												<option value="04">4</option>
												<option value="05">5</option>
												<option value="06">6</option>
												<option value="07">7</option>
												<option value="08">8</option>
												<option value="09">9</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
											</select>
										
											<select id="optMultiExpiredYear" class="CardExpiryDate" onchange="EmptyErrorMessage();">
												<option value="" class="watermarkOn"></option>
											</select>
										
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail Left" id="dvMultiCvv">
										<label for="txtMultiCvv">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_12", "CVV", _stdLanguage)%>
										</label>
									
										<div class="InformationInput">
											<input id="txtMultiCvv" type="text" class="InputboxCVV" maxlength="4" onkeypress="EmptyErrorMessage();"/>
											<div class="mandatory">
												<div class="MultiDisplayCVV"></div>
											</div>
										</div>
									</div>
								
									<div class="clear-all"></div>
								
									<div class="SpecialInformation">
										<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_25", "Information on credit card security", _stdLanguage)%>
									</div>
								
									<div class="clear-all"></div>
								</div>
							</div>
						
							<div class="clear-all"></div>
						
							<div class="BottominnerBox">
								<div class="whiteboxbottomleft"></div>
								<div class="whiteboxbottomcontent"></div>
								<div class="whiteboxbottomright"></div>
							</div>
						
							<div class="clear-all"></div>
						
							<!-- Address Detail Section -->
							<div id="dvMultiAddress">
								<div class="PaymentDetail">
									<div class="whiteboxtopleft"></div>
									<div class="whiteboxtopcontent"></div>
									<div class="whiteboxtopright"></div>
								</div>
							
								<div class="clear-all"></div>
							
								<div class="PassengerDetails">
									<div class="ContactDetail">
										<label for="chkMultiMyname">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_5", "Use contact address", _stdLanguage)%>
										</label>
										
										<div class="checkbox">
											<input id="chkMultiMyname" type="checkbox" onclick="LoadMultiMyName();"/>
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail">
										<label for="txtMultiAddress1">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_16", "Address Line 1", _stdLanguage)%>
										</label>
										
										<div class="InformationInput">
											<input id="txtMultiAddress1" type="text" onkeypress="EmptyErrorMessage();"/>
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail">
										<label for="txtMultiAddress2">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_17", "Address Line 2 (Optional)", _stdLanguage)%>
										</label>
										
										<div class="InformationInput">
											<input id="txtMultiAddress2" type="text"/>
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail">
										<label for="txtMultiCounty">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_20", "Province/County", _stdLanguage)%>
										</label>
										
										<div class="InformationInput">
											<input id="txtMultiCounty" type="text"/>
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail">
										<label for="txtMultiCity">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_19", "Town/City", _stdLanguage)%>
										</label>
										
										<div class="InformationInput">
											<input id="txtMultiCity" type="text" onkeypress="EmptyErrorMessage();"/>
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail">
										<label for="txtMultiPostCode">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_22", "Postal Code", _stdLanguage)%>
										</label>
										
										<div class="InformationInput">
											<input id="txtMultiPostCode" type="text" onkeypress="EmptyErrorMessage();"/>
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								
									<div class="ContactDetail">
										<label for="ctl00_optMultiCountry">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_23", "Country", _stdLanguage)%>
										</label>
										
										<div class="InformationInput">
											<select id="optMultiCountry" runat="server" onchange="EmptyErrorMessage();">
												<option value="" class="watermarkOn"></option>
											</select>
											<div class="mandatory"></div>
										</div>
										<div class="clear-all"></div>
									</div>
								</div>
							
								<div class="clear-all"></div>
							
								<div class="BottominnerBox">
									<div class="whiteboxbottomleft"></div>
									<div class="whiteboxbottomcontent"></div>
									<div class="whiteboxbottomright"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="clear-all"></div>
						
						</div>
					
						<div class="clear-all"></div>
						<div class="BottomBox">
							<div class="BoxdownleftCorner"></div>
							<div class="BoxdownmiddleCorner"></div>
							<div class="BoxdownrightCorner"></div>
						</div>
					
						<div class="clear-all"></div>
					
						<div id="dvMultiCCButton" class="BTN-Search">
							<div class="ButtonAlignLeft">
								<a href="javascript:CloseDialog();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="backbutton">
									<span>
										<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
									</span>
								</a>
							</div>
						
							<div class="ButtonAlignRight">
								<a href="javascript:PaymentMultipleForm();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>" class="defaultbutton">
									<span>
										<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>
									</span>
								</a>
							</div>
						</div>
						</div>
						
						<div id="dvFormCCSummary" class="YourSelection" runat="server"></div>
					
						<div class="clear-all"></div>
					</div>
				</div>
				<div class="clear-all"></div>
				<div class="seatboxbottomleft"></div>
				<div class="multiboxbottom"></div>
				<div class="seatboxbottomright"></div>
				<div class="clear-all"></div>
		</div>
	</div>
</div>