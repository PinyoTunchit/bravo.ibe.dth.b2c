<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeatMap.ascx.cs" Inherits="tikAeroB2C.UserControls.SeatMap" %>
  <div class="xouter">
    <div class="xcontainer">
      <div class="xinner SeatSelection">

        <div class="seatboxtopleft"></div>
        <div class="seatboxtop"></div>
        <div class="seatboxtopright"></div>

        <div class="clear-all"></div>

        <div id="seatboxgradient">
          <div class="closepage">
            <a href="javascript:CloseDialog();" title="<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_39", "Close", _stdLanguage)%>">
              <div>
                <%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_39", "Close", _stdLanguage)%>
              </div>
              <img src="App_Themes/Default/Images/close.png" alt="<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_39", "Close", _stdLanguage)%>" title="<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_39", "Close", _stdLanguage)%>" />
			</a>
          </div>
          <div class="SeatMapSection">
            <div class="SeatAssignment">
              <div>
                <div class="seatmaptopleft"></div>
                <div class="seatmaptop"></div>
                <div class="seatmaptopright"></div>
              </div>

              <div class="clear-all"></div>

              <div class="SelectSeat">
                <div class="HeadAirPlane"></div>
                <span id="dvSeatMap" runat="server"></span>
                <div class="TailAirPlane"></div>
              </div>

              <div>
                <div class="seatmapbottomleft"></div>
                <div class="seatmapbottom"></div>
                <div class="seatmapbottomright"></div>
              </div>

            </div>
          </div>

          <div class="WrapperTBLSeat">

            <div id="dvSeatItinerary" runat="server"></div>

            <div id="dvSeatPassenger" runat="server"></div>
            
            <div class="bookinginform">

			
			<div class="whiteboxtopleft"></div>
			<div class="innerseatboxmiddle"></div>
			<div class="whiteboxtopright"></div>
			
			<div class="clear-all"></div>
			
			<div class="SeatLegend innerwhitebox">
				<div class="SeatLegendGroup">
					 <div class="SeatIcon">
					 	<img src="App_Themes/Default/Images/normalseat.png">
					 	<img src="App_Themes/Default/Images/normalseat_invert.png">
					</div>
	                <div class="SeatIconDesc">Available Seat</div>
				</div>
				
		        <div class="SeatLegendGroup">
	                <div class="SeatIcon">
						<img src="App_Themes/Default/Images/lavatory.png">
					</div>
	                <div class="SeatIconDesc">Lavatory</div>	
				</div>				
                
				<div class="SeatLegendGroup">
	                <div class="SeatIcon">
								<div class="wings"></div>
					</div>
	                <div class="SeatIconDesc">Wing</div>
				</div>				
    
				<div class="SeatLegendGroup">
	                <div class="SeatIcon">
						<img src="App_Themes/Default/Images/exit.png">
					</div>
	                <div class="SeatIconDesc">Exit</div>
				</div>
			</div>

                   
			
			
			<div class="clear-all"></div>
			
			<div class="BottominnerBox">
				<div class="whiteboxbottomleft"></div>
				<div class="innerseatboxmiddlebottom"></div>
				<div class="whiteboxbottomright"></div>
			</div>
			
			<div class="clear-all"></div>
	    </div>

            <div id="dvSeatButton" class="BTN-Seat">
              <div class="ButtonAlignLeft">
                <a href="javascript:CancelSelectSeat();" title="<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_17", "Back", _stdLanguage)%>" class="defaultbutton">
                  <span>
                    <%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_17", "Back", _stdLanguage)%>
                  </span>
                </a>
              </div>

              <div class="ButtonAlignRight">
                <a href="javascript:SaveSeatMap();" title="<%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_18", "Confirm", _stdLanguage)%>" class="defaultbutton">
                  <span>
                    <%=tikAeroB2C.Classes.Language.Value("Seat_Reservation_18", "Confirm", _stdLanguage)%>
                  </span>
                </a>
              </div>
              <div class="clear-all"></div>
            </div>
          </div>

          <div class="clear-all"></div>

        </div>

        <div class="clear-all"></div>

        <div class="seatboxbottomleft"></div>
        <div class="seatboxbottom"></div>
        <div class="seatboxbottomright"></div>

        <div class="clear-all"></div>

      </div>

    </div>
  </div>


