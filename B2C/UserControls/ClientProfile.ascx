<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientProfile.ascx.cs" Inherits="tikAeroB2C.UserControls.ClientProfile" %>

<div class="xouter">
	<div class="xcontainer">
		<div class="xinner TBLPopupWrapper">
			<div class="passengerlisttop"></div>
			<div class="passengerlistcontent">
			
				<div class="PopupPassengerList">
					<div class="BoxtopleftCorner"></div>
					<div class="Boxtopmiddle">
				        <%=tikAeroB2C.Classes.Language.Value("Passenger_List_1", "Passenger List", _stdLanguage)%>
						
						<div class="TotalPax">
				            <div id="dvPaxSelecter" class="SelectedPassenger">0</div>
							<div class="AvailablePassenger">/</div>
				            <div id="dvTotalPax" class="AvailablePassenger">0</div>
						</div>
				    </div>
					<div class="BoxtoprightCorner"></div>
					<div class="clear-all"></div>
					
					<div id="graygradient">
						<div id="dvPassengerList" runat="server"></div>
					
					</div>
					
					<div class="clear-all"></div>
					
					<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				<div class="clear-all"></div>
				</div>
			
				<div class="clear-all"></div>
	
				<div class="BTN-Search">
					<div class="ButtonAlignLeft">
						<a href="javascript:GetSelectedClientPosition();" class="defaultbutton" title="<%=tikAeroB2C.Classes.Language.Value("Passenger_List_7", "OK", _stdLanguage)%>">
							<span><%=tikAeroB2C.Classes.Language.Value("Passenger_List_7", "OK", _stdLanguage)%></span>
						</a>
					</div>
				    
				</div>
				
				<div class="clear-all"></div>
			
			</div>	
			
			<div class="passengerlistbottom"></div>
					
		    <!-- <div class="ButtonMultiRight" onclick="ClosePassengerList();">
		        <div class="BTN-Left"></div>
		        <div class="BTN-Middle">
		            <%=tikAeroB2C.Classes.Language.Value("Passenger_List_8", "CANCLE", _stdLanguage)%>
		        </div>
		        <div class="BTN-Right"></div>
		    </div> -->

			<div class="clear-all"></div>
		</div>
	</div>
</div>