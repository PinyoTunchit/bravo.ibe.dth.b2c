<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FlightSummary.ascx.cs" Inherits="tikAeroB2C.UserControls.FlightSummary" %>
<div class="CoverDetailPopup">
    <div class="buttonStepContent">
        <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Step 3", _stdLanguage)%>
    </div>
    <div class="clearboth"></div>
    <div class="WrapperTBLStep3">
        <div class="TabDesc">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_2", "Your Itinerary", _stdLanguage)%>
        </div>
        <div class="clearboth"></div>
        <!--Flight information-->
        <div id="dvFlightInfo" runat="server"/>
        <div class="TabDesc">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_9", "Quote", _stdLanguage)%>
        </div>
        <div class="clearboth"></div>
        <!--Quote information-->
        <div id="dvQuoteInfo" runat="server"/>
        <div class="ButtonAlignLeft" onclick="BackToAvailability();">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_18", "Back", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
        </div>
        <div class="ButtonAlignMiddleStep3" onclick="loadHome();">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_19", "Cancel", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
        </div>
        <div class="ButtonAlignRight" onclick="GetPassengerDetail();">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_20", "Next", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
        </div>
    </div>
    <div class="RulesBox">
        <div id="dvRuleInfo" runat="server"/>
    </div>
    <div class="clearboth"></div>
</div>