<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PassengerDetail.ascx.cs" Inherits="tikAeroB2C.UserControls.PassengerDetail" %>

	<div class="stepbar2">
	<ul>
		<li class="step1">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Flight selection", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step2">
			<div class="stepactive">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Personal information", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step3">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Purchase", _stdLanguage)%>
            </div>
		</li>
	</ul>

</div>

<div class="CoverDetailPopup">

    <div class="PassengerMainContact">
      <div class="MainContactDetails">
        <input id="txtClientProfileId" type="hidden" runat="server"/>
        <input id="txtClientNumber" type="hidden" runat="server"/>

        <input id="txtCompanyName" type="text" runat="server" style="display:none;"/>
        <input id="txtVat" type="text" runat="server" style="display:none;"/>
		<div class="BoxtopleftCorner"></div>
        <div class="Boxtopmiddle">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_3", "Main Contact Details (details in English only)", _stdLanguage)%>
        </div>
		<div class="BoxtoprightCorner"></div>
		<div class="clear-all"></div>
		
		<div id="graygradient">
		
			<div class="whitebox">
				<div class="whiteboxtopleft"></div>
				<div class="whiteboxtopcontent"></div>
				<div class="whiteboxtopright"></div>
				<div class="clear-all"></div>
				<div class="innerwhitebox">
					<div class="Topinformation">
						<div class="ContactDetail">
				            <label for="ctl00_stContactTitle" class="toptitle">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_4", "Title", _stdLanguage)%>
							</label>
							
							<div class="clear-all"></div>
							
							<div class="InformationInput">
								<select id="stContactTitle" class="Selectbox" runat="server">
                                    <option value="" class="watermarkOn"></option>
                                </select>
								<div class="mandatory"></div>
							</div>
							 <div class="clear-all"></div>
						</div>

                       	<div class="ContactDetail">
							<label for="ctl00_txtContactFirstname" class="topname">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_5", "First Name", _stdLanguage)%>
							</label>
							
							<div class="clear-all"></div>
				        
					        <div class="InformationInput">
					          <input id="txtContactFirstname" type="text" runat="server" maxlength="60"/>
					          <div id="spErrFirstname" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
					
                        <div class="ContactDetail">
							<label for="ctl00_txtContactLastname" class="topname">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_6", "Surname", _stdLanguage)%>
							</label>	
							
							<div class="clear-all"></div>
				        
					        <div class="InformationInput">
					          <input id="txtContactLastname" type="text" runat="server" maxlength="60" />
					          <div id="spErrLastname" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
					
						<div class="clear-all"></div>
						
					</div>
					
					<div class="leftdetail">
						 <div class="ContactDetail">
							<label for="ctl00_txtContactEmail">
				            	<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_10", "Email", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtContactEmail" type="text" runat="server" maxlength="60" onfocus="bindEvent(this);" />
					          <div id="spErrEmail" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						<div class="ContactDetail">
							<label for="ctl00_txtEmailConfirm">
				            	<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_93", "Email Confirmation", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtEmailConfirm" type="text" runat="server" maxlength="60" onfocus="bindEvent(this);" />
					          <div id="spErrEmailConfirm" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						<div class="ContactDetail">
							<label for="ctl00_txtMobileEmail">
				            	<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_73", "Mobile Email", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtMobileEmail" type="text" runat="server" maxlength="60" />
					          <div id="spMobileEmail" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="ctl00_txtContactHome">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_8", "Home No", _stdLanguage)%>
				        	</label>
					        <div class="InformationInput">
					          <input id="txtContactHome" type="text" runat="server" maxlength="60" />
					          <div id="spErrHome" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
				        <div class="ContactDetail">
							<label for="ctl00_txtContactMobile">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_7", "Mobile No", _stdLanguage)%>
				        	</label>
							
					        <div class="InformationInput">
					          <input id="txtContactMobile" type="text" runat="server" maxlength="60" />
					          <div id="spErrMobile" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="optional">
							<div class="Remember FirstLine">
								<input id="chkCopy" type="checkbox" value="" onclick="CopyContactToPassenger();"/>
								<label for="chkCopy" >
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_18", "I'm a passenger on the flight.", _stdLanguage)%>
								</label>
							</div>
						
							<div class="clear-all"></div>
							
							<div class="Remember">
								<input id="chkRemember" name="Input" type="checkbox" value="" />
								<label for="chkRemember" >
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_17", "Remember these details for next visit.", _stdLanguage)%>
								</label>
								
								<div class="cookietooltip">
	
									<div class="fg-tooltip ui-widget ui-widget-tooltip ui-corner-all" style="display: none;">

										<ul class="tooltipcontent">
											<li>&#149;
                                               <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_74", "By creating a member profile on Tassili:", _stdLanguage)%>
                                            </li>
											<li class="nodisplaylist">
                                                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_75", "Making new bookings is simple!", _stdLanguage)%>
                                            </li>
											<li class="nodisplaylist">
                                                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_76", "You can manage and list all your bookings", _stdLanguage)%>
                                            </li>
											
										
										</ul>
										
										 <div class="fg-tooltip-pointer-down ui-widget-tooltip">
											<div class="fg-tooltip-pointer-down-inner"></div>
										 </div>
									</div>
						
									<p class="demoText"><a href="#" class="tooltip"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_61", "more", _stdLanguage)%></a></p>
						
								</div>
							
							</div>

							
							
							
						</div>
						<div class="clear-all"></div>
						
					</div>
					
					<div class="leftdetail Last">
						 <div class="ContactDetail">
					        <label for="ctl00_txtContactAddress1">
						        <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_11", "Address 1", _stdLanguage)%>
					        </label>
		        
			                <div class="InformationInput">
			                  <input id="txtContactAddress1" type="text" runat="server" maxlength="60" />
			                  <div id="spErrAddress1" class="mandatory"></div>
			                </div>
					        <div class="clear-all"></div>
				        </div>
						
						<div class="ContactDetail">
							<label for="ctl00_txtContactCity">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_13", "Town/City", _stdLanguage)%>
					        </label>
						
					        <div class="InformationInput">
					          <input id="txtContactCity" type="text" runat="server" maxlength="60" />
					          <div id="spErrCity" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="ctl00_txtContactState">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_14", "State", _stdLanguage)%>
							</label>
							
					        <div class="InformationInput">
					          <input id="txtContactState" type="text" runat="server" maxlength="60" />
					          <div id="spErrState" class="mandatory"></div>
					        </div> 
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="ctl00_txtContactZip">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_15", "Zip Code", _stdLanguage)%>
							</label>
					   
					        <div class="InformationInput">
					          <input id="txtContactZip" type="text" runat="server" maxlength="60" />
					          <div id="spErrZip" class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
				        	<label for="ctl00_stContactCountry">
				            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_16", "Country", _stdLanguage)%>
				           </label>
				       
				        	<div class="InformationInput">
					          	<select id="stContactCountry" class="Selectbox" runat="server">
                                    <option value="" class="watermarkOn"></option>
                                </select>
					          	<div class="mandatory"></div>
				        	</div>
							<div class="clear-all"></div>
				         </div>
	
					</div>
					<div class="clear-all"></div>
				</div>				
				
				<div class="BottominnerBox">
					<div class="whiteboxbottomleft"></div>
					<div class="whiteboxbottomcontent"></div>
					<div class="whiteboxbottomright"></div>
				</div>
			</div>
            <div class="none">
                <input id="txtContactBusiness" type="text" runat="server" maxlength="60" />
                <input id="txtContactAddress2" type="text" runat="server" maxlength="60" />
                <select id="stContactLanguage" class="Selectbox" name="select2" runat="server">
                    <option value="" class="watermarkOn"></option>
                </select>
			</div>
			<div class="clear-all"></div>
		</div>
		
		<div class="clear-all"></div>
		
		<div class="BottomBox">
			<div class="BoxdownleftCorner"></div>
			<div class="BoxdownmiddleCorner"></div>
			<div class="BoxdownrightCorner"></div>
		</div>
		<div class="clear-all"></div>
      </div>
	  
	  <div class="clear-all"></div>
	  
	  <div class="PassengerDetail">
		<div class="BoxtopleftCorner"></div>
	    <div class="Boxtopmiddle">
	        <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_30", "Passenger", _stdLanguage)%>
	    </div>
		<div class="BoxtoprightCorner"></div>
		
	    <!--Passenger Input-->
		<div id="graygradient">
		
		    <div id="dvPassengerInput" runat="server"></div>
			
			<div class="PaxBG">
				
				<div class="SelectSeatButton">
					<div id="dvFlightInformation" runat="server"></div>
				</div>
					
				<div class="clear-all"></div>

			</div>
			
			<div class="paxdetailbottom">
				<div class="paxleftbottom"></div>
				<div class="paxcontent"></div>
				<div class="paxrightbottom"></div>
			</div>

			<div id="dvPaxError" class="mandatory"></div>
		
			<div class="clear-all"></div>
		</div>
		<div class="BottomBox">
			<div class="BoxdownleftCorner"></div>
			<div class="BoxdownmiddleCorner"></div>
			<div class="BoxdownrightCorner"></div>
		</div>
		<div class="clear-all"></div>
	</div>
	
	<div class="clear-all"></div>
	
	<div class="BTN-Search">

		<div class="ButtonAlignLeft">
			<a href="javascript:BackToAvailability();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_55", "Back", _stdLanguage)%>" class="defaultbutton">
				<span>
					<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_55", "Back", _stdLanguage)%>
				</span>
			</a>
		</div>

		<div class="ButtonAlignRight">
			<a href="javascript:SavePassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_57", "Next", _stdLanguage)%>" class="defaultbutton">
				<span>
					<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_57", "Next", _stdLanguage)%>
				</span>
			</a>
		</div>
		
		
		
	</div>

    </div>

	<div class="Remarks_Airline_Itinerary boxborder none">
		<div class="boxheader">Remarks for Airline</div>
		<textarea id="txaAirlineRemark" runat="server"></textarea>
	</div>
	
	<div class="Remarks_Airline_Itinerary boxborder none">
		<div class="boxheader">Remarks for Itinerary</div>
		<textarea id="txaItineraryRemark" runat="server"></textarea>
	</div>
	
	
</div>
