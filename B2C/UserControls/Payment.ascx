<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Payment.ascx.cs" Inherits="tikAeroB2C.UserControls.Payment" %>
<!--button-->
	<div class="stepbar3">
	<ul>
		<li class="step1">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Flight selection", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step2">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Personal information", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step3">
			<div class="stepactive">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Purchase", _stdLanguage)%>
            </div>
		</li>
	</ul>

</div>
	
	

	<div class="Payment" id="dvPayment">
		<div class="clear-all"></div>
		
		<div class="MethodBox">
			<div class="BoxtopleftCorner"></div>
			<div class="Boxtopmiddle">
				<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_58", "Please select your method of payment:", _stdLanguage)%>
			</div>
			<div class="BoxtoprightCorner"></div>
			
			<div class="clear-all"></div>
	
			<div class="PaymentMethod">
				<div class="Paymentwhitetop">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>
				</div>
				<div class="clear-all"></div>
				<div id="dvPaymentTab" runat="server"></div>
			</div>
			<div class="clear-all"></div>
		</div>
		
		<div class="PaymentMethodDetail">
		
			
			<!-- Credit Card details -->
			<div style="display:block">
			<div class="TabCreditCard" id="CreditCard">
				<div id="graygradient">
					<div class="PassengerDetails paymentinnercontent" >

                    <div style="display:block">
					<div class="PaymentContentDetail">
						<div class="paymentcontent">
							<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_140", "You've almost finished booking your flights! We recommend that you double check your information provided is accurate and the total amount due before you select the mode of payment that best suits you.", _stdLanguage)%>
						</div>
						
						<div class="paymentcontent">
                            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_141", "Once you select your payment method, we will calculate any additional fees that are due and automatically update the total displayed at the bottom of this page.", _stdLanguage)%>
						</div>
						
						<div class="paymentcontent">
							<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_142", "By Selecting to pay by credit card you have the added benefit of having an instantly confirmed booking.", _stdLanguage)%> 
						</div>
						
						<div class="paymentcontent">
							<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_143", "Once you have confirmed your bookings details and click 'Purchase' we will begin processing the payment.", _stdLanguage)%> 
						</div>
					
					</div>
					
					
					<div id="dvCardTypeLogoNonJPY" class="displaycard">
						<img src="App_Themes/Default/Images/creditcardtype_no_jpy.jpg" alt="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_192", "We accept Mastercard and Visa", _stdLanguage)%>" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_192", "We accept Mastercard and Visa", _stdLanguage)%>">
					</div>
				    <div id="dvCardTypeLogoJPY"  class="displaycard">
                        <img src="App_Themes/Default/Images/creditcard.png" alt="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_196", "We accept JCB and Diners Club", _stdLanguage)%>" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_196", "We accept JCB and Diners Club", _stdLanguage)%>">
                    </div>
					<div>
							<div class="ContactDetail">
								<label for="ctl00_optCardType">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_7", "Credit Card Type", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <select id="optCardType" onchange="activateCreditCardControl();" runat="server">
                                    <option value="" class="watermarkOn"></option>
                                  </select>
						          <div class="mandatory"></div>
						        </div>
								
								<div class="creditcardcharge">
	                                <span><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_119", "A payment fee of", _stdLanguage)%> <span id="dvCCPercentage"></span> <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_120", "applies", _stdLanguage)%></span>
								</div>
								
								<div class="clear-all"></div>
								
							</div>
							
							<div class="ContactDetail">
								<label for="txtCardNumber">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_10", "Credit Card Number", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <input id="txtCardNumber" type="text" />
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="txtNameOnCard">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_6", "Name On Card", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <input id ="txtNameOnCard" type="text" onkeypress="EmptyErrorMessage();"/>
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail" id="dvIssueNumber">
								<label for="txtIssueNumber">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_8", "Issue Number", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
									<input id="txtIssueNumber" type="text" />
									<div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail" id="dvIssuDate">
								<label for="optIssueMonth">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_9", "Issue Date(if present)", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
									<select id="optIssueMonth" class="CardExpiryDate">
                                        <option value="" class="watermarkOn"></option>
										<option selected="selected" value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									
									<select id="optIssueYear" class="CardExpiryDate">
                                        <option value="" class="watermarkOn"></option>
										<option selected="selected" value="2008">2008</option>
										<option value="2009">2009</option>
										<option value="2010">2010</option>
										<option value="2011">2011</option>
										<option value="2012">2012</option>
										<option value="2013">2013</option>
										<option value="2014">2014</option>
										<option value="2015">2015</option>
										<option value="2016">2016</option>
										<option value="2017">2017</option>
										<option value="2018">2018</option>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
									</select>
	
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							
							<div class="ContactDetail" id="dvExpiryDate">
								<label for="optExpiredMonth">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_11", "Expiry Date", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
									<select id="optExpiredMonth" class="CardExpiryDate" onchange="EmptyErrorMessage();">
                                        <option value="" class="watermarkOn"></option>
										<option selected="selected" value="01">1</option>
										<option value="02">2</option>
										<option value="03">3</option>
										<option value="04">4</option>
										<option value="05">5</option>
										<option value="06">6</option>
										<option value="07">7</option>
										<option value="08">8</option>
										<option value="09">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									
									<select id="optExpiredYear" class="CardExpiryDate" onchange="EmptyErrorMessage();">
                                        <option value="" class="watermarkOn"></option>
			                        </select>
	
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail Left" id="dvCvv">
								<label for="txtCvv">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_12", "CVV", _stdLanguage)%>
								</label>
			
								<div class="InformationInput">
									<input id="txtCvv" type="text" class="InputboxCVV" maxlength="4" onkeypress="EmptyErrorMessage();"/>
									<div class="mandatory">
										<div class="DisplayCVV"></div>
									</div>
								</div>

							</div>

							<div class="clear-all"></div>
							
							
							<div class="SpecialInformation">
							<div class="SpecialInformationtooltip">
	
									<div class="fg-tooltip ui-widget ui-widget-tooltip ui-corner-all" style="display: none;">

										<div class="tooltipcontent">
											<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_15", "The Security Code is the last 3 digits. Of the number on the signature strip on the reverse of your card.", _stdLanguage)%>
										</div>
										
										 <div class="fg-tooltip-pointer-down ui-widget-tooltip">
											<div class="fg-tooltip-pointer-down-inner"></div>
										 </div>
									</div>
								</div>
								</div>
						
							
	
						
						<div class="clear-all"></div>
					</div>

                    </div>

					</div>

					
                    <div class="clear-all"></div>
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
					
					<div class="clear-all"></div>
		            

					<!-- Address Detail Section -->
					<div id="dvAddress">
                    
						<div class="PaymentDetail">
							<div class="whiteboxtopleft"></div>
							<div class="whiteboxtopcontent"></div>
							<div class="whiteboxtopright"></div>
						</div>
                     
						
						<div class="clear-all"></div>
						
                        <div style="display:block">
						<div class="PassengerDetails">
							<div class="ContactDetail">
								<label for="chkMyname">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_5", "Use contact address", _stdLanguage)%>
								</label>
					        
						        <div class="checkbox">
						          <input id="chkMyname" type="checkbox" onclick="LoadMyName();"/>
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="txtAddress1">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_16", "Address Line 1", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <input id="txtAddress1" type="text" onkeypress="EmptyErrorMessage();"/>
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="txtAddress2">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_17", "Address Line 2 (Optional)", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <input id="txtAddress2" type="text"/>
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="txtCounty">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_20", "Province/County", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <input id="txtCounty" type="text"/>
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="txtCity">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_19", "Town/City", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
	                                 <input id="txtCity" type="text" onkeypress="EmptyErrorMessage();"/>
						          
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
			
							<div class="ContactDetail">
								<label for="txtPostCode">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_22", "Postal Code", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
						          <input id="txtPostCode" type="text" onkeypress="EmptyErrorMessage();"/>
						          <div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="ctl00_optCountry">
						            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_23", "Country", _stdLanguage)%>
								</label>
					        
						        <div class="InformationInput">
									<select id="optCountry" runat="server" onchange="EmptyErrorMessage();">
                                        <option value="" class="watermarkOn"></option>
									</select>
									<div class="mandatory"></div>
						        </div>
								<div class="clear-all"></div>
							</div>
						</div>
						</div>

						<div class="clear-all"></div>
						
						<div class="BottominnerBox">
							<div class="whiteboxbottomleft"></div>
							<div class="whiteboxbottomcontent"></div>
							<div class="whiteboxbottomright"></div>
						</div>
						<div class="clear-all"></div>
					</div>

					<div class="clear-all"></div>
				</div>
				<div class="clear-all"></div>
				<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BTN-Search">

                   <div style="display:block">
						<div class="ButtonAlignLeft">
							<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
								<span>
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
								</span>
							</a>
						</div>
						
						<div class="PurchaseConfirm">
							<div class="ConfirmCheckbox">
								
								<input id="chkPayCreditCard" type="checkbox" />
								
								<label for="chkPayCreditCard" class="checking">
                                <a href="javascript:CreateWnd('http://www.tassiliairlines.dz/agreement.html', 820, 500, false, scrollbars=1); ">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_180", "by checking the Checkbox before selecting Purchase.", _stdLanguage)%>
                                </a>
								</label>
								
							</div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ButtonAlignRight"> 
							 <a href="javascript:paymentCreditCard();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>" class="defaultbutton">
								<span>                    
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>
								</span>
							</a>
						</div>
				  </div>	
	             </div>
				<div class="clear-all"></div>
			</div>
			</div>
			<!-- END Credit Card details -->

			<div class="clear-all"></div>
	
		  <span class="asterisk">
		    <span id="spnError"></span>
		  </span>
			<div class="clear-all"></div>
			
			<!-- Voucher -->
			<div class="TabVoucher" id="Voucher">
				<div id="graygradient">	
					<div class="PassengerDetails paymentinnercontent">
						<div class="PaymentContentDetail">
							<div class="paymentcontent">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_181", "You've almost finished booking your flights! We recommend that you double check your information provided is accurate and the total amount due before you select the mode of payment that best suits you.Once you select your payment method, we will calculate any additional fees that are due and automatically update the total displayed at the bottom of this page.", _stdLanguage)%>
							</div>
						
							<div class="paymentcontent">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_182", "Vouchers issued by Tassili can be used for full or part payment of your fare and add-ons offered. No booking and service fee applies.", _stdLanguage)%>
							</div>

							<div class="paymentcontent">
								<div class="fontB">
                                    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_183", "To redeem a voucher:", _stdLanguage)%>
                                </div>
								<ul>
									<li><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_184", "1) Enter the voucher number and pin number below and then click on the 'search' button", _stdLanguage)%></li>
									<li><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_185", "2) If the amount of the voucher is not sufficient for payment of the booking, please contact Tassili Airlines to use this voucher as a partial payment. You will not be able to finish the booking using a voucher which does not fully cover the costs of booking.", _stdLanguage)%></li>
								</ul>
							</div>
						
							<div class="paymentcontent">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_186", "Vouchers are subject to the terms and conditions appearing on them.", _stdLanguage)%>
							</div>
						
							<div class="paymentcontent">
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_147", "Once you have confirmed your bookings details and click 'Purchase' we will begin processing the payment.", _stdLanguage)%>
							</div>

						</div>
					
						<div class="ContactDetail fontB FirstLine">
							<label for="txtVoucherNumber">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_27", "Voucher Number", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtVoucherNumber" type="text" maxlength="10"/>
					          <div class="mandatory"></div>
					        </div>
							<div class="clear-all"></div>
						</div>
	
						<div class="ContactDetail fontB">
							<label for="txtVoucherPassword">
					            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_28", "Password", _stdLanguage)%>
							</label>
				        
					        <div class="InformationInput">
					          <input id="txtVoucherPassword" type="password" />
					          <div class="mandatory"></div>
					        </div>
							<div class="validatevoucherbutton">
								<a href="javascript:validateVoucher(document.getElementById('txtVoucherNumber').value,document.getElementById('txtVoucherPassword').value);" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_29", "Validate Voucher", _stdLanguage)%>" class="defaultbutton">
									<span>
										<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_29", "Validate Voucher", _stdLanguage)%>
									</span>
								</a>
								<div class="clear-all"></div>
							</div>
							<div class="clear-all"></div>
						</div>
		
						
						<div class="clear-all"></div>
					</div>
					<div class="clear-all"></div>
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
				
					<div class="clear-all"></div>

					<span class="asterisk">
						<span id="spnVoucherError"></span>
					</span>

					<div class="clear-all"></div>
				
				
				
					<div id="dvVoucherDetail"></div>
					<div class="clear-all"></div>
			        <div id="dvMultipleConfirm">
                        <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_121", "Your Voucher amount is not enough. Please click process to pay addition payment with credit card.", _stdLanguage)%>
                    </div>
					<div class="clear-all"></div>
				</div>
				
				<div class="clear-all"></div>
					<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BTN-Search">
					<div class="ButtonAlignLeft">
						<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
							</span>
						</a>
					</div>
					
					<div class="PurchaseConfirm">
							<div class="ConfirmCheckbox">
								
								<input id="chkPayVoucher" type="checkbox" />
								
								<label for="chkPayVoucher" class="checking">
                                <a href="javascript:CreateWnd('http://www.tassiliairlines.dz/agreement.html', 820, 500, false, scrollbars=1);">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_180", "by checking the Checkbox before selecting Purchase.", _stdLanguage)%>
                                    </a>
								</label>

							</div>
							<div class="clear-all"></div>
					</div>

					<div id="dvPayVoucher" class="ButtonAlignRight">
						<a href="javascript:paymentVoucher();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_55", "Pay Now", _stdLanguage)%>
							</span>
						</a>
					</div>

				</div>
			</div>
            <!-- END Voucher -->
			<div class="clear-all"></div>		
		  	
			<!-- Book now -->
			<div class="TabCrAccount" id="BookNow">
				<div id="graygradient">
					<div class="PassengerDetails paymentinnercontent">
                        <div class="PaymentContentDetail">
						<div class="clear-all"></div>

                        <div class="paymentcontent">
                          <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_144", "You've almost finished booking your flights. Please check that all of your details are correct before proceeding to payment.", _stdLanguage)%>                  
                        </div>

                        <div class="paymentcontent">
                          <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_145", "If you chose to pay via �Book Now Pay Later� option, you have to contact Tassili Airlines to pay the outstanding balance.", _stdLanguage)%>                        
                        </div>
                        <div class="paymentcontent">
                           <span>
                              <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_146", "Reservations will be held for ", _stdLanguage)%>                        
                           </span>
                           <span id="booknowTimelimit" />
                        </div>

                        <div class="paymentcontent">
                          <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_147", "You will not be able to travel until the outstanding balance is paid.", _stdLanguage)%>                        
                        </div>
                        </div>
					</div>
					
					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>

                    <div class="clear-all"></div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BTN-Search">
					<div class="ButtonAlignLeft">
						<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
							</span>
						</a>
					</div>
					
					<div class="PurchaseConfirm">
							<div class="ConfirmCheckbox">
							
								<input id="chkPayPostPaid" type="checkbox" />
								 
								<label for="chkPayPostPaid" class="checking">
                                <a href="javascript:CreateWnd('http://www.tassiliairlines.dz/agreement.html', 820, 500, false, scrollbars=1);">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_180", "by checking the Checkbox before selecting Purchase.", _stdLanguage)%>
                                     </a>
								</label>
                               
							</div>
							<div class="clear-all"></div>
					</div>

					<div class="ButtonAlignRight">
						<a href="javascript:SaveBookedNowPayLater();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>
							</span>
						</a>
					</div>
					
					
					
				</div>
			</div>
		    <!-- END Book now -->

            <!-- EFT -->
			<div class="TabCrAccount" id="ELV" style="display:none">
				<div id="graygradient">
					<div class="PassengerDetails paymentinnercontent">
                    <div class="red">
                       <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_198", "This payment method is unavailable", _stdLanguage)%>                    
                    </div>
					</div>
					
					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>

                    <div class="clear-all"></div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BTN-Search">
					<div class="ButtonAlignLeft">
					
					</div>
					
					<div class="PurchaseConfirm">
							<div class="ConfirmCheckbox">
							
								

							</div>
							<div class="clear-all"></div>
					</div>

					<div class="ButtonAlignRight">
						
					</div>
					
					
					
				</div>
			</div>
		    <!-- END EFT -->

            <!-- Satim -->
			<div class="TabCrAccount" id="STM">
				<div id="graygradient">
					<div class="PassengerDetails paymentinnercontent">
						<div class="PaymentContentDetail">
						
						<div class="paymentcontent">
                            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_200", "Satim!", _stdLanguage)%>
						</div>					
						<div class="clear-all"></div>
					</div>
						<div class="clear-all"></div>
					</div>
					
					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>

                    <div class="clear-all"></div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				
				<div class="clear-all"></div>
                <div class="BTN-Search">
					<div class="ButtonAlignLeft">
						<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
							</span>
						</a>
					</div>
					
					<div class="PurchaseConfirm">
							<div class="ConfirmCheckbox">
							
								<input id="chkPaySatim" type="checkbox" />
								 
								<label for="chkPayPostPaid" class="checking">
                                <a href="javascript:CreateWnd('http://www.tassiliairlines.dz/agreement.html', 820, 500, false, scrollbars=1);">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_180", "by checking the Checkbox before selecting Purchase.", _stdLanguage)%>
                                     </a>
								</label>
                               
							</div>
							<div class="clear-all"></div>
					</div>

					<div class="ButtonAlignRight">
						<a href="javascript:SaveSatim();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>
							</span>
						</a>
					</div>					
				</div>
			</div>
		
		    <div class="BTN-Search" id="dvPaymentBack" style="display:none;">
				<div class="ButtonAlignLeft">
					<a href="javascript:GetPassengerDetail();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>" class="defaultbutton">
						<span>
							<%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Step Back", _stdLanguage)%>
						</span>
					</a>
				</div>
		    </div>
            <div class="clear-all"></div>
				
		</div>
		<div class="clear-all"></div>
	</div>
    
	
    <div class="TabCostSummary" id="dvFfpRedeemSummary" style="display:none;">
        <div class="TabCostSummaryHeader"></div>
        <div class="TextCostSummary">
         <div class="TextCostSummaryLeft" id="dvRedeemClientId">00000000</div>
        <div class="TextCostSummaryRight"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_70", "Points", _stdLanguage)%></div>
        </div>
	    <div class="clear-all"></div>
	    <div class="TabCostPointsLeft"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_71", "Current Points", _stdLanguage)%></div>
	    <div class="TabCostPointsRight" id="dvRedeemCurrentPoint">30</div>
	    <div class="TabCostPointsLeft"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_72", "Redeem Points", _stdLanguage)%></div>
	    <div class="TabCostPointsRight" id="dvRedeemPoint">24</div>
	    <div class="TabCostPointsLeft"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_73", "Balance Points", _stdLanguage)%></div>
	    <div class="TabCostPointsRight" id="dvRedeemBalance">6</div>
	    <div class="TabCostRemark "><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_74", "*Points will be deducted when tickets are issued.", _stdLanguage)%></div>
         <div class="TabCostSummaryFooter"></div>
    </div>
    
    <div class="TabCostSummary" id="dvAccualSummary"></div>

    <!-- EFT Control-->
    <div class="TabELV" id="dvEft" style="display:none;">
        <div class="TextELV">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_75", "ELV", _stdLanguage)%>
        </div>
    
        <div class="LeftDesc">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_76", "Bank Name", _stdLanguage)%> 
        </div>
        <div class="RightDesc">
            <input id="txtElvBankName" class="Inputbox" type="text" />
            <span class="RequestStar">*</span>
        </div>
	    <div class="clearboth"></div>
    
        <div class="LeftDesc">
            <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_77", "Bank Code", _stdLanguage)%>
        </div>
        <div class="RightDesc">
            <input id="txtElvBankCode" class="Inputbox" type="text" />
            <span class="RequestStar">*</span>
        </div>
	    <div class="clearboth"></div>
    
        <div class="LeftDesc"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_78", "Account Number", _stdLanguage)%></div>
        <div class="RightDesc">
            <input id="txtElvAccountNumber" class="Inputbox" type="text" />
            <span class="RequestStar">*</span>
        </div>
	    <div class="clearboth"></div>
    
        <div class="LeftDesc"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_79", "Account Holder", _stdLanguage)%></div>
        <div class="RightDesc">
            <input id="txtElvAccountHolder" class="Inputbox" type="text" />
            <span class="RequestStar">*</span>
        </div>
	    <div class="clearboth"></div>
    
        <div class="ELVerror"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_80", "Please input Firstname and Lastname with space in between.", _stdLanguage)%></div>
	    <div class="clearboth"></div>
    
        <div class="LeftDesc">Country</div>
        <div class="RightDesc">
            <select id="optEftCountry" class="Selectbox" runat="server">
                <option value="" class="watermarkOn"></option>
            </select>
            <span class="asterisk">*</span>
        </div>
	    <div class="space15"></div>
    
        <div class="clearboth"></div>
        <div class="Confirm">
     	    <input id="chkElvCondition" type="checkbox" />
     	    <a href="#" target="_blank">
        	    <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_81", "I have read, understood and accept all Terms and Conditions associated with this Reservation.", _stdLanguage)%>
     	    </a>
            <div class="clearboth"></div>
            <span class="asterisk">
    	        <span id="spnEftError"></span>
  	        </span>
        </div>
        <div id="dvElvButton" class="BTNPayment" name="dvELV">
          <!--button-->
          <div class="ButtonAlignRight">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent" onclick="paymentEft();">
                 <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_56", "Proceed", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
          </div>
          <!-- end button-->
          <!--button-->
          <div class="ButtonAlignLeft">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent" onclick="GetPassengerDetail();">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_48", "Back", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
          </div>
          <!-- end button-->
          <!--button-->
          <div class="ButtonAlignMiddleVoucher">
            <div class="buttonCornerLeft"></div>
            <div class="buttonContent" onclick="loadHome();">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_5_49", "Cancel", _stdLanguage)%>
            </div>
            <div class="buttonCornerRight"></div>
          </div>
          <!-- end button-->
          <div class="clearboth"></div>
        </div>
    </div>
