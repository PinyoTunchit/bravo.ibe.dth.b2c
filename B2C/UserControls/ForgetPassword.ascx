<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.ascx.cs" Inherits="tikAeroB2C.UserControls.ForgetPassword" %>
<div class="xouter">
	<div class="xcontainer">
		<div class="xinner LoginBox">
		
			<!-- <div class="LoginBoxHeader">
				<div class="LoginHeaderLeft"></div>
				<div class="LoginHeader LoginMiddleContent"></div>
				<div class="LoginHeaderRight"></div>
			</div> -->
			
			<!-- <div class="clear-all"></div> -->
			
			<!-- <div class="LoginBoxInnerContent"> -->
			
				<div class="LoginHeader">
					<div class="mainlogo">
						<img src="App_Themes/Default/Images/mainlogo.png" alt="" title="">
					</div>
				</div>
			
				<div class="innerboxlogin">
					<div class="ForgetPass">
						<div class="Topic"><%=tikAeroB2C.Classes.Language.Value("Registration_57", "Forget Password", _stdLanguage)%></div>
						<div class="ForgetPasstext"><%=tikAeroB2C.Classes.Language.Value("Registration_64", "Please enter your Username and we will send your password.", _stdLanguage)%></div>
						
						<div class="ContactDetail">
							<label for="txtUserID"><%=tikAeroB2C.Classes.Language.Value("Registration_65", "User-ID (4-15 characters)", _stdLanguage)%></label>
							<div class="InformationInput">
								<input id="txtUserID" type="text" />
							</div>
							<div class="clear-all"></div>
						</div>
					
						<div class="clear-all"></div>

					</div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="continuelogin">
					<div class="ButtonAlignLeft">
						<a href="javascript:CloseDialog();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_8", "Back", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_8", "Back", _stdLanguage)%>
							</span>
						</a>
					</div>
				
					<div class="ButtonAlignRight">
						<a href="javascript:ForgetPassword();" title="<%=tikAeroB2C.Classes.Language.Value("Registration_66", "Request", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Registration_66", "Request", _stdLanguage)%>
							</span>
						</a>
					</div>
				</div>
				
			<!-- </div> -->
			
			<!-- <div class="LoginBoxFooter">
				<div class="LoginFooterLeft"></div>
				<div class="LoginFooter LoginMiddleContent"></div>
				<div class="LoginFooterRight"></div>
			</div> -->
			
		</div>
	</div>
</div>
<div class="clear-all"></div>