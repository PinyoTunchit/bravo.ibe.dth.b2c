<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registration_Edit.ascx.cs" Inherits="tikAeroB2C.UserControls.Registration_Edit" %>
<div class="stepbar">
	<ul>
		<li class="step1">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Flight selection", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step2">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Personal information", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step3">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Purchase", _stdLanguage)%>
            </div>
		</li>
	</ul>

</div>

<div class="clear-all"></div>

<div class="Content">
	<div class="col-one">
		<div id="ClientProfileInfo" class="MyProfile">
			<div class="BoxtopleftCorner"></div>
		
			<div class="Boxtopmiddle"><%=tikAeroB2C.Classes.Language.Value("Registration_7", "User Profile", _stdLanguage)%></div>
			<div class="BoxtoprightCorner"></div>
		
			<div class="clear-all"></div>
		
			<div id="graygradient">
				<div class="whitebox">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>
				
					<div class="clear-all"></div>	
				
					<div class="innerwhitebox">
						<div class="Topinformation">
							<div class="ContactDetail">
								<label for="ctl00_ddlTitle" class="toptitle">
									<%=tikAeroB2C.Classes.Language.Value("Registration_27", "Title", _stdLanguage)%>
								</label>
							
								<div class="clear-all"></div>
							
								<div class="InformationInput">
									<select id="ddlTitle" class="Selectbox" runat="server">
                                        <option value="" class="watermarkOn"></option>
                                    </select>
									<div class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
                            <div class="ContactDetail">
								<label for="ctl00_txtLastName" class="topname">
									<%=tikAeroB2C.Classes.Language.Value("Registration_28", "Last Name", _stdLanguage)%>
								</label>	
							
								<div class="clear-all"></div>
							
								<div class="InformationInput">
									<input id="txtLastName" type="text" onkeypress="return CheckCharacter();" runat="server" maxlength="60" />
									<div id="spLastName" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>

							<div class="ContactDetail">
								<label for="ctl00_txtFirstName" class="topname">
									<%=tikAeroB2C.Classes.Language.Value("Registration_29", "First Name", _stdLanguage)%>
								</label>
							
								<div class="clear-all"></div>
							
								<div class="InformationInput">
									<input id="txtFirstName" type="text" onkeypress="return CheckCharacter();" runat="server" maxlength="60"/>
									<div id="spFirstName" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
							<div class="clear-all"></div>
						
						</div><!-- END Topinformation -->
					
						<div class="leftdetail Last">
							<div class="ContactDetail">
								<label for="ctl00_txtEmail">
									<%=tikAeroB2C.Classes.Language.Value("Registration_41", "Email", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtEmail" type="text" runat="server" maxlength="60" />
									<div id="spEmail" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtMobileEmail">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_62", "Optional Email", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtMobileEmail" type="text" runat="server" maxlength="60" />
									<div id="sptxtEmail2" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtHomePhone">
									<%=tikAeroB2C.Classes.Language.Value("Registration_39", "Home No", _stdLanguage)%>
								</label>
								
								<div class="InformationInput">
									<input id="txtHomePhone" type="text" runat="server" maxlength="60" />
									<div id="" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtMobilePhone">
									<%=tikAeroB2C.Classes.Language.Value("Registration_38", "Mobile No", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtMobilePhone" type="text" runat="server" maxlength="60" />
									<div id="spMobilePhone" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<!-- <div class="optional">
							
								<div class="Remember FirstLine">
									<input id="chkRemember" name="Input" type="checkbox" value="" />
									<label for="chkRemember" >
										<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_17", "Remember these details for next visit.", _stdLanguage)%>
									</label>
								
									<div class="demoA">
	
										<div class="fg-tooltip ui-widget ui-widget-header ui-corner-all" style="display: none;">
	
											<ul class="tooltipcontent">
												<li>* By creating a member profile on Tassili.com: </li>
												<li>* Making new bookings is simple!</li>
												<li>* You can manage and list all your bookings</li>
												<li>* Receive specials and fare promotions</li>
												<li>* keep all your details up to </li>
											</ul>
											
											 <div class="fg-tooltip-pointer-down ui-widget-header">
												<div class="fg-tooltip-pointer-down-inner"></div>
											 </div>
										</div>
							
										<p class="demoText"><a href="#" class="tooltip"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_61", "more", _stdLanguage)%></a></p>
							
									</div>
									
									
								</div>

							</div> -->
							
							<div class="clear-all"></div>
						
						</div><!-- END leftdetail -->
					
						<div class="none">
							<li class="Left">
								<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_40", "Business Phone", _stdLanguage)%> </div>
								<div class="Detail"><input type="text" ID="txtBusinessPhone" runat="server" /></div>
							</li>
						
							<div class="clear-all"></div>
						
							<li class="Left">
								<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_42", "Language", _stdLanguage)%> </div>
								<div class="Detail">
									<select id="optLanguage" runat="server">
                                        <option value="" class="watermarkOn"></option>
                                    </select>
								</div>
							</li>
						
							<div class="clear-all"></div>
						
							<li class="Right">
								<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_44", "Address 2 (Optional)", _stdLanguage)%> </div>
								<div class="Detail">
									<input type="text" ID="txtAddressline2" runat="server" />
								</div>
							</li>
						
						</div><!-- END none -->
					
						<div class="rightdetail Last">
							<div class="ContactDetail">
								<label for="ctl00_txtAddress1">
									<%=tikAeroB2C.Classes.Language.Value("Registration_43", "Address 1", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtAddress1" type="text" runat="server" maxlength="60" />
									<div id="spAddress1" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtcity">
									<%=tikAeroB2C.Classes.Language.Value("Registration_46", "Town/City", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtcity" type="text" runat="server" maxlength="60" />
									<div id="spTownship" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtState">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_14", "State", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtState" type="text" runat="server" maxlength="60" />
									<div id="spState" class="mandatory"></div>
								</div> 
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtZipCode">
									<%=tikAeroB2C.Classes.Language.Value("Registration_45", "Zip Code", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtZipCode" type="text" runat="server" maxlength="60" />
									<div id="spZipCode" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_ddlCountry">
									<%=tikAeroB2C.Classes.Language.Value("Registration_47", "Country", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<select id="ddlCountry" class="Selectbox" runat="server">
                                        <option value="" class="watermarkOn"></option>
                                    </select>
									<div class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
						</div><!-- END leftdetail Last -->
					
						<div class="clear-all"></div>
						
						<div class="Profilebuttoncontrol">
							<div class="ButtonAlignLeft" id="bttEdit">
								<a class="defaultbutton" href="javascript:enableField('ctl00_');" title="<%=tikAeroB2C.Classes.Language.Value("Registration_51", "Edit", _stdLanguage)%>">
									<span><%=tikAeroB2C.Classes.Language.Value("Registration_51", "Edit", _stdLanguage)%></span>
								</a>
							</div>
							
							<div class="ButtonAlignRight" id="bttSave">
								<a class="defaultbutton" href="javascript:ClientProfileSave('ctl00_',0);" title="<%=tikAeroB2C.Classes.Language.Value("Registration_50", "Save", _stdLanguage)%>">
									<span><%=tikAeroB2C.Classes.Language.Value("Registration_50", "Save", _stdLanguage)%></span>
								</a>
							</div>
							
							<div class="clear-all"></div>
						
						</div>
					
						<div id="dvPassengerProfileWrapper">
							<div class="Familymember">
								<div class="ErrorList">  <asp:Label ID="LabError" runat="server"></asp:Label></div>
							
								<div class="Topic">
                                    <%=tikAeroB2C.Classes.Language.Value("Registration_62", "Family member", _stdLanguage)%>
                                </div>
								<div class="addmember" id="bttPassengerAdd">
									<div id="bttPassengerAdds"></div>
									<a href="javascript:ControlBtt();">
										<div class="addleft"></div>
										<div class="add">
											<%=tikAeroB2C.Classes.Language.Value("Registration_10", "add", _stdLanguage)%>
										</div>
										<div class="addright"></div>
									</a>
								</div>
							
								<div class="clear-all"></div>
							
								<div id="dvShowpassenger">
									<div id="dvPassengerAdd" runat="server"></div>   
								</div>
							
								<div class="clear-all"></div>
							
								<div id="dvNewPassenger" style="display:none;">
								
									<div class="passengernumber">
                                        <%=tikAeroB2C.Classes.Language.Value("Registration_52", "Passenger", _stdLanguage)%>
                                        <span id="spnPaxNo"></span>
                                    </div>
								
									<div class="clear-all"></div>
								
									<div class="Familymember">
										<div class="ContactDetail">
											<label for="ddlPsgTitle" class="toptitle">
												<%=tikAeroB2C.Classes.Language.Value("Registration_27", "Title", _stdLanguage)%>
											</label>
										
											<div class="clear-all"></div>
										
											<div class="InformationInput">
												<select id="ddlPsgTitle" class="Selectbox" runat="server">
                                                    <option value="" class="watermarkOn"></option>
                                                </select>
											</div>
											<div class="mandatory"></div>
										
											<div class="clear-all"></div>
										
										</div>
									
										<div class="ContactDetail">
											<label for="txtPsgLastName" class="topname">
												<%=tikAeroB2C.Classes.Language.Value("Registration_28", "Last Name", _stdLanguage)%>
											</label>	
										
											<div class="clear-all"></div>
										
											<div class="InformationInput">
												<input id="txtPsgLastName" type="text" onkeypress="return CheckCharacter();" runat="server" maxlength="60" />
											</div>
											<div id="spPsgLastName" class="mandatory"></div>
										
											<div class="clear-all"></div>
										
										</div>
									    <div class="ContactDetail">
											<label for="txtPsgFirstName" class="topname">
												<%=tikAeroB2C.Classes.Language.Value("Registration_29", "First Name", _stdLanguage)%>
											</label>
										
											<div class="clear-all"></div>
										
											<div class="InformationInput">
												<input id="txtPsgFirstName" type="text" onkeypress="return CheckCharacter();" runat="server" maxlength="60"/>
											</div>
											<div id="spPsgFirstName" class="mandatory"></div>
										
											<div class="clear-all"></div>
										
										</div>
										<div class="ContactDetail">
											<label for="ddlPassengerRole" class="topname">
												<%=tikAeroB2C.Classes.Language.Value("Registration_9", "Passenger Role", _stdLanguage)%>
											</label>
										
											<div class="clear-all"></div>
										
											<div class="InformationInput">
												<select id="ddlPassengerRole" class="role" runat="server">
                                                    <option value="" class="watermarkOn"></option>
                                                </select>
												<div class="mandatory"></div>
											</div>
										
											<div class="clear-all"></div>
										
										</div>
									
										<div class="clear-all"></div>
									
										<div class="ContactDetail marginleft10">
											<label for="TxtPsgDateofBirth" class="topname">
												<%=tikAeroB2C.Classes.Language.Value("Registration_30", "Date of Birth", _stdLanguage)%>
											</label>
										
											<div class="clear-all"></div>
										
											<div class="InformationInput">
												<input id="TxtPsgDateofBirth" type="text" runat="server" maxlength="60" />
												<div id="spPsgDateofBirth" class="mandatory"></div>
											</div>
											
										
											<div class="clear-all"></div>
										
										</div>
										
										<div class="ContactDetail FFPpassengerType">
											<label for="ddlPassengerType" class="topname">
												<%=tikAeroB2C.Classes.Language.Value("Registration_3", "Passenger Type", _stdLanguage)%> 
											</label>
											
											<div class="clear-all"></div>
											
											<div class="InformationInput">
												<select id="ddlPassengerType"> 
	                                                <option value="" class="watermarkOn"></option>
													<option value="ADULT">
	                                                    <%=tikAeroB2C.Classes.Language.Value("Registration_15", "Adult", _stdLanguage)%>
	                                                </option>
													<option value="CHD">
	                                                    <%=tikAeroB2C.Classes.Language.Value("Registration_16", "Child", _stdLanguage)%>
	                                                </option>
													<option value="INF">
	                                                    <%=tikAeroB2C.Classes.Language.Value("Registration_17", "Infant", _stdLanguage)%>
	                                                </option>
												</select>
											</div>
										
										</div>
									
									</div><!-- END Topinformation -->
								    
										
									<div class="none">
									
										<ul>
										
											<div class="clear-all"></div>
										
											<li class="Right">
												<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_31", "Nationality", _stdLanguage)%></div>
												<div class="Detail">
													<select id="optNationality" runat="server">
                                                        <option value="" class="watermarkOn"></option>
                                                    </select>
												</div>
											</li>
										
											<div class="clear-all"></div>
										
											<li class="Left">
												<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_32", "Document Type", _stdLanguage)%> </div>
												<div class="Detail">   
                                                    <select id="optDocumentType" runat="server">  
                                                        <option value="" class="watermarkOn"></option>
                                                    </select>  
                                                </div>
											</li>
										
											<li class="Right">
												<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_33", "Document Number", _stdLanguage)%><span id="spPsgDocumentNumber" class="RequestStar">*</span></div>
												<div class="Detail"><input type="text" ID="txtPsgDocumentNumber" runat="server" /></div>
											</li>
										
											<div class="clear-all"></div>
										
											<li class="Left">
												<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_34", "Place of Issue", _stdLanguage)%><span id="spPsgPlaceOfIssue" class="RequestStar">*</span></div>
												<div class="Detail"><input type="text" ID="txtPsgPlaceOfIssue" runat="server" /></div>
											</li>
										
											<li class="Right">
												<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_35", "Place of Birth", _stdLanguage)%><span id="spPsgPlaceOfBirth" class="RequestStar">*</span></div>
												<div class="Detail"><input type="text" ID="txtPsgPlaceOfBirth" runat="server" /></div>
											</li>
										
											<div class="clear-all"></div>
										
											<li class="Left">
												<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_36", "Issue Date", _stdLanguage)%><span id="spPsgIssueDate" class="RequestStar">*</span></div>
												<div class="Detail"><input type="text" ID="txtPsgIssueDate" runat="server" /></div>
											</li>
										
											<li class="Right">
												<div class="Title"><%=tikAeroB2C.Classes.Language.Value("Registration_37", "Expiry Date", _stdLanguage)%><span id="spPsgExpiryDate" class="RequestStar">*</span></div>
												<div class="Detail"><input type="text" ID="txtPsgExpiryDate" runat="server" /></div>
											</li>
										
											<div class="clear-all"></div>
										</ul>
									</div><!-- END none -->
								
									<div class="clear-all"></div>
								
									<div class="ErrorList">  <asp:Label ID="ErrorNewPassenger" runat="server"></asp:Label></div>
									
									<div class="clear-all"></div>
								
									<li class="Right">
										<div class="ButtonAlignLeft">
											<a class="defaultbutton" href="javascript:LoadRegistration_Edit();" title="<%=tikAeroB2C.Classes.Language.Value("Registration_53", "Cancel", _stdLanguage)%>">
												<span><%=tikAeroB2C.Classes.Language.Value("Registration_53", "Cancel", _stdLanguage)%></span>
											</a>
										</div>           	
										
										<div class="ButtonAlignRight">
											<a class="defaultbutton" href="javascript:NewPassenger('ctl00_');" title="<%=tikAeroB2C.Classes.Language.Value("Registration_50", "Save", _stdLanguage)%>">
												<span><%=tikAeroB2C.Classes.Language.Value("Registration_50", "Save", _stdLanguage)%></span>
											</a>
										</div>
									</li>
									
								</div><!-- END dvNewPassenger -->
							
								<div class="clear-all"></div>
							
							</div><!-- END Familymember -->
						
						</div><!-- END dvPassengerProfileWrapper -->
					
						<div class="clear-all"></div>

					</div><!-- END innerwhitebox -->
					
					<div class="BottominnerBox">
							<div class="whiteboxbottomleft"></div>
							<div class="whiteboxbottomcontent"></div>
							<div class="whiteboxbottomright"></div>
						</div>
				
					<div class="clear-all"></div>
					
				</div><!-- END whitebox -->

				<div class="clear-all"></div>

			</div><!-- end graygradient -->
			
			<div class="clear-all"></div>
			
			<div class="BottomBox">
				<div class="BoxdownleftCorner"></div>
				<div class="BoxdownmiddleCorner"></div>
				<div class="BoxdownrightCorner"></div>
			</div>
			<div class="clear-all"></div>
		</div>	
		
		<div class="clear-all"></div>
		
		<div  id="dvRegPassword" class="ChangePassword">
			<div class="BoxtopleftCorner"></div>
			<div class="Boxtopmiddle"><%=tikAeroB2C.Classes.Language.Value("Registration_5", "Contact Details", _stdLanguage)%></div>
			<div class="BoxtoprightCorner"></div>
			
			<div class="clear-all"></div>
			
			<div id="graygradient">
				<div class="whitebox">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>
					
					<div class="Registerinput">
						<div class="innerwhitebox">
							<div class="ContactDetail">
								<label for="ctl00_txtCurrentPassword">
									<%=tikAeroB2C.Classes.Language.Value("Registration_63", "Current Password", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtCurrentPassword" type="Password" runat="server" maxlength="60" />
									<div id="spEmail" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>	
							
							<div class="ContactDetail">
								<label for="ctl00_txtPassword">
									<%=tikAeroB2C.Classes.Language.Value("Registration_13", "Password", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtPassword" type="Password" runat="server" TextMode="Password" maxlength="60" />
								</div>
								<div id="spPassword" class="mandatory"></div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail">
								<label for="ctl00_txtReconfirmPassword">
									<%=tikAeroB2C.Classes.Language.Value("Registration_48", "Confirm Password", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtReconfirmPassword" type="Password" runat="server" TextMode="Password" maxlength="60" />
								</div>
								<div id="spConfirmPassword" class="mandatory"></div>
								<div class="clear-all"></div>
							</div>
											
						</div><!-- innerwhitebox -->
					</div>
					
					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
					
					<div class="clear-all"></div>
				
				</div><!--  whitebox -->

			</div>
			
			<div class="clear-all"></div>
			
			<div class="BottomBox">
				<div class="BoxdownleftCorner"></div>
				<div class="BoxdownmiddleCorner"></div>
				<div class="BoxdownrightCorner"></div>
			</div>
			
			<div class="clear-all"></div>
			
			<div class="BTN-Search">
				<div class="ButtonAlignRight" onclick="ClientProfileSave('ctl00_',0);">
					<a class="defaultbutton" href="#" title="<%=tikAeroB2C.Classes.Language.Value("Registration_50", "Save", _stdLanguage)%>" onclick="return false;">
						<span><%=tikAeroB2C.Classes.Language.Value("Registration_50", "Save", _stdLanguage)%></span>
					</a>
				</div>
				<div class="clear-all"></div>	
			</div>
		
		</div><!-- END ChangePassword -->
	    
	</div>
</div>



