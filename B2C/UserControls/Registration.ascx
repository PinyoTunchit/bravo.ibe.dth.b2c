<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registration.ascx.cs" Inherits="tikAeroB2C.UserControls.Registration" %>
<div class="stepbar">
	<ul>
		<li class="step1">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Flight selection", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step2">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Personal information", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step3">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Purchase", _stdLanguage)%>
            </div>
		</li>
	</ul>

</div>

<div class="clear-all"></div>

<div class="Content">
	<div class="col-one">
		<div id="ClientRegister" class="MyProfile">
			<div class="BoxtopleftCorner"></div>
			<div class="Boxtopmiddle"><%=tikAeroB2C.Classes.Language.Value("Registration_25", "Registration", _stdLanguage)%></div>
			<div class="BoxtoprightCorner"></div>
		
			<div class="clear-all"></div>
		
			<div id="graygradient">
				<div class="whitebox">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>
					
					<div class="Registerinput">
					<div class="innerwhitebox">
						<div class="Topinformation">
							<div class="ContactDetail">
								<label for="ctl00_ddlTitle" class="toptitle">
									<%=tikAeroB2C.Classes.Language.Value("Registration_27", "Title", _stdLanguage)%>
								</label>
							
								<div class="clear-all"></div>
							
								<div class="InformationInput">
									<select id="ddlTitle" class="Selectbox" runat="server">
                                        <option value="" class="watermarkOn"></option>
                                    </select>
									<div class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>

							<div class="ContactDetail">
								<label for="ctl00_txtLastName" class="topname">
									<%=tikAeroB2C.Classes.Language.Value("Registration_28", "Last Name", _stdLanguage)%>
								</label>	
							
								<div class="clear-all"></div>
							
								<div class="InformationInput">
									<input id="txtLastName" type="text" onkeypress="return CheckCharacter();" runat="server" maxlength="60" />
									<div id="spLastName" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
                            <div class="ContactDetail">
								<label for="ctl00_txtFirstName" class="topname">
									<%=tikAeroB2C.Classes.Language.Value("Registration_29", "First Name", _stdLanguage)%>
								</label>
							
								<div class="clear-all"></div>
							
								<div class="InformationInput">
									<input id="txtFirstName" type="text" onkeypress="return CheckCharacter();" runat="server" maxlength="60"/>
									<div id="spFirstName" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
							
							<div class="ContactDetail FFPpassengerType FFPRegister">
								<label for="ddlPassengerType" class="topname">
									<%=tikAeroB2C.Classes.Language.Value("Registration_3", "Passenger Type", _stdLanguage)%> 
								</label>
								
								<div class="clear-all"></div>
								<div class="InformationInput">
									<select id="ddlPassengerType" >
						                <option value="" class="watermarkOn"></option>
										<option value="ADULT">
											<%=tikAeroB2C.Classes.Language.Value("Registration_15", "Adult", _stdLanguage)%>
										</option>
										<option value="CHD">
											<%=tikAeroB2C.Classes.Language.Value("Registration_16", "Child", _stdLanguage)%>
										</option>
									</select>
								</div>
							</div>

							<div class="clear-all"></div>
						
						</div>
						
						<div class="leftdetail">
							<div class="ContactDetail">
								<label for="ctl00_txtEmail">
									<%=tikAeroB2C.Classes.Language.Value("Registration_41", "Email", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtEmail" type="text" runat="server" maxlength="60" />
									<div id="spEmail" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtMobileEmail">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_62", "Optional Email", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtMobileEmail" type="text" runat="server" maxlength="60" />
									<div id="sptxtEmail2" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtHomePhone">
									<%=tikAeroB2C.Classes.Language.Value("Registration_39", "Home No", _stdLanguage)%>
								</label>
								
								<div class="InformationInput">
									<input id="txtHomePhone" type="text" runat="server" maxlength="60" />
									<div id="" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtMobilePhone">
									<%=tikAeroB2C.Classes.Language.Value("Registration_38", "Mobile No", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtMobilePhone" type="text" runat="server" maxlength="60" />
									<div id="spMobilePhone" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="optional">
							
								<div class="Remember FirstLine">
									<input id="chkReceivemails" name="Input" type="checkbox" value="" checked />
									<label for="chkReceivemails" >
										<%=tikAeroB2C.Classes.Language.Value("Registration_59", "Receive e-mails from Tassili in the Future.", _stdLanguage)%>
									</label>

								</div>
							
							</div>
							
							<div class="clear-all"></div>
						
						</div>
						
						<div class="leftdetail Last">
							<div class="ContactDetail">
								<label for="ctl00_txtAddress1">
									<%=tikAeroB2C.Classes.Language.Value("Registration_43", "Address 1", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtAddress1" type="text" runat="server" maxlength="60" />
									<div id="spAddress1" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtcity">
									<%=tikAeroB2C.Classes.Language.Value("Registration_46", "Town/City", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtcity" type="text" runat="server" maxlength="60" />
									<div id="spTownship" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtState">
									<%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_14", "State", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtState" type="text" runat="server" maxlength="60" />
									<div id="spState" class="mandatory"></div>
								</div> 
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_txtZipCode">
									<%=tikAeroB2C.Classes.Language.Value("Registration_45", "Zip Code", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<input id="txtZipCode" type="text" runat="server" maxlength="60" />
									<div id="spZipCode" class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
							<div class="ContactDetail">
								<label for="ctl00_ddlCountry">
									<%=tikAeroB2C.Classes.Language.Value("Registration_47", "Country", _stdLanguage)%>
								</label>
							
								<div class="InformationInput">
									<select id="ddlCountry" class="Selectbox" runat="server">
                                        <option value="" class="watermarkOn"></option>
                                    </select>
									<div class="mandatory"></div>
								</div>
								<div class="clear-all"></div>
							</div>
						
						</div>
						
						<div class="clear-all"></div>
						
					</div>
					</div>
					<div class="clear-all"></div>
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
					<div class="clear-all"></div>
				
				</div>
				<div class="clear-all"></div>
			</div>
			
			<div class="clear-all"></div>
			
			<div class="BottomBox">
				<div class="BoxdownleftCorner"></div>
				<div class="BoxdownmiddleCorner"></div>
				<div class="BoxdownrightCorner"></div>
			</div>
			
			<div class="clear-all"></div>
			
		</div>
		
		<div class="clear-all"></div>
		
		<div class="ChangePassword">
			<div class="BoxtopleftCorner"></div>
			<div class="Boxtopmiddle">
                <%=tikAeroB2C.Classes.Language.Value("Registration_13", "Password", _stdLanguage)%>
            </div>
			<div class="BoxtoprightCorner"></div>
			
			<div class="clear-all"></div>
			
			<div id="graygradient">
				<div class="whitebox">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>
					
					<div class="Registerinput">
					<div class="innerwhitebox">
						<div class="ContactDetail">
							<label for="ctl00_txtPassword">
								<%=tikAeroB2C.Classes.Language.Value("Registration_13", "Password", _stdLanguage)%>
							</label>
						
							<div class="InformationInput">
								<input id="txtPassword" type="Password" runat="server" TextMode="Password" maxlength="60" />
							</div>
							<div id="spPassword" class="mandatory"></div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="ctl00_txtReconfirmPassword">
								<%=tikAeroB2C.Classes.Language.Value("Registration_48", "Confirm Password", _stdLanguage)%>
							</label>
						
							<div class="InformationInput">
								<input id="txtReconfirmPassword" type="Password" runat="server" TextMode="Password" maxlength="60" />
							</div>
							<div id="spConfirmPassword" class="mandatory"></div>
							<div class="clear-all"></div>
						</div>
										
					</div><!-- innerwhitebox -->
					</div>
					
					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
					
					<div class="clear-all"></div>
				
				</div><!--  whitebox -->

			</div>
			
			<div class="clear-all"></div>
			
			<div class="BottomBox">
				<div class="BoxdownleftCorner"></div>
				<div class="BoxdownmiddleCorner"></div>
				<div class="BoxdownrightCorner"></div>
			</div>
			
			<div class="clear-all"></div>
		</div>

		<div class="none">
			<ul>
				<li class="Right">
					
					
				</li>
			
				<div class="clear-all"></div>
				
				<li class="Left">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_30", "Date of Birth", _stdLanguage)%><span id="spDateOfBirth" class="RequestStar">*</span>
					</div>
					<div class="Detail">
						<input type="text" ID="txtDateofBirth" runat="server" />
					</div>
				</li>
				
				<li class="Right">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_6", "Nationality", _stdLanguage)%></div>
					<div class="Detail">
						<select id="optNationality" runat="server">
                            <option value="" class="watermarkOn"></option>
                        </select>
					</div>
				</li>
				<div class="clear-all"></div>
			</ul>
			
			<div class="BoxGrayContentHeader">
				<%=tikAeroB2C.Classes.Language.Value("Registration_4", "Document Information", _stdLanguage)%>
			</div>
			
			<ul>
				<li class="Left">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_32", "Document Type", _stdLanguage)%>
					</div>
					<div class="Detail">
						<select id="optDocumentType" runat="server">
                            <option value="" class="watermarkOn"></option>
                        </select>
					</div>
				</li>
				
				<li class="Right">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_33", "Document Number", _stdLanguage)%><span id="spDocNumber" class="RequestStar">*</span>
					</div>
					<div class="Detail">
						<input type="text" ID="txtDocumentNumber" runat="server" />
					</div>
				</li>
				
				<div class="clear-all"></div>
				
				<li class="Left">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_34", "Place of Issue", _stdLanguage)%><span id="spPlaceOfIssue" class="RequestStar">*</span>
					</div>
					<div class="Detail">
						<input type="text" ID="txtPlaceOfIssue" runat="server" />
					</div>
				</li>
				<li class="Right">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_35", "Place of Birth", _stdLanguage)%><span id="spPlaceOfBirth" class="RequestStar">*</span>
					</div>
					<div class="Detail">
						<input type="text" ID="txtPlaceOfBirth" runat="server" />
					</div>
				</li>
				<div class="clear-all"></div>
				<li class="Left">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_36", "Issue Date", _stdLanguage)%><span id="spIssueDate" class="RequestStar">*</span>
					</div>
					<div class="Detail">
						<input type="text" ID="txtIssueDate" runat="server" />
					</div>
				</li>
				<li class="Right">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_37", "Expiry Date", _stdLanguage)%><span id="spExpiryDate" class="RequestStar">*</span>
					</div>
					<div class="Detail">
						<input type="text" ID="txtExpiryDate" runat="server" />
					</div>
				</li>
				<div class="clear-all"></div>
				
				<li class="Left">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_40", "Business Phone", _stdLanguage)%>
					</div>
					<div class="Detail">
						<input type="text" ID="txtBusinessPhone" runat="server" />
					</div>
				</li>
				
				<li class="Left">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_42", "Language", _stdLanguage)%></div>
					<div class="Detail">
						<select id="optLanguage" runat="server">
                            <option value="" class="watermarkOn"></option>
                        </select>
					</div>
				</li>
				<div class="clear-all"></div>
				
				<li class="Right">
					<div class="Title">
						<%=tikAeroB2C.Classes.Language.Value("Registration_44", "Address 2 (Optional)", _stdLanguage)%></div>
					<div class="Detail">
						<input type="text" ID="txtAddressline2" runat="server" />
					</div>
				</li>
				<div class="clear-all"></div>
				
			</ul>
			
			<div class="AcceptTerms">
				<input type="checkbox" id="cboConfirm" value="cboConfirm" runat ="server" />
				<a href="#">
					<%=tikAeroB2C.Classes.Language.Value("Registration_6", "I have read, understood and accept all Terms & Conditions ", _stdLanguage)%>
				</a>
			</div>
		</div>

		<div class="BTN-Search">
			<div class="ButtonAlignRight">
				<a href="javascript:CreateClientProfile('ctl00_');" class="defaultbutton" title="Register">
					<span><%=tikAeroB2C.Classes.Language.Value("Registration_49", "Register", _stdLanguage)%></span>
				</a>
			</div>
		</div>
	
		<div class="ErrorList">
			<asp:Label ID="LabError" runat="server"></asp:Label>
		</div>
	
	</div>
	
	<div class="col-two">
		<div class="topbox">
			<div class="homerightnavtopleft"></div>
			<div class="homerightnavtop"></div>
			<div class="homerightnavtopright"></div>
		</div>

		<div class="Rightnav gradient">
			<div class="WhiteTopic">
                <%=tikAeroB2C.Classes.Language.Value("Registration_60", "About Tassili Profile", _stdLanguage)%>
            </div>
			<div class="WhiteFont">
                <%=tikAeroB2C.Classes.Language.Value("Registration_61", "By creating your Tassili profile ", _stdLanguage)%>
            </div>
		</div>
		<div class="clear-all"></div>
		<div class="bottombox">
			<div class="homerightnavbottomleft"></div>
			<div class="homerightnavbottom"></div>
			<div class="homerightnavbottomright"></div>
		</div>
	
	</div>
</div>