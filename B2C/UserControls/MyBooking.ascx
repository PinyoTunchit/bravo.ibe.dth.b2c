<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyBooking.ascx.cs" Inherits="tikAeroB2C.UserControls.MyBooking" %>
<div class="stepbar">
	<ul>
		<li class="step1">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Flight selection", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step2">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Personal information", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step3">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Purchase", _stdLanguage)%>
            </div>
		</li>
	</ul>

</div>

<div class="clear-all"></div>

<div class="Content">
	<div class="col-one">
		<div class="MyBooking">
			<div class="BoxtopleftCorner"></div>
			<div class="Boxtopmiddle"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_3", "My Booking Search", _stdLanguage)%></div>
			<div class="BoxtoprightCorner"></div>
		
			<div class="clear-all"></div>
		
			<div id="graygradient">
				<div class="whitebox">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>	
				
					<div class="innerwhitebox inputboxdetail">
						<div class="ContactDetail">
							<label for="txtBookingCode">
								<%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_14", "My Booking Reference Number", _stdLanguage)%>
							</label>
						
							<div class="InformationInput">
								<input id="txtBookingCode" type="text" runat="server" maxlength="6" onkeypress="return SubmitEnterSearch(this,event)" />
								<div id="" class="mandatory"></div>
							</div>
							
							<div class="clear-all"></div>
						
						</div>
					
						<div class="booknow">
							<a href="Javascript:MyBookingSearch();" title="<%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_4", "Search", _stdLanguage)%>" class="defaultbutton">
								<span><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_4", "Search", _stdLanguage)%></span>
							</a>
						</div>
					
						<div class="clear-all"></div>
					
					</div>
				<div class="clear-all"></div>
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
				
				</div>
			
				<div class="clear-all"></div>
			
			</div>
		
			<div class="clear-all"></div>
		
			<div class="BottomBox">
				<div class="BoxdownleftCorner"></div>
				<div class="BoxdownmiddleCorner"></div>
				<div class="BoxdownrightCorner"></div>
			</div>
			<div class="clear-all"></div>
		</div>

		<div id="dvResultBox">
			<div class="SearchResultBox">
				<div class="BoxtopleftCorner"></div>
				<div class="Boxtopmiddle"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_6", "Search Result", _stdLanguage)%></div>
				<div class="BoxtoprightCorner"></div>
				
				<div class="clear-all"></div>
				
				<div id="graygradient">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>
					
					<div id="dvSearch"></div> 
					
					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
					
					<div class="clear-all"></div>
										
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				
				<div class="clear-all"></div>
			
			</div>
			
		</div> 
	
		<div class="clear-all"></div>
	
		<div id="dvBookingBox"> 
			<div class="ActiveBooking">
				<div class="BoxtopleftCorner"></div>
				<div class="Boxtopmiddle"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_7", "Live Bookings", _stdLanguage)%></div>
				<div class="BoxtoprightCorner"></div>
			
				<div class="clear-all"></div>
				
				<div id="graygradient">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>

					<div id="dvLifeBooking" runat="server" ></div>
					
					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
					
					<div class="clear-all"></div>
	
				</div>
				
				<div class="clear-all"></div>
				
				<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
				
				<div class="clear-all"></div>
				
			</div>
			
			
			
			<div class="clear-all"></div>

			<div class="BookingHistory">
				<div class="BoxtopleftCorner"></div>
				<div class="Boxtopmiddle"><%=tikAeroB2C.Classes.Language.Value("FFP_My_Booking_8", "Booking History", _stdLanguage)%></div>
				<div class="BoxtoprightCorner"></div>
			
				<div class="clear-all"></div>
				
				<div id="graygradient">
					<div class="whiteboxtopleft"></div>
					<div class="whiteboxtopcontent"></div>
					<div class="whiteboxtopright"></div>
					
					<div id="dvHistory" runat="server"></div>
					<div class="clear-all"></div>
					
					<div class="BottominnerBox">
						<div class="whiteboxbottomleft"></div>
						<div class="whiteboxbottomcontent"></div>
						<div class="whiteboxbottomright"></div>
					</div>
					
					<div class="clear-all"></div>
					
				</div>
				
				<div class="clear-all"></div>
				<div class="BottomBox">
					<div class="BoxdownleftCorner"></div>
					<div class="BoxdownmiddleCorner"></div>
					<div class="BoxdownrightCorner"></div>
				</div>
			
				<div class="clear-all"></div>
			
			</div>
			
			
			
		</div>
	
	</div>
	
</div>