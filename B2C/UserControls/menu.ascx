<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="menu.ascx.cs" Inherits="tikAeroB2C.UserControls.menu" %>

<ul id="nav">

	<% if (System.Configuration.ConfigurationManager.AppSettings["UseDialogLogon"].ToString() == "true")
		{
			if (Session["Client"] == null)
				{%>
					<li class="first">
						<a href="javascript:LoadDialog();" title="<%=tikAeroB2C.Classes.Language.Value("Menu_1", "FrequentFlyer Logon", _stdLanguage)%>">
							<%=tikAeroB2C.Classes.Language.Value("Menu_1", "FrequentFlyer Logon", _stdLanguage)%>
						</a>
					</li>
				<% }
			else
				{%>
					<li class="first">
						<a href="javascript:ClientLogOff();" title="<%=tikAeroB2C.Classes.Language.Value("Menu_2", "FrequentFlyer Logout", _stdLanguage)%>">
							<%=tikAeroB2C.Classes.Language.Value("Menu_2", "FrequentFlyer Logout", _stdLanguage)%>
						</a>
					</li>
				<% }
	}%>
	
</ul>
