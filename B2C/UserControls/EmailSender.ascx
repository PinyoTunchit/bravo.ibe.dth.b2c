<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailSender.ascx.cs" Inherits="tikAeroB2C.UserControls.EmailSender" %>

<div class="xouter">
    <div class="xcontainer">
      <div class="xinner LoadingBoxError">
      <div class="errortop"></div>
      <div class="errorcontent">
	<div class="ErrorBoxHeader"><img src="App_Themes/Default/Images/mainlogo.png" alt="Tassili" /></div>
	<span class="Email"><%=tikAeroB2C.Classes.Language.Value("Passenger_Itinerary_47", "Email:", _stdLanguage)%> <input type="text" id="txtEmail" /></span>
    <div class="ButtonEmailBox">
    <div class="ButtonEmail" onclick="SendItineraryEmailInput();">
			<a class="defaultbutton" href="#">
						<span> <%=tikAeroB2C.Classes.Language.Value("Passenger_Itinerary_59", "Send", _stdLanguage)%></span>
				</a>
	</div>
   
<div class="ButtonEmail" onclick="CloseEmailSender();">
				<a class="defaultbutton" href="#">
				<span> <%=tikAeroB2C.Classes.Language.Value("Passenger_Itinerary_60", "Cencel", _stdLanguage)%></span>
			</a>
		</div>
        
</div>
<div class="clearboth"></div>
	</div>
    <div class="errorbottom"></div>
    </div>
        <div class="clearboth"></div>
    </div>
</div>
