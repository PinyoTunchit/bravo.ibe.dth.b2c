<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Availability.ascx.cs" Inherits="tikAeroB2C.UserControls.Availability" %>
<div class="stepbar1">
	<ul>
		<li class="step1">
			<div class="stepactive">
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Flight selection", _stdLanguage)%>
            </div>
			<div class="flightstepactive"></div>
		</li>
		
		<li class="step2">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Personal information", _stdLanguage)%>
            </div>
			<div class="personalstep"></div>
		</li>
		
		<li class="step3">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Purchase", _stdLanguage)%>
            </div>
			<div class="purchasestep"></div>
		</li>
	</ul>

</div>

<div class="CoverDetailPopup">
    <input id="dvClientOnHold" type="hidden" value="<%=ClientOnHold %>" />
    
	
    <div class="clear-all"></div>
	<div class="Availability">
	    <div id="dvOutwardResult" runat="server" />
	    <div class="clear-all"></div>
	    <div id="dvReturnResult" runat="server" />

		<div class="clear-all"></div>
		
		<div class="BTN-Search">
		
			<div id="dvAvaiNext" class="ButtonAlignRight">
				<a href="javascript:SelectFlight();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_9", "Next", _stdLanguage)%>" class="defaultbutton">
					<span>
						<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_9", "Next", _stdLanguage)%>
					</span>
				</a>
			</div>
		</div>

    </div>
   
</div>