<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchAvailability.ascx.cs" Inherits="tikAeroB2C.UserControls.SearchAvailability" %>
<iframe  style="height:0px; z-index:666;position:absolute; display: none;"  id='fdCal1'  src='' class='frmcls'></iframe>
<iframe  style="height:0px; z-index:555;position:absolute; display: none;"  id='fdCal2'  src='' class='frmcls'></iframe>
<div class="FlightSelectionBoxDefault1">	
	<div id="FlightSelectionDefault">
		<div class="innerbox">
				<ul class="type">
			        <li class="type">
						<input id="optReturn" name="optSearch" type="radio" checked="checked" value="" onclick="ShowHideCalendar();"/> 
						<label for="optReturn">
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_1", "Return", _stdLanguage)%>
						</label> 
			        </li>
					
			        <li class="type">
						<input id="optOneWay" name="optSearch" type="radio" value="" onclick="ShowHideCalendar();"/> 
			            <label for="optOneWay">
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_2", "One Way", _stdLanguage)%>
						</label>
			        </li>
			    </ul>
				<div class="clear-all"></div>
				<ul>
			        <li class="FromTo destinationpoint">
						<label for="AvailabilitySearch1_optOrigin">
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_3", "From", _stdLanguage)%>
						</label>
						<select id="optOrigin" runat="server" onchange="javascript:getDestination();">
                            <option value="" class="watermarkOn"></option>
                        </select>
			        </li>
			
			        <li class="FromTo destinationpoint">
						<label for="AvailabilitySearch1_optDestination">
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_4", "To", _stdLanguage)%>
						</label>
						<select id="optDestination" runat="server">
                            <option value="" class="watermarkOn"></option>
                        </select>
			        </li>
					<li class="clear-all"></li>
					<li class="FromTo" style="display: none;">
						<label for="AvailabilitySearch1_optCurrency">	
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_20", "Currency", _stdLanguage)%>
						</label>
						<select id="optCurrency" runat="server"  ></select> 
			        </li>
			
					<li class="FromTo displaynone">
						<label for="optBoardingClass">
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_14", "Class", _stdLanguage)%>
						</label>
						<select id="optBoardingClass" class="class">
							<option value="Y">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_25", "Economy Class", _stdLanguage)%>
			                </option>
						</select>
			        </li>
			
					<li class="clear-all"></li>
			        
			        <li class="FromTo">
						<label for="ddlDate_1">
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_5", "Deparature Date", _stdLanguage)%>
						</label>
			        </li>
			
			        <li id="CalenderBase"  class="TabDate">
			            <%= tikAeroB2C.Classes.CalendarHelper.GetCalOutputControl("1", false, false)%>                  
				   <input type="button" title=""  class="calendar_icon" id="calendar"  style="display:inline" onclick="TriggerCalendar1();" />
				   <div id="divCa1"></div>
			        </li> 			
			        <li id='Cal1' class="CAL"></li>
			        
			        <li id="lblReturnDate" class="FromTo">
						<label for="ddlDate_2">
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_6", "Return Date", _stdLanguage)%>
						</label>
			        </li>
			
			        <li id="calRen2" class="TabDate">
			            <%= tikAeroB2C.Classes.CalendarHelper.GetCalOutputControl("2", false, false)%>                  
					<input type="button" title=""  class="calendar_icon" id="calendar2"  style="display:inline" onclick="TriggerCalendar2();" /> 
				       <div id="divCa2"></div>
			        </li>
			        <li id='Cal2'   style='POSITION: absolute;Z-INDEX: 1001;' class="CAL"></li>
					
					<li class="clear-all"></li>
				</ul>
				<div class="clear-all"></div>				
				<ul class="PassengerSelection">
					<li class="FromTo">
						<label for="optAdult">
                            <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_29", "Guests", _stdLanguage)%>
                        </label>
					</li>
				
			        <li class="passenger">
						<select id="optAdult" class="pax">
							<option value="0">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_11", "Adult", _stdLanguage)%>
			                </option>
							<option value="1" selected="selected">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_11", "Adult", _stdLanguage)%> 1
			                </option>
							<option value="2">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_8", "Adults", _stdLanguage)%> 2
			                </option>
							<option value="3">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_8", "Adults", _stdLanguage)%> 3
			                </option>
							<option value="4">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_8", "Adults", _stdLanguage)%> 4
			                </option>
							<option value="5">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_8", "Adults", _stdLanguage)%> 5
			                </option>
							<option value="6">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_8", "Adults", _stdLanguage)%> 6
			                </option>
							<option value="7">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_8", "Adults", _stdLanguage)%> 7
			                </option>
							<option value="8">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_8", "Adults", _stdLanguage)%> 8
			                </option>
							<option value="9">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_8", "Adults", _stdLanguage)%> 9
			                </option>
						</select>
						
			        </li>
					<li class="passenger">
						<select id="optChild" class="pax">
							<option value="0">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_12", "Child", _stdLanguage)%>
			                </option>
							<option value="1">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_12", "Child", _stdLanguage)%> 1 
			                </option>
							<option value="2">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_9", "Children", _stdLanguage)%> 2 
			                </option>
							<option value="3">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_9", "Children", _stdLanguage)%> 3 
			                </option>
							<option value="4">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_9", "Children", _stdLanguage)%> 4 
			                </option>
							<option value="5">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_9", "Children", _stdLanguage)%> 5 
			                </option>
							<option value="6">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_9", "Children", _stdLanguage)%> 6 
			                </option>
							<option value="7">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_9", "Children", _stdLanguage)%> 7 
			                </option>
							<option value="8">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_9", "Children", _stdLanguage)%> 8 
			                </option>
						</select>
						
					</li>
			        <li class="passenger">
			            <select id="optInfant" class="pax">
			                <option value="0">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_13", "Infant", _stdLanguage)%>
			                </option>
			                <option value="1">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_13", "Infant", _stdLanguage)%> 1
			                </option>
			                <option value="2">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_10", "Infants", _stdLanguage)%> 2
			                </option>
			                <option value="3">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_10", "Infants", _stdLanguage)%> 3
			                </option>
			                <option value="4">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_10", "Infants", _stdLanguage)%> 4
			                </option>
			                <option value="5">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_10", "Infants", _stdLanguage)%> 5
			                </option>
			                <option value="6">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_10", "Infants", _stdLanguage)%> 6
			                </option>
			                <option value="7">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_10", "Infants", _stdLanguage)%> 7
			                </option>
			                <option value="8">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_10", "Infants", _stdLanguage)%> 8
			                </option>
			                <option value="9">
			                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_10", "Infants", _stdLanguage)%> 9
			                </option>
			            </select>
						
			        </li>
			        <li class="clear-all"></li>
			        
			        <li id="dvPromoCode" style="display:none;">
                        <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_15", "Promo code", _stdLanguage)%>
				        <input id="txtPromoCode" type="text" />
                    </li>
			        
			    </ul>
			   
			    <div class="SearchFlight">
					<a href="javascript:SearchAvailabilityParameter();" title="<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_16", "Book Now", _stdLanguage)%>" class="defaultbutton">
						<span id="spnSearchCap">
							<%=tikAeroB2C.Classes.Language.Value("Flight_Selection_16", "Book Now", _stdLanguage)%>
						</span>
					</a>
				</div>
			     <div class="clear-all"></div>

                 <div id="dvlowFareFinderOption" runat="server">
                    <div class="SearchFlightBy">
					    <div class="SearchFlightByOption">
						    <input type="radio" id="rdSearchTypeDate" name="rdSearchType" checked="checked"/> 
						    <label for="rdSearchTypeDate">
                              <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_37", "Search by Date", _stdLanguage)%>
                            </label>
					    </div>
					
					    <div class="SearchFlightByOption">
		                    <input type="radio" id="rdSearchTypeCal" name="rdSearchType"/> 

						    <div class="lowfaretooltip FloatLeft">
							    <p class="LowFareCalendarText">
									    <label for="rdSearchTypeCal">
                                            <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_38", "Low Fare calendar", _stdLanguage)%>
                                        </label>
										    </p>
						    </div>
					    </div>

                    </div>
                 </div>
				
				
				<div class="clear-all"></div>

				<div class="flightselectioncontent">
                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_30", "Adult(12+ years) Child (2-11years) Infant (0-23 month(s))", _stdLanguage)%>
				</div>
				
				<div class="flightselectioncontent" style=" display: none;">
                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_31", "Do you need special assistance?", _stdLanguage)%>
                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_32", "Unaccompanied Children?", _stdLanguage)%>
                    <%=tikAeroB2C.Classes.Language.Value("Flight_Selection_33", "Group bookings", _stdLanguage)%>
				</div>
				

                <div class="clear-all"></div>
				<div onclick="CallCOB()" class="ChangeOfBooking"><%=tikAeroB2C.Classes.Language.Value("Passenger_Itinerary_70", "Manage Booking", _stdLanguage)%></div>
								               
<%--                <div id="dvCheckIn" onclick="OpenWebCheckIn()" >Web CheckIn</div>
--%>
                <div class="clear-all"></div>
			</div>	
		<div class="clear-all"></div>
	</div>	
	<div class="clear-all"></div>
</div>
