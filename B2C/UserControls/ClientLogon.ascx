<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientLogon.ascx.cs" Inherits="tikAeroB2C.UserControls.ClientLogon" %>
<!-- Start FFP Login -->
<div class="xouter">
	<div class="xcontainer">
		<div class="xinner LoginBox">
		
			<!-- <div class="LoginBoxHeader">
				<div class="LoginHeaderLeft"></div>
				<div class="LoginHeader LoginMiddleContent"></div>
				<div class="LoginHeaderRight"></div>
			</div> -->
			
			<!-- <div class="clear-all"></div> -->
			
			<!-- <div class="LoginBoxInnerContent"> -->
			
				<div class="LoginHeader">
					<div class="mainlogo">
						<img src="App_Themes/Default/Images/mainlogo.png" alt="" title="">
					</div>
				</div>
			
				<div class="innerboxlogin">
					<div class="FFPLogin">
						<div class="Topic">
                            <%=tikAeroB2C.Classes.Language.Value("Registration_58", "LOGIN", _stdLanguage)%>
                        </div>
						<div class="ContactDetail">
							<label for="txtClientID">
                                <%=tikAeroB2C.Classes.Language.Value("Registration_12", "Client ID / Email", _stdLanguage)%>
                            </label>
							<div class="InformationInput">
								<input id="txtClientID" type="text" onkeypress="return SubmitEnterUser(this,event,'false')" />
							</div>
							<div class="clear-all"></div>
						</div>
						
						<div class="ContactDetail">
							<label for="txtPassword">
                                <%=tikAeroB2C.Classes.Language.Value("Registration_13", "Password", _stdLanguage)%>
                            </label>
							<div class="InformationInput">
								<input id="txtPassword" type="password" onkeypress="return SubmitEnterUser(this,event,'false')" />
							</div>
							<div class="clear-all"></div>
						</div>
		
						<div class="forgetID">
							<a href="javascript:LoadRegistration();">
								<%=tikAeroB2C.Classes.Language.Value("Registration_49", "Register", _stdLanguage)%>
							</a>
						</div>
					
						<div class="forgetID">
							<a href="javascript:LoadForgetPassword();">
								<%=tikAeroB2C.Classes.Language.Value("Registration_57", "Forget Password", _stdLanguage)%>
							</a>
						</div>
				
						
						<div class="clear-all"></div>
					</div>
				</div>
				
				<div class="clear-all"></div>
				
				<div class="continuelogin">
					<div class="ButtonAlignLeft">
						<a href="javascript:CloseDialog();" title="<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_8", "Back", _stdLanguage)%>" class="defaultbutton">
							<span>
								<%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_8", "Back", _stdLanguage)%>
							</span>
						</a>
					</div>
				
					<div class="ButtonAlignRight">
						<a href="javascript:ClientLogon();" title="Login" class="defaultbutton">
							<span>
                                <%=tikAeroB2C.Classes.Language.Value("Registration_58", "LOGIN", _stdLanguage)%>
                            </span>
						</a>
					</div>
				</div>
				
			<!-- </div> -->
			
			<!-- <div class="LoginBoxFooter">
				<div class="LoginFooterLeft"></div>
				<div class="LoginFooter LoginMiddleContent"></div>
				<div class="LoginFooterRight"></div>
			</div> -->
			
		</div>
	</div>
</div>
<div class="clear-all"></div>
