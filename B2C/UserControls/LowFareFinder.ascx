<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LowFareFinder.ascx.cs" Inherits="tikAeroB2C.UserControls.LowFareFinder" %>

<div class="stepbar">
	<ul>
		<li class="step1">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_1", "Flight selection", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step2">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_3_1", "Personal information", _stdLanguage)%>
            </div>
		</li>
		
		<li class="step3">
			<div>
                <%=tikAeroB2C.Classes.Language.Value("Booking_Step_4_1", "Purchase", _stdLanguage)%>
            </div>
		</li>
	</ul>

</div>

<div class="clear-all"></div>

<div class="LowFareCalendar">
  <!--Outward Flight-->
  <div id="dvOutbound" runat="server">
    <!--<div class="Per2-Outbound"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_25", "Outbound flights", _stdLanguage)%></div>
<div class="Per2-Detail"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_26", "Please select your outbound flight by clicking on the chosen date", _stdLanguage)%>.</div>-->
    <div id="dvOutwardResult" runat="server"/>
  </div>
 <div class="clear-all"></div>
  <!--Return Flight-->
  <div id="dvReturn" runat="server">
    <!--<div class="Per2-Return"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_27", "Return flights", _stdLanguage)%></div>
<div class="Per2-Detail"><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_28", "Please select your return flight by clicking on the chosen date", _stdLanguage)%>.</div>-->
    <div id="dvReturnResult" runat="server"/>
  </div>
 <div class="clear-all"></div>
 
 <div class="BTN-Search">
	 <div id="dvAvaiNext" class="ButtonAlignRight" onclick="GetAvailabilityFromLowFareFinder();">
	    <a class="defaultbutton">
	      <span><%=tikAeroB2C.Classes.Language.Value("Booking_Step_2_9", "Next", _stdLanguage)%></span>
	    </a>
	  </div>
 </div>
  
  <!-- Start Booking Summary -->
  
  <!-- End Booking Summary -->
</div>
